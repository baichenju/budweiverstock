/**
 * Copyright © 2020 eSunny Info. Tech Ltd. All rights reserved.
 *
 * @Package: com.eddc.method
 * @author: jack.zhao
 * @date: 2020年1月15日 下午6:07:10
 */
package com.eddc.method;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.eddc.util.*;
import com.xxl.job.core.log.XxlJobLogger;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.*;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.Parameter;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.task.Hpk_DataCrawlTask;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;

/**
 * 项目名称：Price_monitoring_crawler
 * 类名称：HpkData
 *
 * @author：jack.zhao 创建时间：2020年1月15日 下午6:07:10
 * 类描述：
 */
@Slf4j
@Component
public class HpkData {
	@Autowired
	private ApplicationContext contexts = SpringContextUtil.getApplicationContext();
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	/*抓取多个地区的商品库存*/

	public void regionCrawler(List<QrtzCrawlerTable> listjobName, String tableName, String count, int pageTop, int sum, Map<String, String> mapParam) throws IOException {
		Map<String, Object> parameter = new HashMap<>(5);
		parameter.put("count", count);
		parameter.put("tableName", tableName);
		parameter.put("crawlerType", Fields.STYPE_1);
		parameter.put("pageTop", pageTop);
		parameter.put("platform", listjobName.get(0).getPlatform());
		parameter.put("time", SimpleDate.SimpleDateFormatData().format(new Date()));
		//从数据库获取城市code抓取库存
		List<Craw_keywords_delivery_place> List_Code = new ArrayList<>();
		int datCount =12;
		try {
			List_Code = crawlerPublicClassService.Keywords_Delivery_Place(listjobName, listjobName.get(0).getDatabases());
		} catch (Exception e) {
			XxlJobLogger.log("==>>获取商品库存地址发生异常：<<==" + e);
		}
		if (List_Code != null && List_Code.size() > 0) {
			for (Craw_keywords_delivery_place delivery : List_Code) {
				XxlJobLogger.log("==>>当前商品抓取地区是" + delivery.getDelivery_place_name() + "<<==");
				if (listjobName.size() > 0) {
					parameter.put("listCode", delivery);
					for (int k = sum; k < datCount; k++) {
						/*开始抓取数据*/
						int countData = crawlerCommodityAppDetails(listjobName, parameter, k, mapParam);
						if (countData == 0) {
							break;
						}

					}
				} else {
					XxlJobLogger.log("==>>当前用户" + listjobName.get(0).getPlatform() + "不存在请检查爬虫状态是否关闭！爬虫结束<<==");
					return;
				}
			}
		} else {
			if (listjobName.size() > 0) {
				parameter.put("listCode", new Craw_keywords_delivery_place());
				for (int k = sum; k < datCount; k++) {
					int countData = crawlerCommodityAppDetails(listjobName, parameter, k, mapParam);
					if (countData == 0) {
						break;
					}
				}
			} else {
				XxlJobLogger.log("==>>当前用户" + listjobName.get(0).getPlatform() + "不存在请检查爬虫状态是否关闭！爬虫结束<<==");
				return;
			}
		}


	}

	/* 解析数据*/
	public  int crawlerCommodityAppDetails(List<QrtzCrawlerTable> listjobName, Map<String, Object> parameter, int requestCount, Map<String, String> mapParam) throws IOException {
		int thread=1;
		if(mapParam.containsKey("thread")) {
			thread=Integer.valueOf(mapParam.get("thread"));
		}
		ExecutorService executorService = Executors.newFixedThreadPool(thread);
		ExecutorCompletionService<Map<String, Object>> completion = new ExecutorCompletionService<>(executorService);
		String time = parameter.get("time").toString();
		String count = parameter.get("count").toString();
		String tableName = parameter.get("tableName").toString();
		int pageTop = Integer.valueOf(parameter.get("pageTop").toString());
		Craw_keywords_delivery_place listCode=(Craw_keywords_delivery_place)parameter.get("listCode");
		List<CrawKeywordsInfo> list = new ArrayList<>();

		WebDriver driver = getDriver(mapParam);

		if (requestCount == 1) {

			list = crawlerPublicClassService.crawAndParseInfo(listjobName, listjobName.get(0).getDatabases(), tableName, count, pageTop);
			XxlJobLogger.log("==>>当前一共要抓取:"+list.size()+"个商品<<==");
		} else {
			list = crawlerPublicClassService.crawAndGrabFailureGoodsData(listjobName, listjobName.get(0).getDatabases(), tableName, pageTop);
			XxlJobLogger.log("==>>当前开始补抓失败的商品一共:"+list.size()+"个商品<<==");
		}
		//		for (int i = 0; i < list.size(); i++) {
		//hpkDataCrawlService.chromeDataTest(list,mapParam);
		//		}

		for (int i = 0; i < list.size(); i++) {
			try {
				Map<String, Object> data = new HashMap<String, Object>();
				Hpk_DataCrawlTask dataCrawlTask = (Hpk_DataCrawlTask) contexts.getBean(Hpk_DataCrawlTask.class);
				Parameter param = new Parameter();

				Matcher matcher = Pattern.compile("[\u4e00-\u9fa5]").matcher(list.get(i).getCust_keyword_name());
				data.put(Fields.CRAWLER_DRIVER, driver);
				data.put("crawlerStore",mapParam.get("crawlerStore"));
				data.put("productdesc",mapParam.get("productdesc"));
				
				if (matcher.find()) {
					continue;
				}
				if (StringUtils.isNoneEmpty(list.get(0).getBatch_time())) {
					time = list.get(i).getBatch_time();
				}
				param.setMap(data);
				param.setBatch_time(time);
				param.setCoding(Fields.UTF8);
				param.setPage(Fields.STATUS_OFF);
				param.setListjobName(listjobName);
				param.setDeliveryPlace(listCode);
				param.setCrawKeywordsInfo(list.get(i));
				param.setSum("" + (i + 1) + "/" + list.size() + "");
				param.setType(parameter.get("crawlerType").toString());
				param.setInclude(list.get(i).getCust_keyword_name());
				param.setKeywordId(list.get(i).getCust_keyword_id());
				param.setEgoodsId(list.get(i).getCust_keyword_name());
				param.setItemUtl_2(list.get(i).getCust_keyword_url());
				param.setTimeDate(SimpleDate.SimpleDateFormatData().format(new Date()));
				param.setGoodsId(StringHelper.encryptByString(list.get(i).getCust_keyword_name() + listjobName.get(0).getPlatform()));
				dataCrawlTask.setParameter(param);
				completion.submit(dataCrawlTask);
				XxlJobLogger.log("线程==>>"+Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors()+completion.hashCode()+",Thread"+Thread.currentThread().getId());
				
			} catch (BeansException e) {
				XxlJobLogger.log("==>>" + listjobName.get(0).getPlatform() + " 多线程请求数据发生异常：" + e + "<<==");
				e.printStackTrace();
			}
		}
		executorService.shutdown();
		while (true) {
			if (executorService.isTerminated()) {
				break;
			}
		}
		return list.size();
	}



	/**
	 * @描述 登录网站
	 * @参数
	 * @返回值 ChromeDriver
	 * @创建人 jack.zhao
	 * @创建时间 2020/1/16
	 */
	public WebDriver getDriver(Map<String, String> map) throws IOException {
		//WebDriver driver = new ChromeDriver();
		WebDriver driver = WebDriverUtil.getWebDriver();
		//WebDriver driver = WebDriverUtil.webDriverFirefox();
		File directory = new File(".");
		String imageUrl = directory.getCanonicalPath();
		XxlJobLogger.log("==>>开始登陆海拍客网站<<==");
		for (int i = 0; i < 15; i++) {
			try {
				driver.get("https://mall.hipac.cn/mall/view/login/login.html?loginFlag=false&action=login");
				String itemData = driver.getPageSource();
				Document dataMessage = Jsoup.parse(itemData);
				String url = "http:" + dataMessage.getElementById("loginMain").getElementsByTag("iframe").attr("src").toString();
				driver.get(url);
				Thread.sleep(3000);
				driver.findElement(By.xpath(".//*[@id='username']")).clear();
				driver.findElement(By.id("username")).sendKeys(map.get("userName"));
				driver.findElement(By.xpath(".//*[@id='password']")).clear();
				driver.findElement(By.xpath(".//*[@id='password']")).sendKeys(map.get("password"));
				driver.findElement(By.xpath(".//*[@id='kaptcha']")).clear();
				captureElement(driver);
//				String result = ChaoJiYing.PostPic(imageUrl + "\\code.png");
////				JSONObject currPriceJson = JSONObject.fromObject(result);
//				result = currPriceJson.getString("pic_str");
//				Thread.sleep(10000);
//				driver.findElement(By.xpath(".//*[@id='kaptcha']")).sendKeys(result);
				driver.findElement(By.className("login-btn")).click();
				Thread.sleep(5000);
				driver.get("https://mall.hipac.cn/mall/view/goods/itemIndex.html?t=1579160110239");
				String item = driver.getPageSource();
				XxlJobLogger.log("==>>当前是第："+(i+1)+"次 请求数据<<==");

				System.out.println(item.contains("巨划算"));
				if (item.contains("巨划算")) {
					return driver;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return driver;
	}

	public  void captureElement(WebDriver driver) throws IOException {
		WebElement verifyCodeElement = null;
		File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		BufferedImage img = ImageIO.read(screen);
		verifyCodeElement = driver.findElement(By.xpath("//img[@class='small-btn f-r']"));
		Point p = verifyCodeElement.getLocation();
		// 创建一个矩形使用上面的高度，和宽度
		Rectangle rect = new Rectangle(p, verifyCodeElement.getSize());
		// 得到元素的坐标
		BufferedImage dest = img.getSubimage(p.getX(), p.getY(), rect.width, rect.height);
		//存为png格式
		ImageIO.write(dest, "png", new File("code.png"));
	}
}
