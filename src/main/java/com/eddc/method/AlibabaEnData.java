/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2018年3月14日 下午12:20:51 
 */
package com.eddc.method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.job.JobAndTriggerService;
import com.eddc.task.Alibabaen_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年3月19日 下午12:20:51   
 * 修改人：jack.zhao   
 * 修改时间：2018年3月19日 下午12:20:51   
 * 修改备注：   
 * @version    
 *    
 */
@Component
public class AlibabaEnData { 
	private static Logger logger = LoggerFactory.getLogger(AlibabaEnData.class);
	@Autowired
	private ApplicationContext contexts= SpringContextUtil.getApplicationContext();
	@SuppressWarnings("unused")
	@Autowired
	private JobAndTriggerService jobAndTriggerService ;
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	//启动爬虫 
	public void alibabaenCrawler(List<QrtzCrawlerTable>listjobName,String tableName,String count,int pageTop) throws InterruptedException{
		String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
		ApplicationContext context= SpringContextUtil.getApplicationContext();
		jobAndTriggerService = (JobAndTriggerService) context.getBean(JobAndTriggerService.class);
		crawlerPublicClassService = (CrawlerPublicClassService) context.getBean(CrawlerPublicClassService.class);
		//从数据库获取城市code抓取库存
		List<Craw_keywords_delivery_place>List_Code=crawlerPublicClassService.Keywords_Delivery_Place(listjobName,listjobName.get(0).getDatabases());
		if(listjobName.size()>0){
			if(List_Code.size()>0){
				for(Craw_keywords_delivery_place place:List_Code){
					//crawlerPublicClassService.SearchSynchronousDataAlibaba(Fields.TABLE_CRAW_VENDOR_INFO,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_VENDOR_PRICE_INFO,null); 
					crawAndParseInfoAndPriceAlibabaEn(listjobName,time,place.getDelivery_place_code(),tableName,count,pageTop);//启动爬虫		
				}
			}else{
				//crawlerPublicClassService.SearchSynchronousDataAlibaba(Fields.TABLE_CRAW_VENDOR_INFO,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_VENDOR_PRICE_INFO,null); 
				crawAndParseInfoAndPriceAlibabaEn(listjobName,time,null,tableName,count,pageTop);//启动爬虫		

			}	
		}else {
			logger.info("【当前用户不存在！爬虫结束------】"+new Date());		
		}
	}
	//商品详情
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void crawAndParseInfoAndPriceAlibabaEn (List<QrtzCrawlerTable>listjobName,String time,String code,String tableName, String count,int pageTop) throws InterruptedException{
		int jj=0;
		long awaitTime=Integer.valueOf(listjobName.get(0).getDocker())*1000*480;  
		ArrayList<Future> futureList = new ArrayList<Future>(); 
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndParseInfo(listjobName,listjobName.get(0).getDatabases(),tableName,count, pageTop);//开始解析数据 
		for(CrawKeywordsInfo accountInfo:list){ jj++;
		Alibabaen_DataCrawlTask dataCrawlTask = (Alibabaen_DataCrawlTask) contexts.getBean(Alibabaen_DataCrawlTask.class);
		Map<String,Object>alibabaMap=new HashMap<String,Object>();
		dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
		dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
		dataCrawlTask.setStorage(listjobName.get(0).getStorage());
		dataCrawlTask.setTimeDate(time);
		dataCrawlTask.setSum(""+jj+"/"+list.size()+"");
		dataCrawlTask.setType(Fields.STYPE_1); 
		dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id()); 
		dataCrawlTask.setCrawKeywordsInfo(accountInfo);
		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
		dataCrawlTask.setCodeCookice(code);
		dataCrawlTask.setIp(listjobName.get(0).getIP());
		dataCrawlTask.setAlibabaMap(alibabaMap);
		dataCrawlTask.setSetUrl(accountInfo.getCust_keyword_url());
		//dataCrawlTask.setSetUrl(Fields.PLATFORM_1688_PC_URL.replace("EGOODSID", accountInfo.getCust_keyword_name()));
		Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		futureList.add(future);
		logger.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		}
		taskExecutor.shutdown();
		try {
			if(pageTop==Fields.STATUS_COUNT){
				while(true){
					if(((ExecutorService) taskExecutor).isTerminated()){
						taskExecutor.shutdownNow();  
						if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
							crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
						}
						break;	
					}
				}
			}else{
				if(!taskExecutor.awaitTermination(awaitTime, TimeUnit.MILLISECONDS)){ 
					taskExecutor.shutdownNow();  // 超时的时候向线程池中所有的线程发出中断(interrupted)。  
				}
			} 
		} catch (Exception e) {
			// 超时的时候向线程池中所有的线程发出中断(interrupted)。  
			taskExecutor.shutdownNow();  
		}finally{
			if(pageTop==Fields.STATUS_COUNT_1){
				crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				crawlerPublicClassService.updateCrawKeywordHistory(listjobName,listjobName.get(0).getDatabases());//更新已经抓取商品的状态
			 }
		 } 
	  }
	}
