package com.eddc.method;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Parameter;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.task.XohDataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.StringHelper;
import com.xxl.job.core.log.XxlJobLogger;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：XohData   
 * @author：jack.zhao       
 * 创建时间：2019年2月22日 上午10:16:25   
 * 类描述：   
 * @version    
 *    
 */
@Component
public class XohData {
	private static Logger logger = LoggerFactory.getLogger(XohData.class);
	@Autowired
	private ApplicationContext contexts= SpringContextUtil.getApplicationContext();
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	public int crawlerCommodityAppDetails(List<QrtzCrawlerTable> listjobName,String time) throws Exception {
		ExecutorService executorService = Executors.newFixedThreadPool(Fields.COUNT_30);
		ExecutorCompletionService<Map<String, Object>> completion = new ExecutorCompletionService<> (executorService);
		List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndParseInfo(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_ON,Fields.STATUS_COUNT);
		for(int i=0;i<list.size();i++){
			//if(list.get(i).getCust_keyword_name().equals("爱他美")) {
				
			Map<String,Object>data=new HashMap<String,Object>();
			XohDataCrawlTask dataCrawlTask = (XohDataCrawlTask) contexts.getBean(XohDataCrawlTask.class);
			String productName = URLEncoder.encode(list.get(i).getCust_keyword_name(), "UTF-8");
			String url="http://www.junyier.com/goodslist/cid/0/bid/0/trade/0,0,0/xl/0/fh/0,0/goods_name/"+productName+"/bs/1/p/1";
		    //String url = "http://www.xinouhui.com/goodslist/cid/0/bid/0/trade/0,0,0/xl/0/fh/0,0/bs/1/goods_name/"+productName+"/bs/1/p/";
			Parameter parameter=new Parameter();
			parameter.setMap(data);
			parameter.setItemUrl_1(url);
			parameter.setItemUtl_2(url);
			parameter.setEgoodsId(list.get(i).getCust_keyword_name());
			parameter.setTimeDate(SimpleDate.SimpleDateFormatData().format(new Date()));
			parameter.setBatch_time(time);
			parameter.setCoding(Fields.UTF8);
			parameter.setPage(Fields.STATUS_OFF);
			parameter.setListjobName(listjobName);
			parameter.setCrawKeywordsInfo(list.get(i));  
			parameter.setSum(""+(i+1)+"/"+list.size()+"");
			parameter.setType("1"); 
			parameter.setKeywordId(list.get(i).getCust_keyword_id());
			parameter.setGoodsId(StringHelper.encryptByString(list.get(i).getCust_keyword_name()+listjobName.get(0).getPlatform()));
			dataCrawlTask.setParameter(parameter);
			completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
			XxlJobLogger.log("线程==>>"+Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
			logger.info("线程==>>"+Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		//}
	}
		executorService.shutdown();
		while(true){
			if (executorService.isTerminated ( )) {
				break;
			}	
		}
		return list.size(); 	
	}
}

