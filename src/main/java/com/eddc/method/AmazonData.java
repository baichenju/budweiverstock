/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.method 
 * @author: jack.zhao   
 * @date: 2018年10月8日 下午4:37:15 
 */
package com.eddc.method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.task.Amazon_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：AmazonData   
 * @author：jack.zhao       
 * 创建时间：2018年10月8日 下午4:37:15   
 * 类描述：   
 * @version    
 *    
 */
@Component
public class AmazonData {
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private ApplicationContext contexts= SpringContextUtil.getApplicationContext();
	private static Logger logger = LoggerFactory.getLogger(AmazonData.class);
	public void atartCrawlerAmazon(List<QrtzCrawlerTable>listjobName,String tableName,String count,int pageTop) throws InterruptedException{
		String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
		//从数据库获取城市code抓取库存
		List<Craw_keywords_delivery_place>listCode=crawlerPublicClassService.Keywords_Delivery_Place(listjobName,listjobName.get(0).getDatabases());
		if(listjobName.size()>0){
			if(listCode.size()>0){
				for(Craw_keywords_delivery_place place:listCode){
					if(pageTop==Fields.STATUS_COUNT_1){
						crawlerPublicClassService.updateCrawKeywordHistory(listjobName,listjobName.get(0).getDatabases());//更新已经抓取商品的状态
					}
					crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),tableName);//同步数据
					crawAndParseInfoAndPriceAmazoncn(listjobName,time,place.getDelivery_place_code(),tableName,count,pageTop);//启动爬虫		
				}
			}else{
				if(pageTop==Fields.STATUS_COUNT_1){
					crawlerPublicClassService.updateCrawKeywordHistory(listjobName,listjobName.get(0).getDatabases());//更新已经抓取商品的状态
				}
				crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),tableName);//同步数据
				crawAndParseInfoAndPriceAmazoncn(listjobName,time,null,tableName,count,pageTop);//启动爬虫		

			}	
		}else {
			logger.info("【当前用户不存在！爬虫结束------】"+new Date());		
		}
	}
	//商品详情
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void crawAndParseInfoAndPriceAmazoncn (List<QrtzCrawlerTable>listjobName,String time,String code,String tableName, String count,int pageTop) throws InterruptedException{
		int jj=0;long awaitTime=Integer.valueOf(listjobName.get(0).getDocker())*1000*60;  
		ArrayList<Future> futureList = new ArrayList<Future>();
		ThreadPoolExecutor taskExecutor = new ThreadPoolExecutor(Fields.COUNT_30, listjobName.get(0).getThread_sum(),Fields.SECOND, TimeUnit.SECONDS, new LinkedBlockingDeque<>(2000));
		ExecutorCompletionService completion=new ExecutorCompletionService(taskExecutor);
		List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndParseInfo(listjobName,listjobName.get(0).getDatabases(),tableName,count, pageTop);//开始解析数据 
		for(CrawKeywordsInfo accountInfo:list){ jj++;
		synchronized(this) {
		Amazon_DataCrawlTask dataCrawlTask = (Amazon_DataCrawlTask) contexts.getBean(Amazon_DataCrawlTask.class);
		Map<String,Object>amazon=new HashMap<String,Object>();
		dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
		dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
		dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id()); 
		dataCrawlTask.setStorage(listjobName.get(0).getStorage());
		dataCrawlTask.setTimeDate(time);
		dataCrawlTask.setSum(""+jj+"/"+list.size()+"");
		dataCrawlTask.setType(Fields.STYPE_1); 
		dataCrawlTask.setCrawKeywordsInfo(accountInfo);
		dataCrawlTask.setCodeCookice(code);
		dataCrawlTask.setIp(listjobName.get(0).getIP());
		dataCrawlTask.setPage(Fields.STATUS_ON);
		dataCrawlTask.setAmazon(amazon);
		dataCrawlTask.setStatus_type(Fields.STATUS_OFF_3);
		if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_AMAZON_UN)){
			dataCrawlTask.setSetUrl(Fields.PEPSI_AMAZON_EN_URL+accountInfo.getCust_keyword_name());	
		}else if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_AMAZON)){
			dataCrawlTask.setSetUrl(Fields.PEPSI_AMAZON_URL+accountInfo.getCust_keyword_name());
		}
		Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		futureList.add(future);
		logger.info("线程==>>"+Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors()+completion.hashCode()+",Thread"+Thread.currentThread().getId());
		 }
	}
		taskExecutor.shutdown();
		try {
			if(pageTop==Fields.STATUS_COUNT){
				while(true){ 
					if(((ExecutorService) taskExecutor).isTerminated()){
						taskExecutor.shutdownNow();  
						if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
							crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
						}
						Thread.sleep(9000);
						logger.info("信息爬虫结束开始检查是否有漏抓数据-----"+SimpleDate.SimpleDateFormatData().format(new Date()));
						crawAndParseInfoAndPricefailureAmazonecn(time,listjobName,tableName,code,pageTop);//检查是否有失败的商品
						break;
					}	
				}
			}else{
				if(!taskExecutor.awaitTermination(awaitTime, TimeUnit.MILLISECONDS)){ 
					taskExecutor.shutdownNow();  // 超时的时候向线程池中所有的线程发出中断(interrupted)。  
				}
			}
		} catch (Exception e) {
			taskExecutor.shutdownNow();  // 超时的时候向线程池中所有的线程发出中断(interrupted)。  
		}finally{
			if(pageTop==Fields.STATUS_COUNT_1){
				crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				crawlerPublicClassService.updateCrawKeywordHistory(listjobName,listjobName.get(0).getDatabases());//更新已经抓取商品的状态
			}
		}
	}
	///检查失败的item数据
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void crawAndParseInfoAndPricefailureAmazonecn(String time,List<QrtzCrawlerTable>listjobName,String tableName,String code,int pageTop) throws InterruptedException{
		int jj=0; int count=0;
		ArrayList<Future> futureList = new ArrayList<Future>();
		ThreadPoolExecutor taskExecutor = new ThreadPoolExecutor(Fields.COUNT_30, listjobName.get(0).getThread_sum(),Fields.SECOND, TimeUnit.SECONDS, new LinkedBlockingDeque<>(200));
		ExecutorCompletionService completion=new ExecutorCompletionService(taskExecutor);
		do{  
			List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndGrabFailureGoodsData(listjobName,listjobName.get(0).getDatabases(),tableName,pageTop);//开始解析数据 
			for(CrawKeywordsInfo accountInfo:list){ jj++;	
			synchronized(this) {
			Amazon_DataCrawlTask dataCrawlTask = (Amazon_DataCrawlTask) contexts.getBean(Amazon_DataCrawlTask.class);
			Map<String,Object>amazon=new HashMap<String,Object>();
			dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
			dataCrawlTask.setStorage(listjobName.get(0).getStorage());
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id()); 
			dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
			dataCrawlTask.setTimeDate(time);
			dataCrawlTask.setSum(""+jj+"/"+list.size()+"");
			dataCrawlTask.setType(Fields.STYPE_1); 
			dataCrawlTask.setCrawKeywordsInfo(accountInfo);
			dataCrawlTask.setCodeCookice(code);
			dataCrawlTask.setPage(Fields.STATUS_ON);
			dataCrawlTask.setIp(listjobName.get(0).getIP());
			dataCrawlTask.setStatus_type(Fields.STATUS_OFF_3);
			dataCrawlTask.setAmazon(amazon);
			dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
			if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_AMAZON_UN)){
				dataCrawlTask.setSetUrl(Fields.PEPSI_AMAZON_EN_URL+accountInfo.getCust_keyword_name());	
			}else if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_AMAZON)){
				dataCrawlTask.setSetUrl(Fields.PEPSI_AMAZON_URL+accountInfo.getCust_keyword_name());
			}
			Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
			futureList.add(future);
			logger.info("线程==>>"+Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors()+completion.hashCode()+",Thread"+Thread.currentThread().getId());
			  }
			}
			taskExecutor.shutdown();
			while(true){
				if(((ExecutorService) taskExecutor).isTerminated()){
					if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
						crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
					}
					break;
				}
			}
			jj=0;
			count++;
			Thread.sleep(5000); 
			if (count > 5) {
				Thread.sleep(5000); 
				return;
			}
		}while (true);
	} 
}
