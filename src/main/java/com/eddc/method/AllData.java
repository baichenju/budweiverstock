/**   
 * Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2019年11月14日 上午10:46:10 
 */
package com.eddc.method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.Parameter;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.crawl.SearchDataCrawlService;
import com.eddc.task.AllDataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.StringHelper;
import com.google.common.collect.Lists;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Component
public class AllData {
	@Autowired
	private ApplicationContext contexts= SpringContextUtil.getApplicationContext();
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	SearchDataCrawlService search_DataCrawlService;
	/*抓取多个地区的商品库存*/

	public void regionCrawler(List<QrtzCrawlerTable> listjobName, String tableName, String count,int pageTop,int sum,int crawlerThread) {
		String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
		Map<String,Object>parameter=new HashMap<>(5);
		SimpleDateFormat timeHour = new SimpleDateFormat("H");
		String hour=timeHour.format(new Date());
		parameter.put("hour", hour);
		parameter.put("week", SimpleDate.StringWeekday());
		parameter.put("time", time);
		parameter.put("count", count);
		parameter.put("tableName", tableName);
		parameter.put("crawlerType", Fields.STYPE_1);
		parameter.put("pageTop", pageTop);
		parameter.put("platform", listjobName.get(0).getPlatform());
		parameter.put("crawlerThread",crawlerThread);

		//从数据库获取城市code抓取库存
		List<Craw_keywords_delivery_place> List_Code=new ArrayList<>();
		int datCount=listjobName.get(0).getSum_request_num();
		try {
			List_Code = crawlerPublicClassService.Keywords_Delivery_Place(listjobName,listjobName.get(0).getDatabases());
		} catch (Exception e) {
			log.error("==>>获取商品库存地址发生异常：<<=="+e);
			XxlJobLogger.log("==>>获取商品库存地址发生异常：<<=="+e);
		}
		if(listjobName.get(0).getPlatform().equalsIgnoreCase("pdd")){
			datCount=2;
		}
		if(List_Code!=null && List_Code.size()>0){
			for(Craw_keywords_delivery_place delivery:List_Code){
				XxlJobLogger.log(  "==>>当前商品抓取地区是"+delivery.getDelivery_place_name()+"<<==");
				log.info("==>>当前商品抓取地区是"+delivery.getDelivery_place_name()+"<<==");
				if(listjobName.size()>0){
					parameter.put("listCode", delivery);
					for(int k=sum;k<datCount;k++){
						int countData=crawlerCommodityAppDetails(listjobName,parameter,k);
						if(countData==0){
							break;
						}

					}
					if(listjobName.get(0).getClient().equalsIgnoreCase("ALL")){
						if(!listjobName.get(0).getPlatform().equals("gome")&&!listjobName.get(0).getPlatform().equals("tmall")&& !listjobName.get(0).getPlatform().equals("suning") && !listjobName.get(0).getPlatform().equals("jd")){
							/*PC端商品价格*/
							for(int k=sum;k<datCount;k++){
								int countData=crawlerCommodityPcDetails(listjobName,parameter,k);
								if(countData==0){
									break;
								}

							}		
						}

					}
				}else{
					log.info("==>>当前用户"+listjobName.get(0).getPlatform()+"不存在请检查爬虫状态是否关闭！爬虫结束<<==");
					return ;
				}
			}
		}else{
			if(listjobName.size()>0){
				parameter.put("listCode", new Craw_keywords_delivery_place());
				for(int k=sum;k<=datCount;k++){
					int countData=crawlerCommodityAppDetails(listjobName,parameter,k); 
					if(countData==0){
						break;
					}
				}
				if(listjobName.get(0).getClient().equalsIgnoreCase("ALL")){
					if(!listjobName.get(0).getPlatform().equals("gome")&&!listjobName.get(0).getPlatform().equals("tmall")&& !listjobName.get(0).getPlatform().equals("suning")&& !listjobName.get(0).getPlatform().equals("jd")){
						/*PC端商品价格*/
						for(int k=sum;k<datCount;k++){
							int countData=crawlerCommodityPcDetails(listjobName,parameter,k);
							if(countData==0){
								break;
							}

						}		
					}

				}
			}else{
				log.info("==>>当前用户"+listjobName.get(0).getPlatform()+"不存在请检查爬虫状态是否关闭！爬虫结束<<==");
				return;
			}
		}



	}
	/* 解析手机数据*/
	@SuppressWarnings("unchecked")
	public <newFixedThreadPool> int crawlerCommodityAppDetails(List<QrtzCrawlerTable> listjobName, Map<String, Object> parameter,int requestCount) {

		String hour=parameter.get("hour").toString();
		String week=parameter.get("week").toString();
		String time=parameter.get("time").toString();
		String count=parameter.get("count").toString();
		String tableName=parameter.get("tableName").toString();
		int pageTop=Integer.valueOf(parameter.get("pageTop").toString());
		String crawlerThread=parameter.get("crawlerThread").toString();
		Craw_keywords_delivery_place listCode=(Craw_keywords_delivery_place)parameter.get("listCode");
		List<CrawKeywordsInfo>list=new ArrayList<>();
		int product=listjobName.get(0).getSums();
		int sumSize=0;
		if(requestCount==1){
			//开始解析数据
			list = crawlerPublicClassService.crawAndParseInfo(listjobName, listjobName.get(0).getDatabases(), tableName, count, pageTop);
			XxlJobLogger.log("==>>当前一共要抓取:" + list.size() + "个商品<<==");
		}else {
			//开始解析数据
			list = crawlerPublicClassService.crawAndGrabFailureGoodsData(listjobName, listjobName.get(0).getDatabases(), tableName, pageTop);
			XxlJobLogger.log("==>>当前开始补抓失败的商品一共:" + list.size() + "个商品<<==");
		}
		List<List<CrawKeywordsInfo>> orders_partitions = Lists.partition(list, product);
		XxlJobLogger.log("==>>当前数据抓取一共分:" + orders_partitions.size() + "个批次数据请求<<==");
		for(int k=0;k<orders_partitions.size();k++) {
			ExecutorService executorService = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());
			ExecutorCompletionService<Map<String, Object>> completion = new ExecutorCompletionService<> (executorService);
			ArrayList <Future<Map<String, Object>>> futureList = new ArrayList<> ();
			List<CrawKeywordsInfo>productList=orders_partitions.get(k);
			XxlJobLogger.log("==>>当前是第"+(k+1)+"个批次数据请求<<==");
			for(int i=0;i<productList.size();i++){

				try {
					Map<String,Object>data=new HashMap<String,Object>();
					AllDataCrawlTask dataCrawlTask = (AllDataCrawlTask) contexts.getBean(AllDataCrawlTask.class);
					Parameter param=new Parameter();
					Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
					Matcher m = p.matcher(productList.get(i).getCust_keyword_name());
					data.put("hour", hour);
					data.put("week", week);
					if (m.find()) {
						continue;
					}
					if(StringUtils.isNoneEmpty(productList.get(0).getBatch_time())){
						time=productList.get(i).getBatch_time();
					}
					param.setMap(data);
					param.setBatch_time(time);
					param.setCoding(Fields.UTF8);
					param.setPage(Fields.STATUS_OFF);
					param.setListjobName(listjobName);
					param.setDeliveryPlace(listCode);
					param.setCrawKeywordsInfo(productList.get(i));  
					param.setType(parameter.get("crawlerType").toString()); 
					param.setInclude(productList.get(i).getCust_keyword_name());
					param.setKeywordId(productList.get(i).getCust_keyword_id());
					param.setEgoodsId(productList.get(i).getCust_keyword_name());
					param.setItemUtl_2(productList.get(i).getCust_keyword_url());
					param.setTimeDate(SimpleDate.SimpleDateFormatData().format(new Date()));
					param.setSum("当前是第"+(k+1)+"个批次数据请求，  "+(i+1)*(k+1)+"/"+list.size()+" ,"+week +","+ hour);
					param.setGoodsId(StringHelper.encryptByString(productList.get(i).getCust_keyword_name()+listjobName.get(0).getPlatform()));
					dataCrawlTask.setParameter(param);

					if(!listjobName.get(0).getStorage().equalsIgnoreCase("4")) {
						//线程执行完成以后可以通过引用获取返回值
						Future<Map<String, Object>> future = completion.submit(dataCrawlTask);
						futureList.add(future);
					}else {
						completion.submit(dataCrawlTask);
					}
					//XxlJobLogger.log(param.getSum()+"线程==>>"+Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors()+",ThreadCode:"+completion.hashCode()+",Thread:"+Thread.currentThread().getId());
				} catch (BeansException e) {
					XxlJobLogger.log("==>>"+listjobName.get(0).getPlatform()+" 多线程请求数据发生异常："+e+"<<==");
					log.error("==>>"+listjobName.get(0).getPlatform()+" 多线程请求数据发生异常："+e+"<<==");
					e.printStackTrace();
				} 
			}
			//判断数据是否批量插入数据
			if(!listjobName.get(0).getStorage().equalsIgnoreCase("4")) {
				log.info("==>>开始批量插入数据"+listjobName.get(0).getJob_name()+"<<==");
				if(futureList!=null && futureList.size()>0){
					List<Map<String,Object>>item=new ArrayList<Map<String, Object>>();
					List<Map<String,Object>>price=new ArrayList<Map<String, Object>>();
					List<Map<String,Object>>attributes=new ArrayList<Map<String, Object>>();
					List<Map<String,Object>>productComment=new ArrayList<Map<String, Object>>();
					List<Map<String,Object>>productimageList=new ArrayList<Map<String, Object>>();

					for(int i = 0;i<futureList.size();i++){
						Future<Map<String, Object>> futures=futureList.get(i);
						try {

							//判断商品详情信息是否存在
							if(futures.get().containsKey("itemList")) {
								List<Map<String,Object>>itemPriceList= (List<Map<String, Object>>)futures.get().get("itemPriceList");
								List<Map<String,Object>>itemList=(List<Map<String, Object>>) futures.get().get("itemList");
								item.addAll(itemList);
								price.addAll(itemPriceList);
							}
							//判断商品评论打分信息是否存在
							if(futures.get().containsKey("commentList")) {
								List<Map<String,Object>>commentList=(List<Map<String, Object>>) futures.get().get("commentList");
								productComment.addAll(commentList);
							}

							//判断商品规格属性是否存在
							if(futures.get().containsKey("attributesList")) {
								List<Map<String,Object>>attributesList=(List<Map<String, Object>>) futures.get().get("attributesList");
								attributes.addAll(attributesList);
							}
							//判断商品是否抓取了小图片
							if(futures.get().containsKey("imageList")) {
								List<Map<String,Object>>attributesList=(List<Map<String, Object>>) futures.get().get("imageList");
								productimageList.addAll(attributesList);
							}


						} catch (Exception e) {
							XxlJobLogger.log("==>>批量插入数据发送异常:"+e+"<<==");
							e.printStackTrace();
						}
					}	
					//数据批量插入
					if(price!=null && price.size()>0) {
						XxlJobLogger.log("==>>当前是第"+(k+1)+"/"+orders_partitions.size()+" 个批次数据开始插入数据库,一次插入"+product+" 条数据<<==");

						String database=listjobName.get(0).getDatabases();
						//商品详情
						XxlJobLogger.log("==>>当前 详情 ：是第"+(k+1)+"/"+orders_partitions.size()+" 个批次数据开始插入数据库<<=="+item.size());
						search_DataCrawlService.insertIntoData(item,database, Fields.TABLE_CRAW_GOODS_INFO);
						//商品价格
						XxlJobLogger.log("==>>当前 价格 ：是第"+(k+1)+"/"+orders_partitions.size()+" 个批次数据开始插入数据库<<=="+price.size());
						search_DataCrawlService.insertIntoData(price, database, Fields.TABLE_CRAW_GOODS_PRICE_INFO);

						//判断商品属性是否存在
						if(attributes!=null && attributes.size()>0) {
							XxlJobLogger.log("==>>当前 商品属性 ：是第"+(k+1)+"/"+orders_partitions.size()+" 个批次数据开始插入数据库<<=="+attributes.size());
							search_DataCrawlService.insertIntoData(attributes, database, Fields.CRAW_GOODS_ATTRIBUTE_INFO);
						}
						//获取商品小图片
						if(productimageList!=null && productimageList.size()>0) {
							XxlJobLogger.log("==>>当前 小图片 ：是第"+(k+1)+"/"+orders_partitions.size()+" 个批次数据开始插入数据库<<=="+productimageList.size());
							search_DataCrawlService.insertIntoData(productimageList, database, Fields.TABLE_CRAW_GOODS_PIC_INFO);
						}
						//商品评分
						if(productComment!=null && productComment.size()>0) {
							XxlJobLogger.log("==>>当前 商品评分 ：是第"+(k+1)+"/"+orders_partitions.size()+" 个批次数据开始插入数据库<<=="+productComment.size());
							search_DataCrawlService.insertIntoData(productComment, database, Fields.CRAW_GOODS_COMMENT_INFO);

						}

						//释放内存
						System.gc();
					}
				}
				sumSize=list.size();
			}

			executorService.shutdown();
			if(crawlerThread.equalsIgnoreCase("1")) {
				while(true){
					if (executorService.isTerminated ( )) {
						System.gc();
						break;
					}	
				}	
				sumSize=list.size();
			}

		}

		return sumSize; 
	}

	/**
	 * 解析PC数据
	 */
	public int crawlerCommodityPcDetails(List<QrtzCrawlerTable> listjobName, Map<String, Object> parameter, int requestCount) {

		ExecutorService executorService = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());
		ExecutorCompletionService<Map<String, Object>> completion = new ExecutorCompletionService<> (executorService);
		String time=parameter.get("time").toString();
		String tableName=parameter.get("tableName").toString();
		String hour=parameter.get("hour").toString();
		String week=parameter.get("week").toString();
		int pageTop=Integer.valueOf(parameter.get("pageTop").toString());
		String platform=listjobName.get(0).getPlatform();
		String database=listjobName.get(0).getDatabases();
		Craw_keywords_delivery_place listCode=(Craw_keywords_delivery_place)parameter.get("listCode");
		List<CommodityPrices>listPriceJd= new ArrayList<>();
		if(requestCount==1){
			listPriceJd=crawlerPublicClassService.CommodityPricesData(listjobName,database,tableName,pageTop);
			XxlJobLogger.log("==>>当前一共要抓取:"+listPriceJd.size()+"个商品<<==");
		}else{
			listPriceJd=crawlerPublicClassService.RecursionFailureGoods(listjobName,database,tableName,pageTop);
			XxlJobLogger.log("==>>当前开始补抓失败的商品一共:"+listPriceJd.size()+"个商品<<==");
		}
		for(int i=0;i<listPriceJd.size();i++){
			try {
				Map<String,Object>data=new HashMap<String,Object>();
				AllDataCrawlTask dataCrawlTask = (AllDataCrawlTask) contexts.getBean(AllDataCrawlTask.class);
				Parameter param=new Parameter();	
				data.put("hour", hour);
				data.put("week", week);
				param.setMap(data);
				param.setBatch_time(time);
				param.setCoding(Fields.UTF8);
				param.setPage(Fields.STATUS_OFF);
				param.setListjobName(listjobName);
				param.setSum((i+1)+"/"+listPriceJd.size()+"");
				param.setType(Fields.STYPE_6); 
				param.setDeliveryPlace(listCode);
				param.setCommodityPrices(listPriceJd.get(i));
				param.setInclude(listPriceJd.get(i).getEgoodsId());
				param.setKeywordId(listPriceJd.get(i).getCust_keyword_id());
				param.setEgoodsId(listPriceJd.get(i).getEgoodsId());
				param.setGoodsId(listPriceJd.get(i).getGoodsid());
				param.setTimeDate(SimpleDate.SimpleDateFormatData().format(new Date()));
				dataCrawlTask.setParameter(param);;
				completion.submit(dataCrawlTask);
				XxlJobLogger.log(param.getSum()+"线程==>>"+Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors()+",ThreadCode:"+completion.hashCode()+",Thread:"+Thread.currentThread().getId());
			} catch (BeansException e) {
				log.error("==>>"+platform+" 多线程请求数据发生异常："+e+"<<==");
				e.printStackTrace();
			}
		}
		executorService.shutdown();
		while(true){
			if (executorService.isTerminated ( )) {
				break;
			}	
		}
		return listPriceJd.size();
	}

}
