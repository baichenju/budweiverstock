/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.method 
 * @author: jack.zhao   
 * @date: 2018年9月12日 下午12:02:06 
 */
package com.eddc.method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_keywords_temp_Info_forsearch;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.crawl.SearchDataCrawlService;
import com.eddc.task.SearchDataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;

/**   
 *    
 * 项目名称：selection_pepsi_crawler   
 * 类名称：SearchData   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年9月12日 下午12:02:06   
 * 修改人：jack.zhao   
 * 修改时间：2018年9月12日 下午12:02:06   
 * 修改备注：   
 * @version    
 *    
 */
@Component
public class SearchData {
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private SearchDataCrawlService pepsi_DataCrawlService;
	/** 
	 * @Title: crawlerVipPriceMonitoring  
	 * @Description: TODO 开始解析数据
	 * @param @param listjobName
	 * @param @param message    设定文件  
	 * @return void    返回类型  
	 * @throws  
	 */  
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public int crawlerPriceMonitoring(String time,List<QrtzCrawlerTable>listjobName,String code,String place_name) throws InterruptedException, ExecutionException{
		int sum=0;   
		ExecutorService taskExecutor = new ThreadPoolExecutor(Fields.COUNT_30, listjobName.get(0).getThread_sum(),0L, TimeUnit.MILLISECONDS,new LinkedBlockingQueue<Runnable>());
		ExecutorCompletionService completion=new ExecutorCompletionService(taskExecutor);
		List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndParseInfo(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_ON,Fields.STATUS_COUNT);//开始解析数据 
		List<Map<String,Object>>lsitData=new ArrayList<Map<String,Object>>(); 
		for (CrawKeywordsInfo accountInfo : list) {
			if(accountInfo.getCust_keyword_type().equalsIgnoreCase(Fields.KETWORD)){
					sum=accountInfo.getCrawling_status();  
					SearchDataCrawlTask dataCrawlTask = (SearchDataCrawlTask) SpringContextUtil.getApplicationContext().getBean(SearchDataCrawlTask.class);
					dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
					dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
					dataCrawlTask.setTimeDate(time);
					dataCrawlTask.setCrawKeywordsInfo(accountInfo);
					dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
					dataCrawlTask.setType(Fields.STYPE_1); 
					dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
					dataCrawlTask.setSetUrl(accountInfo.getCust_keyword_url());	
					dataCrawlTask.setStatus(sum);
					dataCrawlTask.setIp(listjobName.get(0).getIP());
					dataCrawlTask.setCodeCookie(code);
					dataCrawlTask.setStatus_type(String.valueOf(listjobName.get(0).getPromotion_status()));
					completion.submit(dataCrawlTask);
			}
		}
		taskExecutor.shutdown();
		for(int i=0;i<list.size();i++){
			Map<String,Object>dataMap=(Map<String,Object>)completion.take().get();
			lsitData.add(dataMap);
		}
		return sum;
	}
	/**  
	 * @Title: crawlerVipPriceMonitoring  
	 * @Description: TODO 开始同步上egoodsId
	 * @param @param listjobName
	 * @throws  
	 */

	public void SynchronousData(List<QrtzCrawlerTable>listjobName,String count,int sum){ 
		if(count.equals(Fields.STATUS_ON)){
			pepsi_DataCrawlService.SynchronousData(listjobName,listjobName.get(0).getDatabases(),Craw_keywords_temp_Info_forsearch.class.getName().split("com.eddc.model.")[1],sum ); 
		}else{
			if(listjobName.get(0).getPlatform().contains(Fields.platform_1688)){
				crawlerPublicClassService.SearchSynchronousDataAlibaba(Fields.TABLE_CRAW_VENDOR_INFO,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_VENDOR_PRICE_INFO,null); 
			}else if(listjobName.get(0).getPlatform().contains(Fields.PLATFORM_JD_ZC) ||listjobName.get(0).getPlatform().contains(Fields.PLATFORM_TAOBAO_ZC)){
				crawlerPublicClassService.updateCrowdFunding(Fields.CRAW_GOODS_TRANSLATE_INFO_HISTORY,listjobName.get(0).getDatabases());//更新历史表状态
				//publicClassService.SearchSynchronousDataAlibaba(Fields.CRAW_GOODS_TRANSLATE_INFO,null);//同步数据  
			}else{
				crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),Fields.TABLE_CRAW_KEYWORDS_INF);//同步数据  
			}

		}
	}
}
