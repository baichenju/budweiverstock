/**
 * Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
 *
 * @Package: com.eddc.method
 * @author: jack.zhao
 * @date: 2019年10月16日 下午5:42:58
 */
package com.eddc.method;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Parameter;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.task.CommentParticiple_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.StringHelper;
import lombok.extern.slf4j.Slf4j;

/**
 * 项目名称：Price_monitoring_crawler
 * 类名称：CommentParticipleData
 *
 * @author：jack.zhao 创建时间：2019年10月16日 下午5:42:58
 * 类描述：
 */
@Component
@Slf4j
public class CommentParticipleData {
	@Autowired
	private ApplicationContext contexts = SpringContextUtil.getApplicationContext();
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;

	public void crawlerCommodityAppDetails(List<QrtzCrawlerTable> listJobName, String tableName, String count) {
		Map<String, Object> parameter = new HashMap<>(5);
		parameter.put("time", SimpleDate.SimpleDateFormatData().format(new Date()));
		parameter.put("count", count);
		parameter.put("tableName", tableName);
		parameter.put("crawlerType", Fields.STYPE_1);
		crawlerCommodity(listJobName, parameter); //开始抓取数据

	}

	public int crawlerCommodity(List<QrtzCrawlerTable> listjobName, Map<String, Object> parameterData) {
		ExecutorService executorService = Executors.newFixedThreadPool(Fields.COUNT_30);
		ExecutorCompletionService<Map<String, Object>> completion = new ExecutorCompletionService<>(executorService);

		String time = parameterData.get("time").toString();
		String count = parameterData.get("count").toString();
		String tableName = parameterData.get("tableName").toString();
		List<CrawKeywordsInfo> list = new ArrayList<>();
		list = crawlerPublicClassService.crawAndParseInfo(listjobName, listjobName.get(0).getDatabases(), tableName, count, Fields.STATUS_COUNT);
		for (int i = 0; i < list.size(); i++) {
			String url = Fields.JD_COMMENT_URL;
			String store = Fields.JD_STORE_URL;
			String ref = Fields.JD_URL_APP;

			if (listjobName.get(0).getPlatform().equalsIgnoreCase(Fields.PLATFORM_JD)) {
				if (StringUtils.isEmpty(list.get(i).getPlatform_shopid())) {
					continue;
				} else {
					url = Fields.JD_COMMENT_URL.replace(Fields.PRODUCTID, list.get(i).getCust_keyword_name());
					ref = Fields.JD_URL_APP.replace(Fields.PRODUCTID, list.get(i).getCust_keyword_name());
					store = Fields.JD_STORE_URL.replace(Fields.PLATFORM_SHOPID, list.get(i).getPlatform_shopid());
				}

			} else if (listjobName.get(0).getPlatform().equalsIgnoreCase(Fields.PLATFORM_SUNING)) {
				url = Fields.SUNING_COMMENT_URL.replace(Fields.PRODUCTID, list.get(i).getCust_keyword_name()).replace(Fields.PLATFORM_SHOPID, list.get(i).getPlatform_shopid());
				ref = url;
				if (StringUtils.isNoneEmpty(list.get(i).getPlatform_shopid())) {
					if (!StringUtils.equalsIgnoreCase("0000000000", list.get(i).getPlatform_shopid())) {	
						store = Fields.SUNING_STORE_URL.replace(Fields.PLATFORM_SHOPID, list.get(i).getPlatform_shopid().replaceFirst("^0*", ""));
					}else{
						store="";
					}
				}else {
					continue;
				}
			} else if(listjobName.get(0).getPlatform().equalsIgnoreCase(Fields.PLATFORM_TMALL_EN)){
				url = Fields.TMALL_COMMENT_URL.replace(Fields.PRODUCTID, list.get(i).getCust_keyword_name())+"&userNumId="+list.get(i).getPlatform_shopid();
				ref = Fields.TMALL_URL+list.get(i).getCust_keyword_name();

			}else if(listjobName.get(0).getPlatform().equalsIgnoreCase(Fields.PLATFORM_TAOBAO_EN)){
				url = Fields.TMALL_COMMENT_URL.replace(Fields.PRODUCTID, list.get(i).getCust_keyword_name())+"&userNumId="+list.get(i).getPlatform_shopid();
				ref = Fields.TAOBAO_URL+list.get(i).getCust_keyword_name();
			}

			if (StringUtils.isNoneEmpty(list.get(0).getBatch_time())) {
				time = list.get(i).getBatch_time();
			}
			if (!url.contains(Fields.PRODUCTID)) {
				Map<String, Object> data = new HashMap<String, Object>();
				CommentParticiple_DataCrawlTask dataCrawlTask = (CommentParticiple_DataCrawlTask) contexts.getBean(CommentParticiple_DataCrawlTask.class);
				Parameter parameter = new Parameter();
				parameter.setMap(data);
				parameter.setTimeDate(SimpleDate.SimpleDateFormatData().format(new Date()));

				parameter.setBatch_time(time);
				parameter.setItemUrl_1(url);
				parameter.setItemUtl_2(ref);
				parameter.setItemUtl_3(store);
				parameter.setCoding(Fields.UTF8);
				parameter.setPage(Fields.STATUS_OFF);
				parameter.setListjobName(listjobName);
				parameter.setCrawKeywordsInfo(list.get(i));
				parameter.setSum("" + (i + 1) + "/" + list.size() + "");
				parameter.setType(parameterData.get("crawlerType").toString());
				parameter.setInclude(list.get(i).getCust_keyword_name());
				parameter.setKeywordId(list.get(i).getCust_keyword_id());
				parameter.setEgoodsId(list.get(i).getCust_keyword_name());
				parameter.setGoodsId(StringHelper.encryptByString(list.get(i).getCust_keyword_name() + listjobName.get(0).getPlatform()));
				dataCrawlTask.setParameter(parameter);
				completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值
				log.info("线程==>>" + Thread.currentThread().getName() + Runtime.getRuntime().availableProcessors());
			}
		}
		executorService.shutdown();
		while (true) {
			if (executorService.isTerminated()) {
				break;
		  }
		}
		return list.size();
		
	}
}
