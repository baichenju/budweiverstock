/**   
 * Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.method 
 * @author: jack.zhao   
 * @date: 2019年12月30日 上午10:11:49 
 */
package com.eddc.method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.Parameter;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.task.Pdd_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.StringHelper;
import com.xxl.job.core.log.XxlJobLogger;

import lombok.extern.slf4j.Slf4j;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：PinDuoDuoData   
 * @author：jack.zhao       
 * 创建时间：2019年12月30日 上午10:11:49   
 * 类描述：   
 * @version    
 *    
 */
@Slf4j
@Component
public class PinDuoDuoData {
	@Autowired
	private ApplicationContext contexts= SpringContextUtil.getApplicationContext();
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	/*抓取多个地区的商品库存*/
	public void regionCrawler(List<QrtzCrawlerTable> listjobName, String tableName, String count,int pageTop,int sum) {
		String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
		Map<String,Object>parameter=new HashMap<>(5);
		parameter.put("time", time);
		parameter.put("count", count);
		parameter.put("tableName", tableName);
		parameter.put("crawlerType", Fields.STYPE_1);
		parameter.put("pageTop", pageTop);
		parameter.put("platform", listjobName.get(0).getPlatform());
		//从数据库获取城市code抓取库存
		List<Craw_keywords_delivery_place> List_Code=new ArrayList<>();

		try {
			List_Code = crawlerPublicClassService.Keywords_Delivery_Place(listjobName,listjobName.get(0).getDatabases());
		} catch (Exception e) {
			log.error("==>>获取商品库存地址发生异常：<<=="+e);
		}

		if(List_Code!=null && List_Code.size()>0){
			for(Craw_keywords_delivery_place delivery:List_Code){
				log.info("==>>当前商品抓取地区是"+delivery.getDelivery_place_name()+"<<==");
				if(listjobName.size()>0){
					parameter.put("listCode", delivery);
					crawlerCommodityAppDetails(listjobName,parameter,1); //开始抓取数据	
				}else{
					log.info("==>>当前用户"+listjobName.get(0).getPlatform()+"不存在请检查爬虫状态是否关闭！爬虫结束<<==");
					return ;
				}
			}
		}else{
			if(listjobName.size()>0){
				parameter.put("listCode", new Craw_keywords_delivery_place() );
				crawlerCommodityAppDetails(listjobName,parameter,1); //开始抓取数据
			}else{
				log.info("==>>当前用户"+listjobName.get(0).getPlatform()+"不存在请检查爬虫状态是否关闭！爬虫结束<<==");
				return;
			}
		}



	}
	/* 解析手机数据*/
	public int crawlerCommodityAppDetails(List<QrtzCrawlerTable> listjobName, Map<String, Object> parameter,int requestCount) {
		ExecutorService executorService = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());//Fields.COUNT_30
		ExecutorCompletionService<Map<String, Object>> completion = new ExecutorCompletionService<> (executorService);
		String stockCity=parameter.get("stockCity").toString();
		String time=parameter.get("time").toString();
		String count=parameter.get("count").toString();
		String tableName=parameter.get("tableName").toString();
		String code=parameter.get("code").toString();
		int pageTop=Integer.valueOf(parameter.get("pageTop").toString());
		Craw_keywords_delivery_place listCode=(Craw_keywords_delivery_place)parameter.get("listCode");
		List<CrawKeywordsInfo>list=new ArrayList<>();
		int page=0;
		list=crawlerPublicClassService.crawAndParseInfo(listjobName,listjobName.get(0).getDatabases(),tableName,count,pageTop);//开始解析数据 	
		XxlJobLogger.log("==>>当前一共要抓取:"+list.size()+"个商品<<==");
		for(int k=0;k<list.size();k++) {
			if(StringUtils.isNotBlank(String.valueOf(list.get(0).getSearch_page_nums()))) {
				page=list.get(k).getSearch_page_nums();
			}
			for(int i=1;i<=page;i++){
				try {
					Map<String,Object>data=new HashMap<String,Object>();
					Pdd_DataCrawlTask dataCrawlTask = (Pdd_DataCrawlTask) contexts.getBean(Pdd_DataCrawlTask.class);
					Parameter param=new Parameter();
					//					Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
					//					Matcher m = p.matcher(list.get(i).getCust_keyword_name());
					//					if (m.find()) {
					//						continue;
					//					}
					if(StringUtils.isNoneEmpty(list.get(0).getBatch_time())){
						time=list.get(k).getBatch_time();
					}	
					param.setMap(data);
					param.setBatch_time(time);
					
					param.setCoding(Fields.UTF8);
					param.setPage(Fields.STATUS_OFF);
					param.setListjobName(listjobName);
					param.setCrawKeywordsInfo(list.get(k));  
					param.setSum(""+(i)+"/"+page+"");
					param.setDeliveryPlace(listCode);
					param.setType(parameter.get("crawlerType").toString()); 
					param.setInclude(list.get(k).getCust_keyword_name());
					param.setKeywordId(list.get(k).getCust_keyword_id());
					param.setEgoodsId(list.get(k).getCust_keyword_name());
					param.setItemUrl_1(list.get(k).getCust_keyword_url());
					param.setItemUtl_2("http://mobile.yangkeduo.com/transac_batch_list.html?refer_page_name=personal&refer_page_id=10001_1594350379327_vzylmtzhfp&refer_page_sn=10001");
					param.setTimeDate(SimpleDate.SimpleDateFormatData().format(new Date()));
					param.setGoodsId(StringHelper.encryptByString(list.get(k).getCust_keyword_name()+listjobName.get(0).getPlatform()));
					dataCrawlTask.setParameter(param);
					completion.submit(dataCrawlTask);
					XxlJobLogger.log(i+"/"+list.size()+"线程==>>"+Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors()+completion.hashCode()+",Thread"+Thread.currentThread().getId());

				} catch (BeansException e) {
					log.error("==>>"+listjobName.get(0).getPlatform()+" 多线程请求数据发生异常："+e+"<<==");
					e.printStackTrace();
				}
			}
		}
		executorService.shutdown();
		while(true){
			if (executorService.isTerminated ( )) {
				break;
			}	
		}
		return list.size(); 
	}

}
