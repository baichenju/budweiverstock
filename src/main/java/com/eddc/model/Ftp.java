/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年5月23日 下午1:33:39 
 */
package com.eddc.model;

import lombok.Data;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Ftp   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年5月23日 下午1:33:39   
* 修改人：jack.zhao   
* 修改时间：2018年5月23日 下午1:33:39   
* 修改备注：   
* @version    
*    
*/
@Data
public class Ftp {
private String ipAddr;
private String userName;
private String pwd;
private int port;
private String path;


}
