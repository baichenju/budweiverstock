package com.eddc.model;

import java.io.Serializable;

import lombok.Data;
@Data
public class Craw_goods_Info_Failure implements Serializable {
	private int cust_keyword_id;
	private String goodsid;
	private String egoodsid;
	private String platform_name;
	private String goods_url;
	private int goods_status;
	private String update_time;
	
}
