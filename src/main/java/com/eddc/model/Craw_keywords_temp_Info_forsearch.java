/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年3月14日 下午3:06:06 
 */
package com.eddc.model;

import java.io.Serializable;

import lombok.Data;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_keywords_temp_Info_forsearch   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年3月14日 下午3:06:06   
* 修改人：jack.zhao   
* 修改时间：2018年3月14日 下午3:06:06   
* 修改备注：   
* @version    
*    
*/
@Data
public class Craw_keywords_temp_Info_forsearch  implements Serializable{
private String cust_account_id;
private String cust_account_name;
private String platform_name;
private String cust_keyword_id;
private String cust_keyword_name;
private String cust_keyword_type;
private String feature_1;
private String crawling_status;
private String crawling_fre;
private String update_time;
private String update_date;
private String always_abnormal;
private String is_presell;
private String platform_shopname;
private String position;
private int crawed_status;
private String cust_keyword_url;
private String platform_goodsname;



}
