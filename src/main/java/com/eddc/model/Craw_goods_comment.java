/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年11月20日 下午2:25:02 
 */
package com.eddc.model;

import lombok.Data;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Craw_goods_comment   
 * @author：jack.zhao       
 * 创建时间：2018年11月20日 下午2:25:02   
 * 类描述：   
 * @version    
 *    
 */
@Data
public class Craw_goods_comment {
	private String egoodsId;
	private String keyword;
	private String dataEgoodsId;

}
