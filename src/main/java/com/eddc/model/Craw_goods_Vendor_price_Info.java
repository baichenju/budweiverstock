/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年4月16日 下午3:22:26 
 */
package com.eddc.model;

import java.io.Serializable;

import lombok.Data;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_goods_Vendor_price_Info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年4月16日 下午3:22:26   
* 修改人：jack.zhao   
* 修改时间：2018年4月16日 下午3:22:26   
* 修改备注：   
* @version    
*    
*/
@Data
public class Craw_goods_Vendor_price_Info implements Serializable {
private int cust_keyword_id;//
private String goodsid;
private String egoodsid;
private String purchase_amount;//采购起批量
private String purchase_unit;//采购单位
private String purchase_price;//采购价格
private String purchase_discount;//采购促销价
private String channel;//
private String update_time;
private String update_date;
private String batch_time;


}
