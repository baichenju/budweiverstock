package com.eddc.model;

import java.io.Serializable;

import lombok.Data;

@SuppressWarnings("serial")
@Data
public class Craw_Access_keyword implements Serializable {
private int id;
private String accountid;
private String platform_name_en;
private String platform_name_cn;
private String keywords_cn;
private String keywords_en;
private String is_searched;
private String search_time;
private String price_floor;
private String page_nums;
}
