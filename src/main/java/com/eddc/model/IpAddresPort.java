package com.eddc.model;

import java.io.Serializable;

import lombok.Data;
@Data
public class IpAddresPort implements Serializable {
private int id;
private String ipAddress;
private String port;
private int number;
private String cookie;

}
