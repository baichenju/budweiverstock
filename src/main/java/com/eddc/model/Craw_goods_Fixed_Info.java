/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年4月23日 下午5:28:41 
 */
package com.eddc.model;

import java.io.Serializable;

import lombok.Data;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_goods_Fixed_Info   
* 类描述：  获取商品上架时间
* 创建人：jack.zhao   
* 创建时间：2018年4月23日 下午5:28:41   
* 修改人：jack.zhao   
* 修改时间：2018年4月23日 下午5:28:41   
* 修改备注：   
* @version    
*    
*/
@Data
public class Craw_goods_Fixed_Info implements Serializable {
private int cust_account_id;
private int cust_keyword_id;
private String platform_name_en;
private String egoodsid;
private String first_reviewtime;//---第一次评论时间
private  String update_time;

}
