package com.eddc.model;

import java.io.Serializable;

import lombok.Data;
@Data
public class Craw_keywords_Info implements Serializable {
private int id;
private int cust_account_id;
private String cust_account_name;
private String platform_name;
private String cust_keyword_id;
private String cust_keyword_name;
private String cust_keyword_type;
private String feature_1;
private int crawling_status;
private String crawling_fre;
private String update_time;
private String update_date;
private String always_abnormal ;
private String is_presell;
private int count; 


}