/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年5月7日 下午6:00:12 
 */
package com.eddc.model;

import java.io.Serializable;

import lombok.Data;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_goods_crowdfunding_Info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年5月7日 下午6:00:12   
* 修改人：jack.zhao   
* 修改时间：2018年5月7日 下午6:00:12   
* 修改备注：   
* @version    
*    
*/
@Data
public class Craw_goods_crowdfunding_Info implements Serializable{
private  int cust_keyword_id;
private String goodsId;
private String egoodsId;
private String platform_goods_name;//商品名称
private String platform_goods_detail;//商品详情
private String platform_name_en;//平台
private String platform_category;//所在类
private String platform_sellername;//卖家名称
private String platform_sellerdetail;//卖家详情
private float curr_amount;//已筹金额
private float target_amount;//目标金额
private float finish_per;//达成率
private  int support_count;//支持人数
private int focus_count;//喜欢人数
private  int remain_day;//剩余天数
private String begin_date;//开始日期
private String end_date;//众筹结束日期
private String goods_status;//众筹状态
private String goods_url;//商品详情链接
private String goods_pic_url;//商品图片
private int price_status;
private String update_time;
private String update_date;
private String batch_time;

}
