package com.eddc.model;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Scope("prototype")
public class CrawKeywordsInfo implements Serializable {  
private String cust_keyword_name;
private String cust_keyword_id;
private String platform_name;
private String cust_account_id;
private String batch_time;
private String number;
private String cust_keyword_url;
private String cust_account_name;
private String cust_keyword_remark;
private String cust_keyword_type;
private String crawling_fre;
private String always_abnormal;
private String is_presell;
private int search_page_nums;
private int crawling_status;
private String category_name;
private String category_id;
private String platform_shopid;
private String cust_keyword_brand;
private String cust_keyword_ref;
}
