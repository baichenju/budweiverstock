/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年3月15日 下午3:24:52 
 */
package com.eddc.model;

import java.io.Serializable;

import lombok.Data;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_keywords_delivery_place   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年3月15日 下午3:24:52   
* 修改人：jack.zhao   
* 修改时间：2018年3月15日 下午3:24:52   
* 修改备注：   
* @version    
*    
*/
@Data
@SuppressWarnings("serial")
public class Craw_keywords_delivery_place implements Serializable {
private int id;
private String cust_account_id;
private String platform_name;
private String delivery_place_code;
private String delivery_place_code2;
private String delivery_place_name;
private String delivery_place_type;
private int  crawling_status;
private String update_time;
private String update_date;
private int search_status;

}
