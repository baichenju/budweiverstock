package com.eddc.model;
import javax.persistence.Table;

import com.eddc.model.base.LifeCycle;
import lombok.Data;
/**
 * @author jack.zhao
 */
@SuppressWarnings("serial")
@Data
@Table(name="craw_goods_shop_promotion_Info")
public class Craw_goods_shop_promotion_Info extends LifeCycle {
	private int cust_keyword_id;
	private String egoodsid ;
	private String SKUid;
	private String channel;
	private String promotion;
	private String batch_time;
	private String platform_name_en;
	private String goodsid;
}
