/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年8月20日 下午1:32:47 
 */
package com.eddc.model;

import java.io.Serializable;

import lombok.Data;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_monitor_url_info   
* 类描述：   检查数据表（是否有抓取失败数据进行补抓）
* 创建人：jack.zhao   
* 创建时间：2018年8月20日 下午1:32:47   
* 修改人：jack.zhao   
* 修改时间：2018年8月20日 下午1:32:47   
* 修改备注：   
* @version    
*    
*/
@Data
@SuppressWarnings("serial")
public class Craw_monitor_url_info implements Serializable{
private int cust_account_id;
private String platform_name_cn;
private String jboName;
}
