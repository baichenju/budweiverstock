/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年8月15日 下午2:00:37 
 */
package com.eddc.model;
import com.eddc.model.base.LifeCycle;
import lombok.Data;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_goods_pic_Info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年8月15日 下午2:00:37   
* 修改人：jack.zhao   
* 修改时间：2018年8月15日 下午2:00:37   
* 修改备注：   
* @author jack.zhao
 * @version
*    
*/
@SuppressWarnings("serial")
@Data
public class Craw_goods_pic_Info extends LifeCycle{
private int cust_keyword_id;
private String goodsid;
private String pic_url;
private String pic_size;
//private String update_time;
//private String update_date;
private String batch_time;
private String pic_type;
private int pic_order;
private String picdown_url;
private String platform_name;
private String egoodsid;


}
