/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年5月14日 下午1:36:15 
 */
package com.eddc.model;

import java.io.Serializable;

import lombok.Data;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_goods_comment_Info   
* 类描述：商品评论数
* 创建人：jack.zhao   
* 创建时间：2018年5月14日 下午1:36:15   
* 修改人：jack.zhao   
* 修改时间：2018年5月14日 下午1:36:15   
* 修改备注：   
* @version    
*    
*/
@Data
@SuppressWarnings("serial")
public class Craw_goods_comment_Info implements Serializable{
private int cust_keyword_id;
private String goodsId;
private String egoodsId;
private String platform_name_en;
private String ttl_comment_num;//总评
private String pos_comment_num;//好评
private String neg_comment_num;//差评
private String neu_comment_num;//中评
private String comment_tags;//商品评论
private String comment_shopscores;//店铺评论
private String update_time;
private String update_date;
private String batch_time;
private int comment_status; 

}
