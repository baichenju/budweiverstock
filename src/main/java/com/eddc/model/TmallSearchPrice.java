package com.eddc.model;
import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import lombok.Data;
@SuppressWarnings("serial")
@Data
@Scope("prototype")
public class TmallSearchPrice  implements Serializable{ 
private String cust_keyword_id;
private String egoodsId;
private String platform_goods_name;

}
