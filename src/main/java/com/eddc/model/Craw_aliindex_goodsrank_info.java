/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年5月11日 下午1:33:34 
 */
package com.eddc.model;

import java.io.Serializable;

import lombok.Data;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_aliindex_goodsrank_info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年5月11日 下午1:33:34   
* 修改人：jack.zhao   
* 修改时间：2018年5月11日 下午1:33:34   
* 修改备注：   
* @version    
*    
*/
@Data
public class Craw_aliindex_goodsrank_info implements Serializable {
private int  category_id;//类别id 例如 122312006　
private int rank_dimention_id;//---1: trade; 2: flow
private int rank_period_id;//7: week; 30: month
private int rank_type_id;//1 hot; 2: latest; 3: rise
private int rank_date_id;//-20180510 排行日期
private int goods_rank;//从零开始 排行榜
private String egoods_id;//商品ID
private String goods_name;///商品名称
private String image_url;//商品图片 　
private String company_name;//公司名称	　
private String company_url;//公司链接
private String comment_count;//商品评论数
private Float goods_price;//商品价格
private int  goods_trade;//商品交易指数
private int goods_flow;//商品流量指数
private String rank_date;//排行日期
private String insert_time;//-插入时间
private String update_time;//更新时间
private String samegoods_source_url;//相似商品url

}
