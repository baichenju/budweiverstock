package com.eddc.model;
import javax.persistence.Table;
import com.eddc.model.base.LifeCycle;
import lombok.Data;
@SuppressWarnings("serial")
@Data
@Table(name="Craw_goods_Info")
public class Craw_goods_InfoVO extends LifeCycle {
private int cust_keyword_id;
private String goodsId;
private String egoodsId;
private String platform_goods_name;
private String platform_name_en;
private int  goods_status;
private String goods_url;
private String goods_pic_url;
//private String update_time;
//private String update_date;
private String batch_time;
private String position;
private String platform_shoptype;
private String platform_shopname;
private String platform_sellername;
private String sale_qty;
private String ttl_comment_num;
private String delivery_place;
private String bsr_rank;
private String delivery_info;
private String seller_location;
private String product_location;
private String platform_shopid;
private String inventory;
private String feature1;
private String platform_category;
private String platform_sellerid;
private String jhs_paid_num;
private String pos_comment_num;//好评
private String neg_comment_num;//差评
private String neu_comment_num;//中评
}
