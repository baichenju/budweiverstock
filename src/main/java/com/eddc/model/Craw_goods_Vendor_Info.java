/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年4月16日 下午3:13:10 
 */
package com.eddc.model;

import java.io.Serializable;

import lombok.Data;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_goods_Vendor_Info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年4月16日 下午3:13:10   
* 修改人：jack.zhao   
* 修改时间：2018年4月16日 下午3:13:10   
* 修改备注：   
* @version    
*    
*/
@Data
@SuppressWarnings("serial")
public class Craw_goods_Vendor_Info implements Serializable {
public int cust_keyword_id;
public String goodsid;
public String egoodsid;//---商品平台ID
public String platform_goodsname;//商品名称
public String platform_goods_detail;//商品详情
public String platform_goods_satisfaction;//商品满意度
public String platform_goodsurl;//商品页链接
public String platform_goods_picurl;//商品图片
public String platform_vendorid;//供应商ID
public String platform_vendorname;//供应商名称
public String platform_vendortype;//供应商类型（经营模式）
public String platform_vendoraddress;//供应商地址
public String goods_description;//货物描述分
public String service_attitude;//服务态度分
public String delivery_speed;//发货速度分
public String purchase_back;//继续购买分

public String goods_description_rate;//货描 高于或者低于均值
public String service_attitude_rate;//服务 高于或者低于均值
public String delivery_speed_rate;//-发货 高于或者低于均值
public String purchase_back_rate;//回头率 高于或者低于均值
public String purchase_stock;//采购库存
public String purchase_qty;//采购数量
public String purchase_comment_num;//评价数
public String purchase_promotion;//采购促销
public String purchaser_impression;//采购商印象
public String channel;//渠道
public String platform_sellername;//店铺掌柜
public int goods_status;
public String platform_name_en;//平台
public String update_time;
public String update_date;
public String batch_time;


}
