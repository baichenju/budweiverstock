/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年5月30日 下午3:37:55 
 */
package com.eddc.model;

import java.io.Serializable;

import lombok.Data;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_goods_crowdfunding_price_Info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年5月30日 下午3:37:55   
* 修改人：jack.zhao   
* 修改时间：2018年5月30日 下午3:37:55   
* 修改备注：   
* @version    
*    
*/
@Data
@SuppressWarnings("serial")
public class Craw_goods_crowdfunding_price_Info implements Serializable{
private int cust_keyword_id;//
private String goodsId;//
private String egoodsId;//
private String source_egoodsid;
private String platform_goods_name;//回报商品内容
private String platform_goods_remark;//回报商品内容的进一步说明
private String platform_name_en;//平台
private Float curr_price;//众筹价
private int support_count;//支持人数
private int limit_total_count;//限制人数
private int remain_count;//剩余人数
private String exp_shiptime_remark;//预计回报说明
private String exp_postfee_remark;//邮费说明
private String goods_url;//商品页面链接
private String goods_pic_url;//回报商品的商品小图片链接
private Float source_curr_amount;
private int source_support_count;
private int source_focus_count;
private Float source_finish_per;
private String source_goods_status;
private String update_time;//
private String update_date;//
private String batch_time;//


}
