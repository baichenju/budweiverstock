/**   
 * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2017年11月16日 下午3:40:39 
 */
package com.eddc.model;

import java.io.Serializable;

import lombok.Data;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Craw_customerweb_categorys_info   
 * 创建人：jack.zhao   
 * 创建时间：2017年11月16日 下午3:40:39   
 * 修改人：jack.zhao   
 * 修改时间：2017年11月16日 下午3:40:39   
 * 修改备注：   
 * @version    
 *    
 */
@Data
public class Craw_customerweb_categorys_info implements Serializable {
private int Userid ;
private String series ;
private String categories ;
private String skus ;
private String  website_name;
private String website_url ;
private String update_time;
private String update_date;

}
