/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年5月15日 上午11:51:04 
 */
package com.eddc.model;

import java.io.Serializable;

import lombok.Data;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_aliindex_samegoods_info   
* 类描述：获取阿里指数相似商品
* 创建人：jack.zhao   
* 创建时间：2018年5月15日 上午11:51:04   
* 修改人：jack.zhao   
* 修改时间：2018年5月15日 上午11:51:04   
* 修改备注：   
* @version    
*    
*/
@Data
public class Craw_aliindex_samegoods_info  implements Serializable{
private int soure_category_id;// ---类别id 例如 
private  String soure_egoods_id;//商品父节点ID
private String egoods_id;//商品egoodsid
private String goods_name;//商品名称
private String image_url;//-商品图片 
private String company_name;//公司名称
private String  company_url;//公司链接
private String  comment_count;//商品评论数
private Float goods_price;//商品价格
private String insert_time;//插入时间
private String update_time;//更新时间

}
