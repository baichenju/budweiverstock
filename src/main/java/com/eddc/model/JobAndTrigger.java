package com.eddc.model;

import java.io.Serializable;
import java.math.BigInteger;

import lombok.Data;
@Data
@SuppressWarnings("serial")
public class JobAndTrigger implements Serializable { 
	private String SCHED_NAME;
	private String JOB_NAME;
	private String JOB_GROUP;
	private String JOB_CLASS_NAME;
	private String TRIGGER_NAME;
	private String TRIGGER_GROUP;
	private BigInteger REPEAT_INTERVAL;
	private BigInteger TIMES_TRIGGERED;
	private String CRON_EXPRESSION;
	private String TIME_ZONE_ID;
	private String STATUS;
	private String IP;
	private String DATASOURCE;
	private String PLATFORM;
	private int THREAD_SUM;
	private String DOCKER;
	private String DESCRIBE;
	private String INVENTORY;
	private String CLIENT;
	private int   PROMOTION_STATUS;
	private int   PRICE_STATUS;
	private int   INVENTORY_STATUS;
	private String DATABASES;
	private String STORAGE;
	private int SUMS;
	private int SUM_REQUEST_NUM;
	private  int REDIS_TIME;
	private String COMMODITY_ATTRIBUTE;
	private String API_URL;

}
