package com.eddc.model;
import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.Data;
@SuppressWarnings("serial")
@Data
@Scope("prototype")
public class CommodityPrices implements Serializable{
private String goodsid;
private String egoodsId;
private String cust_keyword_id;
private String platform_category;
private String platform_shopid;
private String batch_time;
private String message;
private String platform_name_en;
private String channel;
private String platform_sellerid;

}
