/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年5月10日 下午5:03:00 
 */
package com.eddc.model;

import java.io.Serializable;

import lombok.Data;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_aliindex_category_info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年5月10日 下午5:03:00   
* 修改人：jack.zhao   
* 修改时间：2018年5月10日 下午5:03:00   
* 修改备注：   
* @version    
*    
*/
@SuppressWarnings("serial")
@Data
public class Craw_aliindex_category_info implements Serializable {
private int category_id;//类别id
private String category_name;//类别名称 例如 家用游戏机
private int parent_id;//父类id
private int category_level;//类别等级
private  int has_child;//是否有子类  1表示有 0表示无
private String category_path;//组合类别id 例如
private  int category_status;//---类别状态  0表示未删除
private String platform_name;//平台 index1688
private String insert_time;//插入时间
private String update_time;//更新时间

}
