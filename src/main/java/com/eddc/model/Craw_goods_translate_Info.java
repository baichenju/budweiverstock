/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年4月26日 下午4:27:33 
 */
package com.eddc.model;

import java.io.Serializable;

import lombok.Data;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_goods_translate_Info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年4月26日 下午4:27:33   
* 修改人：jack.zhao   
* 修改时间：2018年4月26日 下午4:27:33   
* 修改备注：   
* @version    
*    
*/
@Data
public class Craw_goods_translate_Info implements Serializable {
private int  cust_account_id;
private int cust_keyword_id;
private String platform_name_en;
private String egoodsid;
private String platform_goods_name_en;
private String platform_goods_name_cn;
private String update_time;

}
