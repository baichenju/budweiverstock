/**   
 * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2017年12月12日 下午1:30:47 
 */
package com.eddc.model;

import java.io.Serializable;

import lombok.Data;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Craw_cookies_info   
 * 类描述：   获取商品的Cookie
 * 创建人：jack.zhao   
 * 创建时间：2017年12月12日 下午1:30:47   
 * 修改人：jack.zhao   
 * 修改时间：2017年12月12日 下午1:30:47   
 * 修改备注：   
 * @version    
 *    
 */
@Data
public class Craw_cookies_info implements Serializable {
private int id;
private int userid ;
private String  platform_name ;
private String  cookie;
private String  cookie_state;
private String  url;
private String  insert_time;
private String update_time;
}
