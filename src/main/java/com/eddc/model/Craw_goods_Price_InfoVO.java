package com.eddc.model;

import com.eddc.model.base.LifeCycle;
import lombok.Data;
@Data
@SuppressWarnings("serial")
public class Craw_goods_Price_InfoVO extends LifeCycle {
private String cust_keyword_id;
private String goodsid;
private String original_price;
private String current_price;
//private String update_time;
//private String update_date;
private String batch_time;
private String channel;
private String sku_name;//sku名称


}
