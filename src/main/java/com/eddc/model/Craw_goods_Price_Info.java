package com.eddc.model;
import com.eddc.model.base.LifeCycle;
import lombok.Data;
@Data
public class Craw_goods_Price_Info extends LifeCycle  {
private static final long serialVersionUID = 1L;
private String cust_keyword_id;
private String goodsid;
private String SKUid;
private String channel;
private String original_price;
private String current_price;
private String promotion;
//private String update_time;
//private String update_date;
private String batch_time;
private String sale_qty;//销量
private String inventory;//库存
private String deposit;//定金
private String to_use_amount;//抵用金额
private String reserve_num;//预定件数
private String sku_name;//sku名称
private String platform_name_en;
private String head_promotion;
private String delivery_place;

}
