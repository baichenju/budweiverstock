/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年6月26日 上午11:01:08 
 */
package com.eddc.model;
import com.eddc.model.base.LifeCycle;
import lombok.Data;

/**   
*    
* 项目名称：selection_sibrary_crawler   
* 类名称：Craw_goods_attribute_Info   
* 类描述：   商品属性表
* 创建人：jack.zhao   
* 创建时间：2018年6月26日 上午11:01:08   
* 修改人：jack.zhao   
* 修改时间：2018年6月26日 上午11:01:08   
* 修改备注：   
* @author jack.zhao
 * @version
*    
*/
@Data
@SuppressWarnings("serial")
public class Craw_goods_attribute_Info extends LifeCycle{
private int cust_keyword_id;
private String goodsid;
private String egoodsid;
private String craw_attri_cn;
private String craw_attri_en;
private String attri_value;
//private String update_time;
//private String update_date;
private String batch_time;
private String platform_name_en;
}
