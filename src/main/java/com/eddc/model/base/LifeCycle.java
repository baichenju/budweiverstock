package com.eddc.model.base;
import java.util.Date;
import com.eddc.util.SimpleDate;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public abstract class LifeCycle extends Model {

	private static final long serialVersionUID = -5502630967005123165L;

	protected String update_date =SimpleDate.SimpleDateFormatData().format(new Date());
	//DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss")
	protected String update_time = SimpleDate.SimpleDateFormatData().format(new Date());

}
