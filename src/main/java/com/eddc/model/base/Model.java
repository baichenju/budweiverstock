package com.eddc.model.base;
import java.io.Serializable;

/**
 * 该类和子类中的属性必须是基本数据类型的包装类，为了保证new出来的对象初始值为null
 * @author conner
 *
 */
public class Model implements Serializable {
	
	static final long serialVersionUID = -8357847346186989504L;
	
}
