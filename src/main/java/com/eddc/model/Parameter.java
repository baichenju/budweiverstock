/**   
 * Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2019年7月24日 上午10:48:19 
 */
package com.eddc.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.Data;

/**   
 *    
 * 项目名称：data_monitoring_crawler   
 * 类名称：Parameter   
 * @author：jack.zhao       
 * 创建时间：2019年7月24日 上午10:48:19   
 * 类描述：   
 * @version    
 *    
 */
@SuppressWarnings("serial")
@Data
public class Parameter implements Serializable{
	private String timeDate;
	private String type;
	private String page;
	private String sum;
	private String egoodsId;
	private String goodsId;
	private String batch_time;
	private String keywordId;
	private String include;
	private String coding;
	private String region;
	private String itemUrl_1;
	private String itemUtl_2;
	private String itemUtl_3;
	private String itemUtl_4;
	private String itemUtl_5;
	private String promotion_1;
	private String promotion_2;
	private String promotion_3;
	private String promotion_4;
	private String cookie;
	private String category;
	private Map<String,Object>map;
	private CrawKeywordsInfo crawKeywordsInfo;
	private CommodityPrices commodityPrices;
	private List<QrtzCrawlerTable>listjobName;
	private Craw_keywords_delivery_place deliveryPlace;
	private boolean proxy = true; 
	private int htmlKb=0;
}
