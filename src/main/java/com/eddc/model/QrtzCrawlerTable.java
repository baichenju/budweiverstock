package com.eddc.model;

import java.io.Serializable;

import lombok.Data;
@Data
@SuppressWarnings("serial")
public class QrtzCrawlerTable implements Serializable {
private int id;
private String user_Id;
private String job_name;
private String platform;
private String IP;
private String dataSource;
private String data_time;
private String status;
private String cronEx_expression;
private int thread_sum;
private String docker;
private String number;
private String describe;
private String inventory;
private String client;
private int   Promotion_status;
private int   price_status;
private int   inventory_status;
private String databases;
private String storage;
private int sums;
private int sum_request_num;
private String redis_time;
private String commodity_attribute;
private String api_url;
}
