package com.eddc.model;
import java.io.Serializable;

import lombok.Data;
@Data
public class Craw_goods_Info  implements Serializable {
private static final long serialVersionUID = 2L;  
private int cust_keyword_id;
private String goodsId;
private String egoodsId;
private String message;
private String platform_goods_name;
private String platform_name_en;
private String platform_category;
private String platform_sellerid;
private String platform_sellername;
private String platform_shopid;
private String platform_shopname;
private String platform_shoptype;
private String delivery_info;
private String delivery_place;
private String seller_location;
private int  goods_status;
private String inventory;
private String sale_qty;
private String ttl_comment_num;
private String pos_comment_num;
private String neg_comment_num;
private String neu_comment_num;
private String goods_url;
private String goods_pic_url;
private String update_time;
private String update_date;
private String feature1;
private String batch_time;
private String deposit;
private String to_use_amount;
private String reserve_num;
private String product_location;
private String putaway;
private String shelves;
private String cust_account_id;
private String position;
private String bsr_rank;
}
