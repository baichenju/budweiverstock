/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.util 
 * @author: jack.zhao   
 * @date: 2018年5月15日 下午5:16:09 
 */
package com.eddc.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：SimpleDate   
 * 类描述： 日期归类  
 * 创建人：jack.zhao   
 * 创建时间：2018年5月15日 下午5:16:09   
 * 修改人：jack.zhao   
 * 修改时间：2018年5月15日 下午5:16:09   
 * 修改备注：   
 * @version    
 *    
 */
@Component
public class SimpleDate {

	//时间格式 yyyy-MM-dd HH:mm:ss
	public static SimpleDateFormat SimpleDateFormatData(){
		SimpleDateFormat data=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return data;
	}
	//时间格式 yyyyMMddHHmmss
	public static SimpleDateFormat SimpleDateFormatDataTime(){
		SimpleDateFormat data=new SimpleDateFormat("yyyyMMddHHmmss");
		return data;
	}

	//时间格式 yyyyMMddHHmmss
	public static SimpleDateFormat SimpleDateFormatDataHour(){
		SimpleDateFormat data=new SimpleDateFormat("yyyyMMddHH");
		return data;
	}

	//时间格式 yyyy-MM-dd 
	public static SimpleDateFormat SimpleDateData(){
		SimpleDateFormat data=new SimpleDateFormat("yyyy-MM-dd");
		return data;
	}
	//时间格式 yyyyMMdd 
	public static SimpleDateFormat SimpleDateTime(){
		SimpleDateFormat data=new SimpleDateFormat("yyyyMMdd");
		return data;
	}
	//时间格式 yyyyMMdd 
	public static SimpleDateFormat SimpleDateFormatData_zn(){
		SimpleDateFormat fort=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		return fort;
	}
	//时间格式 H
	public static SimpleDateFormat SimpleDateFormatDataHours(){
		SimpleDateFormat fort=new SimpleDateFormat("HH");
		return fort;
	}
	//毫秒 日期转换
	public static  String getMilliSecond(long mill){
		Date date = new Date();
		date.setTime(mill);
		return SimpleDateFormatData().format(date);

	}
	//获取当前周几
	public static String StringWeekday(){
		String[] weekDays = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (w < 0)
			w = 0;
		return weekDays[w];
	}
}