package com.eddc.util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.os.WindowsUtils;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 项目名称：Price_monitoring_crawler
 * 类名称：WebDriverUtil
 *
 * @author：jack.zhao 创建时间：2020年1月16日 上午9:53:59
 * 类描述：启动 driver 爬虫
 */
public class WebDriverUtil {

    public static WebDriver getWebDriver() {
        WindowsUtils.tryToKillByName("chromedriver.exe");
        WindowsUtils.tryToKillByName("chrome.exe");
        
        String user_Agent="Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36";
		System.setProperty("webdriver.chrome.driver","/Users/baichen/Downloads/chromedriver");
		DesiredCapabilities cap= DesiredCapabilities.chrome();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("user-agent="+user_Agent);
		Map<String, String> mobileEmulation = new HashMap<String, String>();
		mobileEmulation.put("deviceName", "Google Nexus 5");
		Map<String, Object> chromeOptions = new HashMap<String, Object>(); 
		chromeOptions.put("mobileEmulation", mobileEmulation);
		cap.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		WebDriver driver = new ChromeDriver(cap);
		driver.manage().timeouts().pageLoadTimeout(500, TimeUnit.SECONDS);//-----页面加载时间
        /**
        String user_Agent = "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36";
        System.setProperty("webdriver.chrome.driver","d:\\chromedriver.exe");
        List<String> op = new ArrayList<String>();
        ChromeOptions options = new ChromeOptions();
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        //判断是否无界面还是有界面打开浏览器
//        if(type==1){
//        	op.add("--headless");	
//        }
//        op.add("--test-type");
//        op.add("--user-agent="+user_Agent);
        options.addArguments("user-agent="+user_Agent);
        //options.addArguments(op);
        cap.setCapability(ChromeOptions.CAPABILITY, options);
//        cap.setCapability(CapabilityType.ForSeleniumServer.AVOIDING_PROXY, true);
//        cap.setCapability(CapabilityType.ForSeleniumServer.ONLY_PROXYING_SELENIUM_TRAFFIC, true);
//        System.setProperty("http.nonProxyHosts", "localhost");
        ChromeDriver driver = new ChromeDriver(cap);
        **/
//        /*页面加载时间*/
//        driver.manage().timeouts().pageLoadTimeout(10000, TimeUnit.SECONDS);
//        /*元素等待时间（隐式等待）*/
//        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.SECONDS);
//        /* 脚本执行时间*/
//        driver.manage().timeouts().setScriptTimeout(10000, TimeUnit.SECONDS);
//
        
        return driver;
    }
    
    public static  WebDriver webDriverFirefox(){
    	String user_Agent = "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36";
    	System.setProperty("webdriver.firefox.bin", "D:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"); 
    	//System.setProperty("webdriver.firefox.marionette","D:\\geckodriver.exe");
    	WebDriver driver=new FirefoxDriver();
    	driver.get("https://www.baidu.com");
		return driver;
    	
    }

}
