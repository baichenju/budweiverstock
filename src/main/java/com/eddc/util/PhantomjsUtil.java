///**   
// * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
// * 
// * @Package: com.eddc.util 
// * @author: jack.zhao   
// * @date: 2018年9月9日 下午12:36:30 
// */
//package com.eddc.util;
//
//import java.io.File;
//import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.List;
//import java.util.ResourceBundle;
//import java.util.concurrent.TimeUnit;
//
//import org.apache.commons.io.FileUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.openqa.selenium.OutputType;
//import org.openqa.selenium.TakesScreenshot;
//import org.openqa.selenium.TimeoutException;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.phantomjs.PhantomJSDriver;
//import org.openqa.selenium.phantomjs.PhantomJSDriverService;
//import org.openqa.selenium.remote.DesiredCapabilities;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import com.eddc.service.HttpClientService;
//import com.eddc.task.Keywords_DataCrawlTask;
//
//import net.sf.json.JSONObject;
//
///**   
// *    
// * 项目名称：Price_monitoring_crawler   
// * 类名称：PhantomjsUtil   
// * 类描述：   
// * 创建人：jack.zhao   
// * 创建时间：2018年9月9日 下午12:36:30   
// * 修改人：jack.zhao   
// * 修改时间：2018年9月9日 下午12:36:30   
// * 修改备注：   
// * @version    
// *    
// */
//@Component
//public class PhantomjsUtil {
//	@Autowired
//	HttpClientService httpClientService;
//	Logger log = LoggerFactory.getLogger(PhantomjsUtil.class);
//	
//	@SuppressWarnings("static-access")
//	public String phantomjsDataDriver(String url) throws InterruptedException{
//		String path=ResourceBundle.getBundle("docker").getString("IP_ADDRES");
//		String userName=ResourceBundle.getBundle("docker").getString("userName_data");
//		String password=ResourceBundle.getBundle("docker").getString("password_data");
//		String phantomjs=ResourceBundle.getBundle("docker").getString("phantomjs");
//		
//		System.setProperty(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,phantomjs);
//		String message=""; int count=0; WebDriver driver =null;
//		try {
//			String ipData=httpClientService.getJsonObj(path);
//			JSONObject currPriceJson = JSONObject.fromObject(ipData);
//			DesiredCapabilities desiredCapabilities = new DesiredCapabilities().phantomjs();
//			desiredCapabilities.setCapability( "phantomjs.page.settings.userAgent",HttpCrawlerUtil.CrawlerAttribute());
//			desiredCapabilities.setCapability("acceptSslCerts", true);//ssl证书支持
//			desiredCapabilities.setCapability("takesScreenshot", true);//截屏支持
//			desiredCapabilities.setCapability("cssSelectorsEnabled", true);//css搜索支持
//			desiredCapabilities.setJavascriptEnabled(true);//js支持
//			List<String> cli = new ArrayList<>();
//			cli.add("--output-encoding=UTF-8");
//			cli.add("--proxy="+currPriceJson.getString("ip")+":"+ currPriceJson.getString("port"));
//			cli.add("--proxy-auth="+userName+":"+password);
//			desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, cli);
//			driver = new PhantomJSDriver(desiredCapabilities);
//			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//			try{
//				driver.get(url); 
//				Document docs =Jsoup.parse(driver.getPageSource());
//				message=docs.toString();
//				while (Validation.isEmpty(message)) {
//					count++;
//					if (count > 5) {
//						break;
//					} 
//					if(Validation.isNotEmpty(message)){
//						try{
//							driver.close();
//							WebDriver	drivers = new PhantomJSDriver(desiredCapabilities);
//							drivers.get(url); 
//							docs =Jsoup.parse(drivers.getPageSource());
//							message=docs.toString();
//						}catch(Exception e){
//							log.error("请求数据失败>>>>>>>>>>>>>>>>>"+e);
//						}		
//					}
//				}
//			}catch(TimeoutException e){
//				message =httpClientService.httpGet(url,null);
//			}finally{
//				driver.quit();
//			}
//			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");  //转换时间格式
//			String time = dateFormat.format(Calendar.getInstance().getTime());  //获取当前时间
//			Thread.sleep(5000);
//			File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);  //执行屏幕截取
//			FileUtils.copyFile(srcFile, new File("d:/temp/"+ time + ".png"));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return message; 
//
//	}
//}
