package com.eddc.util;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
@Service("whetherShelves")
public class WhetherShelves {
	static Logger log = LoggerFactory.getLogger(WhetherShelves.class);
	
	/**
	 * 判断商品是否下架
	 * @param itemPage
	 * @return 下架或者当前站不销售"0",未下架"1",抓取异常"-1" 该商品已下架
	 */
	
	public  String parseGoodsStatusByItemPage(String itemPage) {
		try {
			if (
				itemPage.contains(Fields.PAGE_WITHOUT)||
				itemPage.contains("sku信息不存在")||
				itemPage.contains("该商品已下架")||
				itemPage.contains("该商品已下柜") ||
				itemPage.contains("暂无报价") || 
				itemPage.contains("该商品已下柜，欢迎挑选其他商品！")  ||
				itemPage.contains("该商品已下架")  ||
				itemPage.contains("errorimg")|| 
				itemPage.contains("宝贝不存在")|| 
				itemPage.contains("已下架") || 
				itemPage.contains("该商品下架不再销售中") ||
				itemPage.contains("您查看的宝贝不存在")|| 
				itemPage.contains("此商品已下架") ||
				itemPage.contains("很抱歉，您查看的商品找不到了！")||
				itemPage.contains("该商品已下柜") ||
				itemPage.contains("商品已经下架啦~要不要瞧瞧别的~")|| 
				itemPage.contains("该商品已下柜，非常抱歉！")||
				itemPage.contains("商品已经下架啦")||
				itemPage.contains("在寻找某商品？")||
				itemPage.contains("很抱歉。您输入的网址不是我们网站上的有效网页") ||
				itemPage.contains("Sorry! We couldn't find that page. Try searching or go to Amazon's home page.")||
				itemPage.contains("京东拼购") ||
				itemPage.contains("错误错误") ||
				itemPage.contains("多多果园") ||
				itemPage.contains("很抱歉，您查看的页面找不到了") ||
				itemPage.contains("isSuperAct=false")||
				itemPage.contains("商品已下架") ||
				itemPage.contains("查看该店铺其他上架商品") ||
				itemPage.contains("抱歉，您要访问的页面不存在")||
				itemPage.contains("\"errCode\":\"10001\"")||
				itemPage.contains("\"logMsg\":\"价格暂不销售\"")||
				itemPage.contains("此商品已删除")||
				itemPage.contains("有可能我们的网页正在维护或者您输入的网址不正确") ||
				itemPage.contains("var Page_type = 1")||
				itemPage.contains("抱歉，本专场商品无法在电脑上查看")||
				itemPage.contains("404-错误，没有找到您想要进入的页面00")||
				itemPage.contains("抱歉，您请求的页面现在无法打开")||
				itemPage.contains("gdstats:\"1\"")||
                itemPage.contains(Fields.SOLOD_OUT)||
				itemPage.contains("\"state\":0,")
			) {
				return "0";
			} else if (Validation.isEmpty(itemPage)) {
				return "-1";
			}
		} catch (Exception e) { 
			return "0"; 
		}
		return "1"; 
	}
	/**
	 * 判断商品是否下架
	 * @param itemPage
	 * @return 下架或者当前站不销售"0",未下架"1",抓取异常"-1"
	 */
	public  String parseGoodsStatusJumei(String itemPage,String egoodsId) {
		if (itemPage.contains("支付方式")||!itemPage.contains(egoodsId) ) {
			return "0"; 
		} else if (Validation.isEmpty(itemPage)) { 
			return "-1";
		}
		return "1";  
	}
	
	/**
	 * 苏宁判断商品是否下架
	 * @param
	 * @return 下架或者当前站不销售"0",未下架"1",抓取异常"-1"
	 */
	public static String getGoodsStatus(String itemPage) {
		try {
			if(!Validation.isEmpty(itemPage)){
				String status="";
				String strs=itemPage.substring(itemPage.indexOf("{"), itemPage.lastIndexOf("}")+1);
				JSONObject currPriceJson = JSONObject.fromObject(strs);
				JSONArray priceInfoJson = currPriceJson.getJSONObject("data").getJSONObject("price").getJSONArray("saleInfo");
				if(priceInfoJson.size()>0){
					status=JSONObject.fromObject(priceInfoJson.toArray()[0]).get("invStatus").toString();
				}else{
					 status=currPriceJson.getJSONObject("data").get("invStatus").toString();	
				}
				if(status.equals(Fields.STATUS_ON) || status.equals(Fields.STATUS_ON_2) || status.equals(Fields.STATUS_ON_3)){
					return Fields.STATUS_ON;
				}else{
					return Fields.STATUS_OFF;	
				} 
			}else{
				log.info("数据请求失败"+itemPage);
				return "-1"; 	
			}
		} catch (Exception e) {
			return "0"; 
		}
	}

	/**
	 * 判断商品是否下架
	 * @param
	 * @return 下架或者当前站不销售"0",未下架"1",抓取异常"-1"
	 */
	public  String parseGoodsStatusKaoLa(String itemPage) {
		if (itemPage.contains("aAddCart")) {
			return "1";
		} else if (Validation.isEmpty(itemPage)) {
			return "-1";
		}else{
			return "0";	
		}

	}
	/**
	 * 设置 天猫抓取数据当前城市
	 * @throws InterruptedException 
	 */
	public static String CityAddressCode(SimpleDateFormat data){
		String code="310100"; 
		if(data.format(new Date()).equals(Fields.data24) || data.format(new Date()).equals(Fields.data1) || data.format(new Date()).equals(Fields.data2) || data.format(new Date()).equals(Fields.data3) || data.format(new Date()).equals(Fields.data4) || data.format(new Date()).equals(Fields.data5) || data.format(new Date()).equals(Fields.data6)|| data.format(new Date()).equals(Fields.data7) || data.format(new Date()).equals(Fields.data8) || data.format(new Date()).equals(Fields.data9) ){
			code="110100";//北京
		}else if(data.format(new Date()).equals(Fields.data10) || data.format(new Date()).equals(Fields.data11) || data.format(new Date()).equals(Fields.data12) || data.format(new Date()).equals(Fields.data13) || data.format(new Date()).equals(Fields.data14) || data.format(new Date()).equals(Fields.data15) || data.format(new Date()).equals(Fields.data16) || data.format(new Date()).equals(Fields.data17) || data.format(new Date()).equals(Fields.data18) || data.format(new Date()).equals(Fields.data19) ){
			code="310100";//上海
		}else if( data.format(new Date()).equals(Fields.data20) || data.format(new Date()).equals(Fields.data21) || data.format(new Date()).equals(Fields.data22) || data.format(new Date()).equals(Fields.data23)  ){
			code="440100";//广州
		}else if(data.format(new Date()).equals(Fields.data19) || data.format(new Date()).equals(Fields.data20) || data.format(new Date()).equals(Fields.data21) || data.format(new Date()).equals(Fields.data22) || data.format(new Date()).equals(Fields.data23) || data.format(new Date()).equals(Fields.data24) ){
			code="440300";//深圳
		}

		return code;

	}
	/**
	 * 设置 一号店抓取数据当前城市
	 * @throws InterruptedException 
	 */
    public Map<String, String> CityAddressCodeYHd(SimpleDateFormat data){
    	Map<String, String> codeMessage=new HashMap<String, String>();
    	String code="&provinceId=2&cityId=1000";
    	String delivery_place="上海";
    	if(data.format(new Date()).equals(Fields.data24) || data.format(new Date()).equals(Fields.data1) || data.format(new Date()).equals(Fields.data2) || data.format(new Date()).equals(Fields.data3) || data.format(new Date()).equals(Fields.data4) || data.format(new Date()).equals(Fields.data5) || data.format(new Date()).equals(Fields.data6)|| data.format(new Date()).equals(Fields.data7) || data.format(new Date()).equals(Fields.data8) || data.format(new Date()).equals(Fields.data9) ){
			code="&provinceId=2&cityId=1000";//北京
			delivery_place="北京";
		}else if(data.format(new Date()).equals(Fields.data10) || data.format(new Date()).equals(Fields.data11) || data.format(new Date()).equals(Fields.data12) || data.format(new Date()).equals(Fields.data13) || data.format(new Date()).equals(Fields.data14) || data.format(new Date()).equals(Fields.data15)  || data.format(new Date()).equals(Fields.data16)  || data.format(new Date()).equals(Fields.data17) || data.format(new Date()).equals(Fields.data18) || data.format(new Date()).equals(Fields.data19)){
			code="&provinceId=1&cityId=1";//上海
			delivery_place="上海";
		}else if(data.format(new Date()).equals(Fields.data20) || data.format(new Date()).equals(Fields.data21) || data.format(new Date()).equals(Fields.data22) || data.format(new Date()).equals(Fields.data23) ){
			code="&provinceId=20&cityId=237";//广州
			delivery_place="广州";
//		}else if(data.format(new Date()).equals(Fields.data19) || data.format(new Date()).equals(Fields.data20) || data.format(new Date()).equals(Fields.data21) || data.format(new Date()).equals(Fields.data22) || data.format(new Date()).equals(Fields.data23) || data.format(new Date()).equals(Fields.data24) ){
//			code="&provinceId=20&cityId=238";//深圳
//			delivery_info="深圳";
		}
    	codeMessage.put("code", code);
    	codeMessage.put("delivery_place", delivery_place);
		return codeMessage;
    	
    }
	public String parseGoodsStatusVip(String itemPage) {
		String result = "1";
		try { 
//			if(itemPage.contains("var Page_type = -1")){
//				result = "0";
		    if(itemPage.contains("var Page_type = 1")){
				result = "1";
			}else if(itemPage.contains("抱歉，本专场商品无法在电脑上查看")){
				result = "0";
			}else if(itemPage.contains("已下架")){
				result = "0";  
			}else if(itemPage.contains("\"state\":0,")){
				result = "0";  
			}
		} catch (Exception e) { 
			result = "-1";
		} 
		
		return result;
	}
	
	public String parseGoodsStatusMia(String itemPage){
		String result = "1";
		try {
			if(itemPage.contains("推荐")){
				result="0";
			}
		} catch (Exception e) {
			result = "-1";
		}
		return result;
	}

}
