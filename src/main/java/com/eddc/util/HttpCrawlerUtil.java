/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.util 
 * @author: jack.zhao   
 * @date: 2018年5月18日 上午9:34:11 
 */
package com.eddc.util;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.apache.http.NameValuePair;  
import org.apache.http.client.entity.UrlEncodedFormEntity;    
import org.apache.http.client.methods.HttpPost;   
import org.apache.http.client.utils.URIBuilder;  
import org.apache.http.message.BasicNameValuePair;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import java.io.*;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.ResourceBundle;
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：HttpCrawlerUtil   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年5月18日 上午9:34:11   
 * 修改人：jack.zhao   
 * 修改时间：2018年5月18日 上午9:34:11   
 * 修改备注：   
 * @version    
 *    
 */
@Component
public class HttpCrawlerUtil {
	private static Logger log = LoggerFactory.getLogger(HttpCrawlerUtil.class);
	private static String EMPTY_STR = "";
	private static String METOD="UTF-8";
	private static PoolingHttpClientConnectionManager cm;  
	private final static int MAX_TIMEOUT = 90000;
	private  void init() {  
		if (cm == null) {  
			cm = new PoolingHttpClientConnectionManager();  
			cm.setMaxTotal(500);// 整个连接池最大连接数  
			cm.setDefaultMaxPerRoute(50);// 每路由最大连接数，默认值是2  
		}  
	} 

	/** 
	 * 通过连接池获取HttpClient 
	 *  
	 * @return 
	 */  
	private  CloseableHttpClient getHttpClient() {  
		init();  
		return HttpClients.custom().setConnectionManager(cm).build();  
	}  

	public  String httpGetRequestData(Map<String, String> params) throws InterruptedException {  
		HttpGet httpGet = new HttpGet(params.get("url").toString());  
		return getResultData(httpGet,params);  
	} 

	public  String httpGetRequest(Map<String, String> params,Map<String, Object> headers)  
			throws URISyntaxException, InterruptedException {  
		URIBuilder ub = new URIBuilder();  
		ub.setPath(params.get("url").toString());  
		ArrayList<NameValuePair> pairs = covertParams2NVPS(headers);  
		ub.setParameters(pairs);  
		HttpGet httpGet = new HttpGet(ub.build());  
		return getResultData(httpGet,params);  
	} 

	public  String httpPostRequest(Map<String, String> params , Map<String, Object> headers)  
			throws UnsupportedEncodingException, InterruptedException {
		HttpPost httpPost = new HttpPost(params.get("url").toString());  
		//		for (Map.Entry<String, Object> param : headers.entrySet()) {  
		//			httpPost.addHeader(param.getKey(), String.valueOf(param.getValue()));  
		//		}  
		ArrayList<NameValuePair> pairs = covertParams2NVPS(headers);  
		httpPost.setEntity(new UrlEncodedFormEntity(pairs,METOD));  
		return getResultData(httpPost,params);  
	}  

	public  ArrayList<NameValuePair> covertParams2NVPS(Map<String, Object> params) {  
		ArrayList<NameValuePair> pairs = new ArrayList<NameValuePair>();  
		for (Map.Entry<String, Object> param : params.entrySet()) {  
			pairs.add(new BasicNameValuePair(param.getKey(), String.valueOf(param.getValue())));  
		}  

		return pairs;  
	}  

	/** 
	 * 处理Http请求 
	 *  
	 * @param request 
	 * @return 
	 * @throws InterruptedException 
	 */  
	@SuppressWarnings("static-access")
	public  String getResultData(HttpRequestBase request,Map<String,String>map) throws InterruptedException {  
		List<String> list=new ArrayList<String>();
		CloseableHttpClient httpClient = getHttpClient();
		RequestConfig defaultRequestConfig = RequestConfig.custom()
				.setSocketTimeout(MAX_TIMEOUT)
				.setConnectTimeout(MAX_TIMEOUT)
				.setConnectionRequestTimeout(MAX_TIMEOUT)
				.setCircularRedirectsAllowed(false).build();
		Thread.sleep(1000);  
		String count="0";
			try {
				String path=ResourceBundle.getBundle("docker").getString("IP_ADDRES");
				String ipData=getJsonObj(path);
				JSONObject currPriceJson = JSONObject.fromObject(ipData);
				map.put("ip", currPriceJson.getString("ip"));
				map.put("port",currPriceJson.getString("port"));
			} catch (Exception e) {
				log.info("IP请求失败");
		}
		if(list.size()>0){
			count="2";
		}
		map.put("status", count);
		if(!Validation.isEmpty(map.get("ip"))){
			HttpHost proxy =new HttpHost(map.get("ip"), Integer.parseInt(map.get("port")));
			defaultRequestConfig.custom().setProxy(proxy).build();	
		}
		request.setConfig(defaultRequestConfig);
		request.setHeader("Host",host(map.get("url").toString()));
		request.setHeader("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
		request.setHeader("Connection", "keep-alive");
		request.setHeader("Content-Type", "application/x-javascript;charset="+map.get("coding"));
		request.setHeader("Accept", "*/*");
		request.setHeader("Cache-Control", "max-age=0");
		request.setHeader("Pragma", "no-cache");
		request.setHeader("X-Requested-With", "XMLHttpRequest");
		request.setHeader("Accept-Encoding", "gzip, deflate, sdch, br");
		request.setHeader("Cache-Control", "no-cache");
		request.setHeader("Upgrade-Insecure-Requests", "1");
		request.setHeader("User-Agent", CrawlerAttribute());
		request.setHeader("Cookie", map.get("cookie")==null ? "" :map.get("cookie").toString());
		request.setHeader("Referer", map.get("urlRef")==null ? "":map.get("urlRef").toString());
		try {  
			CloseableHttpResponse response = httpClient.execute(request); 
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode ==HttpStatus.SC_OK) {
				HttpEntity entity = response.getEntity();  
				if (entity != null) {  
					String result = EntityUtils.toString(entity);  
					response.close();  
					return result;  
				}  	
			}else if (statusCode == HttpStatus.SC_MOVED_TEMPORARILY) {
				log.info("----->> HttpClient request fail status code 302 : " +map.get("url").toString());
			} else if (statusCode == HttpStatus.SC_NOT_FOUND) {
				log.info("----->> HttpClient request fail status code 404 : " +map.get("url").toString());
			} else {
				log.info("----->> HttpClient request fail status code " + statusCode + " : " +map.get("url").toString());
			}

		} catch (ClientProtocolException e) {  
			e.printStackTrace();  
		} catch (IOException e) {  
			e.printStackTrace();  
		} finally {  

		}  

		return EMPTY_STR;  
	} 


	//设置反爬虫设置user
	public static String CrawlerAttribute(){
		String userAgent=ResourceBundle.getBundle("parameter").getString("userAgent");
		String[] uaLength =userAgent.split("&&");
		Random index = new Random();
		String user_Agent = uaLength[Math.abs(index.nextInt()%804)];
		if("".equals(user_Agent)){
			user_Agent="Mozilla/5.0 (Windows; U; Windows NT 5.2) Gecko/2008070208 Firefox/3.0.1";	
		}
		return user_Agent;	
	}
	//获取host
	public static String host(String url){
		String HOST=null;
		String spitString[]=url.split("/");
		if(!Validation.isEmpty(url)){
			HOST=spitString[2];
		}
		return HOST;
	}

	//请求IP
	public static String getJsonObj(String src) {
		InputStreamReader reader = null;
		BufferedReader in = null;
		try { 
			URL url = new URL(src);
			URLConnection connection = url.openConnection();
			connection.setConnectTimeout(1000);
			reader = new InputStreamReader(connection.getInputStream(), "utf-8");
			in = new BufferedReader(reader);
			String line = null;        //每行内容
			int lineFlag = 0;        //标记: 判断有没有数据
			StringBuffer content = new StringBuffer();
			while ((line = in.readLine()) != null) {
				//String temp = line.substring(line.indexOf("{"), line.lastIndexOf("}") + 1);
				content.append(line.toString());
				lineFlag++;
			}
			if(lineFlag!=0 ){
				//JSONObject jsonObject = JSONObject.fromObject(content.toString());	
				return content.toString();
			}else{
				return null;
			}
		} catch (SocketTimeoutException e) {
			log.info("连接超时!!!"+src);
			return null;
		} catch (JSONException e) {
			log.info("网站响应不是json格式，无法转化成JSONObject!!!");
			return null;
		} catch (Exception e) {
			log.info("连接网址不对或读取流出现异常!!!");
			return null;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					log.info("关闭流出现异常!!!");
				}
			}
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					log.info("关闭流出现异常!!!");
				}
			}
		}
	}

}  



