package com.eddc.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.*;

public class JsoupUtil {
    private static final List<String> userAgentList = new ArrayList<>();
    static {
        userAgentList.add("Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50");
        userAgentList.add("Mozilla/5.0 (Windows; U; Windows NT 6.1; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50");
        userAgentList.add("Mozilla/5.0 (Windows NT 10.0; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0");
        userAgentList.add("Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727; .NET CLR 3.0.30729; .NET CLR 3.5.30729; InfoPath.3; rv:11.0) like Gecko");
        userAgentList.add("Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)");
        userAgentList.add("Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0)");
        userAgentList.add("Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)");
        userAgentList.add("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
        userAgentList.add("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0.1) Gecko/20100101 Firefox/4.0.1");
        userAgentList.add("Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1");
        userAgentList.add("Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; en) Presto/2.8.131 Version/11.11");
        userAgentList.add("Opera/9.80 (Windows NT 6.1; U; en) Presto/2.8.131 Version/11.11");
        userAgentList.add("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11");
        userAgentList.add("Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Maxthon 2.0)");
        userAgentList.add("Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; TencentTraveler 4.0)");
        userAgentList.add("Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)");
        userAgentList.add("Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; The World)");
        userAgentList.add("Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; SE 2.X MetaSr 1.0; SE 2.X MetaSr 1.0; .NET CLR 2.0.50727; SE 2.X MetaSr 1.0)");
        userAgentList.add("Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; 360SE)");
        userAgentList.add("Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Avant Browser)");
        userAgentList.add("Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)");
        userAgentList.add("Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5");
        userAgentList.add("Mozilla/5.0 (iPod; U; CPU iPhone OS 4_3_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5");
        userAgentList.add("Mozilla/5.0 (iPad; U; CPU OS 4_3_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5");
        userAgentList.add("MQQBrowser/26 Mozilla/5.0 (Linux; U; Android 2.3.7; zh-cn; MB200 Build/GRJ22; CyanogenMod-7) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1");
    }

    public static String getHtmlByJsoup(String url,String cookie,String refer){
        Map<String,String> headers = new HashMap<>();
        if(cookie!=null){
            headers.put("Cookie",cookie);
        }
        headers.put("referer",refer);
        headers.remove("");
        headers.remove(null);
        //随机选取user-agent
        Random ran = new Random();
        headers.put("user-agent",userAgentList.get(ran.nextInt(userAgentList.size())));
        headers.put("content-type","application/x-www-form-urlencoded");
        headers.put("accept","*/*");
        try {
            Document document = Jsoup.connect(url)
                    .ignoreContentType(true).timeout(10000)
                    .headers(headers).get();
            return document.body().text();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getKaolaPage(String url,String cookie){
        Random ran = new Random();
        Map<String,String> headers = new HashMap<>();
        headers.put("accept","*/*");
        headers.put("accept-encoding","gzip, deflate, br");
        headers.put("content-type","application/x-www-form-urlencoded");
        headers.put("cookie",cookie);
        headers.put("user-agent",userAgentList.get(ran.nextInt(userAgentList.size())));
        headers.put("refer","https://www.kaola.com");
        headers.put("x-requested-with","XMLHttpRequest");
        headers.put("x-accept-language-with","en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7,cy;q=0.6,zh-TW;q=0.5");
        headers.put("referer","https://m.kaola.com/");
       try{
        Document doc = Jsoup.connect(url).ignoreContentType(true)
                .timeout(10000).headers(headers).get();
        return doc.body().text();}
       catch (Exception e){
           e.printStackTrace();
       }
       return "";
    }
}
