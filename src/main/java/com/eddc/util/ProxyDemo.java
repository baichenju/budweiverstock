/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.util 
 * @author: jack.zhao   
 * @date: 2018年5月16日 下午3:53:42 
 */
package com.eddc.util;
import java.io.IOException;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：ProxyDemo   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年5月16日 下午3:53:42   
 * 修改人：jack.zhao   
 * 修改时间：2018年5月16日 下午3:53:42   
 * 修改备注：   
 * @version    
 *    
 */
public class ProxyDemo {
	// 代理隧道验证信息
	final static String ProxyUser = "HN54N0TZA3IO945D";
	final static String ProxyPass = "3524EC2B27DDDDF4";

	// 代理服务器
	final static String ProxyHost = "http-dyn.abuyun.com";
	final static Integer ProxyPort = 9020;
     
	public static String getUrlProxyContent(String url,String referer)
	{
		Authenticator.setDefault(new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication()
			{
				return new PasswordAuthentication(ProxyUser, ProxyPass.toCharArray());
			}
		});

		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ProxyHost, ProxyPort));

		try
		{
			// 此处自己处理异常、其他参数等
			Document doc = Jsoup.connect(url).referrer("ref").timeout(3000).proxy(proxy).get();

			if(doc != null) {
				System.out.println(doc.body().html());
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public static void main(String[] args) throws Exception
	{
		// 要访问的目标页面
		String targetUrl = "https://s.1688.com/selloffer/rpc_async_render.jsonp?templateConfigName=marketOfferresult&startIndex=0&keywords=%D6%C7%C4%DC%CA%D6%BB%B7&enableAsync=true&qrwRedirectEnabled=false&asyncCount=20&pageSize=60&n=y&_pageName_=market&offset=9&rpcflag=new&async=true&uniqfield=pic_tag_id&leftP4PIds=563704606183,545233233591,562761561196,566407864775,563616875375,568521298447,559998931835,557488861826&filterP4pIds=563704606183,545233233591,562761561196,566407864775,563616875375,568521298447,559998931835,557488861826&callback=jQuery17209832683445660071_1526612794295&beginPage=2";
        String ref="https://s.1688.com/selloffer/rpc_async_render.jsonp?templateConfigName=marketOfferresult&startIndex=0&keywords=%D6%C7%C4%DC%CA%D6%BB%B7&enableAsync=true&qrwRedirectEnabled=false&asyncCount=20&pageSize=60&n=y&_pageName_=market&offset=9&rpcflag=new&async=true&uniqfield=pic_tag_id&leftP4PIds=563704606183,545233233591,562761561196,566407864775,563616875375,568521298447,559998931835,557488861826&filterP4pIds=563704606183,545233233591,562761561196,566407864775,563616875375,568521298447,559998931835,557488861826&callback=jQuery17209832683445660071_1526612794295&beginPage=2";
		getUrlProxyContent(targetUrl,ref);
	}
}
