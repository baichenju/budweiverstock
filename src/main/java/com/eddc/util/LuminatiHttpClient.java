package com.eddc.util;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.json.JSONObject;

public class LuminatiHttpClient{
	public static final String username = "lum-customer-analyticservice-zone-ip50";
	public static final String password = "r4withwl7821";
	public static String zone = "zproxy.lum-superproxy.io";
	public static final int port = 22225;
	public static final int max_failures = 3;
	public static final int req_timeout = 15000;
	public String session_id;
	public HttpHost super_proxy;
	public CloseableHttpClient client;
	public String country;
	public int fail_count;
	public int n_req_for_exit_node;
	public Random rng;
	private static Logger logger=LoggerFactory.getLogger(LuminatiHttpClient.class);
	public static Registry<ConnectionSocketFactory> reg;
	public LuminatiHttpClient(String country){
		System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
		this.country = country;
		this.rng = new Random();
		switch_session_id();
	}
	public void switch_session_id() {
		this.session_id = Integer.toString(this.rng.nextInt(2147483647));
		this.n_req_for_exit_node = 0;

		InetAddress address = null;
		try {
			address = InetAddress.getByName("session-" + this.session_id + "." + zone);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		this.super_proxy = new HttpHost(address.getHostAddress(), 22225);
		logger.info("超级代理服务器====session-" + this.session_id + "." + zone);
		update_client();
	}

	public void update_client() {
		String login =username+ (this.country != null ? "-country-" + this.country : "") + "-session-" + this.session_id;
		logger.info("出口节点=====" + login);
		CredentialsProvider cred_provider = new BasicCredentialsProvider();
		cred_provider.setCredentials(new AuthScope(this.super_proxy), 
				new UsernamePasswordCredentials(login, password));
		RequestConfig.Builder builder = RequestConfig.custom();
		builder.setSocketTimeout(15000);
		RequestConfig config = builder
				.setConnectTimeout(15000)
				.setConnectionRequestTimeout(15000)
				.setRedirectsEnabled(false).build();
		SocketConfig.Builder sobuider = SocketConfig.custom().setSoTimeout(15000);
		PoolingHttpClientConnectionManager conn_mgr = 
				new PoolingHttpClientConnectionManager();
		conn_mgr.setDefaultMaxPerRoute(2147483647);
		conn_mgr.setMaxTotal(2147483647);
		conn_mgr.setDefaultSocketConfig(sobuider.build());
		this.client = HttpClients.custom()
				.setConnectionManager(conn_mgr)
				.setProxy(this.super_proxy)
				.setDefaultCredentialsProvider(cred_provider)
				.setDefaultRequestConfig(config)
				.build();
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, String> request(String url, String charset, String cookie, String ref) throws Exception {
		Map resultMap = new HashMap();
		CloseableHttpResponse response = null;
		try {
			HttpGet request = new HttpGet(url);
			request.setHeader("Host", getHost(url));
			request.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:40.0) Gecko/20100101 Firefox/40.0");
			request.setHeader("Accept", "*/*");
			request.setHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
			request.setHeader("Accept-Encoding", "gzip, deflate");
			if (!Validation.isEmpty(cookie).booleanValue()) {
				request.setHeader("Cookie", cookie);
			}
			if (!Validation.isEmpty(ref).booleanValue()) {
				request.setHeader("Referer", ref);
			}
			request.setHeader("Connection", "keep-alive");
			response = this.client.execute(request);
			handle_response(response);
			int i = response.getStatusLine().getStatusCode();
			if ((response != null) && 
					(!status_code_requires_exit_node_switch(
							i)))
			{
				if (i == 200) {
					BufferedReader br = new BufferedReader(
							new InputStreamReader(response.getEntity().getContent(), charset));
					StringBuffer sb = new StringBuffer();
					String line = "";
					while ((line = br.readLine()) != null) {
						sb.append(line);
					}
					br.close();
					String result = sb.toString();
					resultMap.put("result", result);
				} else if (String.valueOf(i).startsWith("30")){
					Header[] location = response.getHeaders("location");
					if (location.length > 0)
					{
						url = location[0].getValue();
						resultMap = request(url, charset, cookie, ref);
						return resultMap;
					}
				} else {
					logger.info(url + "===发生错误，状态码：" + i);
				}
			}
		} catch (Exception e){
			handle_response(null);
			e.printStackTrace();
		}finally {
			if (response != null)
			{
				response.close();
			}
			close();
		}
		return resultMap;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String post(String url, Map<String, Object> map, String cookie, String charset, String refer){
		String res = null;
		List params = new ArrayList();
		if (Validation.isNotEmpty(map).booleanValue()) {
			for (String key : map.keySet()) {
				String value = "";
				if ((map.get(key) instanceof Integer)) {
					value = (Integer)map.get(key)+"";
				} else if ((map.get(key) instanceof String)) {
					value = (String)map.get(key); } else {
						if (!(map.get(key) instanceof List)) continue;
						List<String>list = (List)map.get(key);
						for (String s : list) {
							params.add(new BasicNameValuePair(key, s));
						}

					}

				params.add(new BasicNameValuePair(key, value));
			}
		}
		HttpPost post = new HttpPost(url);
		try {
			post.setEntity(new UrlEncodedFormEntity(params, charset));

			if (!Validation.isEmpty(cookie).booleanValue()) {
				post.setHeader("Cookie", cookie);
			}
			if (!Validation.isEmpty(refer).booleanValue()) {
				post.setHeader("Referer", refer);
			}
			post.setHeader("Host", getHost(url));
			post.setHeader("X-Requested-With", "XMLHttpRequest");
			CloseableHttpResponse response = this.client.execute(post);
			HttpEntity entity = response.getEntity();
			res = EntityUtils.toString(entity);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(map.get("goodsId") + "POST:" + url + "is failed!" + res);
		} finally {
			post.abort();
		}
		return res;
	}

	public static String getHost(String url)
	{
		url = url.replaceAll("(http.*?://)", "");
		return url.substring(0, url.indexOf("/") == -1 ? url.length() : url.indexOf("/"));
	}

	public void handle_response(HttpResponse response) {
		if ((response != null) && 
				(!status_code_requires_exit_node_switch(
						response.getStatusLine().getStatusCode())))
		{
			this.n_req_for_exit_node += 1;
			this.fail_count = 0;
			return;
		}

		this.fail_count += 1;
	}

	public boolean status_code_requires_exit_node_switch(int code) {
		return (code == 403) || (code == 429) || (code == 502) || (code == 503);
	}

	public boolean have_good_super_proxy() {
		return (this.super_proxy != null) && (this.fail_count < 3);
	}

	public void close() {
		if (this.client != null) try {
			this.client.close(); } catch (IOException localIOException) {
			} this.client = null;
	}

	public static void main(String[] args) throws Exception{
		LuminatiHttpClient lmt = new LuminatiHttpClient(null);//cn/us
		Map<String, String> itemMessage=lmt.request("https://wqitem.jd.com/item/view2?datatype=1&sku=6514056&cgi_source=pingou","UTF-8",null,null);
		System.out.println(itemMessage.get("result").toString());
		String item= itemMessage.get("result").toString();
		item=item.toString().substring(item.toString().indexOf("{"), item.toString().lastIndexOf("}")+1);
		String messages=item.toString().replace("\\x3E","").replace("\\x3Ca", "").replace("\\x5C", "").replace("\\x27","").replace("\\x2", "").replace("\\x3CFa", "").replace("\\x3C\\x2Fa\\x3E", "").trim();
		JSONObject jsonObject = JSONObject.fromObject(messages);
		System.out.println(jsonObject.getJSONObject("AdvertCount").get("ad"));
		 String str = jsonObject.getJSONObject("AdvertCount").get("ad").toString(); 
		String message= StringHelper.nullToString(StringHelper.getResultByReg(str, "target=_blank([^~]+)"));
	    System.out.println(message);
	}
}
