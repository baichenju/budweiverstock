package com.eddc.util.okhttp;
import java.util.Map;

/**
 * @author jack.zhao
 * 封装数据请求
 */

public class HttpRequest {
    /**设置header参数*/
    private Map<String, String> header;
    /**请求接口参数*/
    private Map<String, Object> param;
    /**请求接口类型 get/post */
    private String httpMethod;
    /**是否启动在代理 */
    private boolean isProxy;
    /**参数类型  json/String */
    private String parameterType;
    /** 代理类型 luminati,云代理,public,狗子云*/
    private String ProxyType;
    /**商品链接*/
    private String url;
    /**代理Ip*/
    private String ip;
    /**代理 IP 端口号*/
    private int port;
    /**是否需要安全认证*/
    private boolean account;


   private  HttpRequest(Builder builder) {
        this.header = builder.header;
        this.param = builder.param;
        this.httpMethod = builder.httpMethod;
        this.isProxy = builder.isProxy;
        this.parameterType = builder.parameterType;
        this.ProxyType = builder.ProxyType;
        this.url = builder.url;
        this.ip=builder.ip;
        this.port=builder.port;
        this.account=builder.account;
    }
    public Map<String, String> getHeader() {
        return header;
    }
    public Map<String, Object> getParam() {
        return param;
    }
    public String getHttpMethod() {
        return httpMethod;
    }
    public boolean getIsProxy() {
        return isProxy;
    }
    public String  getParameterType(){ return parameterType; }
    public String getProxyType() {
        return ProxyType;
    }
    public String getUrl() {
        return url;
    }

    public String getIp() {
        return ip;
    }
    public int getPort() {
        return port;
    }
    public boolean getAccount() {
        return account;
    }
    public void method(){ System.out.println(httpMethod);
    }

    public static class Builder {
        private Map<String, String> header = null;
        private Map<String, Object> param = null;
        private String httpMethod = null;
        private boolean isProxy = true;
        private String parameterType = null;
        private String ProxyType = null;
        private String url = null;
        private String ip=null;
        private int port=0;
        private boolean account =true;

        public Builder() {};
        public Builder(String httpMethod){this.httpMethod=httpMethod;}; //可以不写

        public Builder setHeader(Map<String, String> header) {
            this.header = header;
            return this;
        }
        public Builder setParam(Map<String, Object> param) {
            this.param = param;
            return this;
        }
        public Builder setHttpMethod(String httpMethod) {
            this.httpMethod = httpMethod;
            return this;
        }
        public Builder setIsProxy(boolean isProxy) {
            this.isProxy = isProxy;
            return this;
        }
        public Builder account(boolean account) {
            this.account = account;
            return this;
        }
        public Builder setParameterType(String parameterType) {
            this.parameterType = parameterType;
            return this;
        }

        public Builder setProxyType(String proxyType) {
            this.ProxyType = proxyType;
            return this;
        }
        public Builder setUrl(String url) {
            this.url = url;
            return this;
        }
        public Builder setIp(String ip) {
            this.ip = ip;
            return this;
        }
        public Builder setPort(int port) {
            this.port = port;
            return this;
        }

        public HttpRequest builder(){
            return new HttpRequest(this);
        }
    }

}
