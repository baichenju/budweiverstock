package com.eddc.util.okhttp;
import java.io.IOException;
import java.net.*;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import com.alibaba.fastjson.JSONObject;
import com.eddc.util.Fields;
import com.eddc.util.http.HttpClientUtils;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import okhttp3.Authenticator;
import org.apache.commons.lang3.StringUtils;

/**
 * postman 数据请求
 *
 * @author jack.zhao
 */
@Slf4j
public class OkHttpClientUtil {
    @SuppressWarnings("deprecation")
    public String getOkHttpClient(HttpRequest httpRequest) {
        Random rng = new Random();
        String item = "";
        String ip = "";
        int port = 0;
        String userName = Fields.CRAWLER_USERNAME;
        String password = Fields.CRAWLER_PASSWORD;
        try {
            /**判断用户使用的哪一种代理方式**/
            if (StringUtils.isNotBlank(httpRequest.getProxyType())) {

            if (httpRequest.getProxyType().equalsIgnoreCase(Fields.CRAWLER_CLOUD_AGENT)) {
                userName = Fields.CRAWLER_USERNAME_CLOUD;
                password = Fields.CRAWLER_PASSWORD_CLOUD;
                String ipMessage = getRequestIp(Fields.CRAWLER_IP_ADDRES);
                JSONObject json = JSONObject.parseObject(ipMessage);
                ip = json.getString("ip");
                port = json.getInteger("port");
            } else if (httpRequest.getProxyType().equalsIgnoreCase(Fields.CRAWLER_LUMINATI)) {
                String sessionId = Integer.toString(rng.nextInt(Integer.MAX_VALUE));
                InetAddress address = null;
                address = InetAddress.getByName("session-" + sessionId + "." + Fields.LU_ZONE);
                ip = address.getHostAddress();
                port = 22225;
            } else if (httpRequest.getProxyType().equalsIgnoreCase(Fields.CRAWLER_PUBLIC)) {
                String ipMessage = getRequestIp(Fields.CRAWLER_IP_ADDRES_COUNTRY);
                JSONObject json = JSONObject.parseObject(ipMessage);
                ip = json.getString("ip");
                port = json.getInteger("port");
            } else if (httpRequest.getProxyType().equalsIgnoreCase(Fields.CRAWLER_DOG_AGENT)) {
                ip = httpRequest.getIp();
                port = httpRequest.getPort();
            }
        }

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            /**是否设置代理*/
            if (httpRequest.getIsProxy()) {
                String finalUserName = userName;
                String finalPassword = password;
                if (httpRequest.getAccount()) {
                    Authenticator proxyAuthenticator = new Authenticator() {
                        public Request authenticate(Route route, Response response) {
                            String credential = Credentials.basic(finalUserName, finalPassword);
                            return response.request().newBuilder()
                                    .header("Proxy-Authorization", credential)
                                    .build();
                        }
                    };
                    builder.proxyAuthenticator(proxyAuthenticator);
                }
                builder.proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ip, port)));
            }
            builder.connectTimeout(50, TimeUnit.SECONDS);
            /** 解决内存溢出问题*/
            builder.connectionPool(new ConnectionPool(20, 3, TimeUnit.SECONDS));
            builder.readTimeout(50, TimeUnit.SECONDS);
            builder.sslSocketFactory(new SsocketFactory());
            /**数据请求**/
            Request.Builder requests = new Request.Builder();

            /***判断请求方式 httpRequest*/
            if (httpRequest.getHttpMethod().equalsIgnoreCase(Fields.CRAWLER_POST)) {
                /**post json格式数据**/
                if (httpRequest.getParameterType().equalsIgnoreCase(Fields.CRAWLER_JSON)) {
                    MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
                    RequestBody body = RequestBody.create(mediaType, httpRequest.getParam().get("json").toString());
                    requests.method(httpRequest.getHttpMethod(), body);
                } else {
                    /**post  Form表单格式数据**/
                    MediaType mediaType = MediaType.parse("text/x-markdown; charset=utf-8");
                    FormBody.Builder builders = new FormBody.Builder();
                    for (String key : httpRequest.getParam().keySet()) {
                        /**追加表单信息*/
                        builders.add(key, httpRequest.getParam().get(key).toString());
                    }
                    requests.method(httpRequest.getHttpMethod(), builders.build());
                }
            } else {
                /***get请求数据*/
                requests.method(httpRequest.getHttpMethod(), null);
            }

            /**设置header**/
            if (httpRequest.getHeader() != null) {
                for (Map.Entry<String, String> entry : httpRequest.getHeader().entrySet()) {
                    requests.addHeader(entry.getKey(), entry.getValue());
                }
            }
            /**注入商品链接**/
            requests.url(httpRequest.getUrl());
            Response response = builder.build().newCall(requests.build()).execute();
            item = response.body().string();
        } catch (Exception e) {
            if (e instanceof SocketTimeoutException) {
                log.info("==>>Data request timeout..........<<==");
            }
            if (e instanceof ConnectException) {
                log.info("==>>Connection timed out: connect..........<<==");
            }
        }
        return item;
    }


    public String getRequestIp(String url) {
    	HttpClientUtils httpClientUtils =new HttpClientUtils();
        String message = "";
        int count = 0;
        while (StringUtils.isEmpty(message)) {
            count++;
            if (count > 10) {
                break;
            }
            try {
                message = httpClientUtils.doGetIP(url);
            } catch (Exception e) {
                log.error("==>>请求代理IP失败,当前是第：" + count + "次请求<<==", e);
            }
        }
        return message;

    }

}
