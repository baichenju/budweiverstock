/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.util 
 * @author: jack.zhao   
 * @date: 2018年3月14日 下午3:57:54 
 */
package com.eddc.util;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：BeanMapUtil   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年3月14日 下午3:57:54   
* 修改人：jack.zhao   
* 修改时间：2018年3月14日 下午3:57:54   
* 修改备注：   
* @version    
*    
*/
@Component
public class BeanMapUtil {
	@Autowired
	SqlSessionTemplate sqlSessionTemplate;
	@SuppressWarnings({"unchecked", "rawtypes"})
    public static Map convertBean2MapWithUnderscoreName(Object bean) throws Exception {
        Map returnMap = null;
        try {
            Class type = bean.getClass();
            returnMap = new HashMap();
            BeanInfo beanInfo = Introspector.getBeanInfo(type);
            PropertyDescriptor[] propertyDescriptors = beanInfo
                    .getPropertyDescriptors();
            for (int i = 0; i < propertyDescriptors.length; i++) {
                PropertyDescriptor descriptor = propertyDescriptors[i];
                String propertyName = descriptor.getName();
                if (!propertyName.equalsIgnoreCase("class")) {
                    Method readMethod = descriptor.getReadMethod();
                    Object result = readMethod.invoke(bean, new Object[0]);
                    //if(result != null && !result.equals("")){
                    	 returnMap.put( StringUtil.underscoreName(propertyName), result);
                   // }
                }
            } 
        } catch (Exception e) {
            // 解析错误时抛出服务器异常 记录日志
            throw new Exception("从bean转换为map时异常!", e);
        }
        return returnMap;
    }
	 @SuppressWarnings("rawtypes")
	public static String  convertBean2MapWithUnderscoreNameArray(Object bean) throws Exception {
	       StringBuffer buffer=new StringBuffer();
	        try {
	            Class type = bean.getClass();
	            BeanInfo beanInfo = Introspector.getBeanInfo(type);
	            PropertyDescriptor[] propertyDescriptors = beanInfo
	                    .getPropertyDescriptors();
	            for (int i = 0; i < propertyDescriptors.length; i++) {
	                PropertyDescriptor descriptor = propertyDescriptors[i];
	                String propertyName = descriptor.getName();
	                if (!propertyName.equalsIgnoreCase("class")) {
	                    Method readMethod = descriptor.getReadMethod();
	                    Object result = readMethod.invoke(bean, new Object[0]);
	                    if(propertyDescriptors.length==i+1){
	                    	buffer.append(result+"_"+StringUtil.underscoreName(propertyName)+";");	
	                    }else{
	                    	buffer.append(result+"_"+StringUtil.underscoreName(propertyName)+",");
	                    }
	                    
	                }
	            } 
	        } catch (Exception e) {
	            // 解析错误时抛出服务器异常 记录日志
	            throw new Exception("从bean转换为map时异常!", e);
	        }
	        return buffer.toString();
	    }
	 
	 @SuppressWarnings({ "rawtypes", "unused" })
	 public static String  convertBean(Object bean) throws Exception {
		 SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();  
		 SqlSessionFactory sessionFactory = factoryBean.getObject();  
		 java.sql.Connection conn = sessionFactory.openSession().getConnection();
		 DatabaseMetaData data = null;
		 try {
			 data = conn.getMetaData();
			 ResultSet rs = data.getColumns(null, "dbo", "craw_screenshot_info_history_2016", "%");
			 List<Map<String, Object>> list = new ArrayList<>();
			 StringBuilder columnNameBuilder = new StringBuilder();
			 while (rs.next()) {
				 String colName = rs.getString("COLUMN_NAME");
				 System.out.println("==============" + colName + "===============");
				 System.out.println("Column Name: " + colName);
				 System.out.println("Column Type: " + rs.getString("DATA_TYPE"));
				 System.out.println("Column Type Name: " + rs.getString("TYPE_NAME"));
				 System.out.println("Column Size: " + rs.getString("COLUMN_SIZE"));
				 System.out.println("Column Decimal Digits: " + rs.getString("DECIMAL_DIGITS"));
				 System.out.println("Column Nullable: " + rs.getString("NULLABLE"));
				 columnNameBuilder.append("\"").append(colName).append("\",\n");
			 }
			 System.out.println(columnNameBuilder.toString());
		 } catch (SQLException e) {
			 e.printStackTrace();
		 }
		return null;
	 }

}

