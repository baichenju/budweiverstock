package com.eddc.util;
import java.util.ResourceBundle;

import org.springframework.stereotype.Component;
/**
 * @author seamus
 * @date 2016年10月19日 下午4:33:54
 * @description 字段集合
 */
@Component
public class Fields { 

	//平台
	public static final String platform_INDEX_1688="index1688";
	public static final String platform_1688="1688";
	public static final String PLATFORM_ALIBABA="alibaba";
	public static final String PLATFORM_YHD = "yhd";
	public static final String PLATFORM_YHD_CN = "1号店";
	public static final String PLATFORM_SUNING = "suning";
	public static final String PLATFORM_SUNING_CN = "苏宁";
	public static final String PLATFORM_TMALL_EN = "tmall";
	public static final String PLATFORM_TMALL_CN = "天猫";
	public static final String PLATFORM_TMALL_GLOBAL_EN = "TmallGlobal";
	public static final String PLATFORM_TMALL_GLOBAL_CN = "天猫国际";
	public static final String PLATFORM_TAOBAO_EN = "taobao";
	public static final String PLATFORM_TAOBAO_CN = "淘宝";
	public static final String PLATFORM_JD = "jd";
	public static final String PLATFORM_JD_CN = "京东";
	public static final String PLATFORM_KAOLA = "kaola";
	public static final String PLATFORM_KAOLA_CN = "考拉";
	public static final String PLATFORM_JX = "jx";
	public static final String PLATFORM_ALIEXPRESS = "aliexpress";
	public static final String PLATFORM_VIP = "vip";
	public static final String PLATFORM_VIP_CN = "唯品会";
	public static final String PLATFORM_SILLA ="shilladfs";
	public static final String PLATFORM_JUMEI ="jumei";
	public static final String PLATFORM_JUMEI_CN ="聚美";
	public static final String PLATFORM_JUMEI_GLOBAL_EN ="jumeiglobal";
	public static final String PLATFORM_JUMEI_GLOBAL_CN ="聚美国际";
	public static final String PLATFORM_MiA_EN = "mia";
	public static final String PLATFORM_MiA_CN = "蜜芽";
	public static final String PLATFORM_MiA_GLOBAL_EN = "miaglobal";
	public static final String PLATFORM_MiA_GLOBAL_CN = "蜜芽国际";
	public static final String PLATFORM_LAZADA="lazada";
	public static final String PLATFORM_TMALL_BABY="天猫宝贝";
	public static final String PLATFORM_TMALL_INTERNATIONAL_BABY="天猫国际宝贝";
	public static final String PLATFORM_ZARA="Zara";
	public static final String PLATFORM_AMAZON="amazon";
	public static final String PLATFORM_AMAZON_UN="amazonusa";
	public static final String PLATFORM_AMAZON_CN="亚马逊";
	public static final String PLATFORM_AMAZONUSA_eN="亚马逊";
	public static final String PLATFORM_TAOBAO_ZC="taobaozc";//淘宝众筹
	public static final String PLATFORM_JD_ZC="jdzc";//京东众筹
	public static final String PLATFORM_PINDUODUO = "pdd";
	public static final String PLATFORM_KIDSWANT="kidswant";
	public static final String PLATFORM_XIOUHUI="xoh";
	public static final String PLATFORM_YUOUHUI="yoh";
	public static final String PLATFORM_SKS="sks";
	public static final String PLATFORM_HPK="hpk";
	

	public static final String PLATFORM_KIDSWANT_OFF="kidswantoff";
	public static final String PLATFORM_KIDSWANT_CN="孩子王";
	public static final String PLATFORM_PINDUODUO_CN = "拼多多";
	public static final String PLATFORM_YUNJI = "yunji";
	public static final String PLATFORM_YUNJI_CN = "云集";
	public static final String UNLIMITED="无限额";//京东众筹
	public static final String GBK="GBK";
	public static final String MESS_CODE="�����";
	public static final String EGOODSID="egoodsId";
	public static final String SEARCH_TMALL="尚天猫，就购了";
	public static final String NO_RESULT="h5搜索无结果";
	public static final String SUNING_SELF_SUPPORT="苏宁易购自营商品";
	public static final String SUNING_SUPERMARKET="苏宁超市";
	public static final String ACCOUNTID="28";
	public static final String ACCOUNTID_294="294";
	public static final String ZERO="0 0";
	public static final String ZERO_ONE="0%";
	public static final String UTF8="UTF-8";
	public static final String CRAWLER_DRIVER="driver";
	
	public static final String DEPOSIT="定金";//定金
	//客户端
	public static final String CLIENT_PC = "PC";
	public static final String CLIENT_MOBILE = "MOBILE";
	
	//IP地址
	public static final String IP_HOST= "121.46.231.180";
	public static final String IP_HOST_LOCAL= "192.168.0.35";
	public static final String IP= "192.168.0.51";
	public static final int IP_PORT = 24001;
	
	//商品上下架
	public static final String STATUS_ON = "1"; // 上架状态
	public static final String STATUS_OFF = "0";// 下架状态
	public static final String STATUS_ON_2 = "2";// 上架状态但是无货
	public static final String STATUS_ON_3 = "3";// 上架状态但是有货
	public static final String STATUS_OFF_3 = "3";// 下架
	public static final String STATUS_EXCEPTION = "-1";// 抓取异常状态
	public static final int STATUS_COUNT =0;// 商品状态
	public static final int STATUS_COUNT_1 =1;// 商品状态
	public static final int STATUS_COUNT_37 =37;// 商品状态
	public static final String STATUS_PRICE ="-1";// 商品价格
	public static final String STATUS_PRICE_DATA ="-1.00";// 商品价格
	
	public static final int STATUS_10 = 10;
	public static final int STATUS_11 = 11;
	public static final int STATUS_12 = 12;
	public static final int COUNT_20 = 20;
	public static final int COUNT_30 = 30;
	public static final int COUNT_10 = 10;
	public static final int SECOND =20;
	public static final String COUNT_0 ="0";
	public static final String COUNT_01 ="1";
	public static final String COUNT_2 ="2";
	public static final String COUNT_3 ="3";
	public static final String COUNT_4 ="4";
	public static final String COUNT_5 ="5";
	public static final String COUNT_6 ="6";
	public static final String COUNT_7 ="7";
	public static final String PRICENOPRICENO ="000000000";
	public static final String PRICENOPRICENO_2 ="0000000";
	//用户分类
	public static final String BUDWEISER="百威啤酒平台7";
	public static final String UNILVER_SHAMPOO="联合利华平台10洗发水";
	public static final String UNILVER_LAUNDRY="联合利华平台7洗衣液";
	public static final String UNILVER_LAUNDRY_15="联合利华平台15洗衣液";
    
	public static final String SHAMPOO="洗发水";
	public static final String BEER="百威啤酒";
	public static final String LAUNDRY="洗衣液";
	//状态联合利华
	public static final String STYPE_1="1";//爬去页面数据
	public static final String STYPE_2="2";//获取店铺
	public static final String STYPE_3="3";//手机端截图
	public static final String STYPE_4="4";//PC端截图
	public static final String STYPE_5="5";//搜索商品的价格
	public static final String STYPE_6="6";//商品的价格
	public static final String STYPE_7="7";//检查失败的item
	public static final String STYPE_8="8";//获取商品开始时间
    public static final String 	TABLE_CRAW_KEYWORDS_INF="craw_keywords_Info";
    public static final String 	TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH="Craw_keywords_temp_Info_forsearch";
    public static final String 	TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH_HISTORY="craw_keywords_temp_Info_forsearch_history";
    public static final String 	TABLE_CRAW_GOODS_INFO="craw_goods_Info";
    public static final String 	TABLE_CRAW_GOODS_PIC_INFO="Craw_goods_pic_Info";
    public static final String 	TABLE_CRAW_GOODS_PRICE_INFO="craw_goods_Price_Info";
    public static final String 	TABLE_CRAW_VENDOR_INFO="craw_goods_Vendor_Info";
    public static final String 	TABLE_CRAW_VENDOR_PRICE_INFO="craw_goods_Vendor_price_Info";
    public static final String 	TABLE_CRAW_GOODS_FIXED_INFO="Craw_goods_Fixed_Info";
    public static final String 	TABLE_CRAW_GOODS_TRANSLATE_INFO="craw_goods_translate_Info";
    public static final String 	CRAW_GOODS_TRANSLATE_INFO="craw_goods_crowdfunding_Info";//众筹
    public static final String 	CRAW_GOODS_TRANSLATE_INFO_HISTORY="craw_goods_crowdfunding_Info_history";//众筹
    public static final String 	TABLE_CRAW_GOODS_CROWDFUNDING_PRICE_INFO="Craw_goods_crowdfunding_price_Info";//众筹阶级表
    public static final String 	CRAW_ALIINDE_GOODSRANK_INFO="craw_aliindex_goodsrank_info";
    public static final String 	CRAW_ALIINDE_SAMEGOODS_INF="Craw_aliindex_samegoods_info";//阿里指数相似产品  
    public static final String 	CRAW_GOODS_COMMENT_INFO="craw_goods_comment_Info";
    public static final String 	CRAW_GOODS_ATTRIBUTE_INFO="Craw_goods_attribute_Info";//淘宝 天猫 属性
    public static final String SPELL_GROUP_PRICE="拼团价:";
    public static final String INFORMATION="基本信息";
	public static final String PROJECT_FUNDRAISING="预计回报发放时间:项目筹款成功后的DAY天内";
    public static final String ARRIVE="到";
	public static final String FINISH="结束";
	public static final String INTEGRAL="积分";
	public static final String SELFLESS_CONTRIBUTION="无私奉献";
	public static final String FAILURE="失败";
	public static final String SUCCESS="成功";
	public static final String RAISE_OF="众筹中";
	public static final String PREHEATING="预热中";
	public static final String RAISE_SUCCESS="众筹成功";
	public static final String PROJECT_SUCCESS="项目成功";
	public static final String PRESELL_MESSAGE="此商品正在参加预售";
	public static final String JD_TIMEOUT="请求数据超时失败";//检查超时失败的
	public static final String JD_DATA="请求数据失败";//检查数据失败的
	public static final String DATAILS="获取当前商品的item页面详情数据";
	public static final String JD_DATAILS_1="抓取京东商品的 商品服务 邮费详情 商品发货地 商品库存";
	public static final String PC_PRICE="抓取解析PC端价格";
	public static final String MOBILE_PRICE="抓取解析APP端价格";
	public static final String PRIOMOTION_DATA="抓取商品的促销";
	public static final String JUMEI_DATAILS="获取当前商品的item页面详情数据";
	public static final String INVENTORY="请求商品库存信息";
	public static final String DETAILS_DATA="抓取页面详情数据";
	public static final String SEARCH_JOB="com.eddc.job.Search_job";
	public static final String STATUS_TYPE="status_type";
	public static final String PLATFORM_SHOPID="shopid";
	public static final String DISCOUNT="折";
	public static final String SERIAL="编号";
	public static final String PURCHASE="限购";
	public static final String REDEMPTION="换购";
	public static final String YUAN_SELECT="元选";
	
	public static final String SEARCH="SEARCH";
	public static final String ALL="ALL";
	public static final String COMMENT="COMMENT";
	public static final String DISABLE="disable";
	public static final String ITMEPAGE="itemPage";
	public static final String PRICEINFOJSON="priceInfoJson";
	public static final String COUNT="count";
	public static final String KEYWORDNAME="keywordName";
	public static final String MSGTIP="请在此登录";
	public static final String JD_SUPERMARKET="京东超市";
	public static final String JING_MUST_REACH="京准达";
	public static final String SHOPNAME_NO="抱歉，没有找到与";
	public static final String CUSTOMERMONITOR="CustomerMonitor";
	public static final String  AMAZONSUA_PAGE="Sorry! We couldn't find that page. Try searching or go to Amazon's home page.";
	public static final String AMAZONCN_NO="没有找到任何与";
	public static final String ABOOVE_COUNT="https://cbu01.alicdn.com/cms/upload/2016/686/107/2701686_2002610654.png";
	public static final String BELOW_COUNT="https://cbu01.alicdn.com/cms/upload/2016/841/707/2707148_2002610654.png";
	public static final String PRICE="价格";
	public static final String ABOOVE="+";
	public static final String BELOW="-";
	public static final String CODE ="310100";
	public static final String EGOODS_ID ="egoodsid";
	public static final String KETWORD ="keyword";
	public static final String COUNT_1="";
	public static final String ACCOUNTID_33="33"; 
	public static final String ACCOUNTID_28="28"; 
	public static final String ACCOUNTID_35="35";
	public static final String ACCOUNTID_36="36";
	public static final String ACCOUNTID_37="33";
	public static final String ACCOUNTID_41="41";
	public static final String ACCOUNTID_42="42";
	public static final String ACCOUNTID_45="45";
	public static final String ACCOUNTID_47="47";
	public static final String ACCOUNTID_50="50";
	public static final String ACCOUNTID_51="51";
	public static final String ACCOUNTID_52="52";
	public static final String ACCOUNTID_53="53";
	public static final String ACCOUNTID_60="60";
	public static String data24="00";
	public static String data1="01";
	public static String data2="02";
	public static String data3="03";
	public static String data4="04";
	public static String data5="05";
	public static String data6="06";
	public static String data7="07";
	public static String data8="08";
	public static String data9="09";
	public static String data10="10";
	public static String data11="11";
	public static String data12="12";
	public static String data13="13";
	public static String data14="14";
	public static String data15="15";
	public static String data16="16";
	public static String data17="17";
	public static String data18="18";
	public static String data19="19";
	public static String data20="20";
	public static String data21="21";
	public static String data22="22";
	public static String data23="23";
	
	public static String PROMOTION_DATA_60="60";
	public static String PROMOTION_DATA_17="17";
	public static String PROMOTION_DATA_10="10";
	public static String PROMOTION_DATA_3="3";
	public static String PROMOTION_DATA_7="7";
	public static String PROMOTION_DATA_15="15";
	public static String PROMOTION_DATA_18="18";
	public static String IPPROXY="ipProxy";
	public static String LAOA="LaoA"; 
	public static String IPPROXY1="ipProxy1";
	public static String IPPROXY2="ipProxy2";
	public static String IPPROXY3="ipProxy3";
	public static String IPPROXY4="ipProxy4";
	public static String IPPROXY5="ipProxy5";
	public static String IPPROXY6="ipProxy6";
	public static String IPPROXY7="ipProxy7";
	public static String IPPROXY8="ipProxy8";
	public static String IPPROXY9="ipProxy9";

	public static String CHROMEBIN="webdriver.chrome.driver";
	public static String WEBDRIVERBIN="E:\\chromedriver.exe";	
	public static String user_Agent="Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543 Safari/419.3";
	public static String JD_URL_COMMENT="https://club.jd.com/comment/productCommentSummaries.action?my=pinglun&referenceIds=COMMENT&area=2_2817_51973_0";
	public static String TMALL_URL="https://detail.tmall.com/item.htm?id=";
	public static String TAOBAO_URL="https://item.taobao.com/item.htm?id=";
	public static String JD_URL_APP_INTERNATIONL="https://mitem.jd.hk/ware/view.action?wareId=";
	public static String JD_URL_APP="https://item.m.jd.com/product/";
	public static String JD_URL_PC="https://item.jd.com/";
	public static String JD_URL_PROMOTION="http://cd.jd.com/promotion/v2?skuId=";
	public static String JD_URL_IMPORTDUTY="https://c.3.cn/globalBuy_v2?skuId=EGOODSID&countryId=1&platformId=1";
	public static String JD_PC_PRICE="http://p.3.cn/prices/mgets?type=1&pduid="; 
	public static String SUNING_MOBILE_URL="http://pas.suning.com/nsitemsale_000000000";
	public static String SUNING_PC_URL="https://pas.suning.com/nspcsale_0_";
	public static String SUNING_PC_URL_1="https://product.suning.com/0000000000/";
	public static String SUNING_PC_URL_2="https://product.suning.com/";
	public static String SUNING_APP_URL_1="https://m.suning.com/product/0000000000";
	public static String SUNING_COMMENT="https://review.suning.com/mobile/getClusterReviewCnt/general--0000000EGOODSID-0000000000-----.htm";
	public static String SUNING_COMMENT_URL="https://review.suning.com/mobile/getClusterCmmdtyLabels/general--0000000EGOODSID-shopid-----.htm";
	public static String SUNING_STORE_URL="https://product.suning.com/pds-web/ajax/getApiRemoteMap_shopid_.html?callback=shopScoreCallback";
	public static String SUNING_APP_URL_PRICE="http://pas.suning.com/nsendetail_0000000";
	public static String SUNING_PC_PROMOTION_2="https://icps.suning.com/icps-web/queryExtendedGift/000000000";
	public static String SUNING_PC_PROMOTION="https://icps.suning.com/icps-web/queryExtendedGift/";  
	public static String TMALL_APP="http://hws.m.taobao.com/cache/wdetail/5.0/?id=";
	public static String TMALL_APP_2="https://acs.m.taobao.com/h5/mtop.taobao.detail.getdetail/6.0/?data=%7B%22itemNumId%22%3A%22EGOODSID%22%7D&qq-pf-to=pcqq.group&areaId=CODE";
	public static String TMALL_APP_4="https://h5api.m.taobao.com/h5/mtop.taobao.detail.getdetail/6.0/?jsv=2.4.8&appKey=12574478&sign=7f0e8635a9727ea0ac1313d2b99c632b&api=mtop.taobao.detail.getdetail&v=6.0&dataType=jsonp&ttid=2017%40taobao_h5_6.6.0&AntiCreep=true&data=%7B%22itemNumId%22%3A%22EGOODSID%22%7D&areaId=CODE";
	public static String TMALL_APP_3="https://h5api.m.taobao.com/h5/mtop.taobao.detail.getdetail/6.0/?jsv=2.4.8&appKey=12574478&t=1517841673123&sign=b2044e6a0d3a7a3035b2833e5b3a5b02&api=mtop.taobao.detail.getdetail&v=6.0&dataType=jsonp&ttid=2017%40taobao_h5_6.6.0&AntiCreep=true&type=jsonp&data=%7B%22itemNumId%22%3A%22EGOODSID%22%7D";
	public static String TMALL_APP_5="https://h5api.m.taobao.com/h5/mtop.taobao.detail.getdetail/6.0/?jsv=2.5.1&appKey=12574478&t=TIME_KEY&sign=1303b8bf8a5bc76a8eac71d3b0a1eb0b&api=mtop.taobao.detail.getdetail&v=6.0&isSec=0&ecode=0&AntiFlood=true&AntiCreep=true&H5Request=true&ttid=2018%40taobao_h5_9.9.9&type=jsonp&dataType=jsonp&data=%7B%22id%22%3A%22EGOODSID%22%2C%22itemNumId%22%3A%22EGOODSID%22%2C%22itemId%22%3A%22EGOODSID%22%2C%22exParams%22%3A%22%7B%5C%22id%5C%22%3A%5C%22EGOODSID%5C%22%7D%22%2C%22detail_v%22%3A%228.0.0%22%2C%22utdid%22%3A%221%22%7D";
	public static String TMALL_APP_6="https://h5api.m.taobao.com/h5/mtop.taobao.detail.getdetail/6.0/?jsv=2.4.8&appKey=12574478&api=mtop.taobao.detail.getdetail&v=6.0&dataType=jsonp&ttid=2017%40taobao_h5_6.6.0&AntiCreep=true&type=jsonp&data=%7B%22itemNumId%22%3A%22EGOODSID%22%7D";
	public static String TMALL_COMMENT_URL="https://rate.taobao.com/detailCommon.htm?auctionNumId=EGOODSID";
	public static String TAOBAOZC_URL="https://izhongchou.taobao.com/dream/ajax/getProjectForDetail.htm?id=";
	public static String TMALL_SEARCH="https://s.taobao.com/search?q=";
	public static String YHD_URL_APP="http://item.m.yhd.com/";
	public static String YHD_URL_PC="http://item.yhd.com/";
	public static String MEN_URL="https://api.empathybroker.com/search/v1/query/zara/search?jsonCallback=jQuery111207381440113544226_1510817130631&o=json&m=24&q=%E7%94%B7%E5%A3%AB&filter=%7B!tag%3DrootFilter%7DrootCategories_filter_21551_11716%3A%22%E7%94%B7%E5%A3%AB%22&scope=default&t=*&lang=zh_CN&store=11716&catalogue=21551&warehouse=14551&start=0&rows=24&session=5cee9726-9358-4f26-a5f2-a1183cd30fae&user=3323ac02-b022-4126-8c5c-1eb8fbac393c&_=1510817130639";
	public static String MS_URL="https://api.empathybroker.com/search/v1/query/zara/search?jsonCallback=jQuery111202417245301579476_1510824464494&o=json&m=24&q=%E5%A5%B3%E5%A3%AB&filter=%7B!tag%3DrootFilter%7DrootCategories_filter_21551_11716%3A%22%E5%A5%B3%E5%A3%AB%22&scope=default&t=*&lang=zh_CN&store=11716&catalogue=21551&warehouse=14551&start=0&rows=24&session=7dbe8156-e1a1-4e09-92ab-78ca366d9aff&user=3323ac02-b022-4126-8c5c-1eb8fbac393c&_=1510824464500";
	public static String CHILDREN_URL="https://api.empathybroker.com/search/v1/query/zara/search?jsonCallback=jQuery111209474184974982469_1511401586440&o=json&m=24&q=%E5%84%BF%E7%AB%A5&filter=%7B!tag%3DrootFilter%7DrootCategories_filter_21551_11716%3A%22%E5%84%BF%E7%AB%A5%22&scope=default&t=*&lang=zh_CN&store=11716&catalogue=21551&warehouse=14551&start=0&rows=24&session=e8b23878-81da-47da-ad84-9690be0e6ac6&user=eb94b540-5e62-4abb-b537-ff6171f7f940&_=1511401586449";
	public static String TRF_URL="https://api.empathybroker.com/search/v1/query/zara/search?jsonCallback=jQuery111207190916814062602_1511401996190&o=json&m=24&q=TRF&filter=%7B!tag%3DrootFilter%7DrootCategories_filter_21551_11716%3A%22TRF%22&scope=default&t=*&lang=zh_CN&store=11716&catalogue=21551&warehouse=14551&start=0&rows=24&session=e8b23878-81da-47da-ad84-9690be0e6ac6&user=eb94b540-5e62-4abb-b537-ff6171f7f940&_=1511401996204";
	public static String PROMOTION="http://item.yhd.com/api/item/ajaxGetPromoInfo.do?params.skuId=";
	public static String APP_PRICE="http://item.m.yhd.com/api/item/getPrices.do?callback=jsonp1&params.area=2_2817_51973_0&params.skuIds=";
	public static String APP_PROMOTION="http://item.m.yhd.com/api/item/ajaxGetPromoInfo.do?params.skuId=";
	public static String EXTRAPARAM="http://item.yhd.com/api/item/getPrices.do?callback=jQuery1113009640222054782965_1513652283363&params.area=2_2817_51973_0&params.skuIds=";
	public static String JD_APP_INTERNATIONAL="https://mitem.jd.hk/ware/view.action?wareId=1931562";
	public static String JD_APP_JDHK="https://mitem.jd.hk/ware/view.action?wareId=1941477";
	public static String JD_APP_JDHK_294="https://mitem.jd.hk/ware/view.action?wareId=3948467";
	public static String SUNING_PC_IMAGE="//image1.suning.cn/uimg/b2c/newcatentries/0000000000-000000000";
	public static String SUNING_PC_IMAGE_2="http://imgservice.suning.cn";
	public static String VIP_PC_URL="https://detail.vip.com/detail-";
	public static String VIP_PC_URL_JSON="https://detail.vip.com/v2/mapi?_path=rest%2Fshop%2Fgoods%2FvendorSkuList%2Fv3&mid=MID&brandid=BRANDID";
	public static String VIP_APP_URL="https://m.vip.com/product-EGOODSID.html";
	public static String MIYA_APP_URL="https://m.mia.com/item-";
	public static String MIYA_PC_URL="https://www.mia.com/item-";
	public static String MIYA_APP_GLOBAL_URL="https://m.miyabaobei.hk/item-";
	public static String MIYA_PC_GLOBAL_URL="https://www.miyabaobei.hk/item-";
	public static String MIYA_PC_PROMOTION="https://www.mia.com/instant/item/promotion/";
	public static String MIYA_APP_PROMOTION="https://m.mia.com/instant/item/promotion/";
	//public static String KAOLA_APP_URL="https://m.kaola.com/product/";
	public static String KAOLA_APP_URL="https://m-goods.kaola.com/product/";
	public static String KAOLA_PC_URL="https://www.kaola.com/product/";
	public static String KAOLA_PC_URL_PRICE="https://www.kaola.com/product/ajax/queryPromotionNew.html?goodsId=";
	public static String KAOLA_PC_URL_PRICE_NEW="https://goods.kaola.com/product/getPcGoodsDetailDynamic.json?provinceCode=310000&cityCode=310100&districtCode=310101&goodsId=EGOODSID&categoryId=CATEGORYID";
	public static String KAOLA_APP_URL_Promotion="https://m.kaola.com/product/queryPromotionNew.html?t=";
	public static String KAOLA_APP_URL_PROMOTION_NEW="https://m-goods.kaola.com/product/getWapGoodsDetailDynamic.json?goodsId=EGOODSID&provinceCode=310000&cityCode=310100&districtCode=310101";
	public static String KAOLA_PC_URL_Promotion="https://www.kaola.com/product/ajax/queryPromotionNew.html?";//goodsId=1377154&categoryId=626&t=1511840564830	
	public static String JUMEI_APP_URL="http://m.jumei.com/product/ajaxDynamicDetail?item_id=";//2232391zc&type=jumei_deal";
	public static String JUMEI_APP_URL_2="https://m.jumei.com/product/ajaxDynamicDetail?item_id=";
	
	public static String JUMEI_APP_GLOBAL_URL="http://h5.jumei.com/product/ajaxDynamicDetail?item_id=";//151190&type=global_mall
	public static String JUMEI_APP_GLOBAL_URL_2="http://h5.jumei.com/product/ajaxDynamicDetail?item_id=";//=ht171129p818510t2&type=global_deal

	public static String JUMEI_PC_URL="http://item.jumei.com/";
	public static String JUMEI_PC_GLOBAL_URL="http://item.jumeiglobal.com/";
	
	public static String JUMEI_APP_URL_Promotion="http://h5.jumei.com/promo/sales?item_id=";//普通产品
	public static String JUMEI_APP_URL_Promotion_2="http://h5.jumei.com/promo/sales?item_id=";//特卖产品 df1711275602636p3443570&type=jumei_pop
	public static String JUMEI_APP_URL_GLOBAL_Promotion_3="http://h5.jumei.com/promo/sales?item_id=";//海外 72886&type=global_pop_mall
	public static String JUMEI_APP_URL_GLOBAL_Promotion_4="http://h5.jumei.com/promo/sales?item_id=";//海外极速免税店  ht1488874340p3403090&type=global_pop
	
	public static String JUMEI_PC_URL_Promotion_2="http://www.jumei.com/i/static/getDealInfoByHashId?hash_id=";//特卖产品 df1711285608660p2353819&_=1512098735327
	public static String JUMEI_PC_URL_Promotion_1="http://www.jumei.com/i/Static/getProductSameInfoByProductID?product_id=";//普通商品 1052632&brand_id=690&price=89&site=sh&callback=jQuery1112017022424268169556_1512110686059&_=1512110686060
	public static String JUMEI_PC_URL_GLOBAL_Promotion_3="http://item.jumeiglobal.com/";//海外 ht1488874340p3403090&callback=static_callback ht171201p1934521t2.html
	public static String JUMEI_PC_URL_GLOBAL_URL="http://www.jumeiglobal.com/ajax_new/MallInfo?mall_id=";//海外 138174&callback=static_callback
	public static String JUMEI_PC_URL_GLOBAL_Promotion_4="http://item.jumeiglobal.com/";//海外极速免税店 73426.html 促销
	public static String JUMEI_APP_GoodsName="http://m.jumei.com/product/ajaxStaticDetail?item_id=";//商品名称
	public static String GUOMEI_APP_URL="https://item.m.gome.com.cn/product-";//国美appurl 9140046570-1130051648.html
	public static String GUOMEI_PC_URL="https://item.gome.com.cn/";//国美PCurl 9140055865-1130523959.html
	public static String GUOMEI_APP_STOCK="https://item.m.gome.com.cn/product/stock?goodsNo=";//手机价格
	public static String GUOMEI_APP_PROMOTION="https://item.m.gome.com.cn/product/ajaxProduct?goodsNo=";//手机促销
	public static String GUOMEI_PC_PRICE="https://ss.gome.com.cn/item/v1/d/m/store/unite/";//pc价格
	public static String GUOMEI_PC_PROMOTION="https://ss.gome.com.cn/item/v1/d/reserve/p/detail/";//PC促销
	public static String PEPSI_KEYWORDS_URL="https://guigeshipin.tmall.com/category-";//搜索商品url
	public static String PEPSI_KEYWORDS_DETAILS_URL="/i/asynSearch.htm?_ksTS=1521006397246_205&callback=jsonp206&mid=w-16557750383-0&wid=16557750383";//搜索商品url  &path=/category-1229558783.htm&catId=1229558783&scid=1229558783
	public static String PEPSI_AMAZON_URL="https://www.amazon.cn/dp/";
	public static String PEPSI_AMAZON_EN_URL="https://www.amazon.com/2200mAh-21-6v-Battery-dc16-battery/dp/";
	public static String TAOBAO_SEARCH_URL="https://s.taobao.com/search?q=SEARCH&imgfile=&js=1&style=grid&tab=all&ie=utf8&bcoffset=0&p4ppushleft=%2C44&stats_click=search_radio_all%3A1&initiative_id=staobaoz_20180321";//淘宝搜索
	public static String TMALL_SEARCH_URL="https://s.taobao.com/search?q=SEARCH&imgfile=&commend=all&ssid=s5-e&search_type=item&sourceId=tb.index&spm=a21bo.50862.201856-taobao-item.1&ie=utf8&initiative_id=tbindexz_20170803&fs=1&filter_tianmao=tmall";//淘宝搜索
	public static String JD_SEARCH_URL="https://search.jd.com/s_new.php?keyword=KEYWORD&enc=utf-8&qrst=1&rt=1&stop=1&vt=2&wq=KEYWORD&page=PAGE&s=SUM";//PC京东搜索2次请求
	public static String JD_SEARCH_REF_URL="https://search.jd.com/Search?keyword=";//PC京东搜索
	public static String JD_SEARCH_REF_URL_APP="https://so.m.jd.com/ware/searchList.action?_format_=json&sort=&page=PAGE&categoryId=CATEGORYID&c1=C1&c2=C2";//APP京东搜索
	public static String JD_SEARCH_REF_URL_APP_KEYWORD="https://so.m.jd.com/ware/searchList.action?_format_=json&page=PAGE&keyword=KEYWORD";//关键字搜索
	public static String YHD_SEARCH_REF_URL_APP="https://search.m.yhd.com/search/A/k/P1-s1-mbname-b-pr-a-d0-f0b-color-size?virtualflag=5&req.needMispellKw=0&viewType=0&req.ajaxFlag=1";//yhd制定类目
	public static String YHD_SEARCH_REF_URL_APP_KEYWORD="https://search.m.yhd.com/search/KEYWORD/P1-s1-si1-t1?req.ajaxFlag=1";//yhd搜索
	public static String YHD_SEARCH_REF_URL_APP_KEYWORD_PC="http://search.yhd.com/searchPage/c0-0-0/mbname-b/a-s1-v4-PAGE-price-d0-f0b-m1-rt0-pid-mid0-color-size-KSEARCH/?callback=jQuery111305730673929612584_1522300621952&&isLargeImg=0&fashionCateType=2&_=1522300621967";//PC端搜索
	public static String JD_SEARCH_REF_URL_APP_KEYWORD_PC_LIST="http://list.jd.com/list.html?cat=1320,5019,5022&ev=exbrand_207961&page=1&delivery=1&sort=sort_totalsales15_desc&trans=1&JL=4_10_0#J_main";
	public static String JD_INVENTORY_HK="https://mitem.jd.hk/ware/thirdAddress.json?checkParam=ailLIITIP&wareId=";
	public static String JD_INVENTORY_DOMESTIC="https://item.m.jd.com/ware/thirdAddress.json?checkParam=ailLIITIP&wareId=";
	public static String JD_INVENTORY_WQITEM="https://wqitem.jd.com/item/view2?datatype=1&sku=EGOODSID&cgi_source=pingou";//&area=AREA
	public static String JD_COMMENT_URL="https://wq.jd.com/commodity/comment/getcommentlist?sku=EGOODSID&sorttype=5&page=1&pagesize=10&sceneval=2&skucomment=1&score=0";
	public static String JD_STORE_URL="https://wq.jd.com/mshop/BatchGetShopInfoByVenderId?venderIds=shopid";
	
	public static String YHD_SEARCH_PC_REF="https://search.yhd.com/c0-0-1004115/MBNAME/a-s1-v4-p1-price-d0-f0b-m1-rt0-pid-mid0-color-size-k/";
	public static String SUNING_SEARCH_PC_REF="https://search.suning.com/emall/searchProductList.do?keyword=KEYWORD&ci=0&pg=01&cp=PAGE&il=0&st=0&iy=0&isDoufu=1&n=1&sc=0&sesab=ABAAABAA&id=IDENTIFYING&cc=CITY";
	public static String YHD_SEARCH_REF_URL_CATEGORY_KEYWORD_PC="http://search.yhd.com/searchPage/CATEGORY/mbname-CODE/a-s1-v4-PA-price-d0-f0b-m1-rt0-pid-mid0-color-size-k/?callback=jQuery1113020640134733684823_1523325886640&isGetMoreProducts=1&moreProductsDefaultTemplate=0&isLargeImg=0&moreProductsFashionCateType=2&nextAdIndex=0&nextImageAdIndex=0&adProductIdListStr=&fashionCateType=2&firstPgAdSize=0&needMispellKw=&onlySearchKeyword=0&_=1523325886642";//PC品类端搜索
	public static String AMAZON_URL_KEYWORD="https://www.amazon.com/s/ref=sr_pg_PAGE?keywords=KEYWORD&page=PAGE";
	public static String AMAZON_URL_KEYWORD_CN="https://www.amazon.cn/s/ref=sr_pg_PAGE?keywords=KEYWORD&page=PAGE&ie=UTF8";
	public static String AMAZON_URL_KEYWORD_EN="https://www.amazon.com/b/ref=sr_aj?node=7939901011&ajr=0&page=PAGE";
	public static String AMAZON_URL_KEYWORD_EN_URL="https://www.amazon.com";
	public static String PLATFORM_1688_PC_SEARCH_URL="https://s.1688.com/selloffer/rpc_async_render.jsonp?keywords=KEYWORD&n=y&beginPage=PAGE&async=true&enableAsync=true&asyncCount=20&startIndex=STARTINDEX&_=1437173453456&rpcflag=new&_serviceId_=marketOfferResultViewService&_template_=controls%2Fnew_template%2Fproducts%2Fmarketoffersearch%2Fofferresult%2Fpkg-a%2Fviews%2Fofferresult.vm&showMySearchUrl=true&salesSortType=quantity_sum_month&token=2321131414&callback=jQuery18308374414248391986_1437173297221";
	public static String PLATFORM_1688_REF="https://p4psearch.1688.com/p4p114/p4psearch/offer2.htm?keywords=KEYWORD&button_click=filtbar&earseDirect=false&n=y&cosite=baidujj&location=landing_t4&trackid=885688110390753370637483&bt=&spm=a312h.7841636.1998813771.dsearch_1998813775_1#beginPage=PAGE&offset=0";
	public static String PLATFORM_1688_PC_URL="https://detail.1688.com/offer/EGOODSID.html";
	public static String PLATFORM_TMALL_SEARCH_URL="https://s.taobao.com/search?q=SEARCH&imgfile=&js=1&style=grid&stats_click=search_radio_all%3A1&initiative_id=staobaoz_20180418&ie=utf8&bcoffset=0&ntoffset=0&p4ppushleft=%2C44&fs=1&filter_tianmao=tmall&s=PAGE";//天猫
	public static String PLATFORM_1688_SERCH="https://www.1688.com/chanpin/-.asyn?smnk=KEYWORD&at_iframe=1&preview=&n=y&previewcontrol=&previewlayout=&beginPage=PAGE&async=true&renderTarget=newShopWindowMainList2&renderModule=chanpinShopWindowOfferResult&showStyle=shopwindow&p4pid=1525398890383000000962&n=y&bt=201702_2-201701_2&beginPage=PAGE&loadCount=1";
	public static String PLATFORM_TAOBA_SEARCH_URL="https://s.taobao.com/search?q=SEARCH&imgfile=&js=1&style=grid&stats_click=search_radio_all%3A1&initiative_id=staobaoz_20180420&ie=utf8&bcoffset=3&ntoffset=3&p4ppushleft=1%2C48&s=PAGE";//天猫
	public static String TMALL_GOODS_TIME_URL="https://rate.tmall.com/list_detail_rate.htm?itemId=ITEMID&spuId=0&sellerId=SELLERID&order=1&currentPage=PAGE&append=0&content=1&tagId=&posi=&picture=&isg=BFJSBIwPXDAl9KDMHUiLXdL4ohj0y1Ufx7Js2hyqaoWYL_EpBPBcDSNtm4sTX86V&needFold=0&_ksTS=1524473923827_1097&callback=jsonp1098";
	public static String JD_GOODS_TIME_URL_PC="https://sclub.jd.com/comment/productPageComments.action?productId=ITEMID&score=0&sortType=6&page=PAGE&pageSize=10&isShadowSku=0&fold=1";
	public static String JD_GOODS_TIME_URL_APP="https://item.m.jd.com/newComments/newCommentsDetail.json?wareId=ITEMID&offset=PAGE&num=10&checkParam=LUIPPTP&category=CATEGORY&isUseMobile=true&isCurrentSku=false&type=5";
	public static String AMAZONUSA_GOODS_TIME_URL_PC="https://www.amazon.com/product-reviews/ITEMID/ref=cm_cr_getr_d_paging_btm_PAGE?ie=UTF8&reviewerType=all_reviews&pageNumber=PAGE";
	
	public static String ALI_INDEX_TRADING_WEEKS="http://index.1688.com/alizs/offer/rank.json?cat=CAT&dim=trade&period=week";//阿里排行》产品排行榜单》维度 ，交易/统计周期 最近7天	
	public static String ALI_INDEX_TRADING_MONTH="http://index.1688.com/alizs/offer/rank.json?cat=CAT&dim=trade&period=month";//阿里排行》产品排行榜单》维度 ，交易/统计周期 最近30天	
	public static String ALI_INDEX_TRAFFIC_WEEKS="http://index.1688.com/alizs/offer/rank.json?cat=CAT&dim=flow&period=week";//阿里排行》产品排行榜单》维度 ，流量/统计周期 最近7天	
	public static String ALI_INDEX_TRAFFIC_MONTH="http://index.1688.com/alizs/offer/rank.json?cat=CAT&dim=flow&period=month";//阿里排行》产品排行榜单》维度 ，流量/统计周期 最近30天	
	public static String ALI_INDEX_SIMILAR_PRODUCTS="https://open-s.1688.com/openservice/graphSearchViewService?fromOfferId=EGOODSID&tab=similarDesign&showStyle=shopwindow&pageSize=2000&beginPage=1&appName=searchweb&appKey=APPKEY=&";//阿里排行》产品排行榜单》商品相似产品
	public static String REF_PRODUCT="https://m.vip.com/recommend-product-EGOODSID.html?prev=product";
	public static String PLATFORM_APP_ROOL="https://wq.jd.com/mjgj/fans/queryusegetcoupon?callback=getCouponListCBA&platform=3&cid=CID&sku=SKUID&popId=POPID";
	public static String PLATFORM_APP_STORE="https://cd.jd.com/stock?skuId=EGOODSID&venderId=VENDERID&cat=CAT&area=AREA&extraParam=%7B%22originid%22%3A%223%22%7D";
	public static String PLATFORM_PC_ROOL="https://wq.jd.com/mjgj/fans/queryusegetcoupon?callback=getCouponListCBA&platform=3&cid=CID&sku=SKUID&popId=POPID";
	public static String IMAGE_URL="https://m.360buyimg.com/mobilecms/s750x750_";
	public  static  final  String JX_DETAIL_UTL_APP ="https://m.jiuxian.com/m_v1/goods/view/EGOODSID";
	public  static  final  String  JX_DETAIL_UTL_PROMOTION="https://m.jiuxian.com/m_v1/goods/detailPromo/EGOODSID";
	public  static  final  String  JX_DETAIL_UTL_COMMENT_NUM="https://m.jiuxian.com/m_v1/goods/selectIndexEvaluateNew/EGOODSID";
	public  static  final  String  JX_DETAIL_UTL_PC="http://www.jiuxian.com/goods-EGOODSID.html?";
	public  static  final  String  VIP_INVENTORY="http://stock.vip.com/detail/?callback=stock_detail&merchandiseId=EGOODSID&is_old=0&areaId=931101101101";
	public  static  final  String  VIP_ITEM_APP_URL="https://mapi.vip.com/vips-mobile/rest/shopping/wap/product/detail/v5?app_name=shop_wap&app_version=4.0&api_key=8cec5243ade04ed3a02c5972bcda0d3f&mobile_platform=2&source_app=yd_wap&warehouse=VIP_SH&fdc_area_id=931101106107&province_id=103101&mars_cid=1574756288248_97164cc61d6762127c300788af492401&mobile_channel=mobiles-%7C%7C&standby_id=www&brandId=BRANDID&productId=EGOODSID";
	public  static  final  String  VIP_IMAGE_URL="https://h2a.appsimg.com/a.appsimg.com";
	public  static  final  String  PINDUODUO_IMAGE_URL="http://mobile.yangkeduo.com/goods.html?goods_id=EDOODSID";
	public  static  final  String  HAIPAIKE_IMAGE_URL="https://detail.hipac.cn/item.html?itemId=EDOODSID";
//	public  static  final  String  PINDUODUO_IMAGE_URL="http://mobile.yangkeduo.com/goods.html?goods_id=EDOODSID&page_id=10014_1570854014997_C3vmZonFt9&is_back=1&refer_page_name=login&refer_page_id=10169_1570860616188_t28M3JcXqx&refer_page_sn=10169";
public static final String JUHUASUAN_IMAGE_URL = "https://dskip.ju.taobao.com/detail/json/item_dynamic.htm?item_id=EDOODSID";
    public static final String YUNJI_IMAGE_URL_1 = "http://item.yunjiglobal.com/yunjiitemapp/buyer/item/getItemInfo.json?appCont=0&itemId=EDOODSID";
    public static final String YUNJI_IMAGE_URL_2 = "http://m.yunjiglobal.com/yjbuyer/detail?itemId=EDOODSID";
    public static final String KIDSWANT_URL = "https://item.cekid.com/item/comm?pid=EDOODSID&version=1_0_4_0&source=H5&_=" + System.currentTimeMillis();
    public static final String KIDSWANT_OFF_URL = "https://item.cekid.com/item/getskuinfobyskuid?entityid=shopid&skuid=EDOODSID&channelid=2&version=20170524&outstore=2&prid=310000_310100_310105&_=" + System.currentTimeMillis();
    public static final String KIDSWANT_ITEM_URL = "https://w.cekid.com/item/EDOODSID.html";
    public static final String KIDSWANT_ITEM_OFF_URL = "https://w.cekid.com/scan-buy/info.html?skuid=EDOODSID&entityid=shopid";
    public static final String TMALL_URL_LOGIN = "https://login.m.taobao.com/login.htm";
    public static final String XOH_URL_LOGIN = "http://www.xinouhui.com/goods/";
    public static final String TMALL_COOKIE = "cna=5zJjGN8Q9xICAWdhPD1vojSs; t=078d54d4ea8b06f629fd6584b74f199e; _tb_token_=e13ebbdaa138e; cookie2=1a6cce9499ebc89e7e00d8df20c63215; _med=dw:1920&dh:1080&pw:1920&ph:1080&ist:0; res=scroll%3A1903*5387-client%3A1903*404-offset%3A1903*5387-screen%3A1920*1080; xlly_s=1; dnk=%5Cu8D75%5Cu65ED%5Cu4E1Czx; uc1=cookie21=UIHiLt3xSixwG45%2Bs3wzsA%3D%3D&pas=0&cookie16=W5iHLLyFPlMGbLDwA%2BdvAGZqLg%3D%3D&cookie15=UIHiLt3xD8xYTw%3D%3D&cookie14=Uoe0ZeBoRb0eNg%3D%3D&existShop=false; uc3=nk2=tsV8GJVVWb0%3D&vt3=F8dCuAJ8Kv2sAazOt8E%3D&lg2=V32FPkk%2Fw0dUvg%3D%3D&id2=WvA07t216W%2BH; tracknick=%5Cu8D75%5Cu65ED%5Cu4E1Czx; lid=%E8%B5%B5%E6%97%AD%E4%B8%9Czx; _l_g_=Ug%3D%3D; uc4=nk4=0%40tBNcj8yLOLh%2FG2FEAu3RsuaPFQ%3D%3D&id4=0%40WDf8BVOubjEyUuXGgpgcW1WM3nY%3D; unb=900013774; lgc=%5Cu8D75%5Cu65ED%5Cu4E1Czx; cookie1=BxEyknt09TlDMkmLqiD%2Fvcub5fm3vir6qMvq%2FhISyXs%3D; login=true; cookie17=WvA07t216W%2BH; _nk_=%5Cu8D75%5Cu65ED%5Cu4E1Czx; sgcookie=E1003WoIrxQgUhi7fsfFYjaNFlOEkKlg06KRd7EcL1Vdg1MOzgovjUzMAxRowlH59nm1n8v8rXNxVfu5djrSp%2FvoJg%3D%3D; sg=x46; csg=aa2da57f; enc=bJQR60FvrUqjGM8wKDuwQ13%2FZeImP3VazmyRDsK%2F1BrO%2F5fuWeIKLx8XoN%2BUEv7zNrx%2FXkMUe%2F%2BV3mluBkVGXQ%3D%3D; pnm_cku822=098%23E1hvsQvUvbpvUvCkvvvvvjiWP2LUzjibR2zw1jnEPmPw6jEbR25U1jtRR25U6jDUi9hvCvvv9UURvpvhvv2MMQ9CvhQWd9%2BvCsfUTWexRdIAcUvaYV0KHkx%2FAjc6f4g7EcqWaNLXrqpyCWmQD70Od5ln%2B8c6sCewwDqXS47BhC3qVUcnDOmOecIUDajxALwpkvhvC99vvOCgLT9Cvv9vvUvqnHi%2Bcf9Cvm9vvvvvphvvvvvv96CvpvBovvm2phCvhRvvvUnvphvppvvv96CvpCCvvvhvC9v9vvCvp29Cvvpvvvvv; cq=ccp%3D0; isg=BKqqCU1vgbkVOA3EzDqR1pMz-xBMGy516tz50zRiYv0HZ0shHKjMhWrR95P7l6YN; l=eBgTpiZPOeUxrbNaBO5Churza779yIOb8sPzaNbMiInca1lPsQ_UuNQ2pO_w-dtjgt5vDetPrUUpAdFB7yULRxGDbLTqUWqSDZJ68e1..; tfstk=c2tRBusiAjcl8yBvQU30R9OAl5LdaxGdG813vAWJ7GHjMbqLAsmWjhnUb46QwiHA.";
    public static final String JD_COOKIE = "shshshfpa=51346882-0f95-fd9a-cf5f-61524d965095-1564651657; __jdv=122270672|direct|-|none|-|1564651658077; shshshfpb=l0503es5LWC9pNBQ24AzGCg%3D%3D; PCSYCityID=CN_310000_310100_310107; __jdu=15646516580761161650744; areaId=19; ipLoc-djd=19-1601-3633-0; __jda=122270672.15646516580761161650744.1564651658.1565167250.1565243248.5; __jdc=122270672; shshshfp=bcbeb9ec8c25d6bcc220f23a202a73a1; 3AB9D23F7A4B3C9B=VYPE2X6DSUNHVQ6WUACIHIE3JFFIBB7TU74YP7USLFHM3SMP5IXXDHW66ATUMWIO6I3J6RCSTPZUZKJF3Z3RBDE5I4; warehistory=\"41294475653,\"; wxa_level=1; retina=0; cid=9; webp=1; mba_muid=15646516580761161650744; sk_history=41294475653%2C; sc_width=1366; visitkey=17361686522245916; commonAddress=0; mitemAddrName=; wq_addr=0%7C2_78_51978_0%7C%u4E0A%u6D77_%u9EC4%u6D66%u533A_%u57CE%u533A_%7C%7C; jdAddrId=2_78_51978_0; jdAddrName=%u4E0A%u6D77_%u9EC4%u6D66%u533A_%u57CE%u533A_; regionAddress=2%2C78%2C51978%2C0; mitemAddrId=2_78_51978_0; wq_logid=1565243706.1520340396; wqmnx1=MDEyNjM4NHMubXQ3bXJfaTcyTTVkMW4pZTdUZSA3OWYuRjQtM1lTKig%3D; __jdb=122270672.5.15646516580761161650744|5.1565243248; mba_sid=15652434870108823528872541342.3; __wga=1565243700764.1565243487722.1565243487722.1565243487722.3.1; PPRD_P=UUID.15646516580761161650744-LOGID.1565243700783.1105725261; shshshsID=056b366177ba7a51e0422dc3b77e8c4a_5_1565243701347";
    public static final String YHD_COOKIE = "__jdv=259140492|baidu-pinzhuan|t_288551095_baidupinzhuan|cpc|yhdbaidupcpz007_0_3bb96dad87b9484281e519ff3d85487c|1508120819221; cart_cookie_uuid=587fdf90-85c3-47c6-9c68-afc36011d322; yhd_location=2_2817_51973_0; provinceId=2; cityId=2817; mba_muid=15081208192201111174638; test=1; cart_num=0; __jda=81617359.15081208192201111174638.1508120819.1508134896.1508137618.4; __jdb=81617359.1.15081208192201111174638|4.1508137618; __jdc=81617359";
    public static String EDOODSID = "EDOODSID";
    public static String PRODUCTID = "EGOODSID";
    public static String ENJOY_EDSC = "享受";
    public static String JD_PRESELL = "京东预售";
    public static String LIMIT = "此价格不与套装优惠同时享受";
    public static String PLEASE_LATER = "哎哟喂,被挤爆啦,请稍后重试";
    public static String GOODS_JHS = "此商品正在参加聚划算";
    public static String GOODS_JHS_DESC = "聚划算";
	public static String BRANDID="BRANDID";
	public static String SYSTEM_ACCESS_FAILED="系统开小差了";
	public static String MESSAGE="皮皮客";
	public static String PROMOTION_DATA="此价格不与套装优惠同时享受"; 
	public static String PAGE_WITHOUT="页面没有了";
	public static String SORRY_COULDN="Sorry! We couldn";
	public static String  MYRIAD="万";
	public static String  THOUSAND="千";
	public static String  BEST="百";
	public static String BOBY="掌柜热卖宝贝";
	public static String TMART="天猫超市";
	public static String TMART_MAIN_VENUE="天猫超市主会场";
	public static String SUPERMARKET="超市";
	public static String SHIPID="3";
	public static String FLAGSHIP_STORE="旗舰店";
	public static String DAY_CAT_FLAGSHIP_STORE="天猫旗舰店";
	public static String SHANGHAI_CODE="310100"; 
	public static String BIEJING_CODE="110100";
	public static String GUANGZHOU_CODE="440100";
	public static String SHENZHEN_CODE="440300";
	public static String SHANGHAI="上海"; 
	public static String BIEJING="北京";
	public static String GUANGZHOU = "广州";
	public static String SHENZHEN = "深圳";
	public static String PROMOTIN = "促销";//
	public static String MEMBER_SPECIALS = "会员特价";//
	public static String DONTPACK_MAIL = "不包邮";//
	public static String PACK_MAIL = "包邮";//
	public static String PROPRIETARY = "非自营";//
	public static String YES_PROPRIETARY = "自营";//
	public static String VIP_PROPRIETARY = "唯品会自营";
	public static String MIYA_PROPRIETARY = "蜜芽自营";
	public static String NETEASE_PROPRIETARY = "考拉自营";
	public static String SPOT = "现货";//
	public static String IN_STOCK = "有货";//
	public static String HAS_GONE = "已抢光";//
	public static String HTML = ".html";//
	public static String IS_NOT_STOCK = "无货";//
	public static String NO_SALES_PROMOTION = "暂无促销";//
	public static String COUPONS = "优惠券";//
	public static String ROLL = "领劵";//
	public static String OFFICIAL = "官方旗舰店";
	public static String TOP_UP_COUPONS = "满额返券:";//
	public static String EACHFULL="每满";
	public static String FULL="满"; 
	public static String PRESENT="赠"; 
	public static String PRESENT_DATA="赠品"; 
	public static String PROMOTION_DETAILS="优惠换购热销商品";
	public static String YUAN="元用";
	public static String PREFERENTIAL="元可优惠";
	public static String NOW_PLACE_ORDER="现在下单， 立即可用";
	public static String AMAZON_STOCK="In Stock.";
	public static String AMAZON_STOCK_ON="Not In Stock";
	public static String AMAZON_STOCK_YES="In Stock";
	public static String AMAZON_STOCK_LOWERCASE="stock";
	public static String JD_SECONDS_KILL="京东秒杀"; 
	public static String TMALL_SECONDS_KILL="秒杀价"; 
	public static String JD_LOGISTICS="京东物流";
	public static String PLATFORM_DATA_JX="酒仙自营";
	
	public static String EXPRESSION="满[^元]+元用(.*?)元";//
	public static String NULL="null";
	public static String COUPONREDMPTION="领券:";
	public static String MONEY="元";
	public static String HOUSEHOLDS_IS_FULL="家居满";
	public static String REDUCTION_OF="减"; 
	public static String ENJOY="享";
	public static String ADVERTISING="广告"; 
	public static String ZARA_STORE="zara官方旗舰店";
	public static String REBATE="返利:买后最高返¥";
	public static String ZARA="ZARA";
	public static String GUOMEI="gome";
	public static String MEN="男士";
	public static String MS="女士";
	public static String CHILDREN="儿童";
	public static String TRF="TRF";
	public static String VIEWPORT="productMainName";
	public static String RETURNURL="returnurl";
	public static String RETURNURL_FLAG="rgv587_flag";
	public static String FORM_J_FORM="form J_form";
	public static String RATEDETAIL="rateDetail";
	public static String SHOPNAME="shopName";
	public static String CURRENTPRICE="currentPrice";
	public static String STOCKSTATENAME="StockStateName";
	public static String QUERYHTM="query.htm";
	public static String ISCOASEOUT="isCoaseOut";
	public static String ORIGIN="产地";
	public static String COUNTRY_ORIGIN="原产地";
	public static String BRAND="品牌";
	public static String NAME="name";
	public static String FOUNT_404="404 Not Found";
    public static String SOLOD_OUT="You need to enable JavaScript to run this app";
    public static String RGV_FLAG="rgv587_flag";
    
	
	public static String VALUE="value";
	public static String LOCATION_HELAN="荷兰";
	public static String LOCATION_AODALIY="澳大利亚";
	public static String LOCATION_RIBEN="日本";
	public static String LOCATION_XINXILAN="新西兰";
	public static String LOCATION_DEGUO="德国";
	public static String LOCATION_MEIGUO="美国";
	public static String LOCATION_YINGGUO="英国";
	public static String LOCATION_FAGUO="法国";
	public static String LOCATION_AGENTING="阿根廷";
	public static String LOCATION_ZHILI="智利";
	public static String LOCATION_HANGUO="韩国";
	public static String LOCATION_ZHONGGUO="中国";
	public static String LOCATION_DANMAI="丹麦";
	public static String LOCATION_AOZHOU="新西兰";
	public static String LOCATION_RUIDIAN="瑞典";
	public static String LOCATION_AIERLAN="爱尔兰"; 
	
	public static String LOCATION_LIBERO="Libero丽贝乐";//瑞典
	public static String LOCATION_BEILAMI="贝拉米";//澳大利亚
	public static String LOCATION_MEIZANCHEN="美赞臣";//美国
	public static String LOCATION_FRISO="Friso";//荷兰
	public static String LOCATION_COW="Cow&Gate";//新西兰
	public static String LOCATION_AOZHOUS="澳洲";//新西兰
	public static String SERVICEBUSY="服务器繁忙";
	public static String ENTRYLIST="entrylist";
	public static String ILLEAL="错误，没有找到您想要进入的页面00";
	public static String DESC="desc";
	public static String DEONSTATUS="onStatus";
	public static String SHOP_NAME_ELLISPIS="shop_name ellipsis";
	public static String GOME_PROPRIETARY="国美自营";
	public static String PROMOTIONRESULT="promotionResult";
	public static String FREE_SHIPPING="免运费";
	
			
    public static final String CRAWLER_IP_ADDRES = "http://compass.earlydata.cn:10086/ip/proxy/get/PROXY_STATIC_DLY/PRICE/OTHER";
    public static final String CRAWLER_IP_ADDRES_COUNTRY = "http://compass.earlydata.cn:10086/ip/proxy/get/PROXY_BY_COUNTRY/OTHER/OTHER/美国";
    public static final String CRAWLER_IP = "ip";
    public static final String CRAWLER_PORT = "port";
    public static final String CRAWLER_GET = "GET";
    public static final String CRAWLER_POST = "POST";
    public static final String CRAWLER_JSON = "json";
    public static final String CRAWLER_STRING = "string";
    public static final String CRAWLER_CODING_FORMAT = "CodingFormat";
    public static final String CRAWLER_USERNAME = "lum-customer-analyticservice-zone-gnpd_us";
    public static final String CRAWLER_PASSWORD = "glgdin0ppnp8";
    public static final String LU_ZONE = "zproxy.lum-superproxy.io";
    public static final String CRAWLER_USERNAME_CLOUD ="379862802";
    public static final String CRAWLER_PASSWORD_CLOUD ="infopower";


    public static final String CRAWLER_CLOUD_AGENT = "云代理";
    public static final String CRAWLER_LUMINATI = "luminati";
    public static final String CRAWLER_PUBLIC = "public";
    public static final String CRAWLER_DOG_AGENT = "狗子云";
    public static final String HTTP_PREFIX = "http";
    public static final String HTTPS_PREFIX = "https";
    public static final String HTTP_PATH_SYMBOL = "/";

    public static final String CRAWLER_STATUS_CODE= "30";
    public static final int CRAWLER_STATUS_CODE_200 = 200;
    public static final int CRAWLER_STATUS_CODE_403 = 403;
    public static final int CRAWLER_STATUS_CODE_429 = 429;
    public static final int CRAWLER_STATUS_CODE_502 = 502;
    public static final int CRAWLER_STATUS_CODE_503 = 503;

    public static final String CRAWLER_RESULT = "result";
    public static final int CRAWLER_PORT_24000 = 24000;
    public static final String CRAWLER_PROXYIP = "121.46.231.180";
	
	
}  
