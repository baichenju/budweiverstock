package com.eddc.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.SerializeWriter;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.ValueFilter;

/**
 * 
 * 类名称：JSONUtil 类描述：暂无 创建人：bin zhang 创建时间：2012-7-5 上午9:49:16 修改人：bin zhang
 * 修改时间：2012-7-5 上午9:49:16 修改备注：
 * 
 * @version
 */
@Component
public class JSONUtil { 

	/***
	 * 将json解析出来的Map转换为对象
	 * 
	 * @param value
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static Object mapToObject(Map value, Class clazz) {
		
		return JSONUtil.JsonToObject(JSONUtil.objectToString(value), clazz);
	}

	/**
	 * 将Map解析成JSON对象
	 * @param o
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static Map objToMap(Object o) {
		
		return (Map) JSONUtil.JsonToObject(JSON.toJSONString(o), HashMap.class);
	}

	/**
	 * 将对象转换成json Sting类型
	 * 
	 * @param o
	 * @return
	 */
	public static String objectToString(Object o) {
		
		return JSON.toJSONString(o);
	}

	/**
	 * 将字符串json转换为对象
	 * 
	 * @param json
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object JsonToObject(String json, Class clazz) {
		
		return JSON.parseObject(json, clazz);
	}

	/**
	 * 将json转换为map
	 * 
	 * @param json
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static Map JsonToMap(String json) {
		
		return JSON.parseObject(json, HashMap.class);
	}

	/**
	 * 将json转换为List
	 * 
	 * @param json
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static List JsonToList(String json) {
		
		return JSON.parseArray(json);
	}

	public static Object toJSONObject(Object javaObject) {

		return JSON.toJSON(javaObject);
	}
	
	public static String toJSONString(Object object, 
			SerializerFeature ...features) {
		SerializeWriter out = new SerializeWriter();
		String s;
		JSONSerializer serializer = new JSONSerializer(out);
		SerializerFeature arr$[] = features;
		int len$ = arr$.length;
		for (int i$ = 0; i$ < len$; i$++) {
			SerializerFeature feature = arr$[i$];
			serializer.config(feature, true);
		}

		serializer.getValueFilters().add(new ValueFilter() {
			public Object process(Object obj, String s, Object value) {
				if(null!=value) {
					if(value instanceof java.util.Date) {
						return String.format("%1$tF %1tT", value);
					}
					
					if ("null".equals(value)){
						return "";
					}
					return value;
				}else {
					return "";
				}
			}
		});
		serializer.write(object);
		s = out.toString();
		out.close();
		return s;
	}


	/**
	 * 替换json字符串中的引号以正常解析
	 * @param jsonString 异常json字符串
	 * @return jsonString
	 */
	public static String formatJsonString(String jsonString){
		char[] temp = jsonString.toCharArray();
		int n = temp.length;
		for (int i = 0; i < n; i++) {
			if (temp[i] == ':' && temp[i + 1] == '"') {
				for (int j = i + 2; j < n; j++) {
					if (temp[j] == '"') {
						if (temp[j + 1] != ',' && temp[j + 1] != '}') {
							temp[j] = '”';
						} else if (temp[j + 1] == ',' || temp[j + 1] == '}') {
							break;
						}
					}
				}
			}
		}
		return new String(temp);
	}
}
