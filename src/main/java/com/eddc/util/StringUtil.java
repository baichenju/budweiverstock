/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.util 
 * @author: jack.zhao   
 * @date: 2018年3月14日 下午4:08:13 
 */
package com.eddc.util;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：StringUtil   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年3月14日 下午4:08:13   
* 修改人：jack.zhao   
* 修改时间：2018年3月14日 下午4:08:13   
* 修改备注：   
* @version    
*    
*/
public class StringUtil {
	/**
     * @param name 转换前的驼峰式命名的字符串
     * @return 转换后下划线大写方式命名的字符串
     */
    public static String underscoreName(String name) {
        StringBuilder result = new StringBuilder();
        if (name != null && name.length() > 0) {
            // 将第一个字符处理成大写
            result.append(name.substring(0, 1).toUpperCase());
            // 循环处理其余字符
            for (int i = 1; i < name.length(); i++) {
                String s = name.substring(i, i + 1);
                // 在大写字母前添加下划线
                if (s.equals(s.toUpperCase()) && !Character.isDigit(s.charAt(0))) {
                    //result.append("_");
                }
                // 其他字符直接转成大写
                result.append(s.toUpperCase());
            }
        }
        return result.toString();
    }
}
