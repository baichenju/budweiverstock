/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.util 
 * @author: jack.zhao   
 * @date: 2018年5月23日 下午1:31:33 
 */
package com.eddc.util;
import java.io.File;  
import java.io.FileInputStream;  
import java.io.FileOutputStream;  
import java.io.IOException;  
import java.io.OutputStream;
import java.util.ResourceBundle;
import org.apache.commons.net.ftp.FTPClient;  
import org.apache.commons.net.ftp.FTPClientConfig;  
import org.apache.commons.net.ftp.FTPFile;  
import org.apache.commons.net.ftp.FTPReply;  
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.eddc.model.Ftp;  
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：FtpUtil   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年5月23日 下午1:31:33   
 * 修改人：jack.zhao   
 * 修改时间：2018年5月23日 下午1:31:33   
 * 修改备注：   
 * @version    
 *    
 */
public class FtpUtil {
	private static Logger logger=LoggerFactory.getLogger(FtpUtil.class);  
	private static FTPClient ftp;  
	/** 
	 * 获取FTP连接 
	 * @param f 
	 * @return 
	 * @throws Exception 
	 */  
	public static boolean connectFtp(Ftp f) throws Exception{  
		ftp=new FTPClient();  
		boolean flag=false;  
		int reply;  
		if (f.getPort()==0) {  
			ftp.connect(f.getIpAddr(),21);  
		}else{  
			ftp.connect(f.getIpAddr(),f.getPort());  
		}  
		ftp.login(f.getUserName(), f.getPwd());  
		ftp.setFileType(FTPClient.BINARY_FILE_TYPE);  
		reply = ftp.getReplyCode();        
		if (!FTPReply.isPositiveCompletion(reply)) {        
			ftp.disconnect();        
			return flag;        
		}        
		ftp.changeWorkingDirectory(f.getPath());        
		flag = true;        
		return flag;  
	}  

	/** 
	 * 关闭FTP连接 
	 */  
	public static void closeFtp(){  
		if (ftp!=null && ftp.isConnected()) {  
			try {  
				ftp.logout();  
				ftp.disconnect();  
			} catch (IOException e) {  
				e.printStackTrace();  
			}  
		}  
	}  

	/** 
	 * ftp上传文件 
	 * @param f 
	 * @throws Exception 
	 */  
	public static void FtupUpload(String path_url) throws Exception{
		Ftp ftps=new Ftp();
		File f = new File(path_url);
		if (!f.exists()) {
			f.mkdirs();
		} 
		ftps.setIpAddr(ResourceBundle.getBundle("docker").getString("IpAddr")); //ip地址   
		ftps.setUserName(ResourceBundle.getBundle("docker").getString("UserName"));//ftp用户名    
		ftps.setPwd(ResourceBundle.getBundle("docker").getString("Pwd"));//ftp密码    
		ftps.setPort(Integer.valueOf(ResourceBundle.getBundle("docker").getString("Port")));//端口号   
		FtpUtil.connectFtp(ftps);  //获取FTP连接 
		if (f.isDirectory()) {  
			ftp.makeDirectory(f.getName());  
			ftp.changeWorkingDirectory(f.getName());  
			String[] files=f.list();  
			for(String fstr : files){    
				File file2=new File(f.getPath()+"/"+fstr);  
				FileInputStream input=new FileInputStream(file2);  
				ftp.storeFile(file2.getName(),input);  
				input.close();   
			}  
		}else{  
			File file2=new File(f.getPath());  
			FileInputStream input=new FileInputStream(file2);  
			ftp.storeFile(file2.getName(),input);  
			input.close();  
		}  
	}  

	/** 
	 * 下载连接配置 
	 * @param f 
	 * @param localBaseDir 本地目录 
	 * @param remoteBaseDir 远程目录 
	 * @throws Exception 
	 */  
	public static void startDown(Ftp f,String localBaseDir,String remoteBaseDir ) throws Exception{  
		if (FtpUtil.connectFtp(f)) {  

			try {   
				FTPFile[] files = null;   
				boolean changedir = ftp.changeWorkingDirectory(remoteBaseDir);   
				if (changedir) {   
					ftp.setControlEncoding("iso-8859-1");//注意编码格式  
					FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_UNIX);  
					conf.setServerLanguageCode("zh");//中文  
					files = ftp.listFiles();   
					for (int i = 0; i < files.length; i++) {   
						try{   

							downloadFile(files[i], localBaseDir, remoteBaseDir);   
						}catch(Exception e){   
							logger.error("【<"+files[i].getName()+"下载失败 error:{}】",e);   
						}   
					}   
				}   
			} catch (Exception e) {     
				logger.error("【下载过程中出现异常 error:{}】",e);   
			}   
		}else{  
			logger.error("连接失败!");  
		}  
	}  
	/**  
	 *  
	 * 下载FTP文件  
	 * 当你需要下载FTP文件的时候，调用此方法  
	 * 根据--获取的文件名，本地地址，远程地址--进行下载  
	 * @param ftpFile  
	 * @param relativeLocalPath  
	 * @param relativeRemotePath  
	 * @throws Exception  
	 */   
	private  static void downloadFile(FTPFile ftpFile, String relativeLocalPath,String relativeRemotePath) throws Exception {   
		if (ftpFile.isFile()) {  

			String filename=new String(ftpFile.getName().getBytes("iso-8859-1"), "utf-8");//涉及到中文文件  
			System.out.println(filename);  

			if (ftpFile.getName().indexOf("?") == -1) {   
				OutputStream outputStream = null;   
				try {   
					File locaFile= new File(relativeLocalPath+ filename);   
					//若文件已存在则返回  
					if(locaFile.exists()){   
						logger.info("提示：目标文件已存在！！！！");  
						return;   
					}else{   
						outputStream = new FileOutputStream(relativeLocalPath+ filename);  

						ftp.retrieveFile(ftpFile.getName(), outputStream);   
						outputStream.flush();   
						outputStream.close();   
					}   
				} catch (Exception e) {   
					logger.error("【下载FTP文件  失败 error:{}】",e);  
				} finally {   
					try {   
						if (outputStream != null){   
							outputStream.close();   
						}  
					} catch (IOException e) {   
						logger.error("输出文件流异常");   
					}   
				}   
			}   
		} else {   
			String newlocalRelatePath = relativeLocalPath + ftpFile.getName();   
			String newRemote = new String(relativeRemotePath+ ftpFile.getName().toString());   
			File fl = new File(newlocalRelatePath);   
			if (!fl.exists()) {   
				fl.mkdirs();   
			}   
			try {   
				newlocalRelatePath = newlocalRelatePath + '/';   
				newRemote = newRemote + "/";   
				String currentWorkDir = ftpFile.getName().toString();   
				boolean changedir = ftp.changeWorkingDirectory(currentWorkDir);   
				if (changedir) {   
					FTPFile[] files = null;   
					files = ftp.listFiles();   
					for (int i = 0; i < files.length; i++) {   
						downloadFile(files[i], newlocalRelatePath, newRemote);   
					}   
				}   
				if (changedir){  
					ftp.changeToParentDirectory();   
				}   
			} catch (Exception e) {   
				logger.error("【下载FTP文件  失败 error:{}】",e);  
			}   
		}   
	}   


	public static void main(String[] args) throws Exception{    
		Ftp f=new Ftp();
		f.setIpAddr("101.231.74.134"); //ip地址   
		f.setUserName("pricemonitor");//ftp用户名    
		f.setPwd("eddc168168");//ftp密码    
		f.setPort(21);//端口号    
		FtpUtil.connectFtp(f);  
		File file = new File("d:/monitoring_crawler/2018-05-23/");    
		//FtpUtil.upload();//  
		//String local = "D:/";  
		//String remote ="/monitoring_crawler/2018-05-23";  
		//String remoteUrl = new String(remote.getBytes("utf-8"), "iso-8859-1");//涉及到中文问题 根据系统实际编码改变  
		//FtpUtil.startDown(f, local,  remoteUrl);  
		System.out.println("ok");  
	}  
}
