package com.eddc.controller;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletRequest;
public class BaseController {
	protected Map<String, Object> getRequestParams(HttpServletRequest request){
		Map<String, Object> requestParams = new HashMap<String, Object>();
		Map<String, String[]> paramsMap = request.getParameterMap();
		Iterator<Entry<String, String[]>> it = paramsMap.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String[]> e = it.next();
			String key = e.getKey();
			String[] vaA = e.getValue();
			String valueS = null;
			for (String p : vaA) {
				if (null == valueS) {
					valueS = p;
				} else {
					valueS += ("," + p);
				}
			}
			requestParams.put(key, valueS);
		}
		return requestParams;
	}


	protected String getParameterMap(HttpServletRequest request){  
		String url="";
		Map<String, String[]> params = request.getParameterMap();  
		String queryString = "";  
		for (String key : params.keySet()) {  
			String[] values = params.get(key);  
			for (int i = 0; i < values.length; i++) {  
				String value = values[i];  
				queryString += key + "=" + value + "&";  
			}  
		}  
		// 去掉最后一个空格  
		queryString = queryString.substring(0, queryString.length() - 1);
		url=request.getRequestURL()+"?"+queryString;
		return url;
	}
}
