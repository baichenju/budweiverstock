package com.eddc.controller;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import javax.servlet.http.HttpServletRequest;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.eddc.method.AmazonData;
import com.eddc.method.AlibabaEnData;
import com.eddc.method.AllData;
import com.eddc.method.CommentParticipleData;
import com.eddc.method.HpkData;
import com.eddc.method.PinDuoDuoData;
import com.eddc.method.SearchData;
import com.eddc.method.XohData;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.Craw_monitor_url_info;
import com.eddc.model.JobAndTrigger;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.redis.JedisService;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.job.JobAndTriggerService;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.publicClass;
import com.github.pagehelper.PageInfo;
import com.google.gson.Gson;
@RestController
@RequestMapping(value="/job")
public class JobController extends BaseController{
	@Autowired	
	private JobAndTriggerService jobAndTriggerService;
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired @Qualifier("Scheduler")
	private Scheduler scheduler; 
	@Autowired
	private SearchData searchData;
	@Autowired
	private AmazonData amazonData;
	@Autowired 
	private AlibabaEnData alibabaEnData; 
	@Autowired 
	private CommentParticipleData commentParticipleData;
	@Autowired 
	private XohData xohData;
	@Autowired 
	private AllData allData;
	@Autowired 
	private HpkData hpkData;
	@Autowired 
	private PinDuoDuoData pinDuoDuoData;
	@Autowired
	private JedisService jedisService;
	
	private static Logger log = LoggerFactory.getLogger(JobController.class);
	private  SimpleDateFormat data=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PostMapping(value="/addjob")
	public String addjob(HttpServletRequest request) throws Exception{
		Map<String, Object> map=this.getRequestParams(request);
		QrtzCrawlerTable qrtzCrawl=new QrtzCrawlerTable();
		Gson gson=new Gson(); 
		int  success=1;
		String jobClassName=map.get("jobClassName").toString();
		String jobGroupName=map.get("jobGroup").toString();
		String cronExpression=map.get("cronExpression").toString();
		String jobName=map.get("jobName").toString();
		String triggersName=map.get("jobName").toString(); 
		String describe=map.get("describe").toString();
		String promotion_status=map.get("promotion_status").toString();
		String database=map.get("database").toString();
		String Ip=publicClass.getLocalIP();
		try { 
			// 启动调度器  
			scheduler.start(); 
			//构建job信息
			Class cls = Class.forName(jobClassName) ;
			cls.newInstance();
			JobDetail jobDetail = JobBuilder.newJob(cls).withIdentity(jobName, jobGroupName).build();
			//表达式调度构建器(即任务执行的时间)
			CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression);
			//按新的cronExpression表达式构建一个新的trigger 触发器    触发器名称 triggersName
			CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(triggersName, jobGroupName) .withSchedule(scheduleBuilder).build();
			//插入中间表
			String platform_Name=jobName.split("_")[1].toString();	
			String state="";
			String User_Id=jobName.split("_")[3].toString();
			if(!User_Id.matches("[0-9]{1,}")){  
				User_Id=jobName.split("_")[4].toString();
				state=jobName.split("_")[2].toString();
			}
			qrtzCrawl.setData_time(data.format(new Date()));
			qrtzCrawl.setIP(Ip);
			qrtzCrawl.setJob_name(jobName);
			qrtzCrawl.setPlatform(platform_Name.toLowerCase()+state.trim());
			qrtzCrawl.setUser_Id(User_Id);
			qrtzCrawl.setStatus(Fields.STATUS_ON);
			qrtzCrawl.setDescribe(describe);
			qrtzCrawl.setPromotion_status(Integer.valueOf(promotion_status));
			qrtzCrawl.setDatabases(database);
			scheduler.scheduleJob(jobDetail, trigger);
			jobAndTriggerService.InsertQrtzCrawl(qrtzCrawl);//配置用户调用代理IP	 
		} catch (SchedulerException e) {
			e.printStackTrace();
			success=0;
			log.error("创建定时任务失败"+e);
		}
		return gson.toJson(success);
	}


	@PostMapping(value="/pausejob")//暂停任务
	public String pausejob(HttpServletRequest request) throws Exception{			
		Map<String, Object> map=this.getRequestParams(request);
		String jobClassName=map.get("jobClassName").toString();
		String jobGroupName=map.get("jobGroupName").toString();
		int  success=1; 
		Gson gson=new Gson();
		try {
			// 通过SchedulerFactory获取一个调度器实例  
			//SchedulerFactory sf = new StdSchedulerFactory();               
			//Scheduler sched = sf.getScheduler();  	
			scheduler.pauseJob(JobKey.jobKey(jobClassName, jobGroupName));
			jobAndTriggerService.UpdateQrtzCrawl(jobClassName, Fields.STATUS_OFF);//停用
		} catch (Exception e) {
			log.error("暂停任务失败"+e);
			success=0;
		}
		return gson.toJson(success);
	}

	//恢复任务
	@PostMapping(value="/resumejob")
	public String resumejob(HttpServletRequest request) throws Exception{			
		Map<String, Object> map=this.getRequestParams(request);
		String jobClassName=map.get("jobClassName").toString();
		String jobGroupName=map.get("jobGroupName").toString();
		int  success=1;
		Gson gson=new Gson();
		try {// 通过SchedulerFactory获取一个调度器实例  
			//SchedulerFactory sf = new StdSchedulerFactory();               
			//Scheduler sched = sf.getScheduler(); 
			scheduler.resumeJob(JobKey.jobKey(jobClassName, jobGroupName));
			jobAndTriggerService.UpdateQrtzCrawl(jobClassName, Fields.STATUS_ON);//启用
		} catch (Exception e) {
			success=0;
			log.error("恢复任务失败"+e);

		}
		return gson.toJson(success);
	}

	//更新任务
	@SuppressWarnings("static-access")
	@PostMapping(value="/reschedulejob")
	public String rescheduleJob(HttpServletRequest request) throws Exception{	
		Map<String, Object> map=this.getRequestParams(request);
		String triggerName=map.get("triggerName").toString();
		String jobGroupName=map.get("jobGroupName").toString();
		String cronExpression=map.get("cronExpression").toString();
		//String jobClassName=map.get("jobClassName").toString();
		int  success=1;
		Gson gson=new Gson();
		try {
			TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, jobGroupName);
			// 表达式调度构建器 
			CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression);
			CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
			// 按新的cronExpression表达式重新构建trigger //创建一个新的newTrigger 防止数据重复调用
			trigger = trigger.getTriggerBuilder().newTrigger().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();
			// 按新的trigger重新设置job执行
			scheduler.rescheduleJob(triggerKey, trigger);
		} catch (SchedulerException e) {
			e.printStackTrace();
			success=0;
			log.error("更新定时任务失败"+e);
			throw new Exception("更新定时任务失败");
		}
		return gson.toJson(success);
	} 

	//删除任务
	@PostMapping(value="/deletejob")
	public String  deletejob(HttpServletRequest request) throws Exception{			
		Map<String, Object> map=this.getRequestParams(request);
		String jobClassName=map.get("jobClassName").toString();
		String jobGroupName=map.get("jobGroupName").toString();
		int  success=1;
		Gson gson=new Gson();
		try {
			// 通过SchedulerFactory获取一个调度器实例  
			//SchedulerFactory sf = new StdSchedulerFactory();               
			//Scheduler sched = sf.getScheduler(); 
			//scheduler.pauseTrigger(TriggerKey.triggerKey(jobClassName, jobGroupName));
			//scheduler.unscheduleJob(TriggerKey.triggerKey(jobClassName, jobGroupName));
			scheduler.deleteJob(JobKey.jobKey(jobClassName, jobGroupName));
			jobAndTriggerService.DeleteQrtzCrawl(jobClassName);
		} catch (Exception e) {
			success=0;
			log.error("删除任务定时任务失败"+e);
		}
		return gson.toJson(success);
	}

	//修改服务器地址
	@PostMapping(value="/reschedule_job_IP")
	public String reschedule_job_IP(HttpServletRequest request) throws Exception{	
		Map<String, Object> map=this.getRequestParams(request);
		String JOB_NAME=map.get("JOB_NAME").toString();
		String IP=map.get("IP").toString();
		int  success=1;
		Gson gson=new Gson();
		try {
			success=jobAndTriggerService.updateIP(JOB_NAME,IP);
		} catch (Exception e) {
			success=0;
		}
		return gson.toJson(success);
	} 
	//修改代理服务器地址
	@PostMapping(value="/reschedule_job_datasource")
	public String reschedule_job_datasource(HttpServletRequest request) throws Exception{	
		Map<String, Object> map=this.getRequestParams(request);
		String JOB_NAME=map.get("JOB_NAME").toString();
		String DATASOURCE=map.get("DATASOURCE").toString();
		int  success=1;
		Gson gson=new Gson();
		try {
			success=jobAndTriggerService.update_Datasource(JOB_NAME,DATASOURCE);
		} catch (Exception e) {
			success=0;
		}
		return gson.toJson(success);
	} 
	//修改抓取商品线程个数
	@PostMapping(value="/reschedule_job_thread")
	public String reschedule_job_thread(HttpServletRequest request) throws Exception{	
		Map<String, Object> map=this.getRequestParams(request);
		String JOB_NAME=map.get("JOB_NAME").toString();
		String THREAD_SUM=map.get("THREAD_SUM").toString();
		String PLATFORM=map.get("PLATFORM").toString();
		int  success=1;
		Gson gson=new Gson();
		try {
			success=jobAndTriggerService.Update_reschedule_job_thread(JOB_NAME,THREAD_SUM,PLATFORM);
		} catch (Exception e) {
			success=0;
		}
		return gson.toJson(success);
	} 
	//修改代理云接口部署 
	@PostMapping(value="/reschedule_job_interface")
	public String reschedule_job_interface(HttpServletRequest request) throws Exception{	
		Map<String, Object> map=this.getRequestParams(request);
		String JOB_NAME=map.get("JOB_NAME").toString();
		String DOCKER=map.get("INTERFACE").toString();
		String PLATFORM=map.get("PLATFORM").toString();
		int  success=1;
		Gson gson=new Gson();
		try {
			success=jobAndTriggerService.Update_reschedule_job_docker(JOB_NAME,DOCKER,PLATFORM);
		} catch (Exception e) {
			success=0;
		}
		return gson.toJson(success);
	} 
	//修改监控城市 
	@PostMapping(value="/reschedule_job_inventory")
	public String reschedule_job_inventory(HttpServletRequest request) throws Exception{	
		Map<String, Object> map=this.getRequestParams(request);
		String JOB_NAME=map.get("JOB_NAME").toString();
		String INVENTORY=map.get("INVENTORY").toString();
		String PLATFORM=map.get("PLATFORM").toString();
		int  success=1;
		Gson gson=new Gson();
		try {
			success=jobAndTriggerService.reschedule_job_inventory(JOB_NAME,INVENTORY,PLATFORM);
		} catch (Exception e) {
			success=0;
		}
		return gson.toJson(success);
	} 
	//修改所以抓数的客户端 ALL PC MOBILE
	@PostMapping(value="/reschedule_job_client")
	public String reschedule_job_client(HttpServletRequest request) throws Exception{	
		Map<String, Object> map=this.getRequestParams(request);
		String JOB_NAME=map.get("JOB_NAME").toString();
		String CLIENT=map.get("CLIENT").toString();
		String PLATFORM=map.get("PLATFORM").toString();
		int  success=1;
		Gson gson=new Gson();
		try {
			success=jobAndTriggerService.reschedule_job_client(JOB_NAME,CLIENT,PLATFORM);
		} catch (Exception e) {
			success=0;
		}
		return gson.toJson(success);
	} 
	@PostMapping(value="/reschedule_job_storage")
	public String reschedule_job_storage(HttpServletRequest request) throws Exception{	
		Map<String, Object> map=this.getRequestParams(request);
		String JOB_NAME=map.get("JOB_NAME").toString();
		String STORAGE=map.get("STORAGE").toString();
		String PLATFORM=map.get("PLATFORM").toString();
		int  success=1;
		Gson gson=new Gson();
		try {
			success=jobAndTriggerService.reschedule_job_storage(JOB_NAME,STORAGE,PLATFORM);
		} catch (Exception e) {
			e.printStackTrace();
			success=0;
		}
		return gson.toJson(success);
	} 
	
	@PostMapping(value="/reschedule_job_request_sum")//REQUEST
	public String reschedule_job_request_sum(HttpServletRequest request) throws Exception{	
		Map<String, Object> map=this.getRequestParams(request);
		String JOB_NAME=map.get("JOB_NAME").toString();
		String SUM_REQUEST_NUM=map.get("SUM_REQUEST_NUM").toString();
		String PLATFORM=map.get("PLATFORM").toString();
		int  success=1;
		Gson gson=new Gson();
		try {
			success=jobAndTriggerService.reschedule_job_request_sum(JOB_NAME,SUM_REQUEST_NUM,PLATFORM);
		} catch (Exception e) {
			e.printStackTrace();
			success=0;
		}
		return gson.toJson(success);
	} 
	

	@PostMapping(value="/reschedule_job_database")
	public String reschedule_job_database(HttpServletRequest request) throws Exception{	
		Map<String, Object> map=this.getRequestParams(request);
		String JOB_NAME=map.get("JOB_NAME").toString();
		String database=map.get("DATABASES").toString();
		String PLATFORM=map.get("PLATFORM").toString();
		int  success=1;
		Gson gson=new Gson();
		try {
			success=jobAndTriggerService.reschedule_job_database(JOB_NAME,database,PLATFORM);
		} catch (Exception e) {
			success=0;
		}
		return gson.toJson(success);
	} 
	@PostMapping(value="/reschedule_job_docker")

	public String reschedule_job_docker(HttpServletRequest request) throws Exception{	
		Map<String, Object> map=this.getRequestParams(request);
		String JOB_NAME=map.get("JOB_NAME").toString();
		String DOCKER=map.get("DOCKER").toString();
		String PLATFORM=map.get("PLATFORM").toString();
		int  success=1;
		Gson gson=new Gson();
		try {
			success=jobAndTriggerService.reschedule_job_docker(JOB_NAME,DOCKER,PLATFORM);
		} catch (Exception e) {
			success=0;
		}
		return gson.toJson(success);
	}
	
	//商品属性开关
	@PostMapping(value="/reschedule_job_attribute")
	public String reschedule_job_attribute(HttpServletRequest request) throws Exception{	
		Map<String, Object> map=this.getRequestParams(request);
		String JOB_NAME=map.get("JOB_NAME").toString();
		String COMMODITY_ATTRIBUTE=map.get("COMMODITY_ATTRIBUTE").toString();
		String PLATFORM=map.get("PLATFORM").toString();
		int  success=1;
		Gson gson=new Gson();
		try {
			success=jobAndTriggerService.reschedule_job_attribute(JOB_NAME,COMMODITY_ATTRIBUTE,PLATFORM);
		} catch (Exception e) {
			success=0;
		}
		return gson.toJson(success);
	}
	
	
	
	
	//修改 爬虫请求数量
	@PostMapping(value="/reschedule_job_sums")

	public String reschedule_job_sums(HttpServletRequest request) throws Exception{	
		Map<String, Object> map=this.getRequestParams(request);
		String JOB_NAME=map.get("JOB_NAME").toString();
		String SUMS=map.get("SUMS").toString();
		String PLATFORM=map.get("PLATFORM").toString();
		int  success=1;
		Gson gson=new Gson();
		try {
			success=jobAndTriggerService.reschedule_job_sums(JOB_NAME,SUMS,PLATFORM);
		} catch (Exception e) {
			success=0;
		}
		return gson.toJson(success);
	}
	@RequestMapping(value="/JobManager") 
	public ModelAndView JobManager(HttpServletRequest request) {
		ModelAndView mode=new ModelAndView();
		mode.setViewName("JobManager");
		return mode;   	
	} 
	
	//关闭缓存时间
	@PostMapping(value="/getRedisDelete")
	public String getRedisDelete(HttpServletRequest request) throws Exception{	
		Map<String, Object> map=this.getRequestParams(request);
		String JOB_NAME=map.get("JOB_NAME").toString();
		String REDIS_TIME=map.get("REDIS_TIME").toString();
		int success=1;
		Gson gson=new Gson();
		try {
			boolean key=jedisService.del(JOB_NAME);
			success=jobAndTriggerService.reschedule_job_redis(JOB_NAME, REDIS_TIME);
			if(key) {
				success=1;
			}else {
				success=0;
			}
		} catch (Exception e) {
			success=0;
		}
		return gson.toJson(success);
	}
	
	
	//页面展示
	@PostMapping(value="/queryjob",produces = {"application/text;charset=UTF-8"})
	public String queryjob(HttpServletRequest request) {
		Map<String, Object> maps=this.getRequestParams(request);
		Gson gson=new Gson();
		Integer pageNum=Integer.valueOf(maps.get("pageNum").toString());
		Integer pageSize=Integer.valueOf(maps.get("pageSize").toString());	
		PageInfo<JobAndTrigger> jobAndTrigger = jobAndTriggerService.getJobAndTriggerDetails(pageNum, pageSize);
		//System.out.println(jobAndTrigger.getList());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("JobAndTrigger", jobAndTrigger);
		map.put("number", jobAndTrigger.getTotal()); 
		return gson.toJson(jobAndTrigger.getList());
	}

	//手动执行爬虫
	@PostMapping(value="/crawlerMessageData")

	public String crawlerMessageData(HttpServletRequest request) throws Throwable{	
		int  success=1;int pageTop=0; Gson gson=new Gson(); 
		String tableName=Fields.TABLE_CRAW_KEYWORDS_INF;//表名称
		String counts=Fields.STATUS_ON;String JOB_CLASS_NAME="";
		Map<String, Object> map=this.getRequestParams(request);
		String JOB_NAME=map.get("JOB_NAME").toString();
		String PLATFORM=map.get("PLATFORM").toString();
		String COUNT=map.get("COUNT").toString();
		if(map.toString().contains("JOB_CLASS_NAME")){
			JOB_CLASS_NAME=map.get("JOB_CLASS_NAME").toString();	
		}
		if(JOB_NAME.contains(Fields.SEARCH) || JOB_NAME.contains(Fields.COMMENT)){
			tableName=Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH;
			counts=Fields.STATUS_OFF;
			pageTop=1;
		}
		try {
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(JOB_NAME);
			if(JOB_NAME.contains(Fields.COMMENT)){

				if(JOB_NAME.contains(Fields.ALL)){
					commentParticipleData.crawlerCommodityAppDetails(listjobName, tableName, counts);
					//commentData.getCommentData(listjobName,tableName);//评论数抓取	
				}else{
					commentParticipleData.crawlerCommodityAppDetails(listjobName, Fields.TABLE_CRAW_KEYWORDS_INF, Fields.STATUS_ON);
					//commentData.getCommentData(listjobName,Fields.TABLE_CRAW_KEYWORDS_INF);//评论数抓取
				}
			}else{
				boolean dataCount=crawlerPublicClassService.examineData(listjobName,listjobName.get(0).getDatabases());
				if(JOB_CLASS_NAME.equals("com.eddc.job.All_Job")){
					if(listjobName.get(0).getJob_name().contains("HPK")) {
						Map<String, String> mapHpk=new HashMap<String, String>();
						mapHpk.put("jobName", listjobName.get(0).getJob_name());
						mapHpk.put("platform", listjobName.get(0).getPlatform());
						mapHpk.put("countId", listjobName.get(0).getUser_Id());
						mapHpk.put("database", listjobName.get(0).getDatabases());
						mapHpk.put("userName", "17601265958");
						mapHpk.put("password", "DAneng12");
						hpkData.regionCrawler(listjobName, tableName, "1", 0, 1,mapHpk);
					}else {
						//asyncData.regionCrawler(listjobName, tableName, counts, pageTop,1,1);
						allData.regionCrawler(listjobName, tableName, counts, pageTop,1,1);
					}
				}else{

					if(JOB_CLASS_NAME.contains(Fields.SEARCH_JOB)){
						List<Craw_keywords_delivery_place>List_Code=crawlerPublicClassService.Keywords_Delivery_Place(listjobName,listjobName.get(0).getDatabases());
						String time=SimpleDate.SimpleDateFormatData().format(new Date());
						if(List_Code.size()>0){
							for(Craw_keywords_delivery_place code:List_Code){
								if(String.valueOf(code.getSearch_status()).equals(Fields.STATUS_ON)){
									searchData.crawlerPriceMonitoring(time,listjobName,code.getDelivery_place_code(),code.getDelivery_place_name());	
								}	
							}
						}else{
							searchData.crawlerPriceMonitoring(time,listjobName,null,null);
						}

					}else if(PLATFORM.equalsIgnoreCase(Fields.PLATFORM_AMAZON)){
						if(COUNT.equalsIgnoreCase(Fields.COUNT_01)){
							amazonData.atartCrawlerAmazon(listjobName,tableName,counts,pageTop);
						}else if(COUNT.equalsIgnoreCase(Fields.COUNT_2)){
							if(!dataCount){
								amazonData.atartCrawlerAmazon(listjobName,tableName,counts,pageTop);
							}
						}
					}else if(PLATFORM.equalsIgnoreCase(Fields.PLATFORM_ALIBABA)){
						if(COUNT.equalsIgnoreCase(Fields.COUNT_01)){
							alibabaEnData.alibabaenCrawler(listjobName, tableName, counts, pageTop);
						}else if(COUNT.equalsIgnoreCase(Fields.COUNT_2)){
							if(!dataCount){
								alibabaEnData.alibabaenCrawler(listjobName,tableName,counts,pageTop);
							}
						}
					}else if(PLATFORM.equalsIgnoreCase(Fields.PLATFORM_XIOUHUI)){//xohData
						String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
						xohData.crawlerCommodityAppDetails(listjobName,time);
					}else if(PLATFORM.equalsIgnoreCase(Fields.PLATFORM_PINDUODUO)){
						pinDuoDuoData.regionCrawler(listjobName, tableName, counts, pageTop,1);

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			success=0;
		}
		return gson.toJson(success);
	}
	/**  
	 * @throws Throwable 
	 * @Title: crawlerFillCatchData  
	 * @Description: TODO(手动补抓失败数据)  
	 */  
	@PostMapping(value="/crawlerFillCatchData")

	public String crawlerFillCatchData(HttpServletRequest request) throws Throwable{	
		int  success=1;int pageTop=0; Gson gson=new Gson(); 
		String tableName=Fields.TABLE_CRAW_KEYWORDS_INF;//表名称
		String counts=Fields.STATUS_ON;String JOB_CLASS_NAME="";
		Map<String, Object> map=this.getRequestParams(request);
		String JOB_NAME=map.get("JOB_NAME").toString();
		String PLATFORM=map.get("PLATFORM").toString();
		if(map.toString().contains("JOB_CLASS_NAME")){
			JOB_CLASS_NAME=map.get("JOB_CLASS_NAME").toString();	
		}
		String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
		List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(JOB_NAME);
		List<Craw_keywords_delivery_place>List_Code=crawlerPublicClassService.Keywords_Delivery_Place(listjobName,listjobName.get(0).getDatabases());//库存
		if(List_Code!=null && List_Code.size()>0){
			for(int i=0;i<List_Code.size();i++){
				success=fillCatchData(JOB_NAME,PLATFORM,time,listjobName,List_Code.get(i).getDelivery_place_code(),tableName,counts,pageTop,JOB_CLASS_NAME);	
			}
		}else{
			success=fillCatchData(JOB_NAME,PLATFORM,time,listjobName,"",tableName,counts,pageTop,JOB_CLASS_NAME);	
		}
		return gson.toJson(success);
	}
	/**
	 * @throws Throwable 
	 * @throws ExecutionException 
	 * @throws InterruptedException   
	 * @Title: fillCatchData  
	 * @Description: TODO(重新获取失败数据)  
	 * @throws  
	 */  
	public int  fillCatchData(String job_name,String platform,String time,List<QrtzCrawlerTable>listjobName,String code,String tableName,String counts,int pageTop,String job_class_name) throws Throwable{
		int  success=1;
		Map<String,Object>parameter=new HashMap<>(5);
		parameter.put("time", time);

		parameter.put("count", Fields.STATUS_ON);
		parameter.put("tableName", tableName);
		parameter.put("crawlerType", Fields.STYPE_1);
		parameter.put("platform", listjobName.get(0).getPlatform());
		try {
			if(job_class_name.equalsIgnoreCase("com.eddc.job.All_Job")){
				if(listjobName.get(0).getJob_name().contains("HPK")) {
					Map<String, String> mapHpk=new HashMap<String, String>();
					mapHpk.put("jobName", listjobName.get(0).getJob_name());
					mapHpk.put("platform", listjobName.get(0).getPlatform());
					mapHpk.put("countId", listjobName.get(0).getUser_Id());
					mapHpk.put("database", listjobName.get(0).getDatabases());
					mapHpk.put("userName", "17601265958");
					mapHpk.put("password", "DAneng12");
					hpkData.regionCrawler(listjobName, tableName, "1", 0, 2,mapHpk);
				}else {
					allData.regionCrawler(listjobName, tableName, Fields.STATUS_OFF, pageTop,2,1);
					//asyncData.regionCrawler(listjobName, tableName, Fields.STATUS_OFF, pageTop,2,1);
					
				}

			}else if(listjobName.get(0).getPlatform().equalsIgnoreCase(Fields.PLATFORM_PINDUODUO)){
				pinDuoDuoData.regionCrawler(listjobName, tableName, counts, pageTop,2);
			}else{
				if(!job_name.contains(Fields.SEARCH) || !job_name.contains(Fields.COMMENT)){
					if(platform.equalsIgnoreCase(Fields.PLATFORM_AMAZON)){
						amazonData.crawAndParseInfoAndPricefailureAmazonecn(time, listjobName, tableName, code, pageTop);
					}else if(platform.equalsIgnoreCase(Fields.PLATFORM_XIOUHUI)){

						xohData.crawlerCommodityAppDetails(listjobName,time);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			success=0;
			log.error("补抓数据失败>>>>>>>>>>>>>"+e);
		}
		return success;
	}
	//页面展示
	@PostMapping(value="/monitorFailureQuery",produces = {"application/text;charset=UTF-8"})
	public String monitorFailureQuery(HttpServletRequest request) {
		Map<String, Object> maps=this.getRequestParams(request);
		String CustomerMonitor=maps.get("CustomerMonitor").toString();
		List <Craw_monitor_url_info>list=new ArrayList<Craw_monitor_url_info>();
		Gson gson=new Gson();
		try {
			list= crawlerPublicClassService.monitoringLeakData(CustomerMonitor,CustomerMonitor);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gson.toJson(list);
	}

}
