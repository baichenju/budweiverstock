package com.eddc;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import com.eddc.framework.DynamicDataSourceRegister;
import com.eddc.util.SimpleDate;
import com.eddc.util.publicClass;
import lombok.extern.slf4j.Slf4j;
@SpringBootApplication
@Import(DynamicDataSourceRegister.class)
@MapperScan("com.eddc.mapper")
@Slf4j
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		log.info("============= 【启动价格监控数据抓取 当前Ip地址是"+publicClass.Ipaddres()+"】 ============="+ SimpleDate.StringWeekday());
	}                                             
}                                                                                                   