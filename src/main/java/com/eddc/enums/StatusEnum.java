package com.eddc.enums;

/**
 * @author jack.zhao
 */

public enum StatusEnum {
	/***
	 * 商品上架
	 */
	PRODUCT_STATUS_NO("1"),
	/***
	 * 商品下架
	 */
	PRODUCT_STATUS_OFF("0");

	private String status;

	StatusEnum(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
