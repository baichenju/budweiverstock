package com.eddc.enums;

/**
 * @author jack.zhao
 */

public enum PlatformEnum {
	/***
	 * 苏宁
	 **/
	SUNING("suning", "苏宁"),
	/***
	 * 天猫
	 **/
	TMALL("tmall", "天猫"),
	/***
	 * 淘宝
	 **/
	TAOBAO("taobao", "淘宝"),
	/***
	 * 京东
	 **/
	JD("jd", "京东"),
	/***
	 * 酒仙
	 **/
	JX("jx", "酒仙"),
	/***
	 * 唯品会
	 **/
	VIP("vip", "唯品会"),
	/***
	 * 云集
	 **/
	YUNJI("yunji", "云集"),
	/***
	 * 拼多多
	 **/
	PDD("pdd", "拼多多"),
	/***
	 * 渝欧汇
	 **/
	YOH("yoh", "渝欧汇"),
	/***
	 * 考拉
	 **/
	KAOLA("kaola", "考拉"),
	/***
	 * 孩子王
	 **/
	KIDSWANT("kidswant", "孩子王"),
	/***
	 * 孩子王
	 **/
	KIDSWANTOFF("kidswantoff", "孩子王");
	private String codeEn;
	private String codeZn;

	PlatformEnum(String codeEn, String codeZn) {
		this.codeEn = codeEn;
		this.codeZn = codeZn;
	}

	public String getCodeEn() {
		return codeEn;
	}

	public void setCodeEn(String codeEn) {
		this.codeEn = codeEn;
	}

	public String getCodeZn() {
		return codeZn;
	}

	public void setCodeZn(String codeZn) {
		this.codeZn = codeZn;
	}

	@Override
	public String toString() {
		return this.getCodeEn() + "----" + this.getCodeZn();
	}
}
