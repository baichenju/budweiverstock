package com.eddc.task;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.service.impl.crawl.AlibabaenDataCrawlService;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.Fields;
import com.eddc.util.StringHelper;
import com.eddc.util.WhetherShelves;
import com.eddc.util.publicClass;
@Component
@Scope("prototype")
public class Alibabaen_DataCrawlTask implements Callable<Map<String, Object>>{
	Logger logger = LoggerFactory.getLogger(Alibabaen_DataCrawlTask.class);
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	private AlibabaenDataCrawlService alibabaen_DataCrawlService;
	@Autowired
	WhetherShelves whetherShelves;
	private CommodityPrices commodityPrices;
	private CrawKeywordsInfo crawKeywordsInfo;
	private Map<String,Object>alibabaMap;
	private String accountId;
	private String dataType;
	private String setUrl;
	private String IPPROXY;
	private String Sum;
	private String timeDate;
	private String type;
	private String codeCookice;
	private String ip;
	private String database;
	private String storage;
	
	public Map<String, Object> call() {
		Map<String,String>Map=new HashMap<String, String>();
		Map=publicClass.parameterMap(setUrl,accountId,dataType,timeDate,IPPROXY);
		String  StringMessage="";
		if(Fields.STYPE_1.equals(type)){//解析数据
			logger.info("【Alibaba平台开始解析数据"+"-------"+this.Sum+"---------------】");
			try { 
				//Thread.sleep(1000); 
				String goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_ALIBABA);//生成goodSId
				Map.put("egoodsId", crawKeywordsInfo.getCust_keyword_name());
				Map.put("keywordId", crawKeywordsInfo.getCust_keyword_id());
				Map.put("goodsId", goodsId);
				Map.put("coding", "GBK");
				Map.put("storage", storage);
				Map.put("database",database);
				StringMessage=httpClientService.interfaceSwitch(ip, Map);//请求数据
				String status =whetherShelves.parseGoodsStatusByItemPage(StringMessage);//判断商品是否下架
				Map<String, Object> sum_status=crawlerPublicClassService.InsertDatStatus(status,database,crawKeywordsInfo,goodsId,timeDate,dataType,setUrl,storage);
				if(Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_OFF) || Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_EXCEPTION)){
					alibabaMap.putAll(sum_status);
					logger.info("【"+crawKeywordsInfo.getPlatform_name() + " >>> The goods have been removed from the shelves_"
                            + Map.get("egoodsId") + " SOLD OUT : >>>>>>>>>>>>>>>>>>>>>>>" + Sum + ">>>】");
					return alibabaMap; 
				}
				Map<String, Object> mapdata=alibabaen_DataCrawlService.alibabaParseItemPage(StringMessage,Map);//解析数据
				alibabaMap.putAll(mapdata);
				alibabaMap.putAll(Map);
				 logger.info("【 >>> get current price finish >>>>>>>>>>>>>>>SUCCESS>>>>>>>"+Sum+">>>>>>>>>>>>>>>>】");
			} catch (Exception e) { 
				e.printStackTrace();
				logger.error("【解析Alibabaen数据失败- error:{}",e);
			}  
		}
		return alibabaMap;
	}
	public CrawKeywordsInfo getCrawKeywordsInfo() {
		return crawKeywordsInfo;
	}
	public void setCrawKeywordsInfo(CrawKeywordsInfo crawKeywordsInfo) {
		this.crawKeywordsInfo = crawKeywordsInfo;
	}

	public String getSum() {
		return Sum;
	}
	
	public void setSum(String sum) {
		Sum = sum;
	}
	
	public String getTimeDate() {
		return timeDate;
	}
	
	public void setTimeDate(String timeDate) {
		this.timeDate = timeDate;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public CrawlerPublicClassService getCrawlerPublicClassService() {
		return crawlerPublicClassService;
	}
	
	public void setCrawlerPublicClassService(
			CrawlerPublicClassService crawlerPublicClassService) {
		this.crawlerPublicClassService = crawlerPublicClassService;
	}
	public String getAccountId() {
		return accountId;
	}
	
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	public String getDataType() {
		return dataType;
	}
	
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	
	public String getSetUrl() {
		return setUrl;
	}
	
	public void setSetUrl(String setUrl) {
		this.setUrl = setUrl;
	}
	
	public String getIPPROXY() {
		return IPPROXY;
	}
	
	public void setIPPROXY(String iPPROXY) {
		IPPROXY = iPPROXY;
	}
	public CommodityPrices getCommodityPrices() {
		return commodityPrices;
	}
	public void setCommodityPrices(CommodityPrices commodityPrices) {
		this.commodityPrices = commodityPrices;
	}
	public String getCodeCookice() {
		return codeCookice;
	}
	public void setCodeCookice(String codeCookice) {
		this.codeCookice = codeCookice;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Map<String, Object> getAlibabaMap() {
		return alibabaMap;
	}
	public void setAlibabaMap(Map<String, Object> alibabaMap) {
		this.alibabaMap = alibabaMap;
	}
	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}
	public String getStorage() {
		return storage;
	}
	public void setStorage(String storage) {
		this.storage = storage;
	}
	
}
