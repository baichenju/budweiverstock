/**
 * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
 *
 * @Package: com.eddc.task
 * @author: jack.zhao
 * @date: 2017年11月23日 下午4:00:14
 */
package com.eddc.task;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.concurrent.Callable;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.crawl.SearchDataCrawlService;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.Fields;
import com.eddc.util.StringHelper;
import com.eddc.util.WhetherShelves;
import com.eddc.util.publicClass;
import com.github.pagehelper.util.StringUtil;

/**
 *
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Pepsi_DataCrawlTask   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年03月14日 下午4:00:14   
 * 修改人：jack.zhao   
 * 修改时间：2018年03月14日 下午4:00:14   
 * 修改备注：   
 * @version
 *
 */
@Component
@Scope("prototype")
public class SearchDataCrawlTask implements Callable<Map<String, Object>> {
	Logger logger = LoggerFactory.getLogger(SearchDataCrawlTask.class);
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	private WhetherShelves whetherShelves;
	@Autowired
	private SearchDataCrawlService search_DataCrawlService;
	private CrawKeywordsInfo crawKeywordsInfo;
	private Map<String, Object> searchMap;
	private String accountId;
	private String dataType;
	private String setUrl;
	private String IPPROXY;
	private String timeDate;
	private String type;
	private String codeCookie;
	private int status;
	private String status_type;
	private String oriUrl = "";
	private String ref = "";
	private String ip;
	private String database;

	public Map<String, Object> call() {
		if (Fields.STYPE_1.equals(type)) {
			try {
				final Map<String, String>  parameter = publicClass.parameterMap(setUrl, accountId, dataType, timeDate, IPPROXY);
				parameter.put("status", String.valueOf(status));
				parameter.put("status_type", status_type);
				parameter.put("database", database);
				parameter.put("egoodsId", crawKeywordsInfo.getCust_keyword_name());
				parameter.put("accountId", accountId);
				parameter.put("ip", ip);
				parameter.put("status", Fields.STATUS_ON);
				parameter.put("codeCookie", codeCookie);
				String StringMessage="";
				//ExecutorService fixedThreadPool = newFixedThreadPool (10);

				for (int i = 1; i <= crawKeywordsInfo.getSearch_page_nums(); i++) {
					//					final int page = i;
					//					fixedThreadPool.execute (() -> { 
					//						
					//						Map<String, String> paramData=new HashMap<String, String>();
					//						String StringMessage="";
					//						
					//						if (crawKeywordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TMALL_EN) || crawKeywordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TMALL_GLOBAL_EN) || crawKeywordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TAOBAO_EN)) {
					//							paramData=tmallDataCrawlService.productPage(parameter, crawKeywordsInfo, page);
					//
					//						}else if(crawKeywordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_JD)) {
					//							paramData=jdDataCrawlService.productPage(parameter, crawKeywordsInfo, page);
					//							
					//						} else if (crawKeywordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_SUNING)) {
					//							paramData=suningDataCrawlService.productPage(parameter, crawKeywordsInfo, page);
					//						}
					//						parameter.putAll(paramData);
					//						
					//						if (parameter.get("status").contains(Fields.STATUS_ON)) {
					//							parameter.put("count", String.valueOf(page));
					//							if(crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_HPK)) {
					//								StringMessage=getItemhpk(page,crawKeywordsInfo.getCust_keyword_name());
					//							}else {
					//								try {
					//									StringMessage = httpClientService.interfaceSwitch(ip, parameter);//请求数据
					//								} catch (Exception e) {
					//									StringMessage = httpClientService.GoodsProduct(parameter);//抓取Itemyem
					//								}
					//							}
					//
					//							String disable = search_DataCrawlService.ParseItemPage(StringMessage, database, crawKeywordsInfo, parameter);//解析数据
					//							if (disable.contains(Fields.DISABLE)) {
					//								return;
					//							}
					//						}
					//
					//					});
					//					fixedThreadPool.shutdown ( );
					//					while(true){  
					//						if(fixedThreadPool.isTerminated()){  
					//							break;
					//						}
					//					}

					//天猫
					if (crawKeywordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TMALL_EN) || crawKeywordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TMALL_GLOBAL_EN) || crawKeywordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TAOBAO_EN)) {
						Map<String, String> tmall = tmallData(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_ON);
						parameter.putAll(tmall);
					}else if (crawKeywordsInfo.getPlatform_name().equalsIgnoreCase("ehaier")) {//海尔
						
						Map<String, String> ehaier =platformEhaier(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_ON);
						parameter.putAll(ehaier);
					} else if (crawKeywordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_JD)) {//京东
						Map<String, String> jd = jdData(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_ON);
						parameter.putAll(jd);
					} else if (crawKeywordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_YHD)) {//一号店
						Map<String, String> yhd = yhdData(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_OFF);
						parameter.putAll(yhd);
						if (yhd.get(Fields.DISABLE).contains(Fields.DISABLE)) {
							return searchMap;
						}
					} else if (crawKeywordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_SUNING)) {//苏宁
						Map<String, String> suning = suningdData(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_OFF);
						parameter.putAll(suning);
						if (suning.get(Fields.DISABLE).contains(Fields.DISABLE)) {
							return searchMap;
						}
					} else if (crawKeywordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_AMAZON) || crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_AMAZON_UN)) {
						Map<String, String> amazon = amazonData(parameter, crawKeywordsInfo, i);//亚马逊
						parameter.put("status", Fields.STATUS_ON);
						parameter.putAll(amazon);
					} else if (crawKeywordsInfo.getPlatform_name().equalsIgnoreCase(Fields.platform_1688)) {
						if (crawKeywordsInfo.getCust_keyword_type().contains("category")) {
							Map<String, String> platform1688 = platform1688_Data(parameter, crawKeywordsInfo, i);
							parameter.put("status", Fields.STATUS_OFF);
							parameter.putAll(platform1688);
							if (parameter.get(Fields.DISABLE).contains(Fields.DISABLE)) {
								return searchMap;
							}
						} else {
							Map<String, String> platform1688 = platform1688_Data(parameter, crawKeywordsInfo, i);
							parameter.put("status", Fields.STATUS_ON);
							parameter.putAll(platform1688);
						}
					} else if (crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_TAOBAO_ZC)) {//淘宝众筹
						Map<String, String> taobazc = platformTaobaoZc_Data(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_ON);
						parameter.putAll(taobazc);
					} else if (crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD_ZC)) {//京东众筹
						Map<String, String> jdzc = platformJdZc_Data(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_ON);
						parameter.putAll(jdzc);
					} else if (crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_VIP)) {//vip
						Map<String, String> vip = platformVIp_Data(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_ON);
						parameter.putAll(vip);
					} else if (crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_KAOLA)) {//考拉
						Map<String, String> laola = platformKaola_Data(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_ON);
						parameter.putAll(laola);
					} else if (crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_JX)) {//酒仙网
						Map<String, String> jx = platformJx_Data(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_ON);
						parameter.putAll(jx);
					} else if (crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_KIDSWANT)) {
						Map<String, String> kidswant = platformKidswant_Data(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_ON);
						parameter.putAll(kidswant);
					} else if (crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_KIDSWANT_OFF)) {
						Map<String, String> kidswant = platformKidswant_Data(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_ON);
						parameter.putAll(kidswant);
					} else if (crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_XIOUHUI)) {
						Map<String, String> inOuHUi = platformXoh_Data(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_ON);
						parameter.putAll(inOuHUi);
					}else if (crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_YUOUHUI)) {
						Map<String, String> yoh = platformYoh_Data(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_ON);
						parameter.putAll(yoh);
					}else if (crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_SKS)) {
						Map<String, String> sks = platformSks_Data(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_ON);
						parameter.putAll(sks);
					}else if(crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_HPK)){
						Map<String, String> sks = platformHpk_Data(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_ON);
						parameter.putAll(sks);	
					} else if (crawKeywordsInfo.getPlatform_name().equals(Fields.GUOMEI)) {//国美
						Map<String, String> gome = platformGome_Data(parameter, crawKeywordsInfo, i);
						parameter.put("status", Fields.STATUS_ON);
						parameter.putAll(gome);
					} 

					if (parameter.get("status").contains(Fields.STATUS_ON)) {
						parameter.put("count", String.valueOf(i));
						if(crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_HPK)) {
							StringMessage=getItemhpk(i,crawKeywordsInfo.getCust_keyword_name());
						}else {
							try {
								StringMessage = httpClientService.interfaceSwitch(ip, parameter);//请求数据
							} catch (Exception e) {
								StringMessage = httpClientService.GoodsProduct(parameter);//抓取Itemyem
							}
						}

						String disable = search_DataCrawlService.ParseItemPage(StringMessage, database, crawKeywordsInfo, parameter);//解析数据
						if (disable.contains(Fields.DISABLE)) {
							return searchMap;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("【解析" + dataType + "数据失败 error:{}】", e);
			}
		} 
		return searchMap;
	}

	//天猫
	public Map<String, String> tmallData(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i) throws UnsupportedEncodingException {
		if (crawKeywordsInfo.getCust_keyword_url().contains("pepsico.m.tmall.com") || crawKeywordsInfo.getCust_keyword_url().contains("guigeshipin.m.tmall.com")) {
			//Map.put("url", crawKeywordsInfo.getCust_keyword_url().split("/")[0]+"//"+crawKeywordsInfo.getCust_keyword_url().split("/")[2]+Fields.PEPSI_KEYWORDS_DETAILS_URL +"&path=/category-"+crawKeywordsInfo.getCust_keyword_name()+".htm&catId="+crawKeywordsInfo.getCust_keyword_name()+"&scid="+crawKeywordsInfo.getCust_keyword_name()+"&pageNo="+i);
			String keyword = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
			Map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("KEYWORD", keyword).replace("PAGE", String.valueOf(i)));
			Map.put("coding", "GBK");
			Map.put("cookie", null);
			Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url().replace("KEYWORD", keyword).replace("PAGE", String.valueOf(i)));
		} else if (crawKeywordsInfo.getCust_keyword_url().contains("list.tmall.com") && crawKeywordsInfo.getCust_keyword_type().contains("keyword")) {
			if (crawKeywordsInfo.getCust_keyword_url().contains("SEARCH")) {
				String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
				Map.put("url", setUrl.replace("PAGE", String.valueOf((i * 60))).replace("SEARCH", keyWord));
				Map.put("coding", "GBK");
			} else {
				if (i != 1) {
					setUrl = setUrl + "&s=" + 40 * i;
					Map.put("coding", "GBK");
					Map.put("url", setUrl);
				}
			}
		} else if (crawKeywordsInfo.getCust_keyword_url().contains("search_radio_tmall") || crawKeywordsInfo.getCust_keyword_url().contains("filter_tianmao=tmall") || crawKeywordsInfo.getCust_keyword_url().contains("search_radio_all")) {
			String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
			setUrl = crawKeywordsInfo.getCust_keyword_url().replace("SEARCH", keyWord).replace("PAGE", String.valueOf((i - 1) * 44));
			Map.put("url", setUrl);
			Map.put("coding", "GBK");
		} else if (crawKeywordsInfo.getCust_keyword_url().contains("list.tmall.com") && crawKeywordsInfo.getCust_keyword_type().contains("category")) {//天猫指定类目搜索
			if (crawKeywordsInfo.getCust_keyword_url().contains("SEARCH")) {
				String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
				Map.put("url", setUrl.replace("PAGE", String.valueOf((i * 60))).replace("SEARCH", keyWord));
			} else {
				Map.put("url", setUrl.replace("PAGE", String.valueOf((i * 60))).replace("CODE", codeCookie));
			}
			Map.put("coding", "GBK");
		} else if (crawKeywordsInfo.getCust_keyword_url().contains("s.m.taobao.com")) {//手机端搜索商品
			String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
			setUrl = crawKeywordsInfo.getCust_keyword_url().replace("SEARCH", keyWord).replace("PAGE", String.valueOf((i)));
			Map.put("url", setUrl);
		}
		return Map;
	}

	//京东
	public Map<String, String> jdData(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i)
			throws UnsupportedEncodingException {
		int mod = i % 2;
		if (crawKeywordsInfo.getCust_keyword_url().contains("search.jd.com")) {//京东搜索PC
			String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
			if (mod == 0) {//https://search.jd.com/s_new.php?keyword=KEYWORD&enc=utf-8&qrst=1&rt=1&stop=1&vt=2&wq=KEYWORD&page=PAGE&s=SUM
				oriUrl = Fields.JD_SEARCH_URL.replace("KEYWORD", keyWord).replace("PAGE", String.valueOf(i)).replace("SUM", String.valueOf(((i - 1) * 26))) + "&scrolling=y&log_id=1524643976.14897&tpl=3_M";
				//oriUrl=Fields.JD_SEARCH_URL+keyWord+"&enc=utf-8&qrst=1&rt=1&stop=1&vt=2&stock=1&page="+i+"&s="+((i-1)*26)+"&click=2&qrst=unchange";
			} else {
				oriUrl = Fields.JD_SEARCH_URL.replace("KEYWORD", keyWord).replace("PAGE", String.valueOf(i)).replace("SUM", String.valueOf(((i - 1) * 26))) + "&click=2";
				//oriUrl=Fields.JD_SEARCH_URL+keyWord+"&enc=utf-8&qrst=1&rt=1&stop=1&vt=2&stock=1&page="+i+"&s="+((i-1)*26)+"&click=2&qrst=unchange";
				ref = Fields.JD_SEARCH_REF_URL + keyWord + "&enc=utf-8&qrst=1&rt=1&stop=1&vt=2&stock=1&page=" + i + "&s=" + ((i - 1) * 26) + "&click=2&qrst=unchange";
			}
			if(StringUtils.isNoneBlank(crawKeywordsInfo.getCust_keyword_brand())) {
				String bran = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_brand(), "UTF-8");
				oriUrl=oriUrl+"&ev="+bran;
				ref=ref+"&ev="+bran;
			}
			Map.put("url", oriUrl);
			Map.put("urlRef", ref);
		} else if (crawKeywordsInfo.getCust_keyword_url().contains("so.m.jd.com")) {//京东APP
			if (crawKeywordsInfo.getCust_keyword_type().equals("category")) {//categoryId=CATEGORYID&c1=C1&c2=C2
				Map.put("url", Fields.JD_SEARCH_REF_URL_APP.replace("PAGE", String.valueOf(i)).replace("C1", crawKeywordsInfo.getCust_keyword_name().split("-")[0]).replace("C2", crawKeywordsInfo.getCust_keyword_name().split("-")[1]).replace("CATEGORYID", crawKeywordsInfo.getCust_keyword_name().split("-")[2]));
			} else if (crawKeywordsInfo.getCust_keyword_type().equals("keyword")) {
				String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
				Map.put("url", Fields.JD_SEARCH_REF_URL_APP_KEYWORD.replace("PAGE", String.valueOf(i)).replace("KEYWORD", keyWord));
			} else if (crawKeywordsInfo.getCust_keyword_type().equals("keywordcategory")) {
				String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
				String keyWord2 = URLEncoder.encode(crawKeywordsInfo.getCategory_name(), "UTF-8");
				if (crawKeywordsInfo.getCust_account_name().contains(Fields.JD_LOGISTICS)) {
					Map.put("url", Fields.JD_SEARCH_REF_URL_APP_KEYWORD.replace("PAGE", String.valueOf(i)).replace("KEYWORD", keyWord2) + "&expressionKey=%5B%7B%22value%22%3A%22" + keyWord + "%22%2C%22key%22%3A%22%E5%93%81%E7%89%8C%22%7D%5D&configuredFilters=%5B%7B%22bodyValues%22%3A%221%22%2C%22bodyKey%22%3A%22stock%22%7D%2C%7B%22bodyValues%22%3A%221%22%2C%22bodyKey%22%3A%22self%22%7D%5D");
				} else {
					Map.put("url", Fields.JD_SEARCH_REF_URL_APP_KEYWORD.replace("PAGE", String.valueOf(i)).replace("KEYWORD", keyWord2) + "&expressionKey=%5B%7B%22value%22%3A%22" + keyWord + "%22%2C%22key%22%3A%22%E5%93%81%E7%89%8C%22%7D%5D&configuredFilters=%5B%7B%22bodyValues%22%3A%221%22%2C%22bodyKey%22%3A%22stock%22%7D%5D");
				}
			}
			Map.put("urlRef", setUrl);
		} else if (crawKeywordsInfo.getCust_keyword_url().contains("list.jd.com")) {//京东指定类目
			if (StringUtil.isNotEmpty(codeCookie)) {
				Map.put("cookie", codeCookie);
			}
			Map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));
			Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));
		}
		return Map;
	}

	//一号店
	public Map<String, String> yhdData(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception {
		String StringMessage = "";
		String disable = "";
		if (crawKeywordsInfo.getCust_keyword_url().contains("search.m.yhd.com")) {//一号店手机端搜索
			String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");//+
			if (crawKeywordsInfo.getCust_keyword_type().contains("category")) {
				Map.put("url", Fields.YHD_SEARCH_REF_URL_APP.replace("A", crawKeywordsInfo.getCategory_id()).replace("P1", "p" + String.valueOf(i)));
			} else if (crawKeywordsInfo.getCust_keyword_type().contains("keyword")) {
				Map.put("url", Fields.YHD_SEARCH_REF_URL_APP_KEYWORD.replace("KEYWORD", "k" + keyWord).replace("P1", "p" + String.valueOf(i)));
			}
			Map.put("urlRef", setUrl);
		} else if (crawKeywordsInfo.getCust_keyword_url().contains("search.yhd.com")) {//PC端搜索
			String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");//+
			String urlpc = Fields.YHD_SEARCH_REF_URL_APP_KEYWORD_PC.replace("PAGE", "p" + String.valueOf(i)).replace("KSEARCH", "k" + keyWord);
			String nnforiUrls = "&isGetMoreProducts=1&moreProductsDefaultTemplate=0&isLargeImg=0";
			Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url().replace("KSEARCH", "k" + keyWord));
			if (crawKeywordsInfo.getCust_keyword_type().equals("categorybrand")) {
				urlpc = crawKeywordsInfo.getCust_keyword_url().replace("MBNAME", keyWord).replace("PA", "p" + String.valueOf(i));
				Map.put("urlRef", Fields.YHD_SEARCH_PC_REF.replace("MBNAME", keyWord));
			} else if (crawKeywordsInfo.getCust_keyword_type().equals("keywordcategory")) {
				String code[] = crawKeywordsInfo.getCust_keyword_url().split("/");
				urlpc = Fields.YHD_SEARCH_REF_URL_CATEGORY_KEYWORD_PC.replace("CATEGORY", code[3]).replace("CODE", code[4]).replace("PA", "p" + String.valueOf(i));
				Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url());
			}
			Map.put("url", urlpc);
			if (StringUtil.isNotEmpty(codeCookie)) {
				Map.put("cookie", codeCookie);
			}
			for (int kk = 0; kk < 2; kk++) {
				if (kk == 1) {
					Map.put("url", urlpc + nnforiUrls);
				}
				Map.put("kk", String.valueOf(kk));
				Map.put("count", String.valueOf(i));
				logger.info(crawKeywordsInfo.getCust_keyword_name() + "----------当前商品第：" + i + "页--------" + Map.get("url"));
				StringMessage = httpClientService.interfaceSwitch(Map.get("ip"), Map);//请求数据
				if (StringUtil.isEmpty(StringMessage) || StringMessage.contains(Fields.RETURNURL_FLAG)) {
					StringMessage = httpClientService.GoodsProduct(Map);//抓取Itemyem
				}
				disable = search_DataCrawlService.ParseItemPage(StringMessage, Map.get("database"), crawKeywordsInfo, Map);//解析数据
				if (disable.contains(Fields.DISABLE)) {
					Map.put(Fields.DISABLE, Fields.DISABLE);
					return Map;
				}
			}
		}
		Map.put(Fields.DISABLE, disable);
		return Map;
	}

	//1688供应商商品PC
	public Map<String, String> platform1688_Data(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception {
		int mi = 1000 * (int) (1 + Math.random() * (9 - 1 + 1));
		Thread.sleep(mi);
		String keyWord = "";
		try {
			if (crawKeywordsInfo.getCust_keyword_type().contains("keyword")) {
				if (crawKeywordsInfo.getCust_account_id().contains(Fields.ACCOUNTID_60)) {
					keyWord = crawKeywordsInfo.getCust_keyword_name().replace(Fields.MESSAGE, "").trim();
					if (keyWord.contains("/")) {
						keyWord = keyWord.split("/")[0].toString().trim();
					} else {
						if (crawKeywordsInfo.getCust_keyword_name().length() > 8) {
							keyWord = crawKeywordsInfo.getCust_keyword_name().substring(0, 8);
						}
					}
					keyWord = URLEncoder.encode(keyWord, "GBK");
				} else {
					keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "GBK");
				}
				Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url() + keyWord + "&beginPage=" + i + "");
				Map.put("count", String.valueOf(i));
				Map.put("url", crawKeywordsInfo.getCust_keyword_url() + keyWord + "&beginPage=" + i);
			} else if (crawKeywordsInfo.getCust_keyword_type().contains("category")) {
				Map.put("count", String.valueOf(i));
				keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "GBK");
				Map.put("url", "https://s.1688.com/selloffer/rpc_async_render.jsonp?keywords=" + keyWord + "&startIndex=0&sug=2_0&n=y&pageSize=60&rpcflag=new&async=true&templateConfigName=marketOfferresult&enableAsync=true&qrwRedirectEnabled=false&asyncCount=20&_pageName_=market&offset=9&uniqfield=pic_tag_id&beginPage=" + i + "");
				Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url() + keyWord + "&beginPage=" + i);
				Map.put("coding", "GBK");
				Thread.sleep(mi);//程序休眠
			}
		} catch (Exception e) {
			logger.info("请求1688数据失败" + e.getMessage());
		}
		return Map;
	}

	//淘宝众筹
	public Map<String, String> platformTaobaoZc_Data(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception {
		Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));
		Map.put("count", String.valueOf(i));
		Map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));
		return Map;
	}

	//京东众筹
	public Map<String, String> platformJdZc_Data(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception {
		Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));
		Map.put("count", String.valueOf(i));
		Map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));
		return Map;
	}

	//唯品会
	public Map<String, String> platformVIp_Data(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception {
		String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name().trim(), "UTF-8");//+
		Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)).replace("SEARCH", keyWord));
		Map.put("count", String.valueOf(i));
		Map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)).replace("SEARCH", keyWord));
		return Map;
	}

	//考拉
	public Map<String, String> platformKaola_Data(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception {
		String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name().trim(), "UTF-8");//+
		Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)).replace("SEARCH", keyWord));
		Map.put("count", String.valueOf(i));
		Map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)).replace("SEARCH", keyWord));
		return Map;
	}

	//酒仙网
	public Map<String, String> platformJx_Data(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception {
		String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name().trim(), "UTF-8");//+
		Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url() + "?key=" + keyWord + "&area=" + i);
		Map.put("count", String.valueOf(i));
		Map.put("url", crawKeywordsInfo.getCust_keyword_url() + "?key=" + keyWord + "&area=" + i);
		return Map;
	}

	//国美
	public Map<String, String> platformGome_Data(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception {
		String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name().trim(), "UTF-8");//+
		String brand="";
		if(StringUtils.isNoneBlank(crawKeywordsInfo.getCust_keyword_brand())) {
			brand="&facets="+crawKeywordsInfo.getCust_keyword_brand();
		}
		Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url()+ keyWord + "&page=" + i+brand);
		Map.put("count", String.valueOf(i));//https://m.gome.com.cn/category.html?from=1&scat=2&key_word=%E6%B5%B7%E5%B0%94&page=30
		Map.put("url", crawKeywordsInfo.getCust_keyword_url()+ keyWord + "&page=" + i+brand);
		return Map;
	}


	//孩子王
	public Map<String, String> platformKidswant_Data(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception {
		if (crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_KIDSWANT)) {
			String productName = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name().trim(), "UTF-8");
			String url = "https://ase.cekid.com/ase-web/product/queryProductIndex.do?wd=%7B\"userId\"%3A0%2C\"address\"%3A\"210000_210800_210882\"%2C\"start\"%3A0%2C\"rows\"%3A1000%2C\"siteId\"%3A1%2C\"categoryIds\"%3A%5B%5D%2C\"skuMetaAttrs\"%3A%5B%5D%2C\"sortInfos\"%3A%5B%5D%2C\"keyStr\"%3A\"" + productName + "\"%2C\"districtId\"%3A1%2C\"sceneId\"%3A1%2C\"sourceId\"%3A1%2C\"multiPriceClose\"%3Atrue%2C\"priceFilter\"%3A\"*\"%2C\"operationModelFilter\"%3A\"\"%7D";
			Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url() + "?key=" + productName);
			Map.put("url", url);
		} else if (crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_KIDSWANT_OFF)) {
			String productName = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
			String url = "https://ase.cekid.com/ase-web/product/queryProductIndex.do?wd=%7B\"userId\"%3A0%2C\"address\"%3A\"210000_210800_210882\"%2C\"start\"%3A0%2C\"rows\"%3A1000%2C\"siteId\"%3A1%2C\"categoryIds\"%3A%5B%5D%2C\"skuMetaAttrs\"%3A%5B%5D%2C\"sortInfos\"%3A%5B%5D%2C\"keyStr\"%3A\"" + productName + "\"%2C\"districtId\"%3A1%2C\"sceneId\"%3A1%2C\"sourceId\"%3A1%2C\"multiPriceClose\"%3Atrue%2C\"priceFilter\"%3A\"*\"%2C\"operationModelFilter\"%3A\"\"%2C\"storeId\"%3A\"" + crawKeywordsInfo.getPlatform_shopid() + "\"%7D";

			Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url() + "https://so.cekid.com/shop/all?key=" + productName + "&shopid=" + crawKeywordsInfo.getPlatform_shopid() + "");
			Map.put("url", url);
		}
		Map.put("count", String.valueOf(i));

		return Map;
	}

	//新欧汇
	public Map<String, String> platformXoh_Data(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception {
		String productName = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
		String url = "http://www.xinouhui.com/goodslist/cid/0/bid/0/trade/0,0,0/xl/0/fh/0,0/goods_name/" + productName + "/bs/1/p/" + i;
		Map.put("urlRef", url);
		Map.put("url", url); 
		Map.put("count", String.valueOf(i));

		return Map;
	}
	//俞欧汇
	public Map<String, String> platformYoh_Data(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception {
		String productName = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
		String url = "https://www.yuouhui.com/search?keywords="+productName+"&page="+i;
		Map.put("urlRef", url);
		Map.put("url", url);
		Map.put("coding", "utf-8");
		// Map.put("cookie", "PHPSESSID=k5b9jhqcnnv395fu79m8d1r2g7; _csrf=dda99c577e1de5600b896f28228e7b8ab42f267f71331db5a1f41becf7ed9217a%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22_csrf%22%3Bi%3A1%3Bs%3A32%3A%22McYRdQ5g93YRu-2oKpF8ofbIgQOEvyeP%22%3B%7D; SERVER_ID=5d31af81-e40d4d59; Hm_lvt_3001f5e5ce76bbc66757b611850d27bc=1575282935; UM_distinctid=16ec62e04be54d-099ad7b0f82b41-5373e62-100200-16ec62e04bf9c7; recent_visit=f9df9aa395b6ebb4ba50a7f2c79a730f308b010607a14fd58042ba7641324688a%3A2%3A%7Bi%3A0%3Bs%3A12%3A%22recent_visit%22%3Bi%3A1%3Bs%3A18%3A%22601000003222-01760%22%3B%7D; CNZZDATA1267424669=296421961-1575282934-%7C1575334406; Hm_lpvt_3001f5e5ce76bbc66757b611850d27bc=1575339854");
		Map.put("count", String.valueOf(i));

		return Map;
	}
	//圣卡斯
	public Map<String, String> platformSks_Data(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception {
		String productName = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");

		Map.put("urlRef", crawKeywordsInfo.getCust_keyword_ref().replace("PAGE", String.valueOf(i)).replace("SEARCH", productName));
		Map.put("count", String.valueOf(i));
		Map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)).replace("SEARCH", productName));

		Map.put("coding", "utf-8");
		Map.put("count", String.valueOf(i));
		if(StringUtils.isEmpty(codeCookie)){
			codeCookie="SHOPEX_SID_MEMBER=7ceae75272d2121503d1bf7ce28f64a5; S[loginName]=yd15800689786; S[MEMBER]=1205-0fada46b1c586b37825084a385e26909-aaaf6c0077d3c433ea21d494da041a73-1575511849; S[UNAME]=yd15800689786; S[MLV]=1; S[CUR]=CNY; S[CART_COUNT]=0";
		}
		Map.put("cookie", codeCookie);
		return Map;
	}
	//海拍客
	public Map<String, String> platformHpk_Data(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception {
		String productName = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
		String sign="";
		try {
			if(crawKeywordsInfo.getCategory_id().contains("&&")){
				sign=crawKeywordsInfo.getCategory_id().split("&&")[i-1]; 
			}else{
				sign=crawKeywordsInfo.getCategory_id();
			}
			String url="https://api.hipac.cn/process/prod/1.0.2/mall.item.searchItemListWithFlashBuyAct.pc/?appKey=1300&data=%7B%22pageNo%22%3A"+i
					+ "%2C%22pageSize%22%3A20%2C%22searchkey%22%3A%22"+productName+"%22%2C%22sortType%22%3A%223%22%2C%22"
					+ "itemSearchTypes%22%3A%22%22%2C%22itemChildTypes%22%3Anull%2C%22searchSource%22%3A%22key%22%2C%22dev%22%3A%22prod%22%7D&os=Chrome&"+sign;
			Map.put("urlRef", url);
			Map.put("url", url);
			Map.put("coding", "utf-8");
			Map.put("count", String.valueOf(i));
			Map.put("cookie", codeCookie);
		} catch (Exception e) {
			logger.info("海拍客连接拼接异常："+e);
		}
		return Map;
	}

	//苏宁
	public Map<String, String> suningdData(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception {
		String StringMessage = "";
		String disable = "";
		if (crawKeywordsInfo.getCust_keyword_url().contains("search.suning.com")) {//苏宁搜索
			String code = "021";
			if (StringUtil.isNotEmpty(codeCookie)) {
				code = StringHelper.getResultByReg(codeCookie, "cityCode=([0-9]+);");
				Map.put("cookie", codeCookie);
			}
			for (int kk = 0; kk < 2; kk++) {
				String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");//+
				String brand="";
				if(StringUtils.isNoneBlank(crawKeywordsInfo.getCust_keyword_brand())) {
					String bran = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_brand(), "UTF-8");
					brand="&hf=brand_Name_FacetAll:"+bran;
				}
				Map.put("url", Fields.SUNING_SEARCH_PC_REF.replace("KEYWORD", keyWord).replace("PAGE", String.valueOf(i - 1)).replace("CITY", code)+brand);
				if (kk == 1) {
					Map.put("url", Fields.SUNING_SEARCH_PC_REF.replace("KEYWORD", keyWord).replace("PAGE", String.valueOf(i - 1)).replace("CITY", code) + "&paging=1&sub=0"+brand);
				}
				Map.put("urlRef", setUrl+ keyWord + "/"+brand);
				Map.put("count", String.valueOf(i));
				logger.info(Map.get("url") + "----------当前商品第：" + i + "页--------" + Map.get("url"));
				StringMessage = httpClientService.interfaceSwitch(Map.get("ip"), Map);//请求数据
				//StringMessage=httpClientService.GoodsProduct(Map);//抓取Itemyem
				disable = search_DataCrawlService.ParseItemPage(StringMessage, Map.get("database"), crawKeywordsInfo, Map);//解析数据
				if (disable.contains(Fields.DISABLE)) {
					Map.put(Fields.DISABLE, Fields.DISABLE);
					return Map;
				}
			}
		}
		Map.put(Fields.DISABLE, disable);
		return Map;
	}

	//亚马逊
	public Map<String, String> amazonData(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception {
		if (crawKeywordsInfo.getCust_keyword_url().contains("www.amazon.com")) {
			if ((crawKeywordsInfo.getCust_keyword_type().contains("category"))) {
				Map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));
			} else {
				String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");//+
				Map.put("url", Fields.AMAZON_URL_KEYWORD.replace("KEYWORD", keyWord).replace("PAGE", String.valueOf(i)));
			}
		} else if (crawKeywordsInfo.getCust_keyword_url().contains("www.amazon.cn")) {
			if ((crawKeywordsInfo.getCust_keyword_type().contains("category"))) {
				Map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));
			} else {
				String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");//+
				Map.put("url", Fields.AMAZON_URL_KEYWORD_CN.replace("KEYWORD", keyWord).replace("PAGE", String.valueOf(i)));
			}

		}
		return Map;
	}

	//海尔
	public Map<String, String> platformEhaier(Map<String, String> Map, CrawKeywordsInfo crawKeywordsInfo, int i){
		try {
			String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name().trim(), "UTF-8");
			String url=crawKeywordsInfo.getCust_keyword_url();
			Map.put("url", url.replace("SEARCH", keyWord)); 
			Map.put("count", String.valueOf(i));
			Map.put("urlRef",crawKeywordsInfo.getCust_keyword_ref());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return Map;
	}

	public String getItemhpk(int i,String productName){
		String coding="gbk";
		String item="";
		try {
			if(productName.equalsIgnoreCase("爱他美")) {
				if(i==8 || i==9) {
					coding="utf-8";
				}
			}
			item = FileUtils.readFileToString(new File("D://hpk//"+productName+"//"+i+".txt"),coding);
			logger.info("当前获取的商品是："+productName+",当前请求的数据是第："+i+"页");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return item;	
	}

	public CrawlerPublicClassService getCrawlerPublicClassService() {
		return crawlerPublicClassService;
	}

	public void setCrawlerPublicClassService(
			CrawlerPublicClassService crawlerPublicClassService) {
		this.crawlerPublicClassService = crawlerPublicClassService;
	}

	public HttpClientService getHttpClientService() {
		return httpClientService;
	}

	public void setHttpClientService(HttpClientService httpClientService) {
		this.httpClientService = httpClientService;
	}

	public WhetherShelves getWhetherShelves() {
		return whetherShelves;
	}

	public void setWhetherShelves(WhetherShelves whetherShelves) {
		this.whetherShelves = whetherShelves;
	}

	public CrawKeywordsInfo getCrawKeywordsInfo() {
		return crawKeywordsInfo;
	}

	public void setCrawKeywordsInfo(CrawKeywordsInfo crawKeywordsInfo) {
		this.crawKeywordsInfo = crawKeywordsInfo;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getSetUrl() {
		return setUrl;
	}

	public void setSetUrl(String setUrl) {
		this.setUrl = setUrl;
	}

	public String getIPPROXY() {
		return IPPROXY;
	}

	public void setIPPROXY(String iPPROXY) {
		IPPROXY = iPPROXY;
	}

	public String getTimeDate() {
		return timeDate;
	}

	public void setTimeDate(String timeDate) {
		this.timeDate = timeDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCodeCookie() {
		return codeCookie;
	}

	public void setCodeCookie(String codeCookie) {
		this.codeCookie = codeCookie;
	}

	public String getStatus_type() {
		return status_type;
	}

	public void setStatus_type(String status_type) {
		this.status_type = status_type;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public Map<String, Object> getSearchMap() {
		return searchMap;
	}

	public void setSearchMap(Map<String, Object> searchMap) {
		this.searchMap = searchMap;
	}
}
