/**   
 * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.task 
 * @author: jack.zhao   
 * @date: 2017年11月23日 下午4:00:14 
 */
package com.eddc.task;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.crawl.JumeiDataCrawlService;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.Fields;
import com.eddc.util.StringHelper;
import com.eddc.util.WhetherShelves;
import com.eddc.util.publicClass;
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Jumei_DataCrawlTask   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2017年11月23日 下午4:00:14   
 * 修改人：jack.zhao   
 * 修改时间：2017年11月23日 下午4:00:14   
 * 修改备注：   
 * @version    
 *    
 */
@Component
@Scope("prototype")
public class Jumei_DataCrawlTask implements  Callable<Map<String, Object>>{
	Logger logger = LoggerFactory.getLogger(Jumei_DataCrawlTask.class);
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	private WhetherShelves whetherShelves;
	@Autowired
	private JumeiDataCrawlService jumei_DataCrawlService;
	private CommodityPrices commodityPrices;
	private CrawKeywordsInfo crawKeywordsInfo;
	private Map<String,Object>jumeiData;
	private String accountId;
	private String dataType;
	private String setUrl;
	private String IPPROXY;
	private String Sum; 
	private String timeDate;
	private String type;
	private String ip;
	private String database;
	private String storage;
	public Map<String, Object> call() throws Exception {//call解析数据
		Map<String,String>Map=new HashMap<String, String>();
		Map=publicClass.parameterMap(setUrl,accountId,dataType,timeDate,IPPROXY);
		if(Fields.STYPE_1.equals(type)){//解析数据
			logger.info("【聚美平台开始解析数据"+"-------"+this.Sum+"---------------】");
			try {
				String goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_JUMEI);//生成goodSId
				Map.put("egoodsId", crawKeywordsInfo.getCust_keyword_name());
				Map.put("keywordId", crawKeywordsInfo.getCust_keyword_id());
				Map.put("goodsId", goodsId);
				String StringMessage=httpClientService.interfaceSwitch(ip, Map);//请求数据
				String status =whetherShelves.parseGoodsStatusJumei(StringMessage,crawKeywordsInfo.getCust_keyword_name());//判断商品是否下架
				Map<String, Object> sum_status=crawlerPublicClassService.InsertDatStatus(status,database,crawKeywordsInfo,goodsId,timeDate,dataType,setUrl,storage);
				if(Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_OFF) || Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_EXCEPTION)){
					jumeiData.putAll(sum_status);
					logger.info("【"+crawKeywordsInfo.getPlatform_name() + " >>> The goods have been removed from the shelves_"
                            + Map.get("egoodsId") + " SOLD OUT : >>>>>>>>>>>>>>>>>>>>>>>" + Sum + ">>>】");
					return jumeiData; 
				}
				Map<String, Object> itemData=jumei_DataCrawlService.JumeiParseItemPage(StringMessage,crawKeywordsInfo,Map,ip,database,storage);//解析数据
				jumeiData.putAll(itemData);
			} catch (Exception e) { 
				logger.error("【解析聚美数据失败- error:{}】",e);
			}

		}else if(Fields.STYPE_6.equals(type)){
			try {
				Thread.sleep(1000);
				Map.put("egoodsId", commodityPrices.getEgoodsId());
				String AppMessage=httpClientService.interfaceSwitch(ip, Map);//请求数据
				commodityPrices.setBatch_time(timeDate);//重新赋值
				Map<String, Object> price=jumei_DataCrawlService.ResolutionCellPhonePrices(commodityPrices, AppMessage,Map,database,storage);//解析数据
				jumeiData.putAll(price);
				logger.info("【>>> get current price finish >>>>>>>>>>>>>>>SUCCESS>>>>>>>>>>>>"+Sum+">>>>>>>>>>>】");
				
			} catch (InterruptedException e) {
				  logger.error("【请求聚美PC数据失败 error:{}】",e);
			} catch (Exception e) {
				logger.error("【请求聚美PC数据失败 error:{}】",e);
			}
			
		}
		return jumeiData; 
	}
	public CrawlerPublicClassService getCrawlerPublicClassService() {
		return crawlerPublicClassService;
	}
	public void setCrawlerPublicClassService(
			CrawlerPublicClassService crawlerPublicClassService) {
		this.crawlerPublicClassService = crawlerPublicClassService;
	}

	public HttpClientService getHttpClientService() {
		return httpClientService;
	}

	public void setHttpClientService(HttpClientService httpClientService) {
		this.httpClientService = httpClientService;
	}
	public WhetherShelves getWhetherShelves() {
		return whetherShelves;
	}

	public void setWhetherShelves(WhetherShelves whetherShelves) {
		this.whetherShelves = whetherShelves;
	}
	public CommodityPrices getCommodityPrices() {
		return commodityPrices;
	}
	public void setCommodityPrices(CommodityPrices commodityPrices) {
		this.commodityPrices = commodityPrices;
	}
	public CrawKeywordsInfo getCrawKeywordsInfo() {
		return crawKeywordsInfo;
	}
	public void setCrawKeywordsInfo(CrawKeywordsInfo crawKeywordsInfo) {
		this.crawKeywordsInfo = crawKeywordsInfo;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getSetUrl() {
		return setUrl;
	}
	public void setSetUrl(String setUrl) {
		this.setUrl = setUrl;
	}
	public String getIPPROXY() {
		return IPPROXY;
	}
	public void setIPPROXY(String iPPROXY) {
		IPPROXY = iPPROXY;
	}
	public String getSum() {
		return Sum;
	}
	public void setSum(String sum) {
		Sum = sum;
	}
	public String getTimeDate() {
		return timeDate;
	}
	public void setTimeDate(String timeDate) {
		this.timeDate = timeDate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Map<String, Object> getJumeiData() {
		return jumeiData;
	}
	public void setJumeiData(Map<String, Object> jumeiData) {
		this.jumeiData = jumeiData;
	}
	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}
	public String getStorage() {
		return storage;
	}
	public void setStorage(String storage) {
		this.storage = storage;
	}

}
