package com.eddc.task;
import java.util.Map;
import java.util.concurrent.Callable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.Parameter;
import com.eddc.service.impl.crawl.CommentParticipleDataCrawlService;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.Fields;
import com.eddc.util.WhetherShelves;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Data
@Component
@Scope("prototype")
public class CommentParticiple_DataCrawlTask  implements Callable<Map<String, Object>> {
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	private CommentParticipleDataCrawlService commentParticipleDataCrawlService;
	@Autowired
	private WhetherShelves whetherShelves;
	private Map<String,Object>comment;
	private Parameter parameter;

	public Map<String, Object> call() throws Exception { 
		String database=parameter.getListjobName().get(0).getDatabases();
		if(Fields.STYPE_1.equals(parameter.getType())){//解析数据
			log.info(parameter.getListjobName().get(0).getPlatform()+"【平台开始解析数据,当前解析的是第：{} 个商品】",parameter.getSum());
			try { 
				String StringMessage=httpClientService.interfaceSwitch(parameter);//请求数据
				//log.info(StringMessage);
				String status =whetherShelves.parseGoodsStatusByItemPage(StringMessage);//判断商品是否下架
				if(status.equals(Fields.COUNT_0)){
					//插入下架商品更新商品状态
					crawlerPublicClassService.soldOutStatus(Fields.COUNT_0,database,parameter);
				}else{
					//解析商品数据
					if(StringMessage.contains("askAroundDisabled")|| StringMessage.contains("commodityLabelCountList")||StringMessage.contains(parameter.getEgoodsId()) || StringMessage.contains(parameter.getCrawKeywordsInfo().getPlatform_shopid())){
						try {
							comment=commentParticipleDataCrawlService.itemAppPage(StringMessage, parameter);
						} catch (Exception e) {
							e.printStackTrace();
						}

					}else{
						log.info("【请求"+parameter.getListjobName().get(0).getPlatform()+"评论分词数据失败。。。。】");
					}

				}
			} catch (Exception e) { 
				log.error("【解析"+parameter.getListjobName().get(0).getPlatform()+"评论分词数据失败- error:{}】",e);
			}
		}
		return comment; 
	}
}
