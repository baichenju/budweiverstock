package com.eddc.task;
import java.io.File;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.Parameter;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.crawl.XohDataCrawlService;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.Fields;
import com.eddc.util.WhetherShelves;
import com.xxl.job.core.log.XxlJobLogger;
@Component
@Scope("prototype")
public class XohDataCrawlTask  implements Callable<Map<String, Object>> {
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	private XohDataCrawlService xohDataCrawlService;
	@Autowired
	private WhetherShelves whetherShelves;
	private Map<String,Object>XohData;
	private Parameter parameter;

	public Map<String, Object> call() throws Exception { 
		if(Fields.STYPE_1.equals(parameter.getType())){
			XxlJobLogger.log("【新欧汇平台开始解析数据,当前解析的是第：{} 个商品】",parameter.getSum());
			//XxlJobLogger.log("【新欧汇平台开始解析数据,当前解析的是第：{} 个商品】",parameter.getSum());
			try { 

				String serchItem=httpClientService.getOkHttpClient(parameter);
				
				if(StringUtils.isNoneBlank(serchItem)) {
					if(!serchItem.contains("没有找到符合的商品")){
						XohData=xohDataCrawlService.itemAppPage(serchItem, parameter);
					}else{
						XxlJobLogger.log("没有找到符合的商品。。。。。。 /n"+parameter.getItemUrl_1());
					}
				}

			} catch (Exception e) { 
				XxlJobLogger.log("【解析新欧汇数据失败- error:{}】",e);
			}
		}
		return XohData; 
	}

	public CrawlerPublicClassService getCrawlerPublicClassService() {
		return crawlerPublicClassService;
	}

	public void setCrawlerPublicClassService(CrawlerPublicClassService crawlerPublicClassService) {
		this.crawlerPublicClassService = crawlerPublicClassService;
	}

	public HttpClientService getHttpClientService() {
		return httpClientService;
	}

	public void setHttpClientService(HttpClientService httpClientService) {
		this.httpClientService = httpClientService;
	}

	public XohDataCrawlService getXohDataCrawlService() {
		return xohDataCrawlService;
	}

	public void setXohDataCrawlService(XohDataCrawlService xohDataCrawlService) {
		this.xohDataCrawlService = xohDataCrawlService;
	}

	public WhetherShelves getWhetherShelves() {
		return whetherShelves;
	}

	public void setWhetherShelves(WhetherShelves whetherShelves) {
		this.whetherShelves = whetherShelves;
	}

	public Map<String, Object> getXohData() {
		return XohData;
	}

	public void setXohData(Map<String, Object> xohData) {
		XohData = xohData;
	}

	public Parameter getParameter() {
		return parameter;
	}

	public void setParameter(Parameter parameter) {
		this.parameter = parameter;
	}

}
