/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.task 
 * @author: jack.zhao   
 * @date: 2018年5月11日 下午3:04:51 
 */
package com.eddc.task;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.eddc.model.Craw_aliindex_category_info;
import com.eddc.model.Craw_aliindex_goodsrank_info;
import com.eddc.service.impl.crawl.AlibabaIndexDataCrawlService;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.Fields;
import com.eddc.util.publicClass;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：AlibabaIndex_DataCrawlTask   
 * 类描述：  阿里指数》产品排行榜
 * 创建人：jack.zhao   
 * 创建时间：2018年5月11日 下午3:04:51   
 * 修改人：jack.zhao   
 * 修改时间：2018年5月11日 下午3:04:51   
 * 修改备注：   
 * @version    
 *    
 */
@Component
@Scope("prototype")
public class AlibabaIndex_DataCrawlTask extends Thread{
	Logger logger = LoggerFactory.getLogger(AlibabaIndex_DataCrawlTask.class);
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	AlibabaIndexDataCrawlService alibabaIndex_DataCrawlService;
	private String url;
	private String ipProxy;
	private int  rank_dimention_id;//交易 流量
	private int rank_period_id;//7天 30天
	private String accountId;
	private String status_type;
	private String platform_name;
	private String ip;
	private String database;
	private Craw_aliindex_category_info info;
	private Craw_aliindex_goodsrank_info goodsrank;
	public void run() {
		Map<String,String>Map=new HashMap<String, String>();
		Map=publicClass.parameterMap(url,accountId,platform_name,null,ipProxy);
		String StringMessage="";
		try {
			if(status_type.equals(Fields.STYPE_1)){
				StringMessage=httpClientService.interfaceSwitch(ip, Map);//请求数据
				alibabaIndex_DataCrawlService.aliRequestDataInsert(info,StringMessage,rank_dimention_id,rank_period_id,database);//解析数据	
			}else if(status_type.equals(Fields.STYPE_2)){//获取相似的商品
				 Base64 base64 = new Base64();
				 String appKey="searchweb;"+System.currentTimeMillis();
				 byte[] textByte = appKey.getBytes("UTF-8");
				 String appkeys=new String(base64.encodeToString(textByte));
				Map.put("url", Fields.ALI_INDEX_SIMILAR_PRODUCTS.replace("EGOODSID", goodsrank.getEgoods_id()).replace("APPKEY", appkeys));
				Map.put("coding", "GBK");
				StringMessage=httpClientService.interfaceSwitch(ip, Map);//请求数据
				alibabaIndex_DataCrawlService.indexSimilarGoodsData(goodsrank,StringMessage,database);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * @return the crawlerPublicClassService
	 */
	public CrawlerPublicClassService getCrawlerPublicClassService() {
		return crawlerPublicClassService;
	}
	/**
	 * @param crawlerPublicClassService the crawlerPublicClassService to set
	 */
	public void setCrawlerPublicClassService(CrawlerPublicClassService crawlerPublicClassService) {
		this.crawlerPublicClassService = crawlerPublicClassService;
	}
	/**
	 * @return the info
	 */
	public Craw_aliindex_category_info getInfo() {
		return info;
	}
	/**
	 * @param info the info to set
	 */
	public void setInfo(Craw_aliindex_category_info info) {
		this.info = info;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the ipProxy
	 */
	public String getIpProxy() {
		return ipProxy;
	}
	/**
	 * @param ipProxy the ipProxy to set
	 */
	public void setIpProxy(String ipProxy) {
		this.ipProxy = ipProxy;
	}
	/**
	 * @return the rank_dimention_id
	 */
	public int getRank_dimention_id() {
		return rank_dimention_id;
	}
	/**
	 * @param rank_dimention_id the rank_dimention_id to set
	 */
	public void setRank_dimention_id(int rank_dimention_id) {
		this.rank_dimention_id = rank_dimention_id;
	}
	/**
	 * @return the rank_period_id
	 */
	public int getRank_period_id() {
		return rank_period_id;
	}
	/**
	 * @param rank_period_id the rank_period_id to set
	 */
	public void setRank_period_id(int rank_period_id) {
		this.rank_period_id = rank_period_id;
	}
	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}
	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	/**
	 * @return the goodsrank
	 */
	public Craw_aliindex_goodsrank_info getGoodsrank() {
		return goodsrank;
	}
	/**
	 * @param goodsrank the goodsrank to set
	 */
	public void setGoodsrank(Craw_aliindex_goodsrank_info goodsrank) {
		this.goodsrank = goodsrank;
	}
	/**
	 * @return the status_type
	 */
	public String getStatus_type() {
		return status_type;
	}
	/**
	 * @param status_type the status_type to set
	 */
	public void setStatus_type(String status_type) {
		this.status_type = status_type;
	}
	/**
	 * @return the platform_name
	 */
	public String getPlatform_name() {
		return platform_name;
	}
	/**
	 * @param platform_name the platform_name to set
	 */
	public void setPlatform_name(String platform_name) {
		this.platform_name = platform_name;
	}
	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * @return the database
	 */
	public String getDatabase() {
		return database;
	}
	/**
	 * @param database the database to set
	 */
	public void setDatabase(String database) {
		this.database = database;
	}
	
}
