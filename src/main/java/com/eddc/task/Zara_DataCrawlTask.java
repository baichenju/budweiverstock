///**   
// * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
// * 
// * @Package: com.eddc.task 
// * @author: jack.zhao   
// * @date: 2017年11月16日 下午5:41:14 
// */
//package com.eddc.task;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Scope;
//import org.springframework.stereotype.Component;
//import com.eddc.service.CrawlerPublicClassService;
//import com.eddc.service.HttpClientService;
//import com.eddc.service.Zara_DataCrawlService;
//import com.eddc.util.SimpleDate;
//import com.eddc.util.Validation;
//import com.eddc.util.publicClass;
//
///**   
// *    
// * 项目名称：Price_monitoring_crawler   
// * 类名称：Zara_DataCrawlTask   
// * 类描述：   
// * 创建人：jack.zhao   
// * 创建时间：2017年11月16日 下午5:41:14   
// * 修改人：jack.zhao   
// * 修改时间：2017年11月16日 下午5:41:14   
// * 修改备注：   
// * @version    
// *    
// */
//@Component
//@Scope("prototype")
//public class Zara_DataCrawlTask extends Thread {
//	Logger logger = LoggerFactory.getLogger(Zara_DataCrawlTask.class);
//	@Autowired
//	CrawlerPublicClassService crawlerPublicClassService;
//	@Autowired
//	HttpClientService httpClientService;
//	@Autowired
//	Zara_DataCrawlService zara_DataCrawlService;
//	private String setUrl;
//	private String IPPROXY;
//	private String Sum;
//	private int userId;
//	private String dataType;
//	private String  series;
//	private String categories;
//	private String ip;
//	public void run() {
//		logger.info("【ZARA平台开始解析数据"+"-------"+this.Sum+"---------------】");
//		Map<String, String>Map=new HashMap<String, String>();
//		Map=publicClass.parameterMap(setUrl,String.valueOf(userId),dataType,null,IPPROXY);	 
//		Map.put("series", series);
//		Map.put("categories", categories);
//		try {
//			String StringMessage=httpClientService.getJsonObj(setUrl);//抓取Itemyem
//			if(Validation.isEmpty(StringMessage)){
//				  StringMessage=httpClientService.interfaceSwitch(ip, Map);//请求数据
//			}
//			if(!Validation.isEmpty(StringMessage)){
//				zara_DataCrawlService.zaraParseItemPage(StringMessage,Map);//解析数据
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		} 
//	}
//	
//	
//	public String getSeries() {
//		return series;
//	}
//
//	public void setSeries(String series) {
//		this.series = series;
//	}
//
//	public String getCategories() {
//		return categories;
//	}
//
//	public void setCategories(String categories) {
//		this.categories = categories;
//	}
//	public String getDataType() {
//		return dataType;
//	}
//	public void setDataType(String dataType) {
//		this.dataType = dataType;
//	}
//	public String getSetUrl() {
//		return setUrl;
//	}
//	public void setSetUrl(String setUrl) {
//		this.setUrl = setUrl;
//	}
//	public String getIPPROXY() {
//		return IPPROXY;
//	}
//	public void setIPPROXY(String iPPROXY) {
//		IPPROXY = iPPROXY;
//	}
//	public String getSum() {
//		return Sum;
//	}
//	public void setSum(String sum) {
//		Sum = sum;
//	}
//	public int getUserId() {
//		return userId;
//	}
//	public void setUserId(int userId) {
//		this.userId = userId;
//	}
//	public String getIp() {
//		return ip;
//	}
//	public void setIp(String ip) {
//		this.ip = ip;
//	}
//	
//}
