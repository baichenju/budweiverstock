package com.eddc.task;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.enums.PlatformEnum;
import com.eddc.model.Parameter;
import com.eddc.util.SpringContextUtil;
import com.xxl.job.core.log.XxlJobLogger;

/**
 * @author jack.zhao
 */
@Component
@Scope("prototype")
public class AllDataCrawlTask implements Callable<Map<String, Object>> {
	private Map<String, Object> allData;
	private Parameter parameter;
	private static Logger logger=LoggerFactory.getLogger(AllDataCrawlTask.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> call() throws Exception {
		logger.info("==>>当前任务名称：" + parameter.getListjobName().get(0).getJob_name() + ", 平台:" + parameter.getListjobName().get(0).getPlatform() + " 开始解析数据,当前解析的是第：{} 个商品，商品Id：{}<<==", parameter.getSum(), parameter.getEgoodsId());
		try {
			String platform = parameter.getListjobName().get(0).getPlatform().toLowerCase();
		
			ApplicationContext contexts= SpringContextUtil.getApplicationContext();
			if(platform.equalsIgnoreCase(PlatformEnum.KIDSWANTOFF.getCodeEn())) {
				platform=PlatformEnum.KIDSWANT.getCodeEn();
			}
			String classs=platform+"DataCrawlService";
			String productMethod="getProductLink";
			Class<?> classes = contexts.getBean(classs).getClass();
			Method method =classes.getMethod(productMethod, Parameter.class);
			allData = (Map<String, Object>) method.invoke(classes.newInstance(),parameter);
		} catch (Exception e) {
			XxlJobLogger.log("==>>解析" + parameter.getListjobName().get(0).getPlatform() + "数据失败- error:{"+e+"}<<==");
			e.printStackTrace();
		}
		return allData;
	}



	public Map<String, Object> getAllData() {
		return allData;
	}

	public void setAllData(Map<String, Object> allData) {
		this.allData = allData;
	}

	public Parameter getParameter() {
		return parameter;
	}

	public void setParameter(Parameter parameter) {
		this.parameter = parameter;
	}
}
