package com.eddc.task;
import java.util.Map;
import java.util.concurrent.Callable;
import com.eddc.service.*;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.crawl.PddDataCrawlService;
import com.eddc.service.impl.http.HttpClientService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.Parameter;
import com.eddc.util.WhetherShelves;
import com.xxl.job.core.log.XxlJobLogger;
@Component
@Scope("prototype")
public class Pdd_DataCrawlTask  implements Callable<Map<String, Object>> {
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;

	@Autowired
	private WhetherShelves whetherShelves;
	@Autowired
	private PddDataCrawlService pinDuoDuoDataCrawlService;
	private Map<String,Object>allData;
	private Parameter parameter;
	public Map<String, Object> call() throws Exception { 
		XxlJobLogger.log("==>>当前任务名称："+parameter.getListjobName().get(0).getJob_name()+", 平台:"+parameter.getListjobName().get(0).getPlatform()+" 开始解析数据,当前解析的是第：{} 个商品，商品Id：{}<<==",parameter.getSum(),parameter.getEgoodsId());
		try { 
			String platform=parameter.getListjobName().get(0).getPlatform().toLowerCase();
			switch (platform ) {
			case "pdd":
				allData=pinDuoDuoDataCrawlService.parameter(parameter);
				break;
			}
		} catch (Exception e) { 
			XxlJobLogger.log("==>>解析"+parameter.getListjobName().get(0).getPlatform()+"数据失败- error:{}<<==",e);
		}
		return allData; 
	}
	public CrawlerPublicClassService getCrawlerPublicClassService() {
		return crawlerPublicClassService;
	}
	public void setCrawlerPublicClassService(CrawlerPublicClassService crawlerPublicClassService) {
		this.crawlerPublicClassService = crawlerPublicClassService;
	}
	public HttpClientService getHttpClientService() {
		return httpClientService;
	}
	public void setHttpClientService(HttpClientService httpClientService) {
		this.httpClientService = httpClientService;
	}
	public WhetherShelves getWhetherShelves() {
		return whetherShelves;
	}
	public void setWhetherShelves(WhetherShelves whetherShelves) {
		this.whetherShelves = whetherShelves;
	}
	public PddDataCrawlService getPinDuoDuoDataCrawlService() {
		return pinDuoDuoDataCrawlService;
	}
	public void setPinDuoDuoDataCrawlService(PddDataCrawlService pinDuoDuoDataCrawlService) {
		this.pinDuoDuoDataCrawlService = pinDuoDuoDataCrawlService;
	}
	public Map<String, Object> getAllData() {
		return allData;
	}
	public void setAllData(Map<String, Object> allData) {
		this.allData = allData;
	}
	public Parameter getParameter() {
		return parameter;
	}
	public void setParameter(Parameter parameter) {
		this.parameter = parameter;
	}
	
}
