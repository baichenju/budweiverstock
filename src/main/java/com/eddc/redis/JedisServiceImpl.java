package com.eddc.redis;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import java.util.List;

/**
 * jedis 操作实现类
 * Created by jack.zhao on 2018/3/1.
 */
@Service
@Slf4j
public class JedisServiceImpl implements JedisService {
	@Autowired
	private JedisPool jedisPool;
	/** 
	 * 写入 值 
	 * @param 
	 */  
	@SuppressWarnings("resource")
	public void set(String key, String value) {
		Jedis   jedis=new  Jedis();
		boolean bool=true;
		for(int i=0;i<6;i++){
			try {
				jedis=jedisPool.getResource();
				jedis.set(key,value,"NX", "EX",60*60*12);
				bool=true;
				if(bool){
					break;
				}
			} catch (Exception e) {
				log.error("==>>把值写入redis发生异常："+e.getMessage()+"<<==当前是第："+(i+1)+"请求");
				e.printStackTrace();
				bool=false;
			}
		}
		if(jedis != null){
			jedis.close();
			jedis.quit();
		}	

	}
	/** 
	 * 写入 json格式数据
	 * @param 
	 */  
	@Override
	public void setObj(String key, Object obj) {
		try {
			String value = JSONObject.toJSONString(obj);
			set(key,value);
		} catch (Exception e) {
			log.error("==>>把值写入redis发生异常："+e.getMessage()+"<<==");
			e.printStackTrace();
		}


	}

	/** 
	 * 查询key
	 * @param 
	 */
	@SuppressWarnings("resource")
	@Override
	public String get(String key) {
		String message="";
		boolean bool=true;
		Jedis   jedis=new  Jedis();
		for(int i=0;i<6;i++){
			try {
				jedis=jedisPool.getResource();
				message=jedis.get(key);
				bool=true;
				if(bool){
					break;
				}
			} catch (Exception e) {
				bool=false;
				log.error("==>>获取redis KEY 发生异常："+e.getMessage()+"<<==当前是第："+(i+1)+"请求");
			}
		}
		if(jedis != null){
			jedis.close();
			jedis.quit();
		}	
		return message;
	}

	@Override
	public <T> Object getObj(String key, Class<T> clazz) {
		String objStr = get(key);
		return JSON.parseObject(objStr, clazz);
	}

	/** 
	 * 设置redis有效时间
	 * @param 
	 */
	@SuppressWarnings("resource")
	@Override
	public long expire(String key, int expire) {
		long message=0;
		boolean bool=true;
		Jedis   jedis=new  Jedis();
		for(int i=0;i<6;i++){
			try {
				jedis=jedisPool.getResource();
				message=jedis.expire(key,expire);
				bool=true;
				if(bool){
					break;
				}
			} catch (Exception e) {
				bool=false;
				log.error("==>>设置redis KEY有效时间 发生异常："+e.getMessage()+"<<== 当前是第："+(i+1)+" 请求");
			}
		}
		if(jedis != null){
			jedis.close();
			jedis.quit();
		}	
		return message;
	}

	/** 
	 *往redis存放LIST数据
	 * @param 
	 */ 
	@SuppressWarnings("resource")
	@Override
	public <T> void setList(String key, List<T> list,String redis_time) {
		Jedis   jedis=new  Jedis();
		int sum=0;
		double redis=Double.valueOf(redis_time);
		long time=(long) (60*60*redis);
		for(int i=0;i<5;i++){
			try {
				jedis=jedisPool.getResource();
				jedis.set(key, JSONArray.toJSONString(list),"NX", "EX",time);//(key, JSONArray.toJSONString(list));
				if(sum==0){
					break;
				}
			} catch (Exception e) {
				log.error("==>>往redis 存List  发生异常："+e.getMessage()+"<<==当前是第："+(i+1)+" 请求");
				e.printStackTrace();
				sum=1;
			}
		}
		if(jedis != null){
			jedis.close();
			jedis.quit();
		}	
	}
	/** 
	 *往redis存放LIST JSON数据
	 * @param 
	 */ 
	@SuppressWarnings("resource")
	@Override
	public <T> void setDataList(String key, List<T> list) {
		Jedis   jedis=new  Jedis();
		boolean bool=true;
		for(int i=0;i<6;i++){
			try {
				Gson son=new Gson();
				jedis = jedisPool.getResource();
				jedis.set(key, son.toJson(list),"NX", "EX",60*60*12);
				bool=true;
				if(bool){
					break;
				}
			} catch (Exception e) {
				bool=false;
				log.error("==>>往redis 存toJOSON  发生异常："+e.getMessage()+"<<== 当前是第："+(i+1)+"请求");
			}
		}
		if(jedis != null){
			jedis.close();
			jedis.quit();
		}
	}

	@Override
	public <T> List<T> getList(String key, Class<T> clz) {
		String json = get(key);
		if(json!=null){
			List<T> list = (List<T>) JSONArray.parseArray(json, clz);
			return list;
		}
		return null;
	}

	@Override
	public long lPush(String key, Object obj) {
		String value = JSONObject.toJSONString(obj);
		return jedisPool.getResource().lpush(key,value);
	}

	@Override
	public long rPush(String key, Object obj) {
		String value = JSONObject.toJSONString(obj);
		return jedisPool.getResource().rpush(key,value);
	}

	@Override
	public String lPop(String key) {
		return jedisPool.getResource().lpop(key);
	}

	/**
	 * 判断key是否存在
	 * @param key 键
	 * @return true 存在 false不存在
	 */
	@Override
	public boolean hasKey(String key){
		Jedis   jedis=null;
		boolean bool=true;
		int sum=0;
		for(int i=0;i<5;i++){
			try {
				jedis=jedisPool.getResource();
				bool= jedis.exists(key);
				sum=0;
				if(sum==0){
					break;
				}
			} catch (Exception e) {
				log.error("==>>判断key是否存在 发生异常："+e.getMessage()+"<<==");
				e.printStackTrace();
				sum=1;
				bool= false;
			}
		}
		if(jedis != null){
			jedis.close();
			jedis.quit();
		} 
		return bool;
	}

	@Override
	public void close() {
		jedisPool.close();	
	}

	/** 
	 * 释放jedis资源 
	 * @param jedis 
	 */  
	@SuppressWarnings("deprecation")
	@Override
	public  void returnResource(final Jedis jedis) {  
		if (jedis != null) {
			jedisPool.returnResourceObject(jedis);  
		}  
	}
	@Override
	public boolean del(String key) {
		boolean keyData=true;
		if(hasKey(key)) {
			try {
				jedisPool.getResource().del(key);
			} catch (Exception e) {
				keyData=false;
				e.printStackTrace();
			}

		}
		return keyData;
	}  
}
