package com.eddc.redis;
import java.util.List;

import redis.clients.jedis.Jedis;

public interface JedisService {
	/**
	 * 设置缓存
	 * @param key
	 * @param value
	 * @return
	 */
	void set(String key, String value);

	/**
	 * 设置缓存对象
	 * @param key
	 * @param obj
	 * @return
	 */
	void setObj(String key, Object obj);

	/**
	 * 根据key获取缓存的值
	 * @param key
	 * @return
	 */
	String get(String key);

	/**
	 * 根据key获取缓存的对象
	 * @param key
	 * @param clazz
	 * @return
	 */
	<T> Object getObj(String key,Class<T> clazz);

	/**
	 * 根据key设置缓存过期时间
	 * @param key
	 * @param expire 秒
	 * @return
	 */
	long expire(String key,int expire);

	/**
	 * 设置列表缓存
	 * @param key
	 * @param list
     *@param redis_time
	 * @param <T>
	 * @return
	 */
	<T> void setList(String key ,List<T> list,String redis_time);

	/**
	 * 获取列表缓存
	 * @param key
	 * @param clz
	 * @param <T>
	 * @return
	 */
	<T> List<T> getList(String key, Class<T> clz);

	/**
	 * 插入列表头部
	 * @param key
	 * @param obj
	 * @return
	 */
	long lPush(String key,Object obj);

	/**
	 * 插入列表尾部
	 * @param key
	 * @param obj
	 * @return
	 */
	long rPush(String key,Object obj);

	/**
	 * 移除并返回列表 key 的头元素
	 * @param key
	 * @return
	 */
	String lPop(String key);
	/**
	 * 判断key是否存在
	 * @param key 键
	 * @return true 存在 false不存在
	 */
	public boolean hasKey(String key);

	public <T> void setDataList(String key, List<T> list);
	
	/**
	 *删除缓存
	 * @param key 键
	 */
	public boolean del(String key) ;

	void close();

	/**  
	* @Title: returnResource  
	* @Description: TODO 释放资源
	* @param @param jedis    设定文件  
	* @return void    返回类型  
	* @throws  
	*/  
	void returnResource(Jedis jedis);
}
