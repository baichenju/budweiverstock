package com.eddc.service;

import com.eddc.enums.StatusEnum;
import com.eddc.model.*;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.crawl.SearchDataCrawlService;
import com.eddc.service.impl.crawl.StoreDataCrawlService;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.*;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * @author jack.zhao
 */
@Service("abstractProductAnalysis")
@Slf4j
public abstract class AbstractProductAnalysis implements ProductAnalysisService {
    @Autowired
    private CrawlerPublicClassService crawlerPublicClassService;
    @Autowired
    private HttpClientService httpClientService;
    @Autowired
    private WhetherShelves whetherShelves;
    @Autowired
    private SearchDataCrawlService search_DataCrawlService;
    @Autowired
    public StoreDataCrawlService storeDataCrawlService;
    private ApplicationContext contexts = SpringContextUtil.getApplicationContext();

    /**
     * 请求数据
     *
     * @param parameter 请求参数
     * @author jack.zhao
     * @data 2020/12/2 13:57
     */
    public Map<String, Object> getProductRequest(Parameter parameter) {
        Map<String, Object> itemMap = new HashMap<String, Object>(10);
        httpClientService = (HttpClientService) contexts.getBean(HttpClientService.class);
        crawlerPublicClassService = (CrawlerPublicClassService) contexts.getBean(CrawlerPublicClassService.class);
        storeDataCrawlService = (StoreDataCrawlService) contexts.getBean(StoreDataCrawlService.class);
        whetherShelves = new WhetherShelves();
        try {
            /***
             * 请求商品报文
             */

        	String productItem = getRequest(parameter);

        	if(parameter.getHtmlKb()!=0) {
        		if(productItem.toString().length()/1024<parameter.getHtmlKb()) {
        			productItem=getOkHttpClient(parameter);
        		
        		}
        	}
            /***
             * 判断商品是否下架
             */

            // 针对天猫付费接口中的下架字段进行额外的判断
             String  status = whetherShelves.parseGoodsStatusByItemPage(productItem);
//             if (productItem.contains("\"sellCount\":\"\"") && parameter.getCrawKeywordsInfo().getPlatform_name().equals("tmall")){
//                 status = "0";
//             }
//             对天猫商品进行额外的下架判断
//            try {
//                if (parameter.getCrawKeywordsInfo().getPlatform_name().equals("tmall")) {
//                    String url = "https://show.re.taobao.com/feature_v2?feature_names=itemTitle,pictUrl,icQuantity,totalSold,promoMPrice,promoMBasePrice,preSellPrice,spBiz30day&auction_ids=" + parameter.getEgoodsId();
//                    String temp = parameter.getItemUrl_1();
//                    parameter.setItemUrl_1(url);
//                    String resp = getOkHttpClient(parameter);
//                    JSONObject js = JSONObject.fromObject(resp);
//                    JSONObject info = js.getJSONArray("auction").getJSONObject(0);
//                    if (StringUtils.isBlank(info.getString("spBiz30day"))&&StringUtils.isBlank(info.getString("totalSold"))) {
//                        status = "0";
//                    }
//                    //换回付费接口url
//                    parameter.setItemUrl_1(temp);
//                }
//            }
//            catch (Exception e){
//                log.warn("天猫下架信息解析失败");
//
//            }
            if (status.equalsIgnoreCase(StatusEnum.PRODUCT_STATUS_OFF.getStatus())) {
                log.info("==>>" + parameter.getEgoodsId() + "当前商品已经下架,插入数据库 当前是第:" + parameter.getSum() + "商品<<==");
                crawlerPublicClassService.soldOutStatus(status, parameter.getListjobName().get(0).getDatabases(), parameter);
                return itemMap;
            }
            /***
             * 开始解析数据
             */
            if(StringUtils.isNoneBlank(productItem)) {
            	 itemMap = getProductItemAnalysisData(productItem, parameter);	
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("==>>" + parameter.getListjobName().get(0).getPlatform() + ":解析数据失败 ,error:" + e + "<<==");
        }

        return itemMap;
    }

    /**
     * http 请求数据
     *
     * @param parameter 产品请求参数
     * @author jack.zhao
     * @data 2020/12/2 13:57
     */
    public String getRequest(Parameter parameter) {
		httpClientService = (HttpClientService) contexts.getBean(HttpClientService.class);
        String productItem = httpClientService.interfaceSwitch(parameter);
        return productItem;
    }
    /**
     * Okhttp请求数据
     *
     * @param parameter 产品请求参数
     * @author jack.zhao
     * @data 2020/12/2 13:57
     */
    public String getOkHttpClient(Parameter parameter) {
        httpClientService = (HttpClientService) contexts.getBean(HttpClientService.class);
        String productItem = httpClientService.getOkHttpClient(parameter);
        return productItem;
    }





    /**
     * 数据插入
     *
     * @param productList 商品集合
     * @param database    数据源
     * @param crawTable   数据落地表
     * @author jack.zhao
     * @data 2020/12/2 13:57
     */
    public void getProductSave(List<Map<String, Object>> productList, String database, String crawTable) {
        search_DataCrawlService = (SearchDataCrawlService) contexts.getBean(SearchDataCrawlService.class);
        search_DataCrawlService.insertIntoData(productList, database, crawTable);
    }


    /**
     * 解析数据接口
     *
     * @param StringMessage 请求的报文
     * @param parameter     请求参数
     * @return Map
     * @author jack.zhao
     * @data 2020/12/2 13:57
     */
    public abstract Map<String, Object> getProductItemAnalysisData(String StringMessage, Parameter parameter) throws Exception;


    /**
     * 封装数据List 对象
     *
     * @param  bean 请求对象
     * @return Map
     * @author jack.zhao
     * @data 2020/12/2 13:57
     */
    @SuppressWarnings("unchecked")
	public List<Map<String, Object>>list(Object bean) throws Exception {
        List<Map<String, Object>> productList = Lists.newArrayList();
        Map<String, Object> insertItem = new HashMap<String, Object>(10);
        insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(bean);
        productList.add(insertItem);
        return productList;
    }



    /**
     * 封装商品属性
     *
     * @param key
     * @param vale
     * @param parameter 参数
     * @return Craw_goods_attribute_Info
     * @author jack.zhao
     * @data 2020/12/2 13:57
     */
    public Craw_goods_attribute_Info attributeInfo(String key, String vale, Parameter parameter) {
        Craw_goods_attribute_Info info = new Craw_goods_attribute_Info();
        info.setAttri_value(vale);
        info.setCraw_attri_cn(key);
        info.setEgoodsid(parameter.getEgoodsId());
        info.setGoodsid(parameter.getGoodsId());
        info.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
        info.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
        //info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
        //info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
        info.setBatch_time(parameter.getBatch_time());
        return info;
    }


    /**
     * 封装商品详情
     *
     * @param parameter 参数
     * @return Craw_goods_attribute_Info
     * @author jack.zhao
     * @data 2020/12/2 13:57
     */
    public Craw_goods_InfoVO productInfo(Parameter parameter) {
        Craw_goods_InfoVO infos = new Craw_goods_InfoVO();
        infos.setEgoodsId(parameter.getEgoodsId());
        infos.setGoodsId(parameter.getGoodsId());
        infos.setGoods_status(1);
        infos.setFeature1(Fields.COUNT_01);
        infos.setBatch_time(parameter.getBatch_time());
        infos.setGoods_url(parameter.getItemUtl_2());
        infos.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
        infos.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
        return infos;
    }

    /**
     * 封装商品价格表
     *
     * @param parameter 参数
     * @return Craw_goods_Price_Info
     * @author jack.zhao
     * @data 2020/12/2 13:57
     */
    public Craw_goods_Price_Info productInfoPrice(Parameter parameter) {
        Craw_goods_Price_Info info = new Craw_goods_Price_Info();
        info.setGoodsid(parameter.getGoodsId());
        info.setBatch_time(parameter.getBatch_time());
        info.setCust_keyword_id(parameter.getKeywordId());
        info.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
        return info;
    }

    /**
     * 封装商品小图片
     *
     * @param parameter 参数
     * @param imageurl  图片链接
     * @param count     当前是第几张图片
     * @return Craw_goods_pic_Info
     * @author jack.zhao
     * @data 2020/12/2 13:57
     */
    public Craw_goods_pic_Info getImageProduct(Parameter parameter, String imageurl, int count) {
        Craw_goods_pic_Info info = new Craw_goods_pic_Info();
        info.setBatch_time(parameter.getBatch_time());
        info.setCust_keyword_id(Integer.valueOf(parameter.getCrawKeywordsInfo().getCust_keyword_id()));
        info.setGoodsid(parameter.getGoodsId());
        info.setPic_size(String.valueOf(count + 1));
        info.setPic_order(count + 1);
        info.setPic_url(imageurl);
        info.setPlatform_name(parameter.getListjobName().get(0).getPlatform());
        info.setEgoodsid(parameter.getEgoodsId());
        info.setPic_type("littlepic");
        return info;
    }


}
