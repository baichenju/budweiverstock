package com.eddc.service;

import com.eddc.model.Parameter;
import java.util.Map;

/**
 * @author jack.zhao
 */
public interface ProductAnalysisService {
    /**
     * 请求商品数据
     * @param parameter 参数
     * @author jack.zhao
     * @data 2020/11/30 17:13
     * @return map;
     */
     Map<String, Object> getProductLink(Parameter parameter) throws Exception;
}
