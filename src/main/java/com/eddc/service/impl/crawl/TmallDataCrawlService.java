package com.eddc.service.impl.crawl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;
import javax.annotation.PostConstruct;
import java.util.Map.Entry;

import com.eddc.model.*;
import com.eddc.util.*;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.eddc.service.AbstractProductAnalysis;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

@Service("tmallDataCrawlService")
@Slf4j
public class TmallDataCrawlService extends AbstractProductAnalysis {
    private static Logger logger = LoggerFactory.getLogger(TmallDataCrawlService.class);

    /***************************************************************************************************************************************************/

    /**
     * 反射机制脱离了spring容器的管理，导致@Resource等注解失效。
     * jack.zhao
     */
    public static TmallDataCrawlService tmallDataCrawlService;

    @PostConstruct
    public void init() {
        tmallDataCrawlService = this;
    }


    @Override
    public Map<String, Object> getProductLink(Parameter parameter) {
        Map<String, Object> mapTmall = new HashMap<>(10);
        String url = "http://api.vephp.com/tbprodetail?vekey=V00005620Y22722244&id=" + parameter.getEgoodsId() + "&areaId=310100";
        String ref = url = "https://detail.tmall.com/item.htm?id=" + parameter.getEgoodsId() + "&areaId=" + parameter.getDeliveryPlace().getDelivery_place_code();
        if (StringUtils.isNotBlank(parameter.getDeliveryPlace().getDelivery_place_code())) {
            url = "https://detail.m.tmall.com/item.htm?id=" + parameter.getEgoodsId() + "&areaId=" + parameter.getDeliveryPlace().getDelivery_place_code();
           // url = "https://detail.tmall.com/item.htm?id=" + parameter.getEgoodsId() + "&areaId=" + parameter.getDeliveryPlace().getDelivery_place_code();
            parameter.setCookie(parameter.getDeliveryPlace().getDelivery_place_code2());
        } else {
            url = "http://api.vephp.com/tbprodetail?vekey=V00005620Y22722244&id=" + parameter.getEgoodsId() + "&areaId=310100";
        }
//        String ref="https://detail.tmall.com/item.htm?id="+parameter.getEgoodsId();
//        //String url="http://api.ds.dingdanxia.com/shop/good_info2&apikey=NeR0Lp1Wlg7tXgKPugbBdfAUTJEVcicc&id="+parameter.getEgoodsId();
//        String url="http://api.vephp.com/tbprodetail?vekey=V00005620Y02376026&id="+parameter.getEgoodsId();
        parameter.setItemUrl_1(url);
        parameter.setItemUtl_2(ref);

        mapTmall = getProductRequest(parameter);
        return mapTmall;
    }

    @Override
    public Map<String, Object> getProductItemAnalysisData(String stringMessage, Parameter parameter) throws Exception {
        Map<String, Object> mapItem = new HashMap<String, Object>(10);
        /*如果地区不能为空就解析html页面，地区为空就调用付费接口*/
        if (StringUtils.isBlank(parameter.getDeliveryPlace().getDelivery_place_code())) {
            mapItem = getProductWrawler(stringMessage, parameter);
        } else {
            mapItem = getProductItemHtmlData(stringMessage, parameter);
        }
        return mapItem;
    }


    /***数据解析*/
    @SuppressWarnings({"static-access"})
    public Map<String, Object> getProductWrawler(String StringMessage, Parameter parameter) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>(10);
        //价格
        List<Map<String, Object>> insertPriceList = Lists.newArrayList();
        //详情
        List<Map<String, Object>> insertItemList = Lists.newArrayList();
        //图片
        List<Map<String, Object>> insertImage = Lists.newArrayList();
        List<Map<String, Object>> commentList = Lists.newArrayList();
        //商品规格属性
        List<Map<String, Object>> attributeList = Lists.newArrayList();


        List<Craw_goods_Price_Info> priceList = new ArrayList<Craw_goods_Price_Info>(10);
        Craw_goods_comment_Info comment = new Craw_goods_comment_Info();
        Craw_goods_InfoVO inform = productInfo(parameter);
        StringBuffer goodsComment = new StringBuffer();
        String shopName = null, platform_shoptype = null, subCatId = null, sellerId = null, picturl = null, goodsName = null, delivery_place = null;
        String postageFree = Fields.PACK_MAIL;
        /*库存*/
        String inventory = "";
        /*定金*/
        String deposit = "";
        /*卷*/
        String coupons = "";
        /*预定件数*/
        String reserve_num = "";
        /*赠品*/
        String present = "";
        /*卖家*/
        String soldCount = "";
        /*付款人数*/
        String sellCount = "sellCount";
        String skuId = null;
        String shopId = null;
        String location = "";
        String quantity = "";
        String handsel = "";
        String sellerName = "";
        String priceDesc = "";
        String commentCount = "";
        String originalPrice = null;
        String currentPrice = null;
        String totalSoldQuantity = "0";
        JSONArray props = new JSONArray();
        StringBuffer prom = new StringBuffer();
        JSONObject jsonItem = new JSONObject();
        String database = parameter.getListjobName().get(0).getDatabases();
        if (StringMessage.contains("apiStack")) {
            JSONObject json = JSONObject.parseObject(StringMessage);
            //jsonItem = json.getJSONObject("data").getJSONObject("data");
            //			if (StringUtils.isBlank(parameter.getPlace_code())) {
            				jsonItem=json.getJSONObject("data");
            //			}else{
            //				jsonItem=json.getJSONObject("data").getJSONObject("data");
            //			}

            JSONArray Carry = jsonItem.getJSONArray("apiStack");
            JSONObject images = jsonItem.getJSONObject("item");


            subCatId = jsonItem.getJSONObject("item").get("categoryId").toString();
            //店铺
            shopName = jsonItem.getJSONObject("seller").get("shopName").toString();
            //用户ID
            sellerId = jsonItem.getJSONObject("seller").get("userId").toString();
            //店铺Id
            shopId = jsonItem.getJSONObject("seller").get("shopId").toString();
            skuId = jsonItem.getString("itemId");


            if (shopName.contains(Fields.TMART)) {
                if (!shopName.contains(Fields.TMART_MAIN_VENUE)) {
                    platform_shoptype = Fields.SUPERMARKET;
                    shopId = Fields.SHIPID;
                    shopName = Fields.TMART;
                } else {
                    if (shopName.indexOf(Fields.FLAGSHIP_STORE) > 0) {
                        platform_shoptype = Fields.DAY_CAT_FLAGSHIP_STORE;
                    } else {
                        platform_shoptype = Fields.PROPRIETARY;
                    }
                }
            } else {
                if (shopName.indexOf(Fields.FLAGSHIP_STORE) > 0) {
                    platform_shoptype = Fields.DAY_CAT_FLAGSHIP_STORE;
                } else {
                    platform_shoptype = Fields.PROPRIETARY;
                }
            }

            try {
                /**获取商品小图片*/
                if (StringUtil.isNotEmpty(images.get("images").toString())) {
                    JSONArray jsonArray = JSONArray.parseArray(images.get("images").toString());
                    for (int i = 0; i < jsonArray.size(); i++) {
                        try {
                            if (i == 0) {
                                picturl = "http:" + jsonArray.getString(i);
                            }
                            //判断是否抓取商品小图片
                            if (parameter.getListjobName().get(0).getDescribe().contains("抓取商品小图片")) {
                                Craw_goods_pic_Info info = getImageProduct(parameter, "http:" + jsonArray.getString(i), i);
                                /*封装数据 商品图片情插入**/
                                insertImage.addAll(list(info));

                            }
                        } catch (Exception e) {
                            logger.error("==>>解析商品小图片失败 ，error:{}<<==", e);
                        }

                    }
                }
            } catch (Exception e) {
                logger.error("==>>天猫获取商品小图片失败 error:{}<<==", e);
            }
            if (StringUtil.isEmpty(picturl)) {
                Set<Entry<String, Object>> entrySet = images.entrySet();
                for (Entry<String, Object> entry : entrySet) {
                    if (entry.getKey().equals("images")) {
                        JSONArray jr2 = (JSONArray) entry.getValue();
                        picturl = jr2.getString(0);
                        break;
                    }
                }
            }
            /**商品开团时间*/
            if (Carry.toString().contains("channel")) {
                try {
                    JSONObject coupon = JSONObject.parseObject(Carry.toArray()[0].toString());
                    JSONObject jsonValue = JSONObject.parseObject(coupon.getString("value"));
                    if (jsonValue.toString().contains("channel")) {
                        if (jsonValue.getJSONObject("consumerProtection").containsKey("channel")) {
                            if (jsonValue.getJSONObject("consumerProtection").getJSONObject("channel").toString().contains("title")) {
                                String channelTitle = jsonValue.getJSONObject("consumerProtection").getJSONObject("channel").getString("title");
                                prom.append(channelTitle + "&&");
                            }
                        }

                    }
                } catch (Exception e) {
                    logger.error("==>>获取商品开团时间，发生异常 error:{}<<==", e);
                }

            }
            /**判断预售**/
            if (Carry.toArray()[0].toString().contains("hintBanner")) {
                JSONObject coupon = JSONObject.parseObject(Carry.toArray()[0].toString());
                JSONObject jsonValue = JSONObject.parseObject(coupon.getString("value"));
                prom.append(jsonValue.getJSONObject("trade").getJSONObject("hintBanner").getString("text") + "&&");
            }

            /**判断优惠卷是否存在*/
            if (Carry.toString().contains("tmallCoupon")) {
                JSONObject coupon = JSONObject.parseObject(Carry.toArray()[0].toString());
                JSONObject jsonValue = JSONObject.parseObject(coupon.getString("value"));
                if (jsonValue.toString().contains("coupon")) {
                    String couponData = jsonValue.getJSONObject("resource").getJSONObject("entrances").getJSONObject("coupon").getString("text");
                    if (Validation.isNotEmpty(couponData)) {
                        prom.append(couponData + "&&");
                    }

                }
            }
            /**判断优惠卷是否存在*/
            if (Carry.toString().contains("couponList")) {
                JSONObject coupon = JSONObject.parseObject(Carry.toArray()[0].toString());
                JSONObject jsonValue = JSONObject.parseObject(coupon.getString("value"));
                if (jsonValue.toString().contains("coupon")) {
                    JSONArray couponData = jsonValue.getJSONObject("resource").getJSONObject("coupon").getJSONArray("couponList");
                    if (couponData.size() > 0) {
                        for (int k = 0; k < couponData.size(); k++) {
                            JSONObject object = JSONObject.parseObject(couponData.toArray()[k].toString());
                            prom.append(object.getString("title") + "&&");
                        }

                    }
                }
                /***商品开卖时间**/

                if (jsonValue.toString().contains("bigPromotion")) {

                    if (jsonValue.getJSONObject("resource").getJSONObject("bigPromotion").toString().contains("memo")) {
                        try {
                            JSONArray array = jsonValue.getJSONObject("resource").getJSONObject("bigPromotion").getJSONArray("memo");
                            if (array != null && array.size() > 0) {
                                JSONObject object = JSONObject.parseObject(array.toArray()[0].toString());
                                prom.append(object.getString("text"));
                            }

                        } catch (Exception e) {
                            logger.error("==>>获取商品开卖时间，发生异常 error:{}<<==", e);
                        }
                    }
                }
            }
            if (parameter.getListjobName().get(0).getPlatform().contentEquals("taobao")) {
                /**快递发货地址**/
                if (Carry.toString().contains("from") || Carry.toString().contains("completedTo")) {
                    JSONObject coupon = JSONObject.parseObject(Carry.toArray()[0].toString());
                    JSONObject jsonValue = JSONObject.parseObject(coupon.getString("value"));
                    if (jsonValue.getJSONObject("delivery").toString().contains("completedTo")) {
                        delivery_place = jsonValue.getJSONObject("delivery").get("completedTo").toString();

                    } else {
                        try {
                            if (jsonValue.getJSONObject("delivery").toString().contains("from")) {
                                delivery_place = jsonValue.getJSONObject("delivery").get("from").toString();
                                /**卖家地址*/
                                location = jsonValue.getJSONObject("delivery").getString("to");
                            }
                        } catch (Exception e) {
                            log.error("==>>天猫解析地址报错" + e.getMessage() + "<<==");
                        }
                    }

                    if (jsonValue.containsKey("resource")) {
                        if (jsonValue.getJSONObject("resource").containsKey("shopProm")) {
                            JSONArray shopProm = jsonValue.getJSONObject("resource").getJSONArray("shopProm");
                            if (shopProm != null && shopProm.size() > 0) {
                                if (shopProm.toString().contains("[\"title\"]")) {
                                    prom.append(JSONObject.parseObject(shopProm.toArray()[0].toString()).getString("title"));
                                } else if (shopProm.toString().contains("iconText")) {
                                    String iconText = JSONObject.parseObject(shopProm.toArray()[0].toString()).getString("iconText");
                                    String content = JSONObject.parseObject(shopProm.toArray()[0].toString()).getString("content");
                                    prom.append(iconText + ":" + StringUtils.replaceEach(content, new String[]{"[", "]", "\""}, new String[]{"", "", ""}));
                                }

                            }
                        }

                    }

                }
            }
            /**判断商品是否存在 propPath sellCoun**/
            if (Carry.toString().contains(sellCount)) {

                for (int i = 0; i < Carry.size(); i++) {
                    JSONObject js = JSONObject.parseObject(Carry.toArray()[i].toString());
                    JSONObject objectJS = JSONObject.parseObject(js.getString("value"));
                    try {
                        /**定金*/
                        //deposit = objectJS.getJSONObject("price").getJSONObject("price").getString("priceText");
                        if (objectJS.getJSONObject("price").toString().contains("depositPrice")) {
                            /**定金卷*/
                            coupons = objectJS.getJSONObject("price").getJSONObject("depositPrice").get("priceDesc").toString();
                            if (coupons.contains(Fields.REDUCTION_OF)) {
                                coupons = StringHelper.getResultByReg(coupons, "([0-9]+)");
                            } else {
                                coupons = "";
                            }
                        }

                        /***文字定金*/
                        handsel = objectJS.getJSONObject("price").getJSONObject("price").getString("priceTitle");

                        if (coupons.contains("，")) {
                            deposit = coupons.split("，")[0].toString();
                            /***定金*/
                            deposit = StringHelper.getResultByReg(deposit, "([0-9]+)");
                            if (coupons.length() > 1) {
                                coupons = coupons.split("，")[1].toString();
                                /**优惠卷*/
                                coupons = StringHelper.getResultByReg(coupons, "([0-9]+)");
                            }
                        } else {
                            if (StringUtils.isNotEmpty(StringHelper.getResultByReg(priceDesc, "([0-9]+)"))) {
                                /**抵用金额*/
                                coupons = StringHelper.getResultByReg(priceDesc, "([0-9]+)");

                            }
                        }
                        //定金字段
                        if(handsel.contains("定金")){
                            deposit = objectJS.getJSONObject("price").getJSONObject("price").getString("priceText");
                        }
                        if (StringUtils.isBlank(deposit)){
                            deposit = coupons;
                        }
                        if (StringUtils.isNotBlank(coupons)) {
                            prom.append(Fields.PRESELL_MESSAGE + "&&");
                        }

                    } catch (Exception e) {
                        deposit = "";
                        coupons = "";
                    }
                    try {
                        /***月销**/
                        totalSoldQuantity = objectJS.getJSONObject("item").get("sellCount").toString();
                    } catch (Exception e) {
                        totalSoldQuantity = "0";
                    }
                    try {
                        location = objectJS.getJSONObject("skuCore").getJSONObject("skuItem").get("location").toString();
                    } catch (Exception e) {
                    }
                    try {
                        reserve_num = objectJS.getJSONObject("vertical").getJSONObject("presale").get("orderedItemAmount").toString();
                    } catch (Exception e) {
                    }
                    try {
                        if (objectJS.getJSONObject("item").toString().contains("title")) {
                            /**标题**/
                            goodsName = objectJS.getJSONObject("item").get("title").toString();
                        } else {
                            goodsName = jsonItem.getJSONObject("item").get("title").toString();
                        }
                    } catch (Exception e) {
                    }
                    try {
                        if (objectJS.getJSONObject("item").toString().contains("commentCount")) {
                            /***评论数*/
                            commentCount = objectJS.getJSONObject("item").get("commentCount").toString();
                        } else {
                            /***评论数*/
                            commentCount = jsonItem.getJSONObject("item").get("commentCount").toString();
                        }
                    } catch (Exception e) {
                    }
                    if (objectJS.getJSONObject("delivery").toString().contains("completedTo")) {
                        /**快递发货地址*/
                        delivery_place = objectJS.getJSONObject("delivery").get("completedTo").toString();
                    } else {
                        try {
                            if (objectJS.getJSONObject("delivery").toString().contains("from")) {
                                delivery_place = objectJS.getJSONObject("delivery").get("from").toString();
                            } else {
                                delivery_place = objectJS.getJSONObject("delivery").getString("to");
                            }
                        } catch (Exception e) {
                            logger.error("==>>天猫解析地址报错" + e.getMessage() + "<<==");
                        }
                    }
                    /***获取商品促销*/
                    if (objectJS.getJSONObject("price").toString().contains("\"shopProm\":")) {
                        try {
                            JSONArray price = objectJS.getJSONObject("price").getJSONArray("shopProm");
                            /*促销*/
                            for (int k = 0; k < price.size(); k++) {
                                String period = "";
                                JSONObject jss = JSONObject.parseObject(price.toArray()[k].toString());
                                if (jss.toString().contains("content")) {
                                    if (jss.toString().contains("period")) {
                                        period = Fields.ARRIVE + jss.getString("period") + Fields.FINISH;
                                    }
                                    if (jss.getString("iconText").equalsIgnoreCase(Fields.INTEGRAL)) {
                                        prom.append(jss.getString("iconText") + ":" + jss.getString("content").replace("\"", "").replace("[", "").replace("]", "").replace("\"", "") + "&&");
                                    } else {
                                        if (jss.containsKey("title")) {
                                            prom.append(jss.getString("iconText") + ":" + jss.getString("title") + period + ">>" + jss.getString("content").replace("\"", "").replace("[", "").replace("]", "").replace("\"", "") + "&&");
                                        } else {
                                            prom.append(jss.getString("iconText") + ":" + jss.getString("content").replace("\"", "").replace("[", "").replace("]", "").replace("\"", "") + period + "&&");
                                        }
                                    }

                                }
                                if (jss.toString().contains("giftOfContent")) {
                                    JSONArray content = jss.getJSONArray("giftOfContent");
                                    for (int ks = 0; ks < content.size(); ks++) {
                                        JSONObject items = JSONObject.parseObject(content.toArray()[ks].toString());
                                        /**促销商品Id*/
                                        JSONArray itemId = items.getJSONArray("items");
                                        for (int it = 0; it < itemId.size(); it++) {
                                            JSONObject jsons = JSONObject.parseObject(itemId.toArray()[it].toString());
                                            /**赠品获取**/
                                            if (StringUtil.isEmpty(present)) {
                                                present = jsons.getString("itemId");
                                            } else {
                                                present += "," + jsons.getString("itemId");
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            logger.error("==>>商品促销不存在 error:{}<<==", e);
                        }
                        /**判断商品是否参加聚划算活动*/
                        if (json.toString().contains(Fields.GOODS_JHS_DESC)) {
                            prom.append(Fields.GOODS_JHS + "&&");
                        }
                    }
                    /***获取参加聚划算的商品*/
                    if (prom.toString().contains(Fields.GOODS_JHS) || prom.toString().contains(Fields.GOODS_JHS_DESC)) {
                        soldCount = juhuasuanSold(parameter);
                    }

                    try {
                        if (js.toString().contains("hongbao")) {

                            if (objectJS.getJSONObject("resource").toString().contains("hongbao")) {
                                prom.append(objectJS.getJSONObject("resource").getJSONObject("bonus").getJSONObject("hongbao").getString("title") + "&&");
                                if (js.toString().contains("shopProm")) {
                                    prom.append(objectJS.getJSONObject("resource").getJSONObject("bonus").getJSONObject("shopProm").getString("title") + "&&");
                                }
                            } else if (objectJS.getJSONObject("resource").toString().contains("shopcoupon")) {
                                prom.append(objectJS.getJSONObject("resource").getJSONObject("shopcoupon").getString("title") + "&&");
                            }
                        }
                    } catch (Exception e1) {
                        logger.error("==>>解析促销失败 <<==" + e1.getMessage());
                        e1.printStackTrace();
                    }
                    /*进口税*/
                    if (Carry.size() > 0 && Carry != null) {
                        try {
                            JSONObject vertical = JSONObject.parseObject(Carry.toArray()[0].toString());
                            if (vertical.toString().contains("tariff")) {
                                String verticalName = vertical.getJSONObject("value").getJSONObject("vertical").getJSONObject("inter").getJSONObject("tariff").getString("name");
                                String verticalValue = vertical.getJSONObject("value").getJSONObject("vertical").getJSONObject("inter").getJSONObject("tariff").getString("value");
                                prom.append(verticalName + verticalValue + "&&");
                            }
                        } catch (Exception e) {
                            logger.error("==>>天猫解析税费报错<<==", e);
                        }

                    }
                    try {
                        if (objectJS.getJSONObject("price").toString().contains("extraPrices")) {
                            if (StringUtil.isNotEmpty(objectJS.getJSONObject("price").getJSONArray("extraPrices").toString())) {
                                JSONArray extraPrices = objectJS.getJSONObject("price").getJSONArray("extraPrices");
                                /**原价*/
                                originalPrice = JSONObject.parseObject(extraPrices.toArray()[0].toString()).get("priceText").toString();
                            }
                        } else {
                            /**原价*/
                            originalPrice = jsonItem.getJSONObject("mockData").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject("0").getJSONObject("price").get("priceText").toString();
                        }
                    } catch (Exception e) {
                        originalPrice = objectJS.getJSONObject("price").getJSONObject("price").get("priceText").toString();
                    }
                    /**现价**/
                    try {
                        if (handsel != null && handsel.equals(Fields.DEPOSIT)) {
                            currentPrice = objectJS.getJSONObject("price").getJSONObject("subPrice").getString("priceText");
                        } else {
                            currentPrice = objectJS.getJSONObject("price").getJSONObject("price").getString("priceText");
                        }
                    } catch (Exception e) {
                    }
                    /**库存**/
                    JSONObject skuCore = JSONObject.parseObject(objectJS.getString("skuCore"));
                    if (skuCore != null) {
                        try {
                            Object jsonStr = JSONObject.parseObject(skuCore.getString("sku2info"));
                            if (jsonStr instanceof JSONObject) {
                                JSONObject jsonObject = (JSONObject) jsonStr;
                                quantity = jsonObject.getJSONObject("0").get("quantity").toString();
                            } else {
                                if (skuCore.toString().contains("sku2info")) {
                                    JSONArray jsonArray = skuCore.getJSONArray("sku2info");
                                    JSONObject jsonQuantity = JSONObject.parseObject(jsonArray.toArray()[0].toString());
                                    quantity = jsonQuantity.getString("quantity");
                                }

                            }

                        } catch (Exception e) {
                            log.info("==>>获取商品库存发生异常：" + e + "<<==");
                            e.printStackTrace();
                        }
                    }

                    /**判断商品是否存在skuId*/
                    if (objectJS.containsKey("skuBase")) {
                        if (objectJS.getJSONObject("skuBase").toString().contains("skus")) {
                            JSONArray skuBase = objectJS.getJSONObject("skuBase").getJSONArray("skus");

                            if (objectJS.getJSONObject("skuBase").toString().contains("props")) {
                                try {
                                    JSONArray skuProps = objectJS.getJSONObject("skuBase").getJSONArray("props");
                                    JSONArray skuName = JSONObject.parseObject(skuProps.toArray()[0].toString()).getJSONArray("values");
                                    for (int kk = 0; kk < skuBase.size(); kk++) {
                                        String sku_nameData = "";
                                        String skuParameter = "";
                                        JSONObject skus = JSONObject.parseObject(skuBase.toArray()[kk].toString());
                                        skuId = skus.get("skuId").toString();
                                        //String data04 = StringEscapeUtils.unescapeJava(jsonItem.getString("mockData"));
                                        /**原价*/
                                        JSONObject mockData = JSONObject.parseObject(jsonItem.getString("mockData"));
                                        originalPrice = mockData.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(skuId).getJSONObject("price").get("priceText").toString();
                                        if (!Validation.isEmpty(objectJS.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(skuId).getJSONObject("subPrice"))) {
                                            /**现价*/
                                            currentPrice = objectJS.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(skuId).getJSONObject("subPrice").get("priceText").toString();
                                            //currentPrice = objectJS.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(skuId).getJSONObject("price").get("priceText").toString();
                                        } else {
                                            currentPrice = objectJS.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(skuId).getJSONObject("price").get("priceText").toString();
                                        }
                                        /**库存*/
                                        inventory = objectJS.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(skuId).get("quantity").toString();
                                        if (currentPrice.contains("-")) {
                                            currentPrice = currentPrice.split("-")[1].toString();
                                        }
                                        if (originalPrice.contains("-")) {
                                            originalPrice = originalPrice.split("-")[1].toString();
                                        }
                                        if (skuProps.size() > 0) {
                                            String propPath = skus.get("propPath").toString();
                                            if (propPath.contains(";")) {
                                                String[] propPathPid = propPath.split(";");
                                                for (int k = 0; k < propPathPid.length; k++) {
                                                    String vid = propPathPid[k].toString().split(":")[1];
                                                    for (int kkk = 0; kkk < skuProps.size(); kkk++) {
                                                        JSONObject propsId = JSONObject.parseObject(skuProps.toArray()[kkk].toString());
                                                        JSONArray values = propsId.getJSONArray("values");
                                                        for (int s = 0; s < values.size(); s++) {
                                                            JSONObject skuNames = JSONObject.parseObject(values.toArray()[s].toString());
                                                            if (skuNames.getString("vid").equals(vid)) {
                                                                skuParameter += propsId.getString("name") + ":" + skuNames.getString("name") + "&&";
                                                                if (k == 1) {
                                                                    sku_nameData = skuParameter.substring(0, skuParameter.length() - 2);
                                                                }
                                                                break;
                                                            }

                                                        }
                                                    }
                                                }
                                            } else {
                                                try {
                                                    JSONObject skuNames = JSONObject.parseObject(skuName.toArray()[kk].toString());
                                                    sku_nameData = skuNames.getString("name");

                                                } catch (Exception e) {
                                                    logger.error("==>>skuName不存在解析出错了 error:{}<<==", e);
                                                }
                                            }
                                        }
                                        Craw_goods_Price_Info priceInfo = productInfoPrice(parameter);

                                        if (StringUtils.isEmpty(inventory)) {
                                            inventory = quantity;
                                        }
                                        /**库存*/
                                        priceInfo.setInventory(inventory);
                                        priceInfo.setSKUid(skuId);
                                        priceInfo.setSku_name(sku_nameData);
                                        priceInfo.setCurrent_price(currentPrice);
                                        priceInfo.setOriginal_price(originalPrice);
                                        priceInfo.setPromotion(prom.toString());
                                        priceInfo.setSale_qty(totalSoldQuantity);
                                        priceInfo.setChannel(parameter.getListjobName().get(0).getClient());
                                        priceInfo.setDeposit(deposit);
                                        priceInfo.setReserve_num(reserve_num);
                                        priceInfo.setTo_use_amount(coupons);
                                        priceList.add(priceInfo);
                                        if (parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")) {
                                            for (int k = 0; k < 2; k++) {
                                                if (k == 0) {
                                                    priceInfo.setChannel(Fields.CLIENT_MOBILE);
                                                } else {
                                                    priceInfo.setChannel(Fields.CLIENT_PC);
                                                }
                                                /*封装数据 商品数据详情 价格插入**/
                                                insertPriceList.addAll(list(priceInfo));
                                            }
                                        } else {
                                            priceInfo.setChannel(Fields.CLIENT_MOBILE);
                                            /*封装数据 商品数据详情 价格插入**/
                                            insertPriceList.addAll(list(priceInfo));
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    logger.error("skuName为空" + e);
                                }
                            }
                        }
                    } else {
                        if (Validation.isEmpty(currentPrice)) {
                            currentPrice = originalPrice;
                        }
                        if (currentPrice.contains("-")) {
                            currentPrice = currentPrice.split("-")[1].toString();
                        }
                        if (originalPrice.contains("-")) {
                            originalPrice = originalPrice.split("-")[1].toString();
                        }
                        Craw_goods_Price_Info priceInfo = productInfoPrice(parameter);
                        if (StringUtils.isEmpty(inventory)) {
                            inventory = quantity;
                        }
                        priceInfo.setInventory(inventory);
                        priceInfo.setBatch_time(parameter.getBatch_time());
                        priceInfo.setCurrent_price(currentPrice);
                        priceInfo.setOriginal_price(originalPrice);
                        priceInfo.setPromotion(prom.toString());
                        priceInfo.setSale_qty(totalSoldQuantity);
                        priceInfo.setChannel(parameter.getListjobName().get(0).getClient());
                        priceInfo.setDeposit(deposit);
                        priceInfo.setReserve_num(reserve_num);
                        priceInfo.setTo_use_amount(coupons);
                        priceList.add(priceInfo);
                        if (parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")) {
                            for (int k = 0; k < 2; k++) {
                                if (k == 0) {
                                    priceInfo.setChannel(Fields.CLIENT_MOBILE);
                                } else {
                                    priceInfo.setChannel(Fields.CLIENT_PC);
                                }
                                /*封装数据 商品数据详情 价格插入**/
                                insertPriceList.addAll(list(priceInfo));
                            }
                        } else {
                            priceInfo.setChannel(Fields.CLIENT_MOBILE);
                            /*封装数据 商品数据详情 价格插入**/
                            insertPriceList.addAll(list(priceInfo));
                        }
                    }
                }
            } else if (!jsonItem.toString().contains("\"skuBase\":[]")) {
                JSONObject jsonData = JSONObject.parseObject(jsonItem.toString());
                props = jsonData.getJSONObject("skuBase").getJSONArray("props");
                /***商品名称*/
                JSONArray sku_id = jsonData.getJSONObject("skuBase").getJSONArray("skus");
                /**商品sku*/
                JSONArray sku_name = JSONObject.parseObject(props.toArray()[0].toString()).getJSONArray("values");
                for (int hh = 0; hh < sku_name.size(); hh++) {
                    JSONObject skuNames = JSONObject.parseObject(sku_name.toArray()[hh].toString());
                    for (int sk = 0; sk < sku_id.size(); sk++) {
                        try {
                            JSONObject object = JSONObject.parseObject(sku_id.toArray()[sk].toString());
                            JSONObject jsonPrice = JSONObject.parseObject(Carry.toArray()[0].toString());
                            JSONObject jsonValue = JSONObject.parseObject(jsonPrice.getString("value"));

                            String price = jsonValue.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(object.getString("skuId")).getJSONObject("price").getString("priceText");
                            Craw_goods_Price_Info priceInfo = productInfoPrice(parameter);
                            priceInfo.setInventory(inventory);
                            priceInfo.setSKUid(object.getString("skuId"));
                            priceInfo.setSku_name(skuNames.getString("name"));
                            priceInfo.setCurrent_price(price);
                            priceInfo.setOriginal_price(price);
                            priceInfo.setPromotion(prom.toString());
                            priceInfo.setSale_qty(totalSoldQuantity);
                            priceInfo.setChannel(parameter.getListjobName().get(0).getClient());
                            priceInfo.setDeposit(deposit);
                            priceInfo.setReserve_num(reserve_num);
                            priceInfo.setTo_use_amount(coupons);
                            priceList.add(priceInfo);
                            if (parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")) {
                                for (int k = 0; k < 2; k++) {
                                    if (k == 0) {
                                        priceInfo.setChannel(Fields.CLIENT_MOBILE);
                                    } else {
                                        priceInfo.setChannel(Fields.CLIENT_PC);
                                    }
                                    /*封装数据 商品数据详情 价格插入**/
                                    insertPriceList.addAll(list(priceInfo));
                                }
                            } else {
                                priceInfo.setChannel(Fields.CLIENT_MOBILE);
                                /*封装数据 商品数据详情 价格插入**/
                                insertPriceList.addAll(list(priceInfo));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            log.error("skuName为空" + e);
                        }
                    }
                }
                /**商品名称*/
                goodsName = jsonItem.getJSONObject("item").getString("title");
                if (jsonItem.getJSONObject("rate").toString().contains("totalCount")) {
                    /**评论数*/
                    commentCount = jsonItem.getJSONObject("rate").getString("totalCount");
                }

            } else {
                /**商品名称*/
                JSONObject jsonTitle = JSONObject.parseObject(jsonItem.getString("item"));
                goodsName = jsonTitle.getString("title");
                if (jsonTitle.containsKey("rate")) {
                    /**评论数*/
                    JSONObject jsonRate = JSONObject.parseObject(jsonTitle.getString("rate"));
                    commentCount = jsonRate.getString("totalCount");
                }
                try {
                    JSONObject jsonMock = JSONObject.parseObject(jsonItem.getString("mockData"));
                    JSONObject coupon = JSONObject.parseObject(Carry.toArray()[0].toString());
                    currentPrice = coupon.getJSONObject("value").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject("0").getJSONObject("price").getString("priceText");
                    originalPrice = currentPrice;
                    if (currentPrice.contains("-")) {
                        currentPrice = jsonMock.getJSONObject("price").getJSONObject("price").getString("priceText");
                        originalPrice = currentPrice;
                    } else {
                        originalPrice = jsonMock.getJSONObject("price").getJSONObject("price").getString("priceText");
                    }
                } catch (Exception e) {
                    JSONObject mockData = JSONObject.parseObject(jsonItem.getString("mockData"));
                    currentPrice = mockData.getJSONObject("price").getJSONObject("price").getString("priceText");
                    originalPrice = currentPrice;

                }

                Craw_goods_Price_Info priceInfo = productInfoPrice(parameter);
                if (StringUtils.isEmpty(inventory)) {
                    inventory = quantity;
                }
                priceInfo.setInventory(inventory);
                priceInfo.setBatch_time(parameter.getBatch_time());
                priceInfo.setGoodsid(parameter.getGoodsId());

                priceInfo.setCurrent_price(currentPrice);
                priceInfo.setOriginal_price(originalPrice);
                priceInfo.setPromotion(prom.toString());
                priceInfo.setSale_qty(totalSoldQuantity);
                priceInfo.setDeposit(deposit);
                priceInfo.setReserve_num(reserve_num);
                priceInfo.setTo_use_amount(coupons);
                priceList.add(priceInfo);
                if (parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")) {
                    for (int k = 0; k < 2; k++) {
                        if (k == 0) {
                            priceInfo.setChannel(Fields.CLIENT_MOBILE);
                        } else {
                            priceInfo.setChannel(Fields.CLIENT_PC);
                        }
                        /*封装数据 商品数据详情 价格插入**/
                        insertPriceList.addAll(list(priceInfo));
                    }
                } else {
                    priceInfo.setChannel(Fields.CLIENT_MOBILE);
                    /*封装数据 商品数据详情 价格插入**/
                    insertPriceList.addAll(list(priceInfo));
                }

            }

            try {
                if (StringUtils.isNotEmpty(jsonItem.getJSONObject("seller").getString("sellerNick"))) {
                    sellerName = jsonItem.getJSONObject("seller").getString("sellerNick");
                }
            } catch (Exception e) {
                sellerName = shopName;
            }
            try {
                /**店铺评分**/
                if (jsonItem.getJSONObject("seller").toString().contains("evaluates")) {
                    JSONArray arryList = jsonItem.getJSONObject("seller").getJSONArray("evaluates");
                    StringBuffer storeComment = new StringBuffer();
                    for (int i = 0; i < arryList.size(); i++) {
                        JSONObject object = JSONObject.parseObject(arryList.toArray()[i].toString());
                        storeComment.append(object.getString("title") + "(" + object.getString("score").trim() + ")" + "&&");
                        comment.setComment_shopscores(storeComment.toString());
                    }
                }
            } catch (Exception e) {
                logger.info("解析店铺评分发生异常：" + e.getMessage());
                e.printStackTrace();
            }
            try {
                //商品评分
                JSONObject objectData = JSONObject.parseObject(json.getString("data"));
                if (objectData.containsKey("rate")) {
                    JSONObject jsonRate = JSONObject.parseObject(objectData.getString("rate"));
                    if (jsonRate.toString().contains("keywords")) {
                        JSONArray rate = jsonItem.getJSONObject("rate").getJSONArray("keywords");
                        for (int k = 0; k < rate.size(); k++) {
                            JSONObject object = JSONObject.parseObject(rate.toArray()[k].toString());
                            goodsComment.append(object.getString("word") + "(" + object.getString("count") + ")," + object.getString("attribute").trim() + "&&");

                        }
                    }
                }

            } catch (Exception e) {
                logger.info("解析商品评分发生异常：" + e.getMessage());
                e.printStackTrace();
            }

            jsonItem.getJSONObject("seller").getString("sellerNick");

            /***评论分析数据*/
            comment.setComment_tags(goodsComment.toString());
            comment.setBatch_time(parameter.getBatch_time());
            comment.setEgoodsId(parameter.getEgoodsId());
            comment.setGoodsId(parameter.getGoodsId());
            comment.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
            comment.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
            comment.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
            comment.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
            comment.setComment_status(Fields.STATUS_COUNT_1);
            comment.setTtl_comment_num(commentCount);

            /**商品详情*/
            inform.setEgoodsId(parameter.getEgoodsId());
            inform.setGoodsId(parameter.getGoodsId());
            //卖家店铺名称
            inform.setPlatform_shopname(shopName);
            //卖家位置
            inform.setSeller_location(location);
            //商品Id
            inform.setPlatform_shopid(shopId);
            //平台商店类型
            inform.setPlatform_shoptype(platform_shoptype);
            //行业ID
            inform.setPlatform_category(subCatId);
            //店铺Id
            inform.setPlatform_sellerid(sellerId);
            //卖家店铺名称
            inform.setPlatform_sellername(sellerName);
            inform.setGoods_pic_url(picturl);
            //商品名称
            inform.setPlatform_goods_name(goodsName);
            //交易地址
            inform.setDelivery_place(delivery_place);
            //商品库存
            inform.setInventory(quantity);
            //月销
            inform.setSale_qty(totalSoldQuantity);
            //是不包邮
            inform.setDelivery_info(postageFree);
            //商品总评论数
            inform.setTtl_comment_num(commentCount);
            //抵用金额
            inform.setJhs_paid_num(soldCount);
            //状态
            inform.setGoods_status(1);
            //商品链接
            inform.setGoods_url("https://detail.m.tmall.com/item.htm?id=" + parameter.getEgoodsId());


            /*封装数据 商品数据详情 插入**/
            insertItemList.addAll(list(inform));

            String hour = parameter.getMap().get("hour").toString();
            String week = parameter.getMap().get("week").toString();
            //获取商品的规格属性
            String dateWeek = parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[0];
            if (dateWeek.contains(week)) {
                String dataTime = parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[1];
                if (dataTime.contains(hour)) {
                    //获取商品属性
                    attributeList = commodityProperty(json, parameter);

                    /*封装数据 商品评论分析数据**/
                    commentList.addAll(list(comment));
                } else {
                    /**清空小图片*/
                    insertImage.clear();
                }
            }
        }
        //判断是否开启批量抓取数据 1，批量 4 实时抓取
        if (parameter.getListjobName().get(0).getStorage().equals("4")) {
            /**插入商品详情 价格*/
            if (StringUtil.isNotEmpty(goodsName)) {
                if (insertPriceList != null && insertPriceList.size() > 0) {

                    try {
                        getProductSave(insertPriceList, database, Fields.TABLE_CRAW_GOODS_PRICE_INFO);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (insertItemList != null && insertItemList.size() > 0) {
                    try {
                        getProductSave(insertItemList, database, Fields.TABLE_CRAW_GOODS_INFO);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


            /**商品小图片*/
            if (insertImage != null && insertImage.size() > 0) {
                try {
                    getProductSave(insertImage, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PIC_INFO);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            /**插入商品评论描述*/
            if (commentList != null && commentList.size() > 0) {
                try {
                    getProductSave(commentList, parameter.getListjobName().get(0).getDatabases(), Fields.CRAW_GOODS_COMMENT_INFO);

                } catch (Exception e) {
                    logger.error("==>>获取商品评分失败error:{}<<==", e);
                    e.printStackTrace();
                }
            }
        }
        /***判断是否开启批量抓取数据*/
        if (!parameter.getListjobName().get(0).getStorage().equals("4")) {
            //图片
            map.put("imageList", insertImage);
            //商品评论描述
            map.put("commentList", commentList);
            //商品价格
            map.put("itemPriceList", insertPriceList);
            //商品详情
            map.put("itemList", insertItemList);
            //商品规格参数
            map.put("attributesList", attributeList);
        }
        return map;
    }

    /**
     * 获取聚划算付款人数
     */
    public String juhuasuanSold(Parameter parameter) {
        String soldCount = "";
        parameter.setItemUrl_1(Fields.JUHUASUAN_IMAGE_URL.replace(Fields.EDOODSID, parameter.getEgoodsId()));
        String StringMessage = getRequest(parameter);
        if (StringMessage.contains("soldCount")) {
            try {
                JSONObject json = JSONObject.parseObject(StringMessage);
                soldCount = json.getJSONObject("data").getString("soldCount");
            } catch (Exception e) {
                logger.error("==>>获取聚划算付款人数请求接口发送异常 ：{}<<=", e);
            }

        }
        return soldCount;

    }

    /***
     *解析商品信息
     */
    @SuppressWarnings("unused")
    public Map<String, Object> getProductItemHtml(String StringMessage, Parameter parameter) throws Exception {

        Map<String, Object> map = new HashMap<String, Object>(10);
        //价格
        List<Map<String, Object>> insertPriceList = Lists.newArrayList();
        //详情
        List<Map<String, Object>> insertItemList = Lists.newArrayList();
        //图片
        List<Map<String, Object>> insertImage = Lists.newArrayList();
        List<Map<String, Object>> commentList = Lists.newArrayList();
        //商品规格属性
        List<Map<String, Object>> attributeList = Lists.newArrayList();

        Craw_goods_InfoVO infovo = productInfo(parameter);
        String database = parameter.getListjobName().get(0).getDatabases();
        Document doc = Jsoup.parseBodyFragment(StringMessage);
        Element ment = doc.getElementById("J_ImgBooth");
        ArrayList<Element> element = doc.select("script");
        if (ment != null) {
            String productName = doc.getElementById("J_ImgBooth").attr("alt");
            String productImage = "http:" + doc.getElementById("J_ImgBooth").attr("src");
            String productStore = doc.getElementsByClass("slogo-shopname").text();
            for (int i = 0; i < element.size(); i++) {
                if (element.get(i).data().toString().contains("TShop.Setup")) {
                    String skuMessage = element.get(i).data();
                    String shopJson = "TShop.Setup(";
                    String productSku = skuMessage.substring(skuMessage.indexOf(shopJson) + shopJson.length(), skuMessage.lastIndexOf(" );"));
                    JSONObject json = JSONObject.parseObject(productSku);

                    //请求商品价格接口
                    String priceUrl = "http:" + json.getString("changeLocationApi");
                    String city = json.getJSONObject("itemDO").getString("prov");
                    String userId = json.getJSONObject("itemDO").getString("userId");
                    String rootCatId = json.getJSONObject("itemDO").getString("rootCatId");
                    String categoryId = json.getJSONObject("itemDO").getString("categoryId");
                    String shopId = json.getString("rstShopId");

                    /*****请求商品价格****/
                    int code = Integer.parseInt(parameter.getDeliveryPlace().getDelivery_place_code()) / 200;
                    int code2 = code + 1;
                    //priceUrl=priceUrl+"&queryDelivery=true&queryProm=true&tmallBuySupport=true&ref=&areaId=" + parameter.getDeliveryPlace().getDelivery_place_code() + "&_ksTS=" + System.currentTimeMillis() + "_" + code + "&callback=jsonp" + code2;
                    //priceUrl = "https://mdskip.taobao.com/core/changeLocation.htm?queryDelivery=true&queryProm=true&tmallBuySupport=true&ref=&areaId=" + parameter.getDeliveryPlace().getDelivery_place_code() + "&_ksTS=" + System.currentTimeMillis() + "_" + code + "&callback=jsonp" + code2 + "&isUseInventoryCenter=true&cartEnable=true&sellerUserTag3=144185556820066432&service3C=false&sellerUserTag2=18014536485306368&isSecKill=false&isAreaSell=true&sellerUserTag4=1411&offlineShop=false&itemId=" + parameter.getEgoodsId() + "&sellerUserTag=125255532&showShopProm=false&tgTag=false&isPurchaseMallPage=false&isRegionLevel=true&household=false&notAllowOriginPrice=false&addressLevel=3";
                    parameter.setItemUrl_1(priceUrl);
                    String itemPrice = getOkHttpClient(parameter);

                    if (StringUtils.isNotBlank(itemPrice) && itemPrice.contains("defaultModel")) {
                        String itemJson=itemPrice.substring(itemPrice.indexOf("{"), itemPrice.lastIndexOf(")"));
                        JSONObject jsonPrice = JSONObject.parseObject(itemJson);
                        //买家地址
                        String deliveryAddress = jsonPrice.getJSONObject("defaultModel").getJSONObject("deliveryDO").getString("deliveryAddress");

                        //商品库存地址
                        String destination  = jsonPrice.getJSONObject("defaultModel").getJSONObject("deliveryDO").getString("destination");
                        //库存
                        String totalQuantity = jsonPrice.getJSONObject("defaultModel").getJSONObject("inventoryDO").getString("icTotalQuantity");
                        //运费
                        JSONArray freightList = jsonPrice.getJSONObject("defaultModel").getJSONObject("deliveryDO").getJSONObject("deliverySkuMap").getJSONArray("default");
                        if (freightList != null && freightList.size() > 0) {
                            JSONObject object = JSONObject.parseObject(freightList.toArray()[0].toString());
                            String freight = object.getString("money");
                            if (StringUtils.isNotBlank(freight)) {
                                if (freight.contains("0.00")) {
                                    infovo.setDelivery_info(Fields.DONTPACK_MAIL);
                                } else {
                                    infovo.setDelivery_info(Fields.PACK_MAIL);
                                }
                            } else {
                                infovo.setDelivery_info(Fields.DONTPACK_MAIL);
                            }
                        }

                        //平台商店类型
                        if (productStore.contains("超市")) {
                            infovo.setPlatform_shoptype("超市");
                        } else if (productStore.contains("旗舰")) {
                            infovo.setPlatform_shoptype("天猫旗舰店");
                        } else if (productStore.contains("天猫会员店")) {
                            infovo.setPlatform_shoptype("天猫会员店");
                        } else {
                            infovo.setPlatform_shoptype("非自营");
                        }

                        //卖家位置
                        if (StringUtils.isEmpty(destination)) {
                            if (StringUtils.isNotBlank(parameter.getDeliveryPlace().getDelivery_place_code())) {
                                destination = parameter.getDeliveryPlace().getDelivery_place_code();
                            }

                        }
                        infovo.setDelivery_place(destination);
                        /**商品详情*/
                        //卖家店铺名称
                        infovo.setPlatform_shopname(productStore);
                        //商品Id
                        infovo.setPlatform_shopid(shopId);
                        //行业ID
                        infovo.setPlatform_category(categoryId);
                        //店铺Id
                        infovo.setPlatform_sellerid(userId);
                        //卖家店铺名称
                        infovo.setPlatform_sellername(productStore);
                        //商品图片
                        infovo.setGoods_pic_url(productImage);
                        //商品名称
                        infovo.setPlatform_goods_name(productName);
                        //交易地址
                        infovo.setSeller_location(deliveryAddress);
                        //库存
                        infovo.setInventory(totalQuantity);
                        //月销
                        //infovo.setSale_qty(totalSoldQuantity);
                        ////商品总评论数
                        //infovo.setTtl_comment_num(commentCount);
                        //状态
                        infovo.setGoods_status(1);
                        infovo.setGoods_url(parameter.getItemUrl_1());


                        /**封装数据 商品数据详情插入**/
                        insertItemList.addAll(list(infovo));
                        /***封装商品价格*/
                        insertPriceList = getProductPrice(json, jsonPrice, parameter);
                        /**获取商品小图片*/
                        insertImage = getProductImage(StringMessage, parameter);
                        /**获取商品属性*/
                        attributeList = getProductAttribute(StringMessage, parameter);


                        String hour = parameter.getMap().get("hour").toString();
                        String week = parameter.getMap().get("week").toString();
                        //获取商品的规格属性
                        String dateWeek = parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[0];
                        if (dateWeek.contains(week)) {
                            String dataTime = parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[1];
                            if (!dataTime.contains(hour)) {
                                attributeList.clear();
                                //清空小图片
                                insertImage.clear();
                            }
                        }
                        //判断是否开启批量抓取数据 1，批量 4 实时抓取
                        if (parameter.getListjobName().get(0).getStorage().equals("4")) {
                            //插入商品详情 价格
                            if (StringUtil.isNotEmpty(infovo.getPlatform_goods_name())) {
                                if (insertPriceList != null && insertPriceList.size() > 0) {

                                    try {
                                        getProductSave(insertPriceList, database, Fields.TABLE_CRAW_GOODS_PRICE_INFO);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                if (insertItemList != null && insertItemList.size() > 0) {
                                    try {
                                        getProductSave(insertItemList, database, Fields.TABLE_CRAW_GOODS_INFO);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            //商品小图片
                            if (insertImage != null && insertImage.size() > 0) {
                                try {
                                    getProductSave(insertImage, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PIC_INFO);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            //插入商品评论描述
                            if (commentList != null && commentList.size() > 0) {
                                try {
                                    getProductSave(commentList, parameter.getListjobName().get(0).getDatabases(), Fields.CRAW_GOODS_COMMENT_INFO);
                                } catch (Exception e) {
                                    logger.error("==>>获取商品评分失败error:{}<<==", e);
                                    e.printStackTrace();
                                }
                            }
                        }
                        //判断是否开启批量抓取数据
                        if (!parameter.getListjobName().get(0).getStorage().equals("4")) {
                            //图片
                            map.put("imageList", insertImage);
                            //商品评论描述
                            map.put("commentList", commentList);
                            //商品价格
                            map.put("itemPriceList", insertPriceList);
                            //商品详情
                            map.put("itemList", insertItemList);
                            //商品规格参数
                            map.put("attributesList", attributeList);
                        }

                    }

                }
                break;
            }
        }
        return map;
    }

    /****商品小图片获取**/
    public List<Map<String, Object>> getProductImage(String item, Parameter parameter) throws Exception {
        List<Map<String, Object>> insertImage = Lists.newArrayList();
        Document doc = Jsoup.parseBodyFragment(item);
        Element ulthumb = doc.getElementById("J_UlThumb");
        if (ulthumb != null) {
            List<Element> listImage = ulthumb.select("li");
            for (int kk = 0; kk < listImage.size(); kk++) {
                String image = "http:" + listImage.get(kk).getElementsByTag("img").attr("src");
                //判断是否抓取商品小图片
                if (parameter.getListjobName().get(0).getDescribe().contains("抓取商品小图片")) {
                    Craw_goods_pic_Info info = getImageProduct(parameter, image, kk);
                    /*封装数据 商品图片情插入**/
                    insertImage.addAll(list(info));
                }
            }
        }
        return insertImage;
    }

    /***商品属性**/
    public List<Map<String, Object>> getProductAttribute(String item, Parameter parameter) {
        List<Map<String, Object>> attributeList = Lists.newArrayList();
        Document doc = Jsoup.parseBodyFragment(item);
        Element attr = doc.getElementById("J_AttrUL");
        if (attr != null) {
            List<Element> list = attr.select("li");

            String colon = ":";
            for (int y = 1; y < list.size(); y++) {
                String attestMessage = list.get(y).text().replace(" ", "");
                try {
                    char[] chars = attestMessage.toCharArray();
                    for (char aChar : chars) {
                        if (isChinesePunctuation(aChar)) {
                            colon = "：";
                            break;
                        } else {
                            colon = ":";
                        }
                    }
                    String[] attrSprite = attestMessage.split(colon);
                    attributeList.addAll(list(attributeInfo(attrSprite[0], attrSprite[1], parameter)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return attributeList;
    }


    /****商品价格封装***/
    public List<Map<String, Object>> getProductPrice(JSONObject json, JSONObject jsonPrice, Parameter parameter) throws Exception {
        List<Craw_goods_Price_Info> priceList = new ArrayList<Craw_goods_Price_Info>(10);
        List<Map<String, Object>> insertPriceList = Lists.newArrayList();
        //解析商品促销
        String promotionItem = getProductPromotion(jsonPrice);
        String destination  = jsonPrice.getJSONObject("defaultModel").getJSONObject("deliveryDO").getString("destination");
        //卖家位置
        if(StringUtils.isEmpty(destination)){
            if(StringUtils.isNotBlank(parameter.getDeliveryPlace().getDelivery_place_code())){
                destination=parameter.getDeliveryPlace().getDelivery_place_code();
            }

        }

        //判断商品是否存在sku
        if (json.containsKey("valItemInfo")) {
            JSONArray array = json.getJSONObject("valItemInfo").getJSONArray("skuList");
            if (array != null && array.size() > 0) {
                for (int kk = 0; kk < array.size(); kk++) {
                    Craw_goods_Price_Info priceInfo = productInfoPrice(parameter);
                    JSONObject object = JSONObject.parseObject(array.toArray()[kk].toString());
                    //商品skuName
                    String skuName = object.getString("names");
                    //商品skuId
                    String skuId = object.getString("skuId");
                    //商品组合key
                    String pvs = object.getString("pvs");
                    //商品原价
                    String originalPrice = json.getJSONObject("valItemInfo").getJSONObject("skuMap").getJSONObject(";" + pvs + ";").getString("price");
                    //商品现价
                    String currentPrice = jsonPrice.getJSONObject("defaultModel").getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").getJSONObject(skuId).getString("price");
                    //商品库存
                    priceInfo.setInventory(destination);
                    priceInfo.setSKUid(skuId);
                    priceInfo.setSku_name(skuName);
                    priceInfo.setPromotion(promotionItem);
                    priceInfo.setCurrent_price(currentPrice);
                    priceInfo.setOriginal_price(originalPrice);
                    //priceInfo.setTo_use_amount(infovo.getTtl_comment_num());
                    priceList.add(priceInfo);
                    if (parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")) {
                        for (int k = 0; k < 2; k++) {
                            if (k == 0) {
                                priceInfo.setChannel(Fields.CLIENT_MOBILE);
                            } else {
                                priceInfo.setChannel(Fields.CLIENT_PC);
                            }
                            /*封装数据 商品价格**/
                            insertPriceList.addAll(list(priceInfo));
                        }
                    } else {
                        priceInfo.setChannel(Fields.CLIENT_MOBILE);

                        /*封装数据 商品价格**/
                        insertPriceList.addAll(list(priceInfo));

                    }
                }
            }
        } else {
            //判断商品的sku
            JSONObject dataJson = JSONObject.parseObject(jsonPrice.getJSONObject("defaultModel").getJSONObject("inventoryDO").getString("skuQuantity"));
            if (dataJson == null || dataJson.isEmpty() || "null".equals(dataJson)) {
                Craw_goods_Price_Info priceInfo = productInfoPrice(parameter);
                //库存
                String totalQuantity = jsonPrice.getJSONObject("defaultModel").getJSONObject("inventoryDO").getString("icTotalQuantity");

                //商品现价
                JSONArray arrayPrice = jsonPrice.getJSONObject("defaultModel").getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").getJSONObject("def").getJSONArray("promotionList");
                if (arrayPrice != null && arrayPrice.size() > 0) {
                    JSONObject jsPrice = JSONObject.parseObject(arrayPrice.toArray()[0].toString());
                    String currentPrice = jsPrice.getString("price");
                    priceInfo.setCurrent_price(currentPrice);
                }
                //商品原价
                if (json.containsKey("detail")) {
                    String originalPrice = json.getJSONObject("detail").getString("defaultItemPrice");
                    priceInfo.setOriginal_price(originalPrice);
                } else {
                    priceInfo.setOriginal_price(priceInfo.getCurrent_price());
                }
                priceInfo.setInventory(totalQuantity);
                priceInfo.setPromotion(promotionItem);
                priceInfo.setDelivery_place(destination);
                priceList.add(priceInfo);
                if (parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")) {
                    for (int k = 0; k < 2; k++) {
                        if (k == 0) {
                            priceInfo.setChannel(Fields.CLIENT_MOBILE);
                        } else {
                            priceInfo.setChannel(Fields.CLIENT_PC);
                        }
                        /*封装数据 商品价格**/
                        insertPriceList.addAll(list(priceInfo));
                    }
                } else {
                    priceInfo.setChannel(Fields.CLIENT_MOBILE);

                    /*封装数据 商品价格**/
                    insertPriceList.addAll(list(priceInfo));

                }
            }
        }
        return insertPriceList;
    }


    /***商品促销**/
    public String getProductPromotion(JSONObject dataJson) {
        StringBuffer bufer = new StringBuffer();
        String item = "";
        try {
            if (!dataJson.toString().contains("\"tmallShopProm\":[]")) {
                JSONArray json = dataJson.getJSONObject("defaultModel").getJSONObject("itemPriceResultDO").getJSONArray("tmallShopProm");
                if (json != null && json.size() > 0) {
                    JSONObject object = JSONObject.parseObject(json.toArray()[0].toString());
                    //商品促销描述
                    String campaignName = object.getString("campaignName");
                    //商品促销时间
                    if (object.containsKey("endTime")) {
                        Long endTime = object.getLong("endTime");
                        campaignName = campaignName + ":" + SimpleDate.getMilliSecond(endTime);
                    }
                    if (object.containsKey("promPlanMsg")) {
                        if (!object.toString().contains("\"promPlanMsg\":[]")) {
                            JSONArray jsonArray = JSONArray.parseArray(object.getString("promPlanMsg"));
                            for (int ii = 0; ii < jsonArray.size(); ii++) {
                                bufer.append(jsonArray.getString(ii) + "&&");
                            }
                        }
                    }
                    if (StringUtils.isNotBlank(campaignName)) {
                        item = campaignName + bufer.toString();
                    } else {
                        item = bufer.toString();
                    }
                }

            }
        } catch (Exception e) {
            log.info("==>>解析商品促销发生异常：" + e + "<<==");
            e.printStackTrace();
        }
        return item;

    }


    public Map<String, Object> getProductItemHtmlData(String StringMessage, Parameter parameter) throws Exception {

        Map<String, Object> map = new HashMap<String, Object>(10);
        //价格
        List<Map<String, Object>> insertPriceList = Lists.newArrayList();
        //详情
        List<Map<String, Object>> insertItemList = Lists.newArrayList();
        //图片
        List<Map<String, Object>> insertImage = Lists.newArrayList();
        List<Map<String, Object>> commentList = Lists.newArrayList();
        //商品规格属性
        List<Map<String, Object>> attributeList = Lists.newArrayList();

        StringBuffer promotion = new StringBuffer();

        List<Craw_goods_Price_Info> priceList = new ArrayList<Craw_goods_Price_Info>(10);
        Craw_goods_InfoVO infovo = productInfo(parameter);
        String database = parameter.getListjobName().get(0).getDatabases();
        Document doc = Jsoup.parseBodyFragment(StringMessage);
        String productName = doc.getElementsByClass("main cell").html();
        String shop_name = doc.getElementsByClass("shop-name").html();
        int inventoryCount = 0;
        String quantity = "";
        String originalPrice = "";
        //判断商品是否存在
        if (StringUtils.isNotBlank(productName)) {
            //FileUtils.writeStringToFile(new File("D://tmall//"+parameter.getEgoodsId()+".html"),StringMessage,"utf-8");
            ArrayList<Element> element = doc.select("script");
            for (int i = 0; i < element.size(); i++) {

                if (element.get(i).data().toString().contains("var _DATA_Detail = ")) {
                    String[] elScriptList = element.get(i).data().toString().split("var _DATA_Detail = ");
                    String itemMessage = elScriptList[1].replace(";", "").trim();

                    //					//获取商品库存
                    //					String[] dataDetail = element.get(i + 1).data().toString().split("var _DATA_Mdskip = ");
                    //					String inventory = dataDetail[1].trim();

                    //通过库存接口解析库存

                    //通过时间戳和地区code拼接接口url
                    Calendar c = Calendar.getInstance();
                    long timeInMillis = c.getTimeInMillis();
                    int code = Integer.parseInt(parameter.getDeliveryPlace().getDelivery_place_code()) / 300;
                    int code2 = code + 1;
                    String inventory_url = "https://mdskip.taobao.com/core/changeLocation.htm?queryDelivery=true&queryProm=true&tmallBuySupport=true&ref=&" +
                            "areaId=" + parameter.getDeliveryPlace().getDelivery_place_code() +
                            "&_ksTS=" + timeInMillis + "_" + code +
                            "&callback=jsonp" + code2 + "&isUseInventoryCenter=true&cartEnable=true&sellerUserTag3=216806109434904736&service3C=false&sellerUserTag2=1170937140066910216&isSecKill=false&isAreaSell=true&sellerUserTag4=35596691079619&offlineShop=false&" +
                            "itemId=" + parameter.getEgoodsId() + "&sellerUserTag=39391266&showShopProm=false&tgTag=false&isPurchaseMallPage=false&isRegionLevel=true&household=false&notAllowOriginPrice=false&addressLevel=3";
                    if ("72".equals(parameter.getDeliveryPlace().getCust_account_id())) {
                        inventory_url = "https://mdskip.taobao.com/core/changeLocation.htm?queryDelivery=true&queryProm=true&tmallBuySupport=true&ref=&areaId=" + parameter.getDeliveryPlace().getDelivery_place_code() + "&_ksTS=" + timeInMillis + "_" + code + "&callback=jsonp" + code2 + "&isUseInventoryCenter=true&cartEnable=true&sellerUserTag3=144185556820066432&service3C=false&sellerUserTag2=18014536485306368&isSecKill=false&isAreaSell=true&sellerUserTag4=1411&offlineShop=false&itemId=" + parameter.getEgoodsId() + "&sellerUserTag=125255532&showShopProm=false&tgTag=false&isPurchaseMallPage=false&isRegionLevel=true&household=false&notAllowOriginPrice=false&addressLevel=3";
                    }
                    //parameter.setProxy(false);
                    String cookie = parameter.getDeliveryPlace().getDelivery_place_code2();
                    String ivPage = JsoupUtil.getHtmlByJsoup(inventory_url,cookie,"https://world.tmall.com");
                    //切换会产品实际url
                    String url = "https://detail.m.tmall.com/item.htm?id=" + parameter.getEgoodsId() + "&areaId=" + parameter.getDeliveryPlace().getDelivery_place_code();
                    parameter.setItemUrl_1(url);
                    //提取json字符串
                    String msg = ivPage.replaceAll(" ", "").replaceFirst("[(]", "%!@#").split("%!@#")[1];
                    String inventory = msg.substring(0, msg.length() - 1);
                    JSONObject inventoryMessage = JSONObject.parseObject(inventory).getJSONObject("defaultModel");
                    JSONObject json = JSONObject.parseObject(itemMessage);

                    //对异常json字符串进行try catch 并格式化异常json字符串
                    JSONObject js;
                    try {
                        js = JSONObject.parseObject(inventory).getJSONObject("defaultModel");
                    } catch (Exception e) {
                        logger.warn("==========================>json解析异常，开始格式化异常字符串并解析：<==========================");
                        js = JSONObject.parseObject(JSONUtil.formatJsonString(inventory)).getJSONObject("defaultModel");
                    }


                    String skuId ="" ;
                    String currPrice="";
                    if(json.containsKey("skuBase")&&json.getJSONObject("skuBase").containsKey("skus")
                            &&json.getJSONObject("skuBase").getJSONArray("skus")!=null){
                        skuId =((JSONObject) json.getJSONObject("skuBase").getJSONArray("skus").get(0)).getString("skuId");
                    }
                    String deliverTo = "";
                    if (js.getJSONObject("deliveryDO").containsKey("deliveryAddress")) {
                        deliverTo = js.getJSONObject("deliveryDO").getString("deliveryAddress");
                    }
                    if (StringUtils.isEmpty(deliverTo)) {
                        try {
                            deliverTo = ((JSONObject) js.getJSONObject("deliveryDO").getJSONObject("deliverySkuMap").getJSONArray("default").get(0)).getString("skuDeliveryAddress");
                        } catch (Exception e) {
                            logger.warn(Arrays.toString(e.getStackTrace()));
                        }
                    }
                    //String destination = js.getJSONObject("deliveryDO").getString("destination");
                     quantity = js.getJSONObject("inventoryDO").getString("icTotalQuantity");
                    inventoryCount+=Integer.parseInt(quantity);

                    //解析价格
                    try{
                        if (js.getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").containsKey("def")) {
                            if(js.getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").getJSONObject("def").containsKey("promotionList")){
                                JSONObject promotionList = (JSONObject) js.getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").getJSONObject("def").getJSONArray("promotionList").get(0);
                                currPrice = promotionList.getString("price");}
                            else{
                                currPrice =js.getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").getJSONObject("def").getString("price");
                            }


                            //现价
                            if(js.getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").containsKey("def")) {
                                originalPrice = js.getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").getJSONObject("def").getString("price");
                            }
                            //存在skuid的情况
                            if (js.getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").containsKey(skuId)){
                                JSONObject priceJs = js.getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").getJSONObject(skuId);
                                currPrice = ((JSONObject) priceJs.getJSONArray("promotionList").get(0)).getString("price");
                            }

                            //获取原价
                            if(StringUtils.isEmpty(originalPrice)){
                                if(json.containsKey("mock")){
                                    originalPrice = json.getJSONObject("mock").getJSONObject("price").getJSONObject("price").getString("priceText");
                                }else{
                                    originalPrice = currPrice;
                                }
                            }
                        }}catch (Exception e) {
                        logger.warn("======================>价格信息解析失败<======================"+e.getMessage());
                    }



                    //循环获取促销
                    try{
                        for (Object o : js.getJSONObject("itemPriceResultDO").getJSONArray("tmallShopProm")) {
                            JSONObject j = (JSONObject) o;
                            for (int i1 = 0; i1 < j.getJSONArray("promPlanMsg").size(); i1++) {
                                promotion.append(j.getJSONArray("promPlanMsg").get(i1)).append("&&");
                            }

                        }}catch (Exception e){
                        logger.warn("======================>促销信息解析失败<======================"+e.getMessage());
                    }

                    //获取邮费信息
                    String postage="";
                    try{
                        JSONObject postageJs = (JSONObject) js.getJSONObject("deliveryDO").getJSONObject("deliverySkuMap").getJSONArray("default").get(0);
                        postage = postageJs.getString("postage");
                        if(postage==null){
                            postage = postageJs.getString("money");
                        }
                    }catch (Exception e) {
                        logger.warn("======================>信息邮费解析失败<======================"+e.getMessage());
                    }

                    //插入库存地址
                    infovo.setSeller_location(deliverTo);
                    if (StringUtils.isBlank(infovo.getDelivery_place())||StringUtils.isBlank(infovo.getSeller_location())) {
                        if (StringUtils.isEmpty(infovo.getDelivery_place())) {
                            infovo.setDelivery_place(parameter.getDeliveryPlace().getDelivery_place_name());
                        }
                        if (infovo.getDelivery_place().contains("区")) {

                            infovo.setDelivery_place(parameter.getDeliveryPlace().getDelivery_place_name() + " " + infovo.getDelivery_place());
                        }
                        if (StringUtils.isEmpty(infovo.getSeller_location())) {
                            infovo.setSeller_location(parameter.getDeliveryPlace().getDelivery_place_name());
                        }
                    }
                    //运费
                    infovo.setDelivery_info(postage);

                    if (StringUtils.isEmpty(productName)) {
                        //商品名称
                        productName = json.getJSONObject("item").getString("title");
                    }
                    if (StringUtils.isEmpty(shop_name)) {
                        //店铺名称
                        shop_name = json.getJSONObject("seller").getString("shopName");
                    }
                    //店铺Id
                    if (json.getJSONObject("seller").containsKey("shopId")) {
                        infovo.setPlatform_shopid(json.getJSONObject("seller").getString("shopId"));
                    }

                    //用户Id
                    String userId = json.getJSONObject("seller").getString("userId");
                    //行业Id
                    String categoryId = json.getJSONObject("item").getString("categoryId");
                    //评论数
                    JSONObject rate = JSONObject.parseObject(json.getString("json"));
                    if (rate != null) {
                        if (rate.containsKey("totalCount")) {
                            String commentCount = rate.getString("totalCount");
                            infovo.setTtl_comment_num(commentCount);
                        }
                    }
                    infovo.setPlatform_shopname(shop_name);
                    infovo.setPlatform_sellername(shop_name);
                    infovo.setPlatform_goods_name(productName);

                    infovo.setPlatform_sellerid(userId);
                    infovo.setPlatform_category(categoryId);
                    infovo.setGoods_url(parameter.getItemUrl_1());
                    if (shop_name.contains("超市")) {
                        infovo.setPlatform_shoptype("超市");
                    } else if (shop_name.contains("旗舰")) {
                        infovo.setPlatform_shoptype("天猫旗舰店");
                    } else if (shop_name.contains("天猫会员店")) {
                        infovo.setPlatform_shoptype("天猫会员店");
                    } else {
                        infovo.setPlatform_shoptype("非自营");
                    }
                    //判断商品小图是否存在
                    if (StringUtil.isNotEmpty(json.getJSONObject("item").get("images").toString())) {
                        JSONArray jsonArray = JSONArray.parseArray(json.getJSONObject("item").getString("images"));
                        for (int ii = 0; ii < jsonArray.size(); ii++) {
                            try {
                                if (ii == 0) {
                                    String productImage = "http:" + jsonArray.getString(ii);
                                    infovo.setGoods_pic_url(productImage);
                                }
                                //判断是否抓取商品小图片
                                if (parameter.getListjobName().get(0).getDescribe().contains("抓取商品小图片")) {
                                    Craw_goods_pic_Info info = getImageProduct(parameter, "http:" + jsonArray.getString(ii), ii);
                                    /*封装数据 商品图片**/
                                    insertImage.addAll(list(info));
                                }
                            } catch (Exception e) {
                                logger.error("==>>解析商品小图片失败 ，error:{}<<==", e);
                            }

                        }
                    }
                    //获取商品价格
                    //					if (json.getJSONObject("skuBase").containsKey("props")) {
                    //						//促销
                    //						JSONObject priceJson = JSONObject.parseObject(inventoryMessage.getString("price"));
                    //						if(priceJson!=null) {
                    //							if (priceJson.containsKey("shopProm")) {
                    //								if (!priceJson.toString().contains("\"shopProm\":null")) {
                    //									JSONArray shopPromList = JSONArray.parseArray(priceJson.getJSONArray("shopProm").toString());
                    //									for (int w = 0; w < shopPromList.size(); w++) {
                    //										JSONObject PromMessage = JSONObject.parseObject(shopPromList.toArray()[w].toString());
                    //										String prom = PromMessage.getString("content").replace("[", "").replace("]", "").replace("\"", "");
                    //										if (shopPromList.size() == w + 1) {
                    //											promotion.append(prom);
                    //										} else {
                    //											promotion.append(prom + "&&");
                    //										}
                    //									}
                    //								}
                    //							}
                    //						}
                    //					}
                    //					//月销量
                    //					String sellcount=productSellCount(parameter);
                    //					infovo.setSale_qty(sellcount);//销量
                    //请求促销
                    //					try {
                    //						parameter.setItemUrl_1("https://h5api.m.tmall.hk/h5/mtop.tmall.detail.couponpage/1.0/?jsv=2.4.8&appKey=12574478&t="+System.currentTimeMillis()+"&sign=06b2458db3e283accd3a177c2cd1acbb&api=mtop.tmall.detail.couponpage&v=1.0&ttid=tmalldetail&type=jsonp&dataType=jsonp&callback=mtopjsonp5&data=%7B%22itemId%22%3A"+parameter.getEgoodsId()+"%2C%22source%22%3A%22tmallH5%22%7D");
                    //						String  Stringpromotion=httpClientService.interfaceSwitch(parameter);//请求数据
                    //						if(Stringpromotion.contains("coupons")) {
                    //							String strs=Stringpromotion.substring(Stringpromotion.indexOf("{"), Stringpromotion.lastIndexOf("}")+1);
                    //							JSONObject PromMessage = JSONObject.fromObject(strs);
                    //							JSONArray couponList = JSONArray.fromObject(PromMessage.getJSONObject("data").getJSONArray("coupons"));
                    //							for(int ii=0;ii<couponList.size();ii++) {
                    //								JSONObject promJson = JSONObject.fromObject(couponList.toArray()[ii].toString());
                    //								JSONArray couponListMessage = JSONArray.fromObject(promJson.getJSONArray("couponList"));
                    //								for(int ss=0;ss<couponList.size();ss++) {
                    //									JSONObject promObject = JSONObject.fromObject(couponListMessage.toArray()[ss].toString());
                    //									promotion.append(promObject.getString("subtitles").replace("[", "").replace("[", "").replace("\"", "")+"&&");
                    //								}
                    //
                    //							}
                    //						}
                    //					} catch (Exception e1) {
                    //
                    //						e1.printStackTrace();
                    //					}
                    //

                    //					try {
                    //						JSONObject resource=JSONObject.parseObject(inventoryMessage.getString("resource"));
                    //						if(resource!=null) {
                    //							if (resource.getJSONObject("coupon").containsKey("couponList")) {
                    //
                    //								JSONArray couponList = JSONArray.parseArray(resource.getJSONObject("coupon").getJSONArray("couponList").toString());
                    //								for (int cc = 0; cc < couponList.size(); cc++) {
                    //									JSONObject PromMessage = JSONObject.parseObject(couponList.toArray()[cc].toString());
                    //									String prom = PromMessage.getString("title");
                    //									promotion.append(prom + "&&");
                    //								}
                    //							}
                    //						}
                    //
                    //					} catch (Exception e1) {
                    //						logger.error("==>>获取商品促销失败error:{}<<==", e1);
                    //					}
                    //解析价格
                    if (json.containsKey("skuBase") && json.getString("skuBase").contains("price")) {
                        if (!json.getJSONObject("skuBase").toString().contains("\"props\":null")) {
                            JSONArray skuBaseList = JSONArray.parseArray(json.getJSONObject("skuBase").getJSONArray("props").toString());
                            if (skuBaseList.size() == 1) {
                                JSONObject valuesprops = JSONObject.parseObject(skuBaseList.toArray()[0].toString());
                                JSONArray valuespropsList = JSONArray.parseArray(valuesprops.getJSONArray("values").toString());
                                for (int kk = 0; kk < valuespropsList.size(); kk++) {
                                    JSONObject valuespropsNameList = JSONObject.parseObject(valuespropsList.toArray()[kk].toString());
                                    String productSkuName = valuespropsNameList.getString("name");

                                    JSONArray skuIdList = JSONArray.parseArray(json.getJSONObject("skuBase").getJSONArray("skus").toString());
                                    JSONObject productSkuId = JSONObject.parseObject(skuIdList.toArray()[kk].toString());
                                    String productSkuMessage = productSkuId.getString("skuId");

                                     originalPrice = json.getJSONObject("mock").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getJSONObject("price").getString("priceText");
                                    //									String productPrice = inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getJSONObject("price").getString("priceText");
                                    //									String inventorys = inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getString("quantity");
                                    //									if (StringUtils.isNotBlank(inventorys)) {
                                    //										inventoryCount = +Integer.valueOf(inventorys);
                                    //									}
                                    //									if (StringUtils.isBlank(originalPrice)) {
                                    //										originalPrice = productPrice;
                                    //									}
                                    Craw_goods_Price_Info priceInfo = productInfoPrice(parameter);
                                    priceInfo.setInventory(quantity);
                                    priceInfo.setSKUid(productSkuMessage);
                                    priceInfo.setSku_name(productSkuName);
                                    priceInfo.setPromotion(promotion.toString());
                                    priceInfo.setCurrent_price(currPrice);
                                    priceInfo.setOriginal_price(originalPrice);
                                    priceInfo.setTo_use_amount(infovo.getTtl_comment_num());
                                    //priceInfo.setSale_qty(sellcount);/
                                    priceInfo.setDelivery_place(infovo.getDelivery_place());
                                    priceList.add(priceInfo);
                                    if (parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")) {
                                        for (int k = 0; k < 2; k++) {
                                            if (k == 0) {
                                                priceInfo.setChannel(Fields.CLIENT_MOBILE);
                                            } else {
                                                priceInfo.setChannel(Fields.CLIENT_PC);
                                            }
                                            /*封装数据 商品价格**/
                                            insertPriceList.addAll(list(priceInfo));
                                        }
                                    } else {
                                        priceInfo.setChannel(Fields.CLIENT_MOBILE);

                                        /*封装数据 商品价格**/
                                        insertPriceList.addAll(list(priceInfo));

                                    }
                                }
                            } else {
                                JSONObject skuBase = JSONObject.parseObject(skuBaseList.toArray()[1].toString());
                                JSONArray valuesList = JSONArray.parseArray(skuBase.getJSONArray("values").toString());
                                for (int s = 0; s < valuesList.size(); s++) {
                                    //颜色分类
                                    JSONObject skuName = JSONObject.parseObject(valuesList.toArray()[s].toString());
                                    String name = skuName.getString("name");
                                    //尺码
                                    JSONObject skuType = JSONObject.parseObject(skuBaseList.toArray()[0].toString());
                                    String skuTypeName = skuType.getString("name");

                                    JSONArray valuespropsList = skuType.getJSONArray("values");
                                    for (int kk = 0; kk < valuespropsList.size(); kk++) {
                                        JSONObject valuespropsNameList = JSONObject.parseObject(valuespropsList.toArray()[kk].toString());
                                        String productSkuName = name + "," + skuTypeName + "," + valuespropsNameList.getString("name");
                                        /**商品价格*/
                                        JSONArray skuIdList = json.getJSONObject("skuBase").getJSONArray("skus");
                                        JSONObject productSkuId = JSONObject.parseObject(skuIdList.toArray()[kk].toString());
                                        String productSkuMessage = productSkuId.getString("skuId");


                                         originalPrice = json.getJSONObject("mock").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getJSONObject("price").getString("priceText");
                                        String productPrice = inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getJSONObject("price").getString("priceText");

                                        String inventorys = inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getString("quantity");
                                        if (StringUtils.isNotBlank(inventorys)) {
                                            inventoryCount = +Integer.valueOf(inventorys);
                                        }
                                        if (StringUtils.isBlank(originalPrice)) {
                                            originalPrice = productPrice;
                                        }
                                        Craw_goods_Price_Info priceInfo = productInfoPrice(parameter);
                                        priceInfo.setInventory(inventorys);
                                        priceInfo.setSKUid(productSkuMessage);
                                        priceInfo.setSku_name(productSkuName);

                                        priceInfo.setPromotion(promotion.toString());
                                        //priceInfo.setCurrent_price(productPrice);
                                        priceInfo.setOriginal_price(originalPrice);
                                        priceInfo.setPromotion(promotion.toString());
                                        priceInfo.setTo_use_amount(infovo.getTtl_comment_num());
                                        //priceInfo.setSale_qty(sellcount);
                                        priceInfo.setDelivery_place(infovo.getDelivery_place());
                                        priceList.add(priceInfo);
                                        if (parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")) {
                                            for (int k = 0; k < 2; k++) {
                                                if (k == 0) {
                                                    priceInfo.setChannel(Fields.CLIENT_MOBILE);
                                                } else {
                                                    priceInfo.setChannel(Fields.CLIENT_PC);
                                                }
                                                /*封装数据 商品价格**/
                                                insertPriceList.addAll(list(priceInfo));
                                            }
                                        } else {
                                            priceInfo.setChannel(Fields.CLIENT_MOBILE);
                                            /*封装数据 商品价格**/
                                            insertPriceList.addAll(list(priceInfo));

                                        }
                                    }

                                }

                            }

                        } else {
                            //获取商品价格
                            Craw_goods_Price_Info priceInfo = productInfoPrice(parameter);
                            if (inventoryMessage.getJSONObject("price").containsKey("extraPrices")) {
                                if (!inventoryMessage.getJSONObject("price").toString().contains("\"extraPrices\":null")) {
                                    //现价
                                    String currentPrice = inventoryMessage.getJSONObject("price").getJSONObject("price").getString("priceText");
                                    priceInfo.setCurrent_price(currentPrice);

                                    //原价
                                    JSONArray priceListData = inventoryMessage.getJSONObject("price").getJSONArray("extraPrices");
                                    for (int y = 0; y < priceListData.size(); y++) {
                                        JSONObject priceMessage = JSONObject.parseObject(priceListData.toArray()[y].toString());
                                         originalPrice = priceMessage.getString("priceText");
                                        priceInfo.setOriginal_price(originalPrice);
                                    }
                                }
                            } else {
                                 originalPrice = inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject("0").getJSONObject("price").getString("priceText");
                                String currentPrice = inventoryMessage.getJSONObject("price").getJSONObject("price").getString("priceText");
                                priceInfo.setOriginal_price(originalPrice);
                                priceInfo.setCurrent_price(currentPrice);

                            }


                            //库存
                            //String quantity = inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject("0").getString("quantity");
                            if (StringUtils.isNotBlank(quantity)) {
                                inventoryCount = +Integer.parseInt(quantity);
                            }
                            if (StringUtils.isBlank(priceInfo.getOriginal_price())) {
                                priceInfo.setOriginal_price(priceInfo.getCurrent_price());
                            }
                            priceInfo.setInventory(quantity);


                            priceInfo.setPromotion(promotion.toString());
                            priceInfo.setDelivery_place(infovo.getDelivery_place());
                            priceInfo.setPromotion(promotion.toString());


                            priceInfo.setTo_use_amount(infovo.getTtl_comment_num());
                            priceInfo.setChannel(parameter.getListjobName().get(0).getClient());
                            priceList.add(priceInfo);
                            if (parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")) {
                                for (int k = 0; k < 2; k++) {
                                    if (k == 0) {
                                        priceInfo.setChannel(Fields.CLIENT_MOBILE);
                                    } else {
                                        priceInfo.setChannel(Fields.CLIENT_PC);
                                    }

                                    /*封装数据 商品价格**/
                                    insertPriceList.addAll(list(priceInfo));
                                }
                            } else {
                                priceInfo.setChannel(Fields.CLIENT_MOBILE);
                                /*封装数据 商品价格**/
                                insertPriceList.addAll(list(priceInfo));
                            }

                        }
                    } else {
                        Craw_goods_Price_Info priceInfo = productInfoPrice(parameter);

                         originalPrice = json.getJSONObject("mock").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject("0").getJSONObject("price").getString("priceText");
                        priceInfo.setOriginal_price(originalPrice);

                        //   价 格


                        if (inventoryMessage.containsKey("price") && inventoryMessage.getJSONObject("price").containsKey("price")) {
                            priceInfo.setCurrent_price(inventoryMessage.getJSONObject("price").getJSONObject("price").getString("priceText"));
                        }
                        if (StringUtils.isBlank(currPrice)) {
                            priceInfo.setCurrent_price(originalPrice);
                        } else {
                            priceInfo.setCurrent_price(currPrice);
                        }

                        priceInfo.setPromotion(promotion.toString());
                        priceInfo.setDelivery_place(infovo.getDelivery_place());

                        //库存
                        priceInfo.setInventory(quantity);


                        priceInfo.setTo_use_amount(infovo.getTtl_comment_num());
                        priceInfo.setChannel(parameter.getListjobName().get(0).getClient());
                        priceList.add(priceInfo);
                        if (parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")) {
                            for (int k = 0; k < 2; k++) {
                                if (k == 0) {
                                    priceInfo.setChannel(Fields.CLIENT_MOBILE);
                                } else {
                                    priceInfo.setChannel(Fields.CLIENT_PC);
                                }

                                /*封装数据 商品价格**/
                                insertPriceList.addAll(list(priceInfo));
                            }
                        } else {
                            priceInfo.setChannel(Fields.CLIENT_MOBILE);
                            /*封装数据 商品价格**/
                            insertPriceList.addAll(list(priceInfo));
                        }
                    }

                    /**商品属性*/
                    if (json.containsKey("props")) {
                        if (!json.getJSONObject("props").toString().contains("\"propsList\":null")) {
                            if (json.getJSONObject("props").toString().contains("propsList")) {
                                JSONArray commodityPropertyList = JSONArray.parseArray(json.getJSONObject("props").getJSONArray("propsList").toString());

                                for (int g = 0; g < commodityPropertyList.size(); g++) {
                                    JSONObject commodityPropert = JSONObject.parseObject(commodityPropertyList.toArray()[g].toString());
                                    JSONArray groupPropsList = commodityPropert.getJSONArray("baseProps");
                                    for (int e = 0; e < groupPropsList.size(); e++) {
                                        JSONObject object = JSONObject.parseObject(groupPropsList.toArray()[e].toString());
                                        String key = object.getString("key");
                                        String value = object.getString("value");

                                        /**封装数据 商品属性**/
                                        attributeList.addAll(list(attributeInfo(key, value, parameter)));
                                    }
                                }
                            }
                        }
                        if (json.getJSONObject("props").containsKey("groupProps")) {
                            JSONArray commodityPropertyList = json.getJSONObject("props").getJSONArray("groupProps");
                            for (int g = 0; g < commodityPropertyList.size(); g++) {
                                JSONObject commodityPropert = JSONObject.parseObject(commodityPropertyList.toArray()[g].toString());
                                JSONArray groupPropsList = commodityPropert.getJSONArray("基本信息");
                                for (int e = 0; e < groupPropsList.size(); e++) {
                                    JSONObject object = JSONObject.parseObject(groupPropsList.toArray()[e].toString());
                                    Set<Entry<String, Object>> setdata = object.entrySet();
                                    //商品属性
                                    for (Entry<String, Object> entry : setdata) {

                                        String key = entry.getKey();
                                        String value = entry.getValue().toString();

                                        /*封装数据 商品属性**/
                                        attributeList.addAll(list(attributeInfo(key, value, parameter)));
                                    }
                                }
                            }
                        }
                    }
                }

            }
            infovo.setInventory(String.valueOf(inventoryCount));

            /**封装数据 商品数据详情插入**/
            insertItemList.addAll(list(infovo));


            String hour = parameter.getMap().get("hour").toString();
            String week = parameter.getMap().get("week").toString();
            //获取商品的规格属性
            String dateWeek = parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[0];
            if (dateWeek.contains(week)) {
                String dataTime = parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[1];
                if (!dataTime.contains(hour)) {
                    attributeList.clear();
                    //清空小图片
                    insertImage.clear();
                }
            }
            //判断是否开启批量抓取数据 1，批量 4 实时抓取
            if (parameter.getListjobName().get(0).getStorage().equals("4")) {
                //插入商品详情 价格
                if (StringUtil.isNotEmpty(infovo.getPlatform_goods_name())) {
                    if (insertPriceList != null && insertPriceList.size() > 0) {

                        try {
                            getProductSave(insertPriceList, database, Fields.TABLE_CRAW_GOODS_PRICE_INFO);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (insertItemList != null && insertItemList.size() > 0) {
                        try {
                            getProductSave(insertItemList, database, Fields.TABLE_CRAW_GOODS_INFO);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                //商品小图片
                if (insertImage != null && insertImage.size() > 0) {
                    try {
                        getProductSave(insertImage, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PIC_INFO);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //插入商品评论描述
                if (commentList != null && commentList.size() > 0) {
                    try {
                        getProductSave(commentList, parameter.getListjobName().get(0).getDatabases(), Fields.CRAW_GOODS_COMMENT_INFO);
                    } catch (Exception e) {
                        logger.error("==>>获取商品评分失败error:{}<<==", e);
                        e.printStackTrace();
                    }
                }
            }
            //判断是否开启批量抓取数据
            if (!parameter.getListjobName().get(0).getStorage().equals("4")) {
                //图片
                map.put("imageList", insertImage);
                //商品评论描述
                map.put("commentList", commentList);
                //商品价格
                map.put("itemPriceList", insertPriceList);
                //商品详情
                map.put("itemList", insertItemList);
                //商品规格参数
                map.put("attributesList", attributeList);
            }
        }

        return map;
    }

    /**
     * 获取商品属性值
     */
    public List<Map<String, Object>> commodityProperty(JSONObject json, Parameter parameter) {
        List<Map<String, Object>> insertList = Lists.newArrayList();
        try {
            JSONObject jsonItem = json.getJSONObject("data");

            String database = parameter.getListjobName().get(0).getDatabases();
            if (jsonItem.getJSONObject("props").toString().contains("groupProps")) {

                JSONArray groupPropsList = json.getJSONObject("data").getJSONObject("props").getJSONArray("groupProps");
                for (int i = 0; i < groupPropsList.size(); i++) {
                    JSONObject jsonObject = JSONObject.parseObject(groupPropsList.toArray()[i].toString());
                    if (jsonObject.toString().contains(Fields.INFORMATION)) {
                        JSONArray jsonpath = jsonObject.getJSONArray(Fields.INFORMATION);
                        for (int k = 0; k < jsonpath.size(); k++) {
                            JSONObject object = JSONObject.parseObject(jsonpath.toArray()[k].toString());
                            Set<Entry<String, Object>> state = object.entrySet();
                            for (Entry<String, Object> entry : state) {
                                Craw_goods_attribute_Info info = attributeInfo(entry.getKey().toString(), entry.getValue().toString(), parameter);
                                //insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
                                //insertList.add(insertItem);
                                /*封装数据 商品属性值**/
                                insertList.addAll(list(info));
                            }
                        }
                    }
                }
            }
            //判断商品1 批量抓取 4 实时抓取
            if (parameter.getListjobName().get(0).getStorage().equals("4")) {
                if (insertList != null && insertList.size() > 0) {
                    try {
                        getProductSave(insertList, database, Fields.CRAW_GOODS_ATTRIBUTE_INFO);
                    } catch (Exception e) {
                        logger.error("==>>插入商品属性失败失败, error:{}<<==", e);
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            logger.error("==>>获取商品属性失败 error:{}<<==", e);
        }
        return insertList;
    }

    //商品销量抓取
    public String productSellCount(Parameter parameter) {
        String sellCount = "";
        parameter.setItemUrl_1("https://mdskip.taobao.com/core/initItemDetail.htm?isUseInventoryCenter=true&cartEnable=true&service3C=false&isApparel=false&isSecKill=false&tmallBuySupport=true&isAreaSell=true&tryBeforeBuy=false&offlineShop=false&itemId=" + parameter.getEgoodsId() + "&showShopProm=false&isPurchaseMallPage=false&itemGmtModified=" + System.currentTimeMillis() + "&isRegionLevel=true&household=false&sellerPreview=false&queryMemberRight=true&addressLevel=3&isForbidBuyItem=false&callback=onMdskip&ref=&brandSiteId=0&isg=eB_TtVCrO3ZIANY9BOfanurza77OSIRYYuPzaNbMiOCP_y5B5RbVWZRAyeL6C3GVh6m9R3-cUFnkBeYBqQAonxvtUR-DrUDmn&isg2=BF9fYonMfNbJOXi9eJjHUgnP7rPpxLNmU21gTvGs-45VgH8C-ZRDtt1SQhD-A4ve");
        parameter.setItemUtl_2(Fields.TMALL_URL + parameter.getEgoodsId());
        parameter.setCookie("_samesite_flag_=true; cookie2=1dbf9b1573ab8622efcfb0eafd8eb76a; t=bface0e160a444c47331207cad376142; enc=mBEnGHv1RWbfgJbyJC5nHgapBFW2MaQTBKVn%2B5JKnGmg4G9bOWLw08whEZMmRUXKmejieNlSzEmrdrM4436H9g%3D%3D; hng=CN%7Czh-CN%7CCNY%7C156; cna=RIc+GBekZAkCAWdhPD2H2WcW; xlly_s=1; munb=900013774; lgc=%5Cu8D75%5Cu65ED%5Cu4E1Czx; dnk=%5Cu8D75%5Cu65ED%5Cu4E1Czx; tracknick=%5Cu8D75%5Cu65ED%5Cu4E1Czx; mt=ci=84_1; miid=2079790330138218716; tk_trace=oTRxOWSBNwn9dPyorMJE%2FoPdY8zfvmw%2Fq5v3hTEI6Ubvq6aoNw9VqzVUL6hA9Dc3Gv9wg3UUayqlMGZ5Fdzdft1VBYVS%2B8%2FtPtqFAkxovrIne7znJclT8BdR3kyUEvkNJp6e16UeRg0L42hiex6uLlRxvEfAzi9glaNRLxBuNVgTwEK7AJyd63qGZcLm0RdJjDvrgGQA81rQTQsAUo5Ac0PPGGUE4vsesSqMxQr88%2BIhOvLmX2oGW3P7nRqAil9AbY85YlzLSnzotL%2BeSDvC1MlBRC%2Bb; lLtC1_=1; _m_h5_tk=968aed1556e6baff88d1f69fca8175f9_" + System.currentTimeMillis() + "; _m_h5_tk_enc=721e97998cc9cc46a35da95fc255d77c; _tb_token_=37e1b9e396b4e; sgcookie=E100xLSUUQCnjyAA%2BYL5ynhgVE3DWRphc5FPHmi0joz4SrQ5f6yXPO1w4a1BlfA1lboIXeA6bvLKM1sUkIPudIZOJg%3D%3D; unb=900013774; uc3=nk2=tsV8GJVVWb0%3D&vt3=F8dCuf2HYBvmEcTL9yA%3D&id2=WvA07t216W%2BH&lg2=V32FPkk%2Fw0dUvg%3D%3D; uc1=cookie15=WqG3DMC9VAQiUQ%3D%3D&cookie21=URm48syIZJfmYzXrEixrAg%3D%3D&cookie14=Uoe0azJWHbmoxA%3D%3D&existShop=false; csg=38546fdb; cookie17=WvA07t216W%2BH; skt=644ca182e37511ac; uc4=nk4=0%40tBNcj8yLOLh%2FG2FEDL5S9U0fKg%3D%3D&id4=0%40WDf8BVOubjEyUuXGgpar3M255Yk%3D; _cc_=U%2BGCWk%2F7og%3D%3D; _l_g_=Ug%3D%3D; sg=x46; _nk_=%5Cu8D75%5Cu65ED%5Cu4E1Czx; cookie1=BxEyknt09TlDMkmLqiD%2Fvcub5fm3vir6qMvq%2FhISyXs%3D");
        try {
            String StringMessage = getRequest(parameter);
            if (StringMessage.contains("defaultModel")) {

                JSONObject json = JSONObject.parseObject(StringMessage);
                sellCount = json.getJSONObject("defaultModel").getJSONObject("sellCountDO").getString("sellCount");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sellCount;

    }


    // 根据UnicodeBlock方法判断中文标点符号
    public boolean isChinesePunctuation(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_FORMS
                || ub == Character.UnicodeBlock.VERTICAL_FORMS) {
            return true;
        } else {
            return false;
        }
    }


    //////////////////////////////////////////////////////关键词搜索///////////////////////////////////////////////////////////////////////////////////////////

    /***
     * 分页请求数据
     * @throws UnsupportedEncodingException
     **/
    public Map<String, String> productPage(Map<String, String> map, CrawKeywordsInfo crawKeywordsInfo, int page) {
        Map<String, String> tmall = new HashMap<String, String>();
        String productUrl = crawKeywordsInfo.getCust_keyword_url();
        try {
            if (crawKeywordsInfo.getCust_keyword_url().contains("pepsico.m.tmall.com") || crawKeywordsInfo.getCust_keyword_url().contains("guigeshipin.m.tmall.com")) {
                String keyword = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
                map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("KEYWORD", keyword).replace("PAGE", String.valueOf(page)));
                map.put("coding", "GBK");
                map.put("cookie", null);
                map.put("urlRef", productUrl.replace("KEYWORD", keyword).replace("PAGE", String.valueOf(page)));
            } else if (crawKeywordsInfo.getCust_keyword_url().contains("list.tmall.com") && crawKeywordsInfo.getCust_keyword_type().contains("keyword")) {
                if (crawKeywordsInfo.getCust_keyword_url().contains("SEARCH")) {
                    String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
                    map.put("url", productUrl.replace("PAGE", String.valueOf((page * 60))).replace("SEARCH", keyWord));
                    map.put("coding", "GBK");
                } else {
                    if (page != 1) {
                        productUrl = productUrl + "&s=" + 40 * page;
                        map.put("coding", "GBK");
                        map.put("url", productUrl);
                    }
                }
            } else if (crawKeywordsInfo.getCust_keyword_url().contains("search_radio_tmall") || crawKeywordsInfo.getCust_keyword_url().contains("filter_tianmao=tmall") || crawKeywordsInfo.getCust_keyword_url().contains("search_radio_all")) {
                String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
                productUrl = crawKeywordsInfo.getCust_keyword_url().replace("SEARCH", keyWord).replace("PAGE", String.valueOf((page - 1) * 44));
                map.put("url", productUrl);
                map.put("coding", "GBK");
            } else if (crawKeywordsInfo.getCust_keyword_url().contains("list.tmall.com") && crawKeywordsInfo.getCust_keyword_type().contains("category")) {//天猫指定类目搜索
                String codeCookie = map.get("codeCookie").toString();
                if (crawKeywordsInfo.getCust_keyword_url().contains("SEARCH")) {
                    String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
                    map.put("url", productUrl.replace("PAGE", String.valueOf((page * 60))).replace("SEARCH", keyWord));
                } else {
                    map.put("url", productUrl.replace("PAGE", String.valueOf((page * 60))).replace("CODE", codeCookie));
                }
                map.put("coding", "GBK");
            } else if (crawKeywordsInfo.getCust_keyword_url().contains("s.m.taobao.com")) {//手机端搜索商品
                String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
                productUrl = crawKeywordsInfo.getCust_keyword_url().replace("SEARCH", keyWord).replace("PAGE", String.valueOf((page)));
                map.put("url", productUrl);
            }
        } catch (Exception e) {
            logger.error("==>>拼接商品的搜索页，发送异常, error:{}<<==", e);
            e.printStackTrace();
        }
        return tmall;
    }

}
