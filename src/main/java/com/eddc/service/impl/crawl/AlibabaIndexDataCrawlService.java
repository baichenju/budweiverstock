/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.service 
 * @author: jack.zhao   
 * @date: 2018年5月11日 下午3:46:27 
 */
package com.eddc.service.impl.crawl;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.model.Craw_aliindex_category_info;
import com.eddc.model.Craw_aliindex_goodsrank_info;
import com.eddc.model.Craw_aliindex_samegoods_info;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.google.common.collect.Lists;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：AlibabaIndex_DataCrawlService   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年5月11日 下午3:46:27   
* 修改人：jack.zhao   
* 修改时间：2018年5月11日 下午3:46:27   
* 修改备注：   
* @version    
*    
*/
@Service
public class AlibabaIndexDataCrawlService {
	private static Logger logger=LoggerFactory.getLogger(AlibabaIndexDataCrawlService.class);
	//private static SimpleDateFormat fort=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	SqlSessionTemplate sqlSessionTemplate;
	@Autowired
	SearchDataCrawlService search_DataCrawlService;
	
	//封装数据
	@SuppressWarnings({ "static-access", "unchecked" })
	public void aliRequestDataInsert(Craw_aliindex_category_info info,String item,int rank_dimention_id,int rank_period_id,String database) throws Exception{
		//SimpleDateFormat sj = new SimpleDateFormat("yyyyMMdd"); 
		//SimpleDateFormat formt = new SimpleDateFormat("yyyy-MM-dd"); 
		//SimpleDateFormat formtdata = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		Map<String,Object> insertItem=new HashMap<String,Object>();
		List<Map<String,Object>>listData = Lists.newArrayList();
		Calendar calendar = Calendar.getInstance();
		calendar.add(calendar.DATE, -1);
		if(item.contains("content")){
			JSONObject currPriceJson = JSONObject.fromObject(item);	
			JSONArray Json = currPriceJson.getJSONObject("content").getJSONArray("hot");//热销榜
			if(Json.size()>0){
				for(int i=0;i<Json.size();i++){
					Float price=(float) 0.0; String sameGoodUrl="";
					try {
						Craw_aliindex_goodsrank_info goodsInfo=new Craw_aliindex_goodsrank_info();
						JSONObject ject=JSONObject.fromObject(Json.toArray()[i]);
						goodsInfo.setCategory_id(info.getCategory_id());//类别id
						goodsInfo.setRank_dimention_id(rank_dimention_id);//---1: trade; 2: flow
						goodsInfo.setRank_period_id(rank_period_id);//7: week; 30: month
						goodsInfo.setRank_type_id(1);////1 hot; 2: latest; 3: rise
						goodsInfo.setRank_date_id(Integer.valueOf(SimpleDate.SimpleDateTime().format(calendar.getTime())));
						goodsInfo.setGoods_rank(i+1);//排行榜位置
						goodsInfo.setRank_date(SimpleDate.SimpleDateData().format(calendar.getTime()));//排行日期
						goodsInfo.setInsert_time(SimpleDate.SimpleDateFormatData().format(new Date()));
						goodsInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
						if(ject.toString().contains("offerId")){
							goodsInfo.setEgoods_id(ject.getString("offerId"));	
						}
						if(ject.toString().contains("title")){
							goodsInfo.setGoods_name(ject.getString("title"));
						}
						if(ject.toString().contains("imgUrl")){
							goodsInfo.setImage_url(ject.getString("imgUrl"));	
						}
						if(ject.toString().contains("comName")){
							goodsInfo.setCompany_name(ject.getString("comName"));//公司名称	　	
						}
						if(ject.toString().contains("comUrl")){
							goodsInfo.setCompany_url(ject.getString("comUrl"));//公司链接	　	
						}
						if(ject.toString().contains("count")){
							goodsInfo.setComment_count(ject.getString("count"));//商品评论数　	
						}else{
							goodsInfo.setComment_count("0");//商品评论数　	
						}
						if(ject.toString().contains("trade")){
							goodsInfo.setGoods_trade(ject.getInt("trade"));//商品交易指数	
						}
						if(rank_dimention_id==2){
							if(ject.toString().contains("flow")){
								goodsInfo.setGoods_flow(Integer.valueOf(ject.getInt("flow")));//商品流量指数	
							}else{
								goodsInfo.setGoods_flow(0);//商品流量指数		
							}	
						}else{
							goodsInfo.setGoods_flow(0);//商品流量指数			
						}
						if(ject.toString().contains("price")){
							price=Float.valueOf(ject.getString("price"));
						}
						if(ject.toString().contains("sameGoodUrl")){
							sameGoodUrl=ject.getString("sameGoodUrl").toString();
						}
						goodsInfo.setSamegoods_source_url(sameGoodUrl);
						goodsInfo.setGoods_price(price);//商品价格
						insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(goodsInfo);
						listData.add(insertItem); 
					} catch (Exception e) {
		              logger.info("插入热销榜商品信息失败"+e.toString()+JSONObject.fromObject(Json.toArray()[i]));
					}
					
				}	
				search_DataCrawlService.insertIntoData(listData,database,Fields.CRAW_ALIINDE_GOODSRANK_INFO);	
			}
			listData.clear();
			JSONArray latest = currPriceJson.getJSONObject("content").getJSONArray("latest");//最新上榜
			if(latest.size()>0){
				Float price=(float) 0.0; String sameGoodUrl="";
				for(int i=0;i<latest.size();i++){
					try {
						Craw_aliindex_goodsrank_info goodsInfo=new Craw_aliindex_goodsrank_info();
						JSONObject ject=JSONObject.fromObject(latest.toArray()[i]);
						goodsInfo.setCategory_id(info.getCategory_id());//类别id
						goodsInfo.setRank_dimention_id(rank_dimention_id);//---1: trade; 2: flow
						goodsInfo.setRank_period_id(rank_period_id);//7: week; 30: month
						goodsInfo.setRank_type_id(2);////1 hot; 2: latest; 3: rise
						goodsInfo.setRank_date_id(Integer.valueOf(SimpleDate.SimpleDateTime().format(calendar.getTime())));
						goodsInfo.setGoods_rank(i+1);//排行榜位置
						goodsInfo.setRank_date(SimpleDate.SimpleDateData().format(calendar.getTime()));//排行日期
						goodsInfo.setInsert_time(SimpleDate.SimpleDateFormatData().format(new Date()));
						goodsInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
						if(ject.toString().contains("offerId")){
							goodsInfo.setEgoods_id(ject.getString("offerId"));	
						}
						if(ject.toString().contains("title")){
							goodsInfo.setGoods_name(ject.getString("title"));
						}
						if(ject.toString().contains("imgUrl")){
							goodsInfo.setImage_url(ject.getString("imgUrl"));	
						}
						if(ject.toString().contains("comName")){
							goodsInfo.setCompany_name(ject.getString("comName"));//公司名称	　	
						}
						if(ject.toString().contains("comUrl")){
							goodsInfo.setCompany_url(ject.getString("comUrl"));//公司链接	　	
						}
						if(ject.toString().contains("count")){
							goodsInfo.setComment_count(ject.getString("count"));//商品评论数　	
						}else{
							goodsInfo.setComment_count("0");//商品评论数　	
						}
						if(ject.toString().contains("trade")){
							goodsInfo.setGoods_trade(ject.getInt("trade"));//商品交易指数	
						}
						if(rank_dimention_id==2){
							if(ject.toString().contains("flow")){
								goodsInfo.setGoods_flow(Integer.valueOf(ject.getInt("flow")));//商品流量指数	
							}else{
								goodsInfo.setGoods_flow(0);//商品流量指数		
							}	
						}else{
							goodsInfo.setGoods_flow(0);//商品流量指数			
						}
						if(ject.toString().contains("price")){
							price=Float.valueOf(ject.getString("price"));
						}
						if(ject.toString().contains("sameGoodUrl")){
							sameGoodUrl=ject.getString("sameGoodUrl").toString();
						}
						goodsInfo.setSamegoods_source_url(sameGoodUrl);
						goodsInfo.setGoods_price(price);//商品价格
						insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(goodsInfo);
						listData.add(insertItem); 
					} catch (Exception e) {
						logger.info("插入最新上榜商品信息失败"+e.toString());
					}
				}	
				search_DataCrawlService.insertIntoData(listData,database,Fields.CRAW_ALIINDE_GOODSRANK_INFO);
				listData.clear();
			}
			JSONArray rise = currPriceJson.getJSONObject("content").getJSONArray("rise");//上升榜
			if(rise.size()>0){
				Float price=(float) 0.0; String sameGoodUrl="";
				for(int i=0;i<rise.size();i++){
					try {
						Craw_aliindex_goodsrank_info goodsInfo=new Craw_aliindex_goodsrank_info();
						JSONObject ject=JSONObject.fromObject(rise.toArray()[i]);
						goodsInfo.setCategory_id(info.getCategory_id());//类别id
						goodsInfo.setRank_dimention_id(rank_dimention_id);//---1: trade; 2: flow
						goodsInfo.setRank_period_id(rank_period_id);//7: week; 30: month
						goodsInfo.setRank_type_id(3);////1 hot; 2: latest; 3: rise
						goodsInfo.setRank_date_id(Integer.valueOf(SimpleDate.SimpleDateTime().format(calendar.getTime())));
						goodsInfo.setGoods_rank(i+1);//排行榜位置
						goodsInfo.setRank_date(SimpleDate.SimpleDateData().format(calendar.getTime()));//排行日期
						goodsInfo.setInsert_time(SimpleDate.SimpleDateFormatData().format(new Date()));
						goodsInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
						if(ject.toString().contains("offerId")){
							goodsInfo.setEgoods_id(ject.getString("offerId"));	
						}
						if(ject.toString().contains("title")){
							goodsInfo.setGoods_name(ject.getString("title"));
						}
						if(ject.toString().contains("imgUrl")){
							goodsInfo.setImage_url(ject.getString("imgUrl"));	
						}
						if(ject.toString().contains("comName")){
							goodsInfo.setCompany_name(ject.getString("comName"));//公司名称	　	
						}
						if(ject.toString().contains("comUrl")){
							goodsInfo.setCompany_url(ject.getString("comUrl"));//公司链接	　	
						}
						if(ject.toString().contains("count")){
							goodsInfo.setComment_count(ject.getString("count"));//商品评论数　	
						}else{
							goodsInfo.setComment_count("0");//商品评论数　	
						}
						if(ject.toString().contains("trade")){
							goodsInfo.setGoods_trade(ject.getInt("trade"));//商品交易指数	
						}
						if(rank_dimention_id==2){
							if(ject.toString().contains("flow")){
								goodsInfo.setGoods_flow(Integer.valueOf(ject.getInt("flow")));//商品流量指数	
							}else{
								goodsInfo.setGoods_flow(0);//商品流量指数		
							}	
						}else{
							goodsInfo.setGoods_flow(0);//商品流量指数			
						}
						if(ject.toString().contains("price")){
							price=Float.valueOf(ject.getString("price"));
						}
						if(ject.toString().contains("sameGoodUrl")){
							sameGoodUrl=ject.getString("sameGoodUrl").toString();
						}
						goodsInfo.setSamegoods_source_url(sameGoodUrl);
						goodsInfo.setGoods_price(price);//商品价格
						insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(goodsInfo);
						listData.add(insertItem); 
					} catch (Exception e) {
						logger.info("插入上升榜商品信息失败"+e.toString());
					}
				}	
				search_DataCrawlService.insertIntoData(listData,database,Fields.CRAW_ALIINDE_GOODSRANK_INFO);
				listData.clear();
			}
		}
		
	}
	//获取相似商品
	@SuppressWarnings("unchecked")
	public void indexSimilarGoodsData(Craw_aliindex_goodsrank_info goodsrank,String item,String database) throws Exception{
		//SimpleDateFormat formtdata = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		Map<String,Object> insertItem=new HashMap<String,Object>();
		List<Map<String,Object>>listData = Lists.newArrayList();
		if(item.contains("offerList")){
			try {
				JSONObject currPriceJson = JSONObject.fromObject(item);	
				JSONArray jsonList = currPriceJson.getJSONObject("data").getJSONArray("offerList");
			     for(int i=0;i<jsonList.size();i++){
			    	 JSONObject ject=JSONObject.fromObject(jsonList.toArray()[i]);
			    	 Craw_aliindex_samegoods_info info=new Craw_aliindex_samegoods_info();
			    	 info.setEgoods_id(ject.getJSONObject("aliTalk").getString("infoId"));//商品egoodsId
			    	 info.setGoods_name(ject.getJSONObject("information").getString("simpleSubject"));//商品名称
			    	 info.setSoure_egoods_id(goodsrank.getEgoods_id());//父节点商品egoo
			    	 info.setSoure_category_id(goodsrank.getCategory_id());//父节点商品id
			    	 info.setCompany_url(ject.getJSONObject("company").getString("memberCreditUrl"));//公司详情url
			    	 info.setImage_url(ject.getJSONObject("image").getString("imgUrlOf290x290"));//商品图片 
			    	 info.setInsert_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			    	 info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			    	 info.setCompany_name(ject.getJSONObject("aliTalk").getString("loginId"));//公司名称
			    	 if(ject.toString().contains("caigouPriceFen")){
			    		 Float caigouPriceFen=Float.valueOf(ject.getJSONObject("tradePrice").getJSONObject("offerPrice").getString("caigouPriceFen"));
			    		 Float caigouPriceYuan=Float.valueOf(ject.getJSONObject("tradePrice").getJSONObject("offerPrice").getString("caigouPriceYuan"));
			    		 info.setGoods_price(caigouPriceYuan+caigouPriceFen);	 
			    	 }else{
			    		 info.setGoods_price((float) 0);	 
			    	 }
			    	 insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(info);
					 listData.add(insertItem); 
			     }
			     search_DataCrawlService.insertIntoData(listData,database,Fields.CRAW_ALIINDE_SAMEGOODS_INF);
			} catch (Exception e) {
				logger.info("批量插入阿里相似产品报错 当前时间是："+SimpleDate.SimpleDateFormatData().format(new Date()) +"报错日志"+e.toString());
			}
			
		}
		
	}
}
