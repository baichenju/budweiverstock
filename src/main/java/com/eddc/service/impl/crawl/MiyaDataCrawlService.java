package com.eddc.service.impl.crawl;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.Fields;
import com.eddc.util.StringHelper;
import com.eddc.util.Validation;
import com.github.pagehelper.util.StringUtil;
/**
 * @author jack.zhao
 */
@Service("miyaDataCrawlService")
public class MiyaDataCrawlService {
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	private static Logger logger = LoggerFactory.getLogger(MiyaDataCrawlService.class); 
	//private static SimpleDateFormat forts=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
	/**
	 * 项目名称：springmvc_crawler
	 * 类名称：GoodsProductDetails
	 * 类描述：解析蜜芽页面信息
	 * 创建人：Jack
	 * 创建时间：2017年06月13日 上午10:50:10
	 * 修改备注：
	 *
	 * @throws SQLException
	 * @version
	 */
	public Map<String, Object> MiyaParseItemPage(String item, CrawKeywordsInfo wordsInfo, Map<String, String> map,String database,String storage) {
		Map<String, String> data = new HashMap<String, String>();
		Map<String, Object> Itemdata = new HashMap<String, Object>();
		data.putAll(map);
		String shopType = Fields. PROPRIETARY;//自营
		String sellerName =Fields.PROPRIETARY; //店铺
		String postageFree = Fields.DONTPACK_MAIL; //是否包邮
		String inventory =Fields.IS_NOT_STOCK; //库存
		String promotion = ""; //促销
		String image = ""; //图片路径
		String presentPrice = ""; //现价
		String originalPrice = ""; //原价
		String delivery_place = ""; //发货地址delivery_place
		String platformGoodsName="";
		String rateNum="";//评论数
		String product_location="";//产地
		try{
		Document docs =Jsoup.parse(item.toString()); 
		if(docs.toString().contains("priceBox")){
		platformGoodsName  = StringHelper.getResultByReg(docs.toString(), "bfd_name : \"([^,\"]+)");
		image = StringHelper.getResultByReg(docs.toString(), "bfd_image_link : \"([^,\"]+)");
		presentPrice=docs.getElementsByClass("priceBox").get(0).getElementsByClass("sp").html();//现价
		originalPrice=docs.getElementsByClass("priceBox").get(0).getElementsByTag("del").html().replaceAll("￥", "").trim();//原价
		postageFree=docs.getElementsByClass("priceBox").get(0).getElementsByClass("span-icon").html();//是否包邮
		rateNum=docs.getElementsByClass("title-cn").get(0).getElementsByTag("b").html();//评论
		inventory = StringHelper.getResultByReg(docs.toString(), "bfd_stock : ([^,]+)");
		if(inventory.equals(Fields.STATUS_OFF)){
			inventory=Fields.IS_NOT_STOCK;
		}else{
			inventory=Fields.IN_STOCK;
		}
		if(docs.toString().contains(Fields.YES_PROPRIETARY)){
			shopType=Fields.YES_PROPRIETARY;
			sellerName=Fields.MIYA_PROPRIETARY;
		}else{
			shopType=Fields.PROPRIETARY;
			sellerName=Fields.PROPRIETARY; 
		}
		try {
			Elements content =docs.getElementsByClass("w201506b parameters mt10").get(0).getElementsByTag("p");
			String[]  ement=content.toString().replaceAll("<p> ","").replaceAll("</p>","").replaceAll("<!--思源品牌，隐藏品牌-->", "").trim().split("<br>");
			for(int i=0;i<ement.length; i++){
				if(ement[i].contains(Fields.COUNTRY_ORIGIN)){
					product_location=ement[i].split("：")[1].toString();
				}
			}
		} catch (Exception e) {
			product_location="";
		}
		
		try {
			map.put("url", Fields.MIYA_APP_PROMOTION+wordsInfo.getCust_keyword_name()); 
			promotion=httpClientService.GoodsProduct(map);//请求库存
			if(Validation.isEmpty(promotion)){
				promotion=httpClientService.GoodsProduct(map);//请求库存
			}
		} catch (Exception e) {
			promotion="";
		}
		if(!Validation.isEmpty(promotion)){
			JSONObject currPriceJson = JSONObject.fromObject(promotion);
			JSONArray priceInfoJson = currPriceJson.getJSONArray("p");
			if(priceInfoJson.size()>0){
				String type=JSONObject.fromObject(priceInfoJson.toArray()[0]).getString("type");
				String name=JSONObject.fromObject(priceInfoJson.toArray()[0]).getString("name");
				promotion=type+":"+name;
			}else{
				promotion="";
			}
		  }
			data.put("originalPrice", originalPrice);//原价
			data.put("currentPrice", presentPrice);//现价
			data.put("shopName", sellerName);//店铺名称
			data.put("region",delivery_place);//卖家位置
			data.put("platform_shoptype", shopType);//平台商店类型
			data.put("sellerName", sellerName);//卖家店铺名称
			data.put("picturl", image);//图片url 
			data.put("goodsName", platformGoodsName);//商品名称
			data.put("delivery_place", delivery_place);//交易地址
			data.put("inventory", inventory);//商品库存
			data.put("postageFree",postageFree);//是不包邮
			data.put("skuId", wordsInfo.getCust_keyword_name());//商品sku
			data.put("egoodsId", wordsInfo.getCust_keyword_name());
			data.put("keywordId", wordsInfo.getCust_keyword_id());
			data.put("promotion", promotion);//促销
			data.put("product_location", product_location);//产地
			data.put("accountId",wordsInfo.getCust_account_id());
			data.put("goodsId", data.get("goodsId"));
			data.put("timeDate", data.get("timeDate"));
			data.put("platform_name",data.get("platform"));
			data.put("channel",Fields.CLIENT_MOBILE);
			data.put("feature1", "1");
			data.put("rateNum", rateNum);//商品总评论数
			data.put("subCatId", null);
			data.put("transactNum", null);//月销
			data.put("message", null);   
			data.put("deposit", null);
			data.put("coupons", null);
			data.put("sale_qty", null);
			data.put("reserve_num", null);
			data.put("shopid", null);//商品Id
			data.put("sellerId", null);
		   if(data.get("platform").equals(Fields.PLATFORM_MiA_EN)){
			  data.put("goodsUrl",Fields.MIYA_PC_URL+wordsInfo.getCust_keyword_name()+ ".html");
           }else{
        	  data.put("goodsUrl",Fields.MIYA_PC_GLOBAL_URL+wordsInfo.getCust_keyword_name()+ ".html");
           }
		    if(StringUtil.isEmpty(platformGoodsName)){
		    	Craw_goods_InfoVO info=crawlerPublicClassService.itemFieldsData(data,database,storage); //商品详情插入数据库 
		    	Craw_goods_Price_Info price=crawlerPublicClassService.parseItemPrice(data,database, wordsInfo.getCust_keyword_id(),data.get("goodsId"),data.get("timeDate"),data.get("platform"),Fields.STATUS_COUNT_1,storage);//APP端价格插入数据库
		    	Itemdata.put(Fields.TABLE_CRAW_GOODS_INFO, info);
		    	Itemdata.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, price);
		    	Itemdata.putAll(data);
		    }
			
		  }
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("【蜜芽数据插入信息失败  error:{}",e);
		}
		return Itemdata;
	}

	/**
	 * 项目名称：springmvc_crawler
	 * 类名称：GoodsProductDetails
	 * 类描述：解析蜜芽PC页面价格信息
	 * 创建人：Jack
	 * 创建时间：2017年06月13日 上午10:50:10
	 * 修改备注：
	 *
	 * @throws SQLException
	 * @version
	 */
	public Map<String, Object> ResolutionCellPhonePrices(CommodityPrices commodityPrices, String AppMessage,Map<String,String>map,String database,String storage) {
		Map<String, Object> data = new HashMap<String, Object>();
		String promotion = "";
		String presentPrice = "";
		String originalPrice = "";
		try {
			Document docs = Jsoup.parse(AppMessage.toString());
			presentPrice  = StringHelper.getResultByReg(docs.toString(),"siteprice: \"([^,\"]+)");//现价
			originalPrice  = StringHelper.getResultByReg(docs.toString(),"marketprice: \"([^,\"]+)");//原价
			try {
				map.put("url", Fields.MIYA_PC_PROMOTION+commodityPrices.getEgoodsId()); 
				promotion=httpClientService.GoodsProduct(map);//请求库存
				if(Validation.isEmpty(promotion)){
					promotion=httpClientService.GoodsProduct(map);//请求库存
				}
			} catch (Exception e) {
				promotion="";
			}
			if(!Validation.isEmpty(promotion)){
				JSONObject currPriceJson = JSONObject.fromObject(promotion);
				JSONArray priceInfoJson = currPriceJson.getJSONArray("p");
				if(priceInfoJson.size()>0){
					String type=JSONObject.fromObject(priceInfoJson.toArray()[0]).getString("type");
					String name=JSONObject.fromObject(priceInfoJson.toArray()[0]).getString("name");
					promotion=type+":"+name;
				}else{
					promotion="";
				}
			}
			data.put("channel",Fields.CLIENT_PC);
			data.put("currentPrice",presentPrice);
			data.put("originalPrice", originalPrice);
			data.put("promotion", promotion);
			data.put("goodsId", commodityPrices.getGoodsid());
			data.put("keywordId", commodityPrices.getCust_keyword_id());
			data.put("SKUid",commodityPrices.getEgoodsId());
			if(StringUtil.isNotEmpty(presentPrice)){
				Craw_goods_Price_Info price=crawlerPublicClassService.commdityPricesData(commodityPrices,data,database,storage);//插入PC端价格
				data.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, price);	
			}
		} catch (Exception e) {
			logger.info("【解析蜜芽PC端信息失败------error:{}】",e);
		}
		return data;

	}
	 
}
