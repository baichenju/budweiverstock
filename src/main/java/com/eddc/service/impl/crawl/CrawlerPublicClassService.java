package com.eddc.service.impl.crawl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.framework.TargetDataSource;
import com.eddc.mapper.CrawlerPublicClassMapper;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_aliindex_category_info;
import com.eddc.model.Craw_aliindex_goodsrank_info;
import com.eddc.model.Craw_cookies_info;
import com.eddc.model.Craw_goods_Fixed_Info;
import com.eddc.model.Craw_goods_Info;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Info_Failure;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Craw_goods_Vendor_Info;
import com.eddc.model.Craw_goods_comment_Info;
import com.eddc.model.Craw_goods_crowdfunding_Info;
import com.eddc.model.Craw_goods_pic_Info;
import com.eddc.model.Craw_goods_translate_Info;
import com.eddc.model.Craw_keywords_Info;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.Craw_monitor_url_info;
import com.eddc.model.Parameter;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.model.TmallSearchPrice;
import com.eddc.redis.JedisService;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.Validation;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;
import com.xxl.job.core.log.XxlJobLogger;

@Service("crawlerPublicClassService")
public class CrawlerPublicClassService {
	@Autowired
	private CrawlerPublicClassMapper crawlerPublicClassMapper;
	@Autowired
	private BatchExecutionCrawlService batchExecutionCrawlService;
	@Autowired
	private BatchFileUploadService batchFileUploadService;
	@Autowired
	private SearchDataCrawlService search_DataCrawlService;
	@SuppressWarnings("rawtypes")
	@Autowired
	private JedisService jedisService;
	// redis中存储的过期时间60s
	private static int expireTime = 24 * 60 * 60;
	private static Logger logger = LoggerFactory.getLogger(CrawlerPublicClassService.class);

	/*商品下架设置状态*/
	@TargetDataSource
	public String soldOutStatus(String status, String database, Parameter parameter) throws SQLException {
		try {
			SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Craw_goods_Info info = new Craw_goods_Info();
			info.setUpdate_date(form.format(new Date()));
			info.setUpdate_time(form.format(new Date()));
			info.setBatch_time(parameter.getBatch_time());
			info.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
			info.setGoodsId(parameter.getGoodsId());
			info.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
			info.setGoods_status(Integer.valueOf(Fields.STATUS_OFF));
			info.setEgoodsId(parameter.getEgoodsId());
			info.setGoods_url(parameter.getItemUtl_2());
			InsertShelvesDataCraw(info, database, null, Integer.valueOf(Fields.STATUS_OFF));
		} catch (Exception e) {
			logger.error("【插入数据下架商品失败：{}】", e);
		}
		return Fields.STATUS_OFF;
	}

	@TargetDataSource
	public Map<String, Object> InsertDatStatus(String status, String database, CrawKeywordsInfo Info, String goodsId, String timeDate, String dataType, String setUrl, String storage) throws SQLException {
		SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Map<String, Object> Data_status = new HashMap<String, Object>();
		Data_status.put("keywordId", Info.getCust_keyword_id());
		Data_status.put("goodsId", goodsId);
		Data_status.put("timeDate", timeDate);
		Data_status.put("platform_name", dataType);
		Data_status.put("goodsUrl", setUrl);
		Data_status.put("egoodsId", Info.getCust_keyword_name());
		//判断商品是否下架
		if (Fields.STATUS_OFF.equals(status)) {
			Craw_goods_InfoVO info = new Craw_goods_InfoVO();
			info.setUpdate_date(form.format(new Date()));
			info.setUpdate_time(form.format(new Date()));
			info.setBatch_time(timeDate);
			info.setCust_keyword_id(Integer.valueOf(Info.getCust_keyword_id()));
			info.setGoodsId(goodsId);
			info.setPlatform_name_en(dataType);
			info.setGoods_status(Integer.valueOf(Fields.STATUS_OFF));
			info.setEgoodsId(Info.getCust_keyword_name());
			if (dataType.equals(Fields.PLATFORM_TMALL_EN)) {
				setUrl = Fields.TMALL_URL + Info.getCust_keyword_name();
			}
			info.setGoods_url(setUrl);
			if (storage.contains(Fields.COUNT_4)) {
				Craw_goods_Info infos = new Craw_goods_Info();
				infos.setUpdate_date(form.format(new Date()));
				infos.setUpdate_time(form.format(new Date()));
				infos.setBatch_time(timeDate);
				infos.setCust_keyword_id(Integer.valueOf(Info.getCust_keyword_id()));
				infos.setGoodsId(goodsId);
				infos.setPlatform_name_en(dataType);
				infos.setGoods_status(Integer.valueOf(Fields.STATUS_OFF));
				infos.setEgoodsId(Info.getCust_keyword_name());
				if (dataType.equals(Fields.PLATFORM_TMALL_EN)) {
					setUrl = Fields.TMALL_URL + Info.getCust_keyword_name();
				}
				info.setGoods_url(setUrl);
				InsertShelvesDataCraw(infos, database, null, Integer.valueOf(Fields.STATUS_OFF));
			}
			Data_status.put(Fields.TABLE_CRAW_GOODS_INFO, info);
			Data_status.put("status", Fields.STATUS_OFF);
			return Data_status;
		} else if (Fields.STATUS_EXCEPTION.equals(status)) {
			// 网络异常,没抓取到数据 .插入到
			Craw_goods_Info_Failure failure = new Craw_goods_Info_Failure();
			failure.setUpdate_time(form.format(new Date()));
			failure.setCust_keyword_id(Integer.valueOf(Info.getCust_keyword_id()));
			failure.setGoodsid(goodsId);
			failure.setEgoodsid(Info.getCust_keyword_name());
			failure.setGoods_url(setUrl);
			failure.setPlatform_name(dataType);
			failure.setGoods_status(Integer.valueOf(Fields.STATUS_EXCEPTION));
			if (dataType.equals(Fields.PLATFORM_TMALL_EN)) {
				setUrl = Fields.TMALL_URL + Info.getCust_keyword_name();
			}
			InsertShelvesDataCraw(null, database, failure, Integer.valueOf(Fields.STATUS_EXCEPTION));
			Data_status.put("status", Fields.STATUS_EXCEPTION);
			return Data_status;
		}
		Data_status.put("status", Fields.STATUS_ON);
		return Data_status;
	}


	@SuppressWarnings("unchecked")
	public Craw_goods_InfoVO itemFieldsData(Map<String, String> map, String database, String storage) throws Exception {
		SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Map<String, Object> insertItem = new HashMap<String, Object>();
		List<Map<String, Object>> insertItems = Lists.newArrayList();
		Craw_goods_InfoVO info = new Craw_goods_InfoVO();
		info.setEgoodsId(map.get("egoodsId"));
		info.setGoodsId(map.get("goodsId"));
		info.setGoods_url(map.get("goodsUrl"));
		info.setBatch_time(map.get("timeDate"));
		info.setUpdate_date(form.format(new Date()));
		info.setDelivery_info(map.get("postageFree"));
		info.setUpdate_time(form.format(new Date()));
		info.setPlatform_goods_name(map.get("goodsName"));
		info.setPlatform_name_en(map.get("platform_name"));
		info.setInventory(map.get("inventory"));
		info.setSeller_location(map.get("region"));
		info.setDelivery_place(map.get("delivery_place"));
		info.setDelivery_info(map.get("postageFree"));
		info.setPlatform_shoptype(map.get("platform_shoptype"));
		info.setPlatform_shopname(map.get("shopName"));
		info.setPlatform_sellername(map.get("sellerName"));
		info.setCust_keyword_id(Integer.valueOf(map.get("keywordId").toString()));
		info.setFeature1(Fields.STATUS_ON);
		info.setPlatform_category(map.get("subCatId"));
		info.setGoods_pic_url(map.get("picturl"));
		info.setPlatform_shopid(map.get("shopid"));
		info.setPlatform_sellerid(map.get("sellerId"));
		info.setTtl_comment_num(map.get("rateNum"));
		info.setSale_qty(map.get("transactNum"));
		info.setGoods_status(Integer.valueOf(Fields.STATUS_ON));
		if (map.toString().contains("product_location")) {
			info.setProduct_location(map.get("product_location"));
		} else {
			info.setProduct_location("");
		}
		if (map.toString().contains("currentPrice")) {
			if (StringUtils.isEmpty(map.get("currentPrice"))) {
				info.setGoods_status(Integer.valueOf(Fields.STATUS_OFF));
			} else if (map.get("currentPrice").contains(Fields.STATUS_PRICE_DATA)) {
				info.setGoods_status(Integer.valueOf(Fields.STATUS_OFF));
			} else {
				info.setGoods_status(Integer.valueOf(Fields.STATUS_ON));
			}
		}
		if (map.toString().contains("jhs_paid_num")) {
			info.setJhs_paid_num(map.get("jhs_paid_num"));
		} else {
			info.setJhs_paid_num("");
		}
		if (Validation.isNotEmpty(storage)) {
			if (storage.equals(Fields.COUNT_4)) {
				insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
				insertItems.add(insertItem);
				search_DataCrawlService.insertIntoData(insertItems, database, Fields.TABLE_CRAW_GOODS_INFO);
			}
		}

		return info;
	}

	//单个插入价格
	public void crawgoodsPriceInfo(Craw_goods_Price_Info price) {
		crawlerPublicClassMapper.CrawgoodsPriceInfo(price);
	}

	@TargetDataSource
	public Craw_goods_Price_Info parseItemPrice(Map<String, String> map, String database, String keywordId, String goodsId, String batch_time, String platform, int pageTop, String storage) throws Exception {
		Map<String, Object> priceMap = new HashMap<String, Object>();
		Craw_goods_Price_Info price = new Craw_goods_Price_Info();
		if (!Validation.isEmpty(map.get("promotion"))) {
			if (map.get("promotion").contains("description")) {
				map.put("promotion", "null");
			}
		}
		if (Validation.isEmpty(map.get("skuId"))) {
			priceMap.put("skuId", null);
		} else {
			priceMap.put("skuId", map.get("skuId").toString());
		}
		priceMap.put("goodsId", goodsId);
		priceMap.put("keywordId", keywordId);
		priceMap.put("channel", map.get("channel").toString());
		try {
			price.setCust_keyword_id(keywordId);
			price.setGoodsid(goodsId);
			price.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			price.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			price.setBatch_time(batch_time);
			price.setOriginal_price(map.get("originalPrice"));
			price.setCurrent_price(map.get("currentPrice"));
			price.setPromotion(map.get("promotion"));
			price.setSKUid(map.get("skuId"));
			price.setDeposit(map.get("deposit"));//定金
			price.setTo_use_amount(map.get("coupons"));//优惠卷
			price.setInventory(map.get("inventory"));//库存
			price.setSale_qty(map.get("sale_qty"));//月销量
			price.setReserve_num(map.get("reserve_num"));//预定件数
			price.setChannel(map.get("channel").toString());
			price.setPlatform_name_en(platform);
			if (map.toString().contains("sku_name")) {
				price.setSku_name(map.get("sku_name").toString());//sku名称
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (storage.equals(Fields.COUNT_4)) {
			if (platform.equals(Fields.PLATFORM_TMALL_EN) || platform.equals(Fields.PLATFORM_TAOBAO_EN)) {
				if (map.get("client").equals("ALL") || map.get("client").equals("all")) {
					price.setChannel(Fields.CLIENT_MOBILE);
					crawlerPublicClassMapper.CrawgoodsPriceInfo(price);//App插入价格
					price.setChannel(Fields.CLIENT_PC);
					crawlerPublicClassMapper.CrawgoodsPriceInfo(price);//PC插入价格
				} else if (map.get("client").equals("MOBILE") || map.get("client").equals("mobile")) {
					price.setChannel(Fields.CLIENT_MOBILE);
					crawlerPublicClassMapper.CrawgoodsPriceInfo(price);
				}
			} else {
				crawlerPublicClassMapper.CrawgoodsPriceInfo(price); //插入商品价格详情
			}
		}
		return price;
	}

	@SuppressWarnings("unchecked")
	public Craw_goods_Price_Info commdityPricesData(CommodityPrices commodityPricesJD, Map<String, Object> map, String database, String storage) throws Exception {
		Map<String, Object> insertItem = new HashMap<String, Object>();
		List<Map<String, Object>> insertItems = Lists.newArrayList();

		Craw_goods_Price_Info price = new Craw_goods_Price_Info();
		price.setCust_keyword_id(commodityPricesJD.getCust_keyword_id());
		price.setGoodsid(commodityPricesJD.getGoodsid());
		price.setChannel(map.get("channel").toString());
		price.setOriginal_price(map.get("originalPrice").toString());
		price.setCurrent_price(map.get("currentPrice").toString());
		price.setPromotion(map.get("promotion").toString());
		price.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
		price.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		price.setBatch_time(commodityPricesJD.getBatch_time());
		price.setSKUid(commodityPricesJD.getEgoodsId());
		price.setTo_use_amount(null);//定金可抵卷 map.get("depositWorth")
		price.setDeposit(null); //定金 map.get("deposit")
		price.setReserve_num(null);//已预定件 map.get("HasScheduled")
		if (storage.equals(Fields.COUNT_4)) {
			insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(price);
			insertItems.add(insertItem);
			search_DataCrawlService.insertIntoData(insertItems, database, Fields.TABLE_CRAW_GOODS_PRICE_INFO);
		}
		return price;

	}
	//Commodity images

	/**
	 * @param 设定文件
	 * @return void    返回类型
	 * @throws
	 * @Title: commodityImages
	 * @Description: TODO(获取商品详情所以图片)
	 */
	public Craw_goods_pic_Info commodityImages(Map<String, String> Map, String imageurl, int i) {
		Craw_goods_pic_Info info = new Craw_goods_pic_Info();
		info.setBatch_time(Map.get("timeDate").toString());//
		info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setCust_keyword_id(Integer.valueOf(Map.get("keywordId").toString()));
		info.setGoodsid(Map.get("goodsId").toString());
		info.setPic_size(String.valueOf(i + 1));//第几张图片
		info.setPic_order(i + 1);
		info.setPic_url(imageurl);
		info.setPlatform_name(Map.get("dataType"));
		info.setEgoodsid(Map.get("egoodsId"));
		info.setPic_type("littlepic");
		return info;
	}

	@TargetDataSource
	public int InsertShelvesDataCraw(Craw_goods_Info info, String database, Craw_goods_Info_Failure failure, int sum) {//下架产品
		int count = 0;
		try {
			if (sum == Integer.valueOf(Fields.STATUS_OFF)) {
				//List<Craw_goods_Info>list=crawlerPublicClassMapper.dataMessageItemQuery(info.getCust_keyword_id(),info.getEgoodsId());
				//if(list.size()==0){
				count = crawlerPublicClassMapper.InsertShelvesDataCraw(info);
				//}
			} else {
				count = crawlerPublicClassMapper.InsertShelvesDataCrawFailure(failure);
			}

		} catch (Exception e) {
			count = 0;
		}
		return count;
	}

	@TargetDataSource
	public void SynchronousData(String platform, String satabases, String accountId, String tableName) {
		logger.info(">>> Start Synchronous " + platform + " data >>>");
		try {
			crawlerPublicClassMapper.SynchronousData(platform, accountId, tableName);//同步数据
			crawlerPublicClassMapper.SynchronousDataDelete(platform, accountId, tableName); //删除当前历史表数据
			logger.info(">>> Finish Synchronous " + platform + " data >>>");
		} catch (Exception e) {
			logger.error(">>> Synchronous data fail,please check! >>>");
			logger.error(e.getMessage());
		}
	}

	//定制同步数据
	@TargetDataSource
	public void SearchSynchronousDataAlibaba(String currentTable, String database, String currentPriceTable, String platform) {
		try {
			logger.info(">>> Start Synchronous [Alibaba] " + platform + " data >>>");
			crawlerPublicClassMapper.SearchSynchronousDataAlibaba(currentTable, currentPriceTable, platform);
			logger.info(">>> Finish Synchronous [Alibaba] " + platform + " data >>>");
		} catch (Exception e) {
			logger.error(">>> Synchronous [Alibaba] data fail,please check! >>>");
			logger.error(e.getMessage());
		}
	}

	//更新状态
	@TargetDataSource
	public void updateCrowdFunding(String table, String database) {
		try {
			logger.info(">>> Start updateCrowdFunding >>>");
			crawlerPublicClassMapper.updateCrowdFunding(table);
			logger.info(">>> Finish updateCrowdFunding >>>");
		} catch (Exception e) {
			logger.error(">>> updateCrowdFunding fail,please check! >>>");
			logger.error(e.getMessage());
		}
	}

	//查询爬虫url条数
	@TargetDataSource
	public List<CrawKeywordsInfo> crawAndParseInfo(List<QrtzCrawlerTable> listjobName, String database, String tableName, String count, int pageTop) {
		List<CrawKeywordsInfo> list = new ArrayList<CrawKeywordsInfo>();
		try {
//			list = crawlerPublicClassMapper.crawAndParseInfo(listjobName.get(0).getUser_Id(), listjobName.get(0).getPlatform(), tableName, Integer.valueOf(count), pageTop, listjobName.get(0).getSums());
//			if (pageTop == Fields.STATUS_COUNT_1) {//如果craw_keywords_temp_Info_forsearch没有数据就从历史表获取数据craw_keywords_temp_Info_forsearch_history
//				if (list.size() == 0) {
//					list = crawlerPublicClassMapper.crawAndParseInfo(listjobName.get(0).getUser_Id(), listjobName.get(0).getPlatform(), Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH_HISTORY, Integer.valueOf(count), pageTop, listjobName.get(0).getSums());
//				}
//			}
			logger.info(">>> Start crawAndParseInfo >>>");//listjobName.get(0).getSums()
			boolean key = jedisService.hasKey(listjobName.get(0).getJob_name());
			if (key) {
				list = jedisService.getList(listjobName.get(0).getJob_name(), CrawKeywordsInfo.class);
			} else {
				list = crawlerPublicClassMapper.crawAndParseInfo(listjobName.get(0).getUser_Id(), listjobName.get(0).getPlatform(), tableName, Integer.valueOf(count), pageTop, listjobName.get(0).getSums());
				if (pageTop == Fields.STATUS_COUNT_1) {//如果craw_keywords_temp_Info_forsearch没有数据就从历史表获取数据craw_keywords_temp_Info_forsearch_history
					if (list.size() == 0) {
						list = crawlerPublicClassMapper.crawAndParseInfo(listjobName.get(0).getUser_Id(), listjobName.get(0).getPlatform(), Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH_HISTORY, Integer.valueOf(count), pageTop, listjobName.get(0).getSums());
					}
				}
				if (list!=null&& list.size()>0) {
					jedisService.setList(listjobName.get(0).getJob_name(), list,listjobName.get(0).getRedis_time());	//listjobName.get(0).getJob_name()
				}
			}
			logger.info(">>> Finish crawAndParseInfo >>>");
		} catch (Exception e) {
			XxlJobLogger.log("==>>数据库："+database+",  平台："+listjobName.get(0).getPlatform()+"用户："+listjobName.get(0).getUser_Id()+ "请求商品数量 异常："+e+"<<==");
			try {
				list = crawlerPublicClassMapper.crawAndParseInfo(listjobName.get(0).getUser_Id(), listjobName.get(0).getPlatform(), tableName, Integer.valueOf(count), pageTop, listjobName.get(0).getSums());
				if (pageTop == Fields.STATUS_COUNT_1) {//如果craw_keywords_temp_Info_forsearch没有数据就从历史表获取数据craw_keywords_temp_Info_forsearch_history
					if (list.size() == 0) {
						list = crawlerPublicClassMapper.crawAndParseInfo(listjobName.get(0).getUser_Id(), listjobName.get(0).getPlatform(), Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH_HISTORY, Integer.valueOf(count), pageTop, listjobName.get(0).getSums());
					}
				}
			} catch (Exception ex) {
				XxlJobLogger.log("==>> 数据库："+database+",  平台："+listjobName.get(0).getPlatform()+"用户："+listjobName.get(0).getUser_Id()+ "请求商品数量 异常："+e+"<<==");
				logger.error(e.getMessage());
			}
		}
		return list;
	}

	//判断有多少条数据抓取失败重新抓取
	@TargetDataSource
	public List<CrawKeywordsInfo> crawAndGrabFailureGoodsData(List<QrtzCrawlerTable> listjobName, String database, String tableName, int pageTop) {
		List<CrawKeywordsInfo> list = new ArrayList<CrawKeywordsInfo>();
		try {
			logger.info(">>> Start crawAndGrabFailureGoodsData >>>");
			list = crawlerPublicClassMapper.crawAndGrabFailureGoodsData(listjobName.get(0).getUser_Id(), listjobName.get(0).getPlatform(), tableName, pageTop);
			logger.info(">>> Finish crawAndGrabFailureGoodsData >>>");
		} catch (Exception e) {
			logger.error(">>> crawAndGrabFailureGoodsData fail,please check! >>>");
			logger.error(e.getMessage());
			XxlJobLogger.log("==>> 数据库："+database+",  平台："+listjobName.get(0).getPlatform()+"用户："+listjobName.get(0).getUser_Id()+ "据抓取失败重新抓取 请求异常《》："+e+"<<==");
		}
		return list;
	}

	//获取商品的egoodsId等抓取商品价格
	@TargetDataSource
	public List<CommodityPrices> CommodityPricesData(List<QrtzCrawlerTable> listjobName, String database, String tableName, int pageTop) {
		List<CommodityPrices> list = new ArrayList<CommodityPrices>();
		try {
			//list = crawlerPublicClassMapper.queryForListEntye(Integer.valueOf(listjobName.get(0).getUser_Id()), listjobName.get(0).getPlatform(), tableName, pageTop);	
			logger.info(">>> Start CommodityPricesData >>>");
			boolean key = jedisService.hasKey(listjobName.get(0).getJob_name()+"_PC");
			if(key){
				list = jedisService.getList(listjobName.get(0).getJob_name()+"_PC", CommodityPrices.class);
			}else{
				list = crawlerPublicClassMapper.queryForListEntye(Integer.valueOf(listjobName.get(0).getUser_Id()), listjobName.get(0).getPlatform(), tableName, pageTop);	
				jedisService.setList(listjobName.get(0).getJob_name()+"_PC", list,listjobName.get(0).getRedis_time());
			}
			logger.info(">>> Finish CommodityPricesData >>>");
		} catch (Exception e) {
			try {
				list = crawlerPublicClassMapper.queryForListEntye(Integer.valueOf(listjobName.get(0).getUser_Id()), listjobName.get(0).getPlatform(), tableName, pageTop);	
			} catch (Exception e1) {
				logger.error(e1.getMessage());
			}
		}
		return list;

	}

	//判断商品价格抓取失败重新抓取1
	@TargetDataSource
	public List<CommodityPrices> RecursionFailureGoods(List<QrtzCrawlerTable> listjobName, String database, String tableName, int pageTop) {
		List<CommodityPrices> list = new ArrayList<CommodityPrices>();
		try {
			logger.info(">>> Start RecursionFailureGoods >>>");
			list = crawlerPublicClassMapper.RecursionFailureGoods(Integer.valueOf(listjobName.get(0).getUser_Id()), listjobName.get(0).getPlatform(), tableName, pageTop);
			logger.info(">>> Finish RecursionFailureGoods >>>");
		} catch (Exception e) {
			logger.error(">>> RecursionFailureGoods fail,please check! >>>");
			logger.error(e.getMessage());
			XxlJobLogger.log("==>> 数据库："+database+",  平台："+listjobName.get(0).getPlatform()+"用户："+listjobName.get(0).getUser_Id()+ "据抓取失败重新抓取 请求异常  RecursionFailureGoods："+e+"<<==");
		}
		return list;
	}

	//判断商品价格抓取失败重新抓取2
	@TargetDataSource
	public List<CommodityPrices> RecursionFailureGoods_Price(List<QrtzCrawlerTable> listjobName, String database, String tableName) {
		List<CommodityPrices> list = new ArrayList<CommodityPrices>();
		try {
			logger.info(">>> Start RecursionFailureGoods_Price >>>");
			list = crawlerPublicClassMapper.RecursionFailureGoods_Price(Integer.valueOf(listjobName.get(0).getUser_Id()), listjobName.get(0).getPlatform(), tableName);
			logger.info(">>> Finish RecursionFailureGoods_Price >>>");
		} catch (Exception e) {
			logger.error(">>> RecursionFailureGoods_Price fail,please check! >>>");
			logger.error(e.getMessage());
		}
		return list;
	}

	//抓取众筹商品状态是1的
	@TargetDataSource
	public List<Craw_goods_crowdfunding_Info> crowdFundingData(String platform, String database, int pageTop) {
		List<Craw_goods_crowdfunding_Info> list = new ArrayList<Craw_goods_crowdfunding_Info>();
		try {
			logger.info(">>> Start crowdFundingData >>>");
			list = crawlerPublicClassMapper.crowdFundingData(platform, pageTop);
			logger.info(">>> Finish crowdFundingData >>>");
		} catch (Exception e) {
			logger.info(">>> crowdFundingData fail,please check! >>>");
			logger.error(e.getMessage());
		}
		return list;
	}


	/**
	 * 项目名称：crawler_Price_Monitoring
	 * 类名称：screenShotItem_PC
	 * 类描述：  根据搜索页面获取商品价格search
	 * 创建人：jack.zhao
	 * 创建时间：2017年2月15日 上午9:54:04
	 *
	 * @throws Exception
	 * @version
	 */
	@TargetDataSource
	public List<TmallSearchPrice> SearchPriceQuery(String accountId, String database, String platformName) {
		List<TmallSearchPrice> list = crawlerPublicClassMapper.SearchPriceQuery(accountId, platformName);
		return list;
	}
	@TargetDataSource
	public String selectCrawlerCount(String accountId, String database, String platformName){
		return crawlerPublicClassMapper.selectCrawlerCount(accountId, platformName);	
	}

	/**
	 * @param @return 设定文件
	 * @return List<Craw_monitor_url_info>    返回类型
	 * @throws
	 * @Title: monitoringLeakData
	 * @Description: TODO(监控数据漏抓商品)
	 */
	@TargetDataSource
	public List<Craw_monitor_url_info> monitoringLeakData(String databases, String database) {
		List<Craw_monitor_url_info> list = crawlerPublicClassMapper.monitoringLeakData();
		return list;
	}

	//同步搜索历史数据
	@TargetDataSource
	public int historicalData(String database, String databases) {
		return crawlerPublicClassMapper.historicalData();
	}

	/**
	 * @param @param  accountId
	 * @param @param  platform
	 * @param @return 设定文件
	 * @return List<CrawKeywordsInfo>    返回类型
	 * @throws
	 * @Title: examineData
	 * @Description: TODO(检查数据是否正常抓取)
	 */
	@TargetDataSource
	public boolean examineData(List<QrtzCrawlerTable> listjobName, String database) {
		boolean dataCount = true;
		try {
			List<CrawKeywordsInfo> list = crawlerPublicClassMapper.examineData(listjobName.get(0).getUser_Id(), listjobName.get(0).getPlatform());
			if (null != list && list.size() > 0) {
				dataCount = true;
			} else {
				dataCount = false;
			}
		} catch (Exception e) {
			dataCount = false;
		}
		return dataCount;
	}


	//插入商品信息
	public int dataMessageItem(Craw_goods_Info info) {
		int count = 0;
		//List<Craw_goods_Info>list=crawlerPublicClassMapper.dataMessageItemQuery(info.getCust_keyword_id(),info.getEgoodsId());
		try {
			//if(list.size()==0){
			crawlerPublicClassMapper.Craw_Goods_Info_Insert(info);
			//}else{
			//	crawlerPublicClassMapper.Craw_Goods_Info_Update(info);
			//}
		} catch (Exception e) {
			count = 0;
			logger.info("插入信息失败--------------" + SimpleDate.SimpleDateFormatData().format(new Date()) + "------" + e.getMessage());
		}
		return count;
	}

	//查询价格失败的商品
	@TargetDataSource
	public List<CommodityPrices> RecursionFailureGoodsMessage(List<QrtzCrawlerTable> listjobName, String database, String channel, String tableName) {
		logger.info(">>> Start RecursionFailureGoodsMessage >>>");
		return crawlerPublicClassMapper.RecursionFailureGoodsMessage(Integer.valueOf(listjobName.get(0).getUser_Id()), listjobName.get(0).getPlatform(), channel, tableName);
	}

	public int updateDataPrice(String price, String Id) {
		int count = 0;
		try {
			logger.info(">>> Start updateDataPrice >>>");
			crawlerPublicClassMapper.updateDataPrice(price, Id);
			logger.info(">>> Finish updateDataPrice >>>");
		} catch (Exception e) {
			logger.error(">>> updateDataPrice fail,please check!");
			logger.error(e.getMessage());
			count = 0;
		}
		return count;

	}

	//判断商品是否下架
	public int Status_ShopName(String egoodsId, int status) {
		int count = 0;
		try {
			crawlerPublicClassMapper.Status_ShopName(egoodsId, status);
		} catch (Exception e) {
			count = 0;
		}
		return count;
	}

	//获取商品的Cookie
	@TargetDataSource
	public String dataCookie(String userId, String database, String platform) {
		String cookie = null;
		logger.info(">>> Start dataCookie >>>");
		List<Craw_cookies_info> list = crawlerPublicClassMapper.dataCookie(userId, platform);
		if (list.size() > 0) {
			cookie = list.get(0).getCookie();
		}
		logger.info(">>> Finish dataCookie >>>");
		logger.info(">>> Current cookie:" + cookie + " >>>");
		return cookie;
	}

	public int JudgewhetherCurrentDateDataFetching(String userId, String platform, String batch_time) {
		int count = 0;
		List<Craw_goods_Info> list = crawlerPublicClassMapper.JudgewhetherCurrentDateDataFetching(Integer.valueOf(userId), platform, batch_time);
		if (list.size() > 0) {
			count = 1;
		}
		return count;
	}

	//获取爬虫抓取失败商品个数
	public List<CrawKeywordsInfo> failure_information_list() {
		return crawlerPublicClassMapper.failure_information_list();
	}

	//获取商品城市code
	@TargetDataSource
	public List<Craw_keywords_delivery_place> Keywords_Delivery_Place(List<QrtzCrawlerTable> listjobName, String database) {
		String inventory = "";
		List<Craw_keywords_delivery_place> list = new ArrayList<Craw_keywords_delivery_place>();
		if (listjobName.get(0).getInventory().contains(",")) {
			String code[] = listjobName.get(0).getInventory().split(",");
			for (int i = 0; i < code.length; i++) {
				if (Validation.isEmpty(inventory)) {
					inventory = "'" + code[i] + "'";
				} else {
					inventory += "," + "'" + code[i] + "'";
				}
			}

		} else {
			inventory = "'" + listjobName.get(0).getInventory() + "'";
		}
		try {
			list = crawlerPublicClassMapper.Keywords_Delivery_Place(listjobName.get(0).getPlatform(), listjobName.get(0).getUser_Id(), "(" + inventory + ")");	
			//list = crawlerPublicClassMapper.Keywords_Delivery_Place(listjobName.get(0).getPlatform(), listjobName.get(0).getUser_Id(), "(" + inventory + ")");	
//			boolean key = jedisService.hasKey(listjobName.get(0).getJob_name()+"_stock");
//			if(key){
//				list = jedisService.getList(listjobName.get(0).getJob_name()+"_stock",Craw_keywords_delivery_place.class);
//			}else{
//				list = crawlerPublicClassMapper.Keywords_Delivery_Place(listjobName.get(0).getPlatform(), listjobName.get(0).getUser_Id(), "(" + inventory + ")");	
//				if(list!=null && list.size()>0){
//					jedisService.setList(listjobName.get(0).getJob_name()+"_stock", list,listjobName.get(0).getRedis_time());
//				}
//				
//			}
			
		} catch (Exception e) {
			list = crawlerPublicClassMapper.Keywords_Delivery_Place(listjobName.get(0).getPlatform(), listjobName.get(0).getUser_Id(), "(" + inventory + ")");
			e.printStackTrace();
		}
		return list;
	}

	//删除搜索重复数据
	@TargetDataSource
	public int deleteDuplicateData(List<QrtzCrawlerTable> listjobName, String database) {
		logger.info(">>> Start deleteDuplicateData >>>");
		return crawlerPublicClassMapper.deleteDuplicateData(listjobName.get(0).getPlatform(), listjobName.get(0).getUser_Id());
	}

	//判断Alibaba商品是否存在
	public List<Craw_goods_Vendor_Info> goodsShopMessage(String egoodsId) {
		logger.info(">>> Start goodsShopMessage >>>");
		return crawlerPublicClassMapper.goodsShopMessage(egoodsId);

	}

	//抓取商品上架时间
	@TargetDataSource
	public List<Craw_goods_Info> goodsOnTimeQuery(String cust_account_id, String database, String platform_name, int pageTop) {
		logger.info(">>> Start goodsOnTimeQuery >>>");
		return crawlerPublicClassMapper.goodsOnTimeQuery(cust_account_id, platform_name, pageTop);
	}

	//判断商品上架时间是否存在
	@TargetDataSource
	public List<Craw_goods_Fixed_Info> whetherGoodsShelves(Craw_goods_Info info, String database) {
		logger.info(">>> Start whetherGoodsShelves >>>");
		return crawlerPublicClassMapper.whetherGoodsShelves(info.getEgoodsId(), info.getPlatform_name_en(), info.getCust_account_id());

	}

	//京东商品上下架时间
	@TargetDataSource
	public List<Craw_goods_Info> jdGoodsOnTimeQuery(String cust_account_id, String database, String platform_name, int pageTop) {

		return crawlerPublicClassMapper.goodsOnTimeQuery(cust_account_id, platform_name, pageTop);
	}

	//京东获取商品的好评率
	public int updateCrawGoodsInfoMessage(Map<String, Object> map) {
		return crawlerPublicClassMapper.updateCrawGoodsInfoMessage(map);
	}

	//查询当前评论信息是否存在
	@TargetDataSource
	public List<Craw_goods_comment_Info> crawGoodsCommentInfoQuery(Craw_goods_Info info, String database) {
		logger.info(">>> Start crawGoodsCommentInfoQuery >>>");
		return crawlerPublicClassMapper.crawGoodsCommentInfoQuery(info.getEgoodsId(), info.getBatch_time());
	}

	//获取amazonusa 中文名称是否已经存在
	@TargetDataSource
	public List<Craw_goods_translate_Info> nameCommoditySwitchMessage(CrawKeywordsInfo inf, String database) {
		logger.info(">>> Start nameCommoditySwitchMessage >>>");
		Craw_goods_translate_Info info = new Craw_goods_translate_Info();
		info.setCust_account_id(Integer.valueOf(inf.getCust_account_id()));
		info.setCust_keyword_id(Integer.valueOf(inf.getCust_keyword_id()));
		info.setPlatform_name_en(inf.getPlatform_name());
		info.setEgoodsid(inf.getCust_keyword_name());
		return crawlerPublicClassMapper.nameCommoditySwitchMessage(info);

	}

	//获取阿里指数三级菜单
	public void alibabaIndex1688MessageInsert(Craw_aliindex_category_info info) {
		crawlerPublicClassMapper.alibabaIndex1688MessageInsert(info);
	}

	//获取阿里指数三级菜单信息
	public List<Craw_aliindex_category_info> aliIndexCateegoryInfoQuery() {
		return crawlerPublicClassMapper.aliIndexCateegoryInfoQuery();

	}

	//获取指数相似商品
	public List<Craw_aliindex_goodsrank_info> indexSimilarGoods(String rank_date) {
		return crawlerPublicClassMapper.indexSimilarGoods(rank_date);
	}

	/**
	 * @param @return 设定文件
	 * @return List<Craw_goods_Info>    返回类型
	 * @throws
	 * @Title: getCommentNumber
	 * @Description: TODO(获取商品的评论数)
	 */
	@TargetDataSource
	public List<Craw_goods_Info> getCommentNumber(List<QrtzCrawlerTable> listjobName, String database, String table) {
		String accountId = "";
		if (!listjobName.get(0).getJob_name().contains(Fields.ALL)) {
			accountId = listjobName.get(0).getUser_Id();
		}
		return crawlerPublicClassMapper.getCommentNumber(table, listjobName.get(0).getPlatform(), accountId);
	}

	//更新已经抓取商品的状态
	@TargetDataSource
	public void updateCrawKeywordHistory(List<QrtzCrawlerTable> listjobName, String database) {
		logger.info(">>> Start updateCrawKeywordHistory >>>");
		crawlerPublicClassMapper.updateCrawKeywordHistory(listjobName.get(0).getPlatform(), listjobName.get(0).getUser_Id());
	}

	//获取爬虫状态
	@TargetDataSource
	public int crawlerStatus(String platform, String database, String accountId) {
		int count = 0;
		try {
			logger.info(">>> Start crawlerStatus >>>");
			Craw_keywords_Info info = crawlerPublicClassMapper.crawlerStatus(platform, accountId);
			count = info.getCrawling_status();
		} catch (Exception e) {
			logger.error(">>> crawlerStatus fail,please check! >>>");
			logger.error(e.getMessage());
			count = 0;
		}
		return count;
	}

	//商品信息实例化
	public <T> void commodityInformationList(T entity, Map<String, Object> returnMap, String path_url) throws ClassNotFoundException {
		String skuId = "";
		String path = "";
		String egoodsId = "";
		String table = entity.getClass().getName().toString().substring(entity.getClass().getName().toString().lastIndexOf(".") + 1, entity.getClass().getName().toString().length());
		if (table.equalsIgnoreCase(Fields.TABLE_CRAW_GOODS_PRICE_INFO) || table.equalsIgnoreCase(Fields.TABLE_CRAW_VENDOR_PRICE_INFO)) {//价格
			skuId = getFieldValueByName("SKUid", entity) == null ? "" : getFieldValueByName("SKUid", entity).toString();
			path = "//" + returnMap.get("platform") + "#accountId-" + returnMap.get("accountId") + "#skuId-" + skuId + "#egoodsId-" + returnMap.get("egoodsId") + "#channel-" + getFieldValueByName("channel", entity) + "#" + table + ".txt";
		} else {
			if (returnMap.toString().contains("skuId")) {//详情
				skuId = returnMap.get("skuId") == null ? "" : returnMap.get("skuId").toString();
			}
			egoodsId = getFieldValueByName("egoodsId", entity).toString();
			if (StringUtil.isEmpty(egoodsId)) {
				egoodsId = getFieldValueByName("egoodsid", entity).toString();
			}
			path = "//" + returnMap.get("platform") + "#accountId-" + returnMap.get("accountId") + "#skuId-" + skuId + "#egoodsId-" + getFieldValueByName("egoodsId", entity) + "#" + table + ".txt";
		}
		Serializable(entity, path, path_url);
	}

	//对象实例化
	public <T> String Serializable(T entity, String path, String path_url) throws ClassNotFoundException {
		File file = new File(path_url);
		try {
			if (!file.exists()) {
				file.mkdirs();
			}
			ObjectOutputStream objOutputStream = new ObjectOutputStream(new FileOutputStream(path_url + path));
			objOutputStream.writeObject(entity);//写入对象
			objOutputStream.flush();
			objOutputStream.close();
			//反序列化一个对象
			//ObjectInputStream objInputStream = new ObjectInputStream(new FileInputStream(name));
			// Craw_goods_Info infos=(Craw_goods_Info)objInputStream.readObject();
			//System.out.println("读取person对象"+infos.getEgoodsId()+":"+infos.getPlatform_goods_name());
			//objInputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return entity.toString();
	}

	//批量插入数据
	@SuppressWarnings("rawtypes")
	@TargetDataSource
	public void bulkInsertData(ArrayList<Future> futureList, String database, String storage) {
		logger.info(">>> Invoke bulkInsertData , Storage is : " + storage + " >>>");
		try {
			if (null != futureList && futureList.size() > 0) {
				if (storage.contains(Fields.COUNT_0) || storage.contains(Fields.COUNT_2)) {
					batchExecutionCrawlService.insertCrawlData(futureList, database);//多线程批量插入数据

				}
				if (storage.contains(Fields.COUNT_01) || storage.contains(Fields.COUNT_2)) {//1 代表上传文件 0 代表数据插入数据库 2 代表 文件 数据库都要
					batchFileUploadService.getFutureCallback(futureList);//遍历任务的结果上传文件
					logger.info(">>> Finish Invoke getFutureCallback >>>");
				}
			}
		} catch (Exception e1) {
			logger.error(">>> bulkInsertData fail,please check!");
			logger.error(e1.getMessage());
		} finally {

		}
	}

	public void bulkInsertCompletionData(List<Map<String, Object>> lsitData, String database, String storage) {
		logger.info(">>> Invoke bulkInsertData , Storage is : " + storage + " >>>");
		try {
			if (null != lsitData && lsitData.size() > 0) {
				if (storage.contains(Fields.COUNT_0) || storage.contains(Fields.COUNT_2)) {
					insertCrawlCompletionData(lsitData, database);//多线程批量插入数据
				}
			}
		} catch (Exception e1) {
			logger.error(">>> bulkInsertData fail,please check!");
			logger.error(e1.getMessage());
		} finally {

		}
	}

	/**
	 * 根据数据库字段获取对应的信息
	 */
	public <T> String convertBean(T entity, Connection session) throws Exception {
		DatabaseMetaData data = null;
		String dataString = "";
		StringBuilder columnNameBuilder = new StringBuilder();
		String table = entity.getClass().getName().toString().substring(entity.getClass().getName().toString().lastIndexOf(".") + 1, entity.getClass().getName().toString().length());
		try {
			data = session.getMetaData();
			ResultSet rs = data.getColumns(null, "dbo", table.replace("VO", "").trim(), "%");
			while (rs.next()) {
				String colName = rs.getString("COLUMN_NAME");
				if (!colName.equalsIgnoreCase("Id")) {
					columnNameBuilder.append(getFieldValueByName(colName, entity) + "\\x00");
				}
			}
			dataString = columnNameBuilder.toString().substring(0, columnNameBuilder.length() - 4) + "\r\n";
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataString;
	}

	/**
	 * 根据属性名获取属性值
	 */
	protected Object getFieldValueByName(String fieldName, Object o) {
		try {
			String firstLetter = fieldName.substring(0, 1).toUpperCase();
			String getter = "get" + firstLetter + fieldName.substring(1);
			Method method = o.getClass().getMethod(getter, new Class[]{});
			Object value = method.invoke(o, new Object[]{});
			if (StringUtil.isEmpty(value.toString())) {
				value = null;
			}
			return value;
		} catch (Exception e) {
			return null;
		}
	}

	//封装数据
	@SuppressWarnings("unchecked")
	public void insertCrawlCompletionData(List<Map<String, Object>> lsitData, String database) throws Exception {
		List<Map<String, Object>> insert = Lists.newArrayList();
		List<Map<String, Object>> insertprice = Lists.newArrayList();
		String tableName = "";
		String tableNamePirce = "";
		try {
			for (int ii = 0; ii < lsitData.size(); ii++) {
				Map<String, Object> insertItemData = new HashMap<String, Object>();
				Map<String, Object> insertPriceData = new HashMap<String, Object>();
				if (lsitData.get(ii).toString().contains(Fields.TABLE_CRAW_GOODS_INFO)) {
					Craw_goods_InfoVO info = (Craw_goods_InfoVO) lsitData.get(ii).get(Fields.TABLE_CRAW_GOODS_INFO);
					tableName = Fields.TABLE_CRAW_GOODS_INFO;
					insertItemData = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
					insert.add(insertItemData);
				}
				if (lsitData.get(ii).toString().contains(Fields.TABLE_CRAW_GOODS_PRICE_INFO)) {
					Craw_goods_Price_Info price = (Craw_goods_Price_Info) lsitData.get(ii).get(Fields.TABLE_CRAW_GOODS_PRICE_INFO);
					tableNamePirce = Fields.TABLE_CRAW_GOODS_PRICE_INFO;
					insertPriceData = BeanMapUtil.convertBean2MapWithUnderscoreName(price);
					insertprice.add(insertPriceData);
				}
				//处理tmall商品价格
				if (lsitData.get(ii).toString().contains("goods_info_price")) {//Craw_goods_Price_Info
					List<Craw_goods_Price_Info> list = (List<Craw_goods_Price_Info>) lsitData.get(ii).get("goods_info_price");
					tableNamePirce = Fields.TABLE_CRAW_GOODS_PRICE_INFO;
					for (int is = 0; is < list.size(); is++) {
						Craw_goods_Price_Info info = list.get(is);
						if (lsitData.get(ii).get("client").toString().equalsIgnoreCase("ALL")) {
							info.setChannel(Fields.CLIENT_MOBILE);
							insertItemData = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
							insertprice.add(insertItemData);
							info.setChannel(Fields.CLIENT_PC);
							insertItemData = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
							insertprice.add(insertItemData);
						} else if (lsitData.get(ii).get("client").toString().equalsIgnoreCase("MOBILE")) {
							info.setChannel(Fields.CLIENT_MOBILE);
							insertItemData = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
							insertprice.add(insertItemData);
						}
					}
				}
			}
			if (insert.size() > 0) {
				search_DataCrawlService.insertIntoData(insert, database, tableName);
			}
			if (insertprice.size() > 0) {
				search_DataCrawlService.insertIntoData(insertprice, database, tableNamePirce);
			}
		} catch (Exception e) {
			logger.info("解析数据失败》》》》》》》》》》》》》》》》" + e.getMessage());
		}
	}
}
