package com.eddc.service.impl.crawl;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Parameter;
import com.eddc.service.AbstractProductAnalysis;
import com.eddc.util.Fields;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Slf4j
@Service("kidswantDataCrawlService")
public class KidswantDataCrawlService  extends AbstractProductAnalysis {
	 /**
	  * 请求数据
	  *
	  * */
	@Override
	public Map<String, Object> getProductLink(Parameter parameter) {
		Map<String, Object> item=new HashMap<String, Object>(10);
		log.info("==>>孩子王平台开始解析数据,当前解析的是第：{} 个商品<<==",parameter.getSum());
		String url=Fields.KIDSWANT_URL.replace(Fields.EDOODSID, parameter.getEgoodsId());
		if(parameter.getListjobName().get(0).getPlatform().equalsIgnoreCase(Fields.PLATFORM_KIDSWANT_OFF)){
			url=Fields.KIDSWANT_OFF_URL.replace(Fields.EDOODSID, parameter.getEgoodsId()).replace(Fields.PLATFORM_SHOPID, parameter.getCrawKeywordsInfo().getPlatform_shopid());
		}
		parameter.setItemUrl_1(url);
		parameter.setItemUtl_2(parameter.getCrawKeywordsInfo().getCust_keyword_url());
		item=getProductRequest(parameter);
		return item;
	}

	@Override
	public Map<String, Object> getProductItemAnalysisData(String StringMessage, Parameter parameter){

		Map<String, Object> item=new HashMap<String, Object>(10);
		StringBuilder bufer=new StringBuilder();
		Craw_goods_InfoVO info=productInfo(parameter);
		Craw_goods_Price_Info price=productInfoPrice(parameter);
		List<Map<String,Object>> insertItems = Lists.newArrayList();
		List<Map<String,Object>> insertPriceList = Lists.newArrayList();
		try {

			price.setGoodsid(parameter.getGoodsId());;
			price.setChannel(Fields.CLIENT_MOBILE);
			/* 线上*/
			if(parameter.getCrawKeywordsInfo().getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_KIDSWANT)){
				if(StringMessage.contains("pminfo")){
					JSONObject json = JSONObject.fromObject(StringMessage);
					int btnState =json.getInt("bt_state");
					if(btnState==1){
						bufer.append("暂不销售&&");	
					}

					/* 促销*/
					if(StringUtils.isNotEmpty(json.getString("bgcash"))){
						bufer.append("黑金PLUS:"+json.getString("bgcash")+"&&");
					}
					net.sf.json.JSONArray jsonArray =json.getJSONObject("pminfo").getJSONArray("gift_list");
					if(jsonArray!=null && jsonArray.size()>0){
						for(int i=0;i<jsonArray.size();i++){
							JSONObject object=JSONObject.fromObject(jsonArray.toArray()[i]);
							bufer.append(object.getString("name")+"&&");
						}

					}

					/*获取量贩价*/
					try {
						if (json.containsKey("predictprice")) {
							String liangfan_desc = json.getJSONObject("predictprice").getString("liangfan_desc");
							bufer.append(liangfan_desc).append("&&");
						}
					}catch (Exception e){
						log.warn("量贩价获取失败");
					}


					if(json.toString().contains("promotion_list")){
						JSONArray array =json.getJSONObject("pminfo").getJSONArray("promotion_list");
						if(array!=null && array.size()>0){
							for(int i=0;i<array.size();i++){
								JSONObject object=JSONObject.fromObject(array.toArray()[i]);
								bufer.append(object.getString("pm_ruletypedesc")+":"+object.getString("pm_info")+"&&");
							}

						}
					}
					price.setPromotion(bufer.toString());
					//自营非自营
					String cooperName=json.getString("cooper_name");
					if(cooperName.contains("孩子王")){
						cooperName=Fields.YES_PROPRIETARY;
						info.setPlatform_sellername("孩子王直营");
						info.setPlatform_shopname("孩子王直营");
					}else{
						cooperName=Fields.PROPRIETARY;
						info.setPlatform_sellername("非直营");
						info.setPlatform_shopname("非直营");
					}
					String cooper_shortname=json.getString("cooper_shortname");
					if(StringUtils.isNotEmpty(cooper_shortname)){
						if(cooperName.contains("直营")){
							cooperName=Fields.YES_PROPRIETARY;
							info.setPlatform_sellername("孩子王直营");
							info.setPlatform_shopname("孩子王直营");
						}else{
							cooperName=Fields.PROPRIETARY;
							info.setPlatform_sellername("非直营");
							info.setPlatform_shopname("非直营");
						}	
					}
					info.setPlatform_shoptype(cooperName);

					//运费
					String freight=json.getString("freight");
					if(freight.contains("不足则收取")){
						freight=Fields.DONTPACK_MAIL;
					}else{
						freight=Fields.PACK_MAIL;
					}
					info.setDelivery_info(freight);

					//原价
					Double marketPrice=Double.valueOf(json.getString("market_price"));
					String originalPrice=String.valueOf(marketPrice/100);
					price.setOriginal_price(originalPrice);
					//现价
					Double multiprice=Double.valueOf(json.getJSONObject("pminfo").getString("multiprice"));
					String blackgoldPrice=String.valueOf(multiprice/100);

					price.setCurrent_price(blackgoldPrice);

					//商品名称
					String productName=json.getString("name");
					info.setPlatform_goods_name(productName);

					//商品图片
					net.sf.json.JSONArray imageArray =json.getJSONArray("pic_list");
					if(imageArray!=null && imageArray.size()>0){
						JSONObject image=JSONObject.fromObject(imageArray.toArray()[0]);

						info.setGoods_pic_url(image.getString("url"));
					}
					//sku名称
					String skuName=json.getString("sale_attr");

					if(StringUtils.isNotEmpty(skuName)){
						String sku=skuName.substring(skuName.lastIndexOf("?")+1);
						price.setSku_name(sku);

					}
					//库存
					String stockNum=json.getJSONObject("stockinfo").getString("stock_num");
					info.setInventory(stockNum);
					price.setInventory(stockNum);

					//行业Id
					String categoryId=json.getString("category_id");
					info.setPlatform_category(categoryId);

					//产地
					String bNnation=json.getString("bd_nation");
					info.setProduct_location(bNnation);

					//发货地
					String disPlace=json.getString("dis_place");
					info.setDelivery_place(disPlace);

				}

				/* 线下*/	
			}else if(parameter.getCrawKeywordsInfo().getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_KIDSWANT_OFF)){
				if(StringMessage.contains("pminfo")){
					JSONObject json = JSONObject.fromObject(StringMessage);
					int btnState =json.getInt("bt_state");
					if(btnState==1 && btnState==0 && btnState==2){
						bufer.append("暂不销售&&");	
					}
					/* 促销*/
					if(StringUtils.isNotEmpty(json.getString("bgcash"))){
						bufer.append("黑金PLUS:"+json.getString("bgcash")+"&&");
					}
					if(json.containsKey("groupbuyinfo")) {
						try {
							if(json.getJSONObject("groupbuyinfo").containsKey("price_list")) {
								if (!json.toString().contains("\"price_list\":null,\"")) {
									JSONArray jsonArray =json.getJSONObject("groupbuyinfo").getJSONArray("price_list");	
									if(jsonArray!=null && jsonArray.size()>0){
										for(int i=0;i<jsonArray.size();i++){
											JSONObject object=JSONObject.fromObject(jsonArray.toArray()[i]);
											bufer.append(object.getString("people_num")+"人拼团价:￥"+(object.getInt("people_price")/100)+"&&");
										}
									}
								}
							}							
						} catch (Exception e) {
							e.printStackTrace();
						}

					}
					
					
					net.sf.json.JSONArray jsonArray =json.getJSONObject("pminfo").getJSONArray("promotion_list");
					if(jsonArray!=null && jsonArray.size()>0){
						for(int i=0;i<jsonArray.size();i++){
							JSONObject object=JSONObject.fromObject(jsonArray.toArray()[i]);
							bufer.append(object.getString("pm_ruletypedesc")+":"+object.getString("pm_info")+"&&");
						}

					}
					/*获取量贩价*/
					try {
						if (json.containsKey("predictprice")) {
							String liangfan_desc = json.getJSONObject("predictprice").getString("liangfan_desc");
							bufer.append(liangfan_desc).append("&&");
						}
					}catch (Exception e){
						log.warn("量贩价获取失败");
					}

					price.setPromotion(bufer.toString());

					//运费
					String freight=json.getString("freight");
					if(freight.contains("不足则收取")){
						freight=Fields.DONTPACK_MAIL;
					}else{
						freight=Fields.PACK_MAIL;
					}
					info.setDelivery_info(freight);

					//店铺名称
					String storeName=json.getString("rel_store_name");
					info.setPlatform_sellername(storeName);
					info.setPlatform_shopname(storeName);
					info.setPlatform_shoptype("非直营");
					info.setPlatform_shopid(parameter.getCrawKeywordsInfo().getPlatform_shopid());

					//原价
					Double marketPrice=Double.valueOf(json.getString("price"));
					String originalPrice=String.valueOf(marketPrice/100);
					price.setOriginal_price(originalPrice);
					//现价
					Double multiprice=Double.valueOf(json.getJSONObject("pminfo").getString("multiprice"));
					String blackgoldPrice=String.valueOf(multiprice/100);

					price.setCurrent_price(blackgoldPrice);

					//商品名称
					String productName=json.getString("skutitle");
					info.setPlatform_goods_name(productName);

					//商品图片
					net.sf.json.JSONArray imageArray =json.getJSONArray("pic_list");
					if(imageArray!=null && imageArray.size()>0){
						JSONObject image=JSONObject.fromObject(imageArray.toArray()[0]);

						info.setGoods_pic_url(image.getString("url"));
					}
					//sku名称
					net.sf.json.JSONArray skuName =json.getJSONArray("related_skulist");
					if(skuName!=null && skuName.size()>0){
						for(int i=0;i<skuName.size();i++){
							JSONObject skuMessage=JSONObject.fromObject(skuName.toArray()[i]);
							if(skuMessage.getString("skuid").contains(parameter.getGoodsId())){
								String sku=skuMessage.getString("skusaleattr").substring(skuMessage.getString("skusaleattr").lastIndexOf("?")+1);
								log.info(sku);
								price.setSku_name(sku);
								break;
							}	
						}
					}
					//库存
					String stockNum=json.getString("stock_num");
					info.setInventory(stockNum);
					price.setInventory(stockNum);
					//行业Id
					String categoryId=json.getString("erp_categoryid");
					info.setPlatform_category(categoryId);
				}
			}

			if(StringUtils.isNotEmpty(price.getCurrent_price())){


				//开始插入商品详情信息
				
				//insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(info);
				//insertItems.add(insertItem);
				
				/**封装数据 详情**/
				insertItems.addAll(list(info));
				
				getProductSave(insertItems,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_INFO);
				//search_DataCrawlService.insertIntoData(insertItems,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_INFO);

				//开始插入商品价格信息
				//insertPriceMap= BeanMapUtil.convertBean2MapWithUnderscoreName(price);
				//insertPriceList.add(insertPriceMap);
				
				/**封装数据 价格**/
				insertPriceList.addAll(list(price));
				
				getProductSave(insertPriceList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PRICE_INFO);
				//search_DataCrawlService.insertIntoData(insertPriceList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PRICE_INFO);

			}


		} catch (Exception e) {
			e.printStackTrace();
			log.error("==>>解析孩子王数据发生异常:"+e.getMessage()+"<<==");
		}
		item.put("Craw_goods_Info", info);
		item.put("Craw_goods_Price_Info", price);
		return item;
	}

	public String  speciaValue(String monster){
		String dest = "";  
		if (monster != null) {  
			dest = monster.replaceAll("[^0-9]","");  
		}  
		return dest; 
	}

}
