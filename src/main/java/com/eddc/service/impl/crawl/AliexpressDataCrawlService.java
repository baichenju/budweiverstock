package com.eddc.service.impl.crawl;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.eddc.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.Validation;
import com.eddc.util.WhetherShelves;
import com.google.common.collect.Lists;
/**
 * @author jack.zhao
 */
@Service
public class AliexpressDataCrawlService extends WhetherShelves {
	private static Logger logger=LoggerFactory.getLogger(AliexpressDataCrawlService.class);

	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	SearchDataCrawlService search_DataCrawlService; 

	/*****************************************解析数据****************************************************************************************************/
	public Map<String, Object> parameter(Parameter Par){
		Map<String, Object> itemKaoLa = new HashMap<>(10);
		Par.setItemUrl_1(Par.getCrawKeywordsInfo().getCust_keyword_url());
		Par.setItemUtl_2(Par.getCrawKeywordsInfo().getCust_keyword_url());
		try {
			//请求数据
			String itemPage = httpClientService.interfaceSwitch(Par);
			//判断商品是否下架
			String status = parseGoodsStatusByItemPage(itemPage);
			if (Integer.valueOf(status) == Integer.valueOf(Fields.STATUS_OFF)) {
				logger.info("==>>" + Par.getEgoodsId() + Par.getListjobName().get(0).getPlatform() + "当前商品已经下架,插入数据库 当前是第:" + Par.getSum() + "商品<<==");
				crawlerPublicClassService.soldOutStatus(status, Par.getListjobName().get(0).getDatabases(), Par);
				return itemKaoLa;
			}
			itemKaoLa = itemAppPage(itemPage, Par);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("==>>" + Par.getListjobName().get(0).getPlatform() + "解析数据失败 ,error:" + e.getMessage() + "<<==");
		}
		return itemKaoLa;
	}
	/**
	 *数据解析
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object>  itemAppPage(String itemPage, Parameter parameter) throws Exception{
		Map<String, Object> map=new HashMap<>();
		//价格
		Map<String, Object> insertPriceMap = new HashMap<String, Object>();
		List<Map<String, Object>> insertPriceList = Lists.newArrayList();
		//详情
		Map<String, Object> insertItemMap = new HashMap<String, Object>();
		List<Map<String, Object>> insertItemList = Lists.newArrayList();
		Craw_goods_InfoVO infovo=new Craw_goods_InfoVO();
		Craw_goods_Price_Info priceInfo = new Craw_goods_Price_Info();
		Map<String,Object> imageMap=new HashMap<String,Object>();
		List<Map<String,Object>> insertImage= Lists.newArrayList();

		if(!Validation.isEmpty(itemPage)){
			Document document = Jsoup.parse(itemPage);
			int index="var skuProducts=".length();
			String productId=document.toString().substring(document.toString().indexOf("var skuProducts=")+index,document.toString().lastIndexOf("}}];")+3);
			productId="{\"list\":"+productId+"}";
			try {
				//商品图片
				infovo.setGoods_pic_url(document.getElementsByClass("ui-image-viewer-thumb-frame").get(0).getElementsByTag("img").attr("src").trim());	 
			} catch (Exception e) {
				logger.error("【解析商品详情图片出现错误】"+e);
			}
			//获取商品图片
			try{
				List<Element>listImage=document.getElementById("j-image-thumb-list").select("li");	
				for(int s=0;s<listImage.size();s++){
					String imageThumb=listImage.get(s).getElementsByTag("img").attr("src").trim().replace("50x50","640x640");
					Craw_goods_pic_Info picInfo = new Craw_goods_pic_Info();
					picInfo.setBatch_time(parameter.getBatch_time());//
					picInfo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
					picInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
					picInfo.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
					picInfo.setGoodsid(parameter.getGoodsId());
					picInfo.setPic_size(String.valueOf(s + 1));//第几张图片
					picInfo.setPic_order(s + 1);
					picInfo.setPic_url(imageThumb);
					picInfo.setPlatform_name(parameter.getListjobName().get(0).getPlatform());
					picInfo.setEgoodsid(parameter.getEgoodsId());
					picInfo.setPic_type("littlepic");
					imageMap= BeanMapUtil.convertBean2MapWithUnderscoreName(picInfo);
					insertImage.add(imageMap);
				}
			}catch(Exception e){
				logger.error("【解析商品详情小图片出现错误】"+e);
			}


			infovo.setPlatform_shopname(document.getElementsByClass("store-name notranslate").text());//店铺名称
			infovo.setEgoodsId(parameter.getEgoodsId());
			infovo.setGoodsId(parameter.getGoodsId());
			infovo.setSeller_location(document.getElementsByClass("store-address").text());//卖家位置
			infovo.setPlatform_sellername(document.getElementsByClass("store-name notranslate").text());

			infovo.setSale_qty(document.getElementsByClass("order-num").text());
			infovo.setDelivery_info(Fields.PACK_MAIL);//是不包邮
			infovo.setBatch_time(parameter.getBatch_time());
			infovo.setGoods_url(parameter.getItemUtl_2());
			infovo.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
			infovo.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));

			infovo.setGoods_status(1);//状态
			infovo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			infovo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));

			insertItemMap=BeanMapUtil.convertBean2MapWithUnderscoreName(infovo);
			insertItemList.add(insertItemMap);



			/*****************************商品价格*******************************************/
			try {
				JSONObject json = JSONObject.parseObject(productId.toString());
				JSONArray arry =json.getJSONArray("list");
				for(int i=0;i<arry.size();i++){
					JSONObject listJson=JSON.parseObject(arry.toArray()[i].toString()); 
					if(listJson.toString().contains("skuAttr")){
						priceInfo.setSku_name(listJson.getString("skuAttr").substring(listJson.getString("skuAttr").lastIndexOf("#")+1).trim());
					}
					infovo.setInventory(listJson.getJSONObject("skuVal").getString("inventory"));//月销
					priceInfo.setInventory(listJson.getJSONObject("skuVal").getString("inventory"));//库存
					priceInfo.setOriginal_price(listJson.getJSONObject("skuVal").getString("skuCalPrice"));
					priceInfo.setCurrent_price(listJson.getJSONObject("skuVal").getString("actSkuCalPrice"));
					priceInfo.setSKUid(listJson.getString("skuId"));//商品sku

					priceInfo.setBatch_time(parameter.getBatch_time());
					priceInfo.setGoodsid(parameter.getGoodsId());
					priceInfo.setSKUid(parameter.getEgoodsId());
					priceInfo.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
					priceInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
					priceInfo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
					insertPriceMap=BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
					insertPriceList.add(insertPriceMap);
				}
			} catch (Exception e) {
				logger.error("【解析商品详情出现错误】"+e);
				e.printStackTrace();
			}
			if(insertPriceList.size()>0){

				search_DataCrawlService.insertIntoData(insertItemList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_INFO);
				search_DataCrawlService.insertIntoData(insertImage,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PIC_INFO);
				search_DataCrawlService.insertIntoData(insertPriceList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PRICE_INFO);
			}

		}
		return map;
	}
}
