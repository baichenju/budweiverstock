package com.eddc.service.impl.crawl;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import java.util.Map.Entry;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Craw_goods_attribute_Info;
import com.eddc.model.Craw_goods_comment_Info;
import com.eddc.model.Craw_goods_pic_Info;
import com.eddc.model.Parameter;
import com.eddc.service.AbstractProductAnalysis;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.Validation;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;
@Service("taobaoDataCrawlService")
@Slf4j
public class TaobaoDataCrawlService extends  AbstractProductAnalysis{
	private static Logger logger=LoggerFactory.getLogger(TaobaoDataCrawlService.class);
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	SearchDataCrawlService search_DataCrawlService;

	/***************************************************************************************************************************************************/
	/**
	 * 反射机制脱离了spring容器的管理，导致@Resource等注解失效。
	 * jack.zhao
	 */
	public static TaobaoDataCrawlService taobaoDataCrawlService;
	@PostConstruct
	public void init () {
		taobaoDataCrawlService=this;
	}


	@Override
	public Map<String, Object> getProductLink(Parameter parameter) {
		Map<String,Object> map=new HashMap<>();
		if(StringUtils.isEmpty(parameter.getDeliveryPlace().getDelivery_place_code())){
			parameter.getDeliveryPlace().setDelivery_place_code("310100");
		}
		//String url="http://api.vephp.com/tbprodetail?vekey=V00005620Y22722244&id="+parameter.getEgoodsId()+"&areaId="+parameter.getDeliveryPlace().getDelivery_place_code();
		//String url="http://api.ds.dingdanxia.com/shop/good_info2?apikey=hV4Hcm3vhRlBBbD6pM4IJlTrPWQLyz3c&id="+Par.getEgoodsId()+"&areaId="+Par.getPlace_code();

		//String url="http://api.ds.dingdanxia.com/shop/good_info2&apikey=NeR0Lp1Wlg7tXgKPugbBdfAUTJEVcicc&id="+parameter.getEgoodsId();
		//String url="http://api.vephp.com/tbprodetail?vekey=V00005620Y02376026&id="+parameter.getEgoodsId();
		parameter.setItemUrl_1(parameter.getListjobName().get(0).getApi_url().replace("-", "?")+"&id="+parameter.getEgoodsId());
		parameter.setItemUtl_2( Fields.TMALL_URL+parameter.getEgoodsId());
		map=getProductRequest(parameter);
		return map;
	}


	/*数据解析*/
	@Override
	@SuppressWarnings("unchecked")
	public  Map<String, Object>getProductItemAnalysisData(String StringMessage,Parameter parameter) throws Exception{
		Map<String,Object > map=new HashMap<String,Object >();

		Map<String,Object> insertItem=new HashMap<String,Object>(10);
		

		//价格
		
		List<Map<String, Object>> insertPriceList = Lists.newArrayList();

		//详情
		Map<String, Object> insertItemMap = new HashMap<String, Object>(10);
		List<Map<String, Object>> insertItemList = Lists.newArrayList();
		//图片
		List<Map<String,Object>> insertImage= Lists.newArrayList();
		List<Map<String,Object>> commentList= Lists.newArrayList();
		//商品规格属性
		List<Map<String,Object>> attributeList=Lists.newArrayList();


		List<Craw_goods_Price_Info>priceList=new ArrayList<Craw_goods_Price_Info>(10);
		Craw_goods_comment_Info comment=new Craw_goods_comment_Info();
		Craw_goods_InfoVO infovo=productInfo(parameter);

		StringBuffer goodsComment=new StringBuffer();
		String shopName=null,platform_shoptype=null,subCatId=null,sellerId=null,picturl=null,goodsName=null,delivery_place=null;
		String postageFree=Fields.PACK_MAIL;
		String originalPrice=null;
		String currentPrice=null;
		String skuId=null;
		String shopId=null;  
		String location="";
		String totalSoldQuantity="0";
		String commentCount="";
		String quantity="";
		String priceDesc="";
		String inventory="";//库存
		String	deposit="";//定金
		String  coupons="";//卷
		String reserve_num="";//预定件数
		String present="";//赠品
		String handsel="";
		String sellerName="";//卖家
		String soldCount="";//付款人数
		String sellCount="sellCount";
		JSONArray props=new JSONArray();
		StringBuffer prom = new StringBuffer();
		String database=parameter.getListjobName().get(0).getDatabases();
		if(StringMessage.toString().contains("apiStack")){
			JSONObject json=JSONObject.fromObject(StringMessage.toString());
			JSONArray arry=json.getJSONObject("data").getJSONArray("apiStack");
			JSONObject images=json.getJSONObject("data").getJSONObject("item");
			subCatId=json.getJSONObject("data").getJSONObject("item").get("categoryId").toString();
			shopName=json.getJSONObject("data").getJSONObject("seller").get("shopName").toString();//店铺
			sellerId=json.getJSONObject("data").getJSONObject("seller").get("userId").toString();//用户ID
			shopId=json.getJSONObject("data").getJSONObject("seller").get("shopId").toString();//店铺Id

			if(shopName.contains(Fields.TMART)){
				if(!shopName.contains(Fields.TMART_MAIN_VENUE)){
					platform_shoptype=Fields.SUPERMARKET;
					shopId = Fields.SHIPID;
					shopName=Fields.TMART; 
				}else{ 
					if(shopName.indexOf(Fields.FLAGSHIP_STORE)>0){
						platform_shoptype=Fields.DAY_CAT_FLAGSHIP_STORE;
					}else {
						platform_shoptype=Fields.PROPRIETARY;
					}
				}
			}else{
				if(shopName.indexOf(Fields.FLAGSHIP_STORE)>0){
					platform_shoptype=Fields.DAY_CAT_FLAGSHIP_STORE;
				}else{
					platform_shoptype=Fields.PROPRIETARY;
				}
			}

			try {
				//获取商品小图片
				if(StringUtil.isNotEmpty(images.get("images").toString())){
					JSONArray jsonArray = JSONArray.fromObject(images.get("images").toString());
					for (int i = 0; i < jsonArray.size(); i++) {
						try {
							if(i==0){
								picturl="http:"+jsonArray.getString(i);	
							}
							//判断是否抓取商品小图片
							if(parameter.getListjobName().get(0).getDescribe().contains("抓取商品小图片")) {
								Craw_goods_pic_Info info=commodityImages(parameter,"http:"+jsonArray.getString(i),i);//获取小图片
								insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(info);
								insertImage.add(insertItem);	
							}
						} catch (Exception e) {
							logger.error("==>>解析商品小图片失败 ，error:{}<<==",e);
						}

					}	
				}
			} catch (Exception e) {
				logger.error("==>>天猫获取商品小图片失败 error:{}<<==",e);
			}
			if(StringUtil.isEmpty(picturl)){
				Set<Entry<String, Object>> entrySet = images.entrySet();
				for(Entry<String, Object> entry : entrySet) {//商品图片
					if(entry.getKey().equals("images")) { 
						JSONArray jr2 = (JSONArray)entry.getValue();
						picturl=jr2.getString(0);
						break;
					}
				}
			}
			//商品开团时间
			if(arry.toString().contains("channel")){
				try {
					JSONObject coupon=JSONObject.fromObject(arry.toArray()[0]);
					if(coupon.getJSONObject("value").toString().contains("channel")){
						if(coupon.getJSONObject("value").getJSONObject("consumerProtection").getJSONObject("channel").toString().contains("title")){
							String channelTitle=coupon.getJSONObject("value").getJSONObject("consumerProtection").getJSONObject("channel").getString("title");
							prom.append(channelTitle+"&&"); 	
						}
						if(coupon.getJSONObject("value").toString().contains("hintBanner")){
							prom.append(coupon.getJSONObject("value").getJSONObject("trade").getJSONObject("hintBanner").getString("text")+"&&");
						}
					}	
				} catch (Exception e) {
					logger.error("==>>获取商品开团时间，发生异常 error:{}<<==",e);
				}

			}
			if(arry.toString().contains("tmallCoupon")){//判断优惠卷是否存在
				JSONObject coupon=JSONObject.fromObject(arry.toArray()[0]);
				if(coupon.getJSONObject("value").toString().contains("coupon")){
					String couponData=coupon.getJSONObject("value").getJSONObject("resource").getJSONObject("entrances").getJSONObject("coupon").getString("text");
					if(Validation.isNotEmpty(couponData)){
						prom.append(couponData+"&&"); 
					}

				}
			}

			if(arry.toString().contains("couponList")){//判断优惠卷是否存在
				JSONObject coupon=JSONObject.fromObject(arry.toArray()[0]);
				if(coupon.getJSONObject("value").toString().contains("coupon")){
					JSONArray couponData=coupon.getJSONObject("value").getJSONObject("resource").getJSONObject("coupon").getJSONArray("couponList");
					if(couponData.size()>0){
						for(int k=0;k<couponData.size();k++){
							JSONObject object=JSONObject.fromObject(couponData.toArray()[k].toString());
							prom.append(object.getString("title")+"&&");	
						}

					}
				}
				//商品开卖时间
				if(coupon.getJSONObject("value").toString().contains("bigPromotion")){
					if(coupon.getJSONObject("value").getJSONObject("resource").getJSONObject("bigPromotion").toString().contains("memo")){
						try {
							JSONArray array=coupon.getJSONObject("value").getJSONObject("resource").getJSONObject("bigPromotion").getJSONArray("memo");
							if(array!=null && array.size()>0){
								JSONObject object=JSONObject.fromObject(array.toArray()[0].toString());
								prom.append(object.getString("text"));
							}

						} catch (Exception e) {
							logger.error("==>>获取商品开卖时间，发生异常 error:{}<<==",e);
						}	
					}
				}
			}
			if(parameter.getListjobName().get(0).getPlatform().contentEquals("taobao")){
				//快递发货地址
				if(arry.toString().contains("from") || arry.toString().contains("completedTo")){
					JSONObject coupon=JSONObject.fromObject(arry.toArray()[0]);
					if(coupon.getJSONObject("value").getJSONObject("delivery").toString().contains("completedTo")){
						delivery_place=coupon.getJSONObject("value").getJSONObject("delivery").get("completedTo").toString();//快递发货地址	

					}else{
						try {
							if(coupon.getJSONObject("value").getJSONObject("delivery").toString().contains("from")){
								delivery_place=coupon.getJSONObject("value").getJSONObject("delivery").get("from").toString();//快递发货地址
								location=coupon.getJSONObject("value").getJSONObject("delivery").getString("to");//卖家地址
							}
						} catch (Exception e) {
							log.error("==>>天猫解析地址报错"+e.getMessage()+"<<==");
						}
					}

					if(coupon.getJSONObject("value").containsKey("resource")) {
						if(coupon.getJSONObject("value").getJSONObject("resource").containsKey("shopProm")) {
							JSONArray shopProm=coupon.getJSONObject("value").getJSONObject("resource").getJSONArray("shopProm");
							if(shopProm!=null && shopProm.size()>0) {
								if(shopProm.toString().contains("[\"title\"]")) {
									prom.append(JSONObject.fromObject(shopProm.toArray()[0].toString()).getString("title"));
								}else if(shopProm.toString().contains("iconText")) {
									String iconText=JSONObject.fromObject(shopProm.toArray()[0].toString()).getString("iconText");
									String content=JSONObject.fromObject(shopProm.toArray()[0].toString()).getString("content");
									prom.append(iconText+":"+StringUtils.replaceEach(content, new String[] {"[","]","\""},new String[] {"","",""}));		
								}

							}
						}

					}

				}
			}
			if(arry.toString().contains(sellCount)){//判断商品是否存在 propPath sellCount

				for(int i=0;i<arry.size();i++){
					JSONObject js=JSONObject.fromObject(arry.toArray()[i]);
					try {
						deposit=js.getJSONObject("value").getJSONObject("price").getJSONObject("price").getString("priceText");//定金
						if(js.getJSONObject("value").getJSONObject("price").toString().contains("depositPrice")){
							coupons=js.getJSONObject("value").getJSONObject("price").getJSONObject("depositPrice").get("priceDesc").toString();//定金卷	
							if(coupons.contains(Fields.REDUCTION_OF)) {
								coupons=StringHelper.getResultByReg(coupons,"([0-9]+)");
							}else {
								coupons=""; 
							}
						}
						handsel=js.getJSONObject("value").getJSONObject("price").getJSONObject("price").getString("priceTitle");//文字定金
						if(coupons.contains("，")){
							deposit=coupons.split("，")[0].toString();//定金
							deposit=StringHelper.getResultByReg(deposit,"([0-9]+)");
							if(coupons.length()>1){
								coupons=coupons.split("，")[1].toString();//优惠卷
								coupons=StringHelper.getResultByReg(coupons,"([0-9]+)");
							}
						}else{
							if(StringUtils.isNotEmpty(StringHelper.getResultByReg(priceDesc,"([0-9]+)"))){
								coupons=StringHelper.getResultByReg(priceDesc,"([0-9]+)");//抵用金额	
							}	
						}
						if(StringUtils.isNotBlank(coupons)) {
							prom.append(Fields.PRESELL_MESSAGE+"&&");	
						}

					} catch (Exception e) {
						deposit="";coupons="";
					}
					try {
						totalSoldQuantity=js.getJSONObject("value").getJSONObject("item").get("sellCount").toString();//月销
					} catch (Exception e) {
						totalSoldQuantity="0";
					} 
					try {location=js.getJSONObject("value").getJSONObject("skuCore").getJSONObject("skuItem").get("location").toString();} catch (Exception e) {}
					try {reserve_num=js.getJSONObject("value").getJSONObject("vertical").getJSONObject("presale").get("orderedItemAmount").toString();} catch (Exception e) {}//预定件数
					try {if(js.getJSONObject("value").getJSONObject("item").toString().contains("title")){
						goodsName=js.getJSONObject("value").getJSONObject("item").get("title").toString();//标题	
					}else{
						goodsName=json.getJSONObject("data").getJSONObject("item").get("title").toString();}} catch (Exception e) {}
					try {if(js.getJSONObject("value").getJSONObject("item").toString().contains("commentCount")){	
						commentCount=js.getJSONObject("value").getJSONObject("item").get("commentCount").toString();//评论数	
					}else{
						commentCount=json.getJSONObject("data").getJSONObject("item").get("commentCount").toString();//评论数
					}
					} catch (Exception e) {}
					if(js.getJSONObject("value").getJSONObject("delivery").toString().contains("completedTo")){
						delivery_place=js.getJSONObject("value").getJSONObject("delivery").get("completedTo").toString();//快递发货地址	
					}else{
						try {
							if(js.getJSONObject("value").getJSONObject("delivery").toString().contains("from")){
								delivery_place=js.getJSONObject("value").getJSONObject("delivery").get("from").toString();//快递发货地址
							}else{
								delivery_place=js.getJSONObject("value").getJSONObject("delivery").getString("to");
							}
						} catch (Exception e) {
							logger.error("==>>天猫解析地址报错"+e.getMessage()+"<<==");
						}
					}
					//获取商品促销
					if(js.getJSONObject("value").getJSONObject("price").toString().contains("\"shopProm\":")){
						try{
							JSONArray price=js.getJSONObject("value").getJSONObject("price").getJSONArray("shopProm");
							for(int k=0;k<price.size();k++){//促销 
								String period="";
								JSONObject jss=JSONObject.fromObject(price.toArray()[k]);
								if(jss.toString().contains("content")){
									if(jss.toString().contains("period")){
										period=Fields.ARRIVE+jss.getString("period")+Fields.FINISH;
									}
									if(jss.getString("iconText").equalsIgnoreCase(Fields.INTEGRAL) ){
										prom.append(jss.getString("iconText")+":"+jss.getString("content").replace("\"", "").replace("[", "").replace("]", "").replace("\"", "")+"&&");	
									}else{
										if(jss.containsKey("title") ) {
											prom.append(jss.getString("iconText")+":"+jss.getString("title")+period+">>"+jss.getString("content").replace("\"", "").replace("[", "").replace("]", "").replace("\"", "")+"&&");
										}else {
											prom.append(jss.getString("iconText")+":"+jss.getString("content").replace("\"", "").replace("[", "").replace("]", "").replace("\"", "")+period+"&&");
										}
									}

								}
								if(jss.toString().contains("giftOfContent")){
									JSONArray content=jss.getJSONArray("giftOfContent");
									for(int ks=0;ks<content.size();ks++){
										JSONObject items=JSONObject.fromObject(content.toArray()[ks]);
										JSONArray itemId=items.getJSONArray("items");//促销商品Id
										for(int it=0;it<itemId.size();it++){
											JSONObject jsons=JSONObject.fromObject(itemId.toArray()[it]);
											//赠品获取
											if(StringUtil.isEmpty(present)){
												present=jsons.getString("itemId");
											}else{
												present+=","+jsons.getString("itemId");
											}	
										}
									}
								}
							}
						}catch(Exception e){
							logger.error("==>>商品促销不存在 error:{}<<==",e);	
						}
						if(json.toString().contains(Fields.GOODS_JHS_DESC)){//判断商品是否参加聚划算活动
							prom.append(Fields.GOODS_JHS+"&&"); 
						}
					}
					//获取参加聚划算的商品
					if(prom.toString().contains(Fields.GOODS_JHS) || prom.toString().contains(Fields.GOODS_JHS_DESC)){
						soldCount=juhuasuanSold(parameter);//parameter
					}

					try {
						if(js.toString().contains("hongbao")){
							//System.out.println(js.getJSONObject("value").getJSONObject("resource"));
							if(js.getJSONObject("value").getJSONObject("resource").toString().contains("hongbao")){
								prom.append(js.getJSONObject("value").getJSONObject("resource").getJSONObject("bonus").getJSONObject("hongbao").getString("title")+"&&");
								if(js.toString().contains("shopProm")){
									prom.append(js.getJSONObject("value").getJSONObject("resource").getJSONObject("bonus").getJSONObject("shopProm").getString("title")+"&&");	
								}	
							}else if(js.getJSONObject("value").getJSONObject("resource").toString().contains("shopcoupon")){
								prom.append(js.getJSONObject("value").getJSONObject("resource").getJSONObject("shopcoupon").getString("title")+"&&");
							}
						}
					} catch (Exception e1) {
						logger.error("==>>解析促销失败 <<=="+e1.getMessage());
						e1.printStackTrace();
					}

					if(arry.size()>0 && arry!=null){//进口税
						try {
							JSONObject vertical=JSONObject.fromObject(arry.toArray()[0]);
							if(vertical.toString().contains("tariff")){
								String verticalName=vertical.getJSONObject("value").getJSONObject("vertical").getJSONObject("inter").getJSONObject("tariff").getString("name");
								String verticalValue=vertical.getJSONObject("value").getJSONObject("vertical").getJSONObject("inter").getJSONObject("tariff").getString("value");
								prom.append(verticalName+verticalValue+"&&");
							}	
						} catch (Exception e) {
							logger.error("==>>天猫解析税费报错<<==",e);
						}

					}
					try {
						if(js.getJSONObject("value").getJSONObject("price").toString().contains("extraPrices")){
							if(StringUtil.isNotEmpty(js.getJSONObject("value").getJSONObject("price").getJSONArray("extraPrices").toString())){
								JSONArray extraPrices=js.getJSONObject("value").getJSONObject("price").getJSONArray("extraPrices");
								originalPrice=JSONObject.fromObject(extraPrices.toArray()[0]).get("priceText").toString();//原价	
							}	
						}else {
							originalPrice=json.getJSONObject("data").getJSONObject("mockData").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject("0").getJSONObject("price").get("priceText").toString();//原价
						}
					} catch (Exception e) {
						originalPrice=js.getJSONObject("value").getJSONObject("price").getJSONObject("price").get("priceText").toString();
					}
					//现价
					try {
						if(handsel.equals(Fields.DEPOSIT)){
							currentPrice=js.getJSONObject("value").getJSONObject("price").getJSONObject("subPrice").getString("priceText");//现价	
						}else{
							currentPrice=js.getJSONObject("value").getJSONObject("price").getJSONObject("price").getString("priceText");//现价
						}} catch (Exception e) {}
					quantity= js.getJSONObject("value").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject("0").get("quantity").toString();//库存
					if(js.getJSONObject("value").getJSONObject("skuBase").toString().contains("skus")){//判断商品是否存在skuId
						JSONArray skuBase=js.getJSONObject("value").getJSONObject("skuBase").getJSONArray("skus");
						JSONArray skuName=new JSONArray();
						if(js.getJSONObject("value").getJSONObject("skuBase").toString().contains("props")){
							try {
								props=js.getJSONObject("value").getJSONObject("skuBase").getJSONArray("props");
								skuName=JSONObject.fromObject(props.toArray()[0]).getJSONArray("values");
							} catch (Exception e) {logger.error("skuName为空"+e);}}

						for(int kk=0;kk<skuBase.size();kk++){//SKUID 
							String sku_nameData=""; String sku_parameter="";//sku参数 
							JSONObject skus=JSONObject.fromObject(skuBase.toArray()[kk]);
							skuId=skus.get("skuId").toString();
							originalPrice=json.getJSONObject("data").getJSONObject("mockData").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(skuId).getJSONObject("price").get("priceText").toString();//原价
							if(!Validation.isEmpty(js.getJSONObject("value").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(skuId).getJSONObject("subPrice"))){
								currentPrice=js.getJSONObject("value").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(skuId).getJSONObject("subPrice").get("priceText").toString();//现价
							}else{ 
								currentPrice=js.getJSONObject("value").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(skuId).getJSONObject("price").get("priceText").toString();//现价		
							}
							inventory=js.getJSONObject("value").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(skuId).get("quantity").toString();//库存
							if(currentPrice.contains("-")){
								currentPrice=currentPrice.split("-")[1].toString();
							}
							if(originalPrice.contains("-")){
								originalPrice=originalPrice.split("-")[1].toString();
							}
							if(props.size()>0){
								String propPath=skus.get("propPath").toString();
								if(propPath.contains(";")){
									String[] propPathPid =propPath.split(";");
									for(int k=0;k<propPathPid.length;k++){
										String vid=propPathPid[k].toString().split(":")[1];
										for(int kkk=0;kkk<props.size();kkk++){
											JSONObject propsId=JSONObject.fromObject(props.toArray()[kkk]);
											JSONArray values=propsId.getJSONArray("values");
											for(int s=0;s<values.size();s++){
												JSONObject skuNames=JSONObject.fromObject(values.toArray()[s]);	
												if(skuNames.getString("vid").equals(vid)){
													sku_parameter+=propsId.getString("name")+":"+skuNames.getString("name")+"&&";
													if(k==1){
														sku_nameData=sku_parameter.substring(0, sku_parameter.length()-2);
													}
													break;
												}

											}	
										}	
									}	 
								}else{
									try {
										JSONObject skuNames=JSONObject.fromObject(skuName.toArray()[kk]);
										sku_nameData=skuNames.getString("name");

									} catch (Exception e) {
										logger.error("==>>skuName不存在解析出错了 error:{}<<==",e);
									}
								}
							}
							Craw_goods_Price_Info priceInfo =productInfoPrice(parameter);

							if(StringUtils.isEmpty(inventory)){
								inventory=quantity;
							}
							priceInfo.setInventory(inventory);//库存
							priceInfo.setSKUid(skuId);
							priceInfo.setSku_name(sku_nameData);
					
							priceInfo.setCurrent_price(currentPrice);
							priceInfo.setOriginal_price(originalPrice);
							priceInfo.setPromotion(prom.toString());
							priceInfo.setSale_qty(totalSoldQuantity);//销量
							priceInfo.setChannel(parameter.getListjobName().get(0).getClient());//渠道
							priceInfo.setDeposit(deposit);//定金
							priceInfo.setReserve_num(reserve_num);//预定件数
							priceInfo.setTo_use_amount(coupons);
						
							priceList.add(priceInfo);
							if(parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")){
								for(int k=0;k<2;k++){
									if(k==0){
										priceInfo.setChannel(Fields.CLIENT_MOBILE);	
									}else{
										priceInfo.setChannel(Fields.CLIENT_PC);	
									}
									/**封装数据 价格**/
									insertPriceList.addAll(list(priceInfo));
									//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
									//insertPriceList.add(insertPriceMap);
								}
							}else{
								priceInfo.setChannel(Fields.CLIENT_MOBILE);
								//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
								//insertPriceList.add(insertPriceMap);
								/**封装数据 价格**/
								insertPriceList.addAll(list(priceInfo));
							}

						} 
					}else{
						if(Validation.isEmpty(currentPrice)){
							currentPrice=originalPrice;
						}
						if(currentPrice.contains("-")){
							currentPrice=currentPrice.split("-")[1].toString();
						}
						if(originalPrice.contains("-")){
							originalPrice=originalPrice.split("-")[1].toString();
						}


						Craw_goods_Price_Info priceInfo =productInfoPrice(parameter);
						if(StringUtils.isEmpty(inventory)){
							inventory=quantity;
						}
						priceInfo.setInventory(inventory);//库存
						//priceInfo.setSKUid(parameter.getEgoodsId());
						//priceInfo.setSku_name(sku_nameData);
						priceInfo.setCurrent_price(currentPrice);
						priceInfo.setOriginal_price(originalPrice);
						priceInfo.setPromotion(prom.toString());
						priceInfo.setSale_qty(totalSoldQuantity);//销量
						priceInfo.setChannel(parameter.getListjobName().get(0).getClient());//渠道
						priceInfo.setDeposit(deposit);//定金
						priceInfo.setReserve_num(reserve_num);//预定件数
						priceInfo.setTo_use_amount(coupons);
						priceList.add(priceInfo);
						if(parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")){
							for(int k=0;k<2;k++){
								if(k==0){
									priceInfo.setChannel(Fields.CLIENT_MOBILE);	
								}else{
									priceInfo.setChannel(Fields.CLIENT_PC);	
								}
								//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
								//insertPriceList.add(insertPriceMap);
								/**封装数据 价格**/
								insertPriceList.addAll(list(priceInfo));
							}
						}else{
							priceInfo.setChannel(Fields.CLIENT_MOBILE);
							//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
							//insertPriceList.add(insertPriceMap);
							/**封装数据 价格**/
							insertPriceList.addAll(list(priceInfo));
						}

					}
				}
			}else if(!json.getJSONObject("data").toString().contains("\"skuBase\":[],")){
				props = json.getJSONObject("data").getJSONObject("skuBase").getJSONArray("props");
				//商品名称
				JSONArray sku_id  = json.getJSONObject("data").getJSONObject("skuBase").getJSONArray("skus");
				//商品sku
				JSONArray sku_name=JSONObject.fromObject(props.toArray()[0]).getJSONArray("values");
				for(int hh=0;hh<sku_name.size();hh++){
					JSONObject skuNames = JSONObject.fromObject(sku_name.toArray()[hh]);
					for(int sk=0;sk<sku_id.size();sk++){
						try {
							JSONObject object=JSONObject.fromObject(sku_id.toArray()[sk].toString());
							String price = JSONObject.fromObject(arry.toArray()[0]).getJSONObject("value").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(object.getString("skuId")).getJSONObject("price").getString("priceText");
							Craw_goods_Price_Info priceInfo =productInfoPrice(parameter);
							priceInfo.setInventory(inventory);//库存
							priceInfo.setSKUid(object.getString("skuId"));
							priceInfo.setSku_name(skuNames.getString("name"));
							priceInfo.setCurrent_price(price);
							priceInfo.setOriginal_price(price);
							priceInfo.setPromotion(prom.toString());//促销
							priceInfo.setSale_qty(totalSoldQuantity);//销量
							priceInfo.setChannel(parameter.getListjobName().get(0).getClient());//渠道
							priceInfo.setDeposit(deposit);//定金
							priceInfo.setReserve_num(reserve_num);//预定件数
							priceInfo.setTo_use_amount(coupons);

							priceList.add(priceInfo);
							if(parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")){
								for(int k=0;k<2;k++){
									if(k==0){
										priceInfo.setChannel(Fields.CLIENT_MOBILE);	
									}else{
										priceInfo.setChannel(Fields.CLIENT_PC);	
									}
									//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
									//insertPriceList.add(insertPriceMap);
									/**封装数据 价格**/
									insertPriceList.addAll(list(priceInfo));
								}
							}else{
								priceInfo.setChannel(Fields.CLIENT_MOBILE);
								//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
								//insertPriceList.add(insertPriceMap);
								
								/**封装数据 价格**/
								insertPriceList.addAll(list(priceInfo));
							}
						} catch (Exception e) {
							e.printStackTrace();
							log.error("skuName为空"+e);
						}
					}
				}
				goodsName=json.getJSONObject("data").getJSONObject("item").getString("title");//商品名称
				if(json.getJSONObject("data").getJSONObject("rate").toString().contains("totalCount")){
					commentCount=json.getJSONObject("data").getJSONObject("rate").getString("totalCount");//评论数	
				}
				//totalSoldQuantity=commentCount;//月销量


			}else{
				goodsName=json.getJSONObject("data").getJSONObject("item").getString("title");//商品名称
				if(json.getJSONObject("data").getJSONObject("rate").toString().contains("totalCount")){
					commentCount=json.getJSONObject("data").getJSONObject("rate").getString("totalCount");//评论数	
				}
				//totalSoldQuantity=commentCount;//月销量

				try {
					JSONObject coupon=JSONObject.fromObject(arry.toArray()[0]);
					currentPrice=coupon.getJSONObject("value").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject("0").getJSONObject("price").getString("priceText");
					originalPrice=currentPrice;//原价
					if(currentPrice.contains("-")){
						currentPrice=json.getJSONObject("data").getJSONObject("mockData").getJSONObject("price").getJSONObject("price").getString("priceText");//价格
						originalPrice=currentPrice;//原价
					}else{
						originalPrice=json.getJSONObject("data").getJSONObject("mockData").getJSONObject("price").getJSONObject("price").getString("priceText");
					}
				} catch (Exception e) {
					currentPrice=json.getJSONObject("data").getJSONObject("mockData").getJSONObject("price").getJSONObject("price").getString("priceText");//价格
					originalPrice=currentPrice;//原价

				}


				Craw_goods_Price_Info priceInfo =productInfoPrice(parameter);
				if(StringUtils.isEmpty(inventory)){
					inventory=quantity;
				}
				priceInfo.setInventory(inventory);//库存

				priceInfo.setCurrent_price(currentPrice);
				priceInfo.setOriginal_price(originalPrice);
				priceInfo.setPromotion(prom.toString());
				priceInfo.setSale_qty(totalSoldQuantity);//销量
				priceInfo.setChannel(parameter.getListjobName().get(0).getClient());//渠道
				priceInfo.setDeposit(deposit);//定金
				priceInfo.setReserve_num(reserve_num);//预定件数
				priceInfo.setTo_use_amount(coupons);

				priceList.add(priceInfo);
				if(parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")){
					for(int k=0;k<2;k++){
						if(k==0){
							priceInfo.setChannel(Fields.CLIENT_MOBILE);	
						}else{
							priceInfo.setChannel(Fields.CLIENT_PC);	
						}
						//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
						//insertPriceList.add(insertPriceMap);
						/**封装数据 价格**/
						insertPriceList.addAll(list(priceInfo));
					}
				}else{
					priceInfo.setChannel(Fields.CLIENT_MOBILE);
					//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
					//insertPriceList.add(insertPriceMap);
					
					/**封装数据 价格**/
					insertPriceList.addAll(list(priceInfo));
				}

			}

			try {
				if(StringUtils.isNotEmpty(json.getJSONObject("data").getJSONObject("seller").getString("sellerNick"))){
					sellerName=json.getJSONObject("data").getJSONObject("seller").getString("sellerNick");
				}
			} catch (Exception e) {
				sellerName=shopName;
			}
			try {
				//店铺评分
				if(json.getJSONObject("data").getJSONObject("seller").toString().contains("evaluates")){
					JSONArray arryList=json.getJSONObject("data").getJSONObject("seller").getJSONArray("evaluates");
					StringBuffer storeComment=new StringBuffer();
					for(int i=0;i<arryList.size();i++){
						JSONObject object=JSONObject.fromObject(arryList.toArray()[i]);	
						storeComment.append(object.getString("title")+"("+object.getString("score").trim()+")"+"&&");
						comment.setComment_shopscores(storeComment.toString());		
					}	
				}
			} catch (Exception e) {
				logger.info("解析店铺评分发生异常："+e.getMessage());
				e.printStackTrace();
			}
			try {
				//商品评分
				if(json.getJSONObject("data").getJSONObject("rate").toString().contains("keywords")){
					JSONArray rate=json.getJSONObject("data").getJSONObject("rate").getJSONArray("keywords");
					for(int k=0;k<rate.size();k++){
						JSONObject object=JSONObject.fromObject(rate.toArray()[k]);
						goodsComment.append(object.getString("word")+"("+object.getString("count")+"),"+object.getString("attribute").trim()+"&&");

					}	
				}
			} catch (Exception e) {
				logger.info("解析商品评分发生异常："+e.getMessage());
				e.printStackTrace();
			}
			json.getJSONObject("data").getJSONObject("seller").getString("sellerNick");

			//评论分析数据
			comment.setComment_tags(goodsComment.toString());
			comment.setBatch_time(parameter.getBatch_time());
			comment.setEgoodsId(parameter.getEgoodsId());
			comment.setGoodsId(parameter.getGoodsId());
			comment.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
			comment.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
			comment.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			comment.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			comment.setComment_status(Fields.STATUS_COUNT_1);
			comment.setTtl_comment_num(commentCount);

			//商品详情
			//infovo.setEgoodsId(parameter.getEgoodsId());
			//infovo.setGoodsId(parameter.getGoodsId());
			infovo.setPlatform_shopname(shopName);//卖家店铺名称
			infovo.setSeller_location(location);//卖家位置
			infovo.setPlatform_shopid(shopId);//商品Id
			infovo.setPlatform_shoptype(platform_shoptype);//平台商店类型
			infovo.setPlatform_category(subCatId);//行业ID
			infovo.setPlatform_sellerid(sellerId);//店铺Id
			infovo.setPlatform_sellername(sellerName);//卖家店铺名称
			infovo.setGoods_pic_url(picturl);
			infovo.setPlatform_goods_name(goodsName);//商品名称
			infovo.setDelivery_place(delivery_place);//交易地址
			infovo.setInventory(quantity);//商品库存
			infovo.setSale_qty(totalSoldQuantity);//月销
			infovo.setDelivery_info(postageFree);//是不包邮
			infovo.setTtl_comment_num(commentCount);//商品总评论数
			//infovo.setBatch_time(parameter.getBatch_time());
			//infovo.setGoods_url(parameter.getItemUtl_2());
			//infovo.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
			//infovo.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
			infovo.setJhs_paid_num(soldCount);//抵用金额
			infovo.setGoods_status(1);//状态

			//数据详情插入
			insertItemMap = BeanMapUtil.convertBean2MapWithUnderscoreName(infovo);
			insertItemList.add(insertItemMap);

			String hour=parameter.getMap().get("hour").toString();
			String week=parameter.getMap().get("week").toString();
			//获取商品的规格属性
			String dateWeek=parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[0];
			if(dateWeek.contains(week)) {
				String dataTime=parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[1];
				if(dataTime.contains(hour)) {
					//获取商品属性
					attributeList=commodityProperty(json,parameter);
					//评论分析数据
//					commentItem= BeanMapUtil.convertBean2MapWithUnderscoreName(comment);
//					commentList.add(commentItem);
					
					/**封装数据 属性**/
					commentList.addAll(list(comment));
				}else {
					//清空小图片
					insertImage.clear();
				}
			}
		} 
		
		 //店铺
        if(parameter.getListjobName().get(0).getUser_Id().equals("98")) {
        	Thread.sleep(5000);
        	taobaoDataCrawlService.storeDataCrawlService.productStore(parameter);	
        }
        
		
		//判断是否开启批量抓取数据 1，批量 4 实时抓取
		if(parameter.getListjobName().get(0).getStorage().equals("4")) {
			//插入商品详情 价格
			if(StringUtil.isNotEmpty(goodsName)){
				if(insertPriceList!=null && insertPriceList.size()>0){

					try {
						getProductSave(insertPriceList, database, Fields.TABLE_CRAW_GOODS_PRICE_INFO);
						//search_DataCrawlService.insertIntoData(insertPriceList, database, Fields.TABLE_CRAW_GOODS_PRICE_INFO);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(insertItemList!=null && insertItemList.size()>0){
					try {
						getProductSave(insertItemList,database, Fields.TABLE_CRAW_GOODS_INFO);
						//search_DataCrawlService.insertIntoData(insertItemList,database, Fields.TABLE_CRAW_GOODS_INFO);
					} catch (Exception e) {
						e.printStackTrace();
					}	
				}
			}


			//商品小图片
			if(insertImage!=null && insertImage.size()>0){
				try {
					getProductSave(insertImage,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PIC_INFO);
					//search_DataCrawlService.insertIntoData(insertImage,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PIC_INFO);
				} catch (Exception e) {
					e.printStackTrace();
				}	
			}	

			//插入商品评论描述
			if(commentList!=null && commentList.size()>0){
				try {
					getProductSave(commentList,parameter.getListjobName().get(0).getDatabases(),Fields.CRAW_GOODS_COMMENT_INFO);
					//search_DataCrawlService.insertIntoData(commentList,parameter.getListjobName().get(0).getDatabases(),Fields.CRAW_GOODS_COMMENT_INFO);
				} catch (Exception e) {
					logger.error("==>>获取商品评分失败error:{}<<==",e);
					e.printStackTrace();
				}
			}
		}
		//map.put("present", present);//获取促销赠品详情;
		//判断是否开启批量抓取数据
		if(!parameter.getListjobName().get(0).getStorage().equals("4")) {
			//图片
			map.put("imageList", insertImage);
			//商品评论描述
			map.put("commentList", commentList);
			//商品价格
			map.put("itemPriceList", insertPriceList);
			//商品详情
			map.put("itemList", insertItemList);
			//商品规格参数
			map.put("attributesList", attributeList);
		}
		return map;
	}


	/*数据解析*/
	@SuppressWarnings("unchecked")
	
	public  Map<String, Object>getProductItemAnalysisData2(String StringMessage,Parameter parameter) throws Exception{
		Map<String,Object > map=new HashMap<String,Object >();

		Map<String,Object> insertItem=new HashMap<String,Object>();
		Map<String,Object> commentItem=new HashMap<String,Object>();

		//价格
		List<Map<String, Object>> insertPriceList = Lists.newArrayList();

		//详情
		Map<String, Object> insertItemMap = new HashMap<String, Object>();
		List<Map<String, Object>> insertItemList = Lists.newArrayList();
		//图片
		List<Map<String,Object>> insertImage= Lists.newArrayList();
		List<Map<String,Object>> commentList= Lists.newArrayList();
		//商品规格属性
		List<Map<String,Object>> attributeList=Lists.newArrayList();


		List<Craw_goods_Price_Info>priceList=new ArrayList<Craw_goods_Price_Info>();
		Craw_goods_comment_Info comment=new Craw_goods_comment_Info();
		Craw_goods_InfoVO infovo=productInfo(parameter);
		StringBuffer goodsComment=new StringBuffer();
		String shopName=null,platform_shoptype=null,subCatId=null,sellerId=null,picturl=null,goodsName=null,delivery_place=null;
		String postageFree=Fields.PACK_MAIL;
		String originalPrice=null;
		String currentPrice=null;
		String skuId=null;
		//String promotion=null;
		String shopId=null;  
		String location="";
		String totalSoldQuantity="0";
		String commentCount="";
		String quantity="";
		String priceDesc="";
		String inventory="";//库存
		String	deposit="";//定金
		String  coupons="";//卷
		String reserve_num="";//预定件数
		String present="";//赠品
		String handsel="";
		String sellerName="";//卖家
		String soldCount="";//付款人数
		String sellCount="sellCount";
		JSONArray props=new JSONArray();
		StringBuffer prom = new StringBuffer();
		String database=parameter.getListjobName().get(0).getDatabases();
		if(StringMessage.toString().contains("apiStack")){
			JSONObject json=JSONObject.fromObject(StringMessage.toString());
			JSONArray arry=json.getJSONObject("data").getJSONObject("data").getJSONArray("apiStack");
			JSONObject images=json.getJSONObject("data").getJSONObject("data").getJSONObject("item");
			subCatId=json.getJSONObject("data").getJSONObject("data").getJSONObject("item").get("categoryId").toString();
			shopName=json.getJSONObject("data").getJSONObject("data").getJSONObject("seller").get("shopName").toString();//店铺
			sellerId=json.getJSONObject("data").getJSONObject("data").getJSONObject("seller").get("userId").toString();//用户ID
			shopId=json.getJSONObject("data").getJSONObject("data").getJSONObject("seller").get("shopId").toString();//店铺Id

			if(shopName.contains(Fields.TMART)){
				if(!shopName.contains(Fields.TMART_MAIN_VENUE)){
					platform_shoptype=Fields.SUPERMARKET;
					shopId = Fields.SHIPID;
					shopName=Fields.TMART; 
				}else{ 
					if(shopName.indexOf(Fields.FLAGSHIP_STORE)>0){
						platform_shoptype=Fields.DAY_CAT_FLAGSHIP_STORE;
					}else {
						platform_shoptype=Fields.PROPRIETARY;
					}
				}
			}else{
				if(shopName.indexOf(Fields.FLAGSHIP_STORE)>0){
					platform_shoptype=Fields.DAY_CAT_FLAGSHIP_STORE;
				}else{
					platform_shoptype=Fields.PROPRIETARY;
				}
			}

			try {
				//获取商品小图片
				if(StringUtil.isNotEmpty(images.get("images").toString())){
					JSONArray jsonArray = JSONArray.fromObject(images.get("images").toString());
					for (int i = 0; i < jsonArray.size(); i++) {
						try {
							if(i==0){
								picturl="http:"+jsonArray.getString(i);	
							}
							//判断是否抓取商品小图片
							if(parameter.getListjobName().get(0).getDescribe().contains("抓取商品小图片")) {
								Craw_goods_pic_Info info=commodityImages(parameter,"http:"+jsonArray.getString(i),i);//获取小图片
								insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(info);
								insertImage.add(insertItem);	
							}
						} catch (Exception e) {
							logger.error("==>>解析商品小图片失败 ，error:{}<<==",e);
						}

					}	
				}
			} catch (Exception e) {
				logger.error("==>>天猫获取商品小图片失败 error:{}<<==",e);
			}
			if(StringUtil.isEmpty(picturl)){
				Set<Entry<String, Object>> entrySet = images.entrySet();
				for(Entry<String, Object> entry : entrySet) {//商品图片
					if(entry.getKey().equals("images")) { 
						JSONArray jr2 = (JSONArray)entry.getValue();
						picturl=jr2.getString(0);
						break;
					}
				}
			}
			//商品开团时间
			if(arry.toString().contains("channel")){
				try {
					JSONObject coupon=JSONObject.fromObject(arry.toArray()[0]);
					if(coupon.getJSONObject("value").toString().contains("channel")){
						if(coupon.getJSONObject("value").getJSONObject("consumerProtection").getJSONObject("channel").toString().contains("title")){
							String channelTitle=coupon.getJSONObject("value").getJSONObject("consumerProtection").getJSONObject("channel").getString("title");
							prom.append(channelTitle+"&&"); 	
						}
						if(coupon.getJSONObject("value").toString().contains("hintBanner")){
							prom.append(coupon.getJSONObject("value").getJSONObject("trade").getJSONObject("hintBanner").getString("text")+"&&");
						}
					}	
				} catch (Exception e) {
					logger.error("==>>获取商品开团时间，发生异常 error:{}<<==",e);
				}

			}
			if(arry.toString().contains("tmallCoupon")){//判断优惠卷是否存在
				JSONObject coupon=JSONObject.fromObject(arry.toArray()[0]);
				if(coupon.getJSONObject("value").toString().contains("coupon")){
					String couponData=coupon.getJSONObject("value").getJSONObject("resource").getJSONObject("entrances").getJSONObject("coupon").getString("text");
					if(Validation.isNotEmpty(couponData)){
						prom.append(couponData+"&&"); 
					}

				}
			}

			if(arry.toString().contains("couponList")){//判断优惠卷是否存在
				JSONObject coupon=JSONObject.fromObject(arry.toArray()[0]);
				if(coupon.getJSONObject("value").toString().contains("coupon")){
					JSONArray couponData=coupon.getJSONObject("value").getJSONObject("resource").getJSONObject("coupon").getJSONArray("couponList");
					if(couponData.size()>0){
						for(int k=0;k<couponData.size();k++){
							JSONObject object=JSONObject.fromObject(couponData.toArray()[k].toString());
							prom.append(object.getString("title")+"&&");	
						}

					}
				}
				//商品开卖时间
				if(coupon.getJSONObject("value").toString().contains("bigPromotion")){
					if(coupon.getJSONObject("value").getJSONObject("resource").getJSONObject("bigPromotion").toString().contains("memo")){
						try {
							JSONArray array=coupon.getJSONObject("value").getJSONObject("resource").getJSONObject("bigPromotion").getJSONArray("memo");
							if(array!=null && array.size()>0){
								JSONObject object=JSONObject.fromObject(array.toArray()[0].toString());
								prom.append(object.getString("text"));
							}

						} catch (Exception e) {
							logger.error("==>>获取商品开卖时间，发生异常 error:{}<<==",e);
						}	
					}
				}
			}
			if(parameter.getListjobName().get(0).getPlatform().contentEquals("taobao")){
				//快递发货地址
				if(arry.toString().contains("from") || arry.toString().contains("completedTo")){
					JSONObject coupon=JSONObject.fromObject(arry.toArray()[0]);
					if(coupon.getJSONObject("value").getJSONObject("delivery").toString().contains("completedTo")){
						delivery_place=coupon.getJSONObject("value").getJSONObject("delivery").get("completedTo").toString();//快递发货地址	

					}else{
						try {
							if(coupon.getJSONObject("value").getJSONObject("delivery").toString().contains("from")){
								delivery_place=coupon.getJSONObject("value").getJSONObject("delivery").get("from").toString();//快递发货地址
								location=coupon.getJSONObject("value").getJSONObject("delivery").getString("to");//卖家地址
							}
						} catch (Exception e) {
							log.error("==>>天猫解析地址报错"+e.getMessage()+"<<==");
						}
					}

					if(coupon.getJSONObject("value").containsKey("resource")) {
						if(coupon.getJSONObject("value").getJSONObject("resource").containsKey("shopProm")) {
							JSONArray shopProm=coupon.getJSONObject("value").getJSONObject("resource").getJSONArray("shopProm");
							if(shopProm!=null && shopProm.size()>0) {
								if(shopProm.toString().contains("[\"title\"]")) {
									prom.append(JSONObject.fromObject(shopProm.toArray()[0].toString()).getString("title"));
								}else if(shopProm.toString().contains("iconText")) {
									String iconText=JSONObject.fromObject(shopProm.toArray()[0].toString()).getString("iconText");
									String content=JSONObject.fromObject(shopProm.toArray()[0].toString()).getString("content");
									prom.append(iconText+":"+StringUtils.replaceEach(content, new String[] {"[","]","\""},new String[] {"","",""}));		
								}

							}
						}

					}

				}
			}
			if(arry.toString().contains(sellCount)){

				for(int i=0;i<arry.size();i++){
					JSONObject js=JSONObject.fromObject(arry.toArray()[i]);
					try {
						deposit=js.getJSONObject("value").getJSONObject("price").getJSONObject("price").getString("priceText");//定金
						if(js.getJSONObject("value").getJSONObject("price").toString().contains("depositPrice")){
							coupons=js.getJSONObject("value").getJSONObject("price").getJSONObject("depositPrice").get("priceDesc").toString();//定金卷	
							if(coupons.contains(Fields.REDUCTION_OF)) {
								coupons=StringHelper.getResultByReg(coupons,"([0-9]+)");
							}else {
								coupons=""; 
							}
						}
						handsel=js.getJSONObject("value").getJSONObject("price").getJSONObject("price").getString("priceTitle");//文字定金
						if(coupons.contains("，")){
							deposit=coupons.split("，")[0].toString();//定金
							deposit=StringHelper.getResultByReg(deposit,"([0-9]+)");
							if(coupons.length()>1){
								coupons=coupons.split("，")[1].toString();//优惠卷
								coupons=StringHelper.getResultByReg(coupons,"([0-9]+)");
							}
						}else{
							if(StringUtils.isNotEmpty(StringHelper.getResultByReg(priceDesc,"([0-9]+)"))){
								coupons=StringHelper.getResultByReg(priceDesc,"([0-9]+)");//抵用金额	
							}	
						}
						if(StringUtils.isNotBlank(coupons)) {
							prom.append(Fields.PRESELL_MESSAGE+"&&");	
						}

					} catch (Exception e) {
						deposit="";coupons="";
					}
					try {
						totalSoldQuantity=js.getJSONObject("value").getJSONObject("item").get("sellCount").toString();//月销
					} catch (Exception e) {
						totalSoldQuantity="0";
					} 
					try {location=js.getJSONObject("value").getJSONObject("skuCore").getJSONObject("skuItem").get("location").toString();} catch (Exception e) {}
					try {reserve_num=js.getJSONObject("value").getJSONObject("vertical").getJSONObject("presale").get("orderedItemAmount").toString();} catch (Exception e) {}//预定件数
					try {if(js.getJSONObject("value").getJSONObject("item").toString().contains("title")){
						goodsName=js.getJSONObject("value").getJSONObject("item").get("title").toString();//标题	
					}else{
						goodsName=json.getJSONObject("data").getJSONObject("data").getJSONObject("item").get("title").toString();}} catch (Exception e) {}
					try {if(js.getJSONObject("value").getJSONObject("item").toString().contains("commentCount")){	
						commentCount=js.getJSONObject("value").getJSONObject("item").get("commentCount").toString();//评论数	
					}else{
						commentCount=json.getJSONObject("data").getJSONObject("data").getJSONObject("item").get("commentCount").toString();//评论数
					}
					} catch (Exception e) {}
					if(js.getJSONObject("value").getJSONObject("delivery").toString().contains("completedTo")){
						delivery_place=js.getJSONObject("value").getJSONObject("delivery").get("completedTo").toString();//快递发货地址	
					}else{
						try {
							if(js.getJSONObject("value").getJSONObject("delivery").toString().contains("from")){
								delivery_place=js.getJSONObject("value").getJSONObject("delivery").get("from").toString();//快递发货地址
							}else{
								delivery_place=js.getJSONObject("value").getJSONObject("delivery").getString("to");
							}
						} catch (Exception e) {
							logger.error("==>>天猫解析地址报错"+e.getMessage()+"<<==");
						}
					}
					//获取商品促销
					if(js.getJSONObject("value").getJSONObject("price").toString().contains("\"shopProm\":")){
						try{
							JSONArray price=js.getJSONObject("value").getJSONObject("price").getJSONArray("shopProm");
							for(int k=0;k<price.size();k++){//促销 
								String period="";
								JSONObject jss=JSONObject.fromObject(price.toArray()[k]);
								if(jss.toString().contains("content")){
									if(jss.toString().contains("period")){
										period=Fields.ARRIVE+jss.getString("period")+Fields.FINISH;
									}
									if(jss.getString("iconText").equalsIgnoreCase(Fields.INTEGRAL) ){
										prom.append(jss.getString("iconText")+":"+jss.getString("content").replace("\"", "").replace("[", "").replace("]", "").replace("\"", "")+"&&");	
									}else{
										if(jss.containsKey("title") ) {
											prom.append(jss.getString("iconText")+":"+jss.getString("title")+period+">>"+jss.getString("content").replace("\"", "").replace("[", "").replace("]", "").replace("\"", "")+"&&");
										}else {
											prom.append(jss.getString("iconText")+":"+jss.getString("content").replace("\"", "").replace("[", "").replace("]", "").replace("\"", "")+period+"&&");
										}
									}

								}
								if(jss.toString().contains("giftOfContent")){
									JSONArray content=jss.getJSONArray("giftOfContent");
									for(int ks=0;ks<content.size();ks++){
										JSONObject items=JSONObject.fromObject(content.toArray()[ks]);
										JSONArray itemId=items.getJSONArray("items");//促销商品Id
										for(int it=0;it<itemId.size();it++){
											JSONObject jsons=JSONObject.fromObject(itemId.toArray()[it]);
											//赠品获取
											if(StringUtil.isEmpty(present)){
												present=jsons.getString("itemId");
											}else{
												present+=","+jsons.getString("itemId");
											}	
										}
									}
								}
							}
						}catch(Exception e){
							logger.error("==>>商品促销不存在 error:{}<<==",e);	
						}
						if(json.toString().contains(Fields.GOODS_JHS_DESC)){//判断商品是否参加聚划算活动
							prom.append(Fields.GOODS_JHS+"&&"); 
						}
					}
					//获取参加聚划算的商品
					if(prom.toString().contains(Fields.GOODS_JHS) || prom.toString().contains(Fields.GOODS_JHS_DESC)){
						soldCount=juhuasuanSold(parameter);//parameter
					}

					try {
						if(js.toString().contains("hongbao")){
							//System.out.println(js.getJSONObject("value").getJSONObject("resource"));
							if(js.getJSONObject("value").getJSONObject("resource").toString().contains("hongbao")){
								prom.append(js.getJSONObject("value").getJSONObject("resource").getJSONObject("bonus").getJSONObject("hongbao").getString("title")+"&&");
								if(js.toString().contains("shopProm")){
									prom.append(js.getJSONObject("value").getJSONObject("resource").getJSONObject("bonus").getJSONObject("shopProm").getString("title")+"&&");	
								}	
							}else if(js.getJSONObject("value").getJSONObject("resource").toString().contains("shopcoupon")){
								prom.append(js.getJSONObject("value").getJSONObject("resource").getJSONObject("shopcoupon").getString("title")+"&&");
							}
						}
					} catch (Exception e1) {
						logger.error("==>>解析促销失败 <<=="+e1.getMessage());
						e1.printStackTrace();
					}

					if(arry.size()>0 && arry!=null){//进口税
						try {
							JSONObject vertical=JSONObject.fromObject(arry.toArray()[0]);
							if(vertical.toString().contains("tariff")){
								String verticalName=vertical.getJSONObject("value").getJSONObject("vertical").getJSONObject("inter").getJSONObject("tariff").getString("name");
								String verticalValue=vertical.getJSONObject("value").getJSONObject("vertical").getJSONObject("inter").getJSONObject("tariff").getString("value");
								prom.append(verticalName+verticalValue+"&&");
							}	
						} catch (Exception e) {
							logger.error("==>>天猫解析税费报错<<==",e);
						}

					}
					try {
						if(js.getJSONObject("value").getJSONObject("price").toString().contains("extraPrices")){
							if(StringUtil.isNotEmpty(js.getJSONObject("value").getJSONObject("price").getJSONArray("extraPrices").toString())){
								JSONArray extraPrices=js.getJSONObject("value").getJSONObject("price").getJSONArray("extraPrices");
								originalPrice=JSONObject.fromObject(extraPrices.toArray()[0]).get("priceText").toString();//原价	
							}	
						}else {
							originalPrice=json.getJSONObject("data").getJSONObject("data").getJSONObject("mockData").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject("0").getJSONObject("price").get("priceText").toString();//原价
						}
					} catch (Exception e) {
						originalPrice=js.getJSONObject("value").getJSONObject("price").getJSONObject("price").get("priceText").toString();
					}
					//现价
					try {
						if(handsel.equals(Fields.DEPOSIT)){
							currentPrice=js.getJSONObject("value").getJSONObject("price").getJSONObject("subPrice").getString("priceText");//现价	
						}else{
							currentPrice=js.getJSONObject("value").getJSONObject("price").getJSONObject("price").getString("priceText");//现价
						}} catch (Exception e) {}
					quantity= js.getJSONObject("value").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject("0").get("quantity").toString();//库存
					if(js.getJSONObject("value").getJSONObject("skuBase").toString().contains("skus")){//判断商品是否存在skuId
						JSONArray skuBase=js.getJSONObject("value").getJSONObject("skuBase").getJSONArray("skus");
						JSONArray skuName=new JSONArray();
						if(js.getJSONObject("value").getJSONObject("skuBase").toString().contains("props")){
							try {
								props=js.getJSONObject("value").getJSONObject("skuBase").getJSONArray("props");
								skuName=JSONObject.fromObject(props.toArray()[0]).getJSONArray("values");
							} catch (Exception e) {logger.error("skuName为空"+e);}}

						for(int kk=0;kk<skuBase.size();kk++){//SKUID 
							String sku_nameData=""; String sku_parameter="";//sku参数 
							JSONObject skus=JSONObject.fromObject(skuBase.toArray()[kk]);
							skuId=skus.get("skuId").toString();
							originalPrice=json.getJSONObject("data").getJSONObject("data").getJSONObject("mockData").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(skuId).getJSONObject("price").get("priceText").toString();//原价
							if(!Validation.isEmpty(js.getJSONObject("value").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(skuId).getJSONObject("subPrice"))){
								currentPrice=js.getJSONObject("value").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(skuId).getJSONObject("subPrice").get("priceText").toString();//现价
							}else{ 
								currentPrice=js.getJSONObject("value").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(skuId).getJSONObject("price").get("priceText").toString();//现价		
							}
							inventory=js.getJSONObject("value").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(skuId).get("quantity").toString();//库存
							if(currentPrice.contains("-")){
								currentPrice=currentPrice.split("-")[1].toString();
							}
							if(originalPrice.contains("-")){
								originalPrice=originalPrice.split("-")[1].toString();
							}
							if(props.size()>0){
								String propPath=skus.get("propPath").toString();
								if(propPath.contains(";")){
									String propPathPid[]=propPath.split(";");
									for(int k=0;k<propPathPid.length;k++){
										String vid=propPathPid[k].toString().split(":")[1];
										for(int kkk=0;kkk<props.size();kkk++){
											JSONObject propsId=JSONObject.fromObject(props.toArray()[kkk]);
											JSONArray values=propsId.getJSONArray("values");
											for(int s=0;s<values.size();s++){
												JSONObject skuNames=JSONObject.fromObject(values.toArray()[s]);	
												if(skuNames.getString("vid").equals(vid)){
													sku_parameter+=propsId.getString("name")+":"+skuNames.getString("name")+"&&";
													if(k==1){
														sku_nameData=sku_parameter.substring(0, sku_parameter.length()-2);
													}
													break;
												}

											}	
										}	
									}	 
								}else{
									try {
										JSONObject skuNames=JSONObject.fromObject(skuName.toArray()[kk]);
										sku_nameData=skuNames.getString("name");

									} catch (Exception e) {
										logger.error("==>>skuName不存在解析出错了 error:{}<<==",e);
									}
								}
							}
							Craw_goods_Price_Info priceInfo = new Craw_goods_Price_Info();

							if(StringUtils.isEmpty(inventory)){
								inventory=quantity;
							}
							priceInfo.setInventory(inventory);//库存
							priceInfo.setSKUid(skuId);
							priceInfo.setSku_name(sku_nameData);
							priceInfo.setBatch_time(parameter.getBatch_time());
							priceInfo.setGoodsid(parameter.getGoodsId());
							priceInfo.setCust_keyword_id(parameter.getKeywordId());

							priceInfo.setCurrent_price(currentPrice);
							priceInfo.setOriginal_price(originalPrice);
							priceInfo.setPromotion(prom.toString());
							priceInfo.setSale_qty(totalSoldQuantity);//销量
							priceInfo.setChannel(parameter.getListjobName().get(0).getClient());//渠道
							priceInfo.setDeposit(deposit);//定金
							priceInfo.setReserve_num(reserve_num);//预定件数
							priceInfo.setTo_use_amount(coupons);
							priceInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
							priceInfo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
							priceInfo.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
							priceList.add(priceInfo);
							if(parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")){
								for(int k=0;k<2;k++){
									if(k==0){
										priceInfo.setChannel(Fields.CLIENT_MOBILE);	
									}else{
										priceInfo.setChannel(Fields.CLIENT_PC);	
									}
									//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
									//insertPriceList.add(insertPriceMap);

									/**封装数据 价格**/
									insertPriceList.addAll(list(priceInfo));

								}
							}else{
								priceInfo.setChannel(Fields.CLIENT_MOBILE);
								//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
								//insertPriceList.add(insertPriceMap);
								/**封装数据 价格**/
								insertPriceList.addAll(list(priceInfo));
							}

						} 
					}else{
						if(Validation.isEmpty(currentPrice)){
							currentPrice=originalPrice;
						}
						if(currentPrice.contains("-")){
							currentPrice=currentPrice.split("-")[1].toString();
						}
						if(originalPrice.contains("-")){
							originalPrice=originalPrice.split("-")[1].toString();
						}


						Craw_goods_Price_Info priceInfo =productInfoPrice(parameter);
						if(StringUtils.isEmpty(inventory)){
							inventory=quantity;
						}
						priceInfo.setInventory(inventory);//库存
						//priceInfo.setSKUid(parameter.getEgoodsId());
						//priceInfo.setSku_name(sku_nameData);
						priceInfo.setCurrent_price(currentPrice);
						priceInfo.setOriginal_price(originalPrice);
						priceInfo.setPromotion(prom.toString());
						priceInfo.setSale_qty(totalSoldQuantity);//销量
						priceInfo.setChannel(parameter.getListjobName().get(0).getClient());//渠道
						priceInfo.setDeposit(deposit);//定金
						priceInfo.setReserve_num(reserve_num);//预定件数
						priceInfo.setTo_use_amount(coupons);
					
						priceList.add(priceInfo);
						if(parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")){
							for(int k=0;k<2;k++){
								if(k==0){
									priceInfo.setChannel(Fields.CLIENT_MOBILE);	
								}else{
									priceInfo.setChannel(Fields.CLIENT_PC);	
								}
								//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
								//insertPriceList.add(insertPriceMap);
								/**封装数据 价格**/
								insertPriceList.addAll(list(priceInfo));
							}
						}else{
							priceInfo.setChannel(Fields.CLIENT_MOBILE);
							//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
							//insertPriceList.add(insertPriceMap);
							/**封装数据 价格**/
							insertPriceList.addAll(list(priceInfo));
						}

					}
				}
			}else if(json.getJSONObject("data").getJSONObject("skuBase").toString().contains("props")){

				props = json.getJSONObject("data").getJSONObject("data").getJSONObject("skuBase").getJSONArray("props");
				//商品名称
				JSONArray sku_id  = json.getJSONObject("data").getJSONObject("data").getJSONObject("skuBase").getJSONArray("skus");
				//商品sku
				JSONArray sku_name=JSONObject.fromObject(props.toArray()[0]).getJSONArray("values");
				for(int skk=0;skk<sku_name.size(); skk++) {
					JSONObject skuNames = JSONObject.fromObject(sku_name.toArray()[skk]);
					for(int sk=0;sk<sku_id.size();sk++){
						try {
							JSONObject object=JSONObject.fromObject(sku_id.toArray()[sk].toString());

							String price = JSONObject.fromObject(arry.toArray()[0]).getJSONObject("value").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(object.getString("skuId")).getJSONObject("price").getString("priceText");
							Craw_goods_Price_Info priceInfo =productInfoPrice(parameter);
							priceInfo.setInventory(inventory);//库存
							priceInfo.setSKUid(object.getString("skuId"));
							priceInfo.setSku_name(skuNames.getString("name"));
							
							priceInfo.setCurrent_price(price);
							priceInfo.setOriginal_price(price);
							priceInfo.setPromotion(prom.toString());//促销
							priceInfo.setSale_qty(totalSoldQuantity);//销量
							priceInfo.setChannel(parameter.getListjobName().get(0).getClient());//渠道
							priceInfo.setDeposit(deposit);//定金
							priceInfo.setReserve_num(reserve_num);//预定件数
							priceInfo.setTo_use_amount(coupons);
					
							priceList.add(priceInfo);
							if(parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")){
								for(int k=0;k<2;k++){
									if(k==0){
										priceInfo.setChannel(Fields.CLIENT_MOBILE);	
									}else{
										priceInfo.setChannel(Fields.CLIENT_PC);	
									}
									//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
									//insertPriceList.add(insertPriceMap);
									/**封装数据 价格**/
									insertPriceList.addAll(list(priceInfo));
								}
							}else{
								priceInfo.setChannel(Fields.CLIENT_MOBILE);
								//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
								//insertPriceList.add(insertPriceMap);
								/**封装数据 价格**/
								insertPriceList.addAll(list(priceInfo));
							}
						} catch (Exception e) {
							e.printStackTrace();
							log.error("skuName为空"+e);
						}
					}
				}
				goodsName=json.getJSONObject("data").getJSONObject("data").getJSONObject("item").getString("title");//商品名称
				if(json.getJSONObject("data").getJSONObject("data").getJSONObject("rate").toString().contains("totalCount")){
					commentCount=json.getJSONObject("data").getJSONObject("data").getJSONObject("rate").getString("totalCount");//评论数	
				}
				//totalSoldQuantity=commentCount;//月销量


			}else{
				goodsName=json.getJSONObject("data").getJSONObject("data").getJSONObject("item").getString("title");//商品名称
				if(json.getJSONObject("data").getJSONObject("data").getJSONObject("rate").toString().contains("totalCount")){
					commentCount=json.getJSONObject("data").getJSONObject("data").getJSONObject("rate").getString("totalCount");//评论数	
				}
				//totalSoldQuantity=commentCount;//月销量

				try {
					JSONObject coupon=JSONObject.fromObject(arry.toArray()[0]);
					currentPrice=coupon.getJSONObject("value").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject("0").getJSONObject("price").getString("priceText");
					originalPrice=currentPrice;//原价
					if(currentPrice.contains("-")){
						currentPrice=json.getJSONObject("data").getJSONObject("data").getJSONObject("mockData").getJSONObject("price").getJSONObject("price").getString("priceText");//价格
						originalPrice=currentPrice;//原价
					}else{
						originalPrice=json.getJSONObject("data").getJSONObject("data").getJSONObject("mockData").getJSONObject("price").getJSONObject("price").getString("priceText");
					}
				} catch (Exception e) {
					currentPrice=json.getJSONObject("data").getJSONObject("data").getJSONObject("mockData").getJSONObject("price").getJSONObject("price").getString("priceText");//价格
					originalPrice=currentPrice;//原价

				}


				Craw_goods_Price_Info priceInfo =productInfoPrice(parameter);
				if(StringUtils.isEmpty(inventory)){
					inventory=quantity;
				}
				priceInfo.setInventory(inventory);//库存
				//priceInfo.setSku_name(sku_name);
				//priceInfo.setSKUid(parameter.getEgoodsId());
				priceInfo.setCurrent_price(currentPrice);
				priceInfo.setOriginal_price(originalPrice);
				priceInfo.setPromotion(prom.toString());
				priceInfo.setSale_qty(totalSoldQuantity);//销量	
				priceInfo.setChannel(parameter.getListjobName().get(0).getClient());//渠道
				priceInfo.setDeposit(deposit);//定金
				priceInfo.setReserve_num(reserve_num);//预定件数
				priceInfo.setTo_use_amount(coupons);
				priceList.add(priceInfo);
				if(parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")){
					for(int k=0;k<2;k++){
						if(k==0){
							priceInfo.setChannel(Fields.CLIENT_MOBILE);	
						}else{
							priceInfo.setChannel(Fields.CLIENT_PC);	
						}
						//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
						//insertPriceList.add(insertPriceMap);
						/**封装数据 价格**/
						insertPriceList.addAll(list(priceInfo));
					}
				}else{
					priceInfo.setChannel(Fields.CLIENT_MOBILE);
					//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
					//insertPriceList.add(insertPriceMap);
					/**封装数据 价格**/
					insertPriceList.addAll(list(priceInfo));
				}

			}

			try {
				if(StringUtils.isNotEmpty(json.getJSONObject("data").getJSONObject("data").getJSONObject("seller").getString("sellerNick"))){
					sellerName=json.getJSONObject("data").getJSONObject("data").getJSONObject("seller").getString("sellerNick");
				}
			} catch (Exception e) {
				sellerName=shopName;
			}
			try {
				//店铺评分
				if(json.getJSONObject("data").getJSONObject("data").getJSONObject("seller").toString().contains("evaluates")){
					JSONArray arryList=json.getJSONObject("data").getJSONObject("data").getJSONObject("seller").getJSONArray("evaluates");
					StringBuffer storeComment=new StringBuffer();
					for(int i=0;i<arryList.size();i++){
						JSONObject object=JSONObject.fromObject(arryList.toArray()[i]);	
						storeComment.append(object.getString("title")+"("+object.getString("score").trim()+")"+"&&");
						comment.setComment_shopscores(storeComment.toString());		
					}	
				}
			} catch (Exception e) {
				logger.info("解析店铺评分发生异常："+e.getMessage());
				e.printStackTrace();
			}
			try {
				//商品评分
				if(json.getJSONObject("data").getJSONObject("data").getJSONObject("rate").toString().contains("keywords")){
					JSONArray rate=json.getJSONObject("data").getJSONObject("data").getJSONObject("rate").getJSONArray("keywords");
					for(int k=0;k<rate.size();k++){
						JSONObject object=JSONObject.fromObject(rate.toArray()[k]);
						goodsComment.append(object.getString("word")+"("+object.getString("count")+"),"+object.getString("attribute").trim()+"&&");

					}	
				}
			} catch (Exception e) {
				logger.info("解析商品评分发生异常："+e.getMessage());
				e.printStackTrace();
			}
			json.getJSONObject("data").getJSONObject("data").getJSONObject("seller").getString("sellerNick");

			//评论分析数据
			comment.setComment_tags(goodsComment.toString());
			comment.setBatch_time(parameter.getBatch_time());
			comment.setEgoodsId(parameter.getEgoodsId());
			comment.setGoodsId(parameter.getGoodsId());
			comment.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
			comment.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
			comment.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			comment.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			comment.setComment_status(Fields.STATUS_COUNT_1);
			comment.setTtl_comment_num(commentCount);

			//商品详情
			infovo.setEgoodsId(parameter.getEgoodsId());
			infovo.setGoodsId(parameter.getGoodsId());
			infovo.setPlatform_shopname(shopName);//卖家店铺名称
			infovo.setSeller_location(location);//卖家位置
			infovo.setPlatform_shopid(shopId);//商品Id
			infovo.setPlatform_shoptype(platform_shoptype);//平台商店类型
			infovo.setPlatform_category(subCatId);//行业ID
			infovo.setPlatform_sellerid(sellerId);//店铺Id
			infovo.setPlatform_sellername(sellerName);//卖家店铺名称
			infovo.setGoods_pic_url(picturl);
			infovo.setPlatform_goods_name(goodsName);//商品名称
			infovo.setDelivery_place(delivery_place);//交易地址
			infovo.setInventory(quantity);//商品库存
			infovo.setSale_qty(totalSoldQuantity);//月销
			infovo.setDelivery_info(postageFree);//是不包邮
			infovo.setTtl_comment_num(commentCount);//商品总评论数
			infovo.setBatch_time(parameter.getBatch_time());
			infovo.setGoods_url(parameter.getItemUtl_2());
			infovo.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
			infovo.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
			infovo.setJhs_paid_num(soldCount);//抵用金额
			infovo.setGoods_status(1);//状态
			infovo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			infovo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));

			//数据详情插入
			insertItemMap = BeanMapUtil.convertBean2MapWithUnderscoreName(infovo);
			insertItemList.add(insertItemMap);

			String hour=parameter.getMap().get("hour").toString();
			String week=parameter.getMap().get("week").toString();
			//获取商品的规格属性
			String dateWeek=parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[0];
			if(dateWeek.contains(week)) {
				String dataTime=parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[1];
				if(dataTime.contains(hour)) {
					//获取商品属性
					attributeList=commodityProperty(json,parameter);
					//评论分析数据
					commentItem= BeanMapUtil.convertBean2MapWithUnderscoreName(comment);
					commentList.add(commentItem);	
				}else {
					//清空小图片
					insertImage.clear();
				}
			}
		} 
		//判断是否开启批量抓取数据 1，批量 4 实时抓取
		if(parameter.getListjobName().get(0).getStorage().equals("4")) {
			//插入商品详情 价格
			if(StringUtil.isNotEmpty(goodsName)){
				if(insertPriceList!=null && insertPriceList.size()>0){

					try {
						getProductSave(insertPriceList, database, Fields.TABLE_CRAW_GOODS_PRICE_INFO);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(insertItemList!=null && insertItemList.size()>0){
					try {
						getProductSave(insertItemList,database, Fields.TABLE_CRAW_GOODS_INFO);
					} catch (Exception e) {
						e.printStackTrace();
					}	
				}
			}


			//商品小图片
			if(insertImage!=null && insertImage.size()>0){
				try {
					getProductSave(insertImage,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PIC_INFO);
				} catch (Exception e) {
					e.printStackTrace();
				}	
			}	

			//插入商品评论描述
			if(commentList!=null && commentList.size()>0){
				try {
					getProductSave(commentList,parameter.getListjobName().get(0).getDatabases(),Fields.CRAW_GOODS_COMMENT_INFO);
				} catch (Exception e) {
					logger.error("==>>获取商品评分失败error:{}<<==",e);
					e.printStackTrace();
				}
			}
		}
		//map.put("present", present);//获取促销赠品详情;
		//判断是否开启批量抓取数据
		if(!parameter.getListjobName().get(0).getStorage().equals("4")) {
			//图片
			map.put("imageList", insertImage);
			//商品评论描述
			map.put("commentList", commentList);
			//商品价格
			map.put("itemPriceList", insertPriceList);
			//商品详情
			map.put("itemList", insertItemList);
			//商品规格参数
			map.put("attributesList", attributeList);
		}
		return map;
	}

	/*获取聚划算付款人数*/
	public String juhuasuanSold(Parameter parameter){
		String soldCount="";
		parameter.setItemUrl_1(Fields.JUHUASUAN_IMAGE_URL.replace(Fields.EDOODSID, parameter.getEgoodsId()));
		String  StringMessage=taobaoDataCrawlService.httpClientService.interfaceSwitch(parameter);//请求数据		
		if(StringMessage.contains("soldCount")){
			try {
				JSONObject json=JSONObject.fromObject(StringMessage);

				soldCount=json.getJSONObject("data").getJSONObject("data").getString("soldCount");
			} catch (Exception e) {
				logger.error("==>>获取聚划算付款人数请求接口发送异常 ：{}<<=",e);
			}

		}
		return soldCount;

	}
	/*商品评分*/
	public Craw_goods_pic_Info commodityImages(Parameter parameter, String imageurl, int i) {
		Craw_goods_pic_Info info = new Craw_goods_pic_Info();
		try {
			info.setBatch_time(parameter.getBatch_time());
			info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			info.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
			info.setGoodsid(parameter.getGoodsId());
			info.setPic_size(String.valueOf(i + 1));//第几张图片
			info.setPic_order(i + 1);
			info.setPic_url(imageurl);
			info.setPlatform_name(parameter.getListjobName().get(0).getPlatform());
			info.setEgoodsid(parameter.getEgoodsId());
			info.setPic_type("littlepic");
		} catch (Exception e) {
			log.error("==>>解析商品评分发生异常："+e.getMessage()+"<<==");
			e.printStackTrace();
		}
		return info;
	}


	/*获取商品属性值*/
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> commodityProperty (JSONObject json,Parameter parameter){
		Map<String,Object> insertItem=new HashMap<String,Object>();
		List<Map<String,Object>> insertList= Lists.newArrayList();
		try {
			String database=parameter.getListjobName().get(0).getDatabases();
			if(json.getJSONObject("data").getJSONObject("data").getJSONObject("props").toString().contains("groupProps")){

				JSONArray groupPropsList=json.getJSONObject("data").getJSONObject("data").getJSONObject("props").getJSONArray("groupProps");
				for(int i=0;i<groupPropsList.size();i++){
					JSONObject jsonObject = JSONObject.fromObject(groupPropsList.toArray()[i]);
					if(jsonObject.toString().contains(Fields.INFORMATION)){
						JSONArray	jsondata=jsonObject.getJSONArray(Fields.INFORMATION);
						for(int k=0;k<jsondata.size();k++){
							JSONObject object = JSONObject.fromObject(jsondata.toArray()[k]);
							Set<Entry<String, Object>> setdata = object.entrySet();
							for(Entry<String, Object> entry : setdata) {//商品属性
								Craw_goods_attribute_Info info =new Craw_goods_attribute_Info();
								info.setAttri_value(entry.getValue().toString());//值
								info.setCraw_attri_cn(entry.getKey().toString());//key
								info.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
								info.setEgoodsid(parameter.getEgoodsId());
								info.setGoodsid(parameter.getGoodsId());
								info.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());
								info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
								info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
								info.setBatch_time(parameter.getBatch_time());
								insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(info);
								insertList.add(insertItem); 	
							} 
						}
					}
				}
			}
			//判断商品1 批量抓取 4 实时抓取
			if(parameter.getListjobName().get(0).getStorage().equals("4")) {
				if(insertList!=null && insertList.size()>0){
					try {
						taobaoDataCrawlService.search_DataCrawlService.insertIntoData(insertList,database,Fields.CRAW_GOODS_ATTRIBUTE_INFO);
					} catch (Exception e) {
						logger.error("==>>插入商品属性失败失败, error:{}<<==",e);
						e.printStackTrace();
					}	
				}
			}	
		} catch (Exception e) {
			logger.error("==>>获取商品属性失败 error:{}<<==",e);
		}
		return insertList;
	}


	@SuppressWarnings("unchecked")
	public  Map<String, Object>productItemWrawler(String StringMessage,Parameter parameter) throws Exception{

		Map<String,Object > map=new HashMap<String,Object >();

		//价格
		Map<String, Object> insertPriceMap = new HashMap<String, Object>();
		List<Map<String, Object>> insertPriceList = Lists.newArrayList();

		//详情
		Map<String, Object> insertItemMap = new HashMap<String, Object>();
		List<Map<String, Object>> insertItemList = Lists.newArrayList();

		//商品规格属性
		Map<String,Object> commodityProperty=new HashMap<String,Object>();
		List<Map<String,Object>> attributeList=Lists.newArrayList();

		List<Craw_goods_Price_Info>priceList=new ArrayList<Craw_goods_Price_Info>();
		Craw_goods_InfoVO infovo=new Craw_goods_InfoVO();
		String database=parameter.getListjobName().get(0).getDatabases();
		Document doc=Jsoup.parseBodyFragment(StringMessage);
		String productName=doc.getElementsByClass("tb-main-title").text();
		if(StringUtils.isNotBlank(productName)) {
			try {
				JSONObject skuIdData=new JSONObject();
				StringBuilder bufer=new StringBuilder();
				if(StringMessage.contains("skuMap")){
					String skuMaplength=" skuMap     : ";
					String skuMap=StringMessage.substring(StringMessage.indexOf(skuMaplength)+skuMaplength.length(),StringMessage.lastIndexOf(",propertyMemoMap:"));
					String skuMapId="{\"skuMap\": "+skuMap+"}";
					skuIdData=JSONObject.fromObject(skuMapId);
				}

				String sibUrl="wholeSibUrl      : '";
				String productUrl=StringMessage.substring(StringMessage.indexOf(sibUrl)+sibUrl.length(),StringMessage.lastIndexOf("areaLimit"));
				productUrl="https:"+productUrl.replace("',", "").trim();

				//获取商品价格
				int sleppTime = (int) (1 + Math.random() * (20 - 1));
				TimeUnit.MILLISECONDS.sleep(sleppTime);
				parameter.setItemUrl_1(productUrl);
				parameter.setCookie(Fields.TMALL_COOKIE); 	
				String itemPrice=taobaoDataCrawlService.httpClientService.interfaceSwitch(parameter);//请求数据
				if(StringUtils.isNotBlank(itemPrice)) {
					if(!itemPrice.contains("detailskip.taobao.com:443")) {
						String itemJson=itemPrice.substring(itemPrice.indexOf("{"), itemPrice.lastIndexOf("}")+1);

						JSONObject jsonObject=JSONObject.fromObject(itemJson);	

						//店铺名称
						String store=doc.getElementsByClass("tb-shop-name").text();
						//商品图片
						String productImage=doc.getElementById("J_ImgBooth").attr("src");

						List<Element>tSaleProp=doc.getElementsByClass("J_TSaleProp tb-clearfix").select("li");
						List<Element>elementList=doc.getElementsByClass("J_TSaleProp tb-img tb-clearfix").select("li");
						//买家地址
						String areaName=jsonObject.getJSONObject("data").getJSONObject("deliveryFee").getJSONObject("data").getString("areaName");
						//卖家位置
						String sendCity=jsonObject.getJSONObject("data").getJSONObject("deliveryFee").getJSONObject("data").getString("sendCity");
						if(jsonObject.getJSONObject("data").getJSONObject("deliveryFee").getJSONObject("data").getJSONObject("serviceInfo").containsKey("list")){
							JSONArray arrayList=jsonObject.getJSONObject("data").getJSONObject("deliveryFee").getJSONObject("data").getJSONObject("serviceInfo").getJSONArray("list");
							if(arrayList!=null && arrayList.size()>0){
								JSONObject info=JSONObject.fromObject(arrayList.toArray()[0]);
								//快递
								infovo.setDelivery_info(info.getString("info"));
							}
						}else{
							infovo.setDelivery_info(Fields.DONTPACK_MAIL);
						}
						//库存
						String stock=jsonObject.getJSONObject("data").getJSONObject("dynStock").getString("stock");
						//销量
						String soldTotalCount=jsonObject.getJSONObject("data").getJSONObject("soldQuantity").getString("soldTotalCount");
						//商品促销
						if(jsonObject.getJSONObject("data").containsKey("couponActivity")){
							if(!jsonObject.getJSONObject("data").getJSONObject("couponActivity").getString("coupon").contains("{}")){
								JSONArray currentList=jsonObject.getJSONObject("data").getJSONObject("couponActivity").getJSONObject("coupon").getJSONArray("couponList");
								if(currentList!=null && currentList.size()>0){
									for(int j=0;j<currentList.size();j++){
										JSONObject info=JSONObject.fromObject(currentList.toArray()[j]);
										bufer.append(info.getString("title")+"&&");
									}
								}
							}
						}

						if(tSaleProp!=null && tSaleProp.size()>0){
							for(int i=0;i<tSaleProp.size();i++){
								String SkuName=tSaleProp.get(i).getElementsByTag("span").text();
								String propId=tSaleProp.get(i).attr("data-value").trim();
								for(int k=0;k<elementList.size();k++){
									Craw_goods_Price_Info priceInfo = new Craw_goods_Price_Info();
									//skuName
									String classifyName=elementList.get(k).getElementsByTag("span").text();
									String classifyPropId=elementList.get(k).attr("data-value").trim();
									String skuName=classifyName+" "+SkuName;
									//商品原价
									String groupId=";"+propId+";"+classifyPropId+";";
									String originalPrice=jsonObject.getJSONObject("data").getJSONObject("originalPrice").getJSONObject(groupId).getString("price");
									//商品现价
									if(StringUtils.isNotBlank(groupId)){
										JSONArray currentList=jsonObject.getJSONObject("data").getJSONObject("promotion").getJSONObject("promoData").getJSONArray(groupId);
										if(currentList!=null && currentList.size()>0){
											JSONObject info=JSONObject.fromObject(currentList.toArray()[0]);
											priceInfo.setCurrent_price(info.getString("price"));
										}else {
											priceInfo.setCurrent_price(originalPrice);	
										}
										if(StringUtils.isNotBlank(skuIdData.toString())){
											//商品SkuId
											String skuId=skuIdData.getJSONObject("skuMap").getJSONObject(groupId).getString("skuId");

											//sku库存
											String stockData=jsonObject.getJSONObject("data").getJSONObject("dynStock").getJSONObject("sku").getJSONObject(groupId).getString("stock");
											System.out.println("商品skuId库存："+stockData);
											//促销
											String promotionData=jsonObject.getJSONObject("data").getJSONObject("upp").getString(skuId);
											Document promotionMessage=Jsoup.parseBodyFragment(promotionData);
											promotionData="淘金币"+promotionMessage.getElementsByClass("tb_dashes_box").text()+"&&";
											promotionData=promotionData+bufer.toString();

											priceInfo.setInventory(stockData);//库存
											priceInfo.setSKUid(skuId);
											priceInfo.setSku_name(skuName);
											priceInfo.setPromotion(promotionData);
										}
									}	
									priceInfo.setBatch_time(parameter.getBatch_time());
									priceInfo.setGoodsid(parameter.getGoodsId());
									priceInfo.setCust_keyword_id(parameter.getKeywordId());
									priceInfo.setOriginal_price(originalPrice);
									priceInfo.setTo_use_amount(infovo.getTtl_comment_num());
									priceInfo.setSale_qty(soldTotalCount);//销量
									priceInfo.setDelivery_place(infovo.getDelivery_place());
									priceInfo.setChannel(parameter.getListjobName().get(0).getClient());//渠道
									priceInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
									priceInfo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
									priceInfo.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
									priceList.add(priceInfo);
									if(parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")){
										for(int kk=0;kk<2;kk++){
											if(kk==0){
												priceInfo.setChannel(Fields.CLIENT_MOBILE);	
											}else{
												priceInfo.setChannel(Fields.CLIENT_PC);	
											}
											insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
											insertPriceList.add(insertPriceMap);

										}
									}else {
										insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
										insertPriceList.add(insertPriceMap);
									}
								}
							}
						}else if(elementList!=null && elementList.size()>0){
							String promotionMessage="";
							for(int k=0;k<elementList.size();k++){
								Craw_goods_Price_Info priceInfo = new Craw_goods_Price_Info();
								//skuName
								String classifyName=elementList.get(k).getElementsByTag("span").text();
								String classifyPropId=elementList.get(k).attr("data-value").trim();


								//商品原价
								String groupId=";"+classifyPropId+";";
								String originalPrice=jsonObject.getJSONObject("data").getJSONObject("originalPrice").getJSONObject(groupId).getString("price");


								//sku库存
								String stockData=jsonObject.getJSONObject("data").getJSONObject("dynStock").getJSONObject("sku").getJSONObject(groupId).getString("stock");


								//商品现价
								if(StringUtils.isNotBlank(groupId)){
									if(!jsonObject.getJSONObject("data").getJSONObject("promotion").getJSONObject("promoData").toString().contains("{}")){
										JSONArray currentList=jsonObject.getJSONObject("data").getJSONObject("promotion").getJSONObject("promoData").getJSONArray(groupId);
										if(currentList!=null && currentList.size()>0){
											JSONObject info=JSONObject.fromObject(currentList.toArray()[0]);
											priceInfo.setCurrent_price(info.getString("price"));
										}
									}else{
										//商品现价
										String currentPrice=originalPrice;
										priceInfo.setCurrent_price(currentPrice);
									}

									//商品SkuId
									if(StringUtils.isNotBlank(skuIdData.toString())) {
										String skuId = skuIdData.getJSONObject("skuMap").getJSONObject(groupId).getString("skuId");
										priceInfo.setSKUid(skuId);
										//促销
										if(k==0) {
											String promotionData=jsonObject.getJSONObject("data").getJSONObject("upp").getString(skuId);
											Document promotionMessageData=Jsoup.parseBodyFragment(promotionData);
											promotionMessage="淘金币"+promotionMessageData.getElementsByClass("tb_dashes_box").text()+"&&";
											promotionMessage=promotionMessage+bufer.toString();
										}
									}


								}
								priceInfo.setInventory(stockData);//库存							
								priceInfo.setSku_name(classifyName);
								priceInfo.setPromotion(promotionMessage);							
								priceInfo.setBatch_time(parameter.getBatch_time());
								priceInfo.setGoodsid(parameter.getGoodsId());
								priceInfo.setCust_keyword_id(parameter.getKeywordId());
								priceInfo.setOriginal_price(originalPrice);
								priceInfo.setTo_use_amount(infovo.getTtl_comment_num());
								priceInfo.setSale_qty(soldTotalCount);//销量
								priceInfo.setDelivery_place(infovo.getDelivery_place());
								priceInfo.setChannel(parameter.getListjobName().get(0).getClient());//渠道
								priceInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
								priceInfo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
								priceInfo.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
								priceList.add(priceInfo);
								if(parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")){
									for(int kk=0;kk<2;kk++){
										if(kk==0){
											priceInfo.setChannel(Fields.CLIENT_MOBILE);	
										}else{
											priceInfo.setChannel(Fields.CLIENT_PC);	
										}
										insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
										insertPriceList.add(insertPriceMap);

									}
								}else {
									insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
									insertPriceList.add(insertPriceMap);
								}
							}
						}else{
							Craw_goods_Price_Info priceInfo = new Craw_goods_Price_Info();

							String originalPrice=jsonObject.getJSONObject("data").getJSONObject("originalPrice").getJSONObject("def").getString("price");
							priceInfo.setOriginal_price(originalPrice);

							if(!jsonObject.getJSONObject("data").getJSONObject("promotion").getJSONObject("promoData").toString().contains("{}")){
								JSONArray currentList=jsonObject.getJSONObject("data").getJSONObject("promotion").getJSONObject("promoData").getJSONArray("def");
								if(currentList!=null && currentList.size()>0){
									JSONObject info=JSONObject.fromObject(currentList.toArray()[0]);
									priceInfo.setCurrent_price(info.getString("price"));
								}
							}else{
								priceInfo.setCurrent_price(originalPrice);
							}
							priceInfo.setPromotion(bufer.toString());							
							priceInfo.setBatch_time(parameter.getBatch_time());
							priceInfo.setGoodsid(parameter.getGoodsId());
							priceInfo.setCust_keyword_id(parameter.getKeywordId());
							priceInfo.setOriginal_price(originalPrice);
							priceInfo.setTo_use_amount(infovo.getTtl_comment_num());
							priceInfo.setSale_qty(soldTotalCount);//销量
							priceInfo.setDelivery_place(infovo.getDelivery_place());
							priceInfo.setChannel(parameter.getListjobName().get(0).getClient());//渠道
							priceInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
							priceInfo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
							priceInfo.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
							priceList.add(priceInfo);
							if(parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")){
								for(int kk=0;kk<2;kk++){
									if(kk==0){
										priceInfo.setChannel(Fields.CLIENT_MOBILE);	
									}else{
										priceInfo.setChannel(Fields.CLIENT_PC);	
									}
									insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
									insertPriceList.add(insertPriceMap);

								}
							}else {
								insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
								insertPriceList.add(insertPriceMap);
							}

						}
						//商品详情
						infovo.setGoods_status(1);//状态

						infovo.setSale_qty(soldTotalCount);
						infovo.setDelivery_place(stock);
						infovo.setDelivery_place(areaName);
						infovo.setSeller_location(sendCity);
						infovo.setPlatform_shopname(store);//卖家店铺名称
						infovo.setPlatform_sellername(store);
						infovo.setGoodsId(parameter.getGoodsId());
						infovo.setPlatform_goods_name(productName);	
						infovo.setEgoodsId(parameter.getEgoodsId());
						infovo.setGoods_url(parameter.getItemUtl_2());
						infovo.setGoods_pic_url("http:"+productImage);
						infovo.setBatch_time(parameter.getBatch_time());
						infovo.setPlatform_shoptype(Fields.PROPRIETARY);
						infovo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
						infovo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
						infovo.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
						infovo.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
						//数据详情插入
						insertItemMap = BeanMapUtil.convertBean2MapWithUnderscoreName(infovo);
						insertItemList.add(insertItemMap);

						//商品属性
						List<Element>attributesList=doc.getElementsByClass("attributes-list").select("li");
						for(int ss=0;ss<attributesList.size();ss++) {
							String value=attributesList.get(ss).attr("title");
							String key=attributesList.get(ss).html().split(":")[0];
							commodityProperty= BeanMapUtil.convertBean2MapWithUnderscoreName(attributeInfo(key,value,parameter));
							attributeList.add(commodityProperty); 
						}

						String hour=parameter.getMap().get("hour").toString();
						String week=parameter.getMap().get("week").toString();
						//获取商品的规格属性
						String dateWeek=parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[0];
						if(dateWeek.contains(week)) {
							String dataTime=parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[1];
							if(!dataTime.contains(hour)) {
								attributeList.clear();
							}
						}
						//判断是否开启批量抓取数据 1，批量 4 实时抓取
						if(parameter.getListjobName().get(0).getStorage().equals("4")) {
							//插入商品详情 价格
							if(StringUtil.isNotEmpty(infovo.getPlatform_goods_name())){
								if(insertPriceList!=null && insertPriceList.size()>0){

									try {
										taobaoDataCrawlService.search_DataCrawlService.insertIntoData(insertPriceList, database, Fields.TABLE_CRAW_GOODS_PRICE_INFO);
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
								if(insertItemList!=null && insertItemList.size()>0){
									try {
										taobaoDataCrawlService.search_DataCrawlService.insertIntoData(insertItemList,database, Fields.TABLE_CRAW_GOODS_INFO);
									} catch (Exception e) {
										e.printStackTrace();
									}	
								}
							}


							//插入商品评论描述
							if(attributeList!=null && attributeList.size()>0){
								try {
									taobaoDataCrawlService.search_DataCrawlService.insertIntoData(attributeList,parameter.getListjobName().get(0).getDatabases(),Fields.CRAW_GOODS_COMMENT_INFO);
								} catch (Exception e) {
									logger.error("==>>获取商品评分失败error:{}<<==",e);
									e.printStackTrace();
								}
							}
						}
					}
				}
			}catch(Exception e) {
				log.error("==>>解析商品详情发送异常："+e.getMessage()+"<<==");
			}
		}
		return map;


	}


	//////////////////////////////////////////////////////关键词搜索///////////////////////////////////////////////////////////////////////////////////////////
	/***
	 * 分页请求数据
	 * @throws UnsupportedEncodingException 
	 **/
	public Map<String, String> productPage(Map<String, String> map, CrawKeywordsInfo crawKeywordsInfo,int page){
		Map<String, String> tmall =new HashMap<String, String>();
		String productUrl=crawKeywordsInfo.getCust_keyword_url();
		try {
			if (crawKeywordsInfo.getCust_keyword_url().contains("pepsico.m.tmall.com") || crawKeywordsInfo.getCust_keyword_url().contains("guigeshipin.m.tmall.com")) {
				String keyword = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
				map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("KEYWORD", keyword).replace("PAGE", String.valueOf(page)));
				map.put("coding", "GBK");
				map.put("cookie", null);
				map.put("urlRef", productUrl.replace("KEYWORD", keyword).replace("PAGE", String.valueOf(page)));
			} else if (crawKeywordsInfo.getCust_keyword_url().contains("list.tmall.com") && crawKeywordsInfo.getCust_keyword_type().contains("keyword")) {
				if (crawKeywordsInfo.getCust_keyword_url().contains("SEARCH")) {
					String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
					map.put("url", productUrl.replace("PAGE", String.valueOf((page * 60))).replace("SEARCH", keyWord));
					map.put("coding", "GBK");
				} else {
					if (page != 1) {
						productUrl = productUrl + "&s=" + 40 * page;
						map.put("coding", "GBK");
						map.put("url", productUrl);
					}
				}
			} else if (crawKeywordsInfo.getCust_keyword_url().contains("search_radio_tmall") || crawKeywordsInfo.getCust_keyword_url().contains("filter_tianmao=tmall") || crawKeywordsInfo.getCust_keyword_url().contains("search_radio_all")) {
				String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
				productUrl = crawKeywordsInfo.getCust_keyword_url().replace("SEARCH", keyWord).replace("PAGE", String.valueOf((page - 1) * 44));
				map.put("url", productUrl);
				map.put("coding", "GBK");
			} else if (crawKeywordsInfo.getCust_keyword_url().contains("list.tmall.com") && crawKeywordsInfo.getCust_keyword_type().contains("category")) {//天猫指定类目搜索
				String codeCookie=map.get("codeCookie").toString();
				if (crawKeywordsInfo.getCust_keyword_url().contains("SEARCH")) {
					String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
					map.put("url", productUrl.replace("PAGE", String.valueOf((page * 60))).replace("SEARCH", keyWord));
				} else {
					map.put("url", productUrl.replace("PAGE", String.valueOf((page * 60))).replace("CODE", codeCookie));
				}
				map.put("coding", "GBK");
			} else if (crawKeywordsInfo.getCust_keyword_url().contains("s.m.taobao.com")) {//手机端搜索商品
				String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
				productUrl = crawKeywordsInfo.getCust_keyword_url().replace("SEARCH", keyWord).replace("PAGE", String.valueOf((page)));
				map.put("url", productUrl);
			}
		} catch (Exception e) {
			logger.error("==>>拼接商品的搜索页，发送异常, error:{}<<==",e);
			e.printStackTrace();
		}
		return tmall;
	}


}
