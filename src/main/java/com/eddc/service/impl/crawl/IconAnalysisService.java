/**   
 * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.service 
 * @author: jack.zhao   
 * @date: 2017年12月5日 下午2:38:10 
 */
package com.eddc.service.impl.crawl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.mapper.IconAnalysisMapper;
import com.eddc.model.Craw_goods_Info;
import com.eddc.model.QrtzCrawlerTable;
@Service
public class IconAnalysisService {
	@Autowired
	private IconAnalysisMapper iconAnalysisMapper;
	//private SimpleDateFormat form=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	//private static Logger logger = Logger.getLogger(IconAnalysisService.class);
	
	/**   
	*    
	* 项目名称：Price_monitoring_crawler   
	* 类名称：IconAnalysisService   
	* 类描述：   获取监控爬虫用户
	* 创建人：jack.zhao   
	* 创建时间：2017年12月5日 下午2:38:10   
	* 修改人：jack.zhao   
	* 修改时间：2017年12月5日 下午2:38:10   
	* 修改备注：   
	* @version    
	*    
	*/
	public List<QrtzCrawlerTable>getIconDetails(){
		return iconAnalysisMapper.getIconDetails();
	}
	/**  
	* @Title: GoodsFrameNumber  
	* @Description: TODO(<!-- 获取用户商品 上架，下架 个数 -->
	* @param @param platform
	* @param @param userId
	* @param @param status
	* @param @return    设定文件  
	* @return List<Craw_goods_Info>    返回类型  
	* @throws  
	*/  
	public List<Craw_goods_Info>GoodsFrameNumber(String platform ,String userId,String status){
		return iconAnalysisMapper.GoodsFrameNumber(platform,userId,status);
	}
	
	public List<Craw_goods_Info>getGoodsInfoQuery(String platform ,String userId,String status){
		return iconAnalysisMapper.getGoodsInfoQuery(platform,userId,status);
	}
}
