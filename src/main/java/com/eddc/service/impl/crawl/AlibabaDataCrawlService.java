package com.eddc.service.impl.crawl;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.eddc.model.Parameter;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.*;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.model.Craw_goods_Vendor_Info;
import com.eddc.model.Craw_goods_Vendor_price_Info;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;

@Service
public class AlibabaDataCrawlService {
	private static Logger logger = LoggerFactory.getLogger(AlibabaDataCrawlService.class);

	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	SqlSessionTemplate sqlSessionTemplate;
	@Autowired
	SearchDataCrawlService search_DataCrawlService;
	@Autowired
	WhetherShelves whetherShelves;


	/*****************************************解析数据****************************************************************************************************/
	public Map<String, Object> parameter(Parameter Par) throws Exception {
		Map<String, Object> itemSuning = new HashMap<>(10);
		if (!Par.getType().equalsIgnoreCase(Fields.STYPE_6)) {
			try {
				String itemUrl=Fields.PLATFORM_1688_PC_URL.replace("EGOODSID",Par.getEgoodsId());
				Par.setItemUrl_1(itemUrl);
				Par.setItemUtl_2(itemUrl);
				//请求数据
				String StringMessage = httpClientService.interfaceSwitch(Par);
				//判断商品是否下架
				String status = whetherShelves.parseGoodsStatusByItemPage(StringMessage);
				if (Integer.valueOf(status).equals(Integer.valueOf(Fields.STATUS_OFF))) {
					logger.info("==>>" + Par.getEgoodsId() + Par.getListjobName().get(0).getPlatform() + "当前商品已经下架,插入数据库 当前是第:" + Par.getSum() + "商品<<==");
					crawlerPublicClassService.soldOutStatus(status, Par.getListjobName().get(0).getDatabases(), Par);
					return itemSuning;
				}
				itemSuning = itemWailer(StringMessage, Par);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("==>>" + Par.getListjobName().get(0).getPlatform() + "解析数据失败 ,error:" + e.getMessage() + "<<==");
			}
		}
		return itemSuning;
	}


	/************************************商品详情*********************************************/

	@SuppressWarnings("unchecked")
	public Map<String, Object> itemWailer(String itemPage, Parameter parameter) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>(10);
		List<Map<String, Object>> insertImage = Lists.newArrayList();
		List<Map<String, Object>> insertPrice = Lists.newArrayList();
		List<Map<String, Object>> insert = Lists.newArrayList();
		Map<String, Object> insertItem = new HashMap<String, Object>();
		Map<String, Object> insertItemPrice = new HashMap<String, Object>();
		StringBuilder builder = new StringBuilder();
		Craw_goods_Vendor_Info info = new Craw_goods_Vendor_Info();
		info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setChannel(Fields.CLIENT_PC);
		info.setEgoodsid(parameter.getEgoodsId());
		info.setGoodsid(parameter.getGoodsId());
		info.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
		info.setGoods_status(Integer.valueOf(Fields.STATUS_ON));
		info.setBatch_time(parameter.getBatch_time());
		if (itemPage.contains(info.getEgoodsid())) {
			try {
				Document jsonp = Jsoup.parse(itemPage.toString());
				//FileUtils.writeStringToFile(new File("D://aa.html"), itemPage);
				info.setPlatform_goodsname(jsonp.getElementsByClass("d-title").text());
				info.setPurchase_promotion(jsonp.getElementsByClass("d-content static-content detail-vip-login-content").text().replace("|", ""));
				info.setPlatform_vendoraddress(jsonp.getElementsByClass("delivery-addr").text());
				info.setPlatform_goods_picurl(jsonp.getElementsByClass("vertical-img").first().getElementsByTag("img").attr("src").toString());
				//供应商Id
				info.setPlatform_vendorid(StringHelper.encryptByString(info.getPlatform_goodsname() + Fields.platform_1688));
				//商品满意度
				info.setPlatform_vendoraddress(StringHelper.nullToString(StringHelper.getResultByReg(jsonp.toString(), "property=\"og:product:score\" content=\"([^<>\"]+)")));
				//商品成交数量
				info.setPurchase_qty(jsonp.getElementsByClass("bargain-number").get(0).getElementsByTag("em").text());
				//卖家店铺
				String platformSellername = jsonp.getElementsByClass("link name").text();
				if (StringUtils.isEmpty(platformSellername)) {
					platformSellername = jsonp.getElementsByClass("contactSeller").get(0).getElementsByClass("name").text();
				}
				info.setPlatform_sellername(platformSellername);
				//评论
				if (jsonp.toString().contains("satisfaction-number")) {
					String purchase_comment_num = jsonp.getElementsByClass("satisfaction-number").get(0).getElementsByTag("em").text();
					if (purchase_comment_num.contains(Fields.MYRIAD)) {
						purchase_comment_num = purchase_comment_num.replace(Fields.MYRIAD, "").trim();
						purchase_comment_num = String.valueOf((Integer.valueOf(purchase_comment_num.toString()) * 10000));
						info.setPurchase_comment_num(purchase_comment_num);
					}
				}
				//经营模式
				String platform_vendortype = jsonp.getElementsByClass("biz-type-model").text();
				//货描 高于或者低于均值
				if (StringUtil.isNotEmpty(jsonp.getElementsByClass("description-value-higher-hm  bsr-value-higher-common ").text())) {
					String goods_description_rate = jsonp.getElementsByClass("description-value-higher-hm  bsr-value-higher-common ").text();
					if (goods_description_rate.contains(Fields.ZERO)) {
						goods_description_rate = Fields.ZERO_ONE;
					} else {
						goods_description_rate = goods_description_rate.split("%")[0] + "%";
					}
					info.setGoods_description_rate(goods_description_rate);
				}
				//发货
				if (StringUtil.isNotEmpty(jsonp.getElementsByClass("description-value-higher-fh  bsr-value-higher-common ").text())) {
					String delivery_speed_rate = jsonp.getElementsByClass("description-value-higher-fh  bsr-value-higher-common ").text();
					if (delivery_speed_rate.contains(Fields.ZERO)) {
						delivery_speed_rate = Fields.ZERO_ONE;
					} else {
						delivery_speed_rate = delivery_speed_rate.split("%")[0] + "%";
					}
					info.setDelivery_speed_rate(delivery_speed_rate);
				}
				//回头率
				if (StringUtil.isNotEmpty(jsonp.getElementsByClass("description-show-ht description-value-ht").text())) {
					String purchase_back_rate = jsonp.getElementsByClass("description-show-ht description-value-ht").text();
					if (purchase_back_rate.contains(Fields.ZERO)) {
						purchase_back_rate = Fields.ZERO_ONE;
					} else {
						purchase_back_rate = purchase_back_rate.split("%")[0] + "%";
					}
					info.setPurchase_back_rate(purchase_back_rate);
				}
				//供应商名称
				if (StringUtil.isNotEmpty(jsonp.getElementsByClass("nameArea").text())) {
					info.setPlatform_vendorname(jsonp.getElementsByClass("nameArea").get(0).getElementsByTag("a").text());
				} else {
					info.setPlatform_vendorname(jsonp.getElementsByClass("name has-tips ").text());
				}
				//价格
				Elements price = jsonp.getElementsByClass("price").select("td");
				for (int k = 0; k < price.size(); k++) {
					Craw_goods_Vendor_price_Info infoPrice = priceInfo(parameter);
					if (!price.get(k).text().contains(Fields.PRICE)) {
						if (price.get(k).toString().contains("begin")) {
							JSONObject currPriceJson = JSONObject.fromObject(price.get(k).getElementsByTag("td").attr("data-range").toString());
							if (StringUtil.isNotEmpty(currPriceJson.get("end").toString())) {
								//起批量
								infoPrice.setPurchase_amount(currPriceJson.get("begin") + "-" + currPriceJson.get("end"));
							} else {
								//起批量
								infoPrice.setPurchase_amount(currPriceJson.get("begin").toString());
							}
							//价格
							infoPrice.setPurchase_price(currPriceJson.get("price").toString());
							//件 ，个，台，套
							if (StringUtil.isNotEmpty(jsonp.getElementsByClass("amount").text())) {
								infoPrice.setPurchase_unit(jsonp.getElementsByClass("amount").get(0).getElementsByClass("unit").get(0).text());
							}
							insertItemPrice = BeanMapUtil.convertBean2MapWithUnderscoreName(infoPrice);
							insertPrice.add(insertItemPrice);

						}

					}
				}
				if (!price.toString().contains("data-range=")) {

					Craw_goods_Vendor_price_Info infoPrice = priceInfo(parameter);
					//价格
					infoPrice.setPurchase_price(price.get(price.size() - 1).getElementsByClass("value").text());
					//件 ，个，台，套
					infoPrice.setPurchase_unit(jsonp.getElementsByClass("amount").get(0).getElementsByClass("unit").text());
					//起批量
					infoPrice.setPurchase_discount(jsonp.getElementsByClass("amount").get(0).getElementsByClass("value").text());

					insertItemPrice = BeanMapUtil.convertBean2MapWithUnderscoreName(infoPrice);
					insertPrice.add(insertItemPrice);

				}

				//产品属性
				if (jsonp.getElementsByClass("widget-custom offerdetail_ditto_attributes").contains("de-")) {
					Elements met = jsonp.getElementsByClass("widget-custom offerdetail_ditto_attributes").get(0).getElementsByClass("obj-content").get(0).getElementsByTag("table").get(0).getElementsByTag("tbody").get(0).getElementsByTag("tr");
					for (Element mss : met) {
						Elements talent = mss.select("td");
						for (int i = 0; i < talent.size(); i++) {
							if (i % 2 == 0) {
								builder.append(talent.get(i).getElementsByClass("de-feature").text() + ":");
							} else {
								builder.append(talent.get(i).getElementsByClass("de-value").text() + "&&");
							}
						}
					}
				}
				//-商品详情
				info.setPlatform_goods_detail(builder.toString());

				if (insertPrice != null && insertPrice.size() > 0) {
					String database = parameter.getListjobName().get(0).getDatabases();
					//插入商品价格
					search_DataCrawlService.insertIntoData(insertPrice, database, Fields.TABLE_CRAW_VENDOR_PRICE_INFO);
					//插入商品信息详情
					insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
					insert.add(insertItem);
					search_DataCrawlService.insertIntoData(insertImage, database, Fields.TABLE_CRAW_VENDOR_INFO);

				}
			} catch (Exception e) {
				logger.error("==>>1688解析出错了 <<=="+e.getMessage());
				e.printStackTrace();
			}
		}
		return map;
	}

	/**
	 * 封装商品价格
	 *
	 * @param parameter
	 * @return
	 * @author jack.zhao
	 * @data 2020/4/8 10:40
	 */
	public Craw_goods_Vendor_price_Info priceInfo(Parameter parameter) {
		Craw_goods_Vendor_price_Info infoPrice = new Craw_goods_Vendor_price_Info();
		infoPrice.setBatch_time(parameter.getBatch_time());
		infoPrice.setChannel(Fields.CLIENT_PC);
		infoPrice.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
		infoPrice.setEgoodsid(parameter.getEgoodsId());
		infoPrice.setGoodsid(parameter.getGoodsId());
		infoPrice.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
		infoPrice.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		return infoPrice;
	}

	//判断商品是否下架
	public List<Craw_goods_Vendor_Info> goodsShopMessage(String egoodsId) {
		return crawlerPublicClassService.goodsShopMessage(egoodsId);

	}


	public HttpClientService getHttpClientService() {
		return httpClientService;
	}

	public void setHttpClientService(HttpClientService httpClientService) {
		this.httpClientService = httpClientService;
	}

	public CrawlerPublicClassService getCrawlerPublicClassService() {
		return crawlerPublicClassService;
	}

	public void setCrawlerPublicClassService(CrawlerPublicClassService crawlerPublicClassService) {
		this.crawlerPublicClassService = crawlerPublicClassService;
	}

	public SqlSessionTemplate getSqlSessionTemplate() {
		return sqlSessionTemplate;
	}

	public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
		this.sqlSessionTemplate = sqlSessionTemplate;
	}

	public SearchDataCrawlService getSearch_DataCrawlService() {
		return search_DataCrawlService;
	}

	public void setSearch_DataCrawlService(SearchDataCrawlService search_DataCrawlService) {
		this.search_DataCrawlService = search_DataCrawlService;
	}

	public WhetherShelves getWhetherShelves() {
		return whetherShelves;
	}

	public void setWhetherShelves(WhetherShelves whetherShelves) {
		this.whetherShelves = whetherShelves;
	}

}
