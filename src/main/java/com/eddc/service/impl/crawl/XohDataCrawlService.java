package com.eddc.service.impl.crawl;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Craw_keywords_temp_Info_forsearch;
import com.eddc.model.Parameter;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.google.common.collect.Lists;
import com.xxl.job.core.log.XxlJobLogger;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jack.zhao
 */
@Slf4j
@Service
public class XohDataCrawlService {
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private SearchDataCrawlService search_DataCrawlService;

	/* 获取商品信息
	 */

	public Map<String, Object> itemAppPage(String itemPage, Parameter parameter) throws Exception {
		Map<String, Object> item = new HashMap<String, Object>(10);
		if (itemPage.contains("paging")) {
			Document docs = Jsoup.parse(itemPage);
			List<Element> listPage = docs.getElementsByClass("table_list clearfix").select(".content");
			String page = "0";
			if (docs.getElementsByClass("paging").toString().contains("href")) {
				listPage = docs.getElementsByClass("paging").get(0).getElementsByTag("a");
				page = docs.getElementsByClass("paging").get(0).getElementsByTag("a").get(listPage.size() - 2).text();
			} else if (docs.getElementsByClass("paging").toString().contains("current")) {
				page = docs.getElementsByClass("current").text();
			}
			if (!page.equalsIgnoreCase("1") && !page.equalsIgnoreCase("0")) {
				item = insertSerchProduct(itemPage, parameter);
				
				int pageData = Integer.valueOf(page);
				//String productName = URLEncoder.encode(parameter.getCrawKeywordsInfo().getCust_keyword_name(), "UTF-8");
				for (int i = 2; i <= pageData; i++) {
					String url="http://www.junyier.com/goodslist/cid/0/bid/0/trade/0,0,0/xl/0/fh/0,0/goods_name/"+parameter.getCrawKeywordsInfo().getCust_keyword_name()+"/bs/1/p/"+i;
					//String url = "http://www.xinouhui.com/goodslist/cid/0/bid/0/trade/0,0,0/xl/0/fh/0,0/bs/1/goods_name/"+productName+"/bs/1/p/"+i;
					parameter.setItemUrl_1(url);
					parameter.setItemUtl_2(url);
					XxlJobLogger.log("==>>开始请求商品数据<<=="+url);
					
					String serchItem = httpClientService.interfaceSwitch(parameter);
					//FileUtils.writeStringToFile(new File("D://"+i+".html"),serchItem,"utf-8");
					if (serchItem.contains("goods")) {
						item = insertSerchProduct(serchItem, parameter);
						
					}
				}
			} else {
				item = insertSerchProduct(itemPage, parameter);
			}

		}
		return item;

	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> insertSerchProduct(String itemPage, Parameter parameter) throws Exception {
		Map<String, Object> item = new HashMap<String, Object>(10);
		Map<String, Object> insertPriceMap = new HashMap<String, Object>();
		List<Map<String, Object>> insertPriceList = Lists.newArrayList();
		Map<String, Object> insertItem = new HashMap<String, Object>();
		List<Map<String, Object>> insertItems = Lists.newArrayList();

		Map<String, Object> insertSerch = new HashMap<String, Object>();
		List<Map<String, Object>>insertList= Lists.newArrayList();

		Document jsoup=Jsoup.parse(itemPage);
		Craw_goods_InfoVO info=new Craw_goods_InfoVO();
		List<Element>element=jsoup.getElementsByClass("table_list clearfix").select(".content");
		for(int i=0;i<element.size();i++){
			String keywordName=element.get(i).getElementsByTag("a").attr("href").replace("/goods/", "").trim();
			String goodsName=element.get(i).getElementsByClass("p3").text();
			//			String price=element.get(i).getElementsByClass("p4").text().replace("￥","").trim();
			String image=element.get(i).getElementsByClass("p1").get(0).getElementsByTag("img").attr("src").trim();
			String goodsUrl="http://www.junyier.com/"+element.get(i).getElementsByTag("a").attr("href").trim();
			//String goodsUrl="http://www.xinouhui.com"+element.get(i).getElementsByTag("a").attr("href").trim();
			List<Element>list=element.get(i).getElementsByClass("bottom").select(".p2");
			parameter.setItemUrl_1(goodsUrl);
			String serchItem=httpClientService.interfaceSwitch(parameter);//请求数据
			XxlJobLogger.log("==>>当前请求的商品url："+goodsUrl+",<<==");
			if(!serchItem.contains("没有找到符合的商品!")){
				XxlJobLogger.log("此商品存在。。。。"+goodsName);
				Map<String, Object> itemDesc=insertProduct( serchItem,  parameter);
				info=(Craw_goods_InfoVO) itemDesc.get("Craw_goods_InfoVO");
				if(StringUtils.isEmpty(info.getPlatform_goods_name())){
					info.setPlatform_goods_name(goodsName);
				}
				info.setEgoodsId(keywordName);
				info.setGoods_pic_url(image);
				String date=itemDesc.get("date").toString();
				for(int k=0;k<list.size();k++){
					Craw_goods_Price_Info priceInfo = new Craw_goods_Price_Info();
					String  price=list.get(k).text().replace("(", "").replace(")", "").split("￥")[2].toString().split("/")[0];
					priceInfo.setGoodsid(StringHelper.encryptByString(keywordName+parameter.getListjobName().get(0).getPlatform()));
					priceInfo.setChannel(Fields.CLIENT_PC);
					priceInfo.setSKUid(keywordName);
					priceInfo.setBatch_time(parameter.getBatch_time());
					priceInfo.setCust_keyword_id(parameter.getKeywordId());
					priceInfo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
					priceInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
					priceInfo.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());
					priceInfo.setSku_name(list.get(k).text() );
					priceInfo.setPromotion("效期:"+date);
					priceInfo.setCurrent_price(price);
					priceInfo.setOriginal_price(price);
					//开始插入商品价格信息
					insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
					insertPriceList.add(insertPriceMap);
				}
			}else{
				XxlJobLogger.log("没有找到符合的商品"+goodsName);
			}
			//开始插入商品详情信息
			insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
			insertItems.add(insertItem);
			
			//把测试数据插入搜索表etl进行对比
			insertSerch = BeanMapUtil.convertBean2MapWithUnderscoreName(crawKeywordsTempInfo(info,parameter.getCrawKeywordsInfo().getCust_account_id()));
			insertList.add(insertSerch);
		}
		
		
		if(insertPriceList!=null && insertPriceList.size()>0){
			XxlJobLogger.log("==>>开始插入数据<<==");
			search_DataCrawlService.insertIntoData(insertItems, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_INFO);

			search_DataCrawlService.insertIntoData(insertPriceList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PRICE_INFO);

			search_DataCrawlService.insertIntoData(insertList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH);
		}
		return item;
	}

	public Craw_keywords_temp_Info_forsearch crawKeywordsTempInfo(Craw_goods_InfoVO info,String account_id ){
		Craw_keywords_temp_Info_forsearch tempInfo = new Craw_keywords_temp_Info_forsearch();

		tempInfo.setCust_keyword_name(info.getEgoodsId());
		tempInfo.setCrawling_status(Fields.STATUS_ON);
		tempInfo.setFeature_1(Fields.STATUS_ON);
		tempInfo.setCust_account_id(account_id);
		tempInfo.setCust_keyword_id(String.valueOf(info.getCust_keyword_id()));
		tempInfo.setPlatform_name(info.getPlatform_name_en());
		tempInfo.setPlatform_shopname(info.getPlatform_shopname());
		tempInfo.setCust_keyword_type(Fields.EGOODS_ID); //wordsInfo.getCust_keyword_type()
		tempInfo.setCrawling_fre("1");

		tempInfo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
		tempInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		tempInfo.setCust_keyword_url(info.getGoods_url());
		tempInfo.setPlatform_goodsname(info.getPlatform_goods_name());
		tempInfo.setCrawed_status(Integer.valueOf(Fields.COUNT_01.toString()));
		return tempInfo;
	}


	public Map<String, Object> insertProduct(String itemPage, Parameter parameter) {
		Craw_goods_InfoVO info = new Craw_goods_InfoVO();
		Map<String, Object>map=new HashMap<>();
		String date="";
		try {
			info.setFeature1(Fields.COUNT_01);
			info.setEgoodsId(parameter.getEgoodsId());
			info.setGoodsId(StringHelper.encryptByString(parameter.getEgoodsId()+parameter.getListjobName().get(0).getPlatform()));
			info.setGoods_url(parameter.getItemUrl_1());
			info.setBatch_time(parameter.getBatch_time());
			info.setGoods_status(Fields.STATUS_COUNT_1);
			info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			info.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
			info.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());
			Document jsoup = Jsoup.parse(itemPage);
			if (jsoup.toString().contains("warehouse_name")) {
				info.setPlatform_goods_name(jsoup.getElementsByClass("title clearfix").select(".span2").text());
				info.setDelivery_place(jsoup.getElementById("warehouse_name").text());
				info.setSeller_location(jsoup.getElementById("warehouse_name").text());
				info.setGoods_pic_url(jsoup.getElementById("showbox").getElementsByTag("img").attr("src").trim());	
				Element element= jsoup.getElementsByClass("p3 options_list clearfix date_xq").select("span").last();
				date=element.text();
				info.setInventory(jsoup.getElementsByClass("p4 clearfix").get(1).getElementsByTag("span").text());
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("【解析新欧汇数据发生异常：{}】", e);
		}
		map.put("Craw_goods_InfoVO", info);
		map.put("date",date);
		return map;
	}
}
