package com.eddc.service.impl.crawl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.eddc.service.AbstractProductAnalysis;
import com.eddc.service.impl.http.HttpClientService;
import com.github.pagehelper.util.StringUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Parameter;
import com.eddc.util.Fields;
import com.eddc.util.StringHelper;
import com.eddc.util.Validation;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;

/**
 * @author jack.zhao
 */
@Service("suningDataCrawlService")
@Slf4j
public class SuningDataCrawlService extends AbstractProductAnalysis {
    private static Logger logger = LoggerFactory.getLogger(SuningDataCrawlService.class);
    @Autowired
    HttpClientService httpClientService;
    @Autowired
    private SearchDataCrawlService search_DataCrawlService;
    /**
     * 反射机制脱离了spring容器的管理，导致@Resource等注解失效。
     * jack.zhao
     */
    public static SuningDataCrawlService suningDataCrawlService;

    @PostConstruct
    public void init() {
        suningDataCrawlService = this;
    }

    /*****************************************解析数据****************************************************************************************************/

    @Override
    public Map<String, Object> getProductLink(Parameter parameter) throws Exception {
        Map<String, Object> itemSuning = new HashMap<>(10);
        if (!parameter.getType().equalsIgnoreCase(Fields.STYPE_6)) {
            try {
                if (StringUtils.isNotBlank(parameter.getDeliveryPlace().getDelivery_place_code())) {
                    parameter.setItemUrl_1(parameter.getCrawKeywordsInfo().getCust_keyword_url().replace("EGOODSID", parameter.getEgoodsId()).replace("SHOPID", parameter.getCrawKeywordsInfo().getPlatform_shopid()).replace("CITY", parameter.getDeliveryPlace().getDelivery_place_code()));
                } else {
                    parameter.setItemUrl_1(parameter.getCrawKeywordsInfo().getCust_keyword_url().replace("EGOODSID", parameter.getEgoodsId()).replace("SHOPID", parameter.getCrawKeywordsInfo().getPlatform_shopid()));
                }

                if (parameter.getListjobName().get(0).getClient().equalsIgnoreCase("ALL")) {
                    parameter.setItemUtl_2(Fields.SUNING_PC_URL_1 + parameter.getEgoodsId().replace("0000000000", parameter.getCrawKeywordsInfo().getPlatform_shopid()) + ".html");
                } else {
                    String ref = Fields.SUNING_APP_URL_1 + "/" + parameter.getEgoodsId() + ".html";
                    parameter.setItemUtl_2(ref.replace("0000000000", parameter.getCrawKeywordsInfo().getPlatform_shopid()));
                }
                /*请求数据*/
                itemSuning = getProductRequest(parameter);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("==>>" + parameter.getListjobName().get(0).getPlatform() + "解析数据失败 ,error:" + e.getMessage() + "<<==");
            }
        }
        return itemSuning;
    }
    /************************************商品详情*********************************************/

    /**
     * @param parameter 参数
     * @Description: 解析数据
     * @Param: itemPage 报文
     * @date: 2020/12/11
     * @Author: Jack.zhao
     * @return: Map
     */

    @Override
    public Map<String, Object> getProductItemAnalysisData(String StringMessage, Parameter parameter) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>(10);
        String shopName = "";
        String proprietary = Fields.PROPRIETARY;
        String catalogId = "";
        String stock = "";
        String jdPrice = "";
        String originalPrice = "";
        String vendor = "";
        String priceType = "";
        String vendorType = "";
        String imageUrl = "";
        //价格
       
        List<Map<String, Object>> insertPriceListApp = Lists.newArrayList();
        List<Map<String, Object>> insertPriceListPC = Lists.newArrayList();
        //详情
      
        List<Map<String, Object>> insertItemList = Lists.newArrayList();
        StringBuffer promotion = new StringBuffer();
        Craw_goods_InfoVO inform = productInfo(parameter);
        Craw_goods_Price_Info priceInfo = productInfoPrice(parameter);
        if (!Validation.isEmpty(StringMessage)) {
            String strs = StringMessage.substring(StringMessage.indexOf("{"), StringMessage.lastIndexOf("}") + 1);
            JSONObject currPriceJson = JSONObject.fromObject(strs);
            if (strs.contains("itemName")) {
                if (currPriceJson.getJSONObject("data").containsKey("bookCanSave")) {
                    priceInfo.setDeposit(currPriceJson.getJSONObject("data").getString("bookCanSave"));
                }
                //商品名称
                inform.setPlatform_goods_name(currPriceJson.getJSONObject("data").getJSONObject("data1").getJSONObject("data").getJSONObject("itemInfoVo").get("itemName").toString());
                //店铺是否存在
                if (currPriceJson.getJSONObject("data").toString().contains("brandName")) {
                    try {

                        if (currPriceJson.getJSONObject("data").getJSONObject("data1").getJSONObject("data").containsKey("brandName")) {
                            shopName = currPriceJson.getJSONObject("data").getJSONObject("data1").getJSONObject("data").getString("brandName");
                        } else {
                            shopName = currPriceJson.getJSONObject("data").getJSONObject("data1").getJSONObject("data").getString("shopName");
                        }
                        inform.setPlatform_shopname(shopName);
                        inform.setPlatform_sellername(inform.getPlatform_shopname());
                    } catch (Exception e) {
                        logger.error("解析店铺名称一次：error{" + e + "}");
                    }
                }

                if (StringUtils.isEmpty(inform.getPlatform_shopname())) {
                    inform.setPlatform_shopname(currPriceJson.getJSONObject("data").getJSONObject("data1").getJSONObject("data").getString("shopName"));
                    inform.setPlatform_sellername(currPriceJson.getJSONObject("data").getJSONObject("data1").getJSONObject("data").getString("shopName"));

                }

                if (inform.getPlatform_shopname().contains(Fields.YES_PROPRIETARY) || StringMessage.contains(Fields.YES_PROPRIETARY)) {
                    proprietary = Fields.YES_PROPRIETARY;
                } else {
                    proprietary = Fields.PROPRIETARY;
                }
                inform.setPlatform_shoptype(proprietary);
            } else if (currPriceJson.getJSONObject("data").toString().contains("snsltDesc")) {
                if (currPriceJson.getJSONObject("data").get("snsltDesc").toString().contains(Fields.YES_PROPRIETARY)) {
                    proprietary = Fields.YES_PROPRIETARY;
                } else {
                    proprietary = Fields.PROPRIETARY;
                }
            } else {
                proprietary = Fields.PROPRIETARY;
            }
            //店铺类型
            inform.setPlatform_shoptype(proprietary);

            try {
                if (currPriceJson.getJSONObject("data").getJSONObject("data1").getJSONObject("data").toString().contains("categoryId")) {
                    catalogId = currPriceJson.getJSONObject("data").getJSONObject("data1").getJSONObject("data").getJSONObject("groupInfoVO").get("categoryId").toString();
                }
            } catch (Exception e1) {
            }
            //行业Id
            inform.setPlatform_category(catalogId);
            //库存
            if (currPriceJson.getJSONObject("data").toString().contains("invStatus")) {
                stock = currPriceJson.getJSONObject("data").get("invStatus").toString();
            }

            if (stock.equals(Fields.STATUS_ON_2)) {
                stock = Fields.IS_NOT_STOCK;
            } else if (stock.equals(Fields.STATUS_OFF_3)) {
                stock = Fields.IS_NOT_STOCK;
            } else {
                stock = Fields.IN_STOCK;
            }
            //库存
            inform.setInventory(stock);

            JSONArray priceInfoJson = currPriceJson.getJSONObject("data").getJSONObject("price").getJSONArray("saleInfo");
            if (priceInfoJson.size() > 0) {
                //现价
                jdPrice = JSONObject.fromObject(priceInfoJson.toArray()[0]).get("promotionPrice").toString();
                if (priceInfoJson.contains("netPrice")) {
                    //原价
                    originalPrice = JSONObject.fromObject(priceInfoJson.toArray()[0]).get("netPrice").toString();
                } else {
                    originalPrice = jdPrice;
                }
                String bookPrice = JSONObject.fromObject(priceInfoJson.toArray()[0]).get("bookPrice").toString();
                if (Validation.isEmpty(priceInfo.getCurrent_price())) {
                    priceInfo.setDeposit(bookPrice);
                }
                String bookPriceSwell = JSONObject.fromObject(priceInfoJson.toArray()[0]).get("bookPriceSwell").toString();
                try {
                    if (StringUtils.isNotBlank(bookPrice)) {
                        double d = (Double.parseDouble(bookPriceSwell) - Double.parseDouble(bookPrice));
                        String toUseAmt = String.valueOf(d);
                        priceInfo.setTo_use_amount(toUseAmt);
                    }

                }catch (Exception e){
                    e.printStackTrace();

                }
                //商品原价
                priceInfo.setOriginal_price(originalPrice);
                //商品现价
                priceInfo.setCurrent_price(jdPrice);
                //拼团价格促销
                if (StringUtils.isNotEmpty(JSONObject.fromObject(priceInfoJson.toArray()[0]).get("pgPrice").toString())) {
                    promotion.append(Fields.SPELL_GROUP_PRICE + JSONObject.fromObject(priceInfoJson.toArray()[0]).get("pgPrice").toString() + "&&");
                }
            }
            //供应商
            vendor = JSONObject.fromObject(priceInfoJson.toArray()[0]).get("vendor").toString();
            priceType = JSONObject.fromObject(priceInfoJson.toArray()[0]).get("priceType").toString();
            //供应商类型
            vendorType = JSONObject.fromObject(priceInfoJson.toArray()[0]).get("vendorType").toString();
            //获取商品图片
            if (currPriceJson.getJSONObject("data").getJSONObject("data1").getJSONObject("data").toString().contains("mainPictureList")) {
                JSONArray imgList = currPriceJson.getJSONObject("data").getJSONObject("data1").getJSONObject("data").getJSONArray("mainPictureList");
                if (imgList.size() > 0) {
                    imageUrl = JSONObject.fromObject(imgList.toArray()[0]).get("pictureUrl").toString().replace("\"", "");
                    imageUrl = Fields.SUNING_PC_IMAGE_2 + imageUrl;
                    inform.setGoods_pic_url(imageUrl);
                }

            }
            //店铺
            if (shopName.contains(Fields.SUNING_SELF_SUPPORT)) {
                inform.setPlatform_shopname(Fields.SUNING_SUPERMARKET);
            }
            //不再抓取评论数
//            //获取商品评论数量
//            String commenturl = Fields.SUNING_COMMENT.replace("0000000000", parameter.getCrawKeywordsInfo().getPlatform_shopid()).replace(Fields.PRODUCTID, parameter.getEgoodsId());
//            parameter.setItemUrl_1(commenturl);
//            String StringItem = getRequest(parameter);
//            if (StringItem.toString().contains("totalCount")) {
//                try {
//                    JSONObject json = JSONObject.fromObject(StringItem);
//                    JSONArray array = json.getJSONArray("reviewCounts");
//                    if (array != null && array.size() > 0) {
//                        JSONObject object = JSONObject.fromObject(array.toArray()[0]);
//                        inform.setTtl_comment_num(object.getString("totalCount"));
//                        if (Validation.isEmpty(priceInfo.getTo_use_amount())) {
//                            priceInfo.setTo_use_amount(object.getString("totalCount"));
//                        }
//                    }
//                } catch (Exception e) {
//                    logger.error("请求商品评论总数发生异常：error{}", e);
//                }

//            }
            //商品库存地址
            if(StringUtils.isNotBlank(parameter.getDeliveryPlace().getDelivery_place_code())) {
            	 inform.setDelivery_place(parameter.getDeliveryPlace().getDelivery_place_name());
            }
           
            //是不包邮
            inform.setDelivery_info(Fields.DONTPACK_MAIL);
            //商品链接
            inform.setGoods_url(parameter.getItemUtl_2());
            //客户端
            priceInfo.setChannel(Fields.CLIENT_MOBILE);
            //商品Sku
            priceInfo.setSKUid(parameter.getEgoodsId());

            map.put("vendor", vendor);
            map.put("priceType", priceType);
            map.put("vendorType", vendorType);
            map.put("platform_shoptype", proprietary);
            map.put("currentPrice", priceInfo.getCurrent_price());
            //获取促销
            String promotionMessage = promotionFromMobile(map, parameter);
            if (StringUtils.isNotEmpty(promotionMessage)) {
                promotion.append(promotionMessage);
            }
            //商品促销优惠卷
            String promotionData = promotionMessage(map, parameter);
            if (StringUtils.isNotEmpty(promotionData)) {
                promotion.append(promotionData);
            }
            //插入促销信息
            priceInfo.setPromotion(promotion.toString());
            //商品详情
            if (StringUtils.isEmpty(parameter.getCrawKeywordsInfo().getPlatform_shopid())) {
                inform.setPlatform_shopid("0000000000");
            } else {
                inform.setPlatform_shopid(parameter.getCrawKeywordsInfo().getPlatform_shopid());
            }


            inform.setGoods_url(parameter.getItemUtl_2().replace("0000000000", parameter.getCrawKeywordsInfo().getPlatform_shopid()));

            /**封装数据 详情**/
            insertItemList.addAll(list(inform));

            //价格

            priceInfo.setChannel(Fields.CLIENT_MOBILE);
            /**封装数据 价格**/
            insertPriceListApp.addAll(list(priceInfo));

        }
        //pc价格
        if (parameter.getListjobName().get(0).getClient().equalsIgnoreCase(Fields.ALL)) {
            if (Validation.isEmpty(vendorType)) {
                vendor = "0000000000";
            } else if (Validation.isEmpty(vendor)) {
                vendor = vendorType;
            }
            map.put("vendor", vendor);
            try {
                Map<String, String> dataPcPrice = crawlPricePc(map, parameter);
                //商品原价
                priceInfo.setOriginal_price(dataPcPrice.get("originalPrice"));
                //商品现价
                priceInfo.setCurrent_price(dataPcPrice.get("currentPrice"));
                priceInfo.setChannel(Fields.CLIENT_PC);
                   
                /**封装数据 价格**/
                insertPriceListApp.addAll(list(priceInfo));
            } catch (Exception e) {
                log.error("==>>解析苏宁PC端价格发生异常：" + e.getMessage() + "<<==");
                e.printStackTrace();
            }
        }
        //Storage 4 单条插入  1，批量插入
        if (parameter.getListjobName().get(0).getStorage().equals("4")) {
            //商品详情
            if (StringUtils.isNotEmpty(jdPrice)) {
                getProductSave(insertItemList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_INFO);
                if (insertPriceListPC != null && insertPriceListPC.size() > 0) {
                    insertPriceListApp.addAll(insertPriceListPC);
                }
                getProductSave(insertPriceListApp, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PRICE_INFO);
            }
        } else {
            //详情
            map.put("itemList", insertItemList);
            //价格
            if (insertPriceListPC != null && insertPriceListPC.size() > 0) {
                insertPriceListApp.addAll(insertPriceListPC);
            }
            map.put("itemPriceList", insertPriceListApp);
        }
        //获取商品的规格属性
        String hour = parameter.getMap().get("hour").toString();
        String week = parameter.getMap().get("week").toString();
        String dateWeek = parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[0];
        if (dateWeek.contains(week)) {
            String dataTime = parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[1];
            if (dataTime.contains(hour)) {
                List<Map<String, Object>> attributesList = getSpecificationAttributes(parameter);
                map.put("attributesList", attributesList);
            }
        }
        return map;
    }

    //pc端数据
    public Map<String, String> crawlPricePc(Map<String, Object> map, Parameter parameter) {
        Map<String, String> result = new HashMap<String, String>(10);
        String originalPrice = "";
        String currentPrice = "";
        String invStatus = "";
        StringBuffer buf = new StringBuffer();
        parameter.setCoding("utf-8");
        String vendor = map.get("vendor").toString();
        if (parameter.getEgoodsId().toString().length() > 10) {
            for (int i = parameter.getEgoodsId().toString().length(); i < 18; i++) {
                buf.append("0");
            }
        }
        parameter.setItemUrl_1(Fields.SUNING_PC_URL + buf.toString() + parameter.getEgoodsId() + "_" + buf.toString() + parameter.getEgoodsId() + "_" + vendor + "_20_021_0210101_313004_1000267_9264_12113_Z001___R9000373_1.14_0_0010114183___00019E499___.html?callback=pcData&_=" + System.currentTimeMillis());
        //请求数据
        String itemPage = getRequest(parameter);
        String temp = itemPage.substring(itemPage.indexOf("{"), itemPage.lastIndexOf("}") + 1);
        JSONObject currPriceJson = JSONObject.fromObject(temp);
        JSONArray arry = currPriceJson.getJSONObject("data").getJSONObject("price").getJSONArray("saleInfo");
        if (arry != null && arry.size() > 0) {
            JSONObject json = JSONObject.fromObject(arry.toArray()[0].toString());
            String refPrice = json.get("refPrice").toString();
            originalPrice = json.get("netPrice").toString();
            currentPrice = json.get("promotionPrice").toString();
            if ("3".equals(invStatus) && Validation.isEmpty(refPrice) && Validation.isEmpty(currentPrice)) {
                originalPrice = "暂无报价";
                currentPrice = "暂无报价";
            }
        }

        result.put("originalPrice", originalPrice);
        result.put("currentPrice", currentPrice);
        return result;

    }

    /**
     * 获取商品满减
     *
     * @return
     * @author jack.zhao
     * @data 2020/4/2 15:39
     */
    public String promotionFromMobile(Map<String, Object> map, Parameter parameter) {
        String currentPrice_mobile = map.get("currentPrice").toString();
        StringBuilder buffer = new StringBuilder();
        String vendorType = map.get("vendorType").toString();
        String vendor = map.get("vendor").toString();
        if (Validation.isEmpty(vendorType)) {
            vendor = "0000000000";
        } else if (Validation.isEmpty(vendor)) {
            vendor = vendorType;
        }
        parameter.setItemUrl_1(Fields.SUNING_APP_URL_PRICE + parameter.getEgoodsId() + "_" + vendor + "_010_0100100_5_R9000372_10051_" + currentPrice_mobile + "_0_10__DH53_0010114183_" + currentPrice_mobile + "_0_0000000" + parameter.getEgoodsId() + "_999_0_E499__00019E499_313007_3_0_20190115_0030000244CH9F0_1000000___313005_01________2_1__.html?callback=detailCommonLogicpriceType");

        try {
            //请求数据
            String urlContent = getRequest(parameter);

            String activity = StringHelper.getResultByReg(urlContent, "activityList\":\\[(.+?)\\]");

            if (Validation.isNull(activity)) {
                return "";
            }
            String item = urlContent.substring(urlContent.indexOf("(") + 1, urlContent.lastIndexOf(")"));
            JSONObject json = JSONObject.fromObject(item);
            if (json.toString().contains("activityList")) {
                if (!json.toString().contains("\"activityList\":null,\"")) {
                    JSONArray array = json.getJSONObject("data").getJSONObject("salesPromotion").getJSONArray("activityList");
                    for (int i = 0; i < array.size(); i++) {
                        String promotion = "";
                        JSONObject activ = JSONObject.fromObject(array.toArray()[i].toString());
                        if (!"{}".equals(activ.get("bonusListInfo").toString())) {
                            JSONArray bonusListInfo = activ.getJSONArray("bonusListInfo");
                            if (bonusListInfo.size() > 0) {
                                for (int k = 0; k < bonusListInfo.size(); k++) {
                                    JSONObject bonus = JSONObject.fromObject(bonusListInfo.toArray()[k].toString());

                                    if (bonus.toString().contains("bounsAmount")) {
                                        promotion = bonus.getString("bounsAmount") + Fields.DISCOUNT + bonus.getString("preferentialDistinct");
                                        if (activ.toString().contains("memberAttLimit")) {
                                            promotion = activ.getString("memberAttLimit") + ":" + promotion;
                                        }
                                        if (StringUtils.isNotEmpty(activ.getString("couponStartTime"))) {
                                            promotion = promotion + ",有效时间：" + activ.getString("couponStartTime") + "-" + activ.getString("couponEndTime");
                                        }
                                        buffer.append(promotion + "&&");
                                    } else {
                                        if (StringUtils.isNotEmpty(activ.getString("couponStartTime"))) {
                                            promotion = ",有效时间：" + activ.getString("couponStartTime") + "-" + activ.getString("couponEndTime");
                                        }
                                        buffer.append(bonus.getString("preferentialDistinct") + promotion + "&&");
                                    }
                                }
                            }
                        } else {
                            if (StringUtils.isNotEmpty(activ.getString("couponStartTime"))) {
                                promotion = ",有效时间：" + activ.getString("couponStartTime") + "-" + activ.getString("couponEndTime");
                            }
                            if (activ.toString().contains("memberAttLimit")) {
                                buffer.append(activ.getString("memberAttLimit") + ":" + activ.getString("couponPromotionLabel") + promotion + "&&");
                            } else {
                                if (StringUtils.isNotEmpty(activ.getString("couponPromotionLabel"))) {
                                    buffer.append(activ.getString("couponPromotionLabel") + promotion + "&&");
                                } else if (StringUtils.isNotEmpty(activ.getString("activityDescription"))) {
                                    buffer.append(activ.getString("activityDescription") + promotion + "&&");
                                }

                            }

                        }
                    }

                }
            }
            if (json.toString().contains("shoppingAllowance")) {
                try {
                    String title = json.getJSONObject("data").getJSONObject("salesPromotion").getJSONObject("shoppingAllowance").getString("activityName");
                    String details = json.getJSONObject("data").getJSONObject("salesPromotion").getJSONObject("shoppingAllowance").getString("unSatisfiedText");
                    String time = json.getJSONObject("data").getJSONObject("salesPromotion").getJSONObject("shoppingAllowance").getString("startTime");
                    buffer.append(title + ":" + details + ",使用时间:" + time);
                } catch (Exception e) {
                    log.error("==>>请求苏宁促销发生异常：" + e.getMessage() + "<<==");
                    e.printStackTrace();
                }

            }
        } catch (Exception e) {
            log.error("==>>请求苏宁促销发生异常：" + e.getMessage() + "<<==");
            e.printStackTrace();
        }
        return buffer.toString();
    }

    /**
     * 获取商品优惠卷
     *
     * @return
     * @author jack.zhao
     * @data 2020/4/2 15:39
     */
    public String promotionMessage(Map<String, Object> map, Parameter parameter) {
        StringBuilder buffer = new StringBuilder();
        parameter.setItemUrl_1(Fields.SUNING_PC_PROMOTION_2 + parameter.getEgoodsId() + "_" + parameter.getCrawKeywordsInfo().getPlatform_shopid() + "_021_0210499_140.00_0_11_1_3,30_pds__1____86_promInfoCallback.vhtm?callback=promInfoCallback");
        try {
            String message = getRequest(parameter);
            if (StringUtils.isNotEmpty(message)) {
                String item = message.substring(message.indexOf("(") + 1, message.lastIndexOf(")"));
                JSONObject json = JSONObject.fromObject(item);
                JSONArray array = json.getJSONArray("promotions");
                for (int k = 0; k < array.size(); k++) {
                    JSONObject promotions = JSONObject.fromObject(array.toArray()[k].toString());
                    if (StringUtils.isNotBlank(promotions.getString("couponStartTime"))) {
                        buffer.append(promotions.getString("activityDesc") + ",有效时间：" + promotions.getString("couponStartTime") + "-" + promotions.getString("couponEndTime") + "&&");
                    } else {
                        if (StringUtils.isNotBlank(promotions.getString("activityDesc"))) {
                            buffer.append(promotions.getString("activityDesc") + "&&");
                        }
                    }

                }
            }

        } catch (Exception e) {
            log.error("==>>获取商品优惠卷发生异常：" + e.getMessage() + "<<==");
            e.printStackTrace();
        }

        return buffer.toString();
    }

    /*获取商品的规格属性*/
	public List<Map<String, Object>> getSpecificationAttributes(Parameter parameter) {

        List<Map<String, Object>> insertList = Lists.newArrayList();
        parameter.setItemUrl_1(parameter.getItemUtl_2().replace("m.suning.com/product", "product.suning.com"));
        try {
            String attributesItem = getRequest(parameter);

            if (attributesItem.contains("pro-para-tbl")) {
                Document document = Jsoup.parse(attributesItem);
                List<Element> list = document.getElementsByClass("pro-para-tbl").select("tr");
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).toString().contains("parametercode")) {
                        if (StringUtils.isNotBlank(list.get(i).getElementsByClass("name-inner").text())) {
                           
                            String key = list.get(i).getElementsByClass("name-inner").text();
                            String value = list.get(i).getElementsByClass("val").text();

                            /**封装数据 属性**/
                            insertList.addAll(list(attributeInfo(key, value, parameter)));
                        }

                    }

                }
            }

            //Storage 1 批量插入数据 4 单条插入数据
            if (parameter.getListjobName().get(0).getStorage().equalsIgnoreCase("4")) {
                if (insertList != null && insertList.size() > 0) {
                    try {
                        getProductSave(insertList, parameter.getListjobName().get(0).getDatabases(), Fields.CRAW_GOODS_ATTRIBUTE_INFO);
                    } catch (Exception e) {
                        logger.error("==>>插入商品属性失败失败, error:{}<<==", e);
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            log.error("==>>获取商品规格发生异常：" + e.getMessage() + "<<==");
        }
        return insertList;

    }


    //////////////////////////////////////////////////////关键词搜索///////////////////////////////////////////////////////////////////////////////////////////

    /***
     * 分页请求数据
     * @throws UnsupportedEncodingException
     **/
    public Map<String, String> productPage(Map<String, String> map, CrawKeywordsInfo crawKeywordsInfo, int page) {
        String StringMessage = "";
        String disable = "";
        String codeCookie = map.get("codeCookie");
        String productUrl = crawKeywordsInfo.getCust_keyword_url();
        String productRef = crawKeywordsInfo.getCust_keyword_url();
        try {
            if (productUrl.contains("search.suning.com")) {
                String code = "021";
                if (StringUtil.isNotEmpty(codeCookie)) {
                    code = StringHelper.getResultByReg(codeCookie, "cityCode=([0-9]+);");
                    map.put("cookie", codeCookie);
                }
                for (int kk = 0; kk < 2; kk++) {
                    String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
                    String brand = "";
                    if (org.apache.commons.lang3.StringUtils.isNoneBlank(crawKeywordsInfo.getCust_keyword_brand())) {
                        String bran = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_brand(), "UTF-8");
                        brand = "&hf=brand_Name_FacetAll:" + bran;
                    }
                    map.put("url", productUrl.replace("KEYWORD", keyWord).replace("PAGE", String.valueOf(page - 1)).replace("CITY", code) + brand);
                    if (kk == 1) {
                        map.put("url", productUrl.replace("KEYWORD", keyWord).replace("PAGE", String.valueOf(page - 1)).replace("CITY", code) + "&paging=1&sub=0" + brand);
                    }
                    map.put("urlRef", productRef + keyWord + "/" + brand);
                    map.put("count", String.valueOf(page));
                    logger.info("==>>" + map.get("url") + "[[苏宁当前商品第：" + page + "页]]<<==");
                    StringMessage = suningDataCrawlService.httpClientService.interfaceSwitch(map.get("ip"), map);//请求数据
                    disable = suningDataCrawlService.search_DataCrawlService.ParseItemPage(StringMessage, map.get("database"), crawKeywordsInfo, map);//解析数据
                    if (disable.contains(Fields.DISABLE)) {
                        map.put(Fields.DISABLE, Fields.DISABLE);
                        return map;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        map.put(Fields.DISABLE, disable);
        return map;

    }


}
