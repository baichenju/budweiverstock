package com.eddc.service.impl.crawl;
import com.eddc.model.Craw_goods_comment_Info;
import com.eddc.model.Parameter;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class CommentParticipleDataCrawlService {
    @Autowired
    HttpClientService httpClientService;
    @Autowired
    CrawlerPublicClassService crawlerPublicClassService;
    @Autowired
    private SearchDataCrawlService search_DataCrawlService;

    /* 获取手机详情信息
     */
    @SuppressWarnings("unchecked")
    public Map<String, Object> itemAppPage(String itemPage, Parameter parameter) {
        Map<String, Object> item = new HashMap<String, Object>(10);
        Craw_goods_comment_Info infoComment = new Craw_goods_comment_Info();
        Map<String, Object> insertItem = new HashMap<String, Object>();
        List<Map<String, Object>> insertItems = Lists.newArrayList();
        try {
            if (parameter.getListjobName().get(0).getPlatform().equalsIgnoreCase(Fields.PLATFORM_JD)) {
                if (itemPage.contains("productCommentSummary")) {
                    infoComment = jdAnalysis(itemPage, parameter);
                }
            } else if (parameter.getListjobName().get(0).getPlatform().equalsIgnoreCase(Fields.PLATFORM_SUNING)) {
                infoComment = suNingAnalysis(itemPage, parameter);

            } else if (parameter.getListjobName().get(0).getPlatform().equalsIgnoreCase(Fields.PLATFORM_TMALL_EN) ||
                    parameter.getListjobName().get(0).getPlatform().equalsIgnoreCase(Fields.PLATFORM_TAOBAO_EN)) {
                infoComment = tmallTaoBaoAnalysis(itemPage, parameter);
            }


            if (StringUtils.isNotEmpty(infoComment.getTtl_comment_num())) {
                //开始插入商品详情信息
                insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(infoComment);
                insertItems.add(insertItem);
                search_DataCrawlService.insertIntoData(insertItems, parameter.getListjobName().get(0).getDatabases(), Fields.CRAW_GOODS_COMMENT_INFO);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("【解析" + parameter.getListjobName().get(0).getPlatform() + "数据发生异常：{}】", e);
        }
        //item.put("Craw_goods_Info", info);
        return item;
    }

    public Craw_goods_comment_Info jdAnalysis(String itemPage, Parameter parameter) {
        Craw_goods_comment_Info infoComment = new Craw_goods_comment_Info();
        StringBuffer bufer = new StringBuffer();
        infoComment.setBatch_time(parameter.getBatch_time());
        infoComment.setEgoodsId(parameter.getEgoodsId());
        infoComment.setGoodsId(parameter.getGoodsId());
        infoComment.setComment_status(1);
        infoComment.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
        infoComment.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
        infoComment.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
        infoComment.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());
        String item = itemPage.substring(itemPage.indexOf("{"), itemPage.lastIndexOf("}") + 1);
        JSONObject currPriceJson = JSONObject.fromObject(item);
        //总评论数
        String commentCount = currPriceJson.getJSONObject("result").getJSONObject("productCommentSummary").getString("CommentCount").replace("+", "");
        infoComment.setTtl_comment_num(commentCount);
        //好评
        String goodCountStr = currPriceJson.getJSONObject("result").getJSONObject("productCommentSummary").getString("GoodCount");
        infoComment.setPos_comment_num(goodCountStr);
        //中评
        String generalCount = currPriceJson.getJSONObject("result").getJSONObject("productCommentSummary").getString("GeneralCount");
        infoComment.setNeu_comment_num(generalCount);
        // 差评
        String poorCount = currPriceJson.getJSONObject("result").getJSONObject("productCommentSummary").getString("PoorCount");
        infoComment.setNeg_comment_num(poorCount);
        // 有图
        String showCountStr = currPriceJson.getJSONObject("result").getJSONObject("productCommentSummary").getString("ShowCount");
        //视频晒单
        String videoCount = currPriceJson.getJSONObject("result").getJSONObject("productCommentSummary").getString("VideoCount");
        bufer.append("好评(" + goodCountStr + ")&&" + "中评(" + generalCount + ")&&" + "差评(" + poorCount + ")&&");
        bufer.append("有图(" + showCountStr + ")&&" + "视频晒单(" + videoCount + ")&&");
        JSONArray jsonArray = currPriceJson.getJSONObject("result").getJSONArray("hotCommentTagStatistics");
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject json = JSONObject.fromObject(jsonArray.toArray()[i].toString());
            bufer.append(json.getString("name") + "(" + json.getString("count") + ")&&");
        }
        infoComment.setComment_tags(bufer.toString());
        //获取jd店铺评分
        parameter.setItemUrl_1(parameter.getItemUtl_3());
        String store = httpClientService.interfaceSwitch(parameter);//请求数据
        if (store.contains("userEvaluateScore")) {
            StringBuffer storeBufer = new StringBuffer();
            JSONObject storeJson = JSONObject.fromObject(store);
            JSONArray storeArray = storeJson.getJSONArray("data");
            for (int k = 0; k < storeArray.size(); k++) {
                JSONObject json = JSONObject.fromObject(storeArray.toArray()[k].toString());
                storeBufer.append("评价(" + json.getString("userEvaluateScore") + ")&&");
                storeBufer.append("物流(" + json.getString("logisticsLvyueScore") + ")&&");
                storeBufer.append("售后(" + json.getString("afterServiceScore") + ")&&");
            }
            infoComment.setComment_shopscores(storeBufer.toString());
        }
        return infoComment;
    }

    public Craw_goods_comment_Info suNingAnalysis(String itemPage, Parameter parameter) {
        Craw_goods_comment_Info comment = new Craw_goods_comment_Info();
        StringBuffer commBufer = new StringBuffer();
        comment.setBatch_time(parameter.getBatch_time());
        comment.setEgoodsId(parameter.getEgoodsId());
        comment.setGoodsId(parameter.getGoodsId());
        comment.setComment_status(1);
        comment.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
        comment.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
        comment.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
        comment.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());
        String item = itemPage.substring(itemPage.indexOf("{"), itemPage.lastIndexOf("}") + 1);
        JSONObject currPriceJson = JSONObject.fromObject(item);
        if (itemPage.contains("commodityLabelCountList")) {
            JSONArray array = currPriceJson.getJSONArray("commodityLabelCountList");
            for (int i = 0; i < array.size(); i++) {
                JSONObject commentJson = JSONObject.fromObject(array.toArray()[i].toString());
                commBufer.append(commentJson.getString("labelName") + "(" + commentJson.getString("labelCnt") + ")&&");
            }
            comment.setComment_tags(commBufer.toString());
        }
		/************************************获取苏宁店铺评分***************************************************/
        if (StringUtils.isNotEmpty(parameter.getItemUtl_3())) {
            parameter.setItemUrl_1(parameter.getItemUtl_3());
            String store = httpClientService.interfaceSwitch(parameter);//请求数据
            if (store.contains("dataList")) {
                StringBuffer storeComment = new StringBuffer();
                String itemStore = store.substring(store.indexOf("{"), store.lastIndexOf("}") + 1);
                JSONObject storeJson = JSONObject.fromObject(itemStore);
                JSONArray array = storeJson.getJSONArray("dataList");
                if (array != null && array.size() > 0) {
                    JSONObject json = JSONObject.fromObject(array.toArray()[0].toString());
                    JSONArray storeArray = json.getJSONArray("bigcatelist");
                    for (int k = 0; k < storeArray.size(); k++) {
                        JSONObject arrayData = JSONObject.fromObject(storeArray.toArray()[k].toString());
                        storeComment.append(arrayData.getString("parentIndexName") + "(" + arrayData.getString("parentIndexScore") + ")&&");
                    }
                    comment.setComment_shopscores(storeComment.toString());
                }
            }
        }
        /**************************************获取评论好中差*************************************************/
		String url = "https://review.suning.com/ajax/review_count/general--000000000" + parameter.getEgoodsId() + "-0000000000-----.htm";
		if (!parameter.getCrawKeywordsInfo().getPlatform_shopid().equalsIgnoreCase("000000000")) {
			url = "https://review.suning.com/ajax/review_count/general--0000000" + parameter.getEgoodsId() + "-" + parameter.getCrawKeywordsInfo().getPlatform_shopid() + "-----.htm";
		}
		parameter.setItemUrl_1(url);
		String reviewCounts = httpClientService.interfaceSwitch(parameter);//请求数据
		if (reviewCounts.contains("reviewCounts")) {
			JSONObject storeJson = JSONObject.fromObject(reviewCounts);
			JSONArray array = storeJson.getJSONArray("reviewCounts");
			if(array!=null&& array.size()>0){
				for (int k = 0; k < array.size(); k++) {
					JSONObject object=JSONObject.fromObject(array.toArray()[k].toString());
					comment.setTtl_comment_num(object.getString("totalCount"));
					int good_num=object.getInt("fiveStarCount")+object.getInt("fourStarCount");
					comment.setPos_comment_num(String.valueOf(good_num));
					int neu_num=object.getInt("threeStarCount")+object.getInt("twoStarCount");
					comment.setNeu_comment_num(String.valueOf(neu_num));
					comment.setNeg_comment_num(object.getString("oneStarCount"));
				}
			}
		}

        return comment;
    }

    public Craw_goods_comment_Info tmallTaoBaoAnalysis(String itemPage, Parameter parameter) {
        Craw_goods_comment_Info info = new Craw_goods_comment_Info();
        StringBuffer commBufer = new StringBuffer();
        info.setBatch_time(parameter.getBatch_time());
        info.setEgoodsId(parameter.getEgoodsId());
        info.setGoodsId(parameter.getGoodsId());
        info.setComment_status(1);
        info.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
        info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
        info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
        info.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());
        if (itemPage.contains("showPicRadio")) {
            String item = itemPage.substring(itemPage.indexOf("{"), itemPage.lastIndexOf("}") + 1);
            JSONObject currPriceJson = JSONObject.fromObject(item);
            JSONArray array = currPriceJson.getJSONObject("data").getJSONArray("impress");
            if (array != null && array.size() > 0) {
                for (int i = 0; i < array.size(); i++) {
                    JSONObject json = JSONObject.fromObject(array.toArray()[i].toString());
                    commBufer.append(json.getString("title") + "(" + json.getString("count") + ")," + json.getString("attribute") + "&&");
                }
            }
            info.setComment_tags(commBufer.toString());
            info.setTtl_comment_num(currPriceJson.getJSONObject("data").getJSONObject("count").getString("total"));
            info.setPos_comment_num(currPriceJson.getJSONObject("data").getJSONObject("count").getString("good"));
            info.setNeu_comment_num(currPriceJson.getJSONObject("data").getJSONObject("count").getString("normal"));
            info.setNeg_comment_num(currPriceJson.getJSONObject("data").getJSONObject("count").getString("bad"));
        }
        return info;

    }


}
