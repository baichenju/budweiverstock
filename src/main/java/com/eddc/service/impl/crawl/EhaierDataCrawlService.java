package com.eddc.service.impl.crawl;

import com.alibaba.fastjson.JSONObject;
import com.eddc.model.*;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.WhetherShelves;
import com.google.common.collect.Lists;
import net.sf.json.JSONArray;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @author jack
 */
@Service("ehaierDataCrawlService")
public class EhaierDataCrawlService extends WhetherShelves{
	private static Logger logger=LoggerFactory.getLogger(EhaierDataCrawlService.class);
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	SearchDataCrawlService search_DataCrawlService;

	/**请求商品"*/
	public Map<String, Object> parameter ( Parameter parameter){ 
		Map<String, Object> item=new HashMap<String, Object>(10);
		logger.info("==>>ehaier台开始解析数据,当前解析的是第：{} 个商品<<==",parameter.getSum());

		parameter.setItemUrl_1("https://detail.ehaier.com/item/sgProduct/purchase/"+parameter.getEgoodsId()+".json?productId="+parameter.getEgoodsId()+"&channel=&openId=&storeId=");
		parameter.setItemUtl_2("https://m.ehaier.com/sgmobile/goodsDetail?productId="+parameter.getEgoodsId());
		String  StringMessage=httpClientService.interfaceSwitch(parameter);//请求数据
		String status =parseGoodsStatusByItemPage(StringMessage);//判断商品是否下架
		try {
			if(Integer.valueOf(status)==Integer.valueOf(Fields.STATUS_OFF) ){
				logger.info("==>>"+parameter.getEgoodsId() +parameter.getListjobName().get(0).getPlatform()+"当前商品已经下架,插入数据库 当前是第:" + parameter.getSum() + "商品<<==");
				crawlerPublicClassService.soldOutStatus(status,parameter.getListjobName().get(0).getDatabases(),parameter);
				return item; 
			}else{
				item=itemAppPage( StringMessage,  parameter);
			}
		} catch (Exception e) {
			logger.error("海尔台开始解析数据,当前解析的是第：{} 个商品 请求数据异常： error:{}<<==",parameter.getSum(),e.getMessage());
			e.printStackTrace();
		}
		return item;
	}

	/**数据解析
	 * @throws Exception */
	@SuppressWarnings("unchecked")
	public Map<String, Object> itemAppPage (String item,Parameter parameter) throws Exception{
		Map<String, Object> map=new HashMap<String, Object>(10);
		//价格
		Map<String, Object> insertPriceMap = new HashMap<String, Object>();
		List<Map<String, Object>> insertPriceList = Lists.newArrayList();

		List<Map<String,Object>> sepList= Lists.newArrayList();
		//详情
		Map<String, Object> insertItemMap = new HashMap<String, Object>();
		List<Map<String, Object>> insertItemList = Lists.newArrayList();
		Craw_goods_InfoVO infovo=new Craw_goods_InfoVO();
		Craw_goods_Price_Info priceInfo = new Craw_goods_Price_Info();
		if(item.contains("storeId")) {
			JSONObject json=JSONObject.parseObject(item.toString());
			//获取商品图片
			JSONArray jsonArray = JSONArray.fromObject(json.getJSONObject("data").get("swiperImgs").toString());
			for (int i = 0; i < jsonArray.size(); i++) {
				infovo.setGoods_pic_url(jsonArray.getString(i));
				break;
			}	
			//店铺id
			infovo.setPlatform_shopid(json.getJSONObject("data").getString("storeId"));
			//商品名称
			infovo.setPlatform_goods_name(json.getJSONObject("data").getJSONObject("products").getString("productFullName"));
			//商品skuId
			priceInfo.setSKUid(json.getJSONObject("data").getJSONObject("products").getString("sku"));
			//月销量
			infovo.setSale_qty(json.getJSONObject("data").getJSONObject("products").getString("saleNum"));
			
			parameter.setItemUrl_1("https://detail.ehaier.com/item/sgProduct/product/dynamic.json?prodId="+parameter.getEgoodsId()+"&memberStoreId=&number=1&provinceId=16&cityId=173&regionId=2450&streetId=12036596&skku=&sku="+priceInfo.getSKUid());
			String priceMessage = httpClientService.interfaceSwitch(parameter);
			if(priceMessage.contains("stockType")) {
				JSONObject jsonPrice=JSONObject.parseObject(priceMessage.toString());
				JSONObject stock = jsonPrice.getJSONObject("data").getJSONObject("sgStockInfo");
				if(stock!=null)  {
					String stockType=jsonPrice.getJSONObject("data").getJSONObject("sgStockInfo").getString("stockType");
					if(StringUtils.isNotBlank(stockType)) {
						if(stockType.contains("STORE")) {
							stockType="无货";
						}else {
							stockType="有货";
						}	
					}else {
						stockType="无货";
					}
					priceInfo.setInventory(stockType);
					infovo.setInventory(stockType);
				}				
				//商品价格
				String priceData=jsonPrice.getJSONObject("data").getJSONObject("priceInfo").getString("actualPrice");
				if(StringUtils.isEmpty(priceData)) {
					infovo.setGoods_status(Fields.STATUS_COUNT);
				}
				priceInfo.setCurrent_price(priceData);
				priceInfo.setOriginal_price(priceData);
				//店铺
				if(jsonPrice.getJSONObject("data").containsKey("storeInfo")) {
					JSONObject obj = jsonPrice.getJSONObject("data").getJSONObject("storeInfo");
					if(obj!=null) {
						infovo.setPlatform_sellername(jsonPrice.getJSONObject("data").getJSONObject("storeInfo").getString("o2OStoreName"));
						infovo.setPlatform_shopname(jsonPrice.getJSONObject("data").getJSONObject("storeInfo").getString("o2OStoreName"));
						infovo.setPlatform_shopid(jsonPrice.getJSONObject("data").getJSONObject("storeInfo").getString("o2oStoreId"));
						infovo.setPlatform_sellerid(jsonPrice.getJSONObject("data").getJSONObject("storeInfo").getString("o2oStoreId"));	
					}else {
						String store="海尔智家";
						infovo.setPlatform_sellername(store);
						infovo.setPlatform_shopname(store);
					}	
				}
				
			}
			
		}

		infovo.setBatch_time(parameter.getBatch_time());
		infovo.setGoods_url(parameter.getItemUtl_2());
		infovo.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
		infovo.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
		infovo.setGoods_status(1);//状态
		infovo.setEgoodsId(parameter.getEgoodsId());
		infovo.setGoodsId(parameter.getGoodsId());
		infovo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		infovo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
		infovo.setPlatform_shoptype(Fields.YES_PROPRIETARY);

		//价格封装
		priceInfo.setBatch_time(parameter.getBatch_time());
		priceInfo.setGoodsid(parameter.getGoodsId());
		priceInfo.setSKUid(parameter.getEgoodsId());

		priceInfo.setCust_keyword_id(parameter.getCrawKeywordsInfo().getCust_keyword_id());
		priceInfo.setChannel(Fields.CLIENT_MOBILE);
		priceInfo.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
		priceInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		priceInfo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
         
		
		//商品规格
		sepList=getSpecificationAttributes(parameter);
		
		insertItemMap=BeanMapUtil.convertBean2MapWithUnderscoreName(infovo);
		insertItemList.add(insertItemMap);
		search_DataCrawlService.insertIntoData(insertItemList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_INFO);
		//商品详情
		if(StringUtils.isNotBlank(priceInfo.getOriginal_price())){

			insertPriceMap=BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
			insertPriceList.add(insertPriceMap);
			
	
			if(sepList!=null && sepList.size()>0){
				search_DataCrawlService.insertIntoData(sepList,parameter.getListjobName().get(0).getDatabases(),Fields.CRAW_GOODS_ATTRIBUTE_INFO);
			}
			
			search_DataCrawlService.insertIntoData(insertPriceList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PRICE_INFO);	
		}
		
		
	
		return map;
	}
	
	//获取商品的规格属性
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>>  getSpecificationAttributes(Parameter parameter) {

		List<Map<String,Object>> insertList= Lists.newArrayList();
		String item="暂无";
		parameter.setItemUrl_1("https://detail.ehaier.com/item/purchase/specifications.json?productId="+parameter.getEgoodsId());
		try {
			item = httpClientService.interfaceSwitch(parameter);
			if(item.contains("productAttrs")) {
				JSONObject json = JSONObject.parseObject(item);
				
				JSONArray jsonArray = JSONArray.fromObject(json.getJSONObject("data").getJSONArray("productAttrs"));
					for(int k=0;k<jsonArray.size();k++) {
						JSONObject atts=JSONObject.parseObject(jsonArray.toArray()[k].toString());
						Craw_goods_attribute_Info info =new Craw_goods_attribute_Info();
						Map<String,Object> insertItem=new HashMap<String,Object>();
						info.setAttri_value(atts.getString("attrValue"));//值
						info.setCraw_attri_cn(atts.getString("attrName"));//key
						info.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
						info.setEgoodsid(parameter.getEgoodsId());
						info.setGoodsid(parameter.getGoodsId());
						info.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());
						info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
						info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
						info.setBatch_time(parameter.getBatch_time());
						insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(info);
						insertList.add(insertItem);  							
					}
				
			}
		}catch(Exception e) {
			logger.error("==>>海尔获取商品规格发生异常：" + e.getMessage() + "<<== \n"+item);
		}
		return insertList;

	}

	
}
