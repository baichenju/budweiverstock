package com.eddc.service.impl.crawl;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Parameter;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.UserAgentUtil;
import com.eddc.util.WhetherShelves;
import com.google.common.collect.Lists;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service("pddDataCrawlService")
public class PddDataCrawlService  extends WhetherShelves {
	private static Logger logger=LoggerFactory.getLogger(PddDataCrawlService.class);
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private SearchDataCrawlService search_DataCrawlService;

	/**请求商品"
	 * @throws InterruptedException */
	public Map<String, Object> parameter ( Parameter parameter) throws InterruptedException{ 
		Map<String, Object> item=new HashMap<String, Object>(10);
		logger.info("==>>拼多多台开始解析数据,当前解析的是第：{} 个商品<<==",parameter.getSum());
		Map<String,String> param = new HashMap<String,String>();
		Map<String, Object> paramData = new HashMap<>();
		//param.put("AccessToken",parameter.getPlace_code());
		param.put("Referer", parameter.getItemUtl_2());
		param.put("User-Agent",UserAgentUtil.getPCUserAgent());
		param.put("Host", " mobile.yangkeduo.com");
		String [] page=parameter.getSum().split("/");
		paramData.put("json", "{\"page\":"+page[0]+",\"size\":10,\"anti_content\":\"0aoAfxuUmy1ys9darRmvsSLPPPPCCSwwteTUbcQ-oRGSnlV5FI65M7k51OoUNaXocwIUSqpZwOWoPyIMr97mlW42a28QUP8DCz78HB4FzLgqpYRWhuRqSvqBoY9N2Yo15Y2GIbW9WpCBJApfeoPHlJcJ_A66ao3pTBkxwtcCJ0-spi_tD5P18UGsd4JZc9pkH4xbon7tvu7R1ZQbNvMIFLRID3mvyRbTLJGLN9UA2Ozv7bQFKQX5zP-GzC3M52RFUFAFHVvTul_ASbGtxNYJ1ZHKG2tJhHs4y5Vb6pXMfT-IfjqSHa_BOrbma6bDTv5I0CtH7znEXjYnuvY7OzGltM1XX5QVHhYuaZCZAfTCf7G91xiCvHSE5uO8DEHS1MEdBqDdciAq5Xd_qvvzgzVtr9koBjdatthBgp8t9bHpxd1okkhifLKgiIk1UJY6x3C1uuPAkUKGjhSAGImaaLXdm7tWFe6mdaFP-Bauw_FW5Ehazu0dmg_8VtbXQTOru5TnrwlMHWHZG-Ld_dpqeTYTjqOQVIjMj3tmpwVESNbUja0LPesF50eYZTH3TIzf9dDJCHkY4P3DEnnO4VDSNa8dqak5Q0CxzC3AfUlMWy0A8NT3OFAu3GON2i_CZ5DtdRQ1MNQ-3CPOQRJ7LZgrOU3Hzxugf8ofuN9u6kOj0LEAwbDQWtJF8_5d0sLV8KM8BLR9KwwafX\"}");
		//数据进行随机休眠
		int time=(int) (1000*(1+Math.random()*(10-1+1)));
		TimeUnit.MICROSECONDS.sleep(time);
		String  StringMessage=httpClientService.getOkHttpClient(paramData, param, parameter );//请求数据
		String status =parseGoodsStatusByItemPage(StringMessage);//判断商品是否下架
		try {
			if(Integer.valueOf(status)==Integer.valueOf(Fields.STATUS_OFF) ){
				logger.info("==>>"+parameter.getEgoodsId() +parameter.getListjobName().get(0).getPlatform()+"当前商品已经下架,插入数据库 当前是第:" + parameter.getSum() + "商品<<==");
				crawlerPublicClassService.soldOutStatus(status,parameter.getListjobName().get(0).getDatabases(),parameter);
				return item; 
			}else{
				logger.info("==>>当前商品抓取的是第："+page[0]+"页<<==");
				item=itemAppPage( StringMessage,  parameter);
			}
		} catch (Exception e) {
			logger.error("拼多多台开始解析数据,当前解析的是第：{} 个商品 请求数据异常： error:{}<<==",parameter.getSum(),e.getMessage());
			e.printStackTrace();
		}
		return item;
	}

	/* 获取手机详情信息
	 */ 
	@SuppressWarnings("unchecked")
	public Map<String, Object> itemAppPage(String itemPage, Parameter parameter) {
		Map<String, Object> item=new HashMap<String, Object>(10);

		//StringBuffer bufer=new StringBuffer();
		//Craw_goods_comment_Info comment=new Craw_goods_comment_Info();
		Map<String,Object> insertItem=new HashMap<String,Object>();
		//Map<String,Object> commentList=new HashMap<String,Object>();
		List<Map<String,Object>> insertItems = Lists.newArrayList();
		//List<Map<String,Object>>commentItems = Lists.newArrayList();
		Map<String,Object> insertPriceMap=new HashMap<String,Object>();
		List<Map<String,Object>> insertPriceList = Lists.newArrayList();

		try {
			if(itemPage.contains("goods_list") || itemPage.contains("list")) {
				JSONArray array =new JSONArray();
				JSONObject json=JSONObject.fromObject(itemPage);
				logger.info(json.toString());
				if(json.containsKey("goods_list")) {
					array=json.getJSONArray("goods_list");
				}else if(json.containsKey("list")) {
					array=json.getJSONArray("list");
				}

				for(int i=0;i<array.size();i++) {
					logger.info("==>>商品当前总个数："+array.size()+"<<==");
					Craw_goods_InfoVO info=new Craw_goods_InfoVO();
					Craw_goods_Price_Info price=new Craw_goods_Price_Info();
					info.setFeature1(Fields.COUNT_01);


					JSONObject object=JSONObject.fromObject(array.toArray()[i].toString());
					//商品id
					info.setEgoodsId(object.getString("goods_id"));

					parameter.setGoodsId(StringHelper.encryptByString(info.getEgoodsId()+info.getPlatform_name_en()));
					//商品连接
					info.setGoods_url("http://mobile.yangkeduo.com/goods.html?goods_id="+info.getEgoodsId());			
					//商品名称
					info.setPlatform_goods_name(object.getString("goods_name"));

					//商品图片
					info.setGoods_pic_url(object.getString("hd_url"));

					//销量
					info.setSale_qty(getNumeric(object.getString("sales_tip")));

					//店铺
                    if(object.containsKey("mall_info_vo")) {
                    	info.setPlatform_sellername(object.getJSONObject("mall_info_vo").getString("mall_name"));
    					info.setPlatform_shopname(object.getJSONObject("mall_info_vo").getString("mall_name"));
    					//店铺id
    					info.setPlatform_sellerid(object.getJSONObject("mall_info_vo").getString("mall_id"));	
                    }else if(object.containsKey("mall_name")){
                    	info.setPlatform_sellername(object.getString("mall_name"));
    					info.setPlatform_shopname(object.getString("mall_name"));
    					//店铺id
    					info.setPlatform_sellerid(object.getString("mall_id"));	
                    }
					

					//商品价格
					price.setCurrent_price(String.valueOf(object.getDouble("price")/100));
					//原价
					price.setOriginal_price(price.getCurrent_price());

					price.setGoodsid(parameter.getGoodsId());
					price.setChannel(Fields.CLIENT_MOBILE);
					price.setSKUid(info.getEgoodsId());
					price.setBatch_time(parameter.getBatch_time());
					price.setCust_keyword_id(parameter.getKeywordId());
					price.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
					price.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
					price.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());
					price.setSale_qty(info.getSale_qty());


					info.setBatch_time(parameter.getBatch_time());
					info.setGoods_status(Fields.STATUS_COUNT_1);
					info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
					info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
					info.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
					info.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());
					info.setPlatform_shoptype(Fields.PROPRIETARY);
					info.setGoodsId(parameter.getGoodsId());

					//促销
					if(object.getJSONObject("mall_info_vo").containsKey("mall_tag_list")) {
						JSONArray promotion=object.getJSONObject("mall_info_vo").getJSONArray("mall_tag_list"); 
						StringBuffer promotionMessage=new StringBuffer();
						for(int k=0;k<promotion.size();k++) {
							JSONObject prom=JSONObject.fromObject(promotion.toArray()[k].toString());

							promotionMessage.append(prom.getString("tag_desc")+"&&");
						}
						price.setPromotion(promotionMessage.toString());
					}
					insertPriceMap= BeanMapUtil.convertBean2MapWithUnderscoreName(price);
					insertPriceList.add(insertPriceMap);

					insertItem=BeanMapUtil.convertBean2MapWithUnderscoreName(info);
					insertItems.add(insertItem);

				}
				//开始插入商品详情信息
				search_DataCrawlService.insertIntoData(insertItems,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_INFO);
				search_DataCrawlService.insertIntoData(insertPriceList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PRICE_INFO);	
			}
			//
			//
			//
			//			if(itemPage.contains("banner")){
			//				Document document = Jsoup.parse(itemPage);	
			//				info.setFeature1(Fields.COUNT_01);
			//				info.setEgoodsId(parameter.getEgoodsId());
			//				info.setGoodsId(parameter.getGoodsId());
			//				info.setGoods_url(parameter.getItemUrl_1());
			//				info.setBatch_time(parameter.getBatch_time());
			//				info.setGoods_status(Fields.STATUS_COUNT_1);
			//				info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			//				info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			//				info.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
			//				info.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());
			//				info.setPlatform_goods_name(document.getElementsByClass("enable-select").text());
			//				//评论分析数据
			//				comment.setBatch_time(parameter.getBatch_time());
			//				comment.setEgoodsId(parameter.getEgoodsId());
			//				comment.setGoodsId(parameter.getGoodsId());
			//				comment.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
			//				comment.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());
			//				comment.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			//				comment.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			//				comment.setComment_status(Fields.STATUS_COUNT_1);
			//				//是否包邮
			//				String deliveryInfo=document.getElementsByClass("g-h-services").text();
			//				if(StringUtils.isEmpty(deliveryInfo)){
			//					deliveryInfo=document.getElementsByClass("g-h-service-txt").toString();
			//				}
			//				if(deliveryInfo.contains(Fields.PACK_MAIL)){
			//					info.setDelivery_info(Fields.PACK_MAIL);
			//				}else{
			//					info.setDelivery_info(Fields.DONTPACK_MAIL);
			//				}
			//				//店铺
			//				info.setPlatform_sellername(document.getElementsByClass("_2guKQnyA").text());
			//				info.setPlatform_shopname(document.getElementsByClass("_2guKQnyA").text());
			//				info.setPlatform_shoptype(Fields.PROPRIETARY);
			//				//销量
			//				info.setSale_qty(speciaValue(document.getElementsByClass("g-sales regular-text").text()));
			//				//图片url
			//				try {
			//					String image=document.getElementById("banner").attr("style").toString();
			//					String img="";
			//					if(image.contains("?")){
			//						img=image.substring(image.indexOf("//"), image.lastIndexOf("?"));	
			//					}else if(image.contains("@")){
			//						img=image.substring(image.indexOf("//"), image.lastIndexOf("@"));	
			//					}
			//
			//					info.setGoods_pic_url("http:"+img);	
			//				} catch (Exception e) {
			//					logger.error("==>>获取图片路径解析发生异常：<<==",e);
			//				}
			//				//现价
			//				String currentPrice=document.getElementsByClass("price-range").text().replace("起", "");
			//				if(StringUtils.isEmpty(currentPrice)){
			//					currentPrice=document.getElementsByClass("g-group-price").text().replace("￥", "").replace("起", "");
			//				}
			//				price.setCurrent_price(currentPrice);
			//				//原价
			//				price.setOriginal_price(document.getElementsByClass("g-market-price regular-text").text().replace("￥", ""));
			//
			//				price.setGoodsid(parameter.getGoodsId());;
			//				price.setChannel(Fields.CLIENT_MOBILE);
			//				price.setSKUid(parameter.getEgoodsId());
			//				price.setBatch_time(parameter.getBatch_time());
			//				price.setCust_keyword_id(parameter.getKeywordId());
			//				price.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			//				price.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			//				price.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());
			//				price.setSale_qty(speciaValue(document.getElementsByClass("g-sales regular-text").text()));
			//
			//
			//				try {
			//					int rawData="window.rawData=".length();		
			//					String   message=itemPage.substring(itemPage.indexOf("window.rawData=")+rawData,itemPage.lastIndexOf("popupModalElement")-2);
			//					JSONObject jsonObject = JSONObject.fromObject(message+"}}");
			//					JSONArray  json=jsonObject.getJSONObject("store").getJSONObject("initDataObj").getJSONObject("goods").getJSONArray("skus");
			//
			//					//店铺
			//					info.setPlatform_sellername(jsonObject.getJSONObject("store").getJSONObject("initDataObj").getJSONObject("mall").getString("mallName"));
			//					info.setPlatform_shopname(jsonObject.getJSONObject("store").getJSONObject("initDataObj").getJSONObject("mall").getString("mallName"));
			//
			//					//销量
			//					info.setSale_qty(jsonObject.getJSONObject("store").getJSONObject("initDataObj").getJSONObject("goods").getString("sideSalesTip"));
			//					//库存
			//					//info.setInventory(jsonObject.getJSONObject("store").getJSONObject("initDataObj").getJSONObject("mall").getString("goodsNum"));
			//					//评论
			//					info.setTtl_comment_num(speciaValue(jsonObject.getJSONObject("store").getJSONObject("initDataObj").getJSONObject("reviews").getString("commentNumText")));
			//					comment.setTtl_comment_num(speciaValue(jsonObject.getJSONObject("store").getJSONObject("initDataObj").getJSONObject("reviews").getString("commentNumText")));
			//					//促销
			//					JSONArray  mallCoupons=jsonObject.getJSONObject("store").getJSONObject("initDataObj").getJSONObject("lisbonInfo").getJSONArray("mallCoupons");
			//					for(int k=0;k<mallCoupons.size();k++){
			//						JSONObject object=JSONObject.fromObject(mallCoupons.toArray()[k].toString());
			//						bufer.append(object.getString("displayName")+":"+object.getString("rulesDesc")+"&&");
			//					}
			//
			//					price.setPromotion(bufer.toString());
			//
			//					for(int i=0;i<json.size();i++){
			//						signboard=1;
			//						JSONObject object=JSONObject.fromObject(json.toArray()[i].toString());
			//						JSONArray  array=object.getJSONArray("specs");
			//						if(array!=null && array.size()>0){
			//							JSONObject specs=JSONObject.fromObject(array.toArray()[0].toString());	
			//							price.setCurrent_price(object.getString("groupPrice"));
			//							price.setOriginal_price(object.getString("normalPrice"));
			//							price.setSKUid(object.getString("skuID"));
			//							currentPrice=object.getString("groupPrice");
			//							price.setSku_name(specs.getString("spec_value"));
			//							if( array.size()>1){
			//								JSONObject specValue=JSONObject.fromObject(array.toArray()[1].toString());
			//								price.setSku_name(specs.getString("spec_value")+"&&"+specValue.getString("spec_value"));
			//							}else{
			//								price.setSku_name(specs.getString("spec_value"));
			//							}
			//
			//							insertPriceMap= BeanMapUtil.convertBean2MapWithUnderscoreName(price);
			//							insertPriceList.add(insertPriceMap); 
			//							search_DataCrawlService.insertIntoData(insertPriceList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PRICE_INFO);
			//							insertPriceList=Lists.newArrayList();
			//						}else{
			//							if(jsonObject.getJSONObject("store").getJSONObject("initDataObj").getJSONObject("goods").toString().contains("linePrice")){
			//								if(StringUtils.isNotEmpty(jsonObject.getJSONObject("store").getJSONObject("initDataObj").getJSONObject("goods").getString("linePrice"))){
			//									price.setOriginal_price(jsonObject.getJSONObject("store").getJSONObject("initDataObj").getJSONObject("goods").getString("linePrice"));	
			//								}else{
			//									price.setOriginal_price(object.getString("normalPrice"));
			//								}
			//							}else{
			//								price.setOriginal_price(object.getString("normalPrice"));	
			//							}
			//							price.setCurrent_price(object.getString("groupPrice"));
			//							price.setSKUid(object.getString("skuID"));
			//							currentPrice=object.getString("groupPrice");
			//							insertPriceMap= BeanMapUtil.convertBean2MapWithUnderscoreName(price);
			//							insertPriceList.add(insertPriceMap);
			//							search_DataCrawlService.insertIntoData(insertPriceList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PRICE_INFO);
			//							insertPriceList=Lists.newArrayList();
			//						}
			//
			//					}
			//
			//					//商品评分
			//					try {
			//						if(jsonObject.getJSONObject("store").getJSONObject("initDataObj").getJSONObject("goods").getJSONObject("review").toString().contains("tagList")){
			//							net.sf.json.JSONArray array=jsonObject.getJSONObject("store").getJSONObject("initDataObj").getJSONObject("goods").getJSONObject("review").getJSONArray("tagList");
			//							StringBuffer buser=new StringBuffer();
			//							for(int i=0;i<array.size();i++){
			//								JSONObject jsonobject=JSONObject.fromObject(array.toArray()[i].toString());
			//								buser.append(jsonobject.getString("text")+","+jsonobject.getJSONObject("view").getString("back_color")+"&&");
			//
			//							}
			//							comment.setComment_tags(buser.toString());
			//						}
			//					} catch (Exception e) {
			//						logger.info("==>>解析商品评分发生异常 error:{}<<==",e.getMessage());
			//						e.printStackTrace();
			//					}
			//
			//					try {
			//						if(jsonObject.getJSONObject("store").getJSONObject("initDataObj").getJSONObject("mall").getJSONObject("dsr").toString().contains("mallRatingTextList")){
			//							//店铺评分
			//							JSONArray testList=jsonObject.getJSONObject("store").getJSONObject("initDataObj").getJSONObject("mall").getJSONObject("dsr").getJSONArray("mallRatingTextList");
			//							StringBuffer storeMessage=new StringBuffer();
			//							for(int k=0;k<testList.size();k++){
			//								JSONObject jsonTest=JSONObject.fromObject(testList.toArray()[k].toString());
			//								storeMessage.append(jsonTest.getJSONObject("mallRatingKey").getString("txt")+"("+jsonTest.getJSONObject("mallRatingValue").getString("txt")+")"+"&&");
			//							}
			//							comment.setComment_shopscores(storeMessage.toString());
			//						}
			//					} catch (Exception e) {
			//						logger.info("解析店铺评分发生异常："+e.getMessage());
			//						e.printStackTrace();
			//					}
			//
			//				} catch (Exception e) {
			//					logger.error("==>>获取规格信息失败：<<==",e);
			//					signboard=-1;
			//					e.printStackTrace();
			//				}
			//
			//
			//				if(StringUtils.isNotEmpty(price.getCurrent_price())){
			//					//开始插入商品详情信息
			//					insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(info);
			//					insertItems.add(insertItem); 
			//					search_DataCrawlService.insertIntoData(insertItems,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_INFO);
			//					if(signboard==0){
			//						//开始插入商品价格信息
			//						insertPriceMap= BeanMapUtil.convertBean2MapWithUnderscoreName(price);
			//						insertPriceList.add(insertPriceMap); 
			//						search_DataCrawlService.insertIntoData(insertPriceList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PRICE_INFO);	
			//					}
			//					//插入评论评分信息
			//					if(StringUtils.isNotEmpty(comment.getComment_shopscores())){
			//						commentList= BeanMapUtil.convertBean2MapWithUnderscoreName(comment);
			//						commentItems.add(commentList); 
			//						search_DataCrawlService.insertIntoData(commentItems,parameter.getListjobName().get(0).getDatabases(),Fields.CRAW_GOODS_COMMENT_INFO);
			//
			//					}
			//
			//				}
			//
			//			}else{
			//				logger.info("==>>拼多多当前请求数据已经检测出来，恶意请求数据！！！商品名称:"+parameter.getListjobName().get(0).getJob_name()+",  商品Id："+parameter.getEgoodsId()+" , 商品链接: "+parameter.getItemUrl_1() +"<<==");
			//			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("==>>解析拼多多数据发生异常：{}<<==",e);
		}
		return item;
	}


	/**
	 * 过滤非数字
	 * @param str
	 * @return
	 */
	public static String getNumeric(String str) {
		String regEx="[^0-9]";  
		Pattern p = Pattern.compile(regEx);  
		Matcher m = p.matcher(str);  
		return m.replaceAll("").trim();
	}

	public String  speciaValue(String monster){
		String dest = "";  
		if (monster != null) {  
			dest = monster.replaceAll("[^0-9]","");  
		}  
		return dest; 
	}
}
