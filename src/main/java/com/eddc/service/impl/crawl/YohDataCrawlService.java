package com.eddc.service.impl.crawl;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Parameter;
import com.eddc.service.AbstractProductAnalysis;
import com.eddc.util.Fields;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jack.zhao
 */
@Slf4j
@Service("yohDataCrawlService")
public class YohDataCrawlService extends AbstractProductAnalysis {
	/*****************************************解析数据****************************************************************************************************/

	@Override
	public Map<String, Object> getProductLink(Parameter parameter) throws Exception {
		Map<String, Object> itemYoh = new HashMap<>(10);
		parameter.setItemUrl_1("https://www.yuouhui.com/goods/"+parameter.getEgoodsId());
		parameter.setItemUtl_2(parameter.getItemUrl_1());
		itemYoh=getProductRequest(parameter);
		return itemYoh;
	}

	@Override
	public Map<String, Object> getProductItemAnalysisData(String StringMessage, Parameter parameter) throws Exception {

		Map<String, Object> item = new HashMap<String, Object>(10);

		//Map<String, Object> insertPriceMap = new HashMap<String, Object>(10);
		List<Map<String, Object>> insertPriceList = Lists.newArrayList();
		//Map<String, Object> insertItem = new HashMap<String, Object>(10);
		List<Map<String, Object>> insertItems = Lists.newArrayList();
		Craw_goods_InfoVO info=productInfo(parameter);
		Craw_goods_Price_Info priceInfo =productInfoPrice(parameter);

		Document docs =Jsoup.parse(StringMessage);

		try {
			if(StringMessage.contains("detail-product-country-info")){
				priceInfo.setSku_name(docs.getElementsByClass("detail-product-country-info").text().split("商品货号")[0].toString());
				info.setPlatform_goods_name(docs.getElementsByClass("detail-product-title").text());
				info.setGoods_pic_url(docs.getElementsByClass("tb-booth tb-pic tb-s428").get(0).getElementsByTag("img").attr("src"));
				info.setGoods_url(parameter.getItemUtl_2());
				priceInfo.setPromotion(docs.getElementsByClass("text-primary").last().text());
				/*请求商品价格*/
				Thread.sleep(3000);
				String quantity=docs.getElementById("quantity").val();
				parameter.setItemUrl_1("https://www.yuouhui.com/goods/ajax-goods-info?goods_sn="+parameter.getEgoodsId()+"&quantity="+quantity);
				/*请求数据*/
				String StringItem = getRequest(parameter);
				if(StringItem.contains("marketPrice")){
					//商品价格
					JSONObject jsonObject = JSONObject.fromObject(StringItem);
					//现价
					priceInfo.setCurrent_price(jsonObject.getJSONObject("data").getString("perPrice"));
					//原价 
					priceInfo.setOriginal_price(jsonObject.getJSONObject("data").getString("marketPrice"));

					priceInfo.setChannel(Fields.CLIENT_PC);
					priceInfo.setSKUid(parameter.getEgoodsId());

					/*封装数据 商品详情**/
					insertItems.addAll(list(info));
					
					//开始插入商品价格
					insertPriceList.addAll(list(priceInfo));
					
				}
				if(insertPriceList!=null && insertPriceList.size()>0){

					getProductSave(insertItems, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_INFO);

					getProductSave(insertPriceList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PRICE_INFO);
				}


			}
		} catch (Exception e) {
			log.error("==>>解析渝欧汇详情发送异常："+e+"<<==");
			e.printStackTrace();
		}
		return item;

	}


}
