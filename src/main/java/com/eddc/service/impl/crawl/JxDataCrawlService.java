package com.eddc.service.impl.crawl;
import com.alibaba.fastjson.JSONObject;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Parameter;
import com.eddc.service.AbstractProductAnalysis;
import com.eddc.util.Fields;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yechengfeng
 */
@Service("jxDataCrawlService")
public class JxDataCrawlService extends AbstractProductAnalysis {
    private static Logger logger = LoggerFactory.getLogger(JxDataCrawlService.class);

    /**
     * 请求商品"
     */
    @Override
    public Map<String, Object> getProductLink(Parameter parameter) {
        Map<String, Object> item = new HashMap<String, Object>(10);
        logger.info("==>>JX台开始解析数据,当前解析的是第：{} 个商品<<==", parameter.getSum());

        parameter.setItemUrl_1(Fields.JX_DETAIL_UTL_APP.replace("EGOODSID", parameter.getEgoodsId()));
        parameter.setItemUtl_2(parameter.getItemUrl_1());
        item = getProductRequest(parameter);
        return item;
    }

	/**
	 * 数据解析
	 *
	 * @throws Exception
	 */
	@Override
    public Map<String, Object> getProductItemAnalysisData(String StringMessage, Parameter parameter) throws Exception {

        Document document = Jsoup.parse(StringMessage);

        Map<String, Object> map = new HashMap<String, Object>(10);
        //价格
       
        List<Map<String, Object>> insertPriceList = Lists.newArrayList();
        //详情
    
        List<Map<String, Object>> insertItemList = Lists.newArrayList();
        Craw_goods_InfoVO infovo =productInfo(parameter);
        Craw_goods_Price_Info priceInfo =productInfoPrice(parameter);
        parameter.setItemUrl_1(Fields.JX_DETAIL_UTL_COMMENT_NUM.replace("EGOODSID", parameter.getEgoodsId()));
        String commentStr = getRequest(parameter);
        Document commentDocument = Jsoup.parse(commentStr);
        //商品详情
        Element totalEval = commentDocument.getElementById("totalEval");
        String commentNum = "";
        if (totalEval != null) {
            commentNum = totalEval.text().replace("人评价", "").trim();
        }
        infovo.setTtl_comment_num(commentNum);
        //价格封装
		priceInfo.setSKUid(parameter.getEgoodsId());
        priceInfo.setChannel(Fields.CLIENT_MOBILE);
        /**获取商品详情**/
        if (StringUtils.isNotEmpty(document.getElementsByTag("h4").text())) {
            infovo.setPlatform_goods_name(document.getElementsByTag("h4").text());
            Boolean ifSelfSupport = document.getElementsByClass("pinInfoBox").first().getElementsByTag("img").attr("class").equals("jxzy");
            infovo.setPlatform_shopname(document.getElementsByClass("pinInfoBox").first().getElementsByTag("p").first().text());
            infovo.setGoods_pic_url(document.getElementsByClass("pic").first().getElementsByTag("img").attr("src").trim());
            if (ifSelfSupport) {
                infovo.setPlatform_shoptype(Fields.YES_PROPRIETARY);
            } else {
                infovo.setPlatform_shoptype(Fields.PROPRIETARY);
            }
        }
        /**获取商品价格**/
        parameter.setItemUrl_1(Fields.JX_DETAIL_UTL_PROMOTION.replace("EGOODSID", parameter.getEgoodsId()));
		//请求数据
        String StringPrice = getRequest(parameter);


        if (StringPrice.contains("price")) {

            JSONObject jsonObject = JSONObject.parseObject(StringPrice);

            String promotionTip = jsonObject.getString("clubDiscountTip");
            JSONObject detailInfo = jsonObject.getJSONObject("productPromo");

            String allowanceTip = "";
            if (jsonObject.getInteger("allowanceShow") == 1) {
                allowanceTip = jsonObject.getString("allowanceTip");
            }
            String promotionInfo = jsonObject.getString("couponNameList");
            StringBuffer bucer = new StringBuffer();
            String clubPrice = detailInfo.getString("clubPrice");
            priceInfo.setCurrent_price(detailInfo.getString("price"));
            priceInfo.setOriginal_price(detailInfo.getString("jxPrice"));
            // 从 goods.js 中可以看出来，如果 useRedEnvelope 为true的话不使用优惠券，
            Boolean useRedEnvelope = false;
            try {
                useRedEnvelope = detailInfo.getBooleanValue("useRedEnvelope");
                if (useRedEnvelope) {
                    promotionInfo = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (StringUtil.isNotEmpty(promotionInfo)) {
                bucer.append(promotionInfo.replace("\"", "").replace("[", "").replace("]", ""));
            }

            if (StringUtil.isNotEmpty(promotionTip)) {
                bucer.append("&&" + promotionTip + "&&" + Fields.MEMBER_SPECIALS + ":" + clubPrice);
            }
            if (StringUtil.isNotEmpty(allowanceTip)) {
                bucer.append("&&" + allowanceTip);
            }
            ////促销信息
            priceInfo.setPromotion(bucer.toString());
        }


        //商品详情
        if (StringPrice.toString().contains("productPromo")) {
            if (StringUtils.isEmpty(priceInfo.getCurrent_price())) {
                infovo.setGoods_status(0);
            }
            
            /**封装数据 详情**/
            insertItemList.addAll(list(infovo));
            
            //insertItemMap = BeanMapUtil.convertBean2MapWithUnderscoreName(infovo);
            //insertItemList.add(insertItemMap);

           // insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
          //  insertPriceList.add(insertPriceMap);
            
            /**封装数据 价格**/
            insertPriceList.addAll(list(priceInfo));
            
            if (parameter.getListjobName().get(0).getStorage().equals("4")) {
                getProductSave(insertItemList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_INFO);
                getProductSave(insertPriceList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PRICE_INFO);
            } else {
                //商品详情
                map.put("itemList", insertItemList);
                //商品价格
                map.put("itemPriceList", insertPriceList);
            }
        }
        return map;
    }


}
