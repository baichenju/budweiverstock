package com.eddc.service.impl.crawl;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.eddc.model.*;
import com.eddc.service.impl.http.HttpClientService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.util.Fields;
import com.eddc.util.Validation;
import com.github.pagehelper.util.StringUtil;
/**
 * @author jack.zhao
 */
@Service("jumeiDataCrawlService")
public class JumeiDataCrawlService {
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	private static Logger logger = LoggerFactory.getLogger(JumeiDataCrawlService.class);

	/**
	 * 项目名称：springmvc_crawler
	 * 类名称：GoodsProductDetails
	 * 类描述：解析聚美页面信息
	 * 创建人：Jack
	 * 创建时间：2017年06月13日 上午10:50:10
	 * 修改备注：
	 *
	 * @throws SQLException
	 * @version
	 */
	public Map<String, Object> JumeiParseItemPage(String item, CrawKeywordsInfo wordsInfo, Map<String, String> map,String ip,String database,String storage) {
		Map<String, String> data = new HashMap<String, String>(10);
		Map<String, Object> itemdata = new HashMap<String, Object>();
		data.putAll(map);
		String shopType = Fields. PROPRIETARY;//自营
		String sellerName =Fields.PROPRIETARY; //店铺
		String postageFree = Fields.DONTPACK_MAIL; //是否包邮
		String inventory =Fields.IS_NOT_STOCK; //库存
		String promotion = ""; //促销
		String image = ""; //图片路径
		String presentPrice = ""; //现价
		String originalPrice = ""; //原价
		String delivery_place = ""; //发货地址delivery_place
		String platformGoodsName="";
		String rateNum="";//评论数
		String product_location="";//产地
		String transactNum="";//月销量
		String regex=".*[a-zA-Z]+.*";  
		String subCatId="";
		String promotionMessage="";
		Matcher m=Pattern.compile(regex).matcher(wordsInfo.getCust_keyword_name());
		try {
			JSONObject currPriceJson = JSONObject.fromObject(item);
			if(data.get("platform").equals(Fields.PLATFORM_JUMEI)){
				if(m.matches()){
					map.put("url", Fields.JUMEI_APP_GoodsName+wordsInfo.getCust_keyword_name()+"&type=jumei_deal"); //聚美国内 特卖商品
				}else{
					map.put("url",Fields.JUMEI_APP_GoodsName+wordsInfo.getCust_keyword_name()+"&type=jumei_mall");//聚美国内 普通商品
				}
			}else if(data.get("platform").equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
				if(m.matches()){
					map.put("url",Fields.JUMEI_APP_GoodsName+wordsInfo.getCust_keyword_name()+"&type=global_deal");//海外购 极速免税店 
				}else{
					map.put("url", Fields.JUMEI_APP_GoodsName+wordsInfo.getCust_keyword_name()+"&type=global_pop_mall");//海外购
				}
			}
			try {
				String itemData=httpClientService.interfaceSwitch(ip, map);//请求数据
				if(Validation.isEmpty(itemData)){
					itemData=httpClientService.GoodsProduct(map);//抓取商品名称
				}
				if(!Validation.isEmpty(itemData)){
					try {
						JSONObject json = JSONObject.fromObject(itemData);	
						platformGoodsName=json.getJSONObject("data").getString("name");
					} catch (Exception e) {
						platformGoodsName="";
					}
					image=currPriceJson.getJSONObject("data").getJSONObject("result").getString("img");//图片
					JSONArray jsonList = currPriceJson.getJSONObject("data").getJSONObject("result").getJSONArray("size");
					if(jsonList.size()>0){
						inventory=JSONObject.fromObject(jsonList.toArray()[0]).getString("stock");//库存
						presentPrice=JSONObject.fromObject(jsonList.toArray()[0]).getString("jumei_price");//现价
						originalPrice=JSONObject.fromObject(jsonList.toArray()[0]).getString("market_price");//原价
						if(originalPrice.equals("-1")){
							originalPrice=presentPrice;
						}
					}
					transactNum=currPriceJson.getJSONObject("data").getJSONObject("result").getString("buyer_number");//月销量
					subCatId=currPriceJson.getJSONObject("data").getJSONObject("result").getString("brand_id");//品牌Id
					rateNum=currPriceJson.getJSONObject("data").getJSONObject("result").getString("detail_comment_num");//评论数
					if(currPriceJson.toString().contains("currPriceJson")){
						sellerName=currPriceJson.getJSONObject("data").getJSONObject("result").getJSONObject("shop_info").getString("store_title");//店铺
						shopType=currPriceJson.getJSONObject("data").getJSONObject("result").getJSONObject("shop_info").getString("is_proprietary");//店铺
					}
					delivery_place=currPriceJson.getJSONObject("data").getJSONObject("result").getJSONObject("default_address").getString("structured_address");//发货地
					try {
						
						if(data.get("platform").equals(Fields.PLATFORM_JUMEI)){
							if(m.matches()){
								map.put("url", Fields.JUMEI_APP_URL_Promotion_2+wordsInfo.getCust_keyword_name()+"&type=jumei_pop"); //聚美国内 特卖商品
							}else{
								map.put("url", Fields.JUMEI_APP_URL_Promotion+wordsInfo.getCust_keyword_name()+"&type=jumei_mall");//聚美国内 普通商品
							}
						}else if(data.get("platform").equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
							if(m.matches()){
								map.put("url", Fields.JUMEI_APP_URL_GLOBAL_Promotion_4+wordsInfo.getCust_keyword_name()+"&type=global_pop");//海外购 极速免税店 
							}else{
								map.put("url", Fields.JUMEI_APP_URL_GLOBAL_Promotion_3+wordsInfo.getCust_keyword_name()+"global_pop_mall");//海外购
							}
						}
						promotion=httpClientService.interfaceSwitch(ip, map);//请求数据
						if(Validation.isEmpty(promotion)){
							promotion=httpClientService.GoodsProduct(map);//请求促销	
						}
						if(!Validation.isEmpty(promotion)){
							if(promotion.contains("promo_sale_text")){
								String temp = promotion.substring(promotion.indexOf("[")+1, promotion.lastIndexOf("]"));
								temp="{promo_sale_text:["+temp+"]}";
								JSONObject pr = JSONObject.fromObject(temp);
								JSONArray  jsonArray=pr.getJSONArray("promo_sale_text"); 
								for(int i=0;i<jsonArray.size();i++){
									JSONObject json = JSONObject.fromObject(jsonArray.toArray()[i]);
									if(Validation.isEmpty(promotionMessage)){
										promotionMessage=json.getString("type_name")+":"+json.getString("show_name");
									}else{
										promotionMessage+=";"+json.getString("type_name")+":"+json.getString("show_name");
									}	
								}	
							}else{
								promotionMessage="";
							}
						}else{
							promotionMessage="";
						}
					} catch (Exception e) {
						promotionMessage="";
					}
					data.put("originalPrice", originalPrice);//原价
					data.put("currentPrice", presentPrice);//现价
					data.put("shopName", sellerName);//店铺名称
					data.put("region",delivery_place);//卖家位置
					data.put("platform_shoptype", shopType);//平台商店类型
					data.put("sellerName", sellerName);//卖家店铺名称
					data.put("picturl", image);//图片url 
					data.put("goodsName", platformGoodsName);//商品名称
					data.put("delivery_place", delivery_place);//交易地址
					data.put("inventory", inventory);//商品库存
					data.put("postageFree",postageFree);//是不包邮
					data.put("skuId", wordsInfo.getCust_keyword_name());//商品sku
					data.put("egoodsId", wordsInfo.getCust_keyword_name());
					data.put("keywordId", wordsInfo.getCust_keyword_id());
					data.put("promotion", promotionMessage);//促销
					data.put("product_location", product_location);//产地
					data.put("accountId",wordsInfo.getCust_account_id());
					data.put("goodsId", data.get("goodsId"));
					data.put("timeDate", data.get("timeDate"));
					data.put("platform_name",data.get("platform"));
					data.put("channel",Fields.CLIENT_MOBILE);
					data.put("feature1", "1");
					data.put("rateNum", rateNum);//商品总评论数
					data.put("transactNum", transactNum);//月销
					data.put("subCatId", subCatId);
					data.put("message", null);   
					data.put("deposit", null);
					data.put("coupons", null);
					data.put("sale_qty", null);
					data.put("reserve_num", null);
					data.put("shopid", null);//商品Id
					data.put("sellerId", null);
					if(data.get("platform").equals(Fields.PLATFORM_JUMEI)){
						data.put("goodsUrl",Fields.JUMEI_PC_URL+wordsInfo.getCust_keyword_name()+ ".html");
					}else{
						data.put("goodsUrl",Fields.JUMEI_PC_GLOBAL_URL+wordsInfo.getCust_keyword_name()+ ".html");
					} 
					if(StringUtil.isNotEmpty(presentPrice)){
						Craw_goods_InfoVO info=crawlerPublicClassService.itemFieldsData(data,database,storage); //商品详情插入数据库 
						Craw_goods_Price_Info price=crawlerPublicClassService.parseItemPrice(data,database, wordsInfo.getCust_keyword_id(),data.get("goodsId"),data.get("timeDate"),data.get("platform"),Fields.STATUS_COUNT_1,storage);//APP端价格插入数据库
						itemdata.putAll(data);
						itemdata.put(Fields.TABLE_CRAW_GOODS_INFO, info);
						itemdata.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, price);
						logger.info(" >>> get current price finish >>>>>>>>>>>>>>>SUCCESS>>>>>>>>>>>>>>>>>>>>>>>");
					}
				}
			} catch (Exception e) {
				logger.info("【聚美获取商品名称失败-- error:{}】",e);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("【聚美插入信息失败------error:{}】",e);
		}
		return itemdata;
	}

	/**
	 * 项目名称：springmvc_crawler
	 * 类名称：GoodsProductDetails
	 * 类描述：解析聚美PC页面价格信息
	 * 创建人：Jack
	 * 创建时间：2017年06月13日 上午10:50:10
	 * 修改备注：
	 *
	 * @throws SQLException
	 * @version
	 */
	public Map<String, Object> ResolutionCellPhonePrices(CommodityPrices commodityPrices, String AppMessage,Map<String,String>map,String database,String storage) {
		Map<String, Object> data = new HashMap<String, Object>();
		String promotion = "";
		String presentPrice = "";
		String originalPrice = "";
		String regex=".*[a-zA-Z]+.*"; 	
		Matcher m=Pattern.compile(regex).matcher(commodityPrices.getEgoodsId());  
		try {
			if(!Validation.isEmpty(AppMessage)){
			if(commodityPrices.getPlatform_name_en().equals(Fields.PLATFORM_JUMEI)){
				if(m.matches()){ 
					//国内聚美特卖商品
					String temp = AppMessage.substring(AppMessage.indexOf("{"), AppMessage.lastIndexOf("}") + 1);
					JSONObject jsonObject = JSONObject.fromObject(temp);
					JSONArray jsonList = jsonObject.getJSONObject("sku_info").getJSONArray("skus");
					if(jsonList.size()>0){
						presentPrice=JSONObject.fromObject(jsonList.toArray()[0]).getJSONObject("price").getString("jumei_price");
						originalPrice=JSONObject.fromObject(jsonList.toArray()[0]).getJSONObject("price").getString("market_price");
					}else {
						if(jsonObject.toString().contains("mall_price")){
							presentPrice=jsonObject.getJSONObject("mall_product").getString("mall_price");
							originalPrice=jsonObject.getJSONObject("mall_product").getString("market_price");
						}
					}
					if(Validation.isEmpty(jsonObject.toString().contains("gift"))){  
					JSONArray jsons = jsonObject.getJSONObject("promo_sale_text").getJSONArray("gift");
					if(jsons.size()>0){ 
						for(int i=0;i<jsons.size();i++){
							JSONObject js = JSONObject.fromObject(jsons.toArray()[i]);
							if(Validation.isEmpty(promotion)){
								promotion=js.getString("show_name");
							}else{
								promotion+=";"+js.getString("show_name"); 
							}
						}	
					  }
					}
				}else{
					//国内普通商品
					String temp = AppMessage.substring(AppMessage.indexOf("{"), AppMessage.lastIndexOf("}") + 1);
					JSONObject jsonObject = JSONObject.fromObject(temp);
					System.out.println(jsonObject); 
					JSONArray jsonList = jsonObject.getJSONObject("stoke").getJSONArray("skus");
					if(jsonList.size()>0){
						presentPrice=JSONObject.fromObject(jsonList.toArray()[0]).getJSONObject("price").getString("jumei_price");
						originalPrice=JSONObject.fromObject(jsonList.toArray()[0]).getJSONObject("price").getString("market_price");
					}
					if(jsonObject.toString().contains("gift")){  
						JSONArray json = jsonObject.getJSONObject("promoSale").getJSONObject("data").getJSONArray("gift");
						if(json.size()>0){ 
							for(int i=0;i<json.size();i++){
								JSONObject js = JSONObject.fromObject(json.toArray()[i]);
								if(Validation.isEmpty(promotion)){
									promotion=js.getString("show_name");
								}else{
									promotion+=";"+js.getString("show_name"); 
								}
							}
						}
					}else if(jsonObject.toString().contains("reduce")){
						JSONArray json = jsonObject.getJSONObject("promoSale").getJSONObject("data").getJSONArray("reduce");
						if(json.size()>0){ 
							for(int i=0;i<json.size();i++){
								JSONObject js = JSONObject.fromObject(json.toArray()[i]);
								if(Validation.isEmpty(promotion)){
									promotion=js.getString("show_name");
								}else{
									promotion+=";"+js.getString("show_name"); 
								}
							}	
						}
					}
					if(jsonObject.toString().contains("card")){
						JSONArray json = jsonObject.getJSONObject("promoSale").getJSONObject("data").getJSONArray("card");
						if(json.size()>0){ 
							for(int i=0;i<json.size();i++){ 
								JSONObject js = JSONObject.fromObject(json.toArray()[i]);
								if(Validation.isEmpty(promotion)){
									promotion=js.getString("show_name");
								}else{
									promotion+=";"+js.getString("show_name"); 
								}
							}	
						}
				    }
				}
			}else if(commodityPrices.getPlatform_name_en().equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
				if(m.matches()){
					//海外购极速免税店可以获取价格促销
					Document docss = Jsoup.parse(AppMessage);
					presentPrice=docss.getElementById("stream_id").attr("price").toString(); 
					originalPrice=presentPrice;
					if(docss.toString().contains("r_second_policy")){
					promotion=docss.getElementsByClass("r_second_policy").get(0).getElementsByTag("span").html();
					}
				}else {
					//海外购
					Document docss = Jsoup.parse(AppMessage);
					if(docss.toString().contains("promotion clearfix")){
						promotion=	docss.getElementsByClass("promotion clearfix").get(0).getElementsByTag("a").html();//商品促销
					}
					map.put("url", "http://www.jumeiglobal.com/ajax_new/MallInfo?mall_id="+commodityPrices.getEgoodsId()+"&callback=static_callback");
					map.put("urlRef", Fields.JUMEI_PC_URL_GLOBAL_Promotion_3+commodityPrices.getEgoodsId()+".html");
					try {
						String price_Message=httpClientService.GoodsProduct(map);//请求商品价格
						logger.info(price_Message+"----");
						if(Validation.isEmpty(price_Message)){
							price_Message=httpClientService.GoodsProduct(map);//请求商品价格
						}
						if(!Validation.isEmpty(price_Message)){
							String temp = price_Message.substring(price_Message.indexOf("{"), price_Message.lastIndexOf("}") + 1);
							JSONObject jsonObject = JSONObject.fromObject(temp);
							presentPrice=jsonObject.getString("jumei_price");
							originalPrice=jsonObject.getString("market_price");
							if(Validation.isEmpty(originalPrice)||originalPrice.equals("-1")){
								originalPrice=presentPrice;
							}	
						}
					} catch (Exception e) {
						logger.info("【聚美海外购请求商品价格失败！！！ error:{}】",e);
					}
				
				}
			  }
			}
			if(!Validation.isEmpty(presentPrice)){
				data.put("channel",Fields.CLIENT_PC);
				data.put("currentPrice",presentPrice);
				data.put("originalPrice", originalPrice);
				data.put("promotion", promotion);
				data.put("goodsId", commodityPrices.getGoodsid());
				data.put("keywordId", commodityPrices.getCust_keyword_id());
				data.put("SKUid",commodityPrices.getEgoodsId());
				Craw_goods_Price_Info infoprice=crawlerPublicClassService.commdityPricesData(commodityPrices,data, database,storage);//插入PC端价格
				data.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, infoprice);
				data.putAll(map);
			}
		} catch (Exception e) {
			logger.info("【解析聚美PC端信息失败------error:{}】",e);
		}
		return data;

	}
	
	 public static String UnicodeMessage(String unicode) {
		 String brand[] = unicode.split("\\\\u");
		 StringBuffer st = new StringBuffer();
		 int data = 0;
		 for (int i = 0; i < brand.length; i++) {
			 if (i != 0) {
				 try {
					 if (brand[i].length() == 4) {
						 data = Integer.parseInt(brand[i], 16);
						 st.append((char) data);
					 } else {
						 String title = String.valueOf(brand[i].substring(4).toString());
						 data = Integer.parseInt(brand[i].substring(0, 4), 16);
						 st.append((char) data);
						 st.append(title);

					 }
				 } catch (Exception e) {
					 e.printStackTrace();
				 }
			 }

		 }
		 return st.toString();

	 }
}
