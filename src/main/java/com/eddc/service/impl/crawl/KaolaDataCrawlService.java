package com.eddc.service.impl.crawl;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.eddc.service.AbstractProductAnalysis;
import com.eddc.util.*;
import com.google.gson.JsonObject;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Parameter;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;

import javax.annotation.PostConstruct;

/**
 * @author jack.zhao
 */
@Service("kaolaDataCrawlService")
public class KaolaDataCrawlService extends AbstractProductAnalysis {
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private WhetherShelves whetherShelves;
	private static final Logger logger = LoggerFactory.getLogger(KaolaDataCrawlService.class);


	/**
	 * 反射机制脱离了spring容器的管理，导致@Resource等注解失效。
	 * jack.zhao
	 */
	public static KaolaDataCrawlService kaolaDataCrawlService;
	@PostConstruct
	public void init () {
		kaolaDataCrawlService=this;
	}

	/*****************************************解析数据****************************************************************************************************/

	@Override
	public Map<String, Object> getProductLink(Parameter parameter) throws SQLException {
		Map<String, Object> itemKaoLa = new HashMap<>(10);
		parameter.setItemUtl_2(Fields.KAOLA_APP_URL + parameter.getEgoodsId()+ ".html");
		if (parameter.getType().equalsIgnoreCase(Fields.STYPE_1)) {

			//122 使用 带hk 域名 22 使用不带hk域名
			if ("122".equals(parameter.getListjobName().get(0).getUser_Id())){
				parameter.setItemUrl_1("https://goods.kaola.com.hk/product/"+parameter.getEgoodsId()+".html");
			}else{
				parameter.setItemUrl_1("https://goods.kaola.com/product/"+parameter.getEgoodsId()+".html");
			}
			//从地区表中获取cookie值
			if(StringUtils.isNotEmpty(parameter.getDeliveryPlace().getDelivery_place_code())){
				parameter.setCookie(parameter.getDeliveryPlace().getDelivery_place_code());
			}
			//请求数据
			String StringMessage = getRequest(parameter);
			String requestMessage = StringMessage;
			//休息会  滑块判断
			if(StringMessage.contains("404 Not Found")||StringMessage.contains("detected unusual traffic")) {
				parameter.setItemUrl_1(Fields.KAOLA_APP_URL + parameter.getEgoodsId() + ".html");
				StringMessage = getOkHttpClient(parameter);
			}
			//getOkHttpClient返回"" 时使用getRequest的数据
			if ("".equals(StringMessage)||StringMessage.contains("punish")){
				StringMessage = requestMessage;
			}
			//判断商品是否下架
			String status = kaolaDataCrawlService.whetherShelves.parseGoodsStatusByItemPage(StringMessage);
			if (Integer.valueOf(status).equals(Integer.valueOf(Fields.STATUS_OFF))) {
				logger.info("==>>" + parameter.getEgoodsId() + parameter.getListjobName().get(0).getPlatform() + "当前商品已经下架,插入数据库 当前是第:" + parameter.getSum() + "商品<<==");
				kaolaDataCrawlService.crawlerPublicClassService.soldOutStatus(status, parameter.getListjobName().get(0).getDatabases(), parameter);
				return itemKaoLa;
			}
			itemKaoLa=this.getProductItemAnalysisData(StringMessage,parameter);

		}
		return itemKaoLa;
	}

	@Override
	public Map<String, Object> getProductItemAnalysisData(String StringMessage, Parameter parameter) {
		//价格
		List<Map<String, Object>> insertPriceList = Lists.newArrayList();
		//详情
		Map<String, Object> insertItemMap = new HashMap<String, Object>(10);
		List<Map<String, Object>> insertItemList = Lists.newArrayList();
		Craw_goods_InfoVO inform =productInfo(parameter);
		Craw_goods_Price_Info priceInfo =productInfoPrice(parameter);
		String shopType = Fields.PROPRIETARY;
		String sellerName = Fields.PROPRIETARY;
		String postageFree = Fields.DONTPACK_MAIL;
		StringBuilder buffer = new StringBuilder();

		//价格封装

		priceInfo.setSKUid(parameter.getEgoodsId());
		priceInfo.setChannel(Fields.CLIENT_MOBILE);
		try {
				Document docs = Jsoup.parse(StringMessage);
			//商品名称
			String platformGoodsName = docs.title().replace("'", "`");
			if (StringUtil.isEmpty(platformGoodsName)) {
				platformGoodsName = StringHelper.getResultByReg(docs.toString(), "qzoneContent: \"([^,\"]+)\",");
			}

			inform.setPlatform_category(StringHelper.getResultByReg(docs.toString(), "\"categoryId\":([\\d]+)"));
			inform.setPlatform_goods_name(platformGoodsName);

			String predictImag=docs.getElementsByClass("v-img").attr("src").trim();
			//新的图片存放位置，存在则覆盖原不准确图片
			try{
				predictImag=docs.getElementsByAttributeValue("property","og:image").attr("content");}
			catch (Exception e){
				logger.warn("==================>meta标签内图片地址不存在<==================");
				e.printStackTrace();
			}

			if(StringUtils.isEmpty(predictImag)) {
				if(docs.toString().contains("showImgBox")) {
					predictImag=docs.getElementById("showImgBox").getElementsByTag("img").attr("src");
				}

			}
			if(!predictImag.contains("http")) {
				predictImag="http:"+predictImag;
			}
			inform.setGoods_pic_url(predictImag);


			if (docs.toString().contains(Fields.YES_PROPRIETARY)) {
				sellerName = Fields.NETEASE_PROPRIETARY;
				shopType = Fields.YES_PROPRIETARY;
			}
			String shopName = StringHelper.getResultByReg(docs.toString(), "\"shopName\":([^,]+)");
			if (StringUtils.isEmpty(shopName)) {
				shopName = sellerName;
			}
			inform.setPlatform_sellername(shopName.replace("\"", "").trim());
			inform.setPlatform_shopname(shopName.replace("\"", "").trim());
			inform.setPlatform_shoptype(shopType);
			/**请求商品价格*/
			//parameter.setItemUrl_1(Fields.KAOLA_APP_URL_PROMOTION_NEW.toString().replace("EGOODSID", parameter.getEgoodsId()) + "&t=" + System.currentTimeMillis());
			//parameter.setItemUrl_1(Fields.KAOLA_PC_URL_PRICE_NEW.replace("EGOODSID", inform.getEgoodsId()).replace("CATEGORYID", inform.getPlatform_category()) + System.currentTimeMillis());

			//String StringItem = getRequest(parameter);
			//String StringItem = getOkHttpClient(parameter);

			//使用kaola app 价格接口
			String app_url = Fields.KAOLA_APP_URL_PROMOTION_NEW.replace("EGOODSID", inform.getEgoodsId());
			if (app_url.contains("CATEGORYID")){
				app_url = app_url.replace("CATEGORYID",inform.getPlatform_category());
			}
			parameter.setItemUrl_1(app_url);

			String StringItem = getRequest(parameter);
			//请求失败,再次尝试
			if (StringItem.contains("rgv587_flag") || "".equals(StringItem)){
				StringItem = JsoupUtil.getHtmlByJsoup(parameter.getItemUrl_1(), parameter.getCookie(),"https://m.kaola.com/");
			}

			if (StringItem.contains("marketPrice")) {
				JSONObject currPriceJson = JSONObject.fromObject(StringItem);
				if (currPriceJson.getJSONObject("data").getJSONObject("skuPrice").toString().contains("kaolaPrice")) {
					if (StringUtils.isNotEmpty(currPriceJson.getJSONObject("data").getJSONObject("skuPrice").getString("kaolaPrice"))) {
						priceInfo.setCurrent_price(currPriceJson.getJSONObject("data").getJSONObject("skuPrice").getString("currentPrice"));
					}
				}else{
					priceInfo.setCurrent_price(currPriceJson.getJSONObject("data").getJSONObject("skuPrice").getString("currentPrice"));
				}
				priceInfo.setOriginal_price(currPriceJson.getJSONObject("data").getJSONObject("skuPrice").getString("currentPrice"));
				priceInfo.setInventory(currPriceJson.getJSONObject("data").getString("goodsCurrentStore"));
				inform.setInventory(currPriceJson.getJSONObject("data").getString("goodsCurrentStore"));
				inform.setDelivery_place(currPriceJson.getJSONObject("data").getString("goodsCurrentStore"));
				inform.setSeller_location(currPriceJson.getJSONObject("data").getString("goodsCurrentStore"));

				if (currPriceJson.getJSONObject("data").getJSONObject("skuPrice").containsKey("newUserPrice")){
					String newUserPrice = currPriceJson.getJSONObject("data").getJSONObject("skuPrice").getString("newUserPrice");
					if (!"null".equals(newUserPrice)) {
						buffer.append("新人价").append(newUserPrice).append("&&");
					}
				}

				//税费不存在则不添加税费预估
				try{
					if (!"null".equals(currPriceJson.getJSONObject("data").getJSONObject("skuTaxInfoPc").getString("taxAmount"))) {
						String tax = currPriceJson.getJSONObject("data").getJSONObject("skuTaxInfoPc").getString("taxAmount");
						buffer.append("税费预估¥").append(tax).append("&&");
				}}catch (Exception e){
					logger.info("不存在税费");
					e.printStackTrace();
				}


				/**促销*/
				if (!currPriceJson.toString().contains("\"promotionList\":null,\"")) {
					//促销
					JSONArray json = currPriceJson.getJSONObject("data").getJSONArray("promotionList");
					if (json != null && json.size() > 0) {
						for (int i = 0; i < json.size(); i++) {
							JSONObject promotionJson = JSONObject.fromObject(json.toArray()[i]);
							if("赠品".equals(promotionJson.getString("promotionTitle"))){
								buffer.append("赠品:");
							}
							buffer.append(promotionJson.getString("promotionContent") + "&&");
						}
					}
				}
				/**优惠卷*/
				if (currPriceJson.toString().contains("contentList")) {
					JSONArray contentList = currPriceJson.getJSONObject("data").getJSONObject("goodsDeclare").getJSONArray("contentList");
					if (contentList != null && contentList.size() > 0) {
						buffer.append(JSONObject.fromObject(contentList.toArray()[0]).getString("detailTitle") + "&&");
					}
				}
				/**优惠卷*/
//				if (!currPriceJson.toString().contains("\"normalCouponDesc\":null,\"")) {
//					if(currPriceJson.getJSONObject("data").toString().contains("normalCouponDesc")) {
//						try {
//							JSONArray jsonArray = JSONArray.fromObject(currPriceJson.getJSONObject("data").getJSONArray("normalCouponDesc"));
//
//							for (int i = 0; i < jsonArray.size(); i++) {
//								buffer.append("领劵:" + jsonArray.getString(i) + "&&");
//							}
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//					}
//				}
				try{
					if (currPriceJson.toString().contains("couponAreaModuleInfo")){
						JSONArray couponArr = currPriceJson.getJSONObject("couponAreaModuleInfo").getJSONArray("canGetCouponVOList");
						for (Object c : couponArr) {
							JSONObject coupon = (JSONObject) c;
							if(coupon.getString("schemeName").contains("会员")){
								buffer.append("会员券：").append("thresholdDesc");
							}
							else {
								buffer.append("领劵:").append(coupon.getString("thresholdDesc"));
							}

						}
					}
				} catch (Exception e){
					logger.info("不存在优惠券");
				}
				if (!currPriceJson.toString().contains("\"goodsCouponList\":null,\"")) {
					if (currPriceJson.getJSONObject("data").toString().contains("goodsCouponList")) {
						JSONArray goodsCouponList = currPriceJson.getJSONObject("data").getJSONArray("goodsCouponList");
						if (goodsCouponList.size() > 0) {
							for (Object o : goodsCouponList) {
								JSONObject j = (JSONObject) o;
								if (StringUtil.isNotEmpty(JSONObject.fromObject(goodsCouponList.toArray()[0]).getString("tipTitle"))) {
									if ("true".equals(j.getString("blackCardCoupon"))){
										buffer.append("黑卡:").append(j.getString("tipTitle") + "&&");
									}else{
										buffer.append(j.getString("tipTitle") + "&&");
									}

								}

							}

						}
					}
				}

				priceInfo.setPromotion(buffer.toString().replace("\"", "").trim());
				/**是否包邮 //运费*/
				if( currPriceJson.getJSONObject("data").toString().contains("goodsPostageInfo")) {
					postageFree = currPriceJson.getJSONObject("data").getJSONObject("goodsPostageInfo").getString("postageShowStr");
					if (postageFree.contains(Fields.FULL)) {
						postageFree = Fields.DONTPACK_MAIL;
					} else {
						postageFree = Fields.PACK_MAIL;
					}
				}
				inform.setDelivery_info(postageFree);
				/**预售*/
				if (currPriceJson.toString().contains("depositGoodsInfo")) {
					if (currPriceJson.getJSONObject("data").getJSONObject("depositGoodsInfo").toString().contains("totalDepositBuyCount")) {
						//预定件数
						priceInfo.setReserve_num(currPriceJson.getJSONObject("data").getJSONObject("depositGoodsInfo").getString("totalDepositBuyCount"));
						//定金
						priceInfo.setDeposit(currPriceJson.getJSONObject("data").getJSONObject("depositGoodsInfo").getString("depositPrice"));
						//抵用金额
						priceInfo.setDeposit(currPriceJson.getJSONObject("data").getJSONObject("depositGoodsInfo").getString("deductionPrice"));

					}else {
						//状态
						inform.setGoods_status(0);
					}
				}
			}

			if (StringUtils.isNotEmpty(priceInfo.getCurrent_price())) {
				//开始插入商品详情信息
				//insertItemMap = BeanMapUtil.convertBean2MapWithUnderscoreName(inform);
				//insertItemList.add(insertItemMap);
				
				/**封装数据 详情**/
				insertItemList.addAll(list(inform));
				
				getProductSave(insertItemList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_INFO);

				//开始插入商品价格信息
				//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
				//insertPriceList.add(insertPriceMap);
				
				/**封装数据 价格**/
	            insertPriceList.addAll(list(priceInfo));
				
				getProductSave(insertPriceList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PRICE_INFO);

				if(parameter.getListjobName().get(0).getClient().equalsIgnoreCase("ALL")) {
					List<Map<String, Object>> insertPrice = Lists.newArrayList();
					priceInfo.setChannel(Fields.CLIENT_PC);
					//开始插入商品价格信息
					//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
					//insertPrice.add(insertPriceMap);
					
					/**封装数据 价格**/
					insertPrice.addAll(list(priceInfo));
					
					getProductSave(insertPrice, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PRICE_INFO);
				}



			}

		} catch (Exception e) {
			logger.error("==>>" + parameter.getListjobName().get(0).getPlatform() + "解析数据失败 ,error:" + e.getMessage() + "<<==");
			e.printStackTrace();
		}
		return insertItemMap;
	}

	
	public Map<String, Object> productPricePc(Parameter parameter,String StringMessage) throws Exception {
		Craw_goods_Price_Info priceInfo = new Craw_goods_Price_Info();
		StringBuilder buffer = new StringBuilder();
		//价格
		Map<String, Object> insertPriceMap = new HashMap<String, Object>();
		List<Map<String, Object>> insertPriceList = Lists.newArrayList();
		if (!Validation.isEmpty(StringMessage)) {
			if(StringMessage.contains("skuDetailList")){
				JSONObject jsonObject = JSONObject.fromObject(StringMessage);
				JSONArray json = jsonObject.getJSONObject("data").getJSONArray("skuDetailList");
				if (json!=null && json.size() > 0) {
					priceInfo.setCurrent_price(JSONObject.fromObject(json.toArray()[0]).getJSONObject("skuPrice").getString("currentPrice"));
					priceInfo.setOriginal_price(JSONObject.fromObject(json.toArray()[0]).getJSONObject("skuPrice").getString("marketPrice"));
					if (StringUtil.isNotEmpty(jsonObject.getJSONObject("data").getJSONObject("goodsDeclare").getString("title"))) {
						buffer.append(jsonObject.getJSONObject("data").getJSONObject("goodsDeclare").getString("title").replace("[", "").replace("]", "").replace("\"", "").trim() + "&&");
					}
					if (!json.toString().contains("\"promotionList\":null,\"")) {
						JSONArray promotionList = JSONObject.fromObject(json.toArray()[0]).getJSONArray("promotionList");
						if (promotionList.toString().contains("promotionContent")) {
							buffer.append(JSONObject.fromObject(promotionList.toArray()[0]).getString("promotionContent") + "&&");
						}
					}
					/**进口税*/
					if (json.toString().contains("skuTaxInfoPc")) {
						buffer.append(JSONObject.fromObject(json.toArray()[0]).getJSONObject("skuTaxInfoPc").getString("taxFeeTitle") + ":" + JSONObject.fromObject(json.toArray()[0]).getJSONObject("skuTaxInfoPc").getString("taxFeeContent") + "&&");
					}
					if (jsonObject.getJSONObject("data").toString().contains("goodsCouponList")) {
						JSONArray goodsCouponList = jsonObject.getJSONObject("data").getJSONArray("goodsCouponList");
						if (goodsCouponList.size() > 0) {
							for (Object o : goodsCouponList) {
								JSONObject j = (JSONObject) o;
								if (StringUtil.isNotEmpty(JSONObject.fromObject(goodsCouponList.toArray()[0]).getString("tipTitle"))) {
									if ("true".equals(j.getString("blackCardCoupon"))){
										buffer.append("黑卡:").append(j.getString("tipTitle") + "&&");
									}else{
										buffer.append(j.getString("tipTitle") + "&&");
									}

								}

							}
						}
					}

				}
			}
			//价格封装
			priceInfo.setBatch_time(parameter.getBatch_time());
			priceInfo.setGoodsid(parameter.getGoodsId());
			priceInfo.setSKUid(parameter.getEgoodsId());
			priceInfo.setPromotion(buffer.toString().replace("null", "").trim());
			priceInfo.setCust_keyword_id(parameter.getKeywordId());
			priceInfo.setChannel(Fields.CLIENT_PC);
			priceInfo.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());
			priceInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			priceInfo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			if (StringUtils.isNotEmpty(priceInfo.getCurrent_price())) {
				
				//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
				//insertPriceList.add(insertPriceMap);
				
				/**封装数据 价格**/
				insertPriceList.addAll(list(priceInfo));
				
				getProductSave(insertPriceList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PRICE_INFO);
			}
		}

		return insertPriceMap;
	}
}
