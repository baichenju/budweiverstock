package com.eddc.service.impl.crawl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import com.eddc.service.AbstractProductAnalysis;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Parameter;
import com.eddc.util.Fields;
import com.eddc.util.Validation;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
/**
 * @author jack.zhao
 */
@Service("vipDataCrawlService")
@Slf4j
public class VipDataCrawlService extends AbstractProductAnalysis {
	private static Logger logger = LoggerFactory.getLogger(VipDataCrawlService.class);

	/*****************************************解析数据****************************************************************************************************/

	@Override
	public Map<String, Object> getProductLink(Parameter parameter) throws Exception {
		Map<String, Object> itemVip=new HashMap<>(10);
		parameter.setItemUtl_2(Fields.REF_PRODUCT.replace(Fields.PRODUCTID, parameter.getEgoodsId()));
		if(parameter.getType().equalsIgnoreCase(Fields.STYPE_1)){
			try {
				parameter.setItemUrl_1(Fields.VIP_ITEM_APP_URL.replace(Fields.PRODUCTID,parameter.getEgoodsId().split("-")[1]).replace(Fields.BRANDID, parameter.getEgoodsId().split("-")[0]));
				itemVip=getProductRequest(parameter);
			} catch (Exception e) {
				e.printStackTrace();
				log.error("==>>"+parameter.getListjobName().get(0).getPlatform()+"解析数据失败 ,error:"+e.getMessage()+"<<==");
			}
			//pc端价格
		}else if(parameter.getType().equalsIgnoreCase(Fields.STYPE_6)){
			itemVip= productPricePc(parameter);
		}
		return itemVip;
	}

	@Override
	public Map<String, Object> getProductItemAnalysisData(String StringMessage, Parameter parameter) throws Exception {
		Map<String,Object> map=new HashMap<>();
		//价格
		List<Map<String, Object>> insertPriceList = Lists.newArrayList();
		//详情
		List<Map<String, Object>> insertItemList = Lists.newArrayList();
		Craw_goods_InfoVO inform=productInfo(parameter);
		Craw_goods_Price_Info priceInfo =productInfoPrice(parameter);

		String shopType="";
		if(StringMessage.contains("max_market_price")){
			try {
				//价格封装

				priceInfo.setSKUid(parameter.getEgoodsId());
				priceInfo.setChannel(Fields.CLIENT_MOBILE);

				JSONObject currPriceJson = JSONObject.fromObject(StringMessage);
				priceInfo.setOriginal_price(currPriceJson.getJSONObject("data").getJSONObject("product").getString("max_market_price"));
				priceInfo.setCurrent_price(currPriceJson.getJSONObject("data").getJSONObject("product").getString("max_vipshop_price"));
				String promotion_price=currPriceJson.getJSONObject("data").getJSONObject("product").getString("promotion_price");

				//特殊活动解析价格
				String vendorProductId = currPriceJson.getJSONObject("data").getJSONObject("product").getString("spuId");
				StringBuilder promoSb = new StringBuilder();
				String brandId= parameter.getEgoodsId().split("-")[0];
				String mid= parameter.getEgoodsId().split("-")[1];
				String url = "https://mapi-rp.vip.com/vips-mobile/rest/shop/goods/vendorSkuList/v4?app_name=shop_wap&app_version=4.0&api_key=8cec5243ade04ed3a02c5972bcda0d3f&mobile_platform=2&source_app=yd_wap&warehouse=VIP_SH&fdc_area_id=103102105&province_id=103102&mars_cid=1607330719088_e13b33238ae7753247592dbfd8fdb9e0&mobile_channel=mobiles-%7C%7C&standby_id=nature&vendorProductId="+vendorProductId+"&mid="+mid+"&brandid="+brandId+"&device=3&functions=midSupportServices%2CuserContext%2CbuyLimit%2CforeShowActive%2CpanelView%2CfuturePriceView%2CshowSingleColor%2CnoProps%2Csku_price%2Cactive_price%2Cprepay_sku_price%2Creduced_point_desc%2CsurprisePrice%2CbusinessCode%2CpromotionTips%2Cinvisible%2Cflash_sale_stock%2CrestrictTips%2CfavStatus%2CbanInfo%2CfuturePrice%2CpriceChart%2CpriceView%2CquotaInfo%2CexclusivePrice%2CextraDetailImages&prepayMsgType=1&promotionTipsVer=5&priceViewVer=6&supportSquare=1&isUseMultiColor=1&couponInfoVer=2&freightTipsVer=3&supportAllPreheatTipsTypes=1&salePriceTypeVer=2&wap_consumer=&mvip=true&_=1607331139";
				parameter.setItemUrl_1(url);
				String actPage =getRequest(parameter);
				JSONObject js = JSONObject.fromObject(actPage);
				try{
					//解析活动页面促销信息

					if (js.getJSONObject("data")!=null){
						JSONObject actjson = js.getJSONObject("data").getJSONObject("product_price_range_mapping").getJSONObject(mid);
						String promPrice = actjson.getJSONObject("priceView").getJSONObject("finalPrice").getString("price");
						String priceTip = actjson.getJSONObject("priceView").getJSONObject("finalPrice").getString("priceTips");
						String quotaInfo = actjson.getJSONObject("priceView").getJSONObject("quotaInfo").getString("totalQuotaShortTips");
						promoSb.append(priceTip).append(":").append(promPrice).append(quotaInfo).append("&&");
					}}catch (Exception e){
						logger.warn("=================>活动页面不存在促销信息<=================");
						e.printStackTrace();
					}
				//解析活动页面促销价格
				try {
					JSONObject actjson = js.getJSONObject("data").getJSONObject("product_price_range_mapping").getJSONObject(mid);
					String presentPrice = actjson.getJSONObject("priceView").getJSONObject("salePrice").getString("salePrice");
					priceInfo.setCurrent_price(presentPrice);
					if (actjson.containsKey("promotionTipsKey")) {
						String promotionTipsKey = actjson.getString("promotionTipsKey");
						if(js.getJSONObject("data").containsKey("promotionTipsMap")){
							JSONObject tipJson = js.getJSONObject("data").getJSONObject("promotionTipsMap");
							if (tipJson.containsKey(promotionTipsKey)){
								JSONArray headTipsList = tipJson.getJSONObject(promotionTipsKey).getJSONArray("headTipsList");
								for (Object o : headTipsList) {
									JSONObject j = (JSONObject) o;
									String tips = j.getString("tips");
									promoSb.append(tips).append("&&");
								}
							}
						}
					}
				}catch (Exception e){
					log.warn("=================>活动页面不存在促销价格信息<=================");
					e.printStackTrace();
				}
				//解析活动页面coupon
				try{
					JSONObject actjson = js.getJSONObject("data").getJSONObject("productCouponMap").getJSONObject(mid);
					promoSb.append(actjson.getString("tips")).append("&&");
				}catch (Exception e){
					logger.warn("=================>活动页面不存在coupon<=================");
					e.printStackTrace();
				}



				inform.setPlatform_goods_name(currPriceJson.getJSONObject("data").getJSONObject("product").getString("title"));

				String promotionDataMessage=currPriceJson.getJSONObject("data").getJSONObject("product").getString("agio");
				//解析特殊活动页面真实折扣
				try{
					JSONObject actjson = js.getJSONObject("data").getJSONObject("product_price_range_mapping").getJSONObject(mid);
					promotionDataMessage = actjson.getJSONObject("priceView").getJSONObject("referPrice").getString("referDiscount");

				}catch (Exception e){
					logger.warn("=================>活动页面不存在折扣信息<=================");
				}


				if(StringUtils.isNotEmpty(promotion_price)){
					priceInfo.setCurrent_price(promotion_price);
				}

				String image=currPriceJson.getJSONObject("data").getJSONObject("product").getString("smallImage");

				inform.setGoods_pic_url(Fields.VIP_IMAGE_URL+image);

				inform.setPlatform_sellername(currPriceJson.getJSONObject("data").getJSONObject("brand").getString("brand_name"));

				inform.setPlatform_shopname(currPriceJson.getJSONObject("data").getJSONObject("brand").getString("brand_name"));
				//产地
				inform.setProduct_location(currPriceJson.getJSONObject("data").getJSONObject("product").getString("areaOutput"));
				//String productId=currPriceJson.getJSONObject("data").getJSONObject("product").getString("productId");
				//促销
				if(StringUtils.isNotEmpty(promotionDataMessage)){
					promotionDataMessage+="&&"+currPriceJson.getJSONObject("data").getJSONObject("product").getString("goodsPointTips");
				}else{
					promotionDataMessage=currPriceJson.getJSONObject("data").getJSONObject("product").getString("goodsPointTips");
				}
				promotionDataMessage+=promoSb;
				priceInfo.setPromotion(promotionDataMessage);

				//商品促销
				String PrplotionPrice=getProductPromotion(parameter);
				if(StringUtils.isNotBlank(PrplotionPrice)) {
					if(StringUtils.isNotBlank(priceInfo.getPromotion())) {
						priceInfo.setPromotion(priceInfo.getPromotion()+"&&"+PrplotionPrice);
					}else {
						priceInfo.setPromotion(PrplotionPrice);
					}
				}

				if(currPriceJson.toString().contains(Fields.VIP_PROPRIETARY)){
					shopType=Fields.YES_PROPRIETARY;
					if(StringUtils.isEmpty(inform.getPlatform_sellername())){
						inform.setPlatform_sellername(Fields.VIP_PROPRIETARY);
					}
				}else{
					shopType=Fields.PROPRIETARY;
				}
				//平台商店类型
				inform.setPlatform_shoptype(shopType);

				String merchandiseId=parameter.getEgoodsId();
				if(merchandiseId.contains("-")){
					merchandiseId=merchandiseId.split("-")[1];
				}
				parameter.setItemUrl_1(Fields.VIP_INVENTORY.replace(Fields.PRODUCTID, merchandiseId)+"&_="+System.currentTimeMillis());
				/*请求数据*/
				String  inventory=getRequest(parameter);

				if(inventory.contains("items")){
					String inventoryMessage=inventory.substring(inventory.indexOf("{"), inventory.lastIndexOf(")"));
					JSONObject json = JSONObject.fromObject(inventoryMessage);
					JSONArray array = json.getJSONArray("items");
					if(array!=null && array.size()>0){
						JSONObject stock=JSONObject.fromObject(array.toArray()[0]);
						inventory=stock.getString("stock");
					}else{
						inventory="0";
					}
				}
				inform.setInventory(inventory);
				priceInfo.setInventory(inventory);

				//商品详情
				if(StringUtils.isNotEmpty(priceInfo.getCurrent_price())){
					/*封装数据 商品详情**/
					insertItemList.addAll(list(inform));
					//insertItemMap=BeanMapUtil.convertBean2MapWithUnderscoreName(inform);
					//insertItemList.add(insertItemMap);
                    
					/*封装数据 商品价格**/
					insertPriceList.addAll(list(priceInfo));
					
					//insertPriceMap=BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
					//insertPriceList.add(insertPriceMap);

					getProductSave(insertItemList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_INFO);
					getProductSave(insertPriceList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PRICE_INFO);
				}
			} catch (Exception e) {
				logger.error("==>>唯品会数据解析失败<<=="+e.getMessage());
				e.printStackTrace();
			}
		}
		return map;

	}
	/**请求商品PC端价格*/
	@SuppressWarnings("unchecked")
	public Map<String, Object>productPricePc(Parameter parameter) {
		Map<String, Object> data = new HashMap<String, Object>();
		//价格
		List<Map<String, Object>> insertPriceList = Lists.newArrayList();
		Craw_goods_Price_Info priceInfo =productInfoPrice(parameter);
		String presentPrice="";

		//价格封装
		priceInfo.setSKUid(parameter.getEgoodsId());
		priceInfo.setChannel(Fields.CLIENT_PC);
		parameter.setItemUrl_1(Fields.VIP_PC_URL_JSON.replace("MID",parameter.getEgoodsId().split("-")[1]).replace("BRANDID", parameter.getEgoodsId().split("-")[0]));
		try{
			/*请求数据*/
			String productPc=getRequest(parameter);

			if(productPc.contains("skus")){
				JSONObject currPriceJson = JSONObject.fromObject(productPc);
				Set<Entry<String, Object>> entrySet =currPriceJson.getJSONObject("data").getJSONObject("skus").entrySet();
				for(Entry<String, Object> entry : entrySet) {
					String originalPrice=currPriceJson.getJSONObject("data").getJSONObject("skus").getJSONObject(entry.getKey()).getJSONObject("price").getString("marketPrice");
					presentPrice=currPriceJson.getJSONObject("data").getJSONObject("skus").getJSONObject(entry.getKey()).getJSONObject("price").getString("salePrice");
					if(currPriceJson.getJSONObject("data").getJSONObject("skus").getJSONObject(entry.getKey()).getJSONObject("price").toString().contains("promotion_price")){
						//促销价格
						String promotionPrice=currPriceJson.getJSONObject("data").getJSONObject("skus").getJSONObject(entry.getKey()).getJSONObject("price").getString("promotion_price");
						if(StringUtils.isNotEmpty(promotionPrice)){
							presentPrice=promotionPrice;
						}
					}
					//promotion 促销
					String promotion=currPriceJson.getJSONObject("data").getJSONObject("skus").getJSONObject(entry.getKey()).getJSONObject("price").getString("vipDiscount");
					if(currPriceJson.getJSONObject("data").getJSONObject("skus").getJSONObject(entry.getKey()).getJSONObject("price").toString().contains("promotionDiscount")){
						String promotionDiscount=currPriceJson.getJSONObject("data").getJSONObject("skus").getJSONObject(entry.getKey()).getJSONObject("price").getString("promotionDiscount");
						if(StringUtils.isNotEmpty(promotionDiscount)){
							promotion=promotionDiscount;
						}
					}

					if(StringUtils.isNotEmpty(presentPrice)){
						if(StringUtils.isEmpty(originalPrice)){
							originalPrice=presentPrice;
						}
						priceInfo.setCurrent_price(presentPrice);
						priceInfo.setOriginal_price(originalPrice);
						priceInfo.setPromotion(promotion);
					}
				}



			}


			//商品促销
			String PrplotionPrice=getProductPromotion(parameter);
			if(StringUtils.isNotBlank(PrplotionPrice)) {
				if(StringUtils.isNotBlank(priceInfo.getPromotion())) {
					priceInfo.setPromotion(priceInfo.getPromotion()+"&&"+PrplotionPrice);
				}else {
					priceInfo.setPromotion(PrplotionPrice);
				}
			}

			if(!Validation.isEmpty(presentPrice)){
				//insertPriceMap=BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
				//insertPriceList.add(insertPriceMap);
				/*封装数据 商品价格 PC**/
				insertPriceList.addAll(list(priceInfo));
				
				getProductSave(insertPriceList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PRICE_INFO);
			}
		} catch (Exception e) {
			logger.error("==>>VIP解析PC端信息失败 <<==",e);
		}
		return data;

	}
	//商品促销
	public String getProductPromotion(Parameter parameter) {
		StringBuffer promotion=new StringBuffer();
		String url="https://mapi-rp.vip.com/vips-mobile/rest/shop/goods/vendorSkuList/v4?app_name=shop_wap&app_version=4.0&api_key=8cec5243ade04ed3a02c5972bcda0d3f&mobile_platform=2&source_app=yd_wap&warehouse=VIP_NH&fdc_area_id=104104101&province_id=104104&mars_cid=1597741580218_03902df85288dc400e89e54e5f5952fe&mobile_channel=mobiles-%7C%7C&standby_id=nature&vendorProductId=94933683666604033&mid="+parameter.getEgoodsId().split("-")[1]+"&brandid="+parameter.getEgoodsId().split("-")[0]+"&device=3&functions=foreShowActive%2CpanelView%2CfuturePriceView%2CshowSingleColor%2CnoProps%2Csku_price%2Cactive_price%2Cprepay_sku_price%2Creduced_point_desc%2CsurprisePrice%2CbusinessCode%2CpromotionTips%2Cinvisible%2Cflash_sale_stock%2CrestrictTips%2CfavStatus%2CbanInfo%2CfuturePrice%2CpriceChart%2CpriceView%2CquotaInfo%2CexclusivePrice%2CextraDetailImages&prepayMsgType=1&promotionTipsVer=5&priceViewVer=5&supportSquare=1&isUseMultiColor=1&couponInfoVer=2&freightTipsVer=3&supportAllPreheatTipsTypes=1&salePriceTypeVer=2&wap_consumer=&mvip=true&_=1597742045";
		parameter.setItemUrl_1(url);
		try {
			/*请求数据*/
			String item=getRequest(parameter);
			JSONObject json = JSONObject.fromObject(item);
			if(item.contains("saleDiscount")) {
				try {

					String discount=json.getJSONObject("data").getJSONObject("product_price_range_mapping").getJSONObject(parameter.getEgoodsId().split("-")[1]).getJSONObject("priceView").getJSONObject("salePrice").getString("saleDiscount");
					String salePrice=json.getJSONObject("data").getJSONObject("product_price_range_mapping").getJSONObject(parameter.getEgoodsId().split("-")[1]).getJSONObject("priceView").getJSONObject("salePrice").getString("salePrice");
					promotion.append("唯品快抢:"+salePrice+","+discount);
					JSONArray array =json.getJSONObject("data").getJSONObject("promotionTipsMap").getJSONObject(parameter.getEgoodsId().split("-")[1]).getJSONArray("headTipsList");
					for(int k=0;k<array.size();k++) {
						JSONObject object = JSONObject.fromObject(array.toArray()[k]);
						promotion.append("&&"+object.getString("tips"));
					}
				} catch (Exception e) {
					logger.error("==>>VIP解析商品促销发生异常： <<==",e);
					e.printStackTrace();
				}

				if(item.contains("productCouponMap")) {
					String tips=json.getJSONObject("data").getJSONObject("productCouponMap").getJSONObject(parameter.getEgoodsId().split("-")[1]).getString("tips");
					if(StringUtils.isNotBlank(tips)) {
						promotion.append("&&"+tips);
					}

				}
			}

		} catch (Exception e) {
			logger.error("==>>VIP解析商品促销发生异常： <<==",e);
			e.printStackTrace();
		}
		return promotion.toString();

	}




}
