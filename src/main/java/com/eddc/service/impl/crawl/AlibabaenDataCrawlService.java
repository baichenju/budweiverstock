package com.eddc.service.impl.crawl;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.model.Craw_goods_Vendor_Info;
import com.eddc.model.Craw_goods_Vendor_price_Info;
import com.eddc.model.Craw_goods_pic_Info;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.google.common.collect.Lists;
import com.microsoft.sqlserver.jdbc.StringUtils;
@Service
public class AlibabaenDataCrawlService {
	private static Logger logger=LoggerFactory.getLogger(AlibabaenDataCrawlService.class);
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	SqlSessionTemplate sqlSessionTemplate;
	@Autowired
	SearchDataCrawlService search_DataCrawlService;
	@SuppressWarnings("unchecked")
	public Map<String, Object> alibabaParseItemPage(String itemPage,Map<String,String>mapType) throws Exception{
		Map<String,Object>messageData=new HashMap<String,Object>();
		List<Craw_goods_Vendor_price_Info> list = new ArrayList<Craw_goods_Vendor_price_Info>();
		List<Map<String,Object>> insertImage= Lists.newArrayList();
		List<Map<String,Object>> insert= Lists.newArrayList();
		Map<String,Object> insertItem=new HashMap<String,Object>();
		String platform_vendorname="";String goods_description_rate=""; String service_attitude_rate="";
		String delivery_speed_rate="";String purchase_back_rate="";String purchase_qty="";String purchase_comment_num="";String image="";
		String platform_sellername="";String purchase_price="";
		messageData.put("batch_time", mapType.get("timeDate"));
		messageData.put("channel", Fields.CLIENT_PC);
		messageData.put("goodsId", mapType.get("goodsId"));
		messageData.put("accountId", mapType.get("accountId"));
		messageData.put("egoodsId",  mapType.get("egoodsId"));
		messageData.put("keywordId",mapType.get("keywordId"));
		mapType.put("dataType",mapType.get("platform_name"));

		String database=mapType.get("database");
		if(itemPage.contains(mapType.get("egoodsId"))){
			Document document =Jsoup.parse(itemPage.toString());
			//商品名称
			String goodsName=document.getElementsByClass("ma-title").text();
			//商品图片
			image="http:"+document.getElementById("J-dcv-image-trigger").attr("src").toString();
			//公司名称
			platform_vendorname=document.getElementsByClass("company-name ").attr("title").trim();
			//公司地址
			String platform_vendoraddress=document.getElementsByClass("register-country").text();
			//评分
			String platform_goods_satisfaction=document.getElementsByClass("score-big").text();
			//评论数
			purchase_comment_num=document.getElementsByClass("reviews").text();
			//商品价格
			if(document.getElementsByClass("ma-reference-price").toString().contains("ma-ref-price")){
				purchase_price=document.getElementsByClass("ma-reference-price").get(0).getElementsByClass("ma-ref-price").tagName("span").text();
				//件数
				String purchase_amount=document.getElementsByClass("ma-reference-price").get(0).getElementsByClass("ma-min-order").text();	
				//价格
				messageData.put("purchase_price",purchase_price);
				//起批量	
				messageData.put("purchase_amount",purchase_amount);
				Craw_goods_Vendor_price_Info Vendor_price=crawGoodsVendorPriceInfoData(messageData);
				list.add(Vendor_price);
				if(mapType.get("storage").equals("4")){
					insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorPriceInfoData(messageData));
					insertImage.add(insertItem); 
				}
			}else if(document.getElementsByClass("ma-price-wrap").toString().contains("ma-ladder-price-item")){
				try {
					List<Element>priceList=document.getElementsByClass("ma-price-wrap").select("li");
					for(int k=0;k<priceList.size();k++){
						//件数
						String purchase_amount=priceList.get(k).getElementsByClass("ma-quantity-range").attr("title").trim();
						//价格
						purchase_price=priceList.get(k).getElementsByClass("ma-spec-price").attr("title").trim();
						if(StringUtils.isEmpty(purchase_price)){
							purchase_price=priceList.get(k).getElementsByClass("priceVal").attr("title").trim();
						}
						//价格
						messageData.put("purchase_price",purchase_price);
						//起批量	
						messageData.put("purchase_amount",purchase_amount);
						Craw_goods_Vendor_price_Info Vendor_price=crawGoodsVendorPriceInfoData(messageData);
						list.add(Vendor_price);
						if(mapType.get("storage").equals("4")){
							insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorPriceInfoData(messageData));
							insertImage.add(insertItem); 
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}else {
				try {
					List<Element>priceList=document.getElementById("ladderPrice").select("li");
					for(int k=0;k<priceList.size();k++){
						//件数
						String purchase_amount=priceList.get(k).getElementsByClass("ma-quantity-range").attr("title").trim();
						//价格
						purchase_price=priceList.get(k).getElementsByClass("priceVal").attr("title").trim();
						//价格
						messageData.put("purchase_price",purchase_price);
						//起批量	
						messageData.put("purchase_amount",purchase_amount);
						Craw_goods_Vendor_price_Info Vendor_price=crawGoodsVendorPriceInfoData(messageData);
						list.add(Vendor_price);
						if(mapType.get("storage").equals("4")){
							insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorPriceInfoData(messageData));
							insertImage.add(insertItem); 
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			//商品小图片
			List<Element>listImage=document.getElementsByClass("inav util-clearfix").select("li");
			for(int i=0;i<listImage.size();i++){
				String imageSrc="http:"+listImage.get(i).getElementsByTag("img").attr("src").replace("50x50", "350x350");
				Craw_goods_pic_Info info=crawlerPublicClassService.commodityImages(mapType,imageSrc,i);//获取小图片
				insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(info);
				insert.add(insertItem);
			}
			
			if(insertImage.size()>0){//价格
				search_DataCrawlService.insertIntoData(insertImage,database,Fields.TABLE_CRAW_VENDOR_PRICE_INFO);
			}
			
			if(insert.size()>0){//图片
				search_DataCrawlService.insertIntoData(insert,database.toString(),Fields.TABLE_CRAW_GOODS_PIC_INFO);	
			}

			messageData.put("platform_vendoraddress",platform_vendoraddress);//供应商地址
			messageData.put("platform_vendorname", platform_vendorname);//供应商名称
			messageData.put("platform_goodsname", goodsName);//---商品名称
			messageData.put("platform_goods_picurl", image );//---商品图片
			messageData.put("platform_goodsurl", mapType.get("url").toString());//商品页链接
			messageData.put("platform_vendoraddress",platform_vendoraddress);//供应商地址
			messageData.put("purchase_qty",purchase_qty);//采购数量
			messageData.put("purchase_comment_num",purchase_comment_num);//评价数
			messageData.put("platform_goods_satisfaction",platform_goods_satisfaction);//商品满意度
			messageData.put("platform_name_en", mapType.get("platform_name"));//平台
			messageData.put("goods_description_rate",goods_description_rate);//货描
			messageData.put("service_attitude_rate",service_attitude_rate);//服务
			messageData.put("delivery_speed_rate", delivery_speed_rate);//发货
			messageData.put("purchase_back_rate",purchase_back_rate);//供应商类型
			messageData.put("platform_sellername",platform_sellername);//卖家店铺
			messageData.put("purchase_promotion","");//采购促销
			messageData.put("platform_goods_detail","");//-商品详情
			messageData.put("platform_vendortype","");//供应商类型
			messageData.put("platform_vendorid", "");
			messageData.put(Fields.TABLE_CRAW_VENDOR_INFO, crawGoodsVendorInfoData(messageData));
			messageData.put("Vendor_priceList", list);//价格
			if(mapType.get("storage").equals("4")){
				insertImage= Lists.newArrayList();
				insertItem=new HashMap<String,Object>();
				insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorInfoData(messageData));
				insertImage.add(insertItem); 
				search_DataCrawlService.insertIntoData(insertImage,database,Fields.TABLE_CRAW_VENDOR_INFO);
			}
		}
		return messageData;
	}

	public Craw_goods_Vendor_Info crawGoodsVendorInfoData(Map<String,Object>data){
		Craw_goods_Vendor_Info info=new Craw_goods_Vendor_Info();
		//SimpleDateFormat fort=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setChannel(data.get("channel").toString());
		info.setCust_keyword_id(Integer.valueOf(data.get("keywordId").toString()));
		info.setDelivery_speed_rate(data.get("delivery_speed_rate").toString());//发货 高于或者低于均值
		info.setEgoodsid(data.get("egoodsId").toString());
		info.setPlatform_vendorid(data.get("platform_vendorid").toString());
		info.setGoods_description_rate(data.get("goods_description_rate").toString());
		info.setGoods_status(Integer.valueOf(Fields.STATUS_ON));
		info.setGoodsid(data.get("goodsId").toString());
		info.setPlatform_goods_detail(data.get("platform_goods_detail").toString());
		info.setPlatform_goods_picurl(data.get("platform_goods_picurl").toString());
		info.setPlatform_goods_satisfaction(data.get("platform_goods_satisfaction").toString());
		info.setPlatform_goodsname(data.get("platform_goodsname").toString());
		info.setPlatform_goodsurl(data.get("platform_goodsurl").toString());
		info.setPlatform_vendoraddress(data.get("platform_vendoraddress").toString());
		info.setPlatform_vendorname(data.get("platform_vendorname").toString());
		info.setPlatform_vendortype(data.get("platform_vendortype").toString());
		info.setPurchase_back_rate(data.get("purchase_back_rate").toString());
		info.setPurchase_comment_num(data.get("purchase_comment_num").toString());
		info.setPurchase_promotion(data.get("purchase_promotion").toString());
		info.setPurchase_qty(data.get("purchase_qty").toString());
		info.setService_attitude_rate(data.get("service_attitude_rate").toString());
		info.setBatch_time(data.get("batch_time").toString());
		info.setPlatform_sellername(data.get("platform_sellername").toString());
		info.setPlatform_name_en(data.get("platform_name_en").toString());
		return info;
	}

	public Craw_goods_Vendor_price_Info  crawGoodsVendorPriceInfoData(Map<String,Object>data){
		Craw_goods_Vendor_price_Info info=new Craw_goods_Vendor_price_Info();
		//SimpleDateFormat fort=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		info.setBatch_time(data.get("batch_time").toString());
		info.setChannel(data.get("channel").toString());
		info.setCust_keyword_id(Integer.valueOf(data.get("keywordId").toString()));
		info.setEgoodsid(data.get("egoodsId").toString());
		info.setPurchase_amount(data.get("purchase_amount").toString());
		info.setPurchase_price(data.get("purchase_price").toString());
		info.setGoodsid(data.get("goodsId").toString());
		info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		if(data.toString().contains("purchase_unit")){
			info.setPurchase_unit(data.get("purchase_unit").toString());	
		}
		return info;

	}
	//判断商品是否下架
	public List<Craw_goods_Vendor_Info>goodsShopMessage(String egoodsId){
		return crawlerPublicClassService.goodsShopMessage(egoodsId);

	}
}
