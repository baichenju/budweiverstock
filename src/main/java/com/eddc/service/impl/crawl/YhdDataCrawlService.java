package com.eddc.service.impl.crawl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.eddc.model.CrawKeywordsInfo;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Parameter;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.Validation;
import com.eddc.util.WhetherShelves;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 项目名称：Price_monitoring_crawler
 * 类名称：Yhd_DataCrawlService
 * 类描述：
 * 创建人：jack.zhao
 * 创建时间：2017年11月22日 上午10:36:33
 * 修改人：jack.zhao
 * 修改时间：2017年11月22日 上午10:36:33
 * 修改备注：
 * @author jack.zhao
 */
@Service
@Slf4j
public class YhdDataCrawlService {
    private static Logger logger = LoggerFactory.getLogger(YhdDataCrawlService.class);
    @Autowired
    HttpClientService httpClientService;
    @Autowired
    CrawlerPublicClassService crawlerPublicClassService;
    @Autowired
    WhetherShelves whetherShelves;
    @Autowired
    SearchDataCrawlService search_DataCrawlService;

    /*****************************************解析数据****************************************************************************************************/
    public Map<String, Object> parameter(Parameter Par) throws Exception {
        Map<String, Object> itemSuning = new HashMap<>(10);
        if (StringUtil.isNotEmpty(Par.getDeliveryPlace().getDelivery_place_code())) {
            Par.setCookie(Par.getDeliveryPlace().getDelivery_place_code());
        }

        Par.setItemUrl_1(Fields.YHD_URL_PC + Par.getEgoodsId() + ".html");
        Par.setItemUtl_2(Fields.YHD_URL_PC + Par.getEgoodsId() + ".html");
//		if(Par.getListjobName().get(0).getClient().equalsIgnoreCase("ALL")){
//			Par.setItemUtl_2(Fields.YHD_URL_PC+Par.getEgoodsId()+".html");
//		}else{
//			Par.setItemUtl_2(Fields.YHD_URL_PC+Par.getEgoodsId()+".html");
//		}
        if (Par.getType().equalsIgnoreCase(Fields.STYPE_1)) {
            try {
                String StringMessage = httpClientService.interfaceSwitch(Par);
                String status = whetherShelves.parseGoodsStatusByItemPage(StringMessage);
                if (Integer.valueOf(status) == Integer.valueOf(Fields.STATUS_OFF)) {
                    log.info("==>>" + Par.getEgoodsId() + Par.getListjobName().get(0).getPlatform() + "当前商品已经下架,插入数据库 当前是第:" + Par.getSum() + "商品<<==");
                    crawlerPublicClassService.soldOutStatus(status, Par.getListjobName().get(0).getDatabases(), Par);
                    return itemSuning;
                }
                itemSuning = itemWrawler(StringMessage, Par);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("==>>" + Par.getListjobName().get(0).getPlatform() + "解析数据失败 ,error:" + e.getMessage() + "<<==");
            }

        } else if (Par.getType().equalsIgnoreCase(Fields.STYPE_6)) {
            itemSuning = crawPricePc(Par);
        }
        return itemSuning;
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> itemWrawler(String item, Parameter parameter) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        //商品图片URL
        String pictureUrl = "";
        //判断自营非自营
        String isYiHaoDian = Fields.YES_PROPRIETARY;
        //库存url
        String stock_url = "";
        String stockDetails = "";
        //店铺
        String platform_sellername = "";
        //省
        String provinceName = "";
        //市
        String cityName = "";
        String venderId = "";
        String StockStateName = "";
        String shopid = "";
        String pricepromotion = "";
        String url = "";
        String commentCount = "";
        //价格
        Map<String, Object> insertPriceMap = new HashMap<String, Object>();
        List<Map<String, Object>> insertPriceList = Lists.newArrayList();
        //详情
        Map<String, Object> insertItemMap = new HashMap<String, Object>();
        List<Map<String, Object>> insertItemList = Lists.newArrayList();
        Craw_goods_InfoVO infovo = new Craw_goods_InfoVO();
        Craw_goods_Price_Info priceInfo = new Craw_goods_Price_Info();
        StringBuffer brff = new StringBuffer();
        long dateTime = System.currentTimeMillis();

        Document doc = Jsoup.parse(item.toString());
        if (doc.toString().contains("productMainName")) {
            infovo.setPlatform_goods_name(doc.getElementById("productMainName").text());

            shopid = StringHelper.getResultByReg(item, "categoryIds:(.+?)\"]");
            shopid = shopid.replace("[", "").replaceAll("\"", "");
            venderId = StringHelper.getResultByReg(item, "venderId: ([^,]+)");
            venderId = StringHelper.getResultByReg(venderId, "([0-9]+)");
            //商品行业Id
            infovo.setPlatform_category(venderId);
            //店铺Id
            infovo.setPlatform_shopid(shopid);
            infovo.setPlatform_sellerid(shopid);
            infovo.setGoods_pic_url("http:" + doc.getElementById("J_prodImg").text());
            //请求库存
            stock_url = StringHelper.getResultByReg(doc.toString(), "jd_item_stock\":\"(.+?)\",");

            //获取库存 店铺 是否有货
            if (StringUtil.isNotEmpty(parameter.getCookie())) {
                String area = StringHelper.getResultByReg(parameter.getCookie(), "yhd_location=([^,]+);").split(";")[0].toString();
                url = "http:" + stock_url + "&skuId=" + parameter.getEgoodsId() + "&venderId=" + venderId + "&cat=" + shopid + "&fqsp=0&area=" + area + "&buyNum=1&callback=loadStockCallBack&_=" + dateTime + "";
            } else {
                url = "http:" + stock_url + "&skuId=" + parameter.getEgoodsId() + "&venderId=" + venderId + "&cat=" + shopid + "&fqsp=0&area=2_2817_51973_0&buyNum=1&callback=loadStockCallBack&_=" + dateTime + "";
            }
            parameter.setItemUrl_1(url);
            parameter.setCoding("GBK");
            try {
                stockDetails = httpClientService.interfaceSwitch(parameter);
                if (!Validation.isEmpty(stockDetails)) {
                    String temp = stockDetails.substring(stockDetails.indexOf("{"), stockDetails.lastIndexOf("}") + 1);
                    JSONObject jsonObject = JSONObject.parseObject(temp);
                    StockStateName = jsonObject.getJSONObject("stock").get("StockStateName").toString();
                    provinceName = jsonObject.getJSONObject("stock").getJSONObject("area").get("provinceName").toString();
                    cityName = jsonObject.getJSONObject("stock").getJSONObject("area").getString("cityName");
                    //商品价格原价
                    priceInfo.setOriginal_price(jsonObject.getJSONObject("stock").getJSONObject("jdPrice").getString("m"));
                    //商品价格现价
                    priceInfo.setCurrent_price(jsonObject.getJSONObject("stock").getJSONObject("jdPrice").getString("p"));

                    //交货地点
                    infovo.setDelivery_place(provinceName + " " + cityName);
                    //销售地点
                    infovo.setSeller_location(provinceName);

                    if (Fields.SPOT.equals(StockStateName)) {
                        StockStateName = Fields.IN_STOCK;
                    } else {
                        StockStateName = Fields.IS_NOT_STOCK;
                    }
                    //是否有货
                    infovo.setInventory(StockStateName);
                    try {
                        if (jsonObject.getJSONObject("stock").toString().contains("self_D")) {
                            platform_sellername = jsonObject.getJSONObject("stock").getJSONObject("self_D").get("vender").toString();
                        } else {
                            platform_sellername = jsonObject.getJSONObject("stock").getJSONObject("D").get("vender").toString();
                        }
                    } catch (Exception e) {
                        platform_sellername = Fields.YES_PROPRIETARY;
                    }
                }
                //店铺名称
                infovo.setPlatform_shopname(platform_sellername);
                infovo.setPlatform_sellername(platform_sellername);

                if (!platform_sellername.contains(Fields.FLAGSHIP_STORE)) {
                    isYiHaoDian = Fields.PROPRIETARY;
                }
                //店铺类型
                infovo.setPlatform_shoptype(isYiHaoDian);

                //促销请求
                String promotionUrl = "https://itemapi.yhd.com/ajaxGetCouponInfo.do?params.skuId=" + parameter.getEgoodsId() + "&params.cat=12259%2C14716%2C15602&params.venderId=" + venderId + "&params.channelId=1&params.popType=0&isCanUseJQ=1&isCanUseDQ=1&isOverseaPurchase=0";
                parameter.setItemUrl_1(promotionUrl);
                parameter.setCoding("UTF-8");
                pricepromotion = httpClientService.interfaceSwitch(parameter);

                if (pricepromotion.toString().contains("discountDesc")) {
                    JSONObject promotionJson = JSONObject.parseObject(pricepromotion);
                    JSONArray json = promotionJson.getJSONObject("data").getJSONArray("joinAs");
                    if (json != null && json.size() > 0) {
                        for (int i = 0; i < json.size(); i++) {
                            JSONObject jsonData = JSONObject.parseObject(json.toArray()[i].toString());
                            brff.append(jsonData.getString("discountDesc") + "," + jsonData.getString("timeDesc") + "&&");
                        }

                    }
                }
                //商品促销
                priceInfo.setPromotion(brff.toString());

                //获取商品的评论数据

                parameter.setItemUrl_1("https://e.m.yhd.com/squ/comment/getFuzzyProductCommentSummarys.do?productId=" + parameter.getEgoodsId());
                String messageComent = httpClientService.interfaceSwitch(parameter);

                if (messageComent.contains("CommentCount")) {
                    try {
                        JSONObject currPriceJson = JSONObject.parseObject(messageComent);
                        JSONArray json = currPriceJson.getJSONObject("data").getJSONObject("result").getJSONArray("commentSummaries");
                        if (json != null && json.size() > 0) {
                            JSONObject comment = JSONObject.parseObject(json.toArray()[0].toString());
                            commentCount = comment.getString("CommentCount");
                            infovo.setPos_comment_num(comment.getString("GoodCount"));
                            infovo.setNeu_comment_num(comment.getString("GeneralCount"));
                            infovo.setNeg_comment_num(comment.getString("PoorCount"));
                            //priceInfo.setTo_use_amount(commentCount);
                        }
                    } catch (Exception e) {
                        logger.error("==>>请求商品评论数发生异常：error{}<<==", e);
                    }
                }

            } catch (Exception e) {
                logger.info("==>>解析一号店数据失败！ error:{}<<==", e);

            }
            //商品评论分
            infovo.setTtl_comment_num(commentCount);

            //是否包邮
            infovo.setDelivery_info(Fields.DONTPACK_MAIL);


            //商品详情
            infovo.setBatch_time(parameter.getBatch_time());
            infovo.setGoods_url(parameter.getItemUtl_2());
            infovo.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
            infovo.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
            infovo.setGoods_status(1);//状态
            infovo.setEgoodsId(parameter.getEgoodsId());
            infovo.setGoodsId(parameter.getGoodsId());
            infovo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
            infovo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
            insertItemMap = BeanMapUtil.convertBean2MapWithUnderscoreName(infovo);
            insertItemList.add(insertItemMap);

            //价格封装
            priceInfo.setBatch_time(parameter.getBatch_time());
            priceInfo.setGoodsid(parameter.getGoodsId());
            priceInfo.setSKUid(parameter.getEgoodsId());
            priceInfo.setPromotion(brff.toString());
            priceInfo.setCust_keyword_id(parameter.getCrawKeywordsInfo().getCust_keyword_id());
            priceInfo.setChannel(Fields.CLIENT_MOBILE);
            priceInfo.setPlatform_name_en(parameter.getCrawKeywordsInfo().getPlatform_name());
            priceInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
            priceInfo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));

            insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
            insertPriceList.add(insertPriceMap);

            //商品详情
            if (StringUtils.isNotEmpty(priceInfo.getCurrent_price())) {
                search_DataCrawlService.insertIntoData(insertItemList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_INFO);
                search_DataCrawlService.insertIntoData(insertPriceList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PRICE_INFO);
            }
        }
        return map;
    }

    /**************************************************PC端价格***********************************************************************************/

    @SuppressWarnings("unchecked")
    public Map<String, Object> crawPricePc(Parameter parameter) throws Exception {
        StringBuffer brff = new StringBuffer();
        Craw_goods_Price_Info priceInfo = new Craw_goods_Price_Info();
        Map<String, Object> mapData = new HashMap<String, Object>();
        //价格
        Map<String, Object> insertPriceMap = new HashMap<String, Object>();
        List<Map<String, Object>> insertPriceList = Lists.newArrayList();

        parameter.setItemUrl_1(Fields.EXTRAPARAM + parameter.getEgoodsId() + "&_=" + System.currentTimeMillis());
        parameter.setCoding("UTF-8");
        //parameter.setCookie(Fields.JD_COOKIE);
        logger.info("==>>开始请求一号店 PC商品价格 url:" + parameter.getItemUrl_1() + "<<==");
        String itemPrice = httpClientService.interfaceSwitch(parameter);

        if (!Validation.isEmpty(itemPrice)) {
            String temp = itemPrice.substring(itemPrice.indexOf("{"), itemPrice.lastIndexOf("}") + 1);
            JSONObject jsonObject = JSONObject.parseObject(temp);
            com.alibaba.fastjson.JSONArray jsonList = jsonObject.getJSONArray("data");
            priceInfo.setCurrent_price(JSONObject.parseObject(jsonList.toArray()[0].toString()).get("p").toString());
            priceInfo.setOriginal_price(JSONObject.parseObject(jsonList.toArray()[0].toString()).get("m").toString());

        }
        //获取商品促销
//		parameter.setItemUrl_1(Fields.PROMOTION+parameter.getEgoodsId()+"&params.venderId="+parameter.getCommodityPrices().getPlatform_category()+"&params.area=2_2817_51973_0");
//		String promotion=httpClientService.interfaceSwitch(parameter);
//		if(!Validation.isEmpty(promotion)){
//			String[] str = promotion.split("content");
//			for(int i=1;i<str.length;i++){
//				String name=StringHelper.getResultByReg("content"+str[i], "name\":\"(.+?)\",");
//				if(i+1==str.length){ 
//					brff.append(name+":"+(StringHelper.getResultByReg("content"+str[i], "content\":\"(.+?)\",")));	
//				}else{
//					brff.append(name+":"+(StringHelper.getResultByReg("content"+str[i], "content\":\"(.+?)\","))+"&&");
//				}
//			}	
//		}
        //促销请求
        String promotionUrl = "https://itemapi.yhd.com/ajaxGetCouponInfo.do?params.skuId=" + parameter.getEgoodsId() + "&params.cat=12259%2C14716%2C15602&params.venderId=" + parameter.getCommodityPrices().getPlatform_category() + "&params.channelId=1&params.popType=0&isCanUseJQ=1&isCanUseDQ=1&isOverseaPurchase=0";
        parameter.setItemUrl_1(promotionUrl);
        parameter.setCoding("UTF-8");
        String pricepromotion = httpClientService.interfaceSwitch(parameter);

        if (pricepromotion.toString().contains("discountDesc")) {
            JSONObject promotionJson = JSONObject.parseObject(pricepromotion);
            JSONArray json = promotionJson.getJSONObject("data").getJSONArray("joinAs");
            if (json != null && json.size() > 0) {
                for (int i = 0; i < json.size(); i++) {
                    JSONObject jsonData = JSONObject.parseObject(json.toArray()[i].toString());
                    brff.append(jsonData.getString("discountDesc") + "," + jsonData.getString("timeDesc") + "&&");
                }

            }
        }
        //商品促销
        priceInfo.setPromotion(brff.toString());
        //价格封装
        priceInfo.setBatch_time(parameter.getBatch_time());
        priceInfo.setGoodsid(parameter.getGoodsId());
        priceInfo.setSKUid(parameter.getEgoodsId());
        priceInfo.setPromotion(brff.toString());
        priceInfo.setCust_keyword_id(parameter.getKeywordId());
        priceInfo.setChannel(Fields.CLIENT_PC);
        priceInfo.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());
        priceInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
        priceInfo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));

        insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
        insertPriceList.add(insertPriceMap);
        if (StringUtils.isNotEmpty(priceInfo.getCurrent_price())) {
            search_DataCrawlService.insertIntoData(insertPriceList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PRICE_INFO);
        }
        return mapData;
    }
}
