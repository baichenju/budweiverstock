package com.eddc.service.impl.crawl;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import com.eddc.service.AbstractProductAnalysis;
import com.eddc.util.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Craw_goods_attribute_Info;
import com.eddc.model.Craw_goods_pic_Info;
import com.eddc.model.Parameter;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;

/**
 * @author jack.zhao
 */
@Service("jdDataCrawlService")
@Slf4j
public class JdDataCrawlService  extends AbstractProductAnalysis {
	private static Logger logger = LoggerFactory.getLogger(JdDataCrawlService.class);
	/**
	 * 反射机制脱离了spring容器的管理，导致@Resource等注解失效。
	 * jack.zhao
	 */
	public static JdDataCrawlService jdDataCrawlService;

	@PostConstruct
	public void init() {
		jdDataCrawlService = this;
	}


	@Override
	public Map<String, Object> getProductLink(Parameter parameter) {
		Map<String, Object> map = new HashMap<>(10);
		//如果库存不存在就获取上海库存数据
		if(StringUtils.isNotEmpty(parameter.getDeliveryPlace().getDelivery_place_code())){
			parameter.setCookie(parameter.getDeliveryPlace().getDelivery_place_code());
		
		} else {
			parameter.setCookie(Fields.JD_COOKIE);
		}
		parameter.setItemUrl_1(Fields.JD_INVENTORY_WQITEM.replace("EGOODSID", parameter.getEgoodsId()));

		if (parameter.getListjobName().get(0).getClient().equalsIgnoreCase("ALL")) {
			parameter.setItemUtl_2(Fields.JD_URL_PC + parameter.getEgoodsId() + ".html");
		} else {
			parameter.setItemUtl_2(Fields.JD_URL_APP + parameter.getEgoodsId() + ".html");
		}
		//抓取赠品
		if (parameter.getListjobName().get(0).getPromotion_status() == 4) {
			log.info("==>>" + parameter.getListjobName().get(0).getPlatform() + "抓取京东卖家赠送的商品<<==");
			map = parseJdData(parameter, null);//解析数据
			String message = map.get("present").toString();
			if (StringUtil.isNotEmpty(message)) {
				map = parseJdData(parameter, message);//解析数据
			}
		} else {
			map = parseJdData(parameter, null);//解析数据
		}
		return map;
	}


	public Map<String, Object> parseJdData(Parameter parameter, String present) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try {
			if (StringUtil.isEmpty(present)) {
				parameter.setGoodsId(StringHelper.encryptByString(parameter.getEgoodsId() + parameter.getListjobName().get(0).getPlatform()));
				dataMap = getProductRequest(parameter);
			} else {
				if (present.contains(",")) {
					String[] data = present.split(",");
					for (int i = 0; i < data.length; i++) {
						parameter.setEgoodsId(data[i]);
						parameter.setItemUrl_1(Fields.JD_INVENTORY_WQITEM.replace("EGOODSID", data[i]));
						parameter.setGoodsId(StringHelper.encryptByString(data[i] + parameter.getListjobName().get(0).getPlatform()));
						dataMap = getProductRequest(parameter);
					}
				} else {
					parameter.setEgoodsId(present);
					parameter.setItemUrl_1(Fields.JD_INVENTORY_WQITEM.replace("EGOODSID", present));
					parameter.setGoodsId(StringHelper.encryptByString(present + parameter.getListjobName().get(0).getPlatform()));
					dataMap = getProductRequest(parameter);
				}
			}

		} catch (Exception e) {
			log.error("==>>京东解析数据失败 ,error：{" + e.getMessage() + "}<<==");
		}

		return dataMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getProductItemAnalysisData(String StringMessage, Parameter parameter) throws Exception {
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> insertPriceListAPP = Lists.newArrayList();

		//详情
		List<Map<String, Object>> insertItemList = Lists.newArrayList();
		//图片
		Map<String, Object> insertImage = new HashMap<String, Object>(5);
		List<Map<String, Object>> imageList = Lists.newArrayList();

		//商品属性
		List<Map<String, Object>> attributeList = Lists.newArrayList();
		Craw_goods_InfoVO infos = productInfo(parameter);
		Craw_goods_Price_Info priceInfo = productInfoPrice(parameter);
		StringBuffer promotion = new StringBuffer();
		JSONObject jsonObject = new JSONObject();
		String delivery_info = Fields.PACK_MAIL;
		String shopId = "0";
		String shopName = "";
		String reserve_num = "";
		String coupons = "";
		String pictureUrl = "";
		String originalPrice = "";
		String serial = "";
		String goodsName = "";
		String jdPrice = "";
		String subCatId = "";
		String provinceName = "";
		String cityName = "";
		String countyName = "";
		String defaultAddress = "";
		String Stock = "";
		String versionId = "";
		String proprietary = Fields.PROPRIETARY;

		//开始解析数据
		if (StringMessage.toString().contains("bigpath")) {
			JSONObject currPriceJsons = JSONObject.fromObject(StringMessage);
			if (currPriceJsons.toString().contains("yuYue")) {
				reserve_num = currPriceJsons.getJSONObject("yuYue").getString("yuyueNum");
			}
			if (StringUtil.isNotEmpty(currPriceJsons.getJSONObject("stock").getString("jdPrice"))) {
				goodsName = currPriceJsons.getJSONObject("ware").getString("wname");
				jdPrice = currPriceJsons.getJSONObject("stock").getString("jdPrice");
				subCatId = currPriceJsons.getJSONObject("ware").getString("category").replace(";", ",");
				pictureUrl = JSONObject.fromObject(currPriceJsons.getJSONObject("ware").getJSONArray("images").toArray()[0]).getString("bigpath");
				provinceName = currPriceJsons.getJSONObject("defaultAddress").getString("provinceName");
				cityName = currPriceJsons.getJSONObject("defaultAddress").getString("cityName");
				countyName = currPriceJsons.getJSONObject("defaultAddress").getString("countyName");
				defaultAddress = provinceName + cityName + countyName;

				if (currPriceJsons.getJSONObject("ware").getJSONObject("mobileShopInfo").toString().contains("shopId")) {
					shopId = currPriceJsons.getJSONObject("ware").getJSONObject("mobileShopInfo").getString("shopId");
				}
				if (!currPriceJsons.getJSONObject("ware").getJSONObject("mobileShopInfo").toString().equals("null")) {
					shopName = currPriceJsons.getJSONObject("ware").getJSONObject("mobileShopInfo").getString("name");
				} else {
					if (currPriceJsons.getJSONObject("stock").toString().contains("iconList")) {
						shopName = JSONObject.fromObject(currPriceJsons.getJSONObject("stock").getJSONArray("iconList").toArray()[0]).getString("name");
						if (shopName.contains(Fields.JING_MUST_REACH)) {
							shopName = Fields.JD_SUPERMARKET;
						}
					}
				}

				proprietary = currPriceJsons.getJSONObject("ware").getString("shortService");
				if (proprietary.contains(Fields.PLATFORM_JD_CN)) {
					proprietary = Fields.YES_PROPRIETARY;
				} else {
					proprietary = Fields.PROPRIETARY;
				}
				String message = shopId + "," + proprietary;
				delivery_info = currPriceJsons.getJSONObject("stock").getString("fare").toString();
				if (delivery_info.contains(Fields.FULL)) {
					delivery_info = Fields.DONTPACK_MAIL;
				} else {
					delivery_info = Fields.PACK_MAIL;
				}
				Stock = currPriceJsons.getJSONObject("stock").getString("status");
				if (Stock.contains(Fields.SPOT)) {
					Stock = Fields.IN_STOCK;
				} else {
					Stock = Fields.IS_NOT_STOCK;
				}

			}
		} else {
			try {
				String item = StringMessage.toString().substring(StringMessage.toString().indexOf("{"), StringMessage.toString().lastIndexOf("}") + 1);
				String messages = item.toString().replace("\\x3E", "").replace("\\x3Ca", "").replace("\\x5C", "").replace("\\x27", "").replace("\\x2", "").replace("\\x3CFa", "").replace("\\x3C\\x2Fa\\x3E", "").trim();
				jsonObject = JSONObject.fromObject(messages);
				goodsName = jsonObject.getJSONObject("item").getString("skuName");
				if (StringUtils.isEmpty(goodsName)) {
					goodsName = jsonObject.getString("skuName");
				}
				pictureUrl = jsonObject.getString("image").replace("FF", "http://");
				originalPrice = jsonObject.getJSONObject("price").getString("op");
				jdPrice = jsonObject.getJSONObject("price").getString("p");
				if (jsonObject.getJSONObject("price").containsKey("tpp")) {
					String plusPrice = jsonObject.getJSONObject("price").getString("tpp");
					if (StringUtils.isNotBlank(plusPrice)) {
						promotion.append("PLUS会员专享价:" + plusPrice + "&&");
					}
				}

				if (jsonObject.containsKey("yuyue")) {
					//预约人数
					reserve_num = jsonObject.getJSONObject("yuyue").getString("num").toString();
				}
				if (jsonObject.containsKey("stock")) {
					//城市
					provinceName = jsonObject.getJSONObject("stock").getJSONObject("area").getString("provinceName");
					//区
					cityName = jsonObject.getJSONObject("stock").getJSONObject("area").getString("cityName");
					//库存
					Stock = jsonObject.getJSONObject("stock").getString("StockStateName");
					//交易地址
					defaultAddress = provinceName + cityName;
				}

				if (jsonObject.getJSONObject("stock").toString().contains("self_D")) {
					//店铺
					shopName = jsonObject.getJSONObject("stock").getJSONObject("self_D").getString("vender");
					subCatId = jsonObject.getJSONObject("stock").getJSONObject("self_D").getString("cg");
					versionId = jsonObject.getJSONObject("stock").getJSONObject("self_D").getString("vid");
					if (jsonObject.getJSONObject("stock").getJSONObject("self_D").toString().contains("shopId")) {
						shopId = jsonObject.getJSONObject("stock").getJSONObject("self_D").getString("shopId");
					}
					if (StringUtils.isEmpty(shopId)) {
						shopId = jsonObject.getJSONObject("stock").getJSONObject("self_D").getString("id");
					}
				} else {
					if (jsonObject.toString().contains("stock")) {
						if (jsonObject.getJSONObject("stock").containsKey("vender")) {
							//店铺
							shopName = jsonObject.getJSONObject("stock").getJSONObject("D").getString("vender");
						} else if (jsonObject.getJSONObject("stock").containsKey("D")) {
							//店铺
							shopName = jsonObject.getJSONObject("stock").getJSONObject("D").getString("deliver");
						} else {
							shopName = Fields.JD_SUPERMARKET;
							versionId = "8888";
						}
						if (jsonObject.getJSONObject("stock").containsKey("D")) {
							subCatId = jsonObject.getJSONObject("stock").getJSONObject("D").getString("cg");
							versionId = jsonObject.getJSONObject("stock").getJSONObject("D").getString("vid");
						}
						if (jsonObject.getJSONObject("stock").getJSONObject("D").toString().contains("shopId")) {
							shopId = jsonObject.getJSONObject("stock").getJSONObject("D").getString("shopId");
						}
						if (StringUtils.isEmpty(shopId)) {
							shopId = jsonObject.getJSONObject("stock").getJSONObject("D").getString("id");
						}
						if (StringUtils.isBlank(subCatId)) {
							subCatId = jsonObject.getJSONObject("item").getString("category").replace("[", "").replace("]", "").replace("\"", "").toString().trim();
						}

					}
				}
				if (jsonObject.getJSONObject("stock").toString().contains("samProductFreight")) {
					delivery_info = jsonObject.getJSONObject("stock").getString("samProductFreight");
					if (delivery_info.contains(Fields.FULL)) {
						delivery_info = Fields.DONTPACK_MAIL;
					} else {
						delivery_info = Fields.PACK_MAIL;
					}
				}

				if (StringUtils.isEmpty(shopName)) {
					if (jsonObject.toString().contains("shopInfo")) {
						//店铺
						shopName = jsonObject.getJSONObject("item").getJSONObject("shopInfo").getString("name");
						subCatId = jsonObject.getJSONObject("item").getString("category").replace("[", "").replace("]", "").replace("\"", "").toString().trim();
						versionId = jsonObject.getJSONObject("item").getString("venderID");
						if (shopName.contains(Fields.PLATFORM_JD_CN)) {
							proprietary = Fields.YES_PROPRIETARY;
						} else {
							proprietary = Fields.PROPRIETARY;
						}
					}
				}
				if (jsonObject.toString().contains("promomiao")) {
					promotion.append(Fields.JD_SECONDS_KILL + ":" + jdPrice + "&&");
				}
				if (jsonObject.containsKey("yuyue")) {
					promotion.append(Fields.JD_PRESELL + ":" + jdPrice + "&&");
				}
				//税费 促销
				if (jsonObject.toString().contains("taxinfov2")) {
					try {
						String taxPrice = jsonObject.getJSONObject("taxinfov2").getString("taxPrice");
						if (Validation.isEmpty(taxPrice)) {
							taxPrice = Fields.COUNT_0;
						}
						promotion.append(jsonObject.getJSONObject("taxinfov2").getString("taxTitle") + taxPrice + "&&");
					} catch (Exception e) {
						logger.error("==>>京东获取税费失败 <<==" + e.getMessage());
					}
				}

				try {
					if (jsonObject.getJSONObject("price").toString().contains("PLUS")) {
						if (jsonObject.getJSONObject("price").toString().contains("limit_text")) {
							promotion.append(jsonObject.getJSONObject("price").getJSONObject("ext").getJSONObject("PLUS").getString("limit_text") + "&&");
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}


				if (shopName.contains(Fields.YES_PROPRIETARY) || shopName.contains(Fields.OFFICIAL) || StringMessage.toString().contains(Fields.JING_MUST_REACH)) {
					proprietary = Fields.YES_PROPRIETARY;
				}


				String message = shopId + "," + proprietary + "," + versionId;
				/*****************************商品属性***************************************/
				try {
					JSONObject jsonItem = jsonObject.getJSONObject("item");
					if (Validation.isNotEmpty(jsonItem.get("Color"))) {
						serial = jsonItem.get("Color").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
						if (serial.contains(",")) {
							serial = serial.split(",")[0].trim().toString();
							Craw_goods_attribute_Info info = attributeInfo(Fields.SERIAL.toString(), serial, parameter);
							/*封装数据**/
							attributeList.addAll(list(info));
							//insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
							//attributeList.add(insertItem);
						}
					}
					JSONObject AttrDesc = jsonItem.getJSONObject("expandAttrDesc");
					Set<Entry<String, Object>> entrySet = AttrDesc.entrySet();
					for (Entry<String, Object> entry : entrySet) {
						
						String messageValue = entry.getValue().toString().replace("[", "").replace("]", "").replace("\"", "").trim();
						Craw_goods_attribute_Info info = attributeInfo(entry.getKey().toString(), messageValue, parameter);
						/*封装数据**/
						attributeList.addAll(list(info));
						//insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
						//attributeList.add(insertItem);
					}

				} catch (Exception e) {
					logger.error("==>>京东析商品属性失败<<==" + e.getMessage());
				}

				/*****************************获取商品小图片***************************************************/
				JSONArray jsonArray = JSONArray.fromObject(jsonObject.getJSONObject("item").get("image").toString());
				for (int i = 0; i < jsonArray.size(); i++) {
					String urlImage = "http://m.360buyimg.com/mobilecms/s750x750_" + jsonArray.getString(i);
					if (i == 0) {
						pictureUrl = urlImage;
					}
					//获取小图片
					Craw_goods_pic_Info info = getImageProduct(parameter, urlImage, i);
					/*封装数据**/
					imageList.addAll(list(info));


				}

			} catch (Exception e) {
				e.printStackTrace();
				logger.error("==>>京东解析出错了 <<==" + e.getMessage());
			}

		}

		/******************************商品详情*************************************************/

		if (StringUtils.isEmpty(shopName)) {
			if (jsonObject.toString().contains("shopInfo")) {
				subCatId = jsonObject.getJSONObject("item").getString("category").replace("[", "").replace("]", "").replace("\"", "").toString().trim();
				versionId = jsonObject.getJSONObject("item").getString("venderID");
				if (StringUtils.isEmpty(shopName)) {
					if (StringMessage.contains("由 京东 发货, 并提供售后服务") || StringMessage.contains("京准达")) {
						proprietary = Fields.YES_PROPRIETARY;
						shopName = Fields.JD_SUPERMARKET;
						shopId = "0";
					}

				}
			}
		}
		//判断商品是否有拼团商品 pingouItem
		if (jsonObject.toString().contains("pingouItem")) {
			if (jsonObject.getJSONObject("pingouItem").toString().contains("m_bp")) {
				promotion.append("京东拼购促销价格:" + jsonObject.getJSONObject("pingouItem").getString("m_bp") + "&&");
			}
		}
		if (jsonObject.toString().contains("\"isPlusLimit\":\"1\"")) {
			promotion.append("PLUS会员专享商品&&");
		}
		if (!pictureUrl.contains("http")) {
			pictureUrl = "http:" + pictureUrl.replace(".webp", "");
		}
		if (shopId.equalsIgnoreCase("0")) {
			shopId = versionId;
		}
		infos.setPlatform_shopname(shopName);//店铺名称
		infos.setEgoodsId(parameter.getEgoodsId());
		infos.setGoodsId(parameter.getGoodsId());
		infos.setSeller_location(provinceName);//卖家位置
		infos.setPlatform_shopid(shopId);//商品Id
		infos.setPlatform_shoptype(proprietary);//平台商店类型
		infos.setPlatform_category(subCatId.replaceAll(":", ",").replaceAll("_", ",").toString());//行业ID
		infos.setPlatform_sellerid(versionId);//店铺Id
		infos.setPlatform_sellername(shopName);//卖家店铺名称
		infos.setGoods_pic_url(pictureUrl);
		infos.setPlatform_goods_name(goodsName);//商品名称
		infos.setDelivery_place(defaultAddress);//交易地址
		infos.setInventory(Stock);//商品库存
		infos.setDelivery_info(Fields.PACK_MAIL);//是不包邮
		infos.setProduct_location(productLocation_message(goodsName));//产地
		if (jdPrice.contains("-1.00")) {
			infos.setGoods_status(0);//状态
		}
		/*封装数据**/
		insertItemList.addAll(list(infos));
		/*****************************商品价格*******************************************/
		//库存
		priceInfo.setInventory(Stock);
		priceInfo.setBatch_time(parameter.getBatch_time());
		priceInfo.setGoodsid(parameter.getGoodsId());
		priceInfo.setSKUid(parameter.getEgoodsId());
		priceInfo.setCurrent_price(jdPrice);
		priceInfo.setOriginal_price(originalPrice);
		priceInfo.setChannel(Fields.CLIENT_MOBILE);
		//预定件数
		priceInfo.setReserve_num(reserve_num);
		priceInfo.setTo_use_amount(coupons);


		CommodityPrices common = new CommodityPrices();
		common.setPlatform_shopid(infos.getPlatform_shopid());
		common.setPlatform_sellerid(infos.getPlatform_sellerid());
		common.setPlatform_category(infos.getPlatform_category());
		parameter.setCommodityPrices(common);
		//商品促销抓取
		Map<String, String> mapPromotion = pricePcPromotion(parameter);

		promotion.append(mapPromotion.get("promotion").toString());
		//判断商品是否有赠品
		map.put("present", mapPromotion.get("present").toString());

		if(mapPromotion.get("promotion") == null || StringUtils.isBlank(mapPromotion.get("promotion"))){

			try {
				JSONArray arry=jsonObject.getJSONArray("promov2");
				if(arry.size()>0){
					if(arry.toString().contains("pis")){
						JSONArray pis=JSONObject.fromObject(arry.toArray()[0]).getJSONArray("pis");
						for(int i=0;i<pis.size();i++){
							JSONObject jso =JSONObject.fromObject(pis.toArray()[i]);
							Set<Entry<String, Object>> entrySet = jso.entrySet();
							for(Entry<String, Object> entry : entrySet) {
								if(entry.getValue().toString().contains(Fields.FULL)||entry.getValue().toString().contains(Fields.PROMOTION_DETAILS) || entry.getValue().toString().contains(Fields.LIMIT)
								||entry.getValue().toString().contains("价")||entry.getValue().toString().contains("￥")){
									promotion.append(entry.getValue().toString()).append("&&");
								}
							}
						}
					}
				}
				if (jsonObject.getJSONObject("price").toString().contains("limit_text")) {
					promotion.append(jsonObject.getJSONObject("price").getJSONObject("ext").getJSONObject("JD").getString("limit_text") + "&&");
				}
				if(jsonObject.containsKey("avlCoupon")){
					JSONArray couponList = jsonObject.getJSONObject("avlCoupon").getJSONArray("coupons");
					for (Object o : couponList) {
						JSONObject coupon = (JSONObject) o;
						String timeDesc = coupon.getString("timeDesc");
						String quota = coupon.getString("quota");
						String discount = coupon.getString("discount");
						if ("100-1".equals(coupon.getString("userLabel"))){
							promotion.append("领会员券:");
						}else{
							promotion.append("领券:");
						}
						if(coupon.getJSONObject("discountdesc").containsKey("info")|| coupon.toString().contains("折")){
							JSONObject discountInfo = coupon.getJSONObject("discountdesc").getJSONArray("info").getJSONObject(0);
							discount = String.valueOf(Double.parseDouble(discountInfo.getString("discount"))*10);
							if (coupon.toString().contains("头号京贴")&&coupon.containsKey("name")){
								promotion.append("满").append(quota).append("享").append(discount).append("折").append(coupon.getString("name")).append(timeDesc).append("&&");
							}else {
								promotion.append("满").append(quota).append("享").append(discount).append("折").append(timeDesc).append("&&");
							}
						}else {
							if (StringUtils.isNotBlank(timeDesc)) {
								timeDesc = "有效期" + timeDesc;
							}
							if (coupon.toString().contains("头号京贴")&&coupon.containsKey("name")){
								promotion.append("满").append(quota).append("减").append(discount).append(",").append(coupon.getString("name")).append(timeDesc).append("&&");
							}
							else {
								promotion.append("满").append(quota).append("减").append(discount).append(timeDesc).append("&&");
							}
						}

					}
				}

			} catch (Exception e) {
				logger.error("解析促销出现问题了>>>>>>>>>>"+e);
			}

			/*预售信息解析*/
			try{
				if(StringMessage.contains("预售")){
					//预售接口
					String presale_url = "https://item-soa.jd.com/getWareBusiness?callback=jQuery5587364&skuId="+parameter.getEgoodsId()+"&cat=12259%2C14716%2C15602&area=2_2830_51812_0&shopId=10154316&venderId=10284102&paramJson=%7B%22platform2%22%3A%221%22%2C%22colType%22%3A100%2C%22specialAttrStr%22%3A%22p0p1ppppppppppppppppppp%22%2C%22skuMarkStr%22%3A%2200%22%7D&num=1";
					String presaleInfo =  JsoupUtil.getHtmlByJsoup(presale_url,"","");
					String json = presaleInfo.substring(presaleInfo.indexOf("(")+1,presaleInfo.lastIndexOf(")"));
					JSONObject preJs = JSONObject.fromObject(json).getJSONObject("YuShouInfo");
					String deposit = preJs.getString("yuShouDeposit").replace("¥","");
					String presalePrice = preJs.getString("yuShouPrice").replace("¥","");
					String tailMoney = preJs.getString("tailMoney").replace("¥","");
					String to_use = "0";
					String startTime = preJs.getString("presaleStartTime");
					String endTime = preJs.getString("presaleEndTime");
					if(preJs.containsKey("yuShouText")){
						to_use = String.valueOf((int)(Double.parseDouble(presalePrice)-Double.parseDouble(tailMoney)-Double.parseDouble(deposit)));
					}

					priceInfo.setDeposit(deposit);
					priceInfo.setTo_use_amount(to_use);
					promotion.append("预售价:").append(presalePrice).append("从").append(startTime).append("到").append(endTime).append("&&");

				}
			}catch (Exception e){
				logger.error("预售解析失败！",e);
			}


//			if(jsonObject.toString().contains("bankpromo")){
//				if(Validation.isEmpty(promotion)){
//					promotion.append(jsonObject.getJSONObject("bankpromo").getString("title"));
//				}else{
//					promotion.append(jsonObject.getJSONObject("bankpromo").getString("title")+"&&");
//				}
//
//			}
//			if(StringUtil.isNotEmpty(promotion.toString())){
//				promotion.append(promotion.substring(0, promotion.length()-2));
//			}
//			if(jsonObject.toString().contains("promomiao")){
//				if(StringUtil.isEmpty(promotion.toString())){
//					promotion.append(Fields.JD_SECONDS_KILL+":"+jdPrice);
//				}else{
//					promotion.append("&&"+Fields.JD_SECONDS_KILL+":"+jdPrice);
//				}
//			}
//			if(jsonObject.toString().contains("yuyue")){
//				if(StringUtil.isEmpty(promotion.toString())){
//					promotion.append(Fields.JD_PRESELL+":"+jdPrice);
//				}else{
//					promotion.append(Fields.JD_PRESELL+":"+jdPrice+"&&");
//				}
//			}
		}

		priceInfo.setPromotion(promotion.toString());


		/*封装数据**/
		insertPriceListAPP.addAll(list(priceInfo));

//		insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
//		insertPriceListAPP.add(insertPriceMap);

		/**PC端价格*/
		if (parameter.getListjobName().get(0).getClient().equalsIgnoreCase("ALL")) {
			long times = System.currentTimeMillis();
			parameter.setItemUrl_1(Fields.JD_PC_PRICE + times + "&area=2_2841_51980_0&skuIds=" + URLEncoder.encode("J_" + parameter.getEgoodsId(), "utf-8"));
			parameter.setItemUtl_2(Fields.JD_URL_PC + parameter.getEgoodsId() + ".html");
			//请求数据
			String priceItem = getRequest(parameter);
			if (!Validation.isEmpty(priceItem)) {
				try {
					String Price = JSONUtil.JsonToList(priceItem).get(0).toString();
					priceInfo.setCurrent_price(JSONUtil.JsonToMap(Price).get("p").toString());
					priceInfo.setOriginal_price(JSONUtil.JsonToMap(Price).get("op").toString());
					if (Validation.isEmpty(priceInfo.getOriginal_price())) {
						priceInfo.setOriginal_price(JSONUtil.JsonToMap(Price).get("p").toString());
					}
				} catch (Exception e) {
					logger.error("==>>价格获取失败 <<==" + e.getMessage());
				}
			}

			priceInfo.setChannel(Fields.CLIENT_PC);
			/*封装数据**/
			insertPriceListAPP.addAll(list(priceInfo));

			//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
			//insertPriceListPC.add(insertPriceMap);
			//insertPriceListAPP.addAll(insertPriceListPC);
		}

		if (parameter.getListjobName().get(0).getStorage().equals("4")) {
			//商品详情
			if (StringUtils.isNotEmpty(jdPrice)) {
				getProductSave(insertItemList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_INFO);
				getProductSave(insertPriceListAPP, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PRICE_INFO);
				//jdDataCrawlService.search_DataCrawlService.insertIntoData(insertItemList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_INFO);
				//search_DataCrawlService.insertIntoData(insertPriceListAPP,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PRICE_INFO);
			}

			//商品属性
			if (attributeList != null && attributeList.size() > 0) {
				getProductSave(attributeList, parameter.getListjobName().get(0).getDatabases(), Fields.CRAW_GOODS_ATTRIBUTE_INFO);
				//search_DataCrawlService.insertIntoData(attributeList,parameter.getListjobName().get(0).getDatabases(),Fields.CRAW_GOODS_ATTRIBUTE_INFO);
			}
			//图片下载
			if (imageList != null && imageList.size() > 0) {
				getProductSave(imageList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PIC_INFO);
				//search_DataCrawlService.insertIntoData(imageList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PIC_INFO);
			}
			attributeList.clear();

		} else {
			//商品价格
			map.put("itemPriceList", insertPriceListAPP);
			//商品详情
			map.put("itemList", insertItemList);
			//商品规格参数
			map.put("attributesList", attributeList);
		}
		//获取商品的规格属性
		String hour = parameter.getMap().get("hour").toString();
		String week = parameter.getMap().get("week").toString();
		String dateWeek = parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[0];
		if (dateWeek.contains(week)) {
			String dataTime = parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[1];
			if (dataTime.contains(hour)) {
				List<Map<String, Object>> attributesList = getSpecificationAttributes(parameter);
				if (!parameter.getListjobName().get(0).getStorage().equals("4")) {
					map.put("attributesList", attributesList);
					//图片
					map.put("imageList", insertImage);
				}
			}
		}
		return map;
	}

	/**************************************************商品PC端价格*******************************************************************/
	@SuppressWarnings("unchecked")
	public Map<String, Object> parameterPricePc(Parameter par) throws Exception {
		Map<String, Object> map = new HashMap<>();
		Map<String, Object> insertPriceMap = new HashMap<String, Object>();
		List<Map<String, Object>> insertPriceList = Lists.newArrayList();
		Craw_goods_Price_Info priceInfo = productInfoPrice(par);
		String priceItem = "";
		long time = System.currentTimeMillis();
		par.setItemUrl_1(Fields.JD_PC_PRICE + time + "&skuIds=" + URLEncoder.encode("J_" + par.getEgoodsId(), "utf-8"));
		par.setItemUtl_2(Fields.JD_URL_PC + par.getEgoodsId() + ".html");
		try {
			priceItem = getRequest(par);
		} catch (Exception e) {
			logger.error("==>>请求PC端价格报错：<<==" + e.getMessage());
		}
		if (!Validation.isEmpty(priceItem)) {
			try {
				String Price = JSONUtil.JsonToList(priceItem).get(0).toString();
				priceInfo.setCurrent_price(JSONUtil.JsonToMap(Price).get("p").toString());
				priceInfo.setOriginal_price(JSONUtil.JsonToMap(Price).get("op").toString());
				if (Validation.isEmpty(priceInfo.getOriginal_price())) {
					priceInfo.setOriginal_price(JSONUtil.JsonToMap(Price).get("p").toString());
				}
			} catch (Exception e) {
				logger.error("==>>价格获取失败 <<==" + e.getMessage());
			}
		}
		try {
			Map<String, String> mapPromotion = pricePcPromotion(par);
			String promotion = mapPromotion.get("promotion");
			//进口税
			promotion += isImportDuty(par);
			priceInfo.setPromotion(promotion);
		} catch (Exception e) {
			logger.error("==>>获取商品进口税促销信息失败<<==" + e.getMessage());
		}


		/*****************************商品价格*******************************************/
		priceInfo.setGoodsid(par.getGoodsId());
		priceInfo.setSKUid(par.getEgoodsId());
		priceInfo.setChannel(par.getListjobName().get(0).getClient());
		priceInfo.setChannel(Fields.CLIENT_PC);

		insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
		insertPriceList.add(insertPriceMap);


		if (insertPriceList != null && insertPriceList.size() > 0) {
			if (StringUtils.isNotEmpty(priceInfo.getCurrent_price())) {
				getProductSave(insertPriceList, par.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PRICE_INFO);
				//search_DataCrawlService.insertIntoData(insertPriceList,par.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PRICE_INFO);
			}
		}
		return map;
	}

	public Map<String, String> pricePcPromotion(Parameter par) throws Exception {
		String Promotion = "", Promotionurl = "";
		String venderId = "";
		String shopId = "";
		String giftsName = "";
		String time = "" + System.currentTimeMillis();
		StringBuffer prom = new StringBuffer();
		StringBuffer present = new StringBuffer();
		Map<String, String> mapPromotion = new HashMap<String, String>();
		if (Validation.isEmpty(par.getCommodityPrices().getPlatform_shopid())) {
			shopId = "0";
		} else {
			shopId = par.getCommodityPrices().getPlatform_shopid();
		}
		if (Validation.isEmpty(par.getCommodityPrices().getPlatform_sellerid())) {
			venderId = "0";
		} else {
			venderId = par.getCommodityPrices().getPlatform_sellerid();
		}
		String proUrl = Fields.JD_URL_PROMOTION + par.getEgoodsId() + "&area=2_0_0_0&shopId=" + shopId + "&venderId=" + venderId + "&cat=" + par.getCommodityPrices().getPlatform_category() + "&_=" + time;
		par.setItemUrl_1(proUrl);
		par.setCoding("GBK");
		logger.info("==>>京东PC端请求促销商品<<==" + proUrl);
		Promotionurl = getRequest(par);//请求数据

		if (Promotionurl.contains("\"title\":\"")) {
			String proInfo = StringHelper.getResultByReg(Promotionurl, "\"title\":\"([^\"]+)");
			if (proInfo.contains(Fields.COUPONS) || proInfo.contains(Fields.FULL) || proInfo.contains(Fields.REDUCTION_OF)) {
				prom.append(Fields.TOP_UP_COUPONS).append(proInfo + "&&");
			}
		}
		if (!Validation.isEmpty(Promotionurl)&&!"{ }".equals(Promotionurl)) {
			Promotion = Promotionurl.substring(Promotionurl.indexOf("{"), Promotionurl.lastIndexOf("}") + 1);
			JSONObject json = JSONObject.fromObject(Promotion);
			if (json.getJSONObject("prom").containsKey("pickOneTag")) {
				JSONArray arry = json.getJSONObject("prom").getJSONArray("pickOneTag");
				if (arry.size() > 0) {
					for (int i = 0; i < arry.size(); i++) {
						JSONObject itemJson = JSONObject.fromObject(arry.toArray()[i].toString());
						String name = itemJson.getString("name").toString();
						//会员特价
						if (name.contains(Fields.MEMBER_SPECIALS)) {
							continue;
						}
						prom.append(name + ":").append(itemJson.getString("content").toString() + "&&");
					}
				}
			}

			if (json.getJSONObject("prom").toString().contains("tags")) {
				JSONArray promTags = json.getJSONObject("prom").getJSONArray("tags");
				if (promTags.size() > 0) {
					for (int i = 0; i < promTags.size(); i++) {
						JSONObject itemJson = JSONObject.fromObject(promTags.toArray()[i].toString());
						if (itemJson.getString("name").contains(Fields.MEMBER_SPECIALS)) {
							continue;
						}
						if (itemJson.toString().contains("gifts")) {
							JSONArray gifts = itemJson.getJSONArray("gifts");
							JSONObject presentJson = JSONObject.fromObject(gifts.toArray()[0].toString());
							present.append(presentJson.getString("sid") + ",");
							giftsName = presentJson.getString("nm");
						}
						prom.append(itemJson.getString("name") + ":" + giftsName + " " + itemJson.getString("content") + "&&");
					}
				}
			}
		}






		if (prom.length() > 0) {
			prom.toString();
		}
		try {
			String items = Promotionurl.substring(Promotionurl.indexOf("{"), Promotionurl.lastIndexOf("}") + 1);
			JSONObject jsonObjects = JSONObject.fromObject(items);
			if (!jsonObjects.getString("ads").equals("null")) {
				JSONArray jsonad = jsonObjects.getJSONArray("ads");
				for (int i = 0; i < jsonad.size(); i++) {
					JSONObject json = JSONObject.fromObject(jsonad.toArray()[i]);
					String promotionAd = json.getString("ad").trim().replaceAll("[^0-9\\u4e00-\\u9fa5]", "").trim();
					if (StringUtil.isNotEmpty(promotionAd)) {
						prom.append(Fields.ADVERTISING + ":" + promotionAd + "&&");
					}
				}
			}

			JSONArray jsonList = jsonObjects.getJSONArray("skuCoupon");
			for (int i = 0; i < jsonList.size(); i++) {
				JSONObject json = JSONObject.fromObject(jsonList.toArray()[i]);
				String tableName = "";
				if (json.containsKey("name")) {
					if (json.getString("name").contains("新客")) {
						tableName = "新客:";
					} else if (json.getString("name").contains("plus")) {
						tableName = "plus:";
					} else if ("30000".equals(json.getString("userClass"))) {
						tableName = "Plus";
					}
				}

				if (json.toString().contains("allDesc")) {
					prom.append(tableName).append(Fields.ROLL).append(":").append(json.getString("allDesc")).append("，").append(json.getString("timeDesc")).append("&&");
				} else {
					if (!"null".equals(json.getString("overlapDesc"))) {
						prom.append(tableName).append(Fields.ROLL).append(":").append(Fields.FULL).append(json.getString("quota")).append(Fields.REDUCTION_OF).append(json.getString("discount")).append("，").append(json.getString("timeDesc")).append("，").append(json.getString("overlapDesc")).append("&&");
					} else {
						prom.append(tableName).append(Fields.ROLL).append(":").append(Fields.FULL).append(json.getString("quota")).append(Fields.REDUCTION_OF).append(json.getString("discount")).append("，").append(json.getString("timeDesc")).append("&&");
					}
				}

			}
		} catch (Exception e) {
			logger.error("==>>京东PC端解析促销发生异常 <<==" + e.getMessage());
		}

		//		//双11 优惠价
		//		String productPromotion="https://wq.jd.com/bases/couponsoa/avlCoupon?callback=getCouponListCBA&sceneval=2&cid=12401&popId="+shopId+"&sku="+par.getEgoodsId()+"&price=2999&platform=4&usejing=true&usedong=true&useglobal=false&t=0.5167788642357602";
		//		par.setItemUrl_1(productPromotion);
		//		Promotionurl =httpClientService.interfaceSwitch(par);//请求数据
		//		if(StringUtils.isNoneBlank(Promotionurl)) {
		//			Promotion=Promotionurl.substring(Promotionurl.indexOf("{"),Promotionurl.lastIndexOf("}")+1);
		//			JSONObject json=JSONObject.fromObject(Promotion);
		//			JSONArray coupons=json.getJSONArray("coupons");
		//			for(int k=0;k<coupons.size();k++) {
		//				JSONObject itemJson = JSONObject.fromObject(coupons.toArray()[k].toString());
		//				if(itemJson.toString().contains("info")) {
		//					JSONArray info=itemJson.getJSONObject("discountdesc").getJSONArray("info");
		//					for(int kk=0;kk<info.size();kk++) {
		//						JSONObject infoJson = JSONObject.fromObject(info.toArray()[kk].toString());
		//						if(infoJson.getString("discount").contains("0.")) {
		//							prom.append("满"+infoJson.getString("quota")+"享"+(infoJson.getDouble("discount")*10)+"折,"+itemJson.getString("name")+", 有效期:"+itemJson.getString("timeDesc")+"&&");
		//						}else {
		//							if(itemJson.getString("name").contains("11.11")) {
		//								prom.append("每满"+infoJson.getString("quota")+"元可减"+infoJson.getString("discount")+","+itemJson.getString("name")+", 有效期:"+itemJson.getString("timeDesc")+"&&");	
		//							}else {
		//								prom.append("满"+infoJson.getString("quota")+"元可用"+infoJson.getString("discount")+","+itemJson.getString("name")+", 有效期:"+itemJson.getString("timeDesc")+"&&");	
		//							}
		//							
		//						}
		//						
		//					}
		//					
		//				}else {
		//					if(itemJson.getString("discount").contains("0.")) {
		//						prom.append("满"+itemJson.getString("quota")+"享"+(itemJson.getDouble("discount")*10)+"折,"+itemJson.getString("name")+", 有效期:"+itemJson.getString("timeDesc")+"&&");
		//					}else {
		//						prom.append("满"+itemJson.getString("quota")+"元可用"+itemJson.getString("discount")+","+itemJson.getString("name")+", 有效期:"+itemJson.getString("timeDesc")+"&&");
		//					}
		//				}
		//				
		//			}
		//		}
		//		
		mapPromotion.put("promotion", prom.toString());
		mapPromotion.put("present", present.toString());
		return mapPromotion;
	}

	public String isImportDuty(Parameter par) throws Exception {
		String message = "";
		String item = "";
		String proUrl = Fields.JD_URL_IMPORTDUTY.replace("EGOODSID", par.getEgoodsId());
		par.setItemUrl_1(proUrl);
		try {
			item = getRequest(par);
		} catch (Exception e) {
			logger.error("==>>请求获取商品进口税失败<<==" + e.getMessage());
		}
		if (StringUtil.isNotEmpty(item)) {
			JSONObject jsonObject = JSONObject.fromObject(item);
			if (jsonObject.toString().contains("taxPrice")) {
				message = "&&" + jsonObject.getString("taxTitle") + jsonObject.getString("taxPrice");
			}
		}
		return message;
	}

	//获取商品的规格属性
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getSpecificationAttributes(Parameter parameter) {

		List<Map<String, Object>> insertList = Lists.newArrayList();
		String item = "暂无";
		String getSignInfo = "";
		parameter.setItemUrl_1("https://wq.jd.com/commodity/itembranch/getspecification?callback=commParamCallBackA&skuid=" + parameter.getEgoodsId());
		try {
			item = getRequest(parameter);
			if (item.contains("productSkuId")) {
				getSignInfo = item.toString().substring(item.toString().indexOf("(") + 1, item.toString().lastIndexOf(")"));
				JSONObject json = JSONObject.fromObject(getSignInfo);
				JSONArray jsonArray = JSONArray.fromObject(json.getJSONObject("data").getJSONArray("propGroups"));
				for (int i = 0; i < jsonArray.size(); i++) {
					JSONObject obj = JSONObject.fromObject(jsonArray.toArray()[i]);
					JSONArray array = JSONArray.fromObject(obj.getJSONArray("atts"));
					for (int k = 0; k < array.size(); k++) {
						JSONObject atts = JSONObject.fromObject(array.toArray()[k]);
						String value = "";
						value = atts.getString("vals").replace("[", "").replace("]", "").replace("\"", "");
						Craw_goods_attribute_Info info = attributeInfo(atts.getString("attName"), value, parameter);
						/*封装数据**/
						insertList.addAll(list(info));

						//insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
						//insertList.add(insertItem);
					}
				}
			}

			//Storage 1 批量插入数据 4 单条插入数据
			if (parameter.getListjobName().get(0).getStorage().equalsIgnoreCase("4")) {
				if (insertList != null && insertList.size() > 0) {
					try {
						//search_DataCrawlService.insertIntoData(insertList,parameter.getListjobName().get(0).getDatabases(),Fields.CRAW_GOODS_ATTRIBUTE_INFO);
						getProductSave(insertList, parameter.getListjobName().get(0).getDatabases(), Fields.CRAW_GOODS_ATTRIBUTE_INFO);
					} catch (Exception e) {
						logger.error("==>>插入商品属性失败失败, error:{}<<==", e);
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			log.error("==>>获取商品规格发生异常：" + e.getMessage() + "<<== \n" + getSignInfo);
		}
		return insertList;

	}

	//产地拆分
	public String productLocation_message(String location){
		String message="";
		for(int i=0;i<publicClass.location().size();i++){
			if(location.contains(publicClass.location().get(i))){
				message=publicClass.location().get(i);
			}
			if(Validation.isEmpty(message)){
				if(location.contains(Fields.LOCATION_LIBERO)){
					message=Fields.LOCATION_RUIDIAN;
				}else if(location.contains(Fields.LOCATION_BEILAMI)){
					message=Fields.LOCATION_AODALIY;
				}else if(location.contains(Fields.LOCATION_MEIZANCHEN)){
					message=Fields.LOCATION_MEIGUO;
				}else if(location.contains(Fields.LOCATION_FRISO)){
					message=Fields.LOCATION_HELAN;
				}else if(location.contains(Fields.LOCATION_COW)){
					message=Fields.LOCATION_XINXILAN;	
				}else if(location.contains(Fields.LOCATION_AOZHOUS)){
					message=Fields.LOCATION_AOZHOU;
				}
			}
		}
		return message;
	}
	//////////////////////////////////////////////////////关键词搜索///////////////////////////////////////////////////////////////////////////////////////////
	/***
	 * 分页请求数据
	 * @throws UnsupportedEncodingException 
	 **/
	public Map<String, String> productPage(Map<String, String> map, CrawKeywordsInfo crawKeywordsInfo,int page){
		int mod = page % 2;
		String productUrl=crawKeywordsInfo.getCust_keyword_url();
		String productRef=crawKeywordsInfo.getCust_keyword_ref();
		//京东搜索PC
		try {
			if (crawKeywordsInfo.getCust_keyword_url().contains("search.jd.com")) {
				String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
				if (mod == 0) {
					productUrl = productUrl.replace("KEYWORD", keyWord).replace("PAGE", String.valueOf(page)).replace("SUM", String.valueOf(((page - 1) * 26))) + "&scrolling=y&log_id=1524643976.14897&tpl=3_M";	
				} else {
					productUrl = productUrl.replace("KEYWORD", keyWord).replace("PAGE", String.valueOf(page)).replace("SUM", String.valueOf(((page - 1) * 26))) + "&click=2";
					productRef = productRef.replace("KEYWORD", keyWord).replace("PAGE", String.valueOf(page)).replace("SUM", String.valueOf(((page - 1) * 26)));

				}
				if(StringUtils.isNoneBlank(crawKeywordsInfo.getCust_keyword_brand())) {
					String bran = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_brand(), "UTF-8");
					productUrl=productUrl+"&ev="+bran;
					productRef=productRef+"&ev="+bran;
				}
				map.put("url", productUrl);
				map.put("urlRef", productRef);
			} else if (crawKeywordsInfo.getCust_keyword_url().contains("so.m.jd.com")) {
				productUrl=crawKeywordsInfo.getCust_keyword_url();
				if (crawKeywordsInfo.getCust_keyword_type().equals("category")) {
					map.put("url", Fields.JD_SEARCH_REF_URL_APP.replace("PAGE", String.valueOf(page)).replace("C1", crawKeywordsInfo.getCust_keyword_name().split("-")[0]).replace("C2", crawKeywordsInfo.getCust_keyword_name().split("-")[1]).replace("CATEGORYID", crawKeywordsInfo.getCust_keyword_name().split("-")[2]));
				} else if (crawKeywordsInfo.getCust_keyword_type().equals("keyword")) {
					String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
					map.put("url", Fields.JD_SEARCH_REF_URL_APP_KEYWORD.replace("PAGE", String.valueOf(page)).replace("KEYWORD", keyWord));
				} else if (crawKeywordsInfo.getCust_keyword_type().equals("keywordcategory")) {
					String keyWord = URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
					String keyWord2 = URLEncoder.encode(crawKeywordsInfo.getCategory_name(), "UTF-8");
					if (crawKeywordsInfo.getCust_account_name().contains(Fields.JD_LOGISTICS)) {
						map.put("url", Fields.JD_SEARCH_REF_URL_APP_KEYWORD.replace("PAGE", String.valueOf(page)).replace("KEYWORD", keyWord2) + "&expressionKey=%5B%7B%22value%22%3A%22" + keyWord + "%22%2C%22key%22%3A%22%E5%93%81%E7%89%8C%22%7D%5D&configuredFilters=%5B%7B%22bodyValues%22%3A%221%22%2C%22bodyKey%22%3A%22stock%22%7D%2C%7B%22bodyValues%22%3A%221%22%2C%22bodyKey%22%3A%22self%22%7D%5D");
					} else {
						map.put("url", Fields.JD_SEARCH_REF_URL_APP_KEYWORD.replace("PAGE", String.valueOf(page)).replace("KEYWORD", keyWord2) + "&expressionKey=%5B%7B%22value%22%3A%22" + keyWord + "%22%2C%22key%22%3A%22%E5%93%81%E7%89%8C%22%7D%5D&configuredFilters=%5B%7B%22bodyValues%22%3A%221%22%2C%22bodyKey%22%3A%22stock%22%7D%5D");
					}
				}
				map.put("urlRef", productUrl);
			} else if (crawKeywordsInfo.getCust_keyword_url().contains("list.jd.com")) {
				String codeCookie=map.get("codeCookie").toString();
				if (StringUtil.isNotEmpty(codeCookie)) {
					map.put("cookie", codeCookie);
				}
				map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(page)));
				map.put("urlRef", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(page)));
			}
		} catch (Exception e) {
			log.error("==>>京东关键词搜索,商品进行分页发生异常" + e.getMessage() + "<<== \n");
			e.printStackTrace();
		}
		return map;
	}

  }
