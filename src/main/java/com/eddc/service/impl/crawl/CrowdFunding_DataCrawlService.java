package com.eddc.service.impl.crawl;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eddc.model.Craw_goods_crowdfunding_Info;
import com.eddc.model.Craw_goods_crowdfunding_price_Info;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.github.pagehelper.util.StringUtil;
@Service
public class CrowdFunding_DataCrawlService {
	@SuppressWarnings("unused")
	private static Logger logger=LoggerFactory.getLogger(CrowdFunding_DataCrawlService.class);
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	SearchDataCrawlService search_DataCrawlService;
	/**   
	 *    
	 * 项目名称：Price_monitoring_crawler   
	 * 类名称：Taobazc_DataCrawlService   
	 * 类描述：解析淘宝众筹详情数据   
	 * 创建人：jack.zhao   
	 * 创建时间：2018年5月31日 下午1:15:40   
	 * 修改人：jack.zhao   
	 * 修改时间：2018年5月31日 下午1:15:40   
	 * 修改备注：   
	 * @version    
	 *    
	 */
	public Map<String,Object>crowdFunding(String item,Craw_goods_crowdfunding_Info crowdfunding){
		Map<String,Object>data=new HashMap<String, Object>();
		List<Craw_goods_crowdfunding_price_Info>list=new ArrayList<Craw_goods_crowdfunding_price_Info>();
		List<Craw_goods_crowdfunding_price_Info>listJd=new ArrayList<Craw_goods_crowdfunding_price_Info>();
		if(crowdfunding.getPlatform_name_en().equalsIgnoreCase(Fields.PLATFORM_TAOBAO_ZC)){
		try{
			String ite[]=item.split("item_id");
			for(int i=1;i<ite.length;i++){
				Craw_goods_crowdfunding_price_Info info=new Craw_goods_crowdfunding_price_Info();
				item="\"item_id"+ite[i];
				//System.out.println(StringHelper.getResultByReg(item.toString(),"\"images\": \"([^,\"]+)"));
				info.setGoodsId(crowdfunding.getGoodsId());
				info.setCust_keyword_id(crowdfunding.getCust_keyword_id());
				info.setGoods_url(crowdfunding.getGoods_url());
				info.setSource_egoodsid(crowdfunding.getEgoodsId());
				info.setGoodsId(crowdfunding.getGoodsId());
				info.setEgoodsId(StringHelper.getResultByReg(item.toString(),"\"item_id\": \"([^,\"]+)"));
				info.setPlatform_goods_name(StringHelper.getResultByReg(item.toString(),"\"title\":\"([^,\"]+)"));//回报商品内容
				info.setPlatform_goods_remark(StringHelper.getResultByReg(item.toString(),"\"desc\": \"([^,\"]+)"));//回报商品内容的进一步说明
				String count=StringHelper.getResultByReg(item.toString(),"\"total\": ([^,\"]+)},");
				String Remain=StringHelper.getResultByReg(item.toString(),"\"support_person\": \"([^,\"]+)");
				info.setExp_shiptime_remark(StringHelper.getResultByReg(item.toString(),"\"make_days\": \"([^,\"]+)"));//预计回报说明
				if(StringUtil.isNotEmpty(count)){
					info.setLimit_total_count(Integer.valueOf(count.trim()));//限制人数;	
				}
				if(StringUtil.isNotEmpty(Remain)){
					info.setRemain_count(Integer.valueOf(Remain.trim()));////剩余人数	
				}
				info.setSource_curr_amount(crowdfunding.getCurr_amount());////已筹金额
				info.setSource_finish_per(crowdfunding.getFinish_per());//达成率
				info.setSource_focus_count(crowdfunding.getFocus_count());//喜欢人数
				info.setSource_support_count(crowdfunding.getSupport_count());//支持人数
				info.setSource_goods_status(crowdfunding.getGoods_status());//众筹状态
				info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
				info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
				info.setBatch_time(SimpleDate.SimpleDateFormatData().format(new Date()));
				info.setGoods_pic_url(crowdfunding.getGoods_url());
				list.add(info); 
			}
			} catch (Exception e) {
				logger.info("商品不存在");
			} 
		}else if(crowdfunding.getPlatform_name_en().equalsIgnoreCase(Fields.PLATFORM_JD_ZC)){
			String regEx="[^0-9]"; Pattern p = Pattern.compile(regEx);     
			Document doc = Jsoup.parse(item); String count=""; String support_count="";
			support_count=doc.getElementsByClass("p-progress").last().getElementsByTag("span").last().text();
			Matcher m = p.matcher(support_count);
			support_count= m.replaceAll("").trim();//父节点支持者
			if(doc.toString().contains("details-right-fixed-box")){
			Elements lists=doc.getElementsByClass("details-right-fixed-box").get(0).getElementsByClass("box-grade");
			for(Element ment:lists){
				try {
					if(ment.toString().contains("font-b")){
						Craw_goods_crowdfunding_price_Info info=new Craw_goods_crowdfunding_price_Info();
						String price=ment.getElementsByClass("t-price").get(0).getElementsByTag("span").text();
						if(ment.toString().contains(Fields.UNLIMITED)){
							count="-1";//无限额人数
							info.setRemain_count(-1);
						}else{
							count=ment.getElementsByClass("limit-num").get(0).getElementsByTag("span").get(1).text(); 
							info.setRemain_count(Integer.valueOf(ment.getElementsByClass("limit-num").get(0).getElementsByTag("span").get(2).text()));////剩余人数
						}
						if(ment.toString().contains("box-imglist")){
							info.setGoods_url(ment.getElementsByClass("box-imglist").get(0).getElementsByTag("ul").get(0).getElementsByTag("li").get(0).getElementsByTag("img").attr("data-src").trim());	
						}
						Float  curr_price=Float.valueOf(price);//众筹价
						info.setCurr_price(curr_price);//众筹价
						info.setGoodsId(crowdfunding.getGoodsId());
						info.setEgoodsId(crowdfunding.getEgoodsId());
						info.setCust_keyword_id(crowdfunding.getCust_keyword_id());
						info.setSource_egoodsid(crowdfunding.getEgoodsId());
						info.setPlatform_goods_name(doc.getElementsByClass("p-title").text());//回报商品内容
						info.setPlatform_goods_remark(ment.getElementsByClass("box-intro").text());//回报商品内容的进一步说明
						info.setLimit_total_count(Integer.valueOf(count));//限制人数;
						info.setExp_shiptime_remark(ment.getElementsByClass("box-item").get(1).text());//预计回报说明
						info.setExp_postfee_remark(ment.getElementsByClass("box-item").get(0).getElementsByClass("font-b").text());//邮费说明
						info.setSupport_count(Integer.valueOf(ment.getElementsByClass("t-people").get(0).getElementsByTag("span").text()));//支持者
						info.setSource_curr_amount(crowdfunding.getCurr_amount());////已筹金额
						info.setSource_finish_per(crowdfunding.getFinish_per());//达成率
						info.setSource_focus_count(crowdfunding.getFocus_count());//喜欢人数
						info.setSource_support_count(Integer.valueOf(support_count=(String) support_count==""?"0":support_count));//支持人数
						info.setSource_goods_status(crowdfunding.getGoods_status());//众筹状态
						info.setGoods_url(crowdfunding.getGoods_url());
						info.setGoods_pic_url(crowdfunding.getGoods_pic_url());
						info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
						info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
						info.setBatch_time(SimpleDate.SimpleDateFormatData().format(new Date()));
						listJd.add(info);

					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			} 
		  }
		}
		data.put("list_taobaozc", list);
		data.put("list_jdzc", listJd);
		return data;
	} 
}
