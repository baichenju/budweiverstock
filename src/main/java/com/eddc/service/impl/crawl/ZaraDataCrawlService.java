package com.eddc.service.impl.crawl;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.mapper.CrawlerPublicClassMapper;
import com.eddc.model.Craw_customerweb_goods_info;
import com.google.common.collect.Lists;
@Service
public class ZaraDataCrawlService {
	@Autowired
	private CrawlerPublicClassMapper crawlerPublicClassMapper;
	private static Logger logger = LoggerFactory.getLogger(ZaraDataCrawlService.class);  
	/**
	 * @throws InterruptedException 
	 * @throws UnsupportedEncodingException   
	 * @Title: zaraParseItemPage  
	 * @Description: 解析页面详情
	 * @param @param item
	 * @param @param message
	 * @param @return    设定文件  
	 * @return Map<String,String>    返回类型  
	 * @throws  
	 */  
	public void zaraParseItemPage(String item ,Map<String,String>message) throws UnsupportedEncodingException, InterruptedException{
		List<Craw_customerweb_goods_info>list1=new ArrayList<Craw_customerweb_goods_info>();
		
		String price="";
		String temp = item.substring(item.indexOf("{"), item.lastIndexOf("}") + 1);
		JSONObject jsonObject = JSONObject.fromObject(temp);
		JSONArray jsonList = jsonObject.getJSONArray("products");
		for (int i = 0; i < jsonList.size(); i++) {
			Craw_customerweb_goods_info info=new Craw_customerweb_goods_info();
			JSONObject itemJson = JSONObject.fromObject(jsonList.toArray()[i].toString());
			String egoodsId=itemJson.get("id").toString();
			try {
			  price=itemJson.getString("price").replaceAll("0", "").trim();
			} catch (Exception e) {
				 price="";
				logger.info("价格抓取失败"+e.toString() +message.get("url"));
			 
			}
			JSONArray  json = itemJson.getJSONArray("xmedia");
			String image=JSONObject.fromObject(json.toArray()[json.size()-1]).get("path").toString();
			String imageName=JSONObject.fromObject(json.toArray()[json.size()-1]).get("name").toString();
			image="http://static.zara-static.cn/photos//"+image+"/w/400/"+imageName+".jpg";
			String goods_name=itemJson.getJSONObject("detail").getString("name");
			String model=itemJson.getJSONObject("detail").getJSONObject("reference").getString("model").toString();
			String quality=itemJson.getJSONObject("detail").getJSONObject("reference").getString("quality").toString();
			String goods_url="https://www.zara.cn/cn/zh/"+URLEncoder.encode(goods_name, "UTF-8")+"-p0"+model+quality+".html?v1="+egoodsId+"";
			info.setEgoods_id(egoodsId);
			info.setCategories(message.get("categories").toString());
			info.setSeries(message.get("series").toString());
			info.setGoods_name(goods_name);
			info.setGoods_pic_url(image);
			info.setGoods_url(goods_url);
			info.setGoods_price(price);
			info.setUserid(Integer.valueOf(message.get("accountId")));
			list1.add(info);
				 
		} 
		logger.info("插入商品个数："+list1.size());
		
		if(list1.size()>0){
		logger.info("插入商品个数："+list1.size());
		List<List<Craw_customerweb_goods_info>> listData=Lists.partition(list1, 100);
		for(List<Craw_customerweb_goods_info> infoList:listData){
			crawlerPublicClassMapper.AaraInsertdetails(infoList); 
		}
		
		
		} 
	}
	//同步数据
	public void SynchronousData(String userId){
		crawlerPublicClassMapper.Zara_synchronousData(Integer.valueOf(userId));
	}
}
