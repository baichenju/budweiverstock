package com.eddc.service.impl.crawl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.eddc.service.AbstractProductAnalysis;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Craw_goods_attribute_Info;
import com.eddc.model.Parameter;
import com.eddc.util.Fields;
import com.eddc.util.StringHelper;
import com.eddc.util.Validation;
import com.google.common.collect.Lists;

/**
 * 项目名称：Price_monitoring_crawler
 * 类名称：GuoMei_DataCrawlService
 * 类描述：
 * 创建人：jack.zhao
 * 创建时间：2017年11月22日 上午10:36:33
 * 修改人：jack.zhao
 * 修改时间：2017年11月22日 上午10:36:33
 * 修改备注：
 *
 * @author jack.zhao
 */
@Service("gomeDataCrawlService")
public class GomeDataCrawlService extends AbstractProductAnalysis {

    private static Logger logger = LoggerFactory.getLogger(GomeDataCrawlService.class);

    /*****************************************解析数据****************************************************************************************************/

    @Override
    public Map<String, Object> getProductLink(Parameter parameter) {
        Map<String, Object> itemCome = new HashMap<>(10);
        //设置地区
		if(StringUtils.isNotEmpty(parameter.getDeliveryPlace().getDelivery_place_code())){
			parameter.setCookie(parameter.getDeliveryPlace().getDelivery_place_code());
		}
		parameter.setEgoodsId(parameter.getEgoodsId().replace("product-","").replace("a","A").trim());
		parameter.setItemUtl_2(Fields.GUOMEI_PC_URL + parameter.getEgoodsId()+ ".html");
		parameter.setItemUrl_1(Fields.GUOMEI_PC_URL + parameter.getEgoodsId() + ".html");


        //请求数据
		itemCome = getProductRequest(parameter);

        return itemCome;
    }

    @Override
    public Map<String, Object> getProductItemAnalysisData(String StringMessage, Parameter parameter) throws Exception {

        Map<String, Object> itemMap = new HashMap<String, Object>(10);
        //价格
        List<Map<String, Object>> insertPriceListApp = Lists.newArrayList();
        //详情
        List<Map<String, Object>> insertItemList = Lists.newArrayList();
        //商品属性
        List<Map<String, Object>> attributeList = Lists.newArrayList();

        Craw_goods_InfoVO inform =productInfo(parameter);
        Craw_goods_Price_Info priceInfo =productInfoPrice(parameter);
        String sellerName = Fields.PROPRIETARY;
        String postageFree = Fields.DONTPACK_MAIL;
		String shopType = Fields.PROPRIETARY;
		String platform_sellername="";
		String pricepromotion="";
		String productPricePc="";
		StringBuilder buffer = new StringBuilder();
        SimpleDateFormat formats = new SimpleDateFormat("E");
        SimpleDateFormat time = new SimpleDateFormat("H");
        String week = formats.format(new Date());
        String hour = time.format(new Date());


        //价格封装
        priceInfo.setSKUid(parameter.getEgoodsId());
        priceInfo.setChannel(Fields.CLIENT_MOBILE);


        try {
            Document docs = Jsoup.parse(StringMessage);
            if (docs.toString().contains("j-bpic-b")) {

                String productName = docs.getElementsByClass("j-bpic-b").attr("alt");
                String image = "http:" + docs.getElementsByClass("j-bpic-b").attr("jqimg");
                String shopName = docs.getElementsByClass("identify").text();
                if (shopName.contains("自营")) {
                    sellerName = Fields.YES_PROPRIETARY;
                }
               String shopid = docs.getElementsByClass("live800-service normal dn").attr("customer-service-button-id").toString();
                if (StringUtils.isEmpty(shopid)) {
                    shopid = StringHelper.getResultByReg(StringMessage, "stid\":\"([^,\"]+)\"");
                    if (StringUtils.isEmpty(shopid)) {
                        shopid = docs.getElementsByClass("contrasts-wrapper shareDB").attr("cid").toString();
                        if (StringUtils.isNotBlank(shopid)) {
                            shopid = shopid.split("/")[1];
                        }
                    }
                }

                String egoodsId[] = parameter.getEgoodsId().split("-");
                if (StringUtils.isNotBlank(docs.getElementsByClass("name").text())) {
                    platform_sellername = docs.getElementsByClass("name").first().text();
                    if (StringUtils.isEmpty(platform_sellername)) {
                        if (sellerName.contains("ziying")) {
                            platform_sellername = sellerName;
                        }
                    }
                }
                inform.setPlatform_shoptype(sellerName);
                inform.setPlatform_goods_name(productName);
                inform.setGoods_pic_url(image);
                inform.setPlatform_sellername(platform_sellername);
                inform.setPlatform_shopname(platform_sellername);
                inform.setPlatform_shopid(shopid);
                inform.setPlatform_sellerid(shopid);
                //获取商品的规格属性
                String dateWeek = parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[0];
                if (dateWeek.contains(week)) {
                    String dataTime = parameter.getListjobName().get(0).getCommodity_attribute().split("@@")[1];
                    if (dataTime.contains(hour)) {
                        List<Element> list = docs.getElementsByClass("specbox").select("li");
                        for (int kk = 0; kk < list.size(); kk++) {
                            try {
                                if (list.get(kk).toString().contains("specinfo")) {
                                    List<Element> span = list.get(kk).select("span");
                                    Craw_goods_attribute_Info info = attributeInfo(span.get(0).text(), span.get(1).text(), parameter);           
                                    /**封装数据 属性**/
                                    attributeList.addAll(list(info));                                    
                                }
                            } catch (Exception e) {
                            	logger.error("==>>解析商品规格属性："+e+"<<==");
                                e.printStackTrace();
                            }
                        }
                    }
                }

                //获取库存详情以及商品价格
                parameter.setCoding(parameter.getCoding());
                parameter.setItemUrl_1(Fields.GUOMEI_APP_STOCK + egoodsId[0] + "&skuID=" + egoodsId[1] + "&shopId=" + shopid + "&shopType=&provinceId=21000000&cityId=21010000&districtId=21011100&townId=210111002&modelId=&stid=" + shopid + "&mid=&isFirst=Y&isPresent=0&ajax=1&_=1517191614144");
                //请求促销
                String stockMessage = getRequest(parameter);
                StringBuilder promotionSb = new StringBuilder();
                if (!Validation.isEmpty(stockMessage)) {
                    JSONObject json = JSONObject.parseObject(stockMessage);
                    //判断是否存在拼团活动
                    if (json.getJSONObject("groupInfo").getJSONObject("fightGroupInfo").containsKey("basePrice")) {
                        //判断拼团活动是否有优惠价
                        if (json.getJSONObject("groupInfo").getJSONObject("fightGroupInfo").containsKey("basePrice")
                                && json.getJSONObject("groupInfo").getJSONObject("fightGroupInfo").containsKey("ptPrice")) {
                            priceInfo.setCurrent_price(json.getJSONObject("groupInfo").getJSONObject("fightGroupInfo").getString("basePrice").substring(1));
                            priceInfo.setOriginal_price(json.getJSONObject("groupInfo").getJSONObject("fightGroupInfo").getString("basePrice").substring(1));
                            promotionSb.append("拼团价:").append(json.getJSONObject("groupInfo").getJSONObject("fightGroupInfo").getString("ptPrice").substring(1));
                        }
                    } else {
                        priceInfo.setCurrent_price(json.getJSONObject("stock").getString("skuPrice"));
                        priceInfo.setOriginal_price(json.getJSONObject("stock").getString("skuPrice"));
                    }

                    if (json.getJSONObject("addr").getString("stockDesc").contains("采购中")) {
                        inform.setInventory(Fields.IS_NOT_STOCK);
                        priceInfo.setInventory(Fields.IS_NOT_STOCK);
                    } else {
                        inform.setInventory(json.getJSONObject("addr").getString("stockDesc"));
                        priceInfo.setInventory(json.getJSONObject("addr").getString("stockDesc"));

                    }

                    postageFree = json.getJSONObject("i").getString("freight");
                    if (postageFree.contains(Fields.FREE_SHIPPING)) {
                        //是否包邮
                        postageFree = Fields.PACK_MAIL;
                    } else if (postageFree.contains("运费1.0元")) {
                        //是否包邮
                        postageFree = Fields.DONTPACK_MAIL;
                    }
                    inform.setDelivery_info(postageFree);

                    if (json.getJSONObject("stock").getJSONObject("rebate").containsKey("buy")) {
                        pricepromotion = json.getJSONObject("stock").getJSONObject("rebate").getJSONObject("buy").getString("desc").toString();

                    }
                    parameter.setItemUrl_1(Fields.GUOMEI_APP_PROMOTION + egoodsId[0] + "&skuID=" + egoodsId[1] + "&skuPrice=" + priceInfo.getCurrent_price() + "&shopId=" + shopid + "&isShop=1&isBbc=N&natural=1&istlp=0&isPT=0&_=" + System.currentTimeMillis());
                    String regex = ".*[a-zA-Z]+.*";
                    Matcher m = Pattern.compile(regex).matcher(parameter.getEgoodsId());
                    if (!m.matches()) {
                        parameter.setCookie("__ugk=8abc5b7907ab4e07b3ed1ed1f8646ed6; awaken=true; global_key=2cc3e1f9edcc4f5a96c3820ec90eba81; gpid=11000000; gcid=11010000; gdid=11010200; gtid=110102002; organizationId=1001; addr_lat=39.92654605581156; addr_lng=116.44901575951573; addr_id=\"\"; addr_name=\"\"; uid=CjqxO18iaDqerzCuDgyMAg==; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%221739e6727632fe-0baf06e4de5108-4353761-1049088-1739e672764a57%22%2C%22%24device_id%22%3A%221739e6727632fe-0baf06e4de5108-4353761-1049088-1739e672764a57%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_referrer%22%3A%22%22%2C%22%24latest_referrer_host%22%3A%22%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%7D%7D; _idusin=84775472098; gm_sid=z2iku7b926yrbeadavm9n8q9y0mwx0exawh15961640231; http_referer=%2F%2Fu.m.gome.com.cn%2Fmy_gome.html; ufpd=222cbde2a4af5ba0af7a737b77a3dcdad7699593571bdcc6530151ef9da71e9b9a2472a1cfcf541aabf7712e183ae7eff5fa93e14cca25667c80c542569799c9; ufpd=222cbde2a4af5ba0af7a737b77a3dcdad7699593571bdcc6530151ef9da71e9b9a2472a1cfcf541aabf7712e183ae7eff5fa93e14cca25667c80c542569799c9; ctx=app-shangcheng|ver-v7.0.0|plt-wap|cmpid-; loginStatus=N; userId=\"\"");
                    } else {
                        parameter.setItemUrl_1(Fields.GUOMEI_APP_PROMOTION + egoodsId[0] + "&skuID=" + egoodsId[1] + "&skuPrice=" + priceInfo.getCurrent_price() + "&isShop=1&isBbc=Y&natural=1&istlp=0&isPT=0&_=" + System.currentTimeMillis());
                    }
                    //数据请求
                    String promotion = getOkHttpClient(parameter);

                    if (!Validation.isEmpty(promotion) && promotion.contains("{") && promotion.contains("}")) {
                        String data = promotion.substring(promotion.indexOf("{"), promotion.lastIndexOf("}") + 1);
                        JSONObject currPriceJson = JSONObject.parseObject(data);
                        if (currPriceJson.getJSONObject("ticket").toString().contains(Fields.ENTRYLIST)) {
                            JSONArray jsons = currPriceJson.getJSONObject("ticket").getJSONArray("entrylist");
                            for (int i = 0; i < jsons.size(); i++) {
                                JSONObject jsonPromotion = JSONObject.parseObject(jsons.toArray()[i].toString());
                                if (Validation.isEmpty(pricepromotion)) {
                                    pricepromotion = jsonPromotion.getString("desc");
                                } else {
                                    pricepromotion += "&&" + jsonPromotion.getString("desc");
                                }
                            }
                        }
                        if (currPriceJson.getJSONObject("rated").containsKey("allNum")) {
                            inform.setTtl_comment_num(currPriceJson.getJSONObject("rated").getString("allNum"));
                        }
                    }

                    if (promotionSb.toString().contains("拼团价")) {
                        pricepromotion += promotionSb.toString();
                    }

                    priceInfo.setPromotion(pricepromotion);


                    if (parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")) {
                        //请求pc端价格 区分自营非自营价格接口
                        String shop = "pop";
                        if (shopid.contentEquals("G001")) {
                            shop = "fshop";
                        }
                        parameter.setItemUrl_1("https://ss.gome.com.cn/item/v1/d/m/store/unite/" + shopid + "/" + shop + "/" + egoodsId[0] + "/" + egoodsId[1] + "/11010000/N/11010200/110102002/null/1/flag/item");
                        String price = getOkHttpClient(parameter);
                        if (price.toString().contains("salePrice")) {
                            JSONObject currPriceJson = JSONObject.parseObject(price);
                            productPricePc = currPriceJson.getJSONObject("result").getJSONObject("gomePrice").getString("salePrice");
                        }

                    }
                }

            }


            if (StringUtils.isNotEmpty(priceInfo.getCurrent_price())) {

                if (parameter.getListjobName().get(0).getClient().equalsIgnoreCase("all")) {
                    for (int k = 0; k < 2; k++) {
                        if (k == 0) {
                            priceInfo.setChannel(Fields.CLIENT_MOBILE);
                        } else {
                            priceInfo.setChannel(Fields.CLIENT_PC);
                            if (StringUtils.isEmpty(productPricePc)) {
                                productPricePc = priceInfo.getCurrent_price();
                            }
                            priceInfo.setOriginal_price(productPricePc);
                            priceInfo.setCurrent_price(productPricePc);
                        }
                        if (priceInfo.getCurrent_price().equalsIgnoreCase("0.0")) {
                            inform.setGoods_status(Fields.STATUS_COUNT);
                        }
                        
                        /**封装数据 价格**/
                        insertPriceListApp.addAll(list(priceInfo));
                        
                        //insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
                        //insertPriceListPC.add(insertPriceMap);
                        //insertPriceListApp.addAll(insertPriceListPC);
                    }
                } else {
                    if (priceInfo.getCurrent_price().equalsIgnoreCase("0.0")) {
                        inform.setGoods_status(Fields.STATUS_COUNT);
                    }
                    priceInfo.setChannel(Fields.CLIENT_MOBILE);
                    
                    /**封装数据 价格**/
                    insertPriceListApp.addAll(list(priceInfo));
                    
                   // insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
                   // insertPriceListApp.add(insertPriceMap);
                }

               // insertItemMap = BeanMapUtil.convertBean2MapWithUnderscoreName(inform);
               // insertItemList.add(insertItemMap);
                /**封装数据 详情**/
                insertItemList.addAll(list(inform));
                
                //判断商品是批量抓取还是 实时抓取 1,批量 ；4 批量
                if (parameter.getListjobName().get(0).getStorage().equalsIgnoreCase("4")) {
                    getProductSave(insertItemList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_INFO);
                    getProductSave(insertPriceListApp, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PRICE_INFO);

                    if (attributeList != null && attributeList.size() > 0) {
                        getProductSave(attributeList, parameter.getListjobName().get(0).getDatabases(), Fields.CRAW_GOODS_ATTRIBUTE_INFO);
                    }
                } else {
                    //详情
                    itemMap.put("itemList", insertItemList);
                    itemMap.put("itemPriceList", insertPriceListApp);
                    itemMap.put("attributesList", attributeList);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return itemMap;
    }

    //获取商品属性信息
    public List<Map<String, Object>> commodityProperty(Parameter parameter) throws Exception {
        List<Map<String, Object>> attributeList = Lists.newArrayList();
        String egoodsId[] = parameter.getEgoodsId().split("-");
        String url = "https://item.m.gome.com.cn/product/detail?goodsNo=" + egoodsId[0] + "&skuID=" + egoodsId[0] + "&ajax=1&_=" + System.currentTimeMillis();
        parameter.setItemUrl_1(url);
        parameter.setItemUtl_2(parameter.getItemUtl_2());
        String commodity = getRequest(parameter);
        if (!Validation.isEmpty(commodity)) {
            String html = StringHelper.escapeHtml(commodity);
            Document doc = Jsoup.parseBodyFragment(html.replace("\\", ""));
            List<Element> title = doc.getElementsByClass("parameter").select("tr");
            for (int k = 0; k < title.size(); k++) {
                if (!title.get(k).toString().contains("bg title")) {
                   // Map<String, Object> insertItem = new HashMap<String, Object>();
                    Craw_goods_attribute_Info info = attributeInfo(title.get(k).getElementsByClass("bg").text(), title.get(k).getElementsByClass("bgv").text(), parameter);
                    //insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
                    //attributeList.add(insertItem);
                    /**封装数据**/
                    attributeList.addAll(list(info));
                }
            }
        }
        return attributeList;

    }

}
