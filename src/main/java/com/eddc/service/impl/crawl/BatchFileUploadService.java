/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.service 
 * @author: jack.zhao   
 * @date: 2018年6月5日 下午2:04:59 
 */
package com.eddc.service.impl.crawl;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Craw_goods_Vendor_Info;
import com.eddc.model.Craw_goods_Vendor_price_Info;
import com.eddc.model.Craw_goods_crowdfunding_price_Info;
import com.eddc.util.Fields;
import com.eddc.util.FtpUtil;
import com.eddc.util.SimpleDate;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：BatchFileUploadService   
 * 类描述：   批量上传文件
 * 创建人：jack.zhao   
 * 创建时间：2018年6月5日 下午2:04:59   
 * 修改人：jack.zhao   
 * 修改时间：2018年6月5日 下午2:04:59   
 * 修改备注：   
 * @version    
 *    
 */
@Service
public class BatchFileUploadService extends Thread {
	@Autowired
	SearchDataCrawlService search_DataCrawlService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;
	private static Logger logger = LoggerFactory.getLogger(BatchFileUploadService.class);
	@SuppressWarnings("rawtypes")
	public void getFutureCallback(ArrayList<Future> futureList) throws InterruptedException{
		ThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(10);
		ExecupteHp hpRunnable = new ExecupteHp(futureList);
		executor.execute(hpRunnable);
		executor.shutdown();
	}
	class ExecupteHp implements Runnable{
		@SuppressWarnings("rawtypes")
		private ArrayList<Future> futureList;
		@SuppressWarnings("rawtypes")
		public ExecupteHp(ArrayList<Future> futureList) {
			this.futureList=futureList;
		}
		@SuppressWarnings("unchecked")
		@Override
		public void run() {
			Future<Map<String, Object>> future=null;
			List<Craw_goods_InfoVO>goods_info=new ArrayList<Craw_goods_InfoVO>();
			List<Craw_goods_Price_Info>info_price=new ArrayList<Craw_goods_Price_Info>();
			List<Craw_goods_Vendor_Info>Vendor_info=new ArrayList<Craw_goods_Vendor_Info>();
			List<Craw_goods_Vendor_price_Info>Vendor_price=new ArrayList<Craw_goods_Vendor_price_Info>();
			List<Craw_goods_crowdfunding_price_Info>crowdfunding=new ArrayList<Craw_goods_crowdfunding_price_Info>();
			Connection session = sqlSessionTemplate.getSqlSessionFactory().openSession().getConnection();
			String path="";  
			String path_url=ResourceBundle.getBundle("docker").getString("path_url");
			path_url=path_url+SimpleDate.SimpleDateData().format(new Date()); 
			for(int i = 0;i<futureList.size();i++){
				try { 
					future=futureList.get(i);
					Map<String, Object> returnMap = future.get();
					Date date=SimpleDate.SimpleDateFormatData().parse(returnMap.get("timeDate").toString());
					path=path_url+"/"+SimpleDate.SimpleDateFormatDataHour().format(date)+"/"+SimpleDate.SimpleDateFormatDataHour().format(new Date())+"_"+returnMap.get("platform")+"_"+returnMap.get("accountId");	
					if(future.get().toString().contains(Fields.TABLE_CRAW_GOODS_INFO)){//通用表商品详情
						Craw_goods_InfoVO info=(Craw_goods_InfoVO) future.get().get(Fields.TABLE_CRAW_GOODS_INFO);	
						goods_info.add(info); 
					}
					if(future.get().toString().contains(Fields.TABLE_CRAW_GOODS_PRICE_INFO)){//通用表价格
						Craw_goods_Price_Info price=(Craw_goods_Price_Info) future.get().get(Fields.TABLE_CRAW_GOODS_PRICE_INFO);
						info_price.add(price);//价格
					}
					if(future.get().toString().contains("goods_info_price")){
						List<Craw_goods_Price_Info>list=(List<Craw_goods_Price_Info>)future.get().get("goods_info_price");
						for(int is=0;is<list.size();is++){
							Craw_goods_Price_Info info=list.get(is);
							if(future.get().get("client").toString().equalsIgnoreCase("ALL")){
								info.setChannel(Fields.CLIENT_PC);
								info_price.add(info);//价格
								info.setChannel(Fields.CLIENT_MOBILE);
								info_price.add(info);//价格
							}else if(future.get().get("client").toString().equalsIgnoreCase("MOBILE")){
								info.setChannel(Fields.CLIENT_MOBILE);
								info_price.add(info);//价格
							}	
						}
					}

					if(future.get().toString().contains(Fields.TABLE_CRAW_VENDOR_INFO)){//1688详情
						Craw_goods_Vendor_Info info=(Craw_goods_Vendor_Info) future.get().get(Fields.TABLE_CRAW_VENDOR_INFO);
						Vendor_info.add(info);
					}
					if(future.get().toString().contains("Vendor_priceList")){//1688价格
						List<Craw_goods_Vendor_price_Info>list=(List<Craw_goods_Vendor_price_Info>)future.get().get("Vendor_priceList");
						Vendor_price.addAll(list);
					}
					if(future.get().toString().contains("list_taobaozc")){//众筹
						List<Craw_goods_crowdfunding_price_Info>list=(List<Craw_goods_crowdfunding_price_Info>) future.get().get("list_taobaozc");
						List<Craw_goods_crowdfunding_price_Info>listjd=(List<Craw_goods_crowdfunding_price_Info>) future.get().get("list_jdzc");
						if(list.size()>0){
							crowdfunding.addAll(list);
						}
						if(listjd.size()>0){
							crowdfunding.addAll(listjd);
						}
					}
				} catch (Exception e) {  
					logger.info("插入信息失败"+e.getMessage());
				}
			}
			if(goods_info.size()>0){
				StringBuilder columnName = new StringBuilder();
				for(int i=0;i<goods_info.size();i++){
					String StrinagData;
					try {
						StrinagData = crawlerPublicClassService.convertBean(goods_info.get(i),session);
						columnName.append(StrinagData);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				commodityInformation(goods_info,path,columnName.toString());
				goods_info.clear();
			}
			if(info_price.size()>0){
				StringBuilder columnName = new StringBuilder();
				for(int i=0;i<info_price.size();i++){
					String StrinagData;
					try {
						StrinagData = crawlerPublicClassService.convertBean(info_price.get(i),session);
						columnName.append(StrinagData);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				commodityInformation(info_price,path,columnName.toString());
				info_price.clear();
			}
			if(Vendor_info.size()>0){
				StringBuilder columnName = new StringBuilder();
				for(int i=0;i<Vendor_info.size();i++){
					String StrinagData;
					try {
						StrinagData = crawlerPublicClassService.convertBean(Vendor_info.get(i),session);
						columnName.append(StrinagData);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				commodityInformation(Vendor_info,path,columnName.toString());
				Vendor_info.clear();
			}
			if(Vendor_price.size()>0){
				StringBuilder columnName = new StringBuilder();
				for(int i=0;i<Vendor_price.size();i++){
					String StrinagData;
					try {
						StrinagData = crawlerPublicClassService.convertBean(Vendor_price.get(i),session);
						columnName.append(StrinagData);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				commodityInformation(Vendor_price,path,columnName.toString());
				Vendor_price.clear();
			}
			if(crowdfunding.size()>0){
				StringBuilder columnName = new StringBuilder();
				for(int i=0;i<crowdfunding.size();i++){
					String StrinagData;
					try {
						StrinagData = crawlerPublicClassService.convertBean(crowdfunding.get(i), session);
						columnName.append(StrinagData);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				commodityInformation(crowdfunding,path,columnName.toString());
				crowdfunding.clear();
			}

		}
		//商品信息实例化 上传文件
		public <T> void commodityInformation(List<T>T,String path_url,String StrinagData){
			String table=T.get(0).getClass().getName().substring(T.get(0).getClass().getName().lastIndexOf(".")+1,T.get(0).getClass().getName().length());
			String path="";
			File file = new File(path_url);
			try {if (!file.exists()) {file.mkdirs();	} 
			if(T.toString().contains(Fields.CLIENT_PC)&& T.toString().contains(Fields.CLIENT_MOBILE)){
				path="//#channel-"+Fields.CLIENT_PC+"_"+Fields.CLIENT_MOBILE+"#"+table+"_"+SimpleDate.SimpleDateFormatDataTime().format(new Date())+".txt";	
			}else if(T.toString().contains(Fields.CLIENT_PC)){
				path="//#channel-"+Fields.CLIENT_PC+"#"+table+"_"+SimpleDate.SimpleDateFormatDataTime().format(new Date())+".txt";
			}else if(T.toString().contains(Fields.CLIENT_MOBILE)){
				path="//#channel-"+Fields.CLIENT_MOBILE+"#"+table+"_"+SimpleDate.SimpleDateFormatDataTime().format(new Date())+".txt";
			}else{
				path="//"+table+"_"+SimpleDate.SimpleDateFormatDataTime().format(new Date())+".txt";
			}
			 BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path_url+path,true)));
			 out.write(StrinagData+"\r\n");
			 out.close();
//			ObjectOutputStream objOutputStream = new ObjectOutputStream(new FileOutputStream(path_url+path));
//			objOutputStream.writeObject(StrinagData);//写入对象
//			objOutputStream.flush();
//			objOutputStream.close();
			FtpUtil.FtupUpload(path_url);//ftp上传文件
			}catch(Exception e){
				e.printStackTrace();
			}	
		}

	}
}
