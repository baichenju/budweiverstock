package com.eddc.service.impl.crawl;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.framework.TargetDataSource;
import com.eddc.mapper.CrawlerPublicClassMapper;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_InfoVO;
import com.eddc.model.Craw_goods_Vendor_Info;
import com.eddc.model.Craw_goods_Vendor_price_Info;
import com.eddc.model.Craw_goods_attribute_Info;
import com.eddc.model.Craw_goods_crowdfunding_Info;
import com.eddc.model.Craw_keywords_temp_Info_forsearch;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.task.Amazon_DataCrawlTask;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.Validation;
import com.eddc.util.publicClass;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

@Service("searchDataCrawlService")
public class SearchDataCrawlService {
	@Autowired
	private CrawlerPublicClassMapper crawlerPublicClassMapper;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	SqlSessionTemplate sqlSessionTemplate;
	@Autowired
	Amazon_DataCrawlTask amazon_DataCrawlTask;
	private String dataDsNameSpace = "com.eddc.mapper.CrawlerPublicClassMapper";
	private static Logger logger = LoggerFactory.getLogger(SearchDataCrawlService.class);

	//同步数据
	@TargetDataSource
	public void SynchronousData(List<QrtzCrawlerTable> listjobName, String database, String className, int sum) {
		try {
			crawlerPublicClassMapper.SearchSynchronousData(listjobName.get(0).getPlatform(), listjobName.get(0).getUser_Id(), className, sum);
		} catch (Exception e) {
		}
	}

	/**
	 * 项目名称：springmvc_crawler
	 * 类名称：GoodsProductDetails
	 * 类描述：解析页面信息
	 * 创建人：Jack
	 * 创建时间：2017年06月13日 上午10:50:10
	 * 修改备注：
	 *
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @version
	 */
	@SuppressWarnings("unchecked")
	@TargetDataSource
	public String ParseItemPage(String item, String database, CrawKeywordsInfo wordsInfo, Map<String, String> map) {
		Map<String, String> data = new HashMap<String, String>();
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> itemData = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		data.putAll(map);
		String disable = "";
		try {
			if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TMALL_EN) || wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TMALL_GLOBAL_EN) || wordsInfo.getPlatform_name().equals(Fields.PLATFORM_TAOBAO_EN)) {
				itemData = tmallDetailsData(item, wordsInfo, map.get(Fields.COUNT));
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}
			} else if (wordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD)) {
				itemData = jdDetailsData(item, wordsInfo, map);
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}
			} else if (wordsInfo.getPlatform_name().equals(Fields.PLATFORM_YHD)) {
				itemData = yhdDetailsData(item, wordsInfo, map);
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}
			} else if (wordsInfo.getPlatform_name().equals(Fields.PLATFORM_SUNING)) {
				itemData = suningDetailsData(item, wordsInfo, map.get(Fields.COUNT));
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}
			} else if (wordsInfo.getPlatform_name().equals(Fields.PLATFORM_AMAZON) || wordsInfo.getPlatform_name().equals(Fields.PLATFORM_AMAZON_UN)) {
				itemData = amzonDetailsData(item, wordsInfo, map.get(Fields.COUNT));
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}
			} else if (wordsInfo.getPlatform_name().equals(Fields.platform_1688)) {
				itemData = Details_1688Data(item, wordsInfo, map.get(Fields.COUNT));
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				disable = itemData.get(Fields.DISABLE).toString();
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
					if (disable.equals((map.get("count").toString()))) {
						disable = Fields.DISABLE;
					}
				}
			} else if (wordsInfo.getPlatform_name().equals(Fields.PLATFORM_TAOBAO_ZC)) {
				itemData = taobaoZcDetailsData(item, wordsInfo, map.get(Fields.COUNT));
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}
			} else if (wordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD_ZC)) {
				itemData = jdZcDetailsData(item, wordsInfo, map.get(Fields.COUNT));
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}
			} else if (wordsInfo.getPlatform_name().equals(Fields.PLATFORM_VIP)) {
				itemData = vipDetailsData(item, wordsInfo, map.get(Fields.COUNT));
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}
			} else if (wordsInfo.getPlatform_name().equals(Fields.PLATFORM_KAOLA)) {
				itemData = kaoLaDetailsData(item, wordsInfo, map.get(Fields.COUNT));
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}
			} else if (wordsInfo.getPlatform_name().equals(Fields.PLATFORM_JX)) {
				itemData = jxDetailsData(item, wordsInfo, map.get(Fields.COUNT));
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}

			} else if (wordsInfo.getPlatform_name().equals(Fields.PLATFORM_KIDSWANT) || wordsInfo.getPlatform_name().equals(Fields.PLATFORM_KIDSWANT_OFF)) {
				itemData = kidswantDetailsData(item, wordsInfo, map.get(Fields.COUNT));
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}
			} else if (wordsInfo.getPlatform_name().equals(Fields.PLATFORM_XIOUHUI)) {
				itemData = xiOuHuiDetailsData(item, wordsInfo, map.get(Fields.COUNT));
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}
			} else if (wordsInfo.getPlatform_name().equals(Fields.PLATFORM_YUOUHUI)) {
				itemData = yOuHuiDetailsData(item, wordsInfo, map.get(Fields.COUNT));
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}
			} else if (wordsInfo.getPlatform_name().equals(Fields.PLATFORM_SKS)) {
				itemData = sksDetailsData(item, wordsInfo, map.get(Fields.COUNT));
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}
			} else if (wordsInfo.getPlatform_name().equals(Fields.PLATFORM_HPK)) {
				itemData = hpkDetailsData(item, wordsInfo, map.get(Fields.COUNT));
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}
			} else if (wordsInfo.getPlatform_name().equals(Fields.GUOMEI)) {
				itemData = guoMeiDetailsData(item, wordsInfo, map.get(Fields.COUNT));
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}
			} else if (wordsInfo.getPlatform_name().equals("ehaier")) {
				itemData = ehaierDetailsData(item, wordsInfo, map.get(Fields.COUNT));
				itemPage = (List<Element>) itemData.get(Fields.ITMEPAGE);
				disable = itemData.get(Fields.DISABLE).toString();
				priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if (priceInfoJson.size() > 0) {
					priceInfoJson = (JSONArray) itemData.get(Fields.PRICEINFOJSON);
				}
			}else {
				disable = Fields.DISABLE;
				return disable;
			}
			insertData(itemPage, wordsInfo, map, priceInfoJson); //插入解析数据
		} catch (Exception e) {
			e.printStackTrace();
			disable = "";
		}
		return disable;
	}

	//天猫数据
	public Map<String, Object> tmallDetailsData(String item, CrawKeywordsInfo wordsInfo, String count) {
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		if (item.contains("J_TItems")) {
			item = item.toString().replaceAll("&quot;", "").replace("\\", "").replace("//", "");
			Document docs = Jsoup.parse(item);
			Elements J_TItems = docs.getElementsByClass("J_TItems");
			String message = J_TItems.toString().substring(J_TItems.toString().indexOf("J_TItems") - 12, J_TItems.toString().lastIndexOf("comboHd") - 13);
			Document J_TGoldData = Jsoup.parse(message.toString());
			disable = J_TGoldData.getElementsByClass("pagination").get(0).getElementsByTag("a").last().className().replace("\\", "");
			itemPage = J_TGoldData.getElementsByClass("item-name J_TGoldData");
			try {
				if (Validation.isNotEmpty(docs.getElementsByClass("ui-page-s-len").text())) {
					String page = docs.getElementsByClass("ui-page-s-len").text().split("/")[1];
					if (count.equalsIgnoreCase(page)) {
						disable = Fields.DISABLE;
					}
				}
			} catch (Exception e) {
				logger.error("【天猫搜索获取商品页数失败 error:{}】", e);
			}
		} else if (item.contains("J-listContainer")) {//天猫
			Document docs = Jsoup.parse(item);
			if (docs.toString().contains("product-list")) {
				String totalPage = docs.getElementById("totalPage").val();
				if (count.equals(totalPage)) {
					disable = Fields.DISABLE;
				}
				itemPage = docs.getElementById("J-listContainer").getElementsByClass("product-list").get(0).getElementsByClass("product");
			}
		} else if (item.contains("g_page_config")) {//tmall taobao search
			if (item.contains("i2iTags")) {
				Document docs = Jsoup.parse(item);
				String itemPageMessage = docs.toString().substring(+docs.toString().indexOf("itemlist"), docs.toString().lastIndexOf(",\"bottomsearch\":")).toString();
				itemPageMessage = "{\"" + itemPageMessage + "}";

				JSONObject currPriceJson = JSONObject.fromObject(itemPageMessage);
				priceInfoJson = currPriceJson.getJSONObject("itemlist").getJSONObject("data").getJSONArray("auctions");
				if (priceInfoJson.size() == 0) {
					disable = Fields.DISABLE;
				}
			} else {
				disable = Fields.DISABLE;
			}
		} else if (item.contains("J_ItemList")) {//天猫指定类目搜索解析
			Document docs = Jsoup.parse(item);
			itemPage = docs.getElementsByClass("product  ");
			if (itemPage.size() == 0) {
				disable = Fields.DISABLE;
			}
		} else if (item.contains("total_page")) {//tmall手机端根据店铺搜索商品
			String itemPageMessage = item.toString().substring(item.indexOf("{"), item.lastIndexOf("}") + 1);
			JSONObject currPriceJson = JSONObject.fromObject(itemPageMessage);
			priceInfoJson = currPriceJson.getJSONArray("items");
			if (priceInfoJson.size() == 0 || count.equalsIgnoreCase(currPriceJson.getString("total_page"))) {
				disable = Fields.DISABLE;
			}
		} else if (wordsInfo.getCust_keyword_url().contains("s.m.taobao.com") || item.contains("totalPage")) {
			JSONObject currPriceJson = JSONObject.fromObject(item.toString());
			if (currPriceJson.toString().contains("listItem")) {
				priceInfoJson = currPriceJson.getJSONArray("listItem");
				if (priceInfoJson.size() == 0 || count.equalsIgnoreCase(currPriceJson.getString("totalPage"))) {
					disable = Fields.DISABLE;
				}
			}

		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}

	//京东数据
	public Map<String, Object> jdDetailsData(String item, CrawKeywordsInfo wordsInfo, Map<String, String> map) {
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		if (item.contains("J_goodsList") || item.contains("gl-item")) {//京东pc 搜索
			Document docs = Jsoup.parse(item);
			itemPage = docs.getElementsByClass("gl-item");
			if (wordsInfo.getCust_keyword_url().contains("search.jd.com")) {
				if (itemPage.size() == 0) {
					disable = Fields.DISABLE;
				}
			} else if (wordsInfo.getCust_keyword_url().contains("list.jd.com")) {
				if (itemPage.size() == 0 || StringUtil.isEmpty(docs.getElementsByClass("p-num").toString())) {//
					disable = Fields.DISABLE;
				}
			}
			if (StringUtil.isNotEmpty(docs.getElementsByClass("fp-text").toString())) {
				if (map.get(Fields.COUNT).trim().contains(docs.getElementsByClass("fp-text").get(0).getElementsByTag("i").text())) {
					disable = Fields.DISABLE;
				} else if (Integer.valueOf(map.get(Fields.COUNT)) > Integer.valueOf(docs.getElementsByClass("fp-text").get(0).getElementsByTag("i").text())) {
					disable = Fields.DISABLE;
				}
			}
		} else if (item.contains("wareCount") || item.contains("wareList")) {
			String itmes = item.toString().replace("\\\"", "\"");
			String stockDesc = StringHelper.nullToString(StringHelper.getResultByReg(itmes, "\"wareCount\":([^}}]+)"));
			if (stockDesc.equals(Fields.STATUS_OFF) || !stockDesc.contains("wareId")) {
				disable = Fields.DISABLE;
			} else {
				String message = itmes.substring(itmes.indexOf("\"wareList\":{\""), itmes.lastIndexOf("}]}}\"}") + 4);
				JSONObject currPriceJsons = JSONObject.fromObject("{" + message);
				priceInfoJson = currPriceJsons.getJSONObject("wareList").getJSONArray("wareList");
			}
		}

		if (item.contains(Fields.SHOPNAME_NO)) {
			disable = Fields.DISABLE;
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}

	//一号店解析
	public Map<String, Object> yhdDetailsData(String item, CrawKeywordsInfo wordsInfo, Map<String, String> map) {//map.get(Fields.COUNT)
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		if (item.contains("middle") && item.contains("title_box")) {//yhd
			Document docs = Jsoup.parse(item);
			String touchweb = docs.getElementsByClass("touchweb_com-noresult J_ping").attr("report-eventparam").toString();
			if (touchweb.contains(Fields.NO_RESULT) || StringUtil.isEmpty(docs.select("li").text())) {
				disable = Fields.DISABLE;
			} else {
				itemPage = docs.select("li");
			}
		} else if (item.contains("mod_search_pro")) {
			StringBuffer su = new StringBuffer();
			try {
				JSONObject currPriceJson = JSONObject.fromObject(item);
				String value = currPriceJson.get("value").toString();
				Document docs = Jsoup.parse(value);
				Elements dom = docs.getElementsByClass("mod_search_pro");
				String dataMessage = dom.toString();
				su.append("<html><head>" + dataMessage + "</head></html>");
				Document jsoupDocument = Jsoup.parse(su.toString());
				itemPage = jsoupDocument.getElementsByClass("mod_search_pro");
			} catch (Exception e) {
				Document docs = Jsoup.parse(item);
				Document docss = Jsoup.parse(docs.outerHtml().replace("&quot;", ""));
				itemPage = docss.getElementsByClass("\\mod_search_pro\\");
				if (Validation.isEmpty(itemPage)) {
					itemPage = docss.getElementsByClass("mod_search_pro");
				}
				try {
					if (docs.toString().contains(Fields.NO_RESULT)) {
						disable = Fields.DISABLE;
					}
					if (map.get("kk").equalsIgnoreCase(Fields.COUNT_0)) {
						int page;
						try {
							Document pageDocm = Jsoup.parse(docs.outerHtml().replace("&quot;", ""));
							page = Integer.valueOf(pageDocm.getElementById("\\pageCountPage\\").val().replace("\\", "").replace("/", "") + 1);
						} catch (Exception e2) {
							page = Integer.valueOf(docs.getElementById("pageCountPage").val()) + 1;

						}
						if (map.get(Fields.COUNT).equals(String.valueOf(page))) {
							disable = Fields.DISABLE;
						}
					}
				} catch (Exception e2) {
				}

			}
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}

	//苏宁
	public Map<String, Object> suningDetailsData(String item, CrawKeywordsInfo wordsInfo, String count) {
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		if (item.contains("sellPoint")) {
			Document docs = Jsoup.parse(item);
			itemPage = docs.getElementsByClass("border-out");
			if (itemPage.contains("border-out")) {
				disable = Fields.DISABLE;
			}
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}

	//亚马逊
	public Map<String, Object> amzonDetailsData(String item, CrawKeywordsInfo wordsInfo, String count) {
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		Document docs = Jsoup.parse(item);
		if (wordsInfo.getCust_keyword_url().contains("www.amazon.com")) {
			if (wordsInfo.getCust_keyword_type().contains("keyword")) {
				itemPage = docs.getElementsByClass("s-result-item celwidget  ");
			} else if (wordsInfo.getCust_keyword_type().contains("category")) {
				if (docs.toString().contains(docs.getElementsByClass("a-row s-result-list-parent-container").toString())) {
					if (docs.getElementsByClass("a-row s-result-list-parent-container").toString().contains("li")) {
						disable = Fields.DISABLE;
					} else if (docs.getElementsByClass("a-row s-result-list-parent-container").toString().contains("li")) {
						itemPage = docs.getElementsByClass("a-row s-result-list-parent-container").get(0).select("li");
					} else if (docs.getElementsByClass("a-fixed-left-grid-col a-col-right").toString().contains("a-ordered-list a-vertical")) {
						itemPage = docs.getElementsByClass("a-ordered-list a-vertical").select("li");
					} else if (itemPage.size() == 0) {//zg_itemImmersion
						itemPage = docs.getElementsByClass("zg-item-immersion").select("li");
						if (itemPage.size() == 0) {
							itemPage = docs.getElementsByClass("zg_itemWrapper");
						}
					}
				} else if (docs.toString().contains(docs.getElementsByClass("a-ordered-list a-vertical").toString())) {
					if (docs.toString().contains(docs.getElementsByClass("a-ordered-list a-vertical").toString())) {
						itemPage = docs.getElementsByClass("a-ordered-list a-vertical").select("li");
						if (itemPage.size() == 0) {
							itemPage = docs.getElementsByClass("a-row s-result-list-parent-container").select("li");
							if (count.contains(docs.getElementsByClass("pagnDisabled").text())) {
								disable = Fields.DISABLE;
							}
						}
					} else {
						disable = Fields.DISABLE;
					}
				}
			}
			if (itemPage.size() == 0) {
				disable = Fields.DISABLE;
			}
		} else if (wordsInfo.getCust_keyword_url().contains("www.amazon.cn") && item.contains("a-row s-result-list-parent-container")) {
			if (docs.toString().contains(Fields.AMAZONCN_NO)) {
				disable = Fields.DISABLE;
			} else {
				itemPage = docs.getElementsByClass("a-row s-result-list-parent-container").get(1).select("li");
			}
		} else if (item.contains("zg-item-immersion") || item.contains("zg_itemWrapper")) {
			if (StringUtil.isNotEmpty(docs.getElementsByClass("a-row a-spacing-top-mini").text())) {
				if (docs.getElementsByClass("a-row a-spacing-top-mini").get(0).getElementsByTag("li").last().toString().contains("a-disabled a-last")) {
					disable = Fields.DISABLE;
				} else {
					String page = docs.getElementsByClass("a-row a-spacing-top-mini").get(0).getElementsByTag("li").last().getElementsByTag("a").attr("href").toString().split("&pg=")[1].toString();
					if (page.contains(count)) {
						disable = Fields.DISABLE;
					}
				}
			}
			itemPage = docs.getElementsByClass("zg-item-immersion");
			if (StringUtil.isEmpty(itemPage.toString())) {
				itemPage = docs.getElementsByClass("zg_itemImmersion");
				if (itemPage.toString().contains(Fields.AMAZONSUA_PAGE)) {
					disable = Fields.DISABLE;
				}
			}
		}

		if (itemPage.size() == 0) {
			try {
				disable = docs.getElementById("g").getElementsByTag("img").attr("alt").toString();
				if (disable.contains(Fields.AMAZONSUA_PAGE)) {
					disable = Fields.DISABLE;
				}
			} catch (Exception e) {
				logger.info("item 页面不存在");
			}
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}

	// 1688
	public Map<String, Object> Details_1688Data(String item, CrawKeywordsInfo wordsInfo, String count) {
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		if (wordsInfo.getCust_keyword_url().contains("offer_search.htm")) {
			if (item.toString().contains("s-widget-breadcrumb")) {
				try {
					Document docs = Jsoup.parse(item);
					itemPage = docs.getElementsByClass("sm-offer-item sw-dpl-offer-item");
					disable = docs.getElementsByClass("sm-widget-offer").get(0).getElementsByTag("em").text();
					if (StringUtils.isEmpty(disable)) {
						disable = Fields.DISABLE;
					} else if (Integer.valueOf(disable) < 20) {
						disable = Fields.DISABLE;
					}
				} catch (Exception e) {
					logger.info("【1688解析数据失败 error:{}】", e);
				}
			} else if (item.contains("i2ip4pinfo") || item.contains("offerResult")) {
				item = item.substring(item.indexOf("(") + 1, item.lastIndexOf(")"));
				try {
					JSONObject currPriceJson = JSONObject.fromObject(item);
					String content = currPriceJson.getJSONObject("content").getJSONObject("offerResult").getString("html");
					Document docs = Jsoup.parse(content);
					itemPage = docs.getElementsByClass("sm-offer-item sw-dpl-offer-item ");
				} catch (Exception e) {
					String message = item.substring(item.indexOf("content"), item.lastIndexOf(")"));
					message = "{\"" + message;
					JSONObject currPriceJsons = JSONObject.fromObject(message);
					String contenst = currPriceJsons.getJSONObject("content").getJSONObject("offerResult").getString("html");
					Document docs = Jsoup.parse(contenst);
					itemPage = docs.getElementsByClass("sm-offer-item sw-dpl-offer-item ");
					e.printStackTrace();
					logger.error("【解析数据失败 error:{}】", e);
				}
			} else {
				Document docs = Jsoup.parse(item);
				itemPage = docs.getElementsByClass("sm-offer ").select("li");
				if (StringUtil.isNotEmpty(docs.getElementsByClass("sw-dpl-tip-guide").text())) {
					disable = Fields.DISABLE;
				}
			}
			if (itemPage.size() == 0) {
				disable = Fields.DISABLE;
			}
		} else if (wordsInfo.getCust_keyword_url().contains("m.1688.com")) {
			int recordCount = 0;
			int pagen = 0;
			int pages = 0;
			item = item.substring(item.indexOf("(") + 1, item.lastIndexOf(")"));
			JSONObject jsonObject = JSONObject.fromObject(item);
			String page = jsonObject.getJSONObject("data").getString("found");
			if (Integer.valueOf(page) > 0) {
				recordCount = Integer.valueOf(page);
				pagen = recordCount % 20;
				pages = recordCount / 20;
				if (pagen != 0) {
					recordCount = pages + 1;
				} else {
					recordCount = pages;
				}
			}
			disable = String.valueOf(recordCount);
			priceInfoJson = jsonObject.getJSONObject("data").getJSONArray("offers");
		} else {
			Document docs = Jsoup.parse(item);
			itemPage = docs.getElementsByClass("sm-offerShopwindow3 high");
			if (itemPage.size() == 0) {
				disable = Fields.DISABLE;
			}
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}

	//封装 淘宝众筹信息
	public Map<String, Object> taobaoZcDetailsData(String item, CrawKeywordsInfo wordsInfo, String count) {
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		if (item.contains("recommendPriceStr")) {
			JSONObject currPriceJson = JSONObject.fromObject(item);
			priceInfoJson = currPriceJson.getJSONArray("data");
		} else {
			disable = Fields.DISABLE;
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}

	//封装 京东众筹信息
	public Map<String, Object> jdZcDetailsData(String item, CrawKeywordsInfo wordsInfo, String count) {
		Map<String, Object> data = new HashMap<String, Object>();
		List<Element> itemPage = new ArrayList<Element>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		if (item.contains("support")) {
			Document docs = Jsoup.parse(item);
			itemPage = docs.getElementsByClass("info type_now");
			if (itemPage.size() == 0) {
				itemPage = docs.getElementsByClass("infos clearfix");
				if (docs.toString().contains("info type_succeed")) {
					List<Element> itemPagedata = docs.getElementsByClass("info type_succeed");
					itemPage.addAll(itemPagedata);
				}
				if (docs.toString().contains("info type_xm")) {
					List<Element> itemPagetype = docs.getElementsByClass("info type_xm");
					itemPage.addAll(itemPagetype);
				}
				if (itemPage.size() == 0) {
					disable = Fields.DISABLE;
				}
			}
		} else {
			disable = Fields.DISABLE;
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}

	//封装 唯品会信息
	public Map<String, Object> vipDetailsData(String item, CrawKeywordsInfo wordsInfo, String count) {
		Map<String, Object> data = new HashMap<String, Object>();
		List<Element> itemPage = new ArrayList<Element>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		try {
			if (item.contains("price_info")) {
				String warehouse = item.toString().substring(item.toString().indexOf("pageCount") - 1, item.toString().lastIndexOf("countryFlag") - 2);
				warehouse = "{" + warehouse + "}]}";
				JSONObject currPriceJson = JSONObject.fromObject(warehouse);
				priceInfoJson = currPriceJson.getJSONArray("products");
			} else {
				disable = Fields.DISABLE;
			}
		} catch (Exception e) {
			logger.error("【Failed encapsulation data error:{}】", e);
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}

	//封装 考拉信息
	public Map<String, Object> kaoLaDetailsData(String item, CrawKeywordsInfo wordsInfo, String count) {
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		if (item.contains("searchresult")) {
			Document docs = Jsoup.parse(item);
			itemPage = docs.getElementsByClass("m-result").get(0).getElementsByClass("goods");
			if (itemPage.size() == 0) {
				disable = Fields.DISABLE;
			}
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}

	//孩子王
	public Map<String, Object> kidswantDetailsData(String item, CrawKeywordsInfo wordsInfo, String count) {
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		try {
			if (item.contains("metaAttrs")) {
				JSONObject currPriceJson = JSONObject.fromObject(item);
				priceInfoJson = currPriceJson.getJSONObject("content").getJSONObject("products").getJSONArray("rows");
			} else {
				disable = Fields.DISABLE;
			}
		} catch (Exception e) {
			logger.error("【Failed encapsulation data error:{}】", e);
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}

	//封装 酒仙网信息
	public Map<String, Object> jxDetailsData(String item, CrawKeywordsInfo wordsInfo, String count) {
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		if (item.contains("proListSearch")) {
			Document docs = Jsoup.parse(item);
			itemPage = docs.getElementsByClass("clearfix").select("li[product-box]");
			if (itemPage.size() == 0) {
				//docs.getElementsByClass("clearfix").get(0).getElementsByClass("totalPage").get(0).getElementsByTag("em").text();
				disable = Fields.DISABLE;
			} else {
				if (docs.toString().contains("totalPage")) {
					String totalPage = docs.getElementsByClass("totalPage").get(0).getElementsByTag("em").text().toString();
					if (count.equals(totalPage)) {
						disable = Fields.DISABLE;
					}
				} else {
					disable = Fields.DISABLE;
				}

			}
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}

	//国美
	public Map<String, Object> guoMeiDetailsData(String item, CrawKeywordsInfo wordsInfo, String count) {
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		if (item.contains("html")) {

			JSONObject json = JSONObject.fromObject(item);
			String message=json.getString("html");
			Document docs = Jsoup.parse(message);
			//System.out.print(docs.toString());
			itemPage = docs.getElementsByClass("gd_list").select("li");
			if (itemPage.size() == 0) {
				disable = Fields.DISABLE;
			} else {
				if (json.toString().contains("totalPage")) {
					String totalPage =json.getString("totalPage");
					if (count.equals(totalPage)) {
						disable = Fields.DISABLE;
					}
				} else {
					disable = Fields.DISABLE;
				}

			}
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}


	public Map<String, Object> xiOuHuiDetailsData(String item, CrawKeywordsInfo wordsInfo, String count) {
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		if (item.contains("list_tab")) {
			Document docs = Jsoup.parse(item);
			itemPage = docs.getElementsByClass("table_list clearfix").select(".content");
			if (itemPage.size() == 0) {
				disable = Fields.DISABLE;
			} else {
				if (docs.toString().contains("paging")) {
					List<Element> listPage = new ArrayList<Element>();
					String page = "0";
					if (docs.getElementsByClass("paging").toString().contains("href")) {
						listPage = docs.getElementsByClass("paging").get(0).getElementsByTag("a");
						page = docs.getElementsByClass("paging").get(0).getElementsByTag("a").get(listPage.size() - 2).text();
					} else if (docs.getElementsByClass("paging").toString().contains("current")) {
						page = docs.getElementsByClass("current").text();
					}
					if (count.equals(page)) {
						disable = Fields.DISABLE;
					}
				} else {
					disable = Fields.DISABLE;
				}

			}
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}


	/**
	 * 渝欧汇
	 */
	public Map<String, Object> yOuHuiDetailsData(String item, CrawKeywordsInfo wordsInfo, String count) {
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		if (item.contains("list-product-box")) {//list-sort-bar-paging
			Document docs = Jsoup.parse(item);
			String pageMessage = docs.getElementsByClass("list-sort-bar-paging").get(0).getElementsByTag("span").last().text().replace("/ ", "");
			itemPage = docs.getElementsByClass("list-product-box").select("li");
			if (itemPage.size() == 0) {
				disable = Fields.DISABLE;
			}
			if (count.equals(pageMessage)) {
				disable = Fields.DISABLE;
			}
		} else if (item.contains("没有找到相关记录")) {
			disable = Fields.DISABLE;
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}

	/**
	 * 圣卡斯
	 */
	public Map<String, Object> sksDetailsData(String item, CrawKeywordsInfo wordsInfo, String count) {
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		try {
			if (item.contains("totalCount")) {
				JSONObject currPriceJson = JSONObject.fromObject(item);
				String page=currPriceJson.getJSONObject("data").getString("totalPageCount");
				if(Integer.valueOf(page)>=100){
					disable = Fields.DISABLE;
				}else{
					priceInfoJson=currPriceJson.getJSONObject("data").getJSONArray("result");
				}
				if (count.equals(page)) {
					disable = Fields.DISABLE;
				}

			}
			//			if(StringUtils.isNotEmpty(item)){
			//				Document docs = Jsoup.parse(item);
			//				if(item.contains("非常抱歉，没有找到相关商品")){
			//					disable = Fields.DISABLE;
			//				}else{
			//					String page = docs.getElementsByClass("pageall").text();
			//					String search_total = docs.getElementsByClass("search_total").select("font").last().text();
			//					String error = docs.getElementsByClass("error").text();
			//					itemPage = docs.getElementsByClass("GoodsSearchWrap").select("td[id]");
			//					if (error.contains("非常抱歉，没有找到相关商品")) {
			//						logger.info("当前搜索的商品不存在");
			//						disable = Fields.DISABLE;
			//					} else {
			//						if (itemPage.size() == 0) {
			//							disable = Fields.DISABLE;
			//						}
			//						if (Integer.valueOf(search_total) > 20) {
			//							logger.info("当前商品一共：" + page + " 页， 共 " + search_total + " 个商品");
			//						} else {
			//							page = "1";
			//							logger.info("当前商品一共：1 页  ， 共 " + search_total + " 个商品 ");
			//						}
			//						if (count.equals(page)) {
			//							disable = Fields.DISABLE;
			//						}
			//					}
			//				}
			//
			//			}
		} catch (Exception e) {
			logger.error("==>>搜索    解析数据发生异常："+e.getMessage()+"<<==");
			e.printStackTrace();
		}	
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}

	/**
	 *海拍客
	 */
	public Map<String, Object> hpkDetailsData(String item, CrawKeywordsInfo wordsInfo, String count) {
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		try {
			if (item.contains("totalCount")) {
				JSONObject currPriceJson = JSONObject.fromObject(item);
				String page=currPriceJson.getJSONObject("data").getString("totalPage");
				if(Integer.valueOf(page)>=100){
					disable = Fields.DISABLE;
				}else{
					priceInfoJson=currPriceJson.getJSONObject("data").getJSONArray("itemList");
				}
				if (count.equals(page)) {
					disable = Fields.DISABLE;
				}

			}
		} catch (Exception e) {
			logger.info("解析json发生异常:"+e);
			e.printStackTrace();
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}

	/**
	 *海尔
	 */
	public Map<String, Object> ehaierDetailsData(String item, CrawKeywordsInfo wordsInfo, String count) {
		List<Element> itemPage = new ArrayList<Element>();
		Map<String, Object> data = new HashMap<String, Object>();
		JSONArray priceInfoJson = new JSONArray();
		String disable = "";
		try {
			if (item.contains("totalCount")) {
				JSONObject currPriceJson = JSONObject.fromObject(item);
				String page="1";
				priceInfoJson=currPriceJson.getJSONObject("data").getJSONArray("productList");
				if (count.equals(page) && priceInfoJson.size()==0) {
					disable = Fields.DISABLE;
				}

			}
		} catch (Exception e) {
			logger.info("解析json发生异常:"+e);
			e.printStackTrace();
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}


	//天猫解析数据
	@SuppressWarnings("unused")
	public Map<String, Object> tmallMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, Map<String, String> map, int sum, int number) {
		Map<String, Object> messageData = new HashMap<String, Object>();
		String comment_count = "";
		String keywordName = "";
		String image = "";
		String goodsName = "";
		String price = "";
		String item_loc = "";
		String view_sales = "";
		String platform_shoptype = "";
		String goodsUrl = "";
		String platform_shopname = "";
		String user_id = "";
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		if (!Validation.isEmpty(element)) {
			if (element.toString().contains("item-name J_TGoldData")) {//天猫旗舰店商品搜索
				keywordName = element.getElementsByClass("item-name J_TGoldData").attr("atpanel").toString().replace("\\", "").toString().split(",")[1].toString();
				goodsUrl = element.getElementsByClass("item-name J_TGoldData").attr("href").toString();
				goodsName = element.getElementsByClass("item-name J_TGoldData").text();
			} else if (element.toString().contains("data-itemid")) {//天猫超市搜索
				keywordName = element.getElementsByClass("product").attr("data-itemid").toString();
				price = element.getElementsByClass("ui-price").get(0).getElementsByTag("strong").text();
				goodsUrl = element.getElementsByClass("product-title").get(0).getElementsByTag("a").attr("href");
				goodsName = element.getElementsByClass("product-title").get(0).getElementsByTag("a").text();
				image = element.getElementsByClass("product-img").get(0).getElementsByTag("img").attr("src").toString();
				view_sales = element.getElementsByClass("item-sum").get(0).getElementsByTag("strong").text();
			} else if (element.toString().contains("product") && element.toString().contains("product-iWrap")) {
				keywordName = element.attr("data-id").toString();//商品egoodsId
				image = element.getElementsByClass("productImg-wrap").get(0).getElementsByTag("img").attr("src").toString();
				if (StringUtil.isEmpty(image)) {
					image = element.getElementsByClass("productImg-wrap").get(0).getElementsByTag("img").attr("data-ks-lazyload").toString();
				}
				goodsUrl = element.getElementsByClass("productImg-wrap").get(0).getElementsByTag("a").attr("href").toString();
				price = element.getElementsByClass("productPrice").get(0).getElementsByTag("em").attr("title");
				goodsName = element.getElementsByClass("productTitle").get(0).getElementsByTag("a").attr("title").trim();
				platform_shopname = element.getElementsByClass("productShop-name").text();
			}
		} else {
			if (messagejson.toString().contains("raw_title")) {
				if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TMALL_EN)) {
					if (!messagejson.toString().contains(Fields.PLATFORM_TMALL_INTERNATIONAL_BABY)) {
						keywordName = messagejson.getString("nid");
						goodsName = messagejson.getString("raw_title");
						platform_shopname = messagejson.getString("nick");
						goodsUrl = messagejson.getString("detail_url");
						image = messagejson.getString("pic_url");
						price = messagejson.getString("view_price");
						item_loc = messagejson.getString("item_loc");//当前位置
						user_id = messagejson.getString("user_id");
						try {
							view_sales = messagejson.getString("view_sales").substring(0, messagejson.getString("view_sales").length() - 3);
						} catch (Exception e) {
						}
						comment_count = messagejson.getString("comment_count");//评论数
						if (platform_shopname.contains(Fields.TMART) || platform_shopname.contains(Fields.FLAGSHIP_STORE)) {
							platform_shoptype = Fields.YES_PROPRIETARY;
						} else {
							platform_shoptype = Fields.PROPRIETARY;
						}
					}
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TAOBAO_EN)) {
					if (!messagejson.toString().contains(Fields.SEARCH_TMALL) && !messagejson.toString().contains(Fields.PLATFORM_TMALL_INTERNATIONAL_BABY)) {
						keywordName = messagejson.getString("nid");
						goodsName = messagejson.getString("raw_title");
						platform_shopname = messagejson.getString("nick");
						goodsUrl = messagejson.getString("detail_url");
						image = messagejson.getString("pic_url");
						item_loc = messagejson.getString("item_loc");//当前位置
						user_id = messagejson.getString("user_id");
						try {
							view_sales = messagejson.getString("view_sales").substring(0, messagejson.getString("view_sales").length() - 3);
						} catch (Exception e) {
						}
						comment_count = messagejson.getString("comment_count");//评论数
						if (platform_shopname.contains(Fields.TMART) || platform_shopname.contains(Fields.FLAGSHIP_STORE)) {
							platform_shoptype = Fields.YES_PROPRIETARY;
						} else {
							platform_shoptype = Fields.PROPRIETARY;
						}
					}
					/*		}else if(wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TMALL_GLOBAL_EN)){
					if(messagejson.toString().contains(Fields.PLATFORM_TMALL_INTERNATIONAL_BABY)){
						keywordName=messagejson.getString("nid");
						goodsName=messagejson.getString("raw_title");
						platform_shopname=messagejson.getString("nick");
						goodsUrl=messagejson.getString("detail_url");
						image=messagejson.getString("pic_url");
						user_id=messagejson.getString("user_id");
						item_loc=messagejson.getString("item_loc");//当前位置
						try {view_sales=messagejson.getString("view_sales").substring(0,messagejson.getString("view_sales").length()-3);} catch (Exception e) {}
						comment_count=messagejson.getString("comment_count");//评论数
						if(platform_shopname.contains(Fields.TMART)||platform_shopname.contains(Fields.FLAGSHIP_STORE)){
							platform_shoptype=Fields.YES_PROPRIETARY;
						}else{
							platform_shoptype=Fields.PROPRIETARY;
						}
					}
				}
					 */
				}
			} else if (messagejson.toString().contains("quantity")) {//tmall手机端指定店铺开发
				keywordName = messagejson.getString("item_id");
				goodsName = messagejson.getString("title").replace("<span class=H>", "").replace("</span>", "").trim();
				image = messagejson.getString("img");//图片
				image = "https:" + image;
				goodsUrl = messagejson.getString("url");
				price = messagejson.getString("price");//现价
				view_sales = messagejson.getString("sold");//月销量
				platform_shopname = wordsInfo.getCust_keyword_remark().replace("+Mobile", "").trim();
				;
				platform_shoptype = Fields.YES_PROPRIETARY;
			} else if (messagejson.toString().contains("tItemType")) {
				keywordName = messagejson.getString("item_id");
				goodsName = messagejson.getString("title");
				platform_shopname = messagejson.getString("nick");
				goodsUrl = messagejson.getString("url");
				image = messagejson.getString("img2");
				user_id = messagejson.getString("userId");
				item_loc = messagejson.getString("location");//当前位置
				price = messagejson.getString("price");//现价
				if (messagejson.toString().contains("commentCount")) {
					comment_count = messagejson.getString("commentCount");//评论数
				}
				if (platform_shopname.contains(Fields.TMART) || platform_shopname.contains(Fields.FLAGSHIP_STORE)) {
					platform_shoptype = Fields.YES_PROPRIETARY;
				} else {
					platform_shoptype = Fields.PROPRIETARY;
				}
			}
		}
		if (StringUtil.isNotEmpty(keywordName)) {
			messageData.put("item_loc", item_loc);
			messageData.put("count", map.get(Fields.COUNT));
			messageData.put("number", number);
			messageData.put("sum", sum);
			messageData.put("keywordName", keywordName);
			messageData.put("goodsName", goodsName);
			messageData.put("goods_pic_url", image);
			messageData.put("batch_time", time);
			messageData.put("price", price);
			messageData.put("goodsUrl", goodsUrl);
			messageData.put("platform_shoptype", platform_shoptype);
			messageData.put("platform_shopname", platform_shopname);
			messageData.put("ttl_comment_num", comment_count);
			messageData.put("sale_qty", view_sales);
			messageData.put("ItemPrice", 0);
			messageData.put("bsr_rank", 0);
			messageData.put("channel", Fields.CLIENT_MOBILE);
			messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
		}
		return messageData;
	}

	//京东
	@SuppressWarnings("unchecked")
	public Map<String, Object> jdMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, Map<String, String> map, int sum, int number) throws Exception {
		Map<String, Object> messageData = new HashMap<String, Object>();
		String keywordName = "";
		String image = "";
		String goodsName = "";
		String price = "";
		String item_loc = "";//当前位置
		String tableName_history = "";
		String platform_shoptype = "";
		String goodsUrl = "";
		String platform_shopname = "";
		String ttl_comment_num = "";
		String sale_qty = "";
		String platform_shopid="";
		String platform_sellerid="";
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		Map<String, Object> insertItem_history = new HashMap<String, Object>();
		List<Map<String, Object>> insertItems_history = Lists.newArrayList();
		Map<String, Object> insertItem = new HashMap<String, Object>();
		List<Map<String, Object>> insertItems = Lists.newArrayList();
		if (!Validation.isEmpty(element)) {
			goodsUrl = element.getElementsByClass("gl-item").get(0).getElementsByClass("p-img").get(0).getElementsByTag("a").attr("href").toString();
			image = element.getElementsByClass("gl-item").get(0).getElementsByClass("p-img").get(0).getElementsByTag("img").attr("src").toString();
			if (StringUtil.isEmpty(image)) {
				image = element.getElementsByClass("gl-item").get(0).getElementsByClass("p-img").get(0).getElementsByTag("img").attr("data-lazy-img").toString();//
			}
			keywordName = element.getElementsByClass("gl-item").attr("data-sku").toString();
			if (StringUtil.isEmpty(keywordName)) {
				keywordName = element.getElementsByClass("gl-item").get(0).getElementsByTag("div").attr("data-sku").toString();
			}
			goodsName = element.getElementsByClass("gl-item").get(0).getElementsByClass("p-name").get(0).getElementsByTag("em").text();
			if (StringUtil.isEmpty(goodsName)) {
				goodsName = element.getElementsByClass("gl-item").get(0).getElementsByClass("p-img").get(0).getElementsByTag("a").attr("title").toString();
			}
			if (StringUtil.isNotEmpty(element.getElementsByClass("J_im_icon").text())) {
				platform_shopname = element.getElementsByClass("gl-item").get(0).getElementsByClass("J_im_icon").get(0).getElementsByTag("a").attr("title");
			}
			if (StringUtil.isNotEmpty(element.getElementsByClass("p-price").text())) {
				price = element.getElementsByClass("p-price").get(0).getElementsByTag("i").text();
			}
			if (StringUtil.isNotEmpty(element.getElementsByClass("p-commit").text())) {//评论数

				try {
					ttl_comment_num = element.getElementsByClass("p-commit").get(0).getElementsByTag("strong").get(0).getElementsByTag("a").text().substring(0, element.getElementsByClass("p-commit").get(0).getElementsByTag("strong").get(0).getElementsByTag("a").text().length() - 1);
					if (ttl_comment_num.contains(Fields.MYRIAD)) {
						ttl_comment_num = ttl_comment_num.substring(0, ttl_comment_num.length() - 1);
						ttl_comment_num = String.valueOf((int) (Float.valueOf(ttl_comment_num) * 10000));
					}
				} catch (Exception e) {
					//logger.info(element.getElementsByClass("p-commit").get(0).getElementsByTag("strong").get(0).getElementsByTag("a").text().substring(0, element.getElementsByClass("p-commit").get(0).getElementsByTag("strong").get(0).getElementsByTag("a").text().length() - 1));
				}

			}
			//供应商  id
			platform_sellerid=element.getElementsByClass("gl-item").get(0).getElementsByClass("p-img").get(0).getElementsByTag("div").attr("data-venid").toString();

			String store=element.getElementsByClass("gl-item").get(0).getElementsByClass("p-shop").get(0).getElementsByTag("a").attr("title").toString();
			//商品Id
			if(StringUtils.isNotEmpty(store)){
				String shopid=element.getElementsByClass("gl-item").get(0).getElementsByClass("J_im_icon").get(0).getElementsByTag("a").attr("onclick").toString();
				platform_shopid=shopid.split(",")[1];

			}
		} else {
			keywordName = messagejson.getString("wareId");
			goodsName = messagejson.getString("wname");
			price = messagejson.getString("jdPrice");
			image = messagejson.getString("goods_pic_url");
			ttl_comment_num = messagejson.getString("totalCount");
			goodsUrl = Fields.JD_URL_APP + keywordName;
			if (messagejson.getString("self").equals("true")) {
				platform_shoptype = Fields.YES_PROPRIETARY;
			} else {
				platform_shoptype = Fields.PROPRIETARY;
			}
		}
		messageData.put("count", map.get(Fields.COUNT));
		messageData.put("number", number);
		messageData.put("sum", sum);
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("price", price);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("sale_qty", sale_qty);
		messageData.put("item_loc", item_loc);
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", 0);
		messageData.put("channel", Fields.CLIENT_MOBILE);
		messageData.put("platform_shopid", platform_shopid);
		messageData.put("platform_sellerid", platform_sellerid);
		messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
		if (map.get(Fields.STATUS_TYPE).equals(Fields.COUNT_5)) {
			List<Element> list = element.getElementsByClass("ps-item");
			String tableName = "";
			messageData.putAll(map);
			for (int i = 0; i < list.size(); i++) {
				goodsName = list.get(i).getElementsByTag("a").attr("title").toString();
				keywordName = list.get(i).getElementsByTag("img").attr("data-sku").toString();
				image = list.get(i).getElementsByTag("img").attr("data-lazy-img").toString().toString().replace("n9", "n7");
				if (com.eddc.util.Validation.isEmpty(list.get(i).getElementsByTag("img").attr("data-lazy-img").toString())) {
					image = list.get(i).getElementsByTag("img").attr("data-lazy-img-slave").toString().toString().replace("n9", "n7");
				}
				messageData.put("goods_pic_url", image);
				messageData.put("keywordName", keywordName);
				messageData.put("goodsName", goodsName);
				messageData.put("goodsUrl", Fields.JD_URL_APP + keywordName + Fields.HTML);
				messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
				tableName_history = Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH;
				insertItem_history = BeanMapUtil.convertBean2MapWithUnderscoreName(keywordsInfo(wordsInfo, map.get(Fields.COUNT), i, sum, messageData));
				insertItems_history.add(insertItem_history); //历史记录商品egoodsId


				tableName = Fields.TABLE_CRAW_GOODS_INFO;
				insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsInfoData(wordsInfo, messageData));
				insertItems.add(insertItem);//详情
			}
			if (insertItems_history.size() > 0) {
				insertIntoData(insertItems_history, map.get("database"), tableName_history);
			}
			if (insertItems.size() > 0) {
				insertIntoData(insertItems, map.get("database"), tableName);
			}

		}
		return messageData;
	}

	//一号店
	public Map<String, Object> yhdMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, String count, int sum, int number) {
		Map<String, Object> messageData = new HashMap<String, Object>();
		//SimpleDateFormat data=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String keywordName = "";
		String image = "";
		String goodsName = "";
		String price = "";
		String item_loc = "";
		String platform_shoptype = "";
		String goodsUrl = "";
		String platform_shopname = "";
		String ttl_comment_num = "";
		String sale_qty = "";
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		if (!Validation.isEmpty(element)) {
			if (element.toString().contains("pic_box") && element.toString().contains("middle")) {//1号店关键词搜索APP
				keywordName = element.getElementsByClass("bug_car").attr("productId").toString();
				goodsName = element.getElementsByClass("title_box").text();//商品名称
				if (StringUtil.isNotEmpty(element.getElementsByClass("new_price").text())) {
					price = element.getElementsByClass("new_price").get(0).getElementsByTag("i").text();//价格
				}
				image = element.getElementsByClass("pic_box").get(0).getElementsByTag("img").attr("src").toString();//商品图片
				goodsUrl = element.getElementsByClass("item").attr("href").toString();//商品url
				if (StringUtil.isNotEmpty(element.getElementsByClass("self_sell").toString())) {
					platform_shoptype = element.getElementsByClass("self_sell").text();
				} else {
					platform_shoptype = Fields.PROPRIETARY;
				}
			} else if (element.toString().contains("itemBox") || element.toString().contains("proPrice")) {
				keywordName = element.getElementsByClass("itemBox").attr("comproid").trim();//商品egoodsId
				if (StringUtil.isEmpty(keywordName)) {
					keywordName = element.getElementsByClass("\\itemBox\\").attr("comproid").toString().replace("\\", "");
				}
				goodsUrl = element.getElementsByClass("\\img\\").attr("href").replace("\\", "");
				image = element.getElementsByClass("\\img\\").get(0).getElementsByTag("img").attr("src").replace("\\", "");
				goodsName = element.getElementById("\\pdlink2_" + keywordName + "\\").attr("title").trim().replace("\\", "");
				price = element.getElementsByClass("\\proPrice\\").get(0).getElementsByTag("em").attr("yhdprice").replace("\\", "");
				platform_shopname = element.getElementsByClass("\\o_1\\").attr("title").toString().replace("\\", "");

			}
		}
		messageData.put("count", count);
		messageData.put("number", number);
		messageData.put("sum", sum);
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("price", price);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("item_loc", item_loc);
		messageData.put("sale_qty", sale_qty);
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", 0);
		messageData.put("channel", Fields.CLIENT_MOBILE);
		messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
		return messageData;
	}

	//苏宁
	public Map<String, Object> suningMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, String count, int sum, int number) {
		Map<String, Object> messageData = new HashMap<String, Object>();
		//SimpleDateFormat data=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String keywordName = "";
		String image = "";
		String goodsName = "";
		String price = "";
		String item_loc = "";
		String platform_shopid = "";
		String platform_shoptype = "";
		String goodsUrl = "";
		String platform_shopname = "";
		String ttl_comment_num = "";
		String sale_qty = "";
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		if (!Validation.isEmpty(element)) {
			if (element.toString().contains("sellPoint") && element.toString().contains("partnumber")) {//苏宁搜索
				goodsName = element.getElementsByClass("search-loading").attr("alt").toString();
				image = element.getElementsByClass("search-loading").attr("src2").toString();
				goodsUrl = element.getElementsByClass("sellPoint").attr("href").toString();
				keywordName = element.getElementsByClass("res-opt").last().getElementsByTag("a").attr("partnumber").toString().split("-")[1].toString();
				platform_shopid = element.getElementsByClass("res-opt").last().getElementsByTag("a").attr("partnumber").toString().split("-")[0].toString();
				if (StringUtil.isEmpty(platform_shopid)) {
					platform_shopid = element.getElementsByClass("hidenInfo").attr("vendor").toString();
				}
				platform_shopname = element.getElementsByClass("seller oh no-more ").attr("salesName").toString();
				if (StringUtil.isEmpty(platform_shopname)) {
					platform_shopname = element.getElementsByClass("seller oh no-cut ").attr("salesName").toString();
				}
				if (StringUtil.isNotEmpty(platform_shopname)) {
					if (platform_shopname.contains(Fields.YES_PROPRIETARY)) {
						platform_shoptype = Fields.YES_PROPRIETARY;
					} else {
						Fields.YES_PROPRIETARY = Fields.PROPRIETARY;
					}
				}

			}
		}

		messageData.put("platform_shopid", platform_shopid);
		messageData.put("count", count);
		messageData.put("number", number);
		messageData.put("sum", sum);
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("price", price);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("sale_qty", sale_qty);
		messageData.put("item_loc", item_loc);
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", 0);
		messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
		messageData.put("channel", Fields.CLIENT_MOBILE);
		return messageData;
	}

	//解析1688_PC
	@SuppressWarnings("unchecked")
	public Map<String, Object> Message_1688(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, String count, int sum, int number, String database) throws Exception {
		Map<String, Object> messageData = new HashMap<String, Object>();
		Map<String, Object> insertItemPrice = new HashMap<String, Object>();
		List<Map<String, Object>> insertprices = Lists.newArrayList();
		String tableNamePrice = Fields.TABLE_CRAW_VENDOR_PRICE_INFO;
		SimpleDateFormat data = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String keywordName = "";
		String image = "";
		String goodsName = "";
		String companyName = "";
		String SourceFactory = "";
		String purchase_back_rate = "";
		String goodsUrl = "";
		String platform_vendoraddress = "";
		String purchase_unit = "";
		String purchase_price = "";
		String purchase_qty = "";
		String time = data.format(new Date());
		messageData.put("batch_time", time);
		messageData.put("channel", Fields.CLIENT_PC);
		String regEx = "[^0-9]";
		int ItemPrice = 0;
		Pattern p = Pattern.compile(regEx);
		if (!Validation.isEmpty(element)) {
			if (element.toString().contains("sm-previewCompany sw-mod-previewCompanyInfo")) {
				keywordName = element.attr("offerId");//egoodsid
				goodsUrl = element.getElementsByClass("sw-ui-photo220-box").attr("href").toString();//goodsUrl
				image = element.getElementsByClass("sw-ui-photo220-box").get(0).getElementsByTag("img").attr("src").trim().toString();//图片url
				goodsName = element.getElementsByClass("sw-ui-photo220-box").get(0).getElementsByTag("img").attr("alt").trim().toString();//商品名称
				companyName = element.getElementsByClass("sm-previewCompany sw-mod-previewCompanyInfo").text();//公司名称
				purchase_price = element.getElementsByClass("su-price").text().replace("¥", "").trim();//价格
				platform_vendoraddress = element.getElementsByClass("su-city").text();//地址
				String bookedCount = element.getElementsByClass("sm-offerShopwindow-trade").text().trim();//成交
				Matcher mat = p.matcher(bookedCount);
				purchase_qty = mat.replaceAll("").trim();
				if (StringUtil.isNotEmpty(element.getElementsByClass("sm-widget-repurchaseicon").text())) {
					purchase_back_rate = element.getElementsByClass("sm-widget-repurchaseicon").get(0).getElementsByTag("span").text();//回头率
				}
			} else if (element.toString().contains("sm-offer-photo sw-dpl-offer-photo")) {
				goodsName = element.getElementsByClass("sm-offer-photo sw-dpl-offer-photo").get(0).getElementsByTag("a").attr("title").toString();
				goodsUrl = element.getElementsByClass("sm-offer-photo sw-dpl-offer-photo").get(0).getElementsByTag("a").attr("href").toString();
				image = element.getElementsByClass("sm-offer-photo sw-dpl-offer-photo").get(0).getElementsByTag("img").attr("src").toString();
				SourceFactory = element.getElementsByClass("brand-name").text();//源头工厂
				platform_vendoraddress = element.getElementsByClass("sm-offer-location").text();//sm-offer-location 广东深圳市 地址
				keywordName = element.attr("offerid").toString();
				if (StringUtil.isEmpty(keywordName)) {
					keywordName = element.getElementsByClass("sm-offer-companyName sw-dpl-offer-companyName ").attr("offerid").trim().toString();
				}
				if (StringUtil.isEmpty(image)) {
					image = element.getElementsByClass("sm-offer-photo sw-dpl-offer-photo").get(0).getElementsByTag("img").attr("data-lazy-src").toString();
				}
				try {
					companyName = element.getElementsByClass("sm-offer-company sw-dpl-offer-company").get(0).getElementsByTag("a").attr("title").toString();//公司名称
				} catch (Exception e) {
					companyName = element.getElementsByClass("sm-offer-companyName sw-dpl-offer-companyName ").text().trim();//公司名称
				}
				try {
					if (element.getElementsByClass("sm-widget-offershopwindowshoprepurchaserate-old").get(0).getElementsByTag("span").size() > 2) {
						purchase_back_rate = element.getElementsByClass("sm-widget-offershopwindowshoprepurchaserate-old").get(0).getElementsByTag("span").get(2).text();//回头率
					}
				} catch (Exception e) {
					if (StringUtil.isNotEmpty(element.getElementsByClass("sm-widget-offershopwindowshoprepurchaserate ").text())) {
						try {
							purchase_back_rate = element.getElementsByClass("sm-widget-offershopwindowshoprepurchaserate ").get(0).getElementsByTag("span").get(2).text();
						} catch (Exception e2) {
						}//回头率
						SourceFactory = element.getElementsByClass("sm-widget-offershopwindowshoprepurchaserate ").attr("i").toString();//生产加工
					}
				}
				Elements purchases = element.getElementsByClass("sm-offer-dealInfo sm-offer-dealInfo-3").select("span");
				if (purchases.size() == 0) {
					purchases = element.getElementsByClass("s-widget-offershopwindowdealinfo sm-offer-dealInfo sm-offer-dealInfo-3").select("span");
					if (purchases.size() == 0) {
						purchases = element.getElementsByClass("s-widget-offershopwindowdealinfo sm-offer-dealInfo sm-offer-dealInfo-1").select("span");
					}
					if (purchases.size() == 0) {
						purchases = element.getElementsByClass("s-widget-offershopwindowdealinfo sm-offer-dealInfo sm-offer-dealInfo-2").select("span");
					}
				}
				for (Element span : purchases) {
					purchase_unit = span.getElementsByTag("em").attr("title").substring(span.getElementsByTag("em").attr("title").length() - 1, span.getElementsByTag("em").attr("title").length());// 件 个 套 台
					String purchase_amount = span.getElementsByTag("em").attr("title").substring(0, span.getElementsByTag("em").attr("title").length() - 1);//   起价数
					purchase_price = span.getElementsByTag("i").attr("title").replace("¥", "");//价格
					messageData.put("purchase_price", purchase_price);
					messageData.put("purchase_unit", purchase_unit);
					messageData.put("purchase_amount", purchase_amount);
					messageData.put("keywordName", keywordName);
					ItemPrice = 1;
					if (StringUtil.isNotEmpty(keywordName)) {
						messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
						if (Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_10)) {
							insertItemPrice = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorPriceInfoData(wordsInfo, messageData));
							insertprices.add(insertItemPrice);
						}
					}
				}
				if (insertprices.size() > 0) {
					insertIntoData(insertprices, database, tableNamePrice);
				}
			}
		} else {
			SourceFactory = messagejson.get("bizType").toString();//生产加工
			platform_vendoraddress = messagejson.get("city").toString();//深圳市
			companyName = messagejson.get("companyName").toString();//公司名称
			image = messagejson.get("offerPicUrl").toString();//图片url
			purchase_price = messagejson.get("price").toString();//价格
			purchase_back_rate = messagejson.get("shopRepurchaseRate").toString();//回头率
			goodsName = messagejson.get("simpleSubject").toString();//商品名称
			purchase_unit = messagejson.get("unit").toString();//台
			purchase_qty = messagejson.get("bookedCount").toString();//成交笔
			ItemPrice = 0;

		}
		if (StringUtil.isNotEmpty(keywordName)) {
			messageData.put("purchase_price", purchase_price);
			messageData.put("purchase_amount", purchase_qty);
			messageData.put("purchase_unit", purchase_unit);
			messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
			messageData.put("platform_vendorid", StringHelper.encryptByString(companyName + Fields.platform_1688));//供应商Id
			messageData.put("companyName", companyName);//供应商名称
			messageData.put("count", count);
			messageData.put("number", number);
			messageData.put("sum", sum);
			messageData.put("keywordName", keywordName);
			messageData.put("goodsName", goodsName);
			messageData.put("goods_pic_url", image);
			messageData.put("goodsUrl", goodsUrl);
			messageData.put("purchase_back_rate", purchase_back_rate);
			messageData.put("SourceFactory", SourceFactory);
			messageData.put("ItemPrice", ItemPrice);
			messageData.put("bsr_rank", 0);
			messageData.put("purchase_qty", purchase_qty);
			messageData.put("channel", Fields.CLIENT_MOBILE);
			messageData.put("platform_vendoraddress", platform_vendoraddress);
		}
		return messageData;
	}

	//亚马逊
	public Map<String, Object> amazonMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, Map<String, String> map, int sum, int number) throws Exception {
		Map<String, Object> messageData = new HashMap<String, Object>();
		//SimpleDateFormat data=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String keywordName = "";
		String image = "";
		String goodsName = "";
		String price = "";
		String bsr_rank = "";
		String item_loc = "";
		String platform_shoptype = "";
		String goodsUrl = "";
		String platform_shopname = "";
		String ttl_comment_num = "";
		String sale_qty = "";
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		if (!Validation.isEmpty(element)) {
			if (element.toString().contains("s-results-list-atf") || element.toString().contains("data-asin")) {//美国亚马逊
				keywordName = element.attr("data-asin").toString();
				goodsName = element.getElementsByClass("a-link-normal s-access-detail-page  s-color-twister-title-link a-text-normal").attr("title").toString();
				price = element.getElementsByClass("a-offscreen").text().replace("$", "");
				image = element.getElementsByClass("a-link-normal a-text-normal").get(0).getElementsByTag("img").attr("src").toString();
				goodsUrl = element.getElementsByClass("a-link-normal a-text-normal").attr("href");
				if (StringUtil.isNotEmpty(element.getElementsByClass("a-column a-span5 a-span-last").text().toString())) {
					ttl_comment_num = element.getElementsByClass("a-column a-span5 a-span-last").get(0).getElementsByClass("a-size-small a-link-normal a-text-normal").text().replace(",", "");//评论数
				}
				if (StringUtil.isNotEmpty(element.getElementsByClass("a-size-small a-link-normal a-text-normal").text().toString())) {
					ttl_comment_num = element.getElementsByClass("a-size-small a-link-normal a-text-normal").last().text().replace(",", "");//评论数
				}
			} else if (element.toString().contains("a-row s-result-list-parent-container")) {
				keywordName = element.attr("data-asin").toString();
				goodsName = element.getElementsByClass("a-link-normal a-text-normal").get(0).getElementsByTag("img").attr("alt");
				image = element.getElementsByClass("a-link-normal a-text-normal").get(0).getElementsByTag("img").attr("src");
				platform_shopname = element.getElementsByClass("a-size-small a-color-secondary").get(1).text();//品牌
				price = element.getElementsByClass("a-size-base a-color-price s-price a-text-bold").text().replace("￥", "");
				goodsUrl = element.getElementsByClass("a-link-normal a-text-normal").attr("href");
			} else if (element.toString().contains("a-ordered-list a-vertical")) {//
				goodsName = element.getElementsByClass("a-section a-spacing-small").get(0).getElementsByClass("a-section a-spacing-small").get(0).getElementsByTag("img").attr("alt").replace("\"", "");
				image = element.getElementsByClass("a-section a-spacing-small").get(0).getElementsByClass("a-section a-spacing-small").get(0).getElementsByTag("img").attr("src");
				ttl_comment_num = element.getElementsByClass("a-icon-row a-spacing-none").get(0).getElementsByClass("a-size-small a-link-normal").text().replace(",", "");
				price = element.getElementsByClass("p13n-sc-price").text().replace("$", "");
				keywordName = element.getElementsByClass("a-link-normal").attr("href").toString().split("/")[3].toString();
				goodsUrl = element.getElementsByClass("a-link-normal").attr("href").toString();
				bsr_rank = element.getElementsByClass("zg-badge-text").text().replace("#", "");
			} else if (element.toString().contains("zg-item-immersion")) {//类目搜索
				bsr_rank = element.getElementsByClass("zg-badge-text").text().replace("#", "");
				goodsName = element.getElementsByClass("a-section a-spacing-small").get(0).getElementsByTag("img").attr("alt").toString().replace("\"", "`");
				image = element.getElementsByClass("a-section a-spacing-small").get(0).getElementsByTag("img").attr("src").toString();
				ttl_comment_num = element.getElementsByClass("a-size-small a-link-normal").text().replace(",", "");
				price = element.getElementsByClass("p13n-sc-price").text().replace("$", "");
				goodsUrl = element.getElementsByClass("a-size-small a-link-normal").attr("href").toString();
				if (StringUtil.isEmpty(goodsUrl)) {
					goodsUrl = element.getElementsByClass("a-link-normal a-text-normal").attr("href").toString();
					keywordName = goodsUrl.split("/")[3].toString();
				} else {
					keywordName = goodsUrl.split("/")[2].toString();
				}
				goodsUrl = Fields.AMAZON_URL_KEYWORD_EN_URL + goodsUrl;

			} else if (element.toString().contains("zg_itemWrapper")) {
				bsr_rank = element.getElementsByClass("zg_rankNumber").text().replace(".", "");//
				image = element.getElementsByClass("zg_itemWrapper").get(0).getElementsByClass("a-link-normal").get(0).getElementsByTag("img").attr("src").toString();
				goodsUrl = element.getElementsByClass("zg_itemWrapper").get(0).getElementsByClass("a-link-normal").attr("href").toString();
				goodsName = element.getElementsByClass("zg_itemWrapper").get(0).getElementsByClass("a-link-normal").get(0).getElementsByTag("img").attr("alt").toString();
				ttl_comment_num = element.getElementsByClass("a-size-small a-link-normal").text().replace(",", "");
				price = element.getElementsByClass("p13n-sc-price").text().replace("$", "").toString();
				if (StringUtil.isNotEmpty(goodsUrl)) {
					keywordName = goodsUrl.split("/")[3].toString();
				}
				goodsUrl = Fields.AMAZON_URL_KEYWORD_EN_URL + goodsUrl;
			}
		}
		if (StringUtil.isNotEmpty(keywordName.toString())) {
			if (price.contains("-")) {
				price = price.split("-")[1].trim();
			}
			messageData.put("count", map.get(Fields.COUNT));
			messageData.put("number", number);
			messageData.put("sum", sum);
			messageData.put("keywordName", keywordName);
			messageData.put("goodsName", goodsName);
			messageData.put("goods_pic_url", image);
			messageData.put("batch_time", time);
			messageData.put("price", price);
			messageData.put("goodsUrl", goodsUrl);
			messageData.put("platform_shoptype", platform_shoptype);
			messageData.put("platform_shopname", platform_shopname);
			messageData.put("ttl_comment_num", ttl_comment_num);
			messageData.put("sale_qty", sale_qty);
			messageData.put("item_loc", item_loc);
			messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
			messageData.put("ItemPrice", 0);
			messageData.put("bsr_rank", bsr_rank);
			messageData.put("channel", Fields.CLIENT_MOBILE);
		}
		return messageData;
	}


	//唯品会
	public Map<String, Object> vipMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, Map<String, String> map, int sum, int number) {
		Map<String, Object> messageData = new HashMap<String, Object>();
		String keywordName = "";
		String image = "";
		String goodsName = "";
		String price = "";
		String item_loc = "";
		String originalprice = "";
		String platform_shoptype = "";
		String goodsUrl = "";
		String platform_shopname = "";
		String ttl_comment_num = "";
		String sale_qty = "";
		String promotion = "";//促销
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		if (!Validation.isEmpty(messagejson)) {
			goodsName = messagejson.getString("product_name");//商品名称
			image = messagejson.getString("small_image");//商品图片
			goodsUrl = "https://detail.vip.com/detail-" + messagejson.getString("brand_id") + "-" + messagejson.getString("product_id") + ".html";//商品url
			keywordName = messagejson.getString("brand_id") + "-" + messagejson.getString("product_id");//商品egoodsid
			platform_shopname = messagejson.getString("brand_name");//店铺
			price = messagejson.getJSONObject("price_info").getString("sell_price_min_tips");//现价价格
			originalprice = messagejson.getJSONObject("price_info").getString("market_price_of_min_sell_price");//原价
			promotion = messagejson.getJSONObject("price_info").getString("discount_index_min_tips");//促销
			if (StringUtil.isNotEmpty(platform_shopname)) {
				if (platform_shopname.contains(Fields.YES_PROPRIETARY)) {
					platform_shoptype = Fields.YES_PROPRIETARY;
				} else {
					platform_shoptype = Fields.PROPRIETARY;
				}
			}
		}
		messageData.put("count", map.get(Fields.COUNT));
		messageData.put("promotion", promotion);
		messageData.put("originalprice", originalprice);
		messageData.put("number", number);
		messageData.put("sum", sum);
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("price", price);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("sale_qty", sale_qty);
		messageData.put("item_loc", item_loc);
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", 0);
		messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
		messageData.put("channel", Fields.CLIENT_MOBILE);
		return messageData;
	}

	//考拉
	public Map<String, Object> kaoLaMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, Map<String, String> map, int sum, int number) {
		Map<String, Object> messageData = new HashMap<String, Object>();
		String keywordName = "";
		String image = "";
		String goodsName = "";
		String price = "";
		String item_loc = "";
		String originalprice = "";
		String platform_shoptype = "";
		String goodsUrl = "";
		String platform_shopname = "";
		String ttl_comment_num = "";
		String sale_qty = "";
		String promotion = "";//促销
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		try {
			if (!Validation.isEmpty(element.toString())) {
				goodsName = element.getElementsByClass("goodswrap promotion").get(0).getElementsByTag("a").attr("title").trim();//商品名称
				image = element.getElementsByClass("goodswrap promotion").get(0).getElementsByTag("img").attr("data-src");//商品图片
				goodsUrl = "https://goods.kaola.com" + element.getElementsByClass("goodswrap promotion").get(0).getElementsByTag("a").attr("href").trim();//商品url
				keywordName = publicClass.messageData(element.getElementsByClass("goodswrap promotion").get(0).getElementsByTag("a").attr("href").trim());//商品egoodsid
				platform_shopname = element.getElementsByClass("goodswrap promotion").get(0).getElementsByClass("selfflag").text();//店铺
				price = publicClass.messageData(element.getElementsByClass("goodswrap promotion").get(0).getElementsByClass("cur").text());//现价价格
				originalprice = publicClass.messageData(element.getElementsByClass("goodswrap promotion").get(0).getElementsByClass("marketprice").text());//原价
				ttl_comment_num = element.getElementsByClass("goodswrap promotion").get(0).getElementsByClass("goodsinfo clearfix").text();//评论数
				item_loc = element.getElementsByClass("goodswrap promotion").get(0).getElementsByClass("proPlace ellipsis").text();//地区
				if (element.getElementsByClass("goodswrap promotion").toString().contains("saelsinfo")) {
					try {
						promotion = element.getElementsByClass("goodswrap promotion").get(0).getElementsByClass("activity z-benefit").text();//促销
						if (element.getElementsByClass("goodswrap promotion").get(0).getElementsByClass("saelsinfo").toString().contains(Fields.YES_PROPRIETARY)) {
							platform_shoptype = Fields.YES_PROPRIETARY;
						} else {
							platform_shoptype = Fields.PROPRIETARY;
						}
					} catch (Exception e) {
						logger.error("获取促销或平台类型失败" + e);
					}

				}
			}
		} catch (Exception e) {
			logger.error("解析考拉数据失败>>>>>>>>>>>>" + e);
		}
		messageData.put("count", map.get(Fields.COUNT));
		messageData.put("promotion", promotion);
		messageData.put("originalprice", originalprice);
		messageData.put("number", number);
		messageData.put("sum", sum);
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("price", price);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("sale_qty", sale_qty);
		messageData.put("item_loc", item_loc);
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", 0);
		messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
		messageData.put("channel", Fields.CLIENT_MOBILE);
		return messageData;

	}

	//酒仙网
	public Map<String, Object> jxMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, Map<String, String> map, int sum, int number) {
		Map<String, Object> messageData = new HashMap<String, Object>();
		String keywordName = "";
		String image = "";
		String goodsName = "";
		String price = "";
		String item_loc = "";
		String originalprice = "";
		String platform_shoptype = "";
		String goodsUrl = "";
		String platform_shopname = "";
		String ttl_comment_num = "";
		String sale_qty = "";
		String promotion = "";//促销
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		try {
			if (!Validation.isEmpty(element.toString())) {
				goodsName = element.getElementsByClass("clearfix").attr("title").toString();//商品名称
				goodsUrl = element.getElementsByClass("img clearfix").attr("href").toString();//商品url
				image = element.getElementsByClass("img clearfix").get(0).getElementsByTag("img").attr("src").trim();//商品图片
				keywordName = element.getElementsByClass("img clearfix").get(0).getElementsByTag("img").attr("proimgid");//商品egoodsid
				if (element.getElementsByClass("seller").toString().contains("span")) {
					platform_shopname = element.getElementsByClass("seller").get(0).getElementsByTag("span").text();//店铺
					platform_shoptype = Fields.PROPRIETARY;
				} else {
					platform_shoptype = Fields.YES_PROPRIETARY;
				}
				if (element.getElementsByClass("judge").toString().contains("span")) {
					ttl_comment_num = element.getElementsByClass("judge").get(0).getElementsByClass("span").text();//评论数
				}
			}
		} catch (Exception e) {
			logger.error("解析酒仙网数据失败>>>>>>>>>>>>" + e);
		}
		messageData.put("count", map.get(Fields.COUNT));
		messageData.put("promotion", promotion);
		messageData.put("originalprice", originalprice);
		messageData.put("number", number);
		messageData.put("sum", sum);
		/*messageData.put("keywordName","goods-"+keywordName);*/
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("price", price);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("sale_qty", sale_qty);
		messageData.put("item_loc", item_loc);
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", 0);
		messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
		messageData.put("channel", Fields.CLIENT_MOBILE);
		return messageData;

	}

	//孩子王
	public Map<String, Object> kidswantMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, Map<String, String> map, int sum, int number) {
		Map<String, Object> messageData = new HashMap<String, Object>();
		String keywordName = "";
		String image = "";
		String goodsName = "";
		String price = "";
		String item_loc = "";
		String originalprice = "";
		String platform_shoptype = "";
		String goodsUrl = "";
		String platform_shopname = "";
		String ttl_comment_num = "";
		String sale_qty = "";
		String promotion = "";//促销
		String hasStock = "有货";
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		if (!Validation.isEmpty(messagejson)) {
			goodsName = messagejson.getString("skuTitle");//商品名称
			image = messagejson.getString("skuPicCdnUrl");//商品图片
			goodsUrl = messagejson.getString("goodsUrl");
			String stock = messagejson.getString("hasStock");
			if (stock.contains("false")) {
				hasStock = "无货";
			}
			keywordName = messagejson.getString("skuId");//商品egoodsid

			platform_shopname = messagejson.getString("skuBrandName");//店铺

			price = messagejson.getString("skuReferPrice").replace("00", "").trim();//现价价格
			originalprice = messagejson.getString("skuReferPrice").replace("00", "").trim();//原价

		}
		messageData.put("count", map.get(Fields.COUNT));
		messageData.put("promotion", promotion);
		messageData.put("originalprice", originalprice);
		messageData.put("number", number);
		messageData.put("sum", sum);
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("price", price);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("sale_qty", sale_qty);
		messageData.put("item_loc", item_loc);
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", 0);
		messageData.put("hasStock", hasStock);
		messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
		messageData.put("channel", Fields.CLIENT_MOBILE);
		if (wordsInfo.getPlatform_shopid() != null) {
			messageData.put("platform_shopid", wordsInfo.getPlatform_shopid());
		}
		return messageData;
	}

	//新欧汇
	public Map<String, Object> xiouhuiMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, Map<String, String> map, int sum, int number) {
		Map<String, Object> messageData = new HashMap<String, Object>();
		String keywordName = "";
		String image = "";
		String goodsName = "";
		String price = "";
		String item_loc = "";
		String platform_shoptype = "";
		String goodsUrl = "";
		String ttl_comment_num = "";
		String sale_qty = "";
		String platform_shopname = "";
		String promotion = "";//促销
		String hasStock = "有货";
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		if (!Validation.isEmpty(element)) {
			keywordName = element.getElementsByTag("a").attr("href").replace("/goods/", "").trim();
			goodsName = element.getElementsByClass("p3").text();
			price = element.getElementsByClass("p4").text().replace("￥", "").trim();
			image = element.getElementsByClass("p1").get(0).getElementsByTag("img").attr("src").trim();
			goodsUrl = "http://www.xinouhui.com" + element.getElementsByTag("a").attr("href").trim();
		}
		messageData.put("count", map.get(Fields.COUNT));
		messageData.put("promotion", promotion);
		messageData.put("originalprice", price);
		messageData.put("number", number);
		messageData.put("sum", sum);
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("price", price);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("sale_qty", sale_qty);
		messageData.put("item_loc", item_loc);
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", 0);
		messageData.put("hasStock", hasStock);
		messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
		messageData.put("channel", Fields.CLIENT_PC);
		return messageData;
	}

	/**
	 * 渝欧汇
	 */
	public Map<String, Object> youhuiMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, Map<String, String> map, int sum, int number) {
		Map<String, Object> messageData = new HashMap<String, Object>();
		String keywordName = "";
		String image = "";
		String goodsName = "";
		String price = "";
		String item_loc = "";
		String platform_shoptype = "";
		String goodsUrl = "";
		String ttl_comment_num = "";
		String sale_qty = "";
		String platform_shopname = "";
		String promotion = "";//促销
		String hasStock = "有货";
		String originalprice = "";
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		if (!Validation.isEmpty(element)) {
			keywordName = element.getElementsByTag("a").attr("href");
			keywordName = keywordName.split("/")[2];
			goodsName = element.getElementsByClass("detail-recent-product-title").text();
			price = element.getElementsByClass("detail-recommend-product-now-price").text().replace("￥", "").trim();
			originalprice = element.getElementsByClass("detail-recommend-product-origin-price").text().replace("￥", "").trim();
			image = element.getElementsByClass("limit-list-img").get(0).getElementsByTag("img").attr("data-original");
			goodsUrl = "https://www.yuouhui.com" + element.getElementsByTag("a").attr("href");
		}
		messageData.put("count", map.get(Fields.COUNT));
		messageData.put("promotion", promotion);
		messageData.put("originalprice", originalprice);
		messageData.put("number", number);
		messageData.put("sum", sum);
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("price", price);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("sale_qty", sale_qty);
		messageData.put("item_loc", item_loc);
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", 0);
		messageData.put("hasStock", hasStock);
		messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
		messageData.put("channel", Fields.CLIENT_PC);
		return messageData;
	}
	/**
	 * 圣卡斯
	 */
	public Map<String, Object> sksMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, Map<String, String> map, int sum, int number) {
		Map<String, Object> messageData = new HashMap<String, Object>();
		String keywordName = "";
		String image = "";
		String goodsName = "";
		String price = "";
		String item_loc = "";
		String platform_shoptype = "";
		String goodsUrl = "";
		String ttl_comment_num = "";
		String sale_qty = "";
		String platform_shopname = "";
		String promotion = "";//促销
		String hasStock = "有货";
		String originalprice = "";
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		if(messagejson.toString().contains("goodsId")) {
			image=messagejson.getString("big");
			platform_shopname=messagejson.getString("brandName");
			keywordName=messagejson.getString("goodsId");
			goodsName=messagejson.getString("name");
			price=messagejson.getString("salePrice");
			originalprice=messagejson.getString("price");
		}
		//		if (!Validation.isEmpty(element)) {
		//			keywordName=element.attr("product").trim();
		//			image=element.getElementsByClass("goodpic").get(0).getElementsByTag("img").attr("src");
		//			goodsUrl="http://www.saintcos.hk/"+element.getElementsByClass("goodpic").get(0).getElementsByTag("a").attr("href");
		//			goodsName=element.getElementsByClass("goodpic").get(0).getElementsByTag("img").attr("alt");
		//			originalprice=element.getElementsByClass("price1").last().text();
		//		}
		messageData.put("count", map.get(Fields.COUNT));
		messageData.put("promotion", promotion);
		messageData.put("originalprice", originalprice);
		messageData.put("price", price);
		messageData.put("number", number);
		messageData.put("sum", sum);
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("price", price);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("sale_qty", sale_qty);
		messageData.put("item_loc", item_loc);
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", 0);
		messageData.put("hasStock", hasStock);
		messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
		messageData.put("channel", Fields.CLIENT_PC);
		return messageData;
	}


	/**
	 * 国美
	 */
	public Map<String, Object> gomeMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, Map<String, String> map, int sum, int number) {
		Map<String, Object> messageData = new HashMap<String, Object>();
		String keywordName = "";
		String image = "";
		String goodsName = "";
		String price = "";
		String item_loc = "";
		String platform_shoptype = "";
		String goodsUrl = "";
		String ttl_comment_num = "";
		String sale_qty = "";
		String platform_shopname = "";
		String promotion = "";//促销
		String hasStock = "有货";
		String originalprice = "";
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		if (!Validation.isEmpty(element)) {
			goodsUrl=element.getElementsByClass("a-mask").attr("href");

			keywordName= StringUtils.replaceEach(StringUtils.substringBetween(goodsUrl, "product-", ".html"), null, null);

			image=" http:"+element.getElementsByClass("gd_img").get(0).getElementsByTag("img").attr("src");

			goodsName=element.getElementsByClass("title ellipsis_two").text();

			originalprice=element.getElementsByClass("price").text().replace("&yen;", "").trim();
			ttl_comment_num=element.getElementsByClass("z-s-dis").text().replace("人评论", "").trim();
			platform_shopname=element.getElementsByClass("z-s-sale").text();
		}
		messageData.put("count", map.get(Fields.COUNT));
		messageData.put("promotion", promotion);
		messageData.put("originalprice", originalprice);
		messageData.put("number", number);
		messageData.put("sum", sum);
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("price", price);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("sale_qty", sale_qty);
		messageData.put("item_loc", item_loc);
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", 0);
		messageData.put("hasStock", hasStock);
		messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
		messageData.put("channel", Fields.CLIENT_PC);
		return messageData;
	}


	/**
	 * 海拍客
	 */
	public Map<String, Object> hpkMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, Map<String, String> map, int sum, int number) {
		Map<String, Object> messageData = new HashMap<String, Object>();
		String keywordName = "";
		String image = "";
		String goodsName = "";
		String price = "";
		String item_loc = "";
		String platform_shoptype = "";
		String goodsUrl = "";
		String ttl_comment_num = "";
		String sale_qty = "";
		String platform_shopname = "";
		String promotion = "";//促销
		String hasStock = "有货";
		String originalprice = "";
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		if (!Validation.isEmpty(messagejson)) {
			//月销量
			sale_qty=messagejson.getString("hotLevel");
			image=messagejson.getString("pic");
			//库存
			hasStock=messagejson.getString("itemStock");
			//商品价格
			price=messagejson.getJSONObject("price").getString("sortPrice");
			//商品名称
			goodsName=messagejson.getString("name");
			//商品url、
			goodsUrl="https://detail.hipac.cn/item.html?itemId="+messagejson.getString("id");
			//商品ID
			keywordName=messagejson.getString("id");
		}
		messageData.put("count", map.get(Fields.COUNT));
		messageData.put("promotion", promotion);
		messageData.put("originalprice", originalprice);
		messageData.put("number", number);
		messageData.put("sum", sum);
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("price", price);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("sale_qty", sale_qty);
		messageData.put("item_loc", item_loc);
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", 0);
		messageData.put("hasStock", hasStock);
		messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
		messageData.put("channel", Fields.CLIENT_PC);
		return messageData;
	}
	
	/**
	 * 海尔商品搜索
	 **/
	public Map<String, Object> ehaierMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, Map<String, String> map, int sum, int number) {
		Map<String, Object> messageData = new HashMap<String, Object>();
		String keywordName = "";
		String image = "";
		String goodsName = "";
		String price = "";
		String item_loc = "";
		String platform_shoptype = "";
		String goodsUrl = "";
		String ttl_comment_num = "";
		String sale_qty = "";
		String platform_shopname = "";
		String promotion = "";//促销
		String hasStock = "有货";
		String originalprice = "";
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		if (!Validation.isEmpty(messagejson)) {
			//月销量
			ttl_comment_num=messagejson.getString("comments").replace("+", "");
			if(ttl_comment_num.contains("comments")) {
				ttl_comment_num="0";
			}
			image=messagejson.getString("defaultImageUrl");
			//库存
			hasStock=messagejson.getString("hasStock");
			//商品价格
			price=messagejson.getString("saleGuidePrice");
			//商品名称
			goodsName=messagejson.getString("productFullName");
		
			//商品url、
			goodsUrl="https://m.ehaier.com/sgmobile/goodsDetail?productId="+messagejson.getString("productId");
			//商品ID
			keywordName=messagejson.getString("productId");
			//店铺
			platform_shopname=messagejson.getString("brand");
		}
		
		messageData.put("goods_pic_url", image);
		messageData.put("count", map.get(Fields.COUNT));
		messageData.put("promotion", promotion);
		messageData.put("originalprice", originalprice);
		messageData.put("number", number);
		messageData.put("sum", sum);
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("price", price);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("sale_qty", sale_qty);
		messageData.put("item_loc", item_loc);
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", 0);
		messageData.put("hasStock", hasStock);
		messageData.put("goodsId", StringHelper.encryptByString(keywordName + wordsInfo.getPlatform_name()));
		messageData.put("channel", Fields.CLIENT_PC);
		return messageData;
	}
	
	

	//淘宝众筹页面解析
	public Map<String, Object> taobaoZcMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, String count, int sum, int number) {
		Map<String, Object> messageData = new HashMap<String, Object>();
		messageData.put("cust_keyword_id", wordsInfo.getCust_keyword_id());
		messageData.put("goodsId", StringHelper.encryptByString(messagejson.get("id") + wordsInfo.getPlatform_name()));
		messageData.put("egoodsId", messagejson.get("id"));
		messageData.put("platform_goods_name", messagejson.get("name"));//商品名称
		messageData.put("platform_name_en", wordsInfo.getPlatform_name());//平台
		messageData.put("platform_category", wordsInfo.getCust_keyword_name());//所在类
		messageData.put("curr_amount", messagejson.get("curr_money"));//已筹金额 目前累计资金
		messageData.put("target_amount", messagejson.get("target_money"));//-目标金额
		messageData.put("finish_per", messagejson.get("finish_per"));//达成率
		messageData.put("support_count", messagejson.get("buy_amount"));//支持人数
		messageData.put("focus_count", messagejson.get("focus_count"));//-喜欢人数
		messageData.put("remain_day", messagejson.get("remain_day"));//剩余天数
		messageData.put("begin_date", messagejson.get("begin_date"));//开始日期
		messageData.put("end_date", messagejson.get("end_date"));//结束日期
		messageData.put("goods_status", messagejson.get("status"));//筹款状态
		messageData.put("goods_url", "https:" + messagejson.get("link"));//商品详情页面
		messageData.put("goods_pic_url", messagejson.get("image"));//商品图片
		messageData.put("update_time", SimpleDate.SimpleDateFormatData().format(new Date()));
		messageData.put("update_date", SimpleDate.SimpleDateFormatData().format(new Date()));
		messageData.put("batch_time", SimpleDate.SimpleDateFormatData().format(new Date()));
		return messageData;
	}

	//京东众筹页面解析
	@SuppressWarnings("static-access")
	public Map<String, Object> jdZcMessage(Element element, CrawKeywordsInfo wordsInfo, JSONObject messagejson, String count, int sum, int number) {
		Map<String, Object> messageData = new HashMap<String, Object>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(new Date());
		String goods_status = "";
		int focus_count = 0;
		if (StringUtil.isNotEmpty(element.getElementsByClass("support").text())) {
			if (element.getElementsByClass("support").get(0).getElementsByTag("span").text().contains(Fields.THOUSAND)) {
				focus_count = Integer.valueOf(Pattern.compile("[^0-9]").matcher(element.getElementsByClass("support").get(0).getElementsByTag("span").text()).replaceAll("")) * 1000;
			} else if (element.getElementsByClass("support").get(0).getElementsByTag("span").text().contains(Fields.BEST)) {
				focus_count = Integer.valueOf(Pattern.compile("[^0-9]").matcher(element.getElementsByClass("support").get(0).getElementsByTag("span").text()).replaceAll("")) * 100;
			} else if (element.getElementsByClass("support").get(0).getElementsByTag("span").text().contains(Fields.MYRIAD)) {
				focus_count = Integer.valueOf(Pattern.compile("[^0-9]").matcher(element.getElementsByClass("support").get(0).getElementsByTag("span").text()).replaceAll("")) * 10000;
			} else {
				focus_count = Integer.valueOf(element.getElementsByClass("support").get(0).getElementsByTag("span").text());
			}
			try {
				goods_status = element.getElementsByClass("support").attr("onclick").replace("(", "").replace(")", "").trim();
				goods_status = goods_status.split(",")[1].toString();
				if (Integer.valueOf(goods_status) == 2) {
					goods_status = Fields.RAISE_OF;
				} else if (Integer.valueOf(goods_status) == 1) {
					goods_status = Fields.PREHEATING;
				} else if (Integer.valueOf(goods_status) == 7) {
					goods_status = Fields.RAISE_SUCCESS;
				} else if (Integer.valueOf(goods_status) == 6) {
					goods_status = Fields.PROJECT_SUCCESS;
				}
			} catch (Exception e) {
			}

			int day = Integer.valueOf(Pattern.compile("[^0-9]").matcher(element.getElementsByClass("fore3").get(0).getElementsByClass("p-percent").text()).replaceAll(""));
			calendar.add(calendar.DATE, day);//把日期往后增加一天.整数往后推,负数往前移动
			messageData.put("cust_keyword_id", wordsInfo.getCust_keyword_id());
			String egoodsId = Pattern.compile("[^0-9]").matcher(element.getElementsByClass("support").attr("id")).replaceAll("");
			messageData.put("goodsId", StringHelper.encryptByString(egoodsId + wordsInfo.getPlatform_name()));
			messageData.put("egoodsId", egoodsId);
			messageData.put("platform_goods_name", element.getElementsByClass("link-tit").attr("title").trim());//商品名称
			messageData.put("platform_name_en", wordsInfo.getPlatform_name());//平台
			messageData.put("platform_category", wordsInfo.getCust_keyword_name());//所在类
			messageData.put("curr_amount", element.getElementsByClass("fore2").get(0).getElementsByClass("p-percent").text().replace("￥", "").trim());//已筹金额 目前累计资金
			messageData.put("target_amount", "");//-目标金额
			messageData.put("finish_per", element.getElementsByClass("fore1").get(0).getElementsByClass("p-percent").text().replace("%", ""));//达成率
			messageData.put("support_count", "");//支持人数
			messageData.put("focus_count", focus_count);//-喜欢人数
			messageData.put("remain_day", String.valueOf(day));//剩余天数
			messageData.put("begin_date", SimpleDate.SimpleDateData().format(new Date()));//开始日期
			messageData.put("end_date", SimpleDate.SimpleDateData().format(calendar.getTime()));//结束日期
			messageData.put("goods_status", goods_status);//筹款状态
			messageData.put("goods_url", "https://z.jd.com" + element.getElementsByClass("link-pic").attr("href").trim());//商品详情页面
			messageData.put("goods_pic_url", element.getElementsByClass("lazyout").attr("src").trim());//商品图片
			messageData.put("update_time", SimpleDate.SimpleDateFormatData().format(new Date()));
			messageData.put("update_date", SimpleDate.SimpleDateFormatData().format(new Date()));
			messageData.put("batch_time", SimpleDate.SimpleDateFormatData().format(new Date()));
		}
		return messageData;
	}

	/**
	 * 项目名称：springmvc_crawler
	 * 类名称：GoodsProductDetails
	 * 类描述：解析封装数据插入
	 * 创建人：Jack
	 * 创建时间：2018年03月16日 上午10:50:10
	 * 修改备注：
	 *
	 * @throws SQLException
	 * @throws Exception
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public void insertData(List<Element> items, CrawKeywordsInfo wordsInfo, Map<String, String> map, JSONArray priceInfoJson) throws Exception {
		List<Map<String, Object>> insertItems = Lists.newArrayList();
		List<Map<String, Object>> insertItems_history = Lists.newArrayList();
		List<Map<String, Object>> insertprices = Lists.newArrayList();

		String tableName = "";
		String tableNamePrice = "";
		String tableName_history = "";
		if (items.size() > 0) {
			for (int i = 0; i < items.size(); i++) {
				Map<String, Object> insertItem = new HashMap<String, Object>();
				Map<String, Object> insertItem_history = new HashMap<String, Object>();
				Map<String, Object> insertItemPrice = new HashMap<String, Object>();
				Map<String, Object> messageData = new HashMap<String, Object>();
				Map<String, Object> dataMessage = new HashMap<String, Object>();
				if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TMALL_EN) || wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TMALL_GLOBAL_EN) || wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TAOBAO_EN)) {
					dataMessage = tmallMessage(items.get(i), wordsInfo, null, map, items.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_JD)) {
					dataMessage = jdMessage(items.get(i), wordsInfo, null, map, items.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_YHD)) {
					dataMessage = yhdMessage(items.get(i), wordsInfo, null, map.get(Fields.COUNT), items.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_SUNING)) {
					dataMessage = suningMessage(items.get(i), wordsInfo, null, map.get(Fields.COUNT), items.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_AMAZON) || wordsInfo.getPlatform_name().contains(Fields.PLATFORM_AMAZON_UN)) {
					dataMessage = amazonMessage(items.get(i), wordsInfo, null, map, items.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_JD_ZC)) {
					dataMessage = jdZcMessage(items.get(i), wordsInfo, null, map.get(Fields.COUNT), items.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.platform_1688)) {
					dataMessage = Message_1688(items.get(i), wordsInfo, null, map.get(Fields.COUNT), items.size(), i, map.get("database"));
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_KAOLA)) {
					dataMessage = kaoLaMessage(items.get(i), wordsInfo, null, map, items.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_JX)) {
					dataMessage = jxMessage(items.get(i), wordsInfo, null, map, items.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_KIDSWANT) || wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_KIDSWANT_OFF)) {
					dataMessage = kidswantMessage(items.get(i), wordsInfo, null, map, items.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_XIOUHUI)) {
					dataMessage = xiouhuiMessage(items.get(i), wordsInfo, null, map, items.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_YUOUHUI)) {
					dataMessage = youhuiMessage(items.get(i), wordsInfo, null, map, items.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.GUOMEI)) {
					dataMessage = gomeMessage(items.get(i), wordsInfo, null, map, items.size(), i);
				}
				if (dataMessage.toString().contains(Fields.KEYWORDNAME) || dataMessage.toString().contains(Fields.EGOODSID)) {
					if (Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_11) || Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_12)) {
						tableName = Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH;
						insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(keywordsInfo(wordsInfo, map.get(Fields.COUNT), i, items.size(), dataMessage));
						insertItems.add(insertItem);
					} else {
						if (Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_10)) {
							if (!map.get(Fields.STATUS_TYPE).equals(Fields.COUNT_5)) {
								if (!wordsInfo.getPlatform_name().equals(Fields.platform_1688) && !wordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD_ZC)) {
									tableName_history = Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH;
									insertItem_history = BeanMapUtil.convertBean2MapWithUnderscoreName(keywordsInfo(wordsInfo, map.get(Fields.COUNT), i, items.size(), dataMessage));
									insertItems_history.add(insertItem_history);
								}
							}

						}
						if (wordsInfo.getPlatform_name().contains(Fields.platform_1688)) {
							tableName = Fields.TABLE_CRAW_VENDOR_INFO;
							tableNamePrice = Fields.TABLE_CRAW_VENDOR_PRICE_INFO;
						} else if (wordsInfo.getPlatform_name().contains(Fields.PLATFORM_JD_ZC)) {
							tableName = Fields.CRAW_GOODS_TRANSLATE_INFO;
						} else {
							tableName = Fields.TABLE_CRAW_GOODS_INFO;
							tableNamePrice = Fields.TABLE_CRAW_GOODS_PRICE_INFO;
						}
						messageData.putAll(dataMessage);
						if (wordsInfo.getPlatform_name().contains(Fields.platform_1688)) {
							insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorInfoData(wordsInfo, messageData));
							insertItemPrice = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorPriceInfoData(wordsInfo, messageData));

						} else if (wordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD_ZC)) {
							insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsCrowdfundingInfo(wordsInfo, messageData));
						} else {
							if (!map.get(Fields.STATUS_TYPE).equals(Fields.COUNT_5)) {//颜色
								insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsInfoData(wordsInfo, messageData));
								if (Integer.valueOf(messageData.get("ItemPrice").toString()) == 0) {
									insertItemPrice = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsPriceInfoData(wordsInfo, messageData));

								}
							}
						}
						if (insertItemPrice.size() > 0) {
							insertprices.add(insertItemPrice);
						}
						insertItems.add(insertItem);
					}
				}
			}
		} else {
			for (int i = 0; i < priceInfoJson.size(); i++) {
				Map<String, Object> insertItem = new HashMap<String, Object>();
				Map<String, Object> insertItem_history = new HashMap<String, Object>();
				Map<String, Object> insertItemPrice = new HashMap<String, Object>();
				Map<String, Object> messageData = new HashMap<String, Object>();
				Map<String, Object> dataMessage = new HashMap<String, Object>();
				JSONObject messagejson = JSONObject.fromObject(priceInfoJson.toArray()[i]);
				if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TMALL_EN) || wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TMALL_GLOBAL_EN) || wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TAOBAO_EN)) {
					dataMessage = tmallMessage(null, wordsInfo, messagejson, map, priceInfoJson.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_JD)) {
					dataMessage = jdMessage(null, wordsInfo, messagejson, map, priceInfoJson.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.platform_1688)) {
					dataMessage = Message_1688(null, wordsInfo, messagejson, map.get(Fields.COUNT), items.size(), i, map.get("database"));
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TAOBAO_ZC)) {
					dataMessage = taobaoZcMessage(null, wordsInfo, messagejson, map.get(Fields.COUNT), items.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_JD_ZC)) {
					dataMessage = jdZcMessage(null, wordsInfo, messagejson, map.get(Fields.COUNT), items.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_VIP)) {
					dataMessage = vipMessage(null, wordsInfo, messagejson, map, priceInfoJson.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_KIDSWANT) || wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_KIDSWANT_OFF)) {
					dataMessage = kidswantMessage(null, wordsInfo, messagejson, map, items.size(), i);
				}else if(wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_HPK)){
					dataMessage = hpkMessage(null, wordsInfo, messagejson, map, items.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_SKS)) {
					dataMessage = sksMessage(null, wordsInfo, messagejson, map, items.size(), i);
				} else if (wordsInfo.getPlatform_name().equalsIgnoreCase("ehaier")) {
					dataMessage =ehaierMessage(null, wordsInfo, messagejson, map, items.size(), i);
				}
				if (dataMessage.size() > 0) {
					messageData.putAll(dataMessage);
					if (Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_11) || Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_12)) {
						tableName = Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH;
						insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(keywordsInfo(wordsInfo, map.get("count"), i, priceInfoJson.size(), dataMessage));
						insertItems.add(insertItem);
					} else {
						if (Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_10)) {
							if (!wordsInfo.getPlatform_name().equals(Fields.platform_1688) && !wordsInfo.getPlatform_name().equals(Fields.PLATFORM_TAOBAO_ZC) && !wordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD_ZC)) {
								tableName_history = Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH;
								insertItem_history = BeanMapUtil.convertBean2MapWithUnderscoreName(keywordsInfo(wordsInfo, map.get(Fields.COUNT), i, items.size(), dataMessage));
								insertItems_history.add(insertItem_history);
							}
						}
						if (wordsInfo.getPlatform_name().equals(Fields.platform_1688)) {
							tableName = Fields.TABLE_CRAW_VENDOR_INFO;
							tableNamePrice = Fields.TABLE_CRAW_VENDOR_PRICE_INFO;
							insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorInfoData(wordsInfo, messageData));
							if (Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_10)) {
								insertItemPrice = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorPriceInfoData(wordsInfo, messageData));
							}
						} else if (wordsInfo.getPlatform_name().equals(Fields.PLATFORM_TAOBAO_ZC) || wordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD_ZC)) {
							tableName = Fields.CRAW_GOODS_TRANSLATE_INFO;
							insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsCrowdfundingInfo(wordsInfo, messageData));
						} else {
							if (dataMessage.size() > 0) {
								tableName = Fields.TABLE_CRAW_GOODS_INFO;
								tableNamePrice = Fields.TABLE_CRAW_GOODS_PRICE_INFO;
								insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsInfoData(wordsInfo, messageData));
								insertItemPrice = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsPriceInfoData(wordsInfo, messageData));
							}
						}
						if (insertItemPrice.size() > 0) {
							insertprices.add(insertItemPrice);
						}
						if (insertItem.size() > 0) {
							insertItems.add(insertItem);
						}
					}
				}
			}
		}

		if (insertItems.size() > 0) {
			insertIntoData(insertItems, map.get("database"), tableName);
		}
		if (insertprices.size() > 0) {
			insertIntoData(insertprices, map.get("database"), tableNamePrice);
		}
		if (insertItems_history.size() > 0) {
			insertIntoData(insertItems_history, map.get("database"), tableName_history);
		}
	}

	//插入数据
	@TargetDataSource
	public void insertIntoData(List<Map<String, Object>> insertItems, String database, String tableName) {
		SqlSession session = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH, false);
		Map<String, Object> params = Maps.newHashMap();
		try {
			if (!insertItems.isEmpty()) {
				//这里把数据分成每1000条执行一次，可根据实际情况进行调整
				int count = insertItems.size() / 15;
				int sum = insertItems.size() % 15;
				for (int i = 0; i <= count; i++) {
					List<Map<String, Object>> subList = Lists.newArrayList();
					if (i == count) {
						if (sum != 0) {
							subList = insertItems.subList(i * 15, 15 * i + sum);
						} else {
							continue;
						}
					} else {
						subList = insertItems.subList(i * 15, 15 * (i + 1));
					}
					params.put("table_name", tableName);
					params.put("fields", subList.get(0));
					params.put("list", subList);
					session.insert(dataDsNameSpace + ".insertDatas", params);
					session.commit();
					session.clearCache();// 清理缓存，防止溢出
				}
			}
		} catch (Exception e) {
			logger.error("==>>数据插入发送异常："+e+"<<==");
			e.printStackTrace();
			session.rollback();
		} finally {
			session.close();
		}

	}

	// 封装对象
	public Craw_goods_InfoVO crawGoodsInfoData(CrawKeywordsInfo wordsInfo, Map<String, Object> data) {
		Craw_goods_InfoVO info = new Craw_goods_InfoVO();
		//SimpleDateFormat data_time=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		info.setPosition((Integer.valueOf(data.get("number").toString()) + 1) + "/" + Integer.valueOf(data.get("count").toString()) + "*" + Integer.valueOf(data.get("sum").toString()));
		info.setCust_keyword_id(Integer.valueOf(wordsInfo.getCust_keyword_id()));
		info.setBatch_time(data.get("batch_time").toString());
		info.setPlatform_name_en(wordsInfo.getPlatform_name());
		info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setGoodsId(data.get("goodsId").toString());
		info.setPlatform_goods_name(data.get("goodsName").toString());
		info.setEgoodsId(data.get("keywordName").toString());
		info.setGoods_pic_url(data.get("goods_pic_url").toString());
		info.setGoods_status(Integer.valueOf(Fields.STATUS_ON));
		info.setGoods_url(data.get("goodsUrl").toString());
		info.setPlatform_shoptype(data.get("platform_shoptype").toString());
		info.setPlatform_sellername(data.get("platform_shopname").toString());
		info.setPlatform_shopname(data.get("platform_shopname").toString());
		info.setSale_qty(data.get("sale_qty").toString());
		info.setTtl_comment_num(data.get("ttl_comment_num").toString());
		info.setDelivery_place(data.get("item_loc").toString());//位置
		info.setBsr_rank(data.get("bsr_rank").toString());//排名

		if (data.toString().contains("platform_shopid")) {
			info.setPlatform_shopid(data.get("platform_shopid").toString());
		}
		if(data.toString().contains("platform_sellerid")){
			info.setPlatform_sellerid(data.get("platform_sellerid").toString());
		}

		if (data.toString().contains("hasStock")) {
			//是否有货
			info.setInventory(data.get("hasStock").toString());
		}
		return info;
	}

	public Craw_goods_attribute_Info attributeInfo(String key, String vale, Map<String, String> map) {
		Craw_goods_attribute_Info info = new Craw_goods_attribute_Info();
		info.setAttri_value(vale);//值
		info.setCraw_attri_cn(key);//key
		info.setCust_keyword_id(Integer.valueOf(map.get("keywordId").toString()));
		info.setEgoodsid(map.get("egoodsId").toString());
		info.setGoodsid(map.get("goodsId").toString());//insertItem
		info.setPlatform_name_en(map.get("platform").toString());
		info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setBatch_time(map.get("timeDate").toString());
		return info;
	}

	public Craw_goods_Vendor_Info crawGoodsVendorInfoData(CrawKeywordsInfo wordsInfo, Map<String, Object> data) {
		Craw_goods_Vendor_Info info = new Craw_goods_Vendor_Info();
		//SimpleDateFormat data_time=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		info.setCust_keyword_id(Integer.valueOf(wordsInfo.getCust_keyword_id()));
		info.setBatch_time(data.get("batch_time").toString());
		info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setGoodsid(data.get("goodsId").toString());
		info.setPlatform_goodsname(data.get("goodsName").toString());
		info.setEgoodsid(data.get("keywordName").toString());
		info.setPlatform_goods_picurl(data.get("goods_pic_url").toString());
		info.setPlatform_goodsurl(data.get("goodsUrl").toString());
		info.setPlatform_vendorid(data.get("platform_vendorid").toString());//供应商Id
		info.setPlatform_vendorname(data.get("companyName").toString());//供应商名称
		info.setPlatform_vendortype(data.get("SourceFactory").toString());//源头工厂
		info.setPurchase_back_rate(data.get("purchase_back_rate").toString());//回头率
		info.setGoods_status(Integer.valueOf(Fields.STATUS_ON));//商品上架
		info.setChannel(Fields.CLIENT_MOBILE);
		if (data.toString().contains("platform_vendoraddress")) {
			info.setPlatform_vendoraddress(data.get("platform_vendoraddress").toString());//商品地址
		}
		return info;
	}

	//众筹表
	public Craw_goods_crowdfunding_Info crawGoodsCrowdfundingInfo(CrawKeywordsInfo wordsInfo, Map<String, Object> data) {
		Craw_goods_crowdfunding_Info info = new Craw_goods_crowdfunding_Info();
		try {
			info.setCust_keyword_id(Integer.valueOf(wordsInfo.getCust_keyword_id()));
			info.setGoodsId(data.get("goodsId").toString());
			info.setEgoodsId(data.get("egoodsId").toString());
			info.setPlatform_goods_name(data.get("platform_goods_name").toString());
			info.setPlatform_name_en(data.get("platform_name_en").toString());//所在类
			info.setPlatform_category(data.get("platform_category").toString());//所在类
			info.setCurr_amount(Float.valueOf(data.get("curr_amount").toString()));//已筹金额 目前累计资金
			info.setFinish_per(Float.valueOf(data.get("finish_per").toString()));//达成率
			info.setFocus_count(Integer.valueOf(data.get("focus_count").toString()));//-喜欢人数
			info.setRemain_day(Integer.valueOf(data.get("remain_day").toString()));//剩余天数
			info.setBegin_date(data.get("begin_date").toString());//开始日期
			info.setEnd_date(data.get("end_date").toString());//结束日期
			info.setGoods_url(data.get("goods_url").toString());//商品详情页面
			info.setGoods_pic_url(data.get("goods_pic_url").toString());//商品图片
			info.setUpdate_time(data.get("update_time").toString());
			info.setUpdate_date(data.get("update_date").toString());
			info.setBatch_time(data.get("batch_time").toString());
			info.setGoods_status(data.get("goods_status").toString());//筹款状态
			info.setPrice_status(Fields.STATUS_COUNT_1);
			if (StringUtil.isNotEmpty(data.get("support_count").toString())) {
				info.setSupport_count(Integer.valueOf(data.get("support_count").toString()));//支持人数);
			}
			if (StringUtil.isNotEmpty(data.get("target_amount").toString())) {
				info.setTarget_amount(Float.valueOf(data.get("target_amount").toString()));//-目标金额
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return info;
	}

	public Craw_goods_Vendor_price_Info crawGoodsVendorPriceInfoData(CrawKeywordsInfo wordsInfo, Map<String, Object> data) {
		Craw_goods_Vendor_price_Info info = new Craw_goods_Vendor_price_Info();
		info.setBatch_time(data.get("batch_time").toString());
		info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setGoodsid(data.get("goodsId").toString());
		info.setPurchase_price(data.get("purchase_price").toString());//价格
		info.setPurchase_amount(data.get("purchase_amount").toString());//件数
		info.setCust_keyword_id(Integer.valueOf(wordsInfo.getCust_keyword_id()));//商品keywordId
		info.setEgoodsid(data.get("keywordName").toString());//商品egoodsId
		info.setChannel(data.get("channel").toString());//渠道
		info.setPurchase_unit(data.get("purchase_unit").toString());//件 个 台
		info.setChannel(data.get("channel").toString());
		return info;
	}

	public Craw_goods_Price_InfoVO crawGoodsPriceInfoData(CrawKeywordsInfo wordsInfo, Map<String, Object> data) {
		Craw_goods_Price_InfoVO info = new Craw_goods_Price_InfoVO();
		info.setBatch_time(data.get("batch_time").toString());
		info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setGoodsid(data.get("goodsId").toString());
		info.setCurrent_price(data.get("price").toString());
		info.setOriginal_price(data.get("price").toString());
		info.setCust_keyword_id(wordsInfo.getCust_keyword_id());
		info.setChannel(data.get("channel").toString());
		return info;
	}


	public Craw_keywords_temp_Info_forsearch keywordsInfo(CrawKeywordsInfo wordsInfo, String count, int number, int sum,  Map<String, Object> messageData) {
		Craw_keywords_temp_Info_forsearch info = new Craw_keywords_temp_Info_forsearch();
		info.setPosition((number + 1) + "/" + count + "*" + sum);
		info.setCust_keyword_name(messageData.get(Fields.KEYWORDNAME).toString());
		info.setCrawling_status(Fields.STATUS_ON);
		info.setFeature_1(Fields.STATUS_ON);
		info.setCust_account_id(wordsInfo.getCust_account_id());
		info.setCust_keyword_id(wordsInfo.getCust_keyword_id());
		info.setPlatform_name(wordsInfo.getPlatform_name());
		info.setPlatform_shopname(wordsInfo.getCust_keyword_remark());
		info.setCust_account_name(wordsInfo.getCust_account_name() + ":" + wordsInfo.getCust_keyword_name() + "/" + wordsInfo.getCust_keyword_remark());
		info.setCust_keyword_type(Fields.EGOODS_ID); //wordsInfo.getCust_keyword_type()
		info.setCrawling_fre(wordsInfo.getCrawling_fre());
		info.setAlways_abnormal(wordsInfo.getAlways_abnormal());
		info.setIs_presell(wordsInfo.getIs_presell());
		info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setCust_keyword_url(messageData.get("goodsUrl").toString());
		info.setPlatform_goodsname(messageData.get("goodsName").toString());
		if (Fields.STATUS_10 == Integer.valueOf(wordsInfo.getCrawling_status())) {
			info.setCrawed_status(Integer.valueOf(Fields.COUNT_01.toString()));
		} else {
			info.setCrawed_status(Integer.valueOf(Fields.COUNT_0.toString()));
		}
		return info;
	}
}
