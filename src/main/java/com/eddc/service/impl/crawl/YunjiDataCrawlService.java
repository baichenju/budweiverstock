package com.eddc.service.impl.crawl;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Parameter;
import com.eddc.service.AbstractProductAnalysis;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.util.*;


/**
 * @author jack.zhao
 */
@Service("yunjiDataCrawlService")
public class YunjiDataCrawlService extends AbstractProductAnalysis {
	private static Logger logger=LoggerFactory.getLogger(YunjiDataCrawlService.class);
	@Override
	public Map<String, Object> getProductLink(Parameter parameter) throws Exception {
		Map<String, Object> itemYun = new HashMap<>(10);
		parameter.setItemUrl_1(Fields.YUNJI_IMAGE_URL_1.replace(Fields.EDOODSID,parameter.getEgoodsId()));
		parameter.setItemUtl_2(Fields.YUNJI_IMAGE_URL_2.replace(Fields.EDOODSID,parameter.getEgoodsId()));
		itemYun=getProductRequest(parameter);
		return itemYun;
	}

	/*获取手机详情信息*/
	@SuppressWarnings("rawtypes")
	@Override
	public Map<String, Object> getProductItemAnalysisData(String StringMessage, Parameter parameter) throws Exception {
		HashMap<String, Object> item=new HashMap<String, Object>(10);
		JSONObject json = JSONObject.parseObject(StringMessage);
		Craw_goods_InfoVO info=productInfo(parameter);
		Craw_goods_Price_Info price=productInfoPrice(parameter);
		//Map<String,Object> insertItem=new HashMap<String,Object>(10);
		List<Map<String,Object>> insertItems = Lists.newArrayList();
		//Map<String,Object> insertPriceMap=new HashMap<String,Object>(10);
		List<Map<String,Object>> insertPriceList = Lists.newArrayList();
		int count=0; String markText="";
		StringBuilder buffers=new StringBuilder();
		try {
			if(json.toJSONString().contains("itemName")){
			

				price.setSale_qty(json.getString("itemMonthlySales"));
				price.setChannel("APP");
		
				//促销 promotion
				//String promotion=json.getJSONObject("objItemBo").getString("activityName")+":"+json.getJSONObject("objItemBo").getString("activityDesc");
				String promotion="null:null";
				String shopName="";
				//增加对额外促销信息 json包的判断
				parameter.setItemUrl_1("http://ys.yunjiglobal.com/yunjiysapp/app/queryItemListPolymerizationMark.json?appCont=0&strVersion=4&itemIds="+parameter.getEgoodsId()+"&businessMarkType=1&templateId=6&adValue=NewBenefit");
				/*请求数据*/

				String StringItem=getRequest(parameter);

				JSONArray dataArr = JSONObject.parseObject(StringItem).getJSONArray("data");
				//获取了该促销json包才进行解析获取 店铺名 与 促销信息
				if(dataArr!=null) {
					for (Object o : dataArr) {
						JSONObject js = (JSONObject) o;
						String itemId = js.getString("itemId");
						//		根据type值获取相应信息：6 ：活动 7：店铺  2、8：促销
						if (itemId.equals(parameter.getEgoodsId())) {
							JSONArray itemLabel = js.getJSONArray("itemLabelMarkingResponses");
							StringBuilder sb = new StringBuilder();
							for (Object o1 : itemLabel) {

								JSONObject js1 = (JSONObject)o1;
								String type = js1.getString("type");
								switch (type){
									case "7":
										shopName = js1.getString("text");
										break;
									case "8":
									case "2":
										sb.append(js1.getString("text")).append("&");
										break;
								}
							}
							info.setPlatform_shopname(shopName);
							//如果没有促销的try catch
							try{
								promotion = sb.deleteCharAt(sb.length()-1).toString();}
							catch (Exception e){
								promotion = "";
							}


						}
					}
				}



				parameter.setItemUrl_1("http://marketing.yunjiglobal.com/yunjimarketingapp/app/itemBusinessMark.json?itemIds="+parameter.getEgoodsId()+"&businessMarkType=1");
				/*请求数据*/
				StringMessage=getRequest(parameter);
				if(StringUtils.isNotEmpty(StringMessage)){
					if(StringMessage.toString().contains("markText")){
						JSONObject jsonObject = JSONObject.parseObject(StringMessage);
						JSONArray jsonList = jsonObject.getJSONArray("data");
						for(int i=0;i<jsonList.size();i++){
							JSONObject object=JSONObject.parseObject(jsonList.toArray()[i].toString());
							markText+=object.getString("markText");
						}
					}
				}
				buffers.append(promotion.replace("null:null","").replace("true", "").replace("false", ""));
				if(StringUtils.isNotEmpty(markText)){
					buffers.append("&&"+markText);
				}

				price.setPromotion(buffers.toString());
				//是否包邮
				String deliveryInfo=json.getString("platformEnsure");
				if(deliveryInfo.contains(Fields.PACK_MAIL)){
					info.setDelivery_info(Fields.PACK_MAIL);
				}else{
					info.setDelivery_info(Fields.DONTPACK_MAIL);
				}
				//判断是否存在另外的json包，存在就进行解析并插入
				parameter.setItemUrl_1("http://item.yunjiglobal.com/yunjiitemapp/buyer/item/getItemAttrList.json?t=1604990335486&itemId="+parameter.getEgoodsId()+"&isNewShopper=0&spikeActivityId=0&activityId=0&activityType=0");
				/*请求数据*/
				String StringSkuName=getRequest(parameter);
				String skuName=null;
				if(StringUtils.isNotEmpty(StringSkuName)){

					String skuString = JSON.parseObject(StringSkuName).getJSONObject("sku").getString("availSku");
					net.sf.json.JSONObject js = net.sf.json.JSONObject.fromObject(skuString);
					Iterator keys = js.keys();
					while (keys.hasNext()){
						String key = (String)keys.next();
						skuName = key;
						//判断是否存在优惠价，否则现价等于原价
						if (js.getJSONObject(key).containsKey("vipPrice")&&js.getJSONObject(key).getInt("vipPrice")!=0){
							price.setOriginal_price(String.valueOf(js.getJSONObject(key).getInt("vipPrice")/100));
						}else{
							price.setOriginal_price(String.valueOf(js.getJSONObject(key).getInt("stockPrice")/100));
						}

						price.setCurrent_price(String.valueOf(js.getJSONObject(key).getInt("stockPrice")/100));
						price.setSku_name(skuName);
						if ("0".equals(js.getJSONObject(key).get("stockCount"))){
							continue;//库存为0就跳过
						}

						count=1;
						//开始插入商品价格信息
						insertPriceList.addAll(list(price));
						//insertPriceMap= BeanMapUtil.convertBean2MapWithUnderscoreName(price);
						//insertPriceList.add(insertPriceMap);
						getProductSave(insertPriceList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PRICE_INFO);
						insertPriceList=Lists.newArrayList();

					}
				}

				JSONArray  array = json.getJSONObject("objItemBo").getJSONArray("tuList");
				if(array.size()>0){
					for(int i=0;i<array.size();i++){
						JSONObject jsonCount = JSONObject.parseObject(array.toArray()[i].toString());
						if(jsonCount.getString("skuStatus").equals("3")){
							logger.info("==>>商品sku下架："+jsonCount.getString("tuName")+"<<==");
							continue;
						}
						price.setOriginal_price(jsonCount.getString("specPrice"));
						//price.setInventory(jsonCount.getString("specStock"));
						if (skuName!=null){
							skuName=jsonCount.getString("tuName");
							price.setSku_name(skuName);
						}
						JSONArray  list = jsonCount.getJSONArray("tuRelationList");
						if(list!=null && list.size()>0){
							JSONObject specPrice = JSONObject.parseObject(list.toArray()[0].toString());
							String pricke=specPrice.getString("skuPrice");
							price.setCurrent_price(pricke);
							price.setSKUid(specPrice.getString("skuCode"));
						}else{
						price.setCurrent_price(jsonCount.getString("specPrice"));
						price.setSKUid(jsonCount.getString("tuCode"));

						count=1;
						if(skuName!=null&&!skuName.startsWith("规格")){
							//开始插入商品价格信息
							insertPriceList.addAll(list(price));
							//insertPriceMap= BeanMapUtil.convertBean2MapWithUnderscoreName(price);
							//insertPriceList.add(insertPriceMap);
							getProductSave(insertPriceList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PRICE_INFO);
							insertPriceList=Lists.newArrayList();
						}

					}


					}
				}else{
					price.setCurrent_price(json.getJSONObject("objItemBo").getString("itemPrice"));
					price.setOriginal_price(json.getJSONObject("objItemBo").getString("itemVipPrice"));
				}

				String stock=json.getJSONObject("objItemBo").getString("stock");
				info.setInventory(stock);
				String image=json.getJSONObject("objItemBo").getJSONObject("itemExpandRedisBo").getString("transparentImage");
				if(StringUtils.isEmpty(image)){
					image=json.getJSONObject("objItemBo").getJSONObject("itemExpandRedisBo").getString("supermarketImg");
				}
				info.setGoods_pic_url(image);
				info.setPlatform_goods_name(json.getJSONObject("objItemBo").getString("itemName"));

			}

			//店铺
			if(json.toString().contains("全球精品")){
				info.setPlatform_sellername(Fields.YES_PROPRIETARY);
				info.setPlatform_shopname(Fields.YES_PROPRIETARY);
				info.setPlatform_shoptype(Fields.YES_PROPRIETARY);
			}
			//销量
			info.setSale_qty(json.getString("itemMonthlySales"));


			if(StringUtils.isNotEmpty(price.getCurrent_price())){
				//开始插入商品详情信息
				insertItems.addAll(list(info));
				
				getProductSave(insertItems,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_INFO);
				//开始插入商品价格信息
				if(count==0){
					//insertPriceMap= BeanMapUtil.convertBean2MapWithUnderscoreName(price);
					//insertPriceList.add(insertPriceMap);
					insertPriceList.addAll(list(price));
					getProductSave(insertPriceList,parameter.getListjobName().get(0).getDatabases(),Fields.TABLE_CRAW_GOODS_PRICE_INFO);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("【解析云集数据发生异常：{}】",e);
		}
		item.put("Craw_goods_Info", info);
		item.put("Craw_goods_Price_Info", price);
		return item;
	}




}
