package com.eddc.service.impl.crawl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.eddc.model.Craw_goods_shop_promotion_Info;
import com.eddc.model.Parameter;
import com.eddc.service.impl.http.HttpClientService;
import com.eddc.util.BeanMapUtil;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service("storeDataCrawlService")
public class StoreDataCrawlService {
	private static Logger logger = LoggerFactory.getLogger(StoreDataCrawlService.class);
	@Autowired
	private SearchDataCrawlService search_DataCrawlService;
	@Autowired
    private HttpClientService httpClientService;
	@SuppressWarnings("unchecked")
	public Map<String, Object>productStore(Parameter parameter){
		Map<String, Object> storeMap=new HashedMap (5);
		StringBuffer bufer=new StringBuffer();
		try {
			Craw_goods_shop_promotion_Info shopPromInfo = new Craw_goods_shop_promotion_Info();
			shopPromInfo.setBatch_time(parameter.getBatch_time());
			shopPromInfo.setEgoodsid(parameter.getEgoodsId());
			shopPromInfo.setChannel(parameter.getListjobName().get(0).getClient());
			shopPromInfo.setCust_keyword_id(Integer.valueOf(parameter.getCrawKeywordsInfo().getCust_keyword_id()));
			shopPromInfo.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());
			shopPromInfo.setGoodsid(parameter.getGoodsId());
			/** 店铺促销接口解析*/   
			List<Map<String, Object>> shopPromList = Lists.newArrayList();

			parameter.setItemUrl_1("http://api.vephp.com/shopcoupon?vekey=V00005620Y02376026&itemid="+parameter.getEgoodsId());
			parameter.setItemUtl_2("https://detail.tmall.com/item.htm?id="+parameter.getEgoodsId());
			
			String productItem = httpClientService.interfaceSwitch(parameter);
			
			if(productItem.contains("titles")) {
				JSONObject promJson = JSONObject.parseObject(productItem);
				String database = parameter.getListjobName().get(0).getDatabases();
				if (!promJson.getString("msg").contains("检查参数")) {
					if (promJson.getString("has_coupon_prom").equals("0")){
						JSONArray couponArray = promJson.getJSONArray("data");
						for (Object o : couponArray) {
							JSONObject couponJson = (JSONObject) o;
							if (couponJson.getJSONArray("titles").size() > 0 && couponJson.getJSONArray("subTitles").size() > 0) {
								if (couponJson.getJSONArray("titles").getString(0).contains("使用")) {
									bufer.append(couponJson.getJSONArray("titles").getString(0).replace("使用", "减")).append(couponJson.getString("priceText")).append("到").append(couponJson.getJSONArray("subTitles").getString(0)).append("&&");
								}
								if (couponJson.getJSONArray("titles").getString(0).contains("卡")) {
									bufer.append(couponJson.getJSONArray("titles").getString(0)).append("到").append(couponJson.getJSONArray("subTitles").getString(0)).append("&&");
								}
							}
						}

					}else if (promJson.getString("has_coupon_prom").equals("1")&&promJson.getJSONArray("data").size()>0){
						for (Object o : promJson.getJSONArray("data")) {
							JSONObject couponJson = (JSONObject) o;
							bufer.append(couponJson.getJSONArray("items").getJSONObject(0).getString("title").replace(",省","减").replaceAll("元","")).append("到").append(couponJson.getJSONArray("subTitles").getString(0)).append("&&");
						}
					}
					//店铺优惠入库
					shopPromInfo.setPromotion(bufer.toString());
					Map<String, Object> insertItem = new HashMap<String, Object>(10);
					insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(shopPromInfo);
					shopPromList.add(insertItem);
					search_DataCrawlService.insertIntoData(shopPromList, database,"craw_goods_shop_promotion_Info");
				}	
			}else {
				logger.error("==>>店铺优惠券查询成功！未找到该店的店铺券，如果您确信该店有店铺券，那么请检查参数是否正确！<<==");	
		 }
			
		}catch(Exception e) {
			logger.error("==>>解析店铺数据异常<<=="+e);
		}
		return storeMap;

	}
}
