package com.eddc.service.impl.crawl;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Parameter;
import com.eddc.service.AbstractProductAnalysis;
import com.eddc.util.Fields;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @author jack.zhao
 */
@Slf4j
@Service("sksDataCrawlService")
public class SksDataCrawlService extends AbstractProductAnalysis {

	/*****************************************解析数据****************************************************************************************************/

    /**
     *@Description: 请求数据
     *@Param:  parameter 参数
     *@date: 2020/12/11
     *@Author: Jack.zhao
     *@return: Map
     *
     */
	@Override
	public Map<String, Object> getProductLink(Parameter parameter) throws Exception {
		Map<String, Object> itemSks = new HashMap<>(10);
		parameter.setItemUrl_1("http://api.saintcos.hk/api/v2/goods/getGoodsInfo?accountId=acda97b8c9064508854a89942d43de38&appkey=86065876&goodsId="+parameter.getEgoodsId()+"&memberId=acda97b8c9064508854a89942d43de38&needActivity=1&needImages=1&needIntro=1&timestamp="+System.currentTimeMillis()+"&token=token&topSign=54BE8336122A705EAE4241BCC17EE996E82A3BEC");
		parameter.setItemUtl_2("http://home.saintcos.hk/#/goods/show/"+parameter.getEgoodsId());
		if(StringUtils.isEmpty(parameter.getDeliveryPlace().getDelivery_place_code())){
			parameter.setCookie("SHOPEX_SID_MEMBER=7ceae75272d2121503d1bf7ce28f64a5; S[loginName]=yd15800689786; S[MEMBER]=1205-0fada46b1c586b37825084a385e26909-aaaf6c0077d3c433ea21d494da041a73-1575511849; S[UNAME]=yd15800689786; S[MLV]=1; S[CUR]=CNY; S[CART_COUNT]=0");
		}else{
			parameter.setCookie(parameter.getDeliveryPlace().getDelivery_place_code());
		}
		itemSks=getProductRequest(parameter);
		return itemSks;
	}

	/**
	 *@Description:  商品解析
	 *@Param: StringMessage 报文
	 *@param parameter 参数
	 *@date: 2020/12/11
	 *@Author: Jack.zhao
	 *@return:
	 *
	 */
	@Override
	public Map<String, Object> getProductItemAnalysisData(String StringMessage, Parameter parameter) {

		Map<String, Object> item = new HashMap<String, Object>(10);

		List<Map<String, Object>> insertPriceList = Lists.newArrayList();


		List<Map<String, Object>> insertItems = Lists.newArrayList();
		Craw_goods_InfoVO info = productInfo(parameter);
		try {
			if (StringMessage.toString().contains("name")) {

				JSONObject json = JSONObject.fromObject(StringMessage);
				info.setPlatform_goods_name(json.getJSONObject("data").getString("name"));
				info.setGoods_pic_url(json.getJSONObject("data").getString("bigPic"));
				info.setPlatform_sellername(json.getJSONObject("data").getString("brandName"));
				info.setPlatform_shopname(json.getJSONObject("data").getString("brandName"));

				JSONArray array = json.getJSONObject("data").getJSONArray("specs");
				if(array!=null && array.size()>0) {
					for(int i=0;i<array.size();i++) {
						Craw_goods_Price_Info priceInfo =productInfoPrice(parameter);
						JSONObject message=JSONObject.fromObject(array.toArray()[i].toString());
						priceInfo.setPromotion(message.getString("life"));
						String num=message.getString("num");
						String specValue=message.getString("specValue");
						//获取商品单罐价格
						parameter.setItemUrl_1("http://api.saintcos.hk/api/v2/goods/getGoodsPrice?accountId=acda97b8c9064508854a89942d43de38&appkey=86065876&cityId=330100&goodsId="+parameter.getEgoodsId()+"&life="+message.getString("life")+"&memberId=acda97b8c9064508854a89942d43de38&num=1&productNum="+num+"&timestamp="+System.currentTimeMillis()+"&token=token&topSign=DB538D32B76EC6C2BDE145298B4F49E38C39DC87");
						//请求数据
						String StringItem = getRequest(parameter);
						if (parameter.toString().contains("data")) {
							JSONObject priceData = JSONObject.fromObject(StringItem);
							priceInfo.setInventory(priceData.getJSONObject("data").getString("enableStore"));
							priceInfo.setOriginal_price(priceData.getJSONObject("data").getString("maxPrice"));
							priceInfo.setCurrent_price(priceData.getJSONObject("data").getString("minPrice"));
							priceInfo.setSku_name(specValue+", 商品单价："+priceData.getJSONObject("data").getString("minPrice"));
							priceInfo.setChannel(Fields.CLIENT_PC);
							//开始插入商品价格信息
							//insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
							//insertPriceList.add(insertPriceMap);
							
							/**封装数据 价格**/
							insertPriceList.addAll(list(priceInfo));
							
						}

						info.setGoods_status(Fields.STATUS_COUNT_1);
					}	
				}
				else {
					info.setGoods_status(Fields.STATUS_COUNT);
				}
				info.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());;
				info.setFeature1(Fields.COUNT_01);


				//开始插入商品详情信息
				//insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
				//insertItems.add(insertItem);

				/**封装数据 详情**/
				insertItems.addAll(list(info));
				
				getProductSave(insertItems, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_INFO);

				if (insertPriceList != null && insertPriceList.size() > 0) {
					getProductSave(insertPriceList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PRICE_INFO);
				}

			}
		} catch (Exception e) {
			log.error("==>>解析圣卡斯详情发送异常：" + e + "<<==");
			e.printStackTrace();
		}
		return item;

	}
}
