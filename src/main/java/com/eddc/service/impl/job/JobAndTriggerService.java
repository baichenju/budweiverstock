package com.eddc.service.impl.job;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.mapper.CrawlerPublicClassMapper;
import com.eddc.mapper.JobAndTriggerMapper;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.JobAndTrigger;
import com.eddc.model.QrtzCrawlerTable;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
@Service
public class JobAndTriggerService { 
	@Autowired
	private JobAndTriggerMapper jobAndTriggerMapper;
	@Autowired
	private CrawlerPublicClassMapper crawlerPublicClassMapper;
	private static Logger logger = LoggerFactory.getLogger(JobAndTriggerService.class); 
	//private  SimpleDateFormat data=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public PageInfo<JobAndTrigger> getJobAndTriggerDetails(int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		List<JobAndTrigger> list = jobAndTriggerMapper.getJobAndTriggerDetails();
		PageInfo<JobAndTrigger> page = new PageInfo<JobAndTrigger>(list);
		return page;
	}
	public List<QrtzCrawlerTable>queryJob(String job_Name){
		logger.info("【获取请求调度参数】");
		List<QrtzCrawlerTable> list=new ArrayList<>();
		try {
			list = jobAndTriggerMapper.queryJob(job_Name);
		} catch (Exception e) {
			logger.info("【请求调度参数失败：】"+e);
			e.printStackTrace();
		}
		return list;	
	}
	
	
	public List<QrtzCrawlerTable>query_rawler_fill_catch(){
		logger.info("【获取所有爬虫失败信息】");
		List<QrtzCrawlerTable>qrtzList=new ArrayList<QrtzCrawlerTable>();
		List<QrtzCrawlerTable> list = jobAndTriggerMapper.query_rawler_fill_catch();
		List<CrawKeywordsInfo>list_failure=crawlerPublicClassMapper.failure_information_list();
		for(int i=0;i<list_failure.size();i++){
			for(int k=0;k<list.size();k++){
				if(list_failure.get(i).getPlatform_name().equals(list.get(k).getPlatform()) && list_failure.get(i).getCust_account_id().equals(list.get(k).getUser_Id()) ){
					QrtzCrawlerTable qrtz=list.get(k);
					qrtz.setNumber(list_failure.get(i).getNumber());
					qrtz.setData_time(list_failure.get(i).getBatch_time());
					qrtzList.add(qrtz);  
				} 
			}
		} 
		return qrtzList;	
	}
	
	public void InsertQrtzCrawl(QrtzCrawlerTable qrtz){
		jobAndTriggerMapper.InsertQrtzCrawl(qrtz);	
	}
	public void UpdateQrtzCrawl(String job_name,String status){
		jobAndTriggerMapper.UpdateQrtzCrawl(job_name,status);
	}
	public void DeleteQrtzCrawl(String job_name){
		jobAndTriggerMapper.DeleteQrtzCrawl(job_name);
	}
	public int updateIP(String JOB_NAME,String IP){
		return jobAndTriggerMapper.updateIP(JOB_NAME,IP);
	}
	public int update_Datasource(String JOB_NAME,String DATASOURCE){
		return jobAndTriggerMapper.update_Datasource(JOB_NAME,DATASOURCE);
	}
	public int Update_reschedule_job_thread(String JOB_NAME,String THREAD_SUM,String PLATFORM){
		return jobAndTriggerMapper.Update_reschedule_job_thread(JOB_NAME,THREAD_SUM,PLATFORM);
	}
	public int Update_reschedule_job_docker(String JOB_NAME,String DOCKER,String PLATFORM){
		return jobAndTriggerMapper.updateIP(JOB_NAME,DOCKER);
	}
	public int reschedule_job_inventory(String JOB_NAME,String INVENTORY,String PLATFORM){
		return jobAndTriggerMapper.reschedule_job_inventory(JOB_NAME,INVENTORY,PLATFORM);
	}
	public int 	reschedule_job_client (String JOB_NAME,String CLIENT,String PLATFORM){
		return jobAndTriggerMapper.reschedule_job_client(JOB_NAME,CLIENT,PLATFORM);
	}
	public int 	reschedule_job_storage (String JOB_NAME,String STORAGE,String PLATFORM){
		return jobAndTriggerMapper.reschedule_job_storage(JOB_NAME,STORAGE,PLATFORM);
	}
	public int 	reschedule_job_request_sum (String JOB_NAME,String SUM_REQUEST_NUM,String PLATFORM){
		return jobAndTriggerMapper.reschedule_job_request_sum(JOB_NAME,SUM_REQUEST_NUM,PLATFORM);
	}
	public int 	reschedule_job_database (String JOB_NAME,String database,String PLATFORM){
		return jobAndTriggerMapper.reschedule_job_database(JOB_NAME,database,PLATFORM);
	}
	public int 	reschedule_job_docker (String JOB_NAME,String DOCKER,String PLATFORM){
		return jobAndTriggerMapper.reschedule_job_docker(JOB_NAME,DOCKER,PLATFORM);
	}
	
	public int  reschedule_job_attribute(String JOB_NAME,String COMMODITY_ATTRIBUTE,String PLATFORM) {
		return jobAndTriggerMapper.reschedule_job_attribute(JOB_NAME,COMMODITY_ATTRIBUTE,PLATFORM);
	}
	
	public int 	reschedule_job_sums (String JOB_NAME,String SUMS,String PLATFORM){
		return jobAndTriggerMapper.reschedule_job_sums(JOB_NAME,SUMS,PLATFORM);
	}
	
	public int 	reschedule_job_redis (String JOB_NAME,String REDIS_TIME){
		return jobAndTriggerMapper.reschedule_job_redis(JOB_NAME,REDIS_TIME);
	}
	
	
}
