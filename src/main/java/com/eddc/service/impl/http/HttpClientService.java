package com.eddc.service.impl.http;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import net.sf.json.JSONObject;
import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.model.Parameter;
import com.eddc.util.ClientHttpDataUtil;
import com.eddc.util.Fields;
import com.eddc.util.HttpCrawlerUtil;
import com.eddc.util.LuminatiHttpClient;
import com.eddc.util.Validation;
import com.eddc.util.publicClass;
import com.eddc.util.http.HttpClientUtil;
import com.eddc.util.http.HttpClientUtils;
import com.eddc.util.http.builder.HCB;
import com.eddc.util.http.common.HttpConfig;
import com.eddc.util.http.common.HttpHeader;
import com.eddc.util.http.common.HttpMethods;
import com.eddc.util.okhttp.HttpRequest;
import com.eddc.util.okhttp.OkHttpClientUtil;
import com.github.pagehelper.util.StringUtil;
import org.apache.http.client.HttpClient;

@Service("httpClientService")
public class HttpClientService {
	@Autowired
	ClientHttpDataUtil clientHttpDataUtil;
	@Autowired
	HttpClientUtils httpClientUtils;
	private static Logger logger = LoggerFactory.getLogger(HttpClientService.class);

	public String interfaceSwitch(String count, Map<String, String> Map) {
		String StringMessage = "";
		try {
			if (count.equals(Fields.COUNT_01)) {
				//StringMessage=abcCloudCrawler(Map);//阿布云1
			} else if (count.equals(Fields.COUNT_2)) {
				//阿布云接口 代理云 2
				StringMessage = mixedInterfaceCrawler(Map);
			} else if (count.equals(Fields.COUNT_3)) {
				//luminati
				StringMessage = requestData(Map);
			} else if (count.equals(Fields.COUNT_4)) {
				//云代理
				StringMessage = GoodsProductDetailsData(Map);
			} else if (count.equals(Fields.COUNT_5)) {
				//云代理+luminati
				StringMessage = agentInterfaceCrawler(Map);
			} else if (count.equals(Fields.COUNT_6)) {
				//luminati+阿布云
				StringMessage = abyAgentInterfaceCrawlerLuminti(Map);
			} else {
				//抓取Itemyem
				StringMessage = mixedInterfaceCrawler(Map);
			}
			if (StringUtil.isEmpty(StringMessage) || StringMessage.contains(Fields.RETURNURL_FLAG)) {
				//云代理
				StringMessage = GoodsProductDetailsData(Map);
			}
		} catch (Exception e) {
			logger.info("【httpClient请求数据失败 error:{}】", e);
		}
		return StringMessage;
	}


	public String interfaceSwitch(Parameter parameter) {
		String StringMessage = "";
		try {
			if (parameter.getListjobName().get(0).getIP().equals(Fields.COUNT_4)) {
				//云代理
				StringMessage = goodsProductDetailsData(parameter);
			} else if (parameter.getListjobName().get(0).getIP().equals(Fields.COUNT_3)) {
				//StringMessage=requestData(parameter);//luminati
			} else if (parameter.getListjobName().get(0).getIP().equals(Fields.COUNT_5)) {
				//StringMessage=agentInterfaceCrawler(parameter);//云代理+luminati
			}
		} catch (Exception e) {
			logger.info("【httpClient请求数据失败 error:{}】", e);
		}
		return StringMessage;
	}

	@SuppressWarnings("deprecation")
	public String goodsProductDetailsData(Parameter parameter) throws Exception {
		int count = 0;
		String itemPage = "";
		int sum = parameter.getListjobName().get(0).getSum_request_num();

		while (Validation.isEmpty(itemPage) || itemPage.contains(Fields.PLEASE_LATER)
				|| itemPage.contains(Fields.FOUNT_404)
				|| itemPage.contains(Fields.FORM_J_FORM)
				|| itemPage.contains(Fields.RETURNURL)
				|| itemPage.contains(Fields.RETURNURL_FLAG)
				|| itemPage.contains(Fields.QUERYHTM)
				|| itemPage.contains(Fields.MSGTIP)
				|| itemPage.contains(Fields.ISCOASEOUT)
				|| itemPage.contains(Fields.TMALL_URL_LOGIN)
				|| itemPage.contains(Fields.SOLOD_OUT)
				|| itemPage.contains(Fields.RGV_FLAG)
				|| !itemPage.contains(parameter.getEgoodsId())) {
			count++;
			if (count > sum) {
				break;
			}
			try {
				JSONObject currPriceJson = portIP(parameter);
				try {
					itemPage = httpClientUtils.doGet(parameter, currPriceJson);

					if (itemPage.contains("多多果园")) {
						logger.info(parameter.getListjobName().get(0).getPlatform() + ":当前商品已经下架！！商品ID：" + parameter.getEgoodsId() + "，商品URL:" + parameter.getItemUrl_1());
						return itemPage;
					} else if (parameter.getItemUrl_1().contains("BatchGetShopInfoByVenderId") || parameter.getItemUrl_1().contains("shopScoreCallback")) {
						String shopId = parameter.getCrawKeywordsInfo().getPlatform_shopid();
						if (itemPage.contains(shopId)) {
							return itemPage;
						}
					}
					if (!parameter.getListjobName().get(0).getPlatform().equalsIgnoreCase("pdd")) {
						if (itemPage.contains("commodityLabelCountList") || itemPage.contains("dataList") || !itemPage.contains("没有找到符合的商品!")) {
							if (!Validation.isEmpty(itemPage)) {
								return itemPage;
							}
						}
					}
					if (parameter.getListjobName().get(0).getPlatform().equalsIgnoreCase("pdd")) {
						if (!itemPage.contains(parameter.getEgoodsId())) {
							currPriceJson = portIP(parameter);
							itemPage = httpClientUtils.getJsoupResult(parameter, currPriceJson);
							if (itemPage.contains(parameter.getEgoodsId())) {
								return itemPage;
							}
						}

					}
					if (StringUtils.isEmpty(itemPage) || itemPage.contains("非授权使用，当前访问IP地址") || !itemPage.contains(parameter.getEgoodsId())) {
						currPriceJson = portIP(parameter);
						HttpConfig config = HttpConfig.custom();
						// 设置header信息
						Header[] headers = headers(parameter);
						//采用默认方式（绕过证书验证）retry(5)
						HttpClient client = HCB.custom().timeout(30 * 1000).proxy(currPriceJson.getString("ip"), currPriceJson.getInt("port")).ssl().build();
						itemPage = HttpClientUtil.get(config.client(client).headers(headers).url(parameter.getItemUrl_1()).method(HttpMethods.GET));
					}
				} catch (Exception e) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					e.printStackTrace(new PrintStream(baos));
					String exception = baos.toString();	
					if(exception.contains("http://err.tmall.com/m-error1.html")) {
						return "当前商品已经下架,很抱歉，您查看的宝贝不存在，可能已下架或被转移";
					}else {
						itemPage = httpClientUtils.doGet(parameter, currPriceJson);	
					}
					
				}
				logger.info("【当前数据第：{}次请求数据， 平台：{} ,item页面url:{}】", count, parameter.getListjobName().get(0).getPlatform(), parameter.getItemUrl_1());

			} catch (Exception e) {
				logger.error("【请求" + parameter.getListjobName().get(0).getPlatform() + "平台数据失败 error:" + e.getMessage() + "\n url:" + parameter.getItemUrl_1() + "】");
			}
		}
		return itemPage;
	}


	public String goodPost(Parameter parameter, String param, Map<String, Object> params) throws Exception {
		int count = 0;
		String itemPage = "";
		while (Validation.isEmpty(itemPage) || !itemPage.contains(param)) {
			count++;
			if (count > 8) {
				break;
			}
			try {
				JSONObject currPriceJson = portIP(parameter);
				itemPage = httpClientUtils.doPost(parameter, currPriceJson, params);
				logger.info("【当前数据第：{}次请求数据， 平台：{} ,item页面url:{}】", count, parameter.getListjobName().get(0).getPlatform(), parameter.getItemUrl_1());
			} catch (Exception e) {
				logger.error("【请求" + parameter.getListjobName().get(0).getPlatform() + "平台数据失败 error:" + e.getMessage() + "\n url:" + parameter.getItemUrl_1() + "】");
			}
		}
		return itemPage;
	}

	public String getOkHttpClient(Parameter parameter ) {
		StringBuffer item=new StringBuffer();
		OkHttpClientUtil okhtp=new OkHttpClientUtil();
		String agency=Fields.CRAWLER_CLOUD_AGENT;
		if(parameter.getListjobName().get(0).getIP().equalsIgnoreCase("3")) {
			agency=Fields.CRAWLER_LUMINATI;
		}
		int count=0;
		Map<String, String> headerParas=httpClientUtils.header(parameter.getItemUrl_1(), parameter.getCoding(), parameter.getCookie(), parameter.getItemUtl_2(),parameter.getListjobName().get(0).getPlatform());
		while (Validation.isEmpty(item.toString())) {
			count++;
			if (count > 10) {
				break;
			}
			try {
				HttpRequest.Builder httpRequest = new HttpRequest.Builder()
						.setParameterType(Fields.CRAWLER_STRING).setIsProxy(parameter.isProxy()).account(true)
						.setHttpMethod(Fields.CRAWLER_GET).setProxyType(agency).setUrl(parameter.getItemUrl_1()).setHeader(headerParas);
				item.append(okhtp.getOkHttpClient(httpRequest.builder()));
				if(parameter.getHtmlKb()!=0) {
					if(item.toString().length()/1024>=parameter.getHtmlKb()) {
						return item.toString();
					}else {
						item=new StringBuffer();
					}
				}
			} catch (Exception e) {
				logger.info("==>>【请求数据失败 error:"+e+"<<==");
				e.printStackTrace();
			}
		}
		return item.toString();
	}


	/**
	 * 项目名称：Price_monitoring_crawler
	 * 类名称：HttpClientService
	 * 类描述： 阿布云,代理云 接口请求数据 2
	 * 创建人：jack.zhao
	 * 创建时间：2018年5月22日 上午10:19:27
	 * 修改人：jack.zhao
	 * 修改时间：2018年5月22日 上午10:19:27
	 * 修改备注：
	 *
	 * @throws InterruptedException
	 * @version
	 */
	public String mixedInterfaceCrawler(Map<String, String> map) throws InterruptedException {
		String itemPage = null;
		int count = 0;
		while (Validation.isEmpty(itemPage) || itemPage.contains(Fields.PLEASE_LATER) || itemPage.contains(Fields.FORM_J_FORM) || itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG) || itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT)) {
			count++;
			if (count > 10) {
				break;
			}
			Thread.sleep(2000);
			try {
				itemPage = GoodsProductDetailsData(map);
			} catch (Exception e) {
				logger.info("【请求" + map.get("platform") + ":数据失败" + e.getMessage() + "】");
			}
		}
		return itemPage;
	}

	/**
	 * 项目名称：Price_monitoring_crawler
	 * 类名称：HttpClientService
	 * 类描述： 代理云 接口请求数据 4
	 * 创建人：jack.zhao
	 * 创建时间：2018年5月22日 上午10:19:27
	 * 修改人：jack.zhao
	 * 修改时间：2018年5月22日 上午10:19:27
	 * 修改备注：
	 *
	 * @throws InterruptedException
	 * @version
	 */
	public String GoodsProductDetailsData(Map<String, String> Map) throws Exception {
		int count = 0;
		String itemPage = null;
		while (Validation.isEmpty(itemPage) || itemPage.contains(Fields.PLEASE_LATER) || itemPage.contains(Fields.FORM_J_FORM)
				|| itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG) ||
				itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) ||
				itemPage.contains(Fields.ISCOASEOUT) || itemPage.contains(Fields.TMALL_URL_LOGIN)) {
			Map<String, String> result = new HashMap<String, String>();
			count++;
			if (count > 5) {
				break;
			}
			try {
				result = getResultMap(Map);//请求获取数据
				if (StringUtils.isNotEmpty(result.get("result"))) {
					itemPage = result.get("result").toString();
				}
				Map.put("status", "2");
				Map.put("messageLog", "请求数据" + Fields.SUCCESS);
			} catch (ConnectTimeoutException e) {
				Map.put("status", "0");
				Map.put("messageLog", Map.get("platform") + Fields.JD_TIMEOUT + e.toString() + e.getMessage());
				logger.error("【请求" + Map.get("platform") + "数据失败 error:{} 】", e);
			} catch (Exception e) {
				Map.put("status", "0");
				Map.put("messageLog", Map.get("platform") + Fields.JD_DATA + e.toString() + e.getMessage());
				logger.error("【请求" + Map.get("platform") + "数据失败 error:{}】", e);
			}
			logger.info("【获取当前" + Map.get("platform") + "商品==={" + Map.get("egoodsId") + "}===的item页面次数为:>>> {" + count + "}" + "商品url:" + Map.get("url") + "】");
			Map.put("messageLog", "");

		}
		return itemPage;
	}

	/**
	 * 项目名称：Price_monitoring_crawler
	 * 类名称：HttpClientService
	 * 类描述： 代理云 +luminati:5
	 * 创建人：jack.zhao
	 * 创建时间：2018年5月22日 上午10:19:27
	 * 修改人：jack.zhao
	 * 修改时间：2018年5月22日 上午10:19:27
	 * 修改备注：
	 *
	 * @throws InterruptedException
	 * @version
	 */
	public String agentInterfaceCrawler(Map<String, String> map) throws InterruptedException {
		int count = 0;
		String itemPage = null;
		while (Validation.isEmpty(itemPage) || itemPage.contains(Fields.PLEASE_LATER) || itemPage.contains(Fields.FORM_J_FORM) || itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG) || itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT)) {
			count++;
			Thread.sleep(2000);
			if (count > 10) {
				break;
			}
			try {
				itemPage = GoodsProductDetailsData(map);
				if (Validation.isEmpty(itemPage)) {
					Map<String, String> mp = new LuminatiHttpClient(null).request(map.get("url"), map.get("coding"), map.get("cookie"), map.get("urlRef"));
					itemPage = mp.get("result").toString();
				}
			} catch (Exception e) {
				logger.info("【请求" + map.get("platform") + "数据失败" + e.getMessage() + "】");
			}
		}

		return itemPage;

	}

	/**
	 * 项目名称：Price_monitoring_crawler
	 * 类名称：HttpClientService
	 * 类描述： 阿布云 +luminati:6
	 * 创建人：jack.zhao
	 * 创建时间：2018年5月22日 上午10:19:27
	 * 修改人：jack.zhao
	 * 修改时间：2018年5月22日 上午10:19:27
	 * 修改备注：
	 *
	 * @throws Exception
	 * @version
	 */
	public String abyAgentInterfaceCrawlerLuminti(Map<String, String> map) throws Exception {
		int count = 0;
		String itemPage = null;
		while (Validation.isEmpty(itemPage) || itemPage.contains(Fields.PLEASE_LATER) || itemPage.contains(Fields.FORM_J_FORM) || itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG) || itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT)) {
			count++;

			if (count > 10) {
				break;
			}
			try {
				Thread.sleep(2000);

				Map<String, String> mp = new LuminatiHttpClient(null).request(map.get("url"), map.get("coding"), map.get("cookie"), map.get("urlRef"));
				itemPage = mp.get("result").toString();

			} catch (Exception e) {
				itemPage = GoodsProductDetailsData(map);
				logger.info("【请求" + map.get("platform") + "数据失败" + e.getMessage() + "】");
			}
		}

		return itemPage;

	}

	public String getJsoupResultMessage(Map<String, String> Map) throws Exception {
		int count = 0;
		String itemPage = null;
		while (Validation.isEmpty(itemPage) || itemPage.contains(Fields.PLEASE_LATER) || itemPage.contains(Fields.FORM_J_FORM) || itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG) || itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT)) {

			count++;
			if (count > 10) {
				break;
			}
			try {
				Thread.sleep(2000);
				itemPage = clientHttpDataUtil.getJsoupResultMessage(Map);//开始请求数据
				Map.put("status", "2");
				Map.put("messageLog", "请求数据" + Fields.SUCCESS);
			} catch (Exception e) {
				Map.put("status", "0");
				Map.put("messageLog", Map.get("platform") + Fields.JD_DATA + e.toString() + e.getMessage());
				logger.error("【请求" + Map.get("platform") + "数据失败 error:{}】", e);
			}
			logger.info("【获取当前" + Map.get("platform") + "商品==={" + Map.get("egoodsId") + "}===的item页面次数为:>>> {" + count + "}" + "商品url:" + Map.get("url") + "】");
			//kafkaProducerLog(Map);
			Map.put("messageLog", "");

		}
		return itemPage;
	}

	/**
	 * 项目名称：Price_monitoring_crawler
	 * 类名称：HttpClientService
	 * 类描述： luminati3
	 * 创建人：jack.zhao
	 * 创建时间：2018年5月22日 上午10:19:27
	 * 修改人：jack.zhao
	 * 修改时间：2018年5月22日 上午10:19:27
	 * 修改备注：
	 *
	 * @throws InterruptedException
	 * @version
	 */

	public String requestData(Map<String, String> map) throws Exception {
		int count = 0;
		String itemPage = null;
		while (Validation.isEmpty(itemPage) || itemPage.contains(Fields.PLEASE_LATER) || itemPage.contains(Fields.FORM_J_FORM) || itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG) || itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT)) {
			count++;
			Thread.sleep(2000);
			if (count > 8) {
				break;
			}
			try {
				Map<String, String> mp = new LuminatiHttpClient(null).request(map.get("url"), map.get("coding"), map.get("cookie"), map.get("urlRef"));
				if (Validation.isEmpty(mp)) {
					itemPage = GoodsProductDetailsData(map);
				} else {
					itemPage = mp.get("result").toString();
				}
				map.put("status", "2");
				map.put("messageLog", map.get("platform") + logMessage(map.get("status")) + map.get("messageLog") + Fields.SUCCESS);
			} catch (ConnectTimeoutException e) {
				if (Validation.isEmpty(itemPage)) {
					itemPage = GoodsProductDetailsData(map);
				}
				map.put("status", "0");
				map.put("count", "0");
				map.put("messageLog", map.get("platform") + logMessage(map.get("status")) + e.toString());
				logger.error("【" + map.get("platform") + map.get("messageLog") + logMessage(map.get("status")) + e.toString() + "】");
			} catch (Exception e) {
				map.put("count", "1");
				map.put("status", "0");
				map.put("messageLog", map.get("platform") + map.get("messageLog") + logMessage(map.get("status")) + e.toString() + "】");
				logger.error("【" + map.get("platform") + map.get("messageLog") + logMessage(map.get("status")) + e.toString() + "】");
			}
			map.put("messageLog", "");
		}
		return itemPage;

	}

	/***
	 * 普通接口请求 数据
	 * @throws Exception
	 */
	public String GoodsProduct(Map<String, String> map)  {
		String itemPage = null;//Map<String,String>postMap
		int count = 0;
		map.put("messageLog", Fields.DATAILS);
		while (Validation.isEmpty(itemPage) || itemPage.contains(Fields.PLEASE_LATER) || itemPage.contains(Fields.FORM_J_FORM) || itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG) || itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT)) {
			count++;
			if (count > 10) {
				break;
			}
			try {
				Thread.sleep(2000);

				itemPage = GoodsProductDetailsData(map);

				if (Validation.isEmpty(itemPage) || itemPage.contains(Fields.RETURNURL_FLAG) || itemPage.contains(Fields.FORM_J_FORM) || itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT)) {
					itemPage = GoodsProductDetailsData(map);
				}
				if (Validation.isEmpty(itemPage) || itemPage.contains(Fields.RETURNURL_FLAG) || itemPage.contains(Fields.FORM_J_FORM) || itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT)) {
					itemPage = GoodsProductDetailsData(map);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		logger.info("【获取当前" + map.get("platform") + "商品==={" + map.get("egoodsId") + "}===的item页面次数为:>>> {" + count + "} 商品url:" + map.get("url") + "】");
		return itemPage;
	}

	/***
	 * 普通接口Item页面请求 数据
	 * @throws Exception
	 */
	public String GoodsProductItem(Map<String, String> map) throws Exception {
		String itemPage = null;
		int count = 0;
		map.put("messageLog", Fields.DATAILS);
		while (Validation.isEmpty(itemPage) || itemPage.contains(Fields.PLEASE_LATER) || itemPage.contains(Fields.RETURNURL_FLAG) || itemPage.contains(Fields.FORM_J_FORM) || itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT) || itemPage.contains(Fields.VIEWPORT)) {
			count++;
			if (count > 8) {
				break;
			}
			itemPage = GoodsProductDetailsData(map);
		}
		logger.info("【获取当前" + map.get("platform") + "商品==={" + map.get("egoodsId") + "}===的item页面次数为:>>> {" + count + "}+】");
		return itemPage;
	}

	public String ProductItem(Map<String, String> map) throws Exception {
		String itemPage = null;
		int count = 0;
		map.put("messageLog", Fields.DATAILS);
		while (Validation.isEmpty(itemPage)) {
			count++;
			if (count > 8) {
				break;
			}
			try {
				Thread.sleep(3000);
				itemPage = GoodsProductDetailsData(map);

			} catch (Exception e) {
				itemPage = GoodsProductDetailsData(map);
			}
		}
		logger.info("【获取当前" + map.get("platform") + "商品==={" + map.get("egoodsId") + "}===的item页面次数为:>>> {" + count + "} 】");
		return itemPage;
	}

	public String ProductItemJd(Map<String, String> map) throws Exception {
		String itemPage = null;
		int count = 0;
		map.put("messageLog", Fields.DATAILS);
		while (Validation.isEmpty(itemPage)) {
			map.put("coding", "utf-8");
			count++;
			if (count > 6) {
				break;
			}
			try {
				Thread.sleep(3000);
				itemPage = GoodsProductDetailsData(map);

			} catch (Exception e) {
				logger.info("【请求数据失败 error:{}】", e);
			}
		}
		logger.info("【获取当前" + map.get("platform") + "商品==={" + map.get("egoodsId") + "}===的item页面次数为:>>> {" + count + "}】");
		return itemPage;
	}


	public Map<String, String> getResultMap(Map<String, String> param) throws Exception {
		Map<String, String> result = new HashMap<String, String>();
		List<String> list = new ArrayList<String>();
		Thread.sleep(1000);
		String count = "0";
		try {
			String path = ResourceBundle.getBundle("parameter").getString("IP_ADDRES");
			String ipData = getJsonObj(path, null);
			JSONObject currPriceJson = JSONObject.fromObject(ipData);
			param.put("ip", currPriceJson.getString("ip"));
			param.put("port", currPriceJson.getString("port"));
		} catch (Exception e) {
			logger.error("【IP请求失败 error:{}】", e);
		}
		if (list.size() > 0) {
			count = "2";
		}
		try {
			String item = httpClientUtils.doGet(param);
			if (StringUtils.isEmpty(item)) {
				if (publicClass.Ipaddres().equalsIgnoreCase(Fields.IP)) {
					param.put("ip", Fields.IP_HOST_LOCAL);
				} else {
					param.put("ip", Fields.IP_HOST);
				}
				param.put("port", String.valueOf(Fields.IP_PORT));
				result = clientHttpDataUtil.getMap(param);//开始请求数据

			} else {
				result.put("result", item);
			}
			result.put("status", count);
			result.putAll(param);
			return result;
		} catch (Exception ex) {
			return result;
		}
	}

	public String getOkHttpClient( Map<String, Object> paramData,Map<String,String> param,Parameter parameter ) {
		StringBuffer item=new StringBuffer();
		OkHttpClientUtil okhtp=new OkHttpClientUtil();
		String agency=Fields.CRAWLER_CLOUD_AGENT;
		if(parameter.getListjobName().get(0).getIP().equalsIgnoreCase("3")) {
			agency=Fields.CRAWLER_LUMINATI;
		}
		int count=0;
		while (Validation.isEmpty(item.toString())) {
			count++;
			if (count > 10) {
				break;
			}

			try {
				HttpRequest.Builder httpRequest = new HttpRequest.Builder().setParam(paramData)
						.setParameterType(Fields.CRAWLER_JSON).setIsProxy(true).account(true)
						.setHttpMethod(Fields.CRAWLER_POST).setProxyType(agency).setUrl(parameter.getItemUrl_1()).setHeader(param);
				item.append(okhtp.getOkHttpClient(httpRequest.builder()));
			} catch (Exception e) {
				logger.info("==>>【请求数据失败 error:"+e+"<<==");
				e.printStackTrace();
			}
		}
		return item.toString();
	}



	public Header[] headers(Parameter parameter) {
		Header[] headers = HttpHeader.custom()
				.userAgent(HttpCrawlerUtil.CrawlerAttribute())
				.host(host(parameter.getItemUrl_1()))
				.referer(parameter.getItemUtl_2())
				.cookie(parameter.getCookie())
				.acceptCharset(parameter.getCoding())
				.acceptEncoding(parameter.getCoding())
				.from(parameter.getItemUrl_1())
				.other(HttpHeader.HttpReqHead.PRAGMA, "no-cache")
				.other(HttpHeader.HttpReqHead.ACCEPT, "*/*")
				.other(HttpHeader.HttpReqHead.CONNECTION, "*keep-alive*")
				.other(HttpHeader.HttpReqHead.CACHE_CONTROL, "*max-age=0*")
				.other(HttpHeader.HttpReqHead.CACHE_CONTROL, "no-cache")
				.other(HttpHeader.HttpReqHead.UPGRADE_INSECURE_REQUESTS, "1")
				.other(HttpHeader.HttpReqHead.X_REQUESTD_ITH, "XMLHttpRequest")
				.other(HttpHeader.HttpReqHead.ACCEPT_ENCODING, "gzip, deflate, sdch, br")
				.other(HttpHeader.HttpReqHead.ACCEPT_LANGUAGE, "*zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3*")
				.other(HttpHeader.HttpReqHead.CONTENT_TYPE, "application/x-javascript;charset=" + parameter.getCoding())
				.build();
		return headers;

	}

	public JSONObject portIP(Parameter parameter) {
		String path = ResourceBundle.getBundle("parameter").getString("IP_ADDRES");
		String ipData = getJsonObj(path, parameter);
		JSONObject currPriceJson = JSONObject.fromObject(ipData);
		return currPriceJson;

	}

	public static String host(String url) {
		String HOST = null;
		String spitString[] = url.split("/");
		if (!Validation.isEmpty(url)) {
			HOST = spitString[2];
		}
		return HOST;
	}

	/**
	 * @param @param  src
	 * @param @param  code
	 * @param @return 设定文件
	 * @return String    返回类型
	 * @throws
	 * @Title: getJsonObj
	 * @Description: TODO 请求数据
	 */
	public String getJsonObj(String url, Parameter parameter) {
		String message = "";
		int count = 0;
		while (Validation.isEmpty(message)) {
			count++;
			if (count > 10) {
				break;
			}
			try {
				if (parameter == null) {
					message = httpClientUtils.getIpResult(url);
				} else {
					message = httpClientUtils.doGetIP(url, parameter);
				}
			} catch (Exception e) {
				logger.error("【请求代理IP失败 error:{}】", e);
			}

		}
		return message;

	}

	public String logMessage(String log) {
		String message = "";
		if (log.equals("0")) {
			message = Fields.JD_TIMEOUT;
		} else if (log.equals("1")) {
			message = Fields.JD_DATA;
		} else {
			message = Fields.SUCCESS;
		}
		return message;
	}

	public Map<String, String> jumeiMap() {
		Map<String, String> jumeiMap = new HashMap<String, String>();
		jumeiMap.put("cookie", "");
		jumeiMap.put("coding", "UTF-8");
		jumeiMap.put("urlRef", "item.jumei.com");
		jumeiMap.put("host", "item.jumei.com");
		jumeiMap.put("platform", Fields.PLATFORM_JUMEI);
		jumeiMap.put("ip", "-");
		return jumeiMap;
	}

	public Map<String, String> KaolaMap() {
		Map<String, String> Map = new HashMap<String, String>();
		Map.put("coding", "UTF-8");
		Map.put("urlRef", "");
		Map.put("cookie", "");
		Map.put("ip", "-");
		Map.put("platform", Fields.PLATFORM_KAOLA);
		return Map;
	}

	public Map<String, String> lazadaMap() {
		Map<String, String> Map = new HashMap<String, String>();
		Map.put("cookie", "");
		Map.put("coding", "UTF-8");
		Map.put("urlRef", "www.lazada.com.my");
		Map.put("platform", Fields.PLATFORM_LAZADA);
		Map.put("ip", "-");
		return Map;
	}

	public Map<String, String> sillaMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("cookie", "JSESSIONID=jtlZ8ll0i37iq2JRa0O8myA27rOUsULZaEWYiPjKj30WkARp3Nwv!-1089477281; ");
		map.put("coding", "UTF-8");
		map.put("urlRef", "www.shilladfs.com");
		map.put("platform", Fields.PLATFORM_SILLA);
		map.put("ip", "-");
		return map;
	}

	public Map<String, String> suningMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("cookie", "cityId=9264; districtId=12113; SN_CITY=20_021_1000267_9264_01_12113_3_0; _snstyxuid=654260626164CBLF; _snsr=baidu%7Cbrand%7C%7Clogo%7C%25E8%258B%258F%25E5%25AE%2581*%3A*; authId=si590FAA6D8F49F4D1F97F6001806593BA; secureToken=8BC76544D7102B852B6C28DC2DA84DF9; _snms=150270379321633744; smhst=101923925|0000000000a101923918|0000000000a101822315|0000000000a625019371|0000000000a102668611|0000000000; _snma=1%7C150086118535495580%7C1500861185354%7C1502703813564%7C1502703822326%7C31%7C3; _snmc=1; _snmp=15027038222755593; _snmb=150270377280922319%7C1502703822438%7C1502703822330%7C8; _ga=GA1.2.721456464.1500861186; _gid=GA1.2.1016444502.1502703773");
		map.put("coding", "UTF-8");
		map.put("urlRef", null);
		map.put("platform", Fields.PLATFORM_SUNING);
		map.put("ip", "-");
		return map;
	}

	public Map<String, String> tmallMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("cookie", null);
		map.put("coding", "UTF-8");
		map.put("urlRef", "acs.m.taobao.com");
		map.put("platform", Fields.PLATFORM_TMALL_EN);
		map.put("ip", "-");
		return map;
	}

	public Map<String, String> vipMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("cookie", null);
		map.put("coding", "UTF-8");
		map.put("urlRef", null);
		map.put("platform", Fields.PLATFORM_VIP);
		map.put("ip", "-");
		return map;
	}

	public Map<String, String> yhdMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("cookie", "__jdv=259140492|baidu-pinzhuan|t_288551095_baidupinzhuan|cpc|yhdbaidupcpz007_0_3bb96dad87b9484281e519ff3d85487c|1508120819221; cart_cookie_uuid=587fdf90-85c3-47c6-9c68-afc36011d322; yhd_location=2_2817_51973_0; provinceId=2; cityId=2817; mba_muid=15081208192201111174638; test=1; cart_num=0; __jda=81617359.15081208192201111174638.1508120819.1508134896.1508137618.4; __jdb=81617359.1.15081208192201111174638|4.1508137618; __jdc=81617359");
		map.put("coding", "UTF-8");
		map.put("urlRef", "www.yhd.com");
		map.put("platform", Fields.PLATFORM_YHD);
		map.put("ip", "-");
		return map;
	}

	public Map<String, String> jdMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("cookie", "");
		map.put("coding", "utf-8");
		map.put("urlRef", "www.jd.com");
		map.put("host", "www.jd.com");
		map.put("platform", Fields.PLATFORM_JD);
		map.put("ip", "-");
		return map;
	}

}
