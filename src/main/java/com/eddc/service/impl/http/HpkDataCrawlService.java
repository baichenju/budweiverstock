package com.eddc.service.impl.http;
import com.eddc.model.Craw_goods_Info;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Parameter;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.crawl.SearchDataCrawlService;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.ChaoJiYing;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.WhetherShelves;
import com.google.common.collect.Lists;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.imageio.ImageIO;

@Service
@Slf4j
public class HpkDataCrawlService extends WhetherShelves {
	private static Logger logger = LoggerFactory.getLogger(HpkDataCrawlService.class);
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private SearchDataCrawlService search_DataCrawlService;

	/**
	 * 请求商品"
	 *
	 * @throws InterruptedException
	 */
	public Map<String, Object> parameter(Parameter parameter) throws InterruptedException {
		Map<String, Object> item = new HashMap<String, Object>(10);
		logger.info("==>>海拍客台开始解析数据,当前解析的是第：{} 个商品<<==", parameter.getSum());
		StringBuffer str = new StringBuffer();
		parameter.setItemUrl_1(Fields.HAIPAIKE_IMAGE_URL.replace("EDOODSID", parameter.getEgoodsId()));
		parameter.setItemUtl_2(parameter.getItemUrl_1());
		str.append(parameter.getItemUrl_1());
		Thread.sleep(3000);
		/****启动webDriver**/
		WebDriver driver = (WebDriver) parameter.getMap().get(Fields.CRAWLER_DRIVER);
		String productdesc = parameter.getMap().get("productdesc").toString();

		if (productdesc.contains("美素")) {
			driver.get("https://mall.hipac.cn/ytms/page/nfthhq.html?rp=2.4.131.21.7.kfkvqh571");
			Thread.sleep(3000);
			String StringMessage = driver.getPageSource();
			Document document = Jsoup.parse(StringMessage);
			//清空集合
			str.delete(0, str.length());
			List<Element> dateMessage = document.getElementsByClass("yt-hotitem-wrap sp-content");
			for (int i = 0; i < dateMessage.size(); i++) {

				String brandItem = dateMessage.get(i).getElementsByClass("hot-item-title").text();
				if (brandItem.contains("美素佳儿")) {
					//
					List<Element> itemList = dateMessage.get(i).getElementsByClass("hot-item-list");
					if (itemList != null && itemList.size() > 0) {
						List<Element> oddList = itemList.get(0).getElementsByClass("item-span-top line-odd");
						List<Element> listEven = itemList.get(0).getElementsByClass("item-span-top line-even");
						str.append(itemId(oddList));
						if(listEven!=null && listEven.size()>0) {
							str.append(itemId(listEven));
						}

					}
					break;
				}	
			}
		}
		//
		str.delete(0, str.length());
		str.append("https://detail.hipac.cn/item.html?itemId=79951");
		String[] itemUrl =str.toString().split("&&");
		for(int ii=0;ii<itemUrl.length;ii ++) {
			try {
				Thread.sleep(3000);
				String productId=itemUrl[ii].split("itemId=")[1];
				parameter.setItemUrl_1(itemUrl[ii]);
				parameter.setEgoodsId(productId);
				parameter.setGoodsId(StringHelper.encryptByString(productId + parameter.getListjobName().get(0).getPlatform()));
				XxlJobLogger.log("当前商品请求的连接是："+itemUrl[ii]);
				driver.get(itemUrl[ii]);
				Thread.sleep(5000);
				item = itemAppPage(parameter, driver);
			} catch (Exception e) {
				logger.error("海拍客台开始解析数据,当前解析的是第：{} 个商品 请求数据异常： error:{}<<==", parameter.getSum(), e.getMessage());
				e.printStackTrace();
			}
		}
		return item;
	}
	public String itemId(List<Element> list) {
		StringBuffer str=new StringBuffer();
		for(int k=0;k<list.size();k++) {
			String productId=list.get(k).tagName("td").getElementsByTag("a").attr("href");
			str.append("https:"+productId+"&&");
		}
		return str.toString();
	}

	/**
	 * @throws Exception 
	 * @描述 数据解析
	 * @参数 itemPage 页面详情  parameter 封装参数  driver 浏览器请求页面
	 * @返回值
	 * @创建人 jack.zhao
	 * @创建时间 2020/1/16
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> itemAppPage(Parameter parameter, WebDriver driver) throws Exception {
		Map<String, Object> item = new HashMap<String, Object>(10);
		Document document = Jsoup.parse(driver.getPageSource());

		Craw_goods_InfoVO info = new Craw_goods_InfoVO();
		Craw_goods_Price_Info price = new Craw_goods_Price_Info();

		Map<String, Object> insertItem = new HashMap<String, Object>(5);
		List<Map<String, Object>> insertItems = Lists.newArrayList();

		Map<String, Object> insertPriceMap = new HashMap<String, Object>(5);
		List<Map<String, Object>> insertPriceList = Lists.newArrayList();
		Thread.sleep(3000);
		String crawlerStore=parameter.getMap().get("crawlerStore").toString();
		String platformName=document.getElementsByClass("detail-name").text();

		//FileUtils.writeStringToFile(new File("D://aa.html"),driver.getPageSource().toString(),"UTF-8");
		if(StringUtils.isEmpty(platformName)){
			platformName=document.getElementsByClass("name-wrap").text();
		}
		logger.info("==>>platform:"+platformName+"<<==");
		try {
			if (StringUtils.isNotEmpty(platformName)) {

				info.setFeature1(Fields.COUNT_01);
				info.setEgoodsId(parameter.getEgoodsId());
				info.setGoodsId(parameter.getGoodsId());
				info.setBatch_time(parameter.getBatch_time());
				info.setGoods_status(Fields.STATUS_COUNT_1);
				info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
				info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
				info.setCust_keyword_id(Integer.valueOf(parameter.getKeywordId()));
				info.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());
				info.setPlatform_goods_name(platformName);
				info.setInventory(document.getElementsByClass("ml-10").first().text());
				info.setGoods_pic_url("http:"+document.getElementsByClass("d-ib  va-m").attr("src"));
				info.setGoods_url(parameter.getItemUrl_1());
				price.setGoodsid(parameter.getGoodsId());
				price.setChannel(Fields.CLIENT_PC);
				price.setBatch_time(parameter.getBatch_time());
				price.setCust_keyword_id(parameter.getKeywordId());
				price.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
				price.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
				price.setPlatform_name_en(parameter.getListjobName().get(0).getPlatform());


				if (document.getElementsByClass("btn buy-btn buy-disable mr-10").text().contains("已抢完")) {
					log.info("==>>商品已经下架<<==");
					info.setGoods_status(Fields.STATUS_COUNT);
				} else {
//					//价格选择
//					if(parameter.getListjobName().get(0).getUser_Id().equalsIgnoreCase("108")) {
//						List<WebElement> priceSelect =driver.findElements(By.xpath("//*[@id='priceCheck']/div[2]/div[1]/span"));
//						if(priceSelect!=null && priceSelect.size()>0) {
//							priceSelect.get(0).click();
//						}
//					}

					List<WebElement> priceSelect =driver.findElements(By.xpath("//*[@id='priceCheck']//span"));
					if (priceSelect.size()>0){
						for (int i = 0; i < priceSelect.size(); i++) {
							priceSelect.get(i).click();
							Thread.sleep(2000);
							if (i==1){
								price.setDelivery_place("促销价");

							}else{
								price.setDelivery_place("常规价");
							}
							parsePricePage(driver,info,price,crawlerStore,parameter,insertPriceList,insertPriceMap);
						}

					}else{
							price.setDelivery_place("常规价");
							parsePricePage(driver,info,price,crawlerStore,parameter,insertPriceList,insertPriceMap);
					}


				}
			} else {

				logger.info("==>>platformName:"+platformName+",url:"+Fields.HAIPAIKE_IMAGE_URL.replace("EDOODSID", parameter.getEgoodsId())+"<<==");
				logger.info("==>>海拍客当前请求数据已经检测出来，恶意请求数据！！！商品名称:" + parameter.getListjobName().get(0).getJob_name() + ",  商品Id：" + parameter.getEgoodsId() + " , 商品链接: " + parameter.getItemUrl_1() + "<<==");
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("==>>解析海拍客数据发生异常：{}<<==", e);
		}

		/*数据插入数据库*/
		if(StringUtils.isNotEmpty(platformName)){
			insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
			insertItems.add(insertItem);
			search_DataCrawlService.insertIntoData(insertItems, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_INFO);
			logger.info("==>>商品详情插入数据库成功！。。。。。。。<<=="+platformName);
			if(info.getGoods_status()==Fields.STATUS_COUNT_1){
				search_DataCrawlService.insertIntoData(insertPriceList, parameter.getListjobName().get(0).getDatabases(), Fields.TABLE_CRAW_GOODS_PRICE_INFO);
				insertPriceList= Lists.newArrayList();
				logger.info("==>>商品价格插入数据库成功！。。。。。。。<<=="+platformName + ",商品单价" + price.getCurrent_price());
			}else{
				logger.info("==>>此商品已经下架："+platformName+",商品链接: " + parameter.getItemUrl_1()+"<<==");	
			}

		}			
		item.put("Craw_goods_Info", info);
		item.put("Craw_goods_Price_Info", price);
		return item;
	}

	/****根据价格标签解析页面****/
	public void parsePricePage(WebDriver driver, Craw_goods_InfoVO info,Craw_goods_Price_Info price,String crawlerStore,Parameter parameter,
							   List<Map<String, Object>> insertPriceList,
		Map<String, Object> insertPriceMap){
		try {
			/* 解析规格配送方式 日期 获取价格*/
			List<WebElement> itemData = driver.findElements(By.xpath(".//div[@class='ml-20 cor-3']"));
			if (itemData != null && itemData.size() > 0) {
				WebElement expressage = null;
				/*配送方式*/
				if (itemData.size() >= 3) {
					expressage = itemData.get(2);
				}
				/*如果没有日期直接获取商品价格*/
				if (itemData.size() == 1) {
					try {
						/****商品价格*/
						String priceData = getProductPrice(driver);
						WebElement selectedItem = driver.findElement(By.xpath("//*[@id='root']/div/div[2]/div[2]/div[2]/div[8]/div[1]"));
						List<WebElement> list = selectedItem.findElements(By.xpath(".//span[@class='selected-item']"));
						if (list.size() == 0) {
							try {
								selectedItem = driver.findElement(By.xpath("//*[@id='root']/div/div[2]/div[2]/div[2]/div[10]/div[1]/div"));
							} catch (Exception e) {
								//selectedItem = driver.findElement(By.xpath("//*[@id='root']/div/div[2]/div[2]/div[2]/div[11]/div[1]"));
							}
							list = selectedItem.findElements(By.xpath(".//span[@class='selected-item']"));
						}
						if (list.size() == 0) {
							String productDay = driver.getPageSource();
							Document doc = Jsoup.parseBodyFragment(productDay);
							List<Element> selectedList = doc.getElementsByClass("selected-item");
							StringBuilder offer = new StringBuilder();
							for (int i = 0; i < selectedList.size(); i++) {
								offer.append(selectedList.get(i).text() + ",");
								if (i == 0) {
									price.setPromotion(selectedList.get(i).text());
								}
							}
							price.setSku_name(offer.toString());
						} else {
							price.setCurrent_price(priceData);
							price.setOriginal_price(priceData);
							price.setPromotion(list.get(0).getText());
							String url = driver.getCurrentUrl();

							if (StringUtils.isNotEmpty(url)) {
								if (url.contains("supplierNo=")) {
									price.setSKUid(url.split("supplierNo=")[1]);
								}

							}

							if (list.size() >= 2) {
								price.setSku_name(list.get(0).getText() + "," + list.get(1).getText() + ",商品单价:" + price.getCurrent_price());
							} else {
								price.setSku_name(list.get(0).getText() + ",商品单价:" + price.getCurrent_price());
							}
						}
						try {
							/****判断商品库存是否存在****/
							Document doc = Jsoup.parseBodyFragment(driver.getPageSource());
							Element ele = doc.getElementById("stockBox");
							if (ele != null) {
								price.setInventory(doc.getElementById("stockBox").getElementsByTag("span").text());
							}
							//price.setInventory(driver.findElement(By.xpath("//*[@id='stockBox']/span")).getText().toString());

							/****判断是否抓取商品营业执照****/
							if (crawlerStore.equalsIgnoreCase("true")) {
								if (StringUtils.isNoneBlank(price.getSKUid())) {
									String promotin = getProductLicense(driver, crawlerStore, price.getSKUid());
									price.setHead_promotion(promotin);
								}

							}
						} catch (Exception e) {
							e.printStackTrace();
							log.info("店铺链接不存在。。。" + price.getSKUid());
						}
						/*封装价格*/
						insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(price);
						insertPriceList.add(insertPriceMap);
						//}
					} catch (Exception e2) {
						try {
							/*获取商品规格*/
							WebElement spe = itemData.get(0);
							List<WebElement> speMessage = spe.findElements(By.xpath(".//div[@tabindex='0']"));
							log.info("==>>当前商品规格一共有：" + speMessage.size() + " 个 ， 开始循环获取商品<<==");
							for (int k = 0; k < speMessage.size(); k++) {

								String isValid = speMessage.get(k).getAttribute("class");

								if (!isValid.contains("features-item-dash")) {

									log.info("==>>当前点击的规格是：" + speMessage.get(k).getText() + "<<==");

									speMessage.get(k).click();
									Thread.sleep(2000);

									/****商品价格*/
									String priceData = getProductPrice(driver);
									price.setCurrent_price(priceData);
									price.setOriginal_price(priceData);

									String url = driver.getCurrentUrl();


									if (StringUtils.isNotEmpty(url)) {
										if (url.contains("supplierNo=")) {
											price.setSKUid(url.split("supplierNo=")[1]);
										}

									}
									try {
										//获取主页面1句柄

										/****判断商品库存是否存在****/
										Document doc = Jsoup.parseBodyFragment(driver.getPageSource());
										Element ele = doc.getElementById("stockBox");
										if (ele != null) {
											price.setInventory(doc.getElementById("stockBox").getElementsByTag("span").text());
										}

										//price.setInventory(driver.findElement(By.xpath("//*[@id='stockBox']/span")).getText().toString());

										/****判断是否抓取商品营业执照****/
										if (crawlerStore.equalsIgnoreCase("true")) {
											if (StringUtils.isNoneBlank(price.getSKUid())) {
												String promotin = getProductLicense(driver, crawlerStore, price.getSKUid());
												price.setHead_promotion(promotin);
											}

										}
									} catch (Exception e) {
										e.printStackTrace();
										log.info("店铺链接不存在。。。" + price.getSKUid());
									}

									/*商品有效期*/
									price.setPromotion("无日期");
									price.setSku_name(speMessage.get(k).getText() + ",商品单价：" + price.getCurrent_price());
									/*封装价格*/
									insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(price);
									insertPriceList.add(insertPriceMap);
								}

							}
						} catch (Exception ee) {
							ee.printStackTrace();
							log.error("当前组合商品解析发生异常：" + ee);
						}

					}
				} else {
					/*有效期*/
					WebElement date = itemData.get(0);
					/*规格*/
					WebElement spe = itemData.get(1);
					/*循环日期 tabindex="0"*/
					List<WebElement> dateMessage = date.findElements(By.xpath(".//div[@tabindex=\"0\"]"));
					log.info("==>>当前商品一共有：" + dateMessage.size() + " 个日期 ， 开始循环获取商品日期<<==");
					for (int i = 0; i < dateMessage.size(); i++) {

						log.info("==>>当前点击的日期是：" + dateMessage.get(i).getText() + "<<==");

						dateMessage.get(i).click();
						Thread.sleep(2000);

						/*获取商品规格*/
						List<WebElement> speMessage = spe.findElements(By.xpath(".//div[@tabindex='0']"));
						log.info("==>>当前商品规格一共有：" + speMessage.size() + " 个 ， 开始循环获取商品<<==");
						for (int k = 0; k < speMessage.size(); k++) {

							String isValid = speMessage.get(k).getAttribute("class");

							if (!isValid.contains("features-item-dash")) {

								log.info("==>>当前点击的规格是：" + speMessage.get(k).getText() + "<<==");
								Thread.sleep(3000);
								speMessage.get(k).click();
								//判断规格是否丢失
								try {
									List<WebElement> elementRed = spe.findElements(By.className("lh-35px ml-20 cor-red"));
									if (elementRed != null && elementRed.size() > 0) {
										String isSpan = speMessage.get(0).getAttribute("class");
										if (!isSpan.contains("lh-35px ml-20 cor-red ")) {
											speMessage.get(k).click();
										}
									}


									String productDay = driver.getPageSource();
									Document doc = Jsoup.parseBodyFragment(productDay);
									String warning = doc.getElementsByClass("lh-35px ml-20 cor-red ").text();
									if (StringUtils.isNoneBlank(warning)) {
										warning = doc.getElementsByClass("lh-35px ml-20 cor-red ").first().text();
										speMessage.get(k).click();
									}

								} catch (Exception e4) {
									log.info("==>>当前点击的规格是：" + speMessage.get(k).getText() + "<<==");
								}

								/* 判断选中的时期是否丢失 lh-35px ml-20 cor-red  */
								try {
									String productDay = driver.getPageSource();
									Document doc = Jsoup.parseBodyFragment(productDay);
									String warning = doc.getElementsByClass("lh-35px ml-20 cor-red ").text();
									if (warning.equalsIgnoreCase("请选择生产日期")) {
										dateMessage.get(i).click();
									}
								} catch (Exception e4) {
									log.info("==>>当前点击的日期是：" + dateMessage.get(i).getText() + "<<==");
								}

								/*判断快递是否存在*/
								if (expressage != null) {
									List<WebElement> exprMessage = expressage.findElements(By.xpath(".//div[@tabindex='0']"));
									log.info("==>>当前配送方式一共有 ：" + exprMessage.size() + " 个 <<==");
									if (exprMessage.size() == 1) {
										try {
											String url = driver.getCurrentUrl();


											if (StringUtils.isNotEmpty(url)) {
												if (url.contains("supplierNo=")) {
													price.setSKUid(url.split("supplierNo=")[1]);
												}

											}

											log.info("==>>当前浏览器商品链接 ：" + url + " <<==");
											String exprName = exprMessage.get(0).getText();
											/****商品价格*/
											String priceData = getProductPrice(driver);
											price.setCurrent_price(priceData);
											price.setOriginal_price(priceData);
											/*商品有效期*/
											price.setPromotion(dateMessage.get(i).getText());
											price.setSku_name(speMessage.get(k).getText() + ",商品单价:" + price.getCurrent_price() + ",物流方式：" + exprName);

											try {
												/****判断商品库存是否存在****/
												Document doc = Jsoup.parseBodyFragment(driver.getPageSource());
												Element ele = doc.getElementById("stockBox");
												if (ele != null) {
													price.setInventory(doc.getElementById("stockBox").getElementsByTag("span").text());
												}
												//price.setInventory(driver.findElement(By.xpath("//*[@id='stockBox']/span")).getText().toString());

												/****判断是否抓取商品营业执照****/
												if (crawlerStore.equalsIgnoreCase("true")) {
													if (StringUtils.isNoneBlank(price.getSKUid())) {
														String promotin = getProductLicense(driver, crawlerStore, price.getSKUid());
														price.setHead_promotion(promotin);
													}

												}

											} catch (Exception e) {
												e.printStackTrace();
												log.info("店铺链接不存在。。。" + price.getSKUid());
											}

											/*封装价格*/
											insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(price);
											insertPriceList.add(insertPriceMap);
										} catch (Exception e3) {
											log.error("==>>获取配送快递解析失败：<<==" + e3);
										}
									} else {
										int lastClick = 0;
										for (int s = 0; s < exprMessage.size(); s++) {

											//判断快递是否失效
											String isValidExp = exprMessage.get(s).getAttribute("class");

											if (!isValidExp.contains("features-item-dash")) {
												//更新最后点击的序号
												lastClick = s;
												if (!parameter.getListjobName().get(0).getDescribe().contains("快递all")) {
													if (exprMessage.get(s).getText().equals("快递") || exprMessage.get(s).getText().equals("物流到店")) {
														//商品价格
														productExpressage(price, crawlerStore, driver, exprMessage.get(s));
													}
												} else {
													productExpressage(price, crawlerStore, driver, exprMessage.get(s));
												}


												if (StringUtils.isNotBlank(price.getCurrent_price())) {
													/*封装价格*/
													insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(price);
													insertPriceList.add(insertPriceMap);
												}

											} else {
												log.info("==>>当前商品选择的快递暂缺货<<==" + exprMessage.get(s).getText() + "商品链接：" + info.getGoods_url());
											}
										}
										//循环完快递信息，再次点击进行重制
										exprMessage.get(lastClick).click();


									}

								} else {
									try {
										//WebElement selectedItem = driver.findElement(By.xpath("//*[@id='root']/div/div[2]/div[2]/div[2]/div[8]/div[1]"));
										Document flexStart = Jsoup.parseBodyFragment(driver.getPageSource());

										//FileUtils.writeStringToFile(new File("D://aa.html"),driver.getPageSource(),"UTF-8");
										if (StringUtils.isNotBlank(flexStart.getElementsByClass("flexStart pWarp").text())) {
											String flexStartWarp = flexStart.getElementsByClass("flexStart pWarp").get(0).getElementsByClass("ml-10").text();
											if (StringUtils.isNotBlank(flexStartWarp)) {
												flexStartWarp = StringUtils.replaceEach(flexStartWarp, new String[]{"/件", "折合单价：", "￥"}, new String[]{"", "", ""});
												price.setCurrent_price(flexStartWarp.replace("含税", ""));
												price.setOriginal_price(flexStartWarp.replace("含税", ""));

											}
										} else {
											try {
												/****商品价格*/
												String priceData = getProductPrice(driver);
												price.setCurrent_price(priceData);
												price.setOriginal_price(priceData);
											} catch (Exception e) {
												log.info("==>>解析商品价格异常 ：" + e.getMessage() + " <<==");
												e.printStackTrace();
											}
										}

										List<Element> list = flexStart.getElementsByClass("selected-item");
										if (list != null && list.size() > 0) {
											if (StringUtils.isNotBlank(dateMessage.get(i).getText())) {
												price.setPromotion(dateMessage.get(i).getText());
											} else {
												price.setPromotion(list.get(0).text());
											}
											String url = driver.getCurrentUrl();

											if (StringUtils.isNotEmpty(url)) {
												if (url.contains("supplierNo=")) {
													price.setSKUid(url.split("supplierNo=")[1]);
												}

											}
											log.info("==>>当前浏览器商品链接 ：" + url + " <<==");
											price.setSku_name(speMessage.get(k).getText() + ",商品单价:" + price.getCurrent_price());
											/*封装价格*/
											try {
												/**判断商品库存是否存在**/

												String stock = flexStart.getElementsByClass("ml-10 ").first().text();
												if (StringUtils.isEmpty(stock)) {
													price.setInventory(driver.findElement(By.xpath("//*[@id='stockBox']/span")).getText());
												} else {
													price.setInventory(stock);
												}

												/****判断是否抓取商品营业执照****/
												if (crawlerStore.equalsIgnoreCase("true")) {
													if (StringUtils.isNoneBlank(price.getSKUid())) {
														String promotin = getProductLicense(driver, crawlerStore, price.getSKUid());
														price.setHead_promotion(promotin);
													}

												}

											} catch (Exception e) {
												e.printStackTrace();
												log.info("店铺链接不存在。。。" + price.getSKUid());
											}


											insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(price);
											insertPriceList.add(insertPriceMap);
										}
									} catch (Exception e1) {
										e1.printStackTrace();
										log.error("当前组合商品已经售完！商品价格不存在了。。。。");
									}
								}

							} else {
								log.info("==>>当前商品规格已经缺货<<==" + speMessage.get(k).getText() + ",url:" + info.getGoods_url());
							}
						}

					}
				}


			}
		}catch (Exception e){
			e.printStackTrace();
			logger.error("==>>解析海拍客数据发生异常：{}<<==", e);
		}
	}

     /****获取商品价格****/
	 public String getProductPrice(WebDriver driver){
		StringBuilder builder=new StringBuilder();
		 try {
			 String priceData = StringUtils.replaceEach(driver.findElement(By.xpath(".//span[@class='ml-10']")).getText(), new String[]{"/件", "折合单价：", "￥","含税"}, new String[]{"", "", "",""});
		     builder.append(priceData);
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
		 return  builder.toString();
	 }

   /***获取验证码并且下载图片*/
	public  void captureElement(WebDriver driver,int count) throws IOException {
		try {
			WebElement verifyCodeElement = null;
			File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			BufferedImage img = ImageIO.read(screen);
			boolean success=true;
			if(count==1) {
				verifyCodeElement = driver.findElement(By.xpath("//img[@class='small-btn f-r']"));	
			}else if(count==2) {
				String item=driver.getPageSource();
				if(item.contains("请输入验证码后查看证件信息")) {
					verifyCodeElement = driver.findElement(By.cssSelector("#J_container > div > div.codeImageWrapper > div.inputImage > img"));
					//verifyCodeElement = driver.findElement(By.xpath("//*[@id=\"J_container\"]/div/div[2]/div[1]/img"));
				}else {
					success=false;
				}
			}
			if(success) {
				Point p = verifyCodeElement.getLocation();
				// 创建一个矩形使用上面的高度，和宽度
				Rectangle rect = new Rectangle(p, verifyCodeElement.getSize());
				// 得到元素的坐标
				BufferedImage dest = img.getSubimage(p.getX(), p.getY(), rect.width, rect.height);
				//存为png格式
				String imageName="code.png";
				if(count==2) {
					imageName="store.png";
				}
				ImageIO.write(dest, "png", new File(imageName));

			}

		} catch (WebDriverException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/*****商品价格*******/
	public Craw_goods_Price_Info productExpressage(Craw_goods_Price_Info price,String crawlerStore ,WebDriver driver,WebElement exprMessage) throws InterruptedException {
		
		try {
			log.info("==>>当前点击的前配送方是：" + exprMessage.getText() + "<<==");
			Thread.sleep(3000);

			if(!exprMessage.getAttribute("class").contains("features-item-checked")){
				exprMessage.click();
			}

			Thread.sleep(3000);															
			/*判断价格是否存在*/
			try {
				String priceData="";
				List<WebElement> list = new ArrayList<WebElement>() ;
				try {
					priceData = StringUtils.replaceEach(driver.findElement(By.xpath(".//span[@class='ml-10']")).getText(), new String[]{"/件", "折合单价：", "￥"}, new String[]{"", "", ""});

					WebElement selectedItem = driver.findElement(By.xpath("//*[@id='root']/div/div[2]/div[2]/div[2]/div[8]/div[1]"));
					list = selectedItem.findElements(By.xpath(".//span[@class='selected-item']"));
					if (list.size()== 0) {
						try {
							selectedItem = driver.findElement(By.xpath("//*[@id='root']/div/div[2]/div[2]/div[2]/div[10]/div[1]/div"));
						} catch (Exception e) {
							//selectedItem = driver.findElement(By.xpath("//*[@id='root']/div/div[2]/div[2]/div[2]/div[11]/div[1]"));
						}
						list = selectedItem.findElements(By.xpath(".//span[@class='selected-item']"));
					}
				} catch (Exception e1) {
					e1.printStackTrace();
					log.info("==>>解析商品价格发生异常：" + e1 + "<<==");
				}
				if(list.size()==0) {
					String productDay=driver.getPageSource();
					Document doc=Jsoup.parseBodyFragment(productDay);
					List<Element>selectedList=doc.getElementsByClass("selected-item");
					StringBuilder Buffer=new StringBuilder();
					for(int ii=0;ii<selectedList.size();ii++) {
						Buffer.append(selectedList.get(ii).text()+",");
						if(ii==0) {
							price.setPromotion(selectedList.get(ii).text());
						}
					}
					price.setSku_name(Buffer.toString());
				}else {
					String express = ""; String speData="";
					if (list.size() >= 3) {
						express =list.get(2).getText();
					} if(list.size()>=2){
						speData=","+list.get(1).getText();
					}
					price.setPromotion(list.get(0).getText());
					price.setSku_name(list.get(0).getText()+speData+ ",商品单价:" + priceData + ",物流方式：" + express);

				}

				price.setCurrent_price(priceData.replace("含税", ""));
				price.setOriginal_price(priceData.replace("含税", ""));

				String url = driver.getCurrentUrl();	
				if(StringUtils.isNotEmpty(url)) {
					if(url.contains("supplierNo=")) {
						price.setSKUid(url.split("supplierNo=")[1]);
					}

				}
				log.info("==>>当前浏览器商品链接 ：" + url + " <<==");

				try {
					
			         /**判断商品库存是否存在**/
					Document doc=Jsoup.parseBodyFragment(driver.getPageSource());
					Element ele=doc.getElementById("stockBox");
					if(ele!=null) {
						price.setInventory(doc.getElementById("stockBox").getElementsByTag("span").text());	
					}
					/****判断是否抓取商品营业执照****/
					if(crawlerStore.equalsIgnoreCase("true")) {
						if(StringUtils.isNoneBlank(price.getSKUid())) {
							String promotin=getProductLicense(driver,crawlerStore ,price.getSKUid());
							price.setHead_promotion(promotin);	
						}

					}
				} catch (Exception e) {
					e.printStackTrace();
					log.info("店铺链接不存在。。。");
				}


			} catch (Exception e2) {
				e2.printStackTrace();
				log.error("当前组合商品已经售完！商品价格不存在了。。。。");
			}
			//}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return price;	
	}

	/**
	 * @param driver
	 * @return 当前打开窗口的最后一个句柄
	 */
	public static String getLastHandle(WebDriver driver) {
		//获取当前打开窗口的所有句柄
		Set<String> Allhandles = driver.getWindowHandles();
		ArrayList<String> lst = new ArrayList<String>(Allhandles);
		return lst.get(lst.size()-1);
	} 
	/**判断商品营业执照抓取是否成功
	 * @param driver
	 * @return 
	 * @throws IOException 
	 */
	public boolean  geCertificate(WebDriver driver) throws IOException, InterruptedException {
		int count=1 ,max=5;
		boolean productBoolean=false;

		File directory = new File(".");
		String imageUrl = directory.getCanonicalPath();
		while(count<=max) {
			try {
				Thread.sleep(3000);
				String item=driver.getPageSource();
				if(item.contains("请输入验证码后查看证件信息")) {
					Thread.sleep(5000);
					captureElement(driver,2);
					String result = ChaoJiYing.PostPic(imageUrl + "\\store.png");
					JSONObject currPriceJson = JSONObject.fromObject(result);
					result = currPriceJson.getString("pic_str");
					//driver.findElement(By.xpath("//*[@id=\"J_container\"]/div/div[2]/div[1]/input")).sendKeys(result);;
					driver.findElement(By.cssSelector("#J_container > div > div.codeImageWrapper > div.inputImage > input")).sendKeys(result);
					driver.findElement(By.cssSelector("#J_container > div > div.codeImageWrapper > div:nth-child(3) > button")).click();
					isAlertPresent(driver);
					log.info("==>-----------------------验证码："+result+"-----------------------------------------------<<==");
					item=driver.getPageSource();
					if(!item.contains("请输入验证码后查看证件信息")) {
						productBoolean=true;
						log.info("==>验证码校验成功！<<==");
					}
				}else {
					productBoolean=true;
				}

			}catch(Exception e) {
				log.info("==>验证码校验失败！<<==");
				Thread.sleep(9000);
				driver.navigate().refresh();
				productBoolean=false;

			}finally {
				count ++;
			}

		 }
		return productBoolean;
	}
	/***请求店铺的营业执照***/
	public String getProductLicense(WebDriver driver,String crawlerStore ,String skuId) {
		StringBuffer str=new StringBuffer(); 
		boolean productType=true;
		String handle="";
		String handle2="";
		try {

			//if(crawlerStore.equalsIgnoreCase("true")) {
				//获取主页面1句柄
				 handle = driver.getWindowHandle();
				/**抓取商品证书商品skuid不能为空**/
				if(StringUtils.isNoneBlank(skuId)) {
					driver.findElement(By.partialLinkText(skuId)).click();
					 handle2 = getLastHandle(driver);

					log.info("新页面2句柄："+handle2);
					driver.switchTo().window(handle2);
					String itemMessage=driver.getPageSource();
					if(itemMessage.contains("请输入验证码后查看证件信息")) {
						productType=geCertificate(driver);
					}
					//判断验证码，校验是否成功
					if(productType) {
						List<WebElement>imageList=driver.findElements(By.className("image"));
						for(int ss=0;ss<imageList.size();ss++) {
							str.append(imageList.get(ss).getAttribute("src")+"&&");
						}
					}

				}
			//}
		}catch(Exception e) {
			log.error("==>>请求商品店铺地址发生异常："+e+"<<==");
		}finally {
			//if(crawlerStore.equalsIgnoreCase("true")) {
				//关闭子节点
				driver.switchTo().window(handle2);
				driver.close();
				//返回主节点
				driver.switchTo().window(handle);
			//}
		}

		return str.toString();
	}
	
	

	/***判断是否有弹出框**/
	public boolean isAlertPresent(WebDriver driver){  
		try  {
			Thread.sleep(9000);
			log.info("==>>验证码输入错误，请重新输入 处理弹出层<<==");
			Alert alert=driver.switchTo().alert();
			alert.accept();
			log.info("==>处理弹出层<<==");
			driver.navigate().refresh();
			log.info("=>>页面刷新<<==");
			return true;  
		}     
		catch (NoAlertPresentException | InterruptedException Ex)  {
			return false;  
		}     
	}
}
