/**
 * Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
 *
 * @Package: com.eddc.handler
 * @author: jack.zhao
 * @date: 2019年11月10日 下午8:57:26
 */
package com.eddc.handler;
import com.eddc.method.*;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.job.JobAndTriggerService;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.publicClass;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 项目名称：Price_monitoring_crawler
 * 类名称：JobCrawlerHandler
 *
 * @author：jack.zhao 创建时间：2019年11月10日 下午8:57:26
 * 类描述：
 */

@Component
@JobHandler(value = "jobCrawlerHandler")
public class JobCrawlerHandler extends IJobHandler {
    @Autowired
    private CrawlerPublicClassService crawlerPublicClassService;
    @Autowired
    private JobAndTriggerService jobAndTriggerService;
    @Autowired
    private AllData allData;
    @Autowired
    private XohData xohData;
    @Autowired
    private PinDuoDuoData pinDuoDuoData;
	@Autowired
	private HpkData hpkData;

	@SuppressWarnings("rawtypes")
	public ReturnT<String> execute(String param) throws Exception {
		XxlJobLogger.log("【启动程序开始监控漏抓数据】 当前服务器Ip地址是："+publicClass.Ipaddres());
		try {
			Map map=getParams(param);
			String jobName=map.get("jobName").toString();
			String platform=map.get("platform").toString();
			String countId=map.get("countId").toString();
			String database=map.get("database").toString();
			String fillCatch="1";
			String crawlerData="0";
			//0 不补抓数据 1，补抓数据
			String crawlerFillCatch="0";
			String count="0";
			int crawler=0;
			if(map.containsKey("crawlerData")){
				crawlerData=map.get("crawlerData").toString();
			}
			if(map.containsKey("crawlerFillCatch")){
				crawlerFillCatch=map.get("crawlerFillCatch").toString();
			}
			if(map.containsKey("fillCatch")){
				fillCatch=map.get("fillCatch").toString();
			}
			if(crawlerData.equalsIgnoreCase("0")) {
				count=	crawlerPublicClassService.selectCrawlerCount(countId,database,platform);
				int productCount=Integer.valueOf(count.split("-")[0]);
				int productCounthistory=Integer.valueOf(count.split("-")[1]);
				if(productCount==0) {
					count="0";
					XxlJobLogger.log("==>>当前时间段一共有："+productCount+"条商品， 需要进行抓取,开始启动程序全量抓取数据<<==");
					crawler=1;
				}else {
					crawler=productCounthistory-productCount;
					XxlJobLogger.log("==>>当前时间段一共有："+crawler+"商品失败， 需要进行补抓数据<<==");
					if(crawler==0) {
						crawler=1;
						count="2";
					}else {
						crawler=2;
                        if (crawlerFillCatch.equalsIgnoreCase("0")) {
                            count = "1";
                        } else {
                            count = "0";
                        }

                    }
                }

            }
            List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName);
			if(fillCatch.equals("1")){
				if(count.contains("0")){
					XxlJobLogger.log("【开始补抓"+platform+"数据】");
					if(listjobName!=null && listjobName.size()>0){

						if(platform.equalsIgnoreCase("xoh")){
							String time=SimpleDate.SimpleDateFormatData().format(new Date());
							xohData.crawlerCommodityAppDetails(listjobName, time);
						}else if(platform.equalsIgnoreCase("pdd")) {
							String type=map.get("type").toString();
							int pagesize=1;
							if(type.equals("2")) {
								pagesize=2;
							}
							pinDuoDuoData.regionCrawler(listjobName, Fields.TABLE_CRAW_KEYWORDS_INF, "1", 0, pagesize);
						}else if(platform.equalsIgnoreCase("hpk")){
							int requestCount=0;
							if(map.containsKey("requestCount")) {
								requestCount=Integer.valueOf(map.get("requestCount").toString());
							}
							hpkData.regionCrawler(listjobName, Fields.TABLE_CRAW_KEYWORDS_INF, "1", 0, requestCount,map);
						}else{
							allData.regionCrawler(listjobName, Fields.TABLE_CRAW_KEYWORDS_INF, "1", 0, crawler, 2);
                        }
                    } else {
                        XxlJobLogger.log("listjobName:size<>"+0);
					}
					XxlJobLogger.log("【数据抓取完成】");
				}else if(count.contains("1")){
					XxlJobLogger.log("【当前补抓程序暂时关闭，如果需要请联系管理员。。。】");

				}else{
					XxlJobLogger.log("【当前 数据没有漏抓不需要进行数据补抓】");
				}
			}else{
				XxlJobLogger.log("【开始启动 "+platform+"平台数据】");
				hpkData.regionCrawler(listjobName, Fields.TABLE_CRAW_KEYWORDS_INF, "1", 0, 2,map);
			}

		} catch (Exception e) {
			XxlJobLogger.log("【补抓数据发生异常】"+e);
			e.printStackTrace();
		} catch (Throwable e) {
			XxlJobLogger.log("【数据抓取发生异常】"+e);
			e.printStackTrace();
		}

		return ReturnT.SUCCESS;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public Map getParams(String param) {
		Map params = new HashMap();
		if (StringUtils.isNotEmpty(param)) {
			String[] ps = StringUtils.split(param, ",");
			for (String p : ps) {
				String[] s = StringUtils.split(p, "=");
				params.put(StringUtils.trim(s[0]), StringUtils.trim(s[1]));
			}
		}
		return params;
	}
}
