/**   
 * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.mapper 
 * @author: jack.zhao   
 * @date: 2017年12月5日 下午2:46:39 
 */
package com.eddc.mapper;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.eddc.model.Craw_goods_Info;
import com.eddc.model.QrtzCrawlerTable;
@Mapper
public interface IconAnalysisMapper {
	public List<QrtzCrawlerTable>getIconDetails();
	public List<Craw_goods_Info>getGoodsInfoQuery(@Param("platform")String platform,@Param("userId")String userId,@Param("status")String status);
	public List<Craw_goods_Info>GoodsFrameNumber(@Param("platform")String platform,@Param("userId")String userId,@Param("status")String status);
}
