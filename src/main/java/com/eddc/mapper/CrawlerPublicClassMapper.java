package com.eddc.mapper;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_Access_keyword;
import com.eddc.model.Craw_aliindex_category_info;
import com.eddc.model.Craw_aliindex_goodsrank_info;
import com.eddc.model.Craw_cookies_info;
import com.eddc.model.Craw_customerweb_categorys_info;
import com.eddc.model.Craw_goods_Fixed_Info;
import com.eddc.model.Craw_goods_Info;
import com.eddc.model.Craw_goods_Info_Failure;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Craw_goods_Vendor_Info;
import com.eddc.model.Craw_goods_comment_Info;
import com.eddc.model.Craw_goods_crowdfunding_Info;
import com.eddc.model.Craw_goods_translate_Info;
import com.eddc.model.Craw_keywords_Info;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.Craw_monitor_url_info;
import com.eddc.model.TmallSearchPrice;
import com.eddc.model.Craw_customerweb_goods_info;

@Mapper
public interface CrawlerPublicClassMapper { 
public void SynchronousData(@Param("platform")String platform,@Param("accountId")String accountId,@Param("tableName")String tableName);
public void SynchronousDataDelete(@Param("platform")String platform,@Param("accountId")String accountId,@Param("tableName")String tableName);
public List<CrawKeywordsInfo> crawAndParseInfo(@Param("accountId")String accountId,@Param("platform")String platform,@Param("tableName")String tableName,@Param("count")int count,@Param("pageTop")int pageTop,@Param("sums")int sums);
public List<CrawKeywordsInfo> crawAndGrabFailureGoodsData(@Param("accountId")String accountId,@Param("platform")String platform_name,@Param("tableName")String tableName,@Param("pageTop")int pageTop);
public int  InsertShelvesDataCraw (Craw_goods_Info info);//下架商品插入数据库
public int  InsertShelvesDataCrawFailure(Craw_goods_Info_Failure failure);//数据异常
public List<Craw_goods_Info>dataMessageItemQuery(@Param("keywordId")int keywordId,@Param("egoodsId")String egoodsId);//判断商品是否存在
public int Craw_Goods_Info_Insert(Craw_goods_Info info);//插入商品信息
public int Craw_Goods_Info_Update (Craw_goods_Info info);//存在更新商品信息
public List<Craw_goods_Price_Info>goods_price_info(Map<String,Object> priceMap);//判断商品价格是否已经存在
public int CrawgoodsPriceInfo(Craw_goods_Price_Info info);//插入商品的价格
public int UpdateGoodsPriceInfo(Craw_goods_Price_Info info);//更新商品的价格信息
public List<Craw_goods_Price_Info> queryArrayList(@Param("keywordId")String keywordId,@Param("channel")String channel);//查询pc端价格
public int updateDataPrice(@Param("price")String price,@Param("ID")String Id);//更新PC端价格
public int Status_ShopName(@Param("egoodsId")String egoodsId,@Param("status")int status);//商品下架修改状态
public List<TmallSearchPrice>SearchPriceQuery (@Param("accountId")String accountId,@Param("platformName")String platformName);// 根据搜索页面获取商品价格search
public String selectCrawlerCount(@Param("accountId")String accountId,@Param("platformName")String platformName);
public List<Craw_keywords_Info> queryList(@Param("accountId")int accountId);//获取当前商品详情临时数据 
public List<Craw_keywords_Info> Craw_keywords_Info(@Param("accountId")int accountId,@Param("cust_keyword_name")String cust_keyword_name);//获取当前商品详情
public void keywordInsertObject(Craw_keywords_Info info);//插入商品搜索egoodsId
public void updateCraw_keywords_info(@Param("accountId")int accountId,@Param("count")int count);//更新商品的状态
public void delete_Craw_keywords_temp_Info_forsearch(@Param("accountId")int accountId);//删除临时数据
public List<Craw_keywords_Info>keywords_Info_query(@Param("accountId")int accountId,@Param("cust_keyword_name")String cust_keyword_name);//判断商品是否存在
public void updatekeyordInfo(@Param("cust_keyword_name")String cust_keyword_name);//更新商品状态
public  List<CommodityPrices> queryForListEntye(@Param("accountId")int  accountId,@Param("platform")String platform,@Param("tableName")String tableName,@Param("pageTop")int pageTop);//获取商品的egoodsId等抓取商品价格
public  List<CommodityPrices> RecursionFailureGoods(@Param("accountId")int  accountId,@Param("platform")String platform,@Param("tableName")String tableName,@Param("pageTop")int pageTop);//获取抓取失败商品价格重新抓取
public List<Craw_Access_keyword>craw_Access_keyword_query(@Param("accountId")String  accountId);//获取关键字搜索商品
public int InsetCustomerweb_categorys_info(List<Craw_customerweb_categorys_info> list);//定制开发插入商品搜关键词
public List<Craw_customerweb_categorys_info>AaraGetproductdetails();//查询zara商品详情 
public int AaraInsertdetails(List<Craw_customerweb_goods_info> list);//插入搜索的商品详情
public int Zara_synchronousData(@Param("userId")int userId);//同步Zara数据
public  List<CommodityPrices> RecursionFailureGoods_Price(@Param("accountId")int  accountId,@Param("platform")String platform,@Param("tableName")String tableName);//获取抓取失败商品价格重新抓取
public List<Craw_cookies_info> dataCookie(@Param("userId")String userId,@Param("platform")String platform);//获取商品的cookie
public List<CommodityPrices>RecursionFailureGoodsMessage(@Param("userId")int  userId,@Param("platform")String platform,@Param("channel")String channel,@Param("tableName")String tableName);//检查抓完失败的商品价格
public List<Craw_goods_Info>JudgewhetherCurrentDateDataFetching(@Param("userId")int  userId,@Param("platform")String platform,@Param("batch_time") String batch_time);//判断当前日期数据是否抓取
public List<CrawKeywordsInfo>failure_information_list();//爬虫抓取失败商品个数
public void SearchSynchronousData(@Param("platform")String platform,@Param("accountId")String accountId,@Param("table_name")String table_name,@Param("sum")int sum);//搜索商品同步历史表数据
//public void SynchronousDataKeyword(@Param("platform")String platform,@Param("accountId")String accountId);//关键字搜索商品同步历史表数据
public List<Craw_keywords_delivery_place>Keywords_Delivery_Place(@Param("platform")String platform,@Param("accountId")String accountId,@Param("inventory")String inventory);//根据不同城市code抓取商品库存详情
public int deleteDuplicateData(@Param("platform")String platform,@Param("accountId")String accountId);//检查抓取重复数据进行删除只保留一条数据
public void SearchSynchronousDataAlibaba(@Param("currentTable") String currentTable,@Param("currentPriceTable") String currentPriceTable,@Param("platform")String platform);//定制同步数据
public List<Craw_goods_Vendor_Info>goodsShopMessage(@Param("egoodsId")String egoodsId);//alibaba商品是否存在
public List<Craw_goods_Info>goodsOnTimeQuery(@Param("cust_account_id")String cust_account_id,@Param("platform_name")String platform_name,@Param("pageTop")int pageTop);//抓取商品上架时间
public List<Craw_goods_Fixed_Info>whetherGoodsShelves(@Param("egoodsId")String egoodsId,@Param("platform")String platform,@Param("accountId")String accountId);//商品上下架时间是否存在
public List<Craw_goods_Info>jdGoodsOnTimeQuery(@Param("cust_account_id")String cust_account_id,@Param("platform_name")String platform_name);//京东指定抓取商品上架时间
public int updateCrawGoodsInfoMessage(Map<String,Object>map);//获取京东商品的好评率 差评率 中评论
public List<Craw_goods_translate_Info> nameCommoditySwitchMessage(Craw_goods_translate_Info info);//获取amazonusa 中文名称是否已经存在
public void alibabaIndex1688MessageInsert(Craw_aliindex_category_info info);//获取阿里指数3级菜单
public List<Craw_aliindex_category_info>aliIndexCateegoryInfoQuery();//获取阿里指数菜单详情
public List<Craw_goods_comment_Info>crawGoodsCommentInfoQuery(@Param("egoodsId")String egoodsId,@Param("batch_time")String batch_time );//判断抓取商品评论是否存在
public List<Craw_aliindex_goodsrank_info>indexSimilarGoods(@Param("rank_date")String rank_date);//获取指数相似产品
public void updateCrawKeywordHistory(@Param("platform")String platform,@Param("accountId")String accountId);//更新抓取的商品状态
public Craw_keywords_Info crawlerStatus(@Param("platform")String platform,@Param("accountId")String accountId);//获取爬虫状态
public void  updateCrowdFunding (@Param("table")String table);//更新数据状态
public List<Craw_goods_crowdfunding_Info>crowdFundingData(@Param("platform")String platform,@Param("pageTop")int pageTop);//获取众筹详情
public List<Craw_monitor_url_info>monitoringLeakData();//监控漏抓商品进行数据补抓
public List<CrawKeywordsInfo>examineData(@Param("accountId")String accountId,@Param("platform")String platform);//检查数据
public int historicalData();//同步搜索商品历史数据
public List<Craw_goods_Info>getCommentNumber(@Param("table")String table,@Param("platform")String platform,@Param("accountId")String accountId);//获取商品评论说
}    
       