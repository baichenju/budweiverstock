package com.eddc.config;
import java.io.IOException;
import java.util.Properties;

import org.quartz.Scheduler;
import org.quartz.ee.servlet.QuartzInitializerListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
/**
 * quartz配置
 * 创建时间	2018年4月3日
 */
@Configuration
public class SchedulerConfig {

	@Autowired
	private SpringJobFactory springJobFactory;

	@Value("${spring.datasource.db}")
	private String quartzDatasource;
	
	
	@Bean(name="SchedulerFactory")//concurrent
	public SchedulerFactoryBean schedulerFactoryBean() throws IOException {
		SchedulerFactoryBean factory = new SchedulerFactoryBean();

		//QuartzScheduler启动时更新己存在的Job,这样就不用每次修改targetObject后删除qrtz_job_details表对应记录
		//quartz参数
        Properties prop = new Properties();
        prop.put("org.quartz.scheduler.instanceName", "DefaultQuartzScheduler");
        prop.put("org.quartz.scheduler.instanceId", "AUTO");
        prop.put("org.quartz.scheduler.rmi.export", "false");
        prop.put("org.quartz.scheduler.rmi.proxy", "false");
        prop.put("org.quartz.scheduler.wrapJobExecutionInUserTransaction", "false");
        
        //线程池配置
        prop.put("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
        prop.put("org.quartz.threadPool.threadCount", "30");
        prop.put("org.quartz.threadPool.threadPriority", "8");
        prop.put("org.quartz.scheduler.wrapJobExecutionInUserTransaction", "false");
        //JobStore配置
        prop.put("org.quartz.jobStore.class", "org.quartz.impl.jdbcjobstore.JobStoreTX");
         
        //集群配置
        prop.put("org.quartz.jobStore.isClustered", "true");
        prop.put("org.quartz.jobStore.clusterCheckinInterval", "15000");
        prop.put("org.quartz.jobStore.maxMisfiresToHandleAtATime", "1");
        prop.put("org.quartz.jobStore.txIsolationLevelSerializable", "false");
      
        
        prop.put("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.StdJDBCDelegate");
        prop.put("org.quartz.jobStore.misfireThreshold", "12000");
        prop.put("org.quartz.jobStore.tablePrefix", "QRTZ_");
        prop.put("org.quartz.jobStore.selectWithLockSQL", "SELECT * FROM {0}LOCKS UPDLOCK WHERE LOCK_NAME = ?");
      
        prop.put("org.quartz.jobStore.useProperties", "true");
        prop.put("org.quartz.jobStore.dataSource", "qzDS");
        prop.put("org.quartz.jobStore.clusterCheckinInterval", "20000");
        
        //PostgreSQL数据库，需要打开此注释
        //prop.put("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.PostgreSQLDelegate");
        prop.put("org.quartz.plugin.triggHistory.class", "org.quartz.plugins.history.LoggingJobHistoryPlugin");
        prop.put("org.quartz.plugin.shutdownhook.class", "org.quartz.plugins.management.ShutdownHookPlugin");
        prop.put("org.quartz.plugin.shutdownhook.cleanShutdown", "true");
        prop.put("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.PostgreSQLDelegate");
       
        //Datasources 数据库配置
        JSONObject json = JSONObject.fromObject(quartzDatasource);
		JSONArray array = json.getJSONArray("databaseNameList");
		JSONObject object = JSONObject.fromObject(array.toArray()[0].toString());
		JSONArray databaseList = json.getJSONArray("databaseList");
		JSONObject ject = JSONObject.fromObject(databaseList.toArray()[0].toString());
		String url = String.format(ject.getString("url"), object.getString("dbIP"), object.getString("dbPort"), object.getString("dbName"));
        prop.put("org.quartz.dataSource.qzDS.driver", ject.getString("driverName"));
        prop.put("org.quartz.dataSource.qzDS.URL", url);
        prop.put("org.quartz.dataSource.qzDS.user", object.getString("userName"));
        prop.put("org.quartz.dataSource.qzDS.password", object.getString("password"));
        
        prop.put("org.quartz.dataSource.qzDS.maxConnections", "100");
        prop.put("org.quartz.dataSource.qzDS.validationQuery", "SELECT 1 FROM DUAL");
        prop.put("org.quartz.dataSource.qzDS.validateOnCheckout", "true");
        factory.setQuartzProperties(prop);
		
	
		factory.setOverwriteExistingJobs(true);
		factory.setAutoStartup(true);
		factory.setStartupDelay(10);//延时10秒启动
		//factory.setQuartzProperties(quartzProperties());
		factory.setJobFactory(springJobFactory);  
		return factory;
	}

//	@Bean
//	public Properties quartzProperties() throws IOException {
//		PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
//		propertiesFactoryBean.setLocation(new ClassPathResource("/quartz.properties"));
//		propertiesFactoryBean.afterPropertiesSet();
//		return propertiesFactoryBean.getObject();
//	}

	/*
	 * quartz初始化监听器
	 */
	@Bean
	public QuartzInitializerListener executorListener() {
		return new QuartzInitializerListener();
	}

	/*
	 * 通过SchedulerFactoryBean获取Scheduler的实例
	 */
	@Bean(name="Scheduler")
	public Scheduler scheduler() throws IOException {
		return schedulerFactoryBean().getScheduler();
	}

}