package com.eddc.config;

import org.apache.tomcat.util.threads.ThreadPoolExecutor;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * @author jack.zhao
 * @description
 * @create 2020-11-16
 */
@Configuration
@EnableAsync
public class ThreadConfig implements AsyncConfigurer {

	/**
	 * The {@link Executor} instance to be used when processing async
	 * method invocations.
	 */
	@Override
	public Executor getAsyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		//配置核心线程数
		executor.setCorePoolSize(1);
		//配置最大线程数
		executor.setMaxPoolSize(1);
		//配置队列大小
		executor.setQueueCapacity(10000);
		//设置线程活性时间(秒) 某线程空闲超过1分钟，就回收该线程
		executor.setKeepAliveSeconds(60);
		//配置线程池中的线程的名称前缀
		executor.setThreadNamePrefix("jack-thread-");
		//设置拒绝策略
		executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());

		//等待程序接受关闭线程
		executor.setWaitForTasksToCompleteOnShutdown(true);
		//执行初始化
		executor.initialize();
		return executor;
	}

	/**
	 * The {@link AsyncUncaughtExceptionHandler} instance to be used
	 * when an exception is thrown during an asynchronous method execution
	 * with {@code void} return type.
	 */
	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		return null;
	}
}
