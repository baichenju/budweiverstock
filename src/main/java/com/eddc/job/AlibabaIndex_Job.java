/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2018年5月11日 下午2:06:21 
 */
package com.eddc.job;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import com.eddc.model.Craw_aliindex_category_info;
import com.eddc.model.Craw_aliindex_goodsrank_info;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.job.JobAndTriggerService;
import com.eddc.task.AlibabaIndex_DataCrawlTask;
import com.eddc.util.BaseJob;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：AlibabaIndex_Job   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年5月11日 下午2:06:21   
 * 修改人：jack.zhao   
 * 修改时间：2018年5月11日 下午2:06:21   
 * 修改备注：   
 * @version    
 *    
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class AlibabaIndex_Job implements BaseJob  {
	private static Logger loog = LoggerFactory.getLogger(AlibabaIndex_Job.class);
	private ApplicationContext contexts= SpringContextUtil.getApplicationContext(); 
	public AlibabaIndex_Job(){}
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		loog.info("【"+jobName+"__阿里巴巴指数 Job执行时间: date:{}】" , new Date());
		try {
			JobAndTriggerService jobAndTriggerService = (JobAndTriggerService) contexts.getBean(JobAndTriggerService.class);
			CrawlerPublicClassService crawlerPublicClassService = (CrawlerPublicClassService)contexts.getBean(CrawlerPublicClassService.class);
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName);
			List<Craw_aliindex_category_info>infoList=crawlerPublicClassService.aliIndexCateegoryInfoQuery();
			if(infoList.size()>0){
				aliIndexCrawlerDataTrading_Weeks(infoList,listjobName);//交易/统计周期 最近7天
				aliIndexCrawlerDataTrading_Month(infoList,listjobName);//交易/统计周期  最近30天
				aliIndexCrawlerDataTraffic_Weeks(infoList,listjobName);//流量/统计周期 最近7天
				aliIndexCrawlerDataTraffic_Month(infoList,listjobName);//流量/统计周期  最近30天
				indexSimilarGoods(listjobName);//获取商品的相似商品 
			}
		} catch (Exception e) {
		}
	}
	//开始爬虫阿里排行》产品排行榜单》维度 ，交易/统计周期 最近7天
	public void aliIndexCrawlerDataTrading_Weeks(List<Craw_aliindex_category_info>infoList,List<QrtzCrawlerTable>listjobName){
		long time=System.currentTimeMillis();
		try {
			ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
			for(Craw_aliindex_category_info categoryList:infoList){ 
			AlibabaIndex_DataCrawlTask dataCrawlTask = (AlibabaIndex_DataCrawlTask) contexts.getBean(AlibabaIndex_DataCrawlTask.class);
			dataCrawlTask.setIpProxy(listjobName.get(0).getDataSource());
			dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
			dataCrawlTask.setStatus_type(Fields.STYPE_1);
			dataCrawlTask.setInfo(categoryList);
			dataCrawlTask.setUrl(Fields.ALI_INDEX_TRADING_WEEKS.replace("CAT",String.valueOf(categoryList.getCategory_id()))+"&time="+time);
			dataCrawlTask.setRank_dimention_id(1);
			dataCrawlTask.setRank_period_id(7);
			dataCrawlTask.setIp(listjobName.get(0).getIP());
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
			dataCrawlTask.setPlatform_name(listjobName.get(0).getPlatform());
			taskExecutor.execute(dataCrawlTask);
			}
			taskExecutor.shutdown();
			while(true){
				if(((ExecutorService) taskExecutor).isTerminated()){
					loog.info("【抓取阿里排行》产品排行榜单》维度 ，交易/统计周期 最近7天 结束】");
					break;
				}	
			} 
		} catch (Exception e) {
			loog.error("【抓取阿里排行》产品排行榜单》维度 ，交易/统计周期 最近7天 发生异常 error】",e);
		}

	}

	//开始爬虫阿里排行》产品排行榜单》维度 ，交易/统计周期 最近30天
	public void aliIndexCrawlerDataTrading_Month(List<Craw_aliindex_category_info>infoList,List<QrtzCrawlerTable>listjobName){
		long time=System.currentTimeMillis();
		try {
			ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
			for(Craw_aliindex_category_info categoryList:infoList){ 
			AlibabaIndex_DataCrawlTask dataCrawlTask = (AlibabaIndex_DataCrawlTask) contexts.getBean(AlibabaIndex_DataCrawlTask.class);
			dataCrawlTask.setIpProxy(listjobName.get(0).getDataSource());
			dataCrawlTask.setInfo(categoryList);
			dataCrawlTask.setUrl(Fields.ALI_INDEX_TRADING_MONTH.replace("CAT",String.valueOf(categoryList.getCategory_id()))+"&time="+time);
			dataCrawlTask.setRank_dimention_id(1);
			dataCrawlTask.setRank_period_id(30);
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
			dataCrawlTask.setStatus_type(Fields.STYPE_1);
			dataCrawlTask.setIp(listjobName.get(0).getIP());
			dataCrawlTask.setPlatform_name(listjobName.get(0).getPlatform());
			taskExecutor.execute(dataCrawlTask);
			}
			taskExecutor.shutdown();
			while(true){
				if(((ExecutorService) taskExecutor).isTerminated()){
					loog.info("抓取阿里排行》产品排行榜单》维度 ，交易/统计周期 最近30天 结束"+SimpleDate.SimpleDateFormatData().format(new Date()));
					break;
				}	
			} 
		} catch (Exception e) {
			loog.info("阿里排行》产品排行榜单》维度 ，流量/统计周期  最近30天 发生异常"+e.toString()+"当前时间"+SimpleDate.SimpleDateFormatData().format(new Date()));
		}

	}
	//开始爬虫阿里排行》产品排行榜单》维度 ，流量/统计周期 最近7天
	public void aliIndexCrawlerDataTraffic_Weeks(List<Craw_aliindex_category_info>infoList,List<QrtzCrawlerTable>listjobName){
		 long time=System.currentTimeMillis();
		try {
			ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
			for(Craw_aliindex_category_info categoryList:infoList){ 
			AlibabaIndex_DataCrawlTask dataCrawlTask = (AlibabaIndex_DataCrawlTask) contexts.getBean(AlibabaIndex_DataCrawlTask.class);
			dataCrawlTask.setIpProxy(listjobName.get(0).getDataSource());
			dataCrawlTask.setInfo(categoryList);
			dataCrawlTask.setRank_dimention_id(2);
			dataCrawlTask.setRank_period_id(7);
			dataCrawlTask.setIp(listjobName.get(0).getIP());
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
			dataCrawlTask.setUrl(Fields.ALI_INDEX_TRAFFIC_WEEKS.replace("CAT",String.valueOf(categoryList.getCategory_id()))+"&time="+time);
			dataCrawlTask.setStatus_type(Fields.STYPE_1);
			dataCrawlTask.setPlatform_name(listjobName.get(0).getPlatform());
			taskExecutor.execute(dataCrawlTask);
			}
			taskExecutor.shutdown();
			while(true){
				if(((ExecutorService) taskExecutor).isTerminated()){
					loog.info("抓取阿里排行》产品排行榜单》维度 ，流量/统计周期 最近7天 结束"+SimpleDate.SimpleDateFormatData().format(new Date()));
					break;
				}	
			} 
		} catch (Exception e) {
			loog.info("阿里排行》产品排行榜单》维度 ，流量/统计周期 最近7天  发生异常"+e.toString()+"当前时间"+SimpleDate.SimpleDateFormatData().format(new Date()));
		}

	}

	//开始爬虫阿里排行》产品排行榜单》维度 ，流量/统计周期 最近30天
	public void aliIndexCrawlerDataTraffic_Month(List<Craw_aliindex_category_info>infoList,List<QrtzCrawlerTable>listjobName){
		 long time=System.currentTimeMillis();
		try {
			ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
			for(Craw_aliindex_category_info categoryList:infoList){
			AlibabaIndex_DataCrawlTask dataCrawlTask = (AlibabaIndex_DataCrawlTask) contexts.getBean(AlibabaIndex_DataCrawlTask.class);
			dataCrawlTask.setIpProxy(listjobName.get(0).getDataSource());
			dataCrawlTask.setInfo(categoryList);
			dataCrawlTask.setUrl(Fields.ALI_INDEX_TRAFFIC_MONTH.replace("CAT",String.valueOf(categoryList.getCategory_id()))+"&time="+time);
			dataCrawlTask.setRank_dimention_id(2);
			dataCrawlTask.setRank_period_id(30);
			dataCrawlTask.setIp(listjobName.get(0).getIP());
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
			dataCrawlTask.setStatus_type(Fields.STYPE_1);
			dataCrawlTask.setPlatform_name(listjobName.get(0).getPlatform());
			taskExecutor.execute(dataCrawlTask);
			} 
			taskExecutor.shutdown();
			while(true){
				if(((ExecutorService) taskExecutor).isTerminated()){
					loog.info("抓取阿里排行》产品排行榜单》维度 ，流量/统计周期 最近30天 结束"+SimpleDate.SimpleDateFormatData().format(new Date()));
					break;
				}	
			} 
		} catch (Exception e) {
			loog.info("阿里排行》产品排行榜单》维度 ，交易/统计周期  最近30天  发生异常"+e.toString()+"当前时间"+SimpleDate.SimpleDateFormatData().format(new Date()));
		}

	}
	//获取商品的相似商品
	@SuppressWarnings("static-access")
	public void indexSimilarGoods(List<QrtzCrawlerTable>listjobName){
		SimpleDateFormat formt = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.add(calendar.DATE, -1);
		try {
			CrawlerPublicClassService crawlerPublicClassService = (CrawlerPublicClassService)contexts.getBean(CrawlerPublicClassService.class);
			List<Craw_aliindex_goodsrank_info>list=crawlerPublicClassService.indexSimilarGoods(formt.format(calendar.getTime()));//获取爬虫所需要的商品信息
			ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
			for(Craw_aliindex_goodsrank_info info:list){
				AlibabaIndex_DataCrawlTask dataCrawlTask = (AlibabaIndex_DataCrawlTask) contexts.getBean(AlibabaIndex_DataCrawlTask.class);
				dataCrawlTask.setIpProxy(listjobName.get(0).getDataSource());
				dataCrawlTask.setPlatform_name(listjobName.get(0).getPlatform());
				dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
				dataCrawlTask.setGoodsrank(info);
				dataCrawlTask.setIp(listjobName.get(0).getIP());
				dataCrawlTask.setStatus_type(Fields.STYPE_2);
				taskExecutor.execute(dataCrawlTask);
			    } 
				taskExecutor.shutdown();
				while(true){
					if(((ExecutorService) taskExecutor).isTerminated()){
						loog.info("获取商品的相似商品 结束"+SimpleDate.SimpleDateFormatData().format(new Date()));
						break;
					}	
				} 
		} catch (Exception e) {
			loog.info("获取商品的相似商品  发生异常"+e.toString()+"当前时间"+SimpleDate.SimpleDateFormatData().format(new Date()));	
		}
		
	}
}
