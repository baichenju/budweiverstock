/**   
 * Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2019年11月14日 上午10:23:31 
 */
package com.eddc.job;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.eddc.method.AllData;
import com.eddc.method.HpkData;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.job.JobAndTriggerService;
import com.eddc.util.BaseJob;
import com.eddc.util.Fields;
import lombok.extern.slf4j.Slf4j;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：All_Job   
* @author：jack.zhao       
* 创建时间：2019年11月14日 上午10:23:31   
* 类描述：   
* @version    
*    
*/
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
@Slf4j
@Component
public class All_Job  implements BaseJob {
	private String tableName=Fields.TABLE_CRAW_KEYWORDS_INF;//表名称
	private String counts=Fields.STATUS_ON;
	@Autowired
	private JobAndTriggerService jobAndTriggerService ;
	@Autowired
	private AllData allData;
	@Autowired
	private HpkData hpkData;
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		Map<String,String> map=new HashMap<String, String>();
		int pageTop=0;
		log.info("==>>启动"+jobName+":程序开始抓取数据<<==");
		try {
			System.gc();
			if(jobName.contains(Fields.SEARCH)){
				tableName=Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH;
				counts=Fields.STATUS_OFF;
				pageTop=1;
			}
			
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName);	
			if(jobName.contains("HPK")) {
				map.put("jobName", jobName);
				map.put("platform", listjobName.get(0).getPlatform());
				map.put("countId", listjobName.get(0).getUser_Id());
				map.put("database", listjobName.get(0).getDatabases());
				map.put("userName", "17601265958");
				map.put("password", "DAneng12");
				hpkData.regionCrawler(listjobName, jobName, "1", 0, 1,map);
			}else {
				allData.regionCrawler(listjobName,tableName,counts,pageTop,1,1);	
			}
			System.gc();
		} catch (Exception e) {
			log.error("==>>解析数据发送异常 ,error:{}<<==",e);
		}
	}

}
