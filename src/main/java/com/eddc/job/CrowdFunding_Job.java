package com.eddc.job;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;  
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import com.eddc.model.Craw_goods_crowdfunding_Info;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.job.JobAndTriggerService;
import com.eddc.task.CrowdFunding_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
@SuppressWarnings("serial")
public class CrowdFunding_Job  implements Job,Serializable  {   
	private static Logger logger = LoggerFactory.getLogger(CrowdFunding_Job.class); 
	private ApplicationContext contexts= SpringContextUtil.getApplicationContext();  
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	private JobAndTriggerService jobAndTriggerService; 
	private String tableName=Fields.TABLE_CRAW_GOODS_CROWDFUNDING_PRICE_INFO;//表名称
	public CrowdFunding_Job(){};
	//开始调用定时任务
	public void execute(JobExecutionContext  context)  throws JobExecutionException { 
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		int pageTop=0;
		logger.info("【程序启动开始抓取众筹数据 "+jobName+"】");
		try {
			crawlerPublicClassService = (CrawlerPublicClassService) contexts.getBean(CrawlerPublicClassService.class);
			jobAndTriggerService = (JobAndTriggerService) contexts.getBean(JobAndTriggerService.class);
		
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName);
			if(listjobName.size()>0){
				if(jobName.contains(Fields.SEARCH)){
					pageTop=1;
				}
				crawlerCrowdFundingPriceMonitoring(listjobName,tableName,pageTop);//启动爬虫	
			}else{
				logger.info("【当前众筹用户不存在！爬虫结束------】"+new Date());	
			}	
		} catch (Exception e) {
			logger.error("【获取JobAndTriggerService对象为空------ error：{}】",e);
		}
	} 

	//开始爬虫
	public void crawlerCrowdFundingPriceMonitoring(List<QrtzCrawlerTable>listjobName,String tableName,int pageTop){ 
		String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
		try {
			crawlerPublicClassService.updateCrawKeywordHistory(listjobName,listjobName.get(0).getDatabases());//更新已经抓取商品的状态
			crawlerPublicClassService.SearchSynchronousDataAlibaba(Fields.CRAW_GOODS_TRANSLATE_INFO,listjobName.get(0).getDatabases(),null,listjobName.get(0).getPlatform());//同步数据 
			crawlerPublicClassService.SearchSynchronousDataAlibaba(tableName,listjobName.get(0).getDatabases(),null,null);//同步数据  
			crowdFunding_craw_item(listjobName,time,tableName,pageTop); //开始抓取数据
		} catch (Exception e) {  
			logger.error("【众筹爬虫失败请检查数据 error:{}】",e);
		}    

	}
	//开始爬数据
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void crowdFunding_craw_item(List<QrtzCrawlerTable>listjobName,String time,String tableName,int pageTop) throws InterruptedException{
		final long awaitTime=Integer.valueOf(listjobName.get(0).getDocker())*1000*60;  
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		List<Craw_goods_crowdfunding_Info>list=crawlerPublicClassService.crowdFundingData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),pageTop);//开始解析数据 
		for(Craw_goods_crowdfunding_Info accountInfo:list){
			CrowdFunding_DataCrawlTask dataCrawlTask = (CrowdFunding_DataCrawlTask) contexts.getBean(CrowdFunding_DataCrawlTask.class);
			Map<String,Object>TaobaozcMap=new HashMap<String,Object>();
			dataCrawlTask.setCrowdfunding(accountInfo);
			dataCrawlTask.setTaobaozcMap(TaobaozcMap);
			dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
			dataCrawlTask.setDataType(listjobName.get(0).getPlatform()); 
			dataCrawlTask.setTimeDate(time);
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
			dataCrawlTask.setType(Fields.STYPE_1);
			dataCrawlTask.setIp(listjobName.get(0).getIP());
			dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
			if(accountInfo.getPlatform_name_en().equals(Fields.PLATFORM_TAOBAO_ZC)){
				dataCrawlTask.setSetUrl(Fields.TAOBAOZC_URL+accountInfo.getEgoodsId());	
			}else{
				dataCrawlTask.setSetUrl(accountInfo.getGoods_url());		
			}
			Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
			futureList.add(future);
			logger.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		}
		try {
			taskExecutor.shutdown();
			if(pageTop==Fields.STATUS_COUNT){
				while(true){
					if(((ExecutorService) taskExecutor).isTerminated()){
						taskExecutor.shutdownNow(); 
						crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据	

						break;	
					}
				}	
			}else{
				if(!taskExecutor.awaitTermination(awaitTime, TimeUnit.MILLISECONDS)){ 
					taskExecutor.shutdownNow();  // 超时的时候向线程池中所有的线程发出中断(interrupted)。  
				}
			}
		} catch (Exception e) {
			// 超时的时候向线程池中所有的线程发出中断(interrupted)。  
			taskExecutor.shutdownNow();  
		}finally{
			if(pageTop==Fields.STATUS_COUNT_1){
				crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				crawlerPublicClassService.updateCrawKeywordHistory(listjobName,listjobName.get(0).getDatabases());//更新已经抓取商品的状态
			}
		}
		
	}  
}
