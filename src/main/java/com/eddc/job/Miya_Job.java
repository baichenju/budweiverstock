/**   
 * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2017年11月23日 下午3:53:36 
 */
package com.eddc.job;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.job.JobAndTriggerService;
import com.eddc.task.Miya_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Miya_Job
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2017年11月23日 下午3:53:36   
 * 修改人：jack.zhao   
 * 修改时间：2017年11月23日 下午3:53:36   
 * 修改备注：   
 * @version    
 *    
 */
@SuppressWarnings("serial")
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class Miya_Job implements Job,Serializable {
	private static Logger logger = LoggerFactory.getLogger(Miya_Job.class);
	private String message="";
	ApplicationContext contexts= SpringContextUtil.getApplicationContext();
	@Autowired
	private JobAndTriggerService jobAndTriggerService ;
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	public Miya_Job(){}
	@Override
	public void execute(JobExecutionContext context)throws JobExecutionException {
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		logger.info("【"+jobName+"__蜜芽 Job执行】");
		atartCrawlerMiya(jobName);
	}
	/**  
	 * @Title: atartCrawlerMiya  
	 * @Description: TODO(爬取蜜芽数据)  
	 * @param     设定文件  
	 * @return void    返回类型  
	 * @throws  
	 */  
	public void atartCrawlerMiya(String  jobName){
		try {
			contexts= SpringContextUtil.getApplicationContext();
			jobAndTriggerService = (JobAndTriggerService) contexts.getBean(JobAndTriggerService.class);
			crawlerPublicClassService = (CrawlerPublicClassService) contexts.getBean(CrawlerPublicClassService.class);
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName);
			String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
			if(listjobName.size()>0){
				logger.info("【开始同步蜜芽数据当前用户是"+listjobName.get(0).getUser_Id()+"--------】");
				crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),Fields.TABLE_CRAW_KEYWORDS_INF);//同步数据
				crawlerVipPriceMonitoring(time,listjobName,message);	
				if(listjobName.get(0).getClient().equals("ALL")|| listjobName.get(0).getClient().equals("all")){
					logger.info("【开始爬蜜芽PC端价格当前用户是"+listjobName.get(0).getUser_Id()+"--------】");
					TerminalCommodityPrices(time,listjobName,message);
				}
			}else{
				logger.info("【当前蜜芽用户不存在请检查爬虫状态是否关闭！爬虫结束------】");	
			}		
		} catch (Exception e) {
			logger.error("【获取JobAndTriggerService对象为空------ error：{}】",e);
		}
	}
	/**  
	 * @Title: crawlerVipPriceMonitoring  
	 * @Description: TODO 开始解析蜜芽数据
	 * @param @param listjobName
	 * @param @param message    设定文件  
	 * @return void    返回类型  
	 * @throws  
	 */  
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void crawlerVipPriceMonitoring(String time,List<QrtzCrawlerTable>listjobName,String message){
		int jj = 0;
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndParseInfo(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_ON,Fields.STATUS_COUNT);//开始解析数据 
		ApplicationContext context = SpringContextUtil.getApplicationContext();
		for (CrawKeywordsInfo accountInfo : list) { jj++;
		Map<String,Object>miyaData=new HashMap<String,Object>();
		Miya_DataCrawlTask dataCrawlTask = (Miya_DataCrawlTask) context.getBean(Miya_DataCrawlTask.class);
		dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
		dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
		dataCrawlTask.setStorage(listjobName.get(0).getStorage());
		dataCrawlTask.setTimeDate(time);
		dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
		dataCrawlTask.setCrawKeywordsInfo(accountInfo);
		dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
		dataCrawlTask.setIp(listjobName.get(0).getIP());
		dataCrawlTask.setType(Fields.STYPE_1); 
		dataCrawlTask.setMiyaData(miyaData);
		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
		if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_MiA_EN)){
			dataCrawlTask.setSetUrl(Fields.MIYA_APP_URL + accountInfo.getCust_keyword_name()+ ".html");
		}else{
			dataCrawlTask.setSetUrl(Fields.MIYA_APP_GLOBAL_URL + accountInfo.getCust_keyword_name()+ ".html");
		}
		Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		futureList.add(future);
		logger.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		}
		taskExecutor.shutdown();
		while (true) {
			if(((ExecutorService) taskExecutor).isTerminated()){
				taskExecutor.shutdownNow(); 
				if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
					crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				}
				break;
			}
		}
		try {
			crawAndParseInfoAndPricefailure(time,listjobName,message);//检查是否有失败的商品
		} catch (Exception e) {
			logger.error("【解析失败信息失败 error:{}】",e);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void crawAndParseInfoAndPricefailure(String time,List<QrtzCrawlerTable>listjobName,String messageId) throws InterruptedException {
		int jj = 0; int count = 0;
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(10);  //初始化线程池
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		do {
			List<CrawKeywordsInfo> list = crawlerPublicClassService.crawAndGrabFailureGoodsData(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_COUNT);//开始解析数据
			ApplicationContext context = SpringContextUtil.getApplicationContext();
			for (CrawKeywordsInfo accountInfo : list) { jj++;
			Map<String,Object>miyaData=new HashMap<String,Object>();
			Miya_DataCrawlTask dataCrawlTask = (Miya_DataCrawlTask) context.getBean(Miya_DataCrawlTask.class);
			dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
			dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
			dataCrawlTask.setStorage(listjobName.get(0).getStorage());
			dataCrawlTask.setTimeDate(time);
			dataCrawlTask.setMiyaData(miyaData);
			dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
			dataCrawlTask.setCrawKeywordsInfo(accountInfo);
			dataCrawlTask.setType(Fields.STYPE_1);
			dataCrawlTask.setIp(listjobName.get(0).getIP());
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
			dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
			if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_MiA_EN)){
				dataCrawlTask.setSetUrl(Fields.MIYA_APP_URL + accountInfo.getCust_keyword_name()+ ".html");
			}else{
				dataCrawlTask.setSetUrl(Fields.MIYA_APP_GLOBAL_URL + accountInfo.getCust_keyword_name()+ ".html");
			}
			Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
			futureList.add(future);
			logger.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
			}
			jj = 0;
			taskExecutor.shutdown();
			while (true) {
				if(((ExecutorService) taskExecutor).isTerminated()){
					taskExecutor.shutdownNow(); 
					if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
						crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
					}
					break;
				}
			}
			count++;
			if (count > 5 ||list!=null&&list.size()==0) {
				Thread.sleep(1000);
				return;
			}
		} while (true);
	}
	/**
	 * 获取PC商品价格
	 *
	 * @throws InterruptedException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void TerminalCommodityPrices(String time,List<QrtzCrawlerTable>listjobName,String message) {
		int ii = 0;
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		List<CommodityPrices> listPrice = crawlerPublicClassService.CommodityPricesData(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_COUNT);
		for (CommodityPrices accountInfo : listPrice) { ii++;
		Map<String,Object>miyaData=new HashMap<String,Object>();
		Miya_DataCrawlTask dataCrawlTask = (Miya_DataCrawlTask) contexts.getBean(Miya_DataCrawlTask.class);
		accountInfo.setBatch_time(time);
		dataCrawlTask.setMiyaData(miyaData);
		dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
		dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
		dataCrawlTask.setSum("" + ii + "/" + listPrice.size() + "");
		accountInfo.setPlatform_name_en(listjobName.get(0).getPlatform());
		dataCrawlTask.setCommodityPrices(accountInfo);
		dataCrawlTask.setType(Fields.STYPE_6); 
		dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
		dataCrawlTask.setIp(listjobName.get(0).getIP());
		dataCrawlTask.setSetUrl(Fields.MIYA_PC_URL+accountInfo.getEgoodsId()+ ".html");
		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
		if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_MiA_EN)){
			dataCrawlTask.setSetUrl(Fields.MIYA_PC_URL + accountInfo.getEgoodsId()+ ".html");
		}else{
			dataCrawlTask.setSetUrl(Fields.MIYA_PC_GLOBAL_URL + accountInfo.getEgoodsId()+ ".html");
		}
		Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		futureList.add(future);
		logger.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		} 
		taskExecutor.shutdown();
		while (true) { 
			if(((ExecutorService) taskExecutor).isTerminated()){
				taskExecutor.shutdownNow();
				if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
					crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				}
				break;
			}
		}
		ii = 0;
		try {// 递归补漏
			Thread.sleep(20000);
			miyaRecursionFailureGoods(time,listjobName);
		} catch (Exception e) {
			logger.error("【递归补漏失败 error:{}】",e);
		}
	}

	/**
	 * 递归检查获取没有成功抓取商品的价格
	 *
	 * @throws InterruptedException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void miyaRecursionFailureGoods(String time,List<QrtzCrawlerTable>listjobName) throws InterruptedException {
		int ii = 0; int count=0;
		ExecutorService taskExecutor = Executors.newFixedThreadPool(10);  //初始化线程池
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		do {
			ArrayList<Future> futureList = new ArrayList<Future>();
			List<CommodityPrices> listmy = crawlerPublicClassService.RecursionFailureGoods(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_COUNT);
			if (listmy.size() > 0) {
				for (CommodityPrices accountInfo : listmy) { ii++;
				Map<String,Object>miyaData=new HashMap<String,Object>();
				Miya_DataCrawlTask dataCrawlTask = (Miya_DataCrawlTask) contexts.getBean(Miya_DataCrawlTask.class);
				dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
				dataCrawlTask.setStorage(listjobName.get(0).getStorage());
				dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
				accountInfo.setPlatform_name_en(listjobName.get(0).getPlatform());
				dataCrawlTask.setMiyaData(miyaData);
				dataCrawlTask.setTimeDate(time);
				dataCrawlTask.setSum("" + ii + "/" + listmy.size() + "");
				accountInfo.setBatch_time(time);
				dataCrawlTask.setCommodityPrices(accountInfo);
				dataCrawlTask.setType(Fields.STYPE_6);
				dataCrawlTask.setIp(listjobName.get(0).getIP());
				dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
				dataCrawlTask.setSetUrl(Fields.MIYA_PC_URL + accountInfo.getEgoodsId() + ".html");
				dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
				Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
				futureList.add(future);
				logger.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
				}
			}
			taskExecutor.shutdown();
			while (true) {
				if(((ExecutorService) taskExecutor).isTerminated()){
					taskExecutor.shutdownNow(); 
					if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
						crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
					}
					break;
				}
			}
			ii = 0;
			count++;
			if (count > 5 ||listmy!=null&&listmy.size()==0) {
				logger.info("【递归补漏 第"+count+"次】");
				Thread.sleep(5000); 
				return;
			}
		} while (true);	
	}
}
