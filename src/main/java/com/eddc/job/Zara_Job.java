//package com.eddc.job;
//import java.io.Serializable;
//import java.util.Date;
//import java.util.List;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.quartz.DisallowConcurrentExecution;
//import org.quartz.Job;
//import org.quartz.JobExecutionContext;
//import org.quartz.JobExecutionException;
//import org.quartz.PersistJobDataAfterExecution;
//import org.springframework.context.ApplicationContext;
//import com.eddc.model.Craw_customerweb_categorys_info;
//import com.eddc.model.QrtzCrawlerTable;
//import com.eddc.service.JobAndTriggerService;
//import com.eddc.service.Keywords_DataCrawlService;
//import com.eddc.service.Zara_DataCrawlService;
//import com.eddc.task.Zara_DataCrawlTask;
//import com.eddc.util.Fields;
//import com.eddc.util.SimpleDate;
//import com.eddc.util.SpringContextUtil;
//import com.eddc.util.publicClass;
//
///**   
// *    
// * 项目名称：Price_monitoring_crawler   
// * 类名称：Zara_Job   
// * 类描述：   抓取商品关键词
// * 创建人：jack.zhao   
// * 创建时间：2017年11月16日 下午3:31:13   
// * 修改人：jack.zhao   
// * 修改时间：2017年11月16日 下午3:31:13   
// * 修改备注：   
// * @version    
// *    
// */
//@SuppressWarnings("serial")
//@PersistJobDataAfterExecution
//@DisallowConcurrentExecution// 不允许并发执行
//public class Zara_Job implements Job,Serializable{
//	private static Logger logger = LoggerFactory.getLogger(Zara_Job.class);
//	private ApplicationContext contexts= SpringContextUtil.getApplicationContext();
//	public Zara_Job(){};
//	//private  SimpleDateFormat data=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//	@Override
//	public void execute(JobExecutionContext context)throws JobExecutionException {
//		String jobName=context.getJobDetail().getKey().toString();
//		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
//		logger.info("【"+jobName+"__Zara关键词搜索 Job执行时间】");
//		
//		try {
//			JobAndTriggerService jobAndTriggerService = (JobAndTriggerService) contexts.getBean(JobAndTriggerService.class);
//			Zara_DataCrawlService zara_DataCrawlService=(Zara_DataCrawlService)contexts.getBean(Zara_DataCrawlService.class);
//			//String Ip=publicClass.getLocalIP();
//			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName);
//			try {
//				if(listjobName.size()>0){ 
//					zara_DataCrawlService.SynchronousData(listjobName.get(0).getUser_Id());
//					crawlerKeywords(listjobName);
//					logger.info("【开始同步ZARA数据 当前用户是"+listjobName.get(0).getUser_Id()+"----------】");
//					zara_DataCrawlService.SynchronousData(listjobName.get(0).getUser_Id());
//					logger.info("【同步ZARA数据结束 当前用户是"+listjobName.get(0).getUser_Id()+"--------】");
//				}else{
//					logger.info("【当前Zara不存在请打开爬虫状态！爬虫结束------】");	
//				}
//			} catch (Exception e) {
//				logger.error("【搜索关键词查询启动失败-----error:{}】",e);
//			} 
//		} catch (Exception e) {
//			logger.error("【获取JobAndTriggerService对象为空------ error：{}】",e);
//		}
//		
//	}	
//	/**  
//	 * @throws Exception   
//	 * @Title: crawlerKeywords  
//	 * @Description: TODO搜索关键词
//	 * @param @param listjobName    设定文件  
//	 * @return void    返回类型  
//	 * @throws  
//	 */  
//	public void crawlerKeywords(List<QrtzCrawlerTable>listjobName) throws Exception{
//		int jj=0;
//		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
//		Keywords_DataCrawlService keywords_DataCrawlService = (Keywords_DataCrawlService) contexts.getBean(Keywords_DataCrawlService.class);
//		List<Craw_customerweb_categorys_info>list=keywords_DataCrawlService.queryKeywordData(listjobName.get(0).getUser_Id());
//		for(Craw_customerweb_categorys_info accountInfo:list){ jj++;
//		Zara_DataCrawlTask dataCrawlTask = (Zara_DataCrawlTask) contexts.getBean(Zara_DataCrawlTask.class);
//		dataCrawlTask.setDataType(Fields.ZARA_STORE);
//		dataCrawlTask.setSum(""+jj+"/"+list.size()+"");
//		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
//		dataCrawlTask.setSetUrl(accountInfo.getWebsite_url());
//		dataCrawlTask.setUserId(accountInfo.getUserid()); 
//		dataCrawlTask.setSeries(accountInfo.getSeries());
//		dataCrawlTask.setCategories(accountInfo.getCategories());
//		dataCrawlTask.setIp(listjobName.get(0).getIP());
//		taskExecutor.execute(dataCrawlTask);
//		}
//		taskExecutor.shutdown();
//		while(true){
//			if(((ExecutorService) taskExecutor).isTerminated()){
//				logger.info("【信息爬虫结束开始检查是否有漏抓数据-----】"+SimpleDate.SimpleDateFormatData().format(new Date()));
//				break;
//			}	
//
//		} 
//		
//	}
//}
//
//
