/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2018年3月14日 下午12:20:51 
 */
package com.eddc.job;
import java.util.Date;
import java.util.List;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import com.eddc.method.AlibabaEnData;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.job.JobAndTriggerService;
import com.eddc.util.BaseJob;
import com.eddc.util.Fields;
import com.eddc.util.SpringContextUtil;
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Alibabaen_Job   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年3月19日 下午12:20:51   
 * 修改人：jack.zhao   
 * 修改时间：2018年3月19日 下午12:20:51   
 * 修改备注：   
 * @version    
 *    
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class Alibabaen_Job  implements BaseJob  {
	private static Logger logger = LoggerFactory.getLogger(Alibabaen_Job.class);
	private String tableName=Fields.TABLE_CRAW_KEYWORDS_INF;//表名称
	private String counts=Fields.STATUS_ON;
	@Autowired
	private JobAndTriggerService jobAndTriggerService ;
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private AlibabaEnData AlibabaEnData; 
	public Alibabaen_Job(){}
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		int pageTop=0;
		logger.info("【"+jobName+"__Alibabaen Job执行时间: date:{}】", new Date());
		ApplicationContext contexts= SpringContextUtil.getApplicationContext(); 
		try {
			crawlerPublicClassService = (CrawlerPublicClassService) contexts.getBean(CrawlerPublicClassService.class);
			jobAndTriggerService = (JobAndTriggerService) contexts.getBean(JobAndTriggerService.class);
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName);
			if(listjobName.size()>0){
				if(jobName.contains(Fields.SEARCH)){
					tableName=Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH_HISTORY;
					counts=Fields.STATUS_OFF;
					pageTop=1;
				}
				AlibabaEnData.alibabaenCrawler(listjobName,tableName,counts,pageTop);
				if(jobName.contains(Fields.SEARCH)){
					crawlerPublicClassService.updateCrawKeywordHistory(listjobName,listjobName.get(0).getDatabases());//更新已经抓取商品的状态
				}

			}else{
				logger.info("【当前AlibabaEn用户不存在请检查爬虫状态是否关闭！爬虫结束------date:{}】",new Date());	
			}
		} catch (Exception e) {
			logger.error("【获取JobAndTriggerService对象为空------ error：{}】",e);
		}

	}
}
