/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2018年9月9日 下午7:23:00 
 */
package com.eddc.job;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.os.WindowsUtils;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.eddc.model.Craw_monitor_url_info;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.job.JobAndTriggerService;
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：FillCatchData   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年9月9日 下午7:23:00   
 * 修改人：jack.zhao   
 * 修改时间：2018年9月9日 下午7:23:00   
 * 修改备注：   
 * @version    
 *    
 */
@Component
@Configuration
@EnableScheduling  //Springboot支持定时任务
public class FillCatchData{
	private static Logger logger = LoggerFactory.getLogger(FillCatchData.class); 
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private JobAndTriggerService jobAndTriggerService;
	/**
	 * @throws ExecutionException 
	 * @throws InterruptedException   
	 * @Title: MonitoringData  
	 * @Description: TODO(监控漏抓数据数据)  
	 * @param     设定文件  
	 * @return void    返回类型  
	 * @throws  
	 */  
//	@Scheduled(cron="30 20 */1 * * ?")
	public void MonitoringDataMessage() throws InterruptedException, ExecutionException{ 
		logger.info("开始监控数据漏抓情况------------------");//EDDC_TMALL_JOB_37
		String url="http://wechat.earlydata.com:9097/page/monitorFailure.html?CustomerMonitor=CustomerMonitor";
		try{
			webDriver(url);
		}catch(Exception e){
			webDriver(url);
		}

	} 
//	@Scheduled(cron="30 22 */1 * * ?")
	public void MonitoringDataMessageDriver() throws InterruptedException, ExecutionException{ 
		String url="http://wechat.earlydata.com:9097/page/monitorFailure.html?CustomerMonitor=CustomerMonitor";
		try{
			webDriver(url);
		}catch(Exception e){
			webDriver(url);
		}

	}
		
//	@Scheduled(cron="10 29 */1 * * ?")
	public void MonitoringDataMessageLeakageDriver() throws InterruptedException, ExecutionException{ 
		String url="http://wechat.earlydata.com:9070/page/monitorFailure.html?CustomerMonitor=CustomerMonitor";
		try{
			webDriver(url);
		}catch(Exception e){
			webDriver(url);
		}

	}
	public void  webDriver(String url) throws InterruptedException{
		if(!System.getProperty("os.name").equals("Linux")){//判断系统
			String CustomerMonitor=ResourceBundle.getBundle("docker").getString("CustomerMonitor");
			
			List<Craw_monitor_url_info>list=crawlerPublicClassService.monitoringLeakData(CustomerMonitor,CustomerMonitor);
			System.out.println("检查是否有漏抓商品，如果有进行补抓 漏抓商品共："+list.size()+"个");
			if(list!=null && list.size()>0){
				WindowsUtils.tryToKillByName("chromedriver.exe");
				WindowsUtils.tryToKillByName("chrome.exe");
				String user_Agent="Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36";
				System.setProperty("webdriver.chrome.driver","d:\\chromedriver.exe");	
				DesiredCapabilities cap= DesiredCapabilities.chrome();
				ChromeOptions options = new ChromeOptions();
				options.addArguments("user-agent="+user_Agent);
				Map<String, String> mobileEmulation = new HashMap<String, String>();
				mobileEmulation.put("deviceName", "Google Nexus 5");
				Map<String, Object> chromeOptions = new HashMap<String, Object>(); 
				chromeOptions.put("mobileEmulation", mobileEmulation);
				cap.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
				cap.setCapability(ChromeOptions.CAPABILITY, options);
				WebDriver driver = new ChromeDriver(cap);
				driver.manage().timeouts().pageLoadTimeout(500, TimeUnit.SECONDS);//-----页面加载时间
				driver.get(url);
				Thread.sleep(10000);
				try {
					for(int i=0;i<list.size();i++){
						Thread.sleep(3000);
						String jobName="EDDC_"+list.get(i).getPlatform_name_cn()+"_JOB_"+list.get(i).getCust_account_id()+"";
						List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName);
						boolean countData=crawlerPublicClassService.examineData(listjobName,CustomerMonitor);
						if(!countData){
							if(driver.findElement(By.id(jobName)).isDisplayed()==true){
								driver.findElement(By.id(jobName)).click();
							} 	
						}		
					}
				} catch (Exception e) {
					logger.error("点击任务出错了"+e);
				}finally{
					driver.close();
					driver.quit();
				}

			}else{
				logger.info("当前没有失败的job不需要补抓数据。。。。");
			}
		}
	}
}
