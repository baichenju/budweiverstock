/**   
 * Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2019年2月22日 上午10:11:49 
 */
package com.eddc.job;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.eddc.method.XohData;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.job.JobAndTriggerService;
import com.eddc.util.SimpleDate;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Xoh_job   
 * @author：jack.zhao       
 * 创建时间：2019年2月22日 上午10:11:49   
 * 类描述：新欧汇
 * @version    
 *    
 */
@SuppressWarnings("serial")
@Component
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class Xoh_job implements Job,Serializable{
	private static Logger log = LoggerFactory.getLogger(Xoh_job.class);
	@Autowired
	private JobAndTriggerService jobAndTriggerService;
	@Autowired
	private XohData xohData;
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		//synchronized(this) {
		System.gc();
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
		log.info("【"+jobName+"新欧汇 Job执行时间:{} 】", new Date());
		try {
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName);	
			xohData.crawlerCommodityAppDetails(listjobName,time);
		} catch (Exception e) {
			log.error("【获取JobAndTriggerService对象为空--error:{}----】",e);
		}
	}
}

