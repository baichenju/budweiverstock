/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2018年3月14日 下午12:20:51 
 */
package com.eddc.job;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;

import com.eddc.method.AllData;
import com.eddc.method.AmazonData;
import com.eddc.method.SearchData;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.job.JobAndTriggerService;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Pepsi_job   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年3月14日 下午12:20:51   
 * 修改人：jack.zhao   
 * 修改时间：2018年3月14日 下午12:20:51   
 * 修改备注：   
 * @version    
 *    
 */
@SuppressWarnings("serial")
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class Search_job  implements Job,Serializable  {
	private static Logger logger = LoggerFactory.getLogger(Search_job.class);
	@Autowired
	private JobAndTriggerService jobAndTriggerService;
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private SearchData searchData;
	@Autowired
	private AllData allData;
	public Search_job(){};
	@Override 
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		logger.info("【"+jobName+"__搜索Job执行时间: " + new Date()+"】");
		List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName);
		String time=SimpleDate.SimpleDateFormatData().format(new Date());
		if(listjobName.size()>0){ 
			//同步 搜索抓取商品的egoodsId
			//获取爬虫状态
			int sum=crawlerPublicClassService.crawlerStatus(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id()); 
			if(listjobName.size()>0){ 
				List<Craw_keywords_delivery_place>List_Code=crawlerPublicClassService.Keywords_Delivery_Place(listjobName,listjobName.get(0).getDatabases());
				if(List_Code.size()>0){
					for(Craw_keywords_delivery_place code:List_Code){
						try {
							if(String.valueOf(code.getSearch_status()).equals(Fields.STATUS_ON)){
								logger.info("【开始执行job搜索商品信息】"); 
								sum=searchData.crawlerPriceMonitoring(time,listjobName,code.getDelivery_place_code(),code.getDelivery_place_name());	
							}	
						} catch (Exception e) {
							 logger.error("【获取多个地区出错了 error:{}】",e);
						}
					}   
				}else{ 
					try {
						sum=searchData.crawlerPriceMonitoring(time,listjobName,null,null);
					} catch (InterruptedException | ExecutionException e) {
						e.printStackTrace();
					}
				}  
				if(sum==Fields.STATUS_11){
					allData.regionCrawler(listjobName, Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH, Fields.STATUS_OFF, Fields.STATUS_COUNT_1, 1,1);
				}
			}
		}
	} 
	
}
