/**   
 * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2017年11月23日 下午3:53:36 
 */
package com.eddc.job;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.crawl.CrawlerPublicClassService;
import com.eddc.service.impl.job.JobAndTriggerService;
import com.eddc.task.Jumei_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Jumei_Job
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2017年11月23日 下午3:53:36   
 * 修改人：jack.zhao   
 * 修改时间：2017年11月23日 下午3:53:36   
 * 修改备注：   
 * @version    
 *    
 */
@SuppressWarnings("serial")
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class Jumei_Job implements Job,Serializable {
	private static Logger logger = LoggerFactory.getLogger(Jumei_Job.class);
	private ApplicationContext contexts= SpringContextUtil.getApplicationContext();
	@Autowired
	private JobAndTriggerService jobAndTriggerService ;
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	private String regex=".*[a-zA-Z]+.*";  
	public Jumei_Job(){}
	@Override
	public void execute(JobExecutionContext context)throws JobExecutionException {
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		logger.info("【程序启动开始抓取聚美数据 "+jobName+"】");
		atartCrawlerJumei(jobName);

	}
	/**  
	 * @Title: atartCrawlerKaola  
	 * @Description: TODO(启动爬虫程序抓取聚美数据)  
	 * @param     设定文件  
	 * @return void    返回类型  
	 * @throws  
	 */  
	public void atartCrawlerJumei(String jobName){
		try {
			contexts= SpringContextUtil.getApplicationContext();
			jobAndTriggerService = (JobAndTriggerService) contexts.getBean(JobAndTriggerService.class);
			crawlerPublicClassService = (CrawlerPublicClassService) contexts.getBean(CrawlerPublicClassService.class);
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName);
			String time=SimpleDate.SimpleDateFormatData().format(new Date());  
			if(listjobName.size()>0){
				logger.info("【开始同步聚美数据当前用户是"+listjobName.get(0).getUser_Id()+"--------】");
				crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),Fields.TABLE_CRAW_KEYWORDS_INF);//同步数据
			
				crawlerJumeiPriceMonitoring(time,listjobName);
				if(listjobName.get(0).getClient().equals("ALL")|| listjobName.get(0).getClient().equals("all")){
					logger.info("【开始爬聚美PC端价格当前用户是"+listjobName.get(0).getUser_Id()+"--------】");
					TerminalCommodityPrices(time,listjobName);
				}
			}else{
				logger.info("【当前聚美用户不存在请检查爬虫状态是否关闭！爬虫结束------】");	
			}	
		} catch (Exception e) {
			logger.error("【获取JobAndTriggerService对象为空------ error：{}】",e);
		}
	}
	
	/**
	 * @throws ExecutionException 
	 * @throws InterruptedException   
	 * @Title: crawlerJumeiPriceMonitoring  
	 * @Description: TODO 开始解析聚美数据
	 * @param @param listjobName
	 * @param @param message    设定文件  
	 * @return void    返回类型  
	 * @throws  
	 */  
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void crawlerJumeiPriceMonitoring(String time,List<QrtzCrawlerTable>listjobName) throws InterruptedException, ExecutionException{
		int jj = 0;
		//ArrayList<Future> futureList = new ArrayList<Future>();
		List<Map<String,Object>>lsitData=new ArrayList<Map<String,Object>>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndParseInfo(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_ON,Fields.STATUS_COUNT);//查询爬虫url条数  
		ApplicationContext context = SpringContextUtil.getApplicationContext();
		for (CrawKeywordsInfo accountInfo : list) {  jj++;
		Jumei_DataCrawlTask dataCrawlTask = (Jumei_DataCrawlTask) context.getBean(Jumei_DataCrawlTask.class);
		Map<String,Object>jumei=new HashMap<String,Object>();
		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
		dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
		dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
		dataCrawlTask.setStorage(listjobName.get(0).getStorage());
		dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
		dataCrawlTask.setTimeDate(time);
		dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
		dataCrawlTask.setCrawKeywordsInfo(accountInfo);
		dataCrawlTask.setType(Fields.STYPE_1); 
		dataCrawlTask.setIp(listjobName.get(0).getIP());
		dataCrawlTask.setJumeiData(jumei);
		if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JUMEI)){
			Matcher m=Pattern.compile(regex).matcher(accountInfo.getCust_keyword_name());  
			if(m.matches()){
				dataCrawlTask.setSetUrl(Fields.JUMEI_APP_URL_2 + accountInfo.getCust_keyword_name()+ "&type=jumei_deal");
			}else{
				dataCrawlTask.setSetUrl(Fields.JUMEI_APP_URL + accountInfo.getCust_keyword_name()+"&type=jumei_mall");
			}
		}else if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
			Matcher m=Pattern.compile(regex).matcher(accountInfo.getCust_keyword_name());  
			if(m.matches()){
				dataCrawlTask.setSetUrl(Fields.JUMEI_APP_GLOBAL_URL_2 + accountInfo.getCust_keyword_name()+ "&type=global_deal"); 
			}else{
				dataCrawlTask.setSetUrl(Fields.JUMEI_APP_GLOBAL_URL + accountInfo.getCust_keyword_name()+ "&type=global_mall"); 
			}

		}
		//Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		//futureList.add(future);
		completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		}
		for(int i=0;i<list.size();i++){
			Map<String,Object>dataMap=(Map<String,Object>)completion.take().get();
			lsitData.add(dataMap);
		}
		taskExecutor.shutdownNow(); 
	    crawlerPublicClassService.bulkInsertCompletionData(lsitData,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据	
		/*while (true) {
			if (taskExecutor.isTerminated()) {
				taskExecutor.shutdownNow();  
				if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
					crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				}
				break;
			}
		}*/
		try {
			crawAndParseInfoAndPricefailure(time,listjobName);//检查是否有失败的商品
		} catch (Exception e) {
			logger.error("【解析失败信息失败 error:{}】",e);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void crawAndParseInfoAndPricefailure(String time,List<QrtzCrawlerTable>listjobName) throws InterruptedException, ExecutionException {
		int jj = 0; int count = 0;
		//ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		do {
			List<Map<String,Object>>lsitData=new ArrayList<Map<String,Object>>();
			List<CrawKeywordsInfo> list = crawlerPublicClassService.crawAndGrabFailureGoodsData(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_COUNT);//开始解析数据
			ApplicationContext context = SpringContextUtil.getApplicationContext();
			for (CrawKeywordsInfo accountInfo : list) {
				jj++;
				Jumei_DataCrawlTask dataCrawlTask = (Jumei_DataCrawlTask) context.getBean(Jumei_DataCrawlTask.class);
				Map<String,Object>jumei=new HashMap<String,Object>();
				Matcher m=Pattern.compile(regex).matcher(accountInfo.getCust_keyword_name());  
				dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
				dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
				 dataCrawlTask.setStorage(listjobName.get(0).getStorage());
				dataCrawlTask.setTimeDate(time);
				dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
				dataCrawlTask.setCrawKeywordsInfo(accountInfo);
				dataCrawlTask.setType(Fields.STYPE_1);
				dataCrawlTask.setJumeiData(jumei);
				dataCrawlTask.setIp(listjobName.get(0).getIP());
				dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
				dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
				if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JUMEI)){
					if(m.matches()){
						dataCrawlTask.setSetUrl(Fields.JUMEI_APP_URL_2 + accountInfo.getCust_keyword_name()+ "&type=jumei_deal");
					}else{
						dataCrawlTask.setSetUrl(Fields.JUMEI_APP_URL + accountInfo.getCust_keyword_name()+"&type=jumei_mall");
					}
				}else if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
					if(m.matches()){
						dataCrawlTask.setSetUrl(Fields.JUMEI_APP_GLOBAL_URL_2 + accountInfo.getCust_keyword_name()+ "&type=global_deal"); 
					}else{
						dataCrawlTask.setSetUrl(Fields.JUMEI_APP_GLOBAL_URL + accountInfo.getCust_keyword_name()+ "&type=global_mall"); 
					}

				} 
				//Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
				//futureList.add(future);
				//logger.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
				completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
			}
			for(int i=0;i<list.size();i++){
				Map<String,Object>dataMap=(Map<String,Object>)completion.take().get();
				lsitData.add(dataMap);
			}
			taskExecutor.shutdownNow(); 
		    crawlerPublicClassService.bulkInsertCompletionData(lsitData,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据	
			/*taskExecutor.shutdown();
			while (true) {
				if(((ExecutorService) taskExecutor).isTerminated()){
					taskExecutor.shutdownNow(); 
					if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
						crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
					}
					break;
				}
			}*/
			count++;
			jj = 0;
			if (count > 5 ||list!=null&&list.size()==0) {
				Thread.sleep(1000);
				return;
			}
		} while (true);
	}
	/**
	 * 获取PC商品价格
	 * @throws ExecutionException 
	 *
	 * @throws InterruptedException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void TerminalCommodityPrices(String time,List<QrtzCrawlerTable>listjobName) throws InterruptedException, ExecutionException {
		int ii = 0;
		long times=System.currentTimeMillis();
		//ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		List<Map<String,Object>>lsitData=new ArrayList<Map<String,Object>>();
		List<CommodityPrices> listPrice = crawlerPublicClassService.CommodityPricesData(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_COUNT);
		for (CommodityPrices accountInfo : listPrice) { ii++;
		Jumei_DataCrawlTask dataCrawlTask = (Jumei_DataCrawlTask) contexts.getBean(Jumei_DataCrawlTask.class);
		Map<String,Object>jumei=new HashMap<String,Object>();
		Matcher m=Pattern.compile(regex).matcher(accountInfo.getEgoodsId());  
		accountInfo.setPlatform_name_en(listjobName.get(0).getPlatform());
		dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
		dataCrawlTask.setStorage(listjobName.get(0).getStorage());
		dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
		accountInfo.setBatch_time(time);
		dataCrawlTask.setSum("" + ii + "/" + listPrice.size() + "");
		dataCrawlTask.setCommodityPrices(accountInfo);
		dataCrawlTask.setType(Fields.STYPE_6); 
		dataCrawlTask.setJumeiData(jumei);
		dataCrawlTask.setIp(listjobName.get(0).getIP());
		dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
		if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JUMEI)){
			if(m.matches()){
				dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_Promotion_2 + accountInfo.getEgoodsId()+ "&brand_id="+accountInfo.getPlatform_category()+"&site=sh&callback=jQuery111207574128594781935_1512112116418&_="+times+"");	//国内聚美特卖商品
			}else{
				dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_Promotion_1 + accountInfo.getEgoodsId()+"&brand_id="+accountInfo.getPlatform_category()+"&site=sh&callback=jQuery1112017022424268169556_1512110686059&_="+times+"");//国内普通商品
			}
		}else if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
			if(m.matches()){
				dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_GLOBAL_Promotion_4 + accountInfo.getEgoodsId()+ ".html");//海外购极速免税店可以获取价格促销
			}else {
				dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_GLOBAL_Promotion_3 + accountInfo.getEgoodsId()+ ".html");//海外购
			}
		} 
		//Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		//futureList.add(future);
		completion.submit(dataCrawlTask);
		} 
		for(int i=0;i<listPrice.size();i++){
			Map<String,Object>dataMap=(Map<String,Object>)completion.take().get();
			lsitData.add(dataMap);
		}
		taskExecutor.shutdownNow(); 
	    crawlerPublicClassService.bulkInsertCompletionData(lsitData,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据	
	    ii = 0;
		try {// 递归补漏
			Thread.sleep(20000);
			jumieRecursionFailureGoods(time,listjobName);
		} catch (Exception e) {
			logger.error("【聚美递归补漏失败+===========error:{}",e);
		}
	    /*	while (true) { 
			if(((ExecutorService) taskExecutor).isTerminated()){
				taskExecutor.shutdownNow(); 
				if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
					crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				}
				break;
			}
		}*/
		
	}

	/**
	 * 递归检查获取没有成功抓取商品的价格
	 *
	 * @throws InterruptedException
	 * @throws ExecutionException 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void jumieRecursionFailureGoods(String time,List<QrtzCrawlerTable>listjobName) throws InterruptedException, ExecutionException {
		int ii = 0; int count=0;
		do {
			//ArrayList<Future> futureList = new ArrayList<Future>();
			List<Map<String,Object>>lsitData=new ArrayList<Map<String,Object>>();
			ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
			CompletionService completion=new ExecutorCompletionService(taskExecutor);
			List<CommodityPrices> listjm = crawlerPublicClassService.RecursionFailureGoodsMessage(listjobName,listjobName.get(0).getDatabases(),Fields.CLIENT_PC,Fields.TABLE_CRAW_KEYWORDS_INF);
			if (listjm.size() > 0) {
				for (CommodityPrices accountInfo : listjm) {
					ii++;
					Jumei_DataCrawlTask dataCrawlTask = (Jumei_DataCrawlTask) contexts.getBean(Jumei_DataCrawlTask.class);
					Matcher m=Pattern.compile(regex).matcher(accountInfo.getEgoodsId());  
					Map<String,Object>jumei=new HashMap<String,Object>();
					dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
					dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
					dataCrawlTask.setStorage(listjobName.get(0).getStorage());
					accountInfo.setPlatform_name_en(listjobName.get(0).getPlatform());
					dataCrawlTask.setTimeDate(time);
					dataCrawlTask.setSum("" + ii + "/" + listjm.size() + "");
					accountInfo.setBatch_time(time);
					dataCrawlTask.setJumeiData(jumei);
					dataCrawlTask.setCommodityPrices(accountInfo);
					dataCrawlTask.setType(Fields.STYPE_6);
					dataCrawlTask.setIp(listjobName.get(0).getIP());
					dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
					dataCrawlTask.setSetUrl(Fields.MIYA_PC_URL + accountInfo.getEgoodsId() + ".html");
					dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
					if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JUMEI)){
						if(m.matches()){
							dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_Promotion_2 + accountInfo.getEgoodsId()+ "&brand_id="+accountInfo.getPlatform_category()+"&callback=jQuery111207574128594781935_1512112116418&_=1512112116419");	//国内聚美特卖商品
						}else{
							dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_Promotion_1 + accountInfo.getEgoodsId()+ "&brand_id="+accountInfo.getPlatform_category()+"&site=sh&callback=jQuery1112017022424268169556_1512110686059&_=1512110686060");//国内普通商品
						}
					}else if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
						if(m.matches()){
							dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_GLOBAL_Promotion_4 + accountInfo.getEgoodsId()+ ".html");//海外购极速免税店可以获取价格促销
						}else {
							dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_GLOBAL_Promotion_3 + accountInfo.getEgoodsId()+ ".html");//海外购
						}
					}
					//Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
					//futureList.add(future);
					//logger.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
					completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
				}
			}
			for(int i=0;i<listjm.size();i++){
				Map<String,Object>dataMap=(Map<String,Object>)completion.take().get();
				lsitData.add(dataMap);
			}
			taskExecutor.shutdownNow(); 
		    crawlerPublicClassService.bulkInsertCompletionData(lsitData,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据	
		/*	taskExecutor.shutdown();
			while (true) {
				if(((ExecutorService) taskExecutor).isTerminated()){
					taskExecutor.shutdownNow(); 
					if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
						crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
					}
					break;
				}
			}*/
			ii = 0;
			count++;
			if (count > 5 ||listjm!=null&&listjm.size()==0) {
				logger.info("【递归补漏 第"+count+"次】");
				Thread.sleep(5000); 
				return;
			}
		} while (true);	
	}
}
