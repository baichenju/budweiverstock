/**   
 * Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2019年10月16日 下午5:35:50 
 */
package com.eddc.job;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import com.eddc.method.CommentParticipleData;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.job.JobAndTriggerService;
import com.eddc.util.Fields;
import lombok.extern.slf4j.Slf4j;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：CommentParticiple_Job   
* @author：jack.zhao       
* 创建时间：2019年10月16日 下午5:35:50   
* 类描述：   
* @version    
*    
*/
@SuppressWarnings("serial")
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
@Slf4j
public class CommentParticiple_Job implements  Job,Serializable{
	@Autowired
	private JobAndTriggerService jobAndTriggerService;
	@Autowired
	private CommentParticipleData commentParticipleData;
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		log.info("【商品评论"+jobName+":Job执行时间:{} 】", new Date());
		System.gc();
		try {
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName);
			commentParticipleData.crawlerCommodityAppDetails(listjobName, Fields.TABLE_CRAW_KEYWORDS_INF, Fields.STATUS_ON);
		} catch (Exception e) {
			log.error("【获取JobAndTriggerService对象为空--error:{}----】",e);
		}
	}

}
