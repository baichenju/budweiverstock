/**   
 * Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2019年12月30日 上午10:00:38 
 */
package com.eddc.job;

import java.io.Serializable;
import java.util.List;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.eddc.method.PinDuoDuoData;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.impl.job.JobAndTriggerService;
import com.eddc.util.Fields;

import lombok.extern.slf4j.Slf4j;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：PinDuoDuo_job   
* @author：jack.zhao       
* 创建时间：2019年12月30日 上午10:00:38   
* 类描述：   
* @version    
*    
*/
@SuppressWarnings("serial")
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
@Slf4j
@Component
public class Pdd_job implements Job,Serializable{
	@Autowired
	private JobAndTriggerService jobAndTriggerService;
	@Autowired
	private PinDuoDuoData pinDuoDuoData;
	private String tableName=Fields.TABLE_CRAW_KEYWORDS_INF;//表名称
	private String counts=Fields.STATUS_ON;
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		log.info("==>>启动程序开始抓取拼多多详情数据<<==");
		int pageTop=0;
		try {
			if(jobName.contains(Fields.SEARCH)){
				tableName=Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH;
				counts=Fields.STATUS_OFF;
				pageTop=1;
			}
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName);	
			pinDuoDuoData.regionCrawler(listjobName,tableName,counts,pageTop,1);
		} catch (Exception e) {
			log.error("==>>请求拼多多数据发送异常:"+e+"<<===");
		}
		
	}

}
