package com.eddc.framework;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
/**
 * 动态数据源
 *
 * @create    2017年10月30日
 */
public class DynamicDataSource extends AbstractRoutingDataSource{

	@Override
	protected Object determineCurrentLookupKey() {
		 return DynamicDataSourceContextHolder.getDataSourceType();
	}

}
