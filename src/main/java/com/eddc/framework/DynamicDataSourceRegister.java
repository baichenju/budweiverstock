package com.eddc.framework;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotationMetadata;

import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.bind.RelaxedDataBinder;
import org.springframework.boot.bind.RelaxedPropertyResolver;
/**
 * 动态数据源注册<br/>
 * 启动动态数据源请在启动类中（如SpringBootSampleApplication）
 * 添加 @Import(DynamicDataSourceRegister.class)
 * @create 2017年10月30日
 */
@Slf4j
public class DynamicDataSourceRegister implements ImportBeanDefinitionRegistrar, EnvironmentAware {
	private static final Logger logger = LoggerFactory.getLogger(DynamicDataSourceRegister.class);
	private ConversionService conversionService = new DefaultConversionService(); 
	private PropertyValues dataSourcePropertyValues;
	// 如配置文件中未指定数据源类型，使用该默认值
	private static final Object DATASOURCE_TYPE_DEFAULT = "org.apache.tomcat.jdbc.pool.DataSource";
	// 数据源
	private DataSource defaultDataSource;
	private Map<String, DataSource> customDataSources = new HashMap<>();
	@Override
	public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
		Map<Object, Object> targetDataSources = new HashMap<Object, Object>();
		// 将主数据源添加到更多数据源中
		targetDataSources.put("dataSource", defaultDataSource);
		DynamicDataSourceContextHolder.dataSourceIds.add("dataSource");
		// 添加更多数据源
		targetDataSources.putAll(customDataSources);
		for (String key : customDataSources.keySet()) {
			DynamicDataSourceContextHolder.dataSourceIds.add(key);
		}

		// 创建DynamicDataSource
		GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
		beanDefinition.setBeanClass(DynamicDataSource.class);
		beanDefinition.setSynthetic(true);
		MutablePropertyValues mpv = beanDefinition.getPropertyValues();
		mpv.addPropertyValue("defaultTargetDataSource", defaultDataSource);
		mpv.addPropertyValue("targetDataSources", targetDataSources);
		registry.registerBeanDefinition("dataSource", beanDefinition);

		logger.info("Dynamic DataSource Registry");
	}

	/**
	 * 创建DataSource
	 *
	 * @param type
	 * @param driverClassName
	 * @param url
	 * @param username
	 * @param password
	 * @return
	 * @author SHANHY
	 * @create 2017年10月30日
	 */
	@SuppressWarnings({ "unchecked" })
	public DataSource buildDataSource(Map<String, Object> dsMap) {
		try {
			Object type = dsMap.get("type");
			if (type == null)
				type = DATASOURCE_TYPE_DEFAULT;// 默认DataSource

			Class<? extends DataSource> dataSourceType;
			dataSourceType = (Class<? extends DataSource>) Class.forName((String) type);

			String driverClassName = dsMap.get("driver-class-name").toString();
			String url = dsMap.get("url").toString();
			String username = dsMap.get("username").toString();
			String password = dsMap.get("password").toString();

			DataSourceBuilder factory = DataSourceBuilder.create().driverClassName(driverClassName).url(url)
					.username(username).password(password).type(dataSourceType);
			return factory.build();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 加载多数据源配置
	 */
	@Override
	public void setEnvironment(Environment env) {
		initCustomDataSources(env);
	}
	/**
	 * 初始化更多数据源
	 *
	 * @author SHANHY
	 * @create 2017年10月30日
	 */
	private void initCustomDataSources(Environment env) {
		// 读取配置文件获取更多数据源，也可以通过defaultDataSource读取数据库获取更多数据源
		RelaxedPropertyResolver propertyResolver = new RelaxedPropertyResolver(env, "spring.datasource.");
		String databases = propertyResolver.getProperty("db");
		JSONObject json = JSONObject.fromObject(databases);
		JSONArray array = json.getJSONArray("databaseNameList");
		for (int i = 0; i < array.size(); i++) {
			JSONObject object = JSONObject.fromObject(array.toArray()[i].toString());
			JSONArray databaseList = json.getJSONArray("databaseList");
			Map<String, Object> dsMap=new HashMap<String, Object>();
			
			for (int k = 0; k < databaseList.size(); k++) {
				
				JSONObject ject = JSONObject.fromObject(databaseList.toArray()[k].toString());
				
				if (object.getString("datasource").equalsIgnoreCase(ject.getString("datasource"))) {
					String url = String.format(ject.getString("url"), object.getString("dbIP"), object.getString("dbPort"), object.getString("dbName"));
					dsMap.put("driver-class-name", ject.getString("driverName"));
					dsMap.put("url", url);
					dsMap.put("username",object.getString("userName"));
					dsMap.put("password",object.getString("password"));
					dsMap.put("min-idle",json.getInt("min-idle"));
					dsMap.put("suspect-timeout",json.getInt("suspect-timeout"));
					dsMap.put("validationQuery",json.getString("validationQuery"));
					dsMap.put("secondary.max-wait",json.getInt("secondary.max-wait"));
					dsMap.put("maximum-pool-size",json.getInt("maximum-pool-size"));
					dsMap.put("poolPreparedStatements",json.getBoolean("poolPreparedStatements"));
					dsMap.put("max-idle",json.getInt("max-idle"));
					dsMap.put("maxPoolPreparedStatementPerConnectionSize",json.getInt("maxPoolPreparedStatementPerConnectionSize"));
					dsMap.put("testOnBorrow",json.getBoolean("testOnBorrow"));
					dsMap.put("testWhileIdle",json.getBoolean("testWhileIdle"));
					dsMap.put("minEvictableIdleTimeMillis",json.getInt("minEvictableIdleTimeMillis"));
					dsMap.put("timeBetweenEvictionRunsMillis",json.getInt("timeBetweenEvictionRunsMillis"));
					dsMap.put("testOnReturn",json.getBoolean("testOnReturn"));
					dsMap.put("validation-timeout",json.getInt("validation-timeout"));
					dsMap.put("removeAbandonedTimeout",json.getInt("removeAbandonedTimeout"));
					//datasourceName 为空 默认主数据库
					if(StringUtils.isBlank(object.getString("datasourceName"))) {
						dsMap.put("type", propertyResolver.getProperty("type"));
						defaultDataSource = buildDataSource(dsMap);
						//加载主数据源
						dataBinder(defaultDataSource, dsMap);
					}else {
						DataSource ds = buildDataSource(dsMap);
						customDataSources.put(object.getString("datasourceName"), ds);	
						//加载数据源
						dataBinder(ds, dsMap);
					}
					log.info("==>>数据库连接："+url+"<<==");
				}
			}
		}
	}


	private void dataBinder(DataSource dataSource, Map<String, Object> dsMap){
		RelaxedDataBinder dataBinder = new RelaxedDataBinder(dataSource);
		dataBinder.setConversionService(conversionService);
		dataBinder.setIgnoreNestedProperties(false);//false
		dataBinder.setIgnoreInvalidFields(false);//false
		dataBinder.setIgnoreUnknownFields(true);//true
		if(dataSourcePropertyValues == null){
			Map<String, Object> values = new HashMap<>(dsMap);
			// 排除已经设置的属性
			values.remove("type");
			values.remove("driver-class-name");
			values.remove("url");
			values.remove("username");
			values.remove("password");
			dataSourcePropertyValues = new MutablePropertyValues(values);
		}
		dataBinder.bind(dataSourcePropertyValues);
	}


}