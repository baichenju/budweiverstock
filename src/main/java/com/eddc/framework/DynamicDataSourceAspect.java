package com.eddc.framework;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
/**
 * 切换数据源Advice
 * @create 2017年10月30日
 */
@Aspect
@Component
public class DynamicDataSourceAspect {
	private static final Logger logger = LoggerFactory.getLogger(DynamicDataSourceAspect.class);
	@Before("@annotation(dataSource)")
//	@Before(value = "execution(* com.eddc.service.IpAddressAndPort_Service.*(..)) && @annotation(dataSource)")
	public void changeDataSource(JoinPoint point,TargetDataSource dataSource) throws Throwable {
		logger.debug("changeDataSource");
		Object[] args = point.getArgs();
		String dsId=args[1].toString().trim();
		//String dsId = dataSource.name();
		if (!DynamicDataSourceContextHolder.containsDataSource(dsId)) {
			logger.error("数据源[{}]不存在，使用默认数据源 > {}", dsId, point.getSignature());
		} else {
			//logger.debug("Use DataSource : {} > {}", dataSource.name(), point.getSignature());
			logger.debug("Use DataSource : {} > {}", dsId, point.getSignature());
			DynamicDataSourceContextHolder.setDataSourceType(dsId);
		}
	}
	@After("@annotation(dataSource)")
//	@After(value = "execution(* com.eddc.service.IpAddressAndPort_Service.*(..)) && @annotation(dataSource)")
	public void restoreDataSource(JoinPoint point, TargetDataSource dataSource) {
//		logger.debug("Revert DataSource : {} > {}", dataSource.name(), point.getSignature());
		DynamicDataSourceContextHolder.clearDataSourceType();
		logger.debug("restoreDataSource");
	}
}
