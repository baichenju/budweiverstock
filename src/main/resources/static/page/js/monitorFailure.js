$(function(){
	var thisURL = document.URL;
	var getval = thisURL.split('?')[1];
	var CustomerMonitor = getval.split("=")[1];
	selectProducts(CustomerMonitor);//获取查询信息
});
function selectProducts (CustomerMonitor){
	var url= '/job/monitorFailureQuery?CustomerMonitor='+CustomerMonitor;
	$("#example2").bootstrapTable('destroy');
	hotSaleTablePro=$('#example2').bootstrapTable({ 
		url: url,
		method: 'post',
		url: url,
		method: 'post',
		cache: false,////是否使用缓存，默认为true  
		striped: true, //是否显示行间隔色
		//search: true,  //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，
		//showRefresh: true,//是否显示刷新按钮
		//clickToSelect: true, //是否启用点击选中行 
		pagination: true,//是否显示分页（*）
		striped: true,//条纹
		pageSize: 10, //每页的记录行数（*）    
		pageList: [10],         //可供选择的每页的行数（*）  25, 50, 100
		sortOrder : 'desc',
		height:550,
		//showColumns: true, //是否显示所有的列
		//cardView: false, //是否显示详细视图
		//showToggle:true, //是否显示详细视图和列表视图的切换按钮
		columns: [//PLATFORM
		          {
		        	  field: 'platform_name_cn',
		        	  title: '平台',
		        	  align: 'center',
		        	  valign: 'middle',
		        	  sortable:true,
		        	  visible: true,
		        	  formatter:function(data,full,type,meta){
		        		  console.log(full);
		        		  return '<span class="textAuto"><font size=1>'+platformsData(data)+'</font></span>'; 
		        	  } 
		          },
		          {
		        	  field: 'cust_account_id',
		        	  title: '用户', 
		        	  align: 'center',
		        	  valign: 'middle',
		        	  sortable:true,
		        	  visible: true,
		        	  formatter:function(data, full, type, meta){
		        		  return "<font size=1>"+data+"</font>" 

		        	  }  
		          },
		          {    
		        	  field: 'cust_account_id',
		        	  title: '操作',
		        	  align: 'center', 
		        	  valign: 'middle',
		        	  sortable:true,
		        	  visible: true,
		        	  formatter:function(data, full, type, meta){
		        		  var jobName="";
		        		  if(data!=null||data!=undefined||data!=""){
		        			  jobName='EDDC_'+full.platform_name_cn+'_JOB_'+data+''; 
		        		  }
		        		  return "<button type=\"button\" class=\"btn btn-success\" style='padding:0px; font-size: 5px;' id="+jobName+" onclick=handleResume('"+jobName+"','"+full.platform_name_cn+"')><font size=2>补抓数据</font>" 

		        	  }   
		          }]
	});

} 

function handleResume(jboName,platform_name_cn){
	$.post('/job/crawlerMessageData',{"JOB_NAME":jboName,"PLATFORM":platform_name_cn,'COUNT':'2'},function (data){
		//if(data.success=1){
		//	alert('数据抓取完成!');	
		//}else{
		//	alert('数据抓取失败!');	  
		//}
		$("#example2").bootstrapTable('refresh');
	});
}