$(function(){
	selectProducts();//获取查询信息
	setInterval("refreshPage()",40000);   
});
function selectProducts (){
		var url= '/crawler/query_rawler_fill_catch';
		$("#example2").bootstrapTable('destroy');
		hotSaleTablePro=$('#example2').bootstrapTable({ 
			url: url,
			method: 'post',
			cache: false,////是否使用缓存，默认为true  
			striped: true, //是否显示行间隔色
			search: true,  //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，
			showRefresh: true,//是否显示刷新按钮
			clickToSelect: true, //是否启用点击选中行 
			pagination: true,//是否显示分页（*）
			striped: true,//条纹
			pageSize: 10, //每页的记录行数（*）    
			pageList: [10],         //可供选择的每页的行数（*）  25, 50, 100
			sortOrder : 'desc',
			height:750,
			columns: [//PLATFORM
			          {
			        	  field: 'platform',
			        	  title: '平台',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  formatter:function(data,full,type,meta){
			                return  platformsData(data); 
			        	  }
			          },
			          {
			        	  field: 'user_Id',
			        	  title: '用户', 
			        	  align: 'center',
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true 
			          },
			         {
			        	  field: 'job_name',
			        	  title: 'Job名称',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true
			          },
			          
			          {
			        	  field: 'dataSource',
			        	  title: '代理IP库', 
			        	  align: 'center',
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  editable:{ 
			        		  type: 'text',
			        		  title: '代理IP库'
			                 }
			          },
			        
			          {
			        	  field: 'thread_sum',
			        	  title: '线程', 
			        	  align: 'center', 
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  editable:{ 
			        		  type: 'text',
			        		  title: '线程'
			                 }
			          },
			          {
			        	  field: 'number',
			        	  title: '补抓商品', 
			        	  align: 'center', 
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  formatter:function (data, full, type, meta){
			        		  if(data==0){
			        			  return '<span class="label label-success">正常</span>';  
			        		  }else{
			        			  return '<span class="label label-danger">'+data+'</span>';
			        		  }
			        		
			        	  }
			          }, 
			          {    
			        	  field: 'SCHED_NAME',
			        	  title: '操作',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  
			        	  formatter:function(data, full, type, meta){//replace(/["""]/g,"")
			        		  var time=full.data_time.replace(/\ +/g,",");
			        		  console.log(time);
			        		  return "<span style='text-align: right;'\><button type=\"button\" class=\"btn btn-success\" style='padding:0px; font-size: 13px;' onclick=handleResume('"+full.platform+"','"+full.user_Id+"','"+full.dataSource+"','"+full.thread_sum+"','"+time+"')>点击补抓</button>&nbsp;&nbsp;" 
			        		          
			        	  }   
			          } 

			          ],
			          onEditableSave: function (field, row, oldValue, $el) { 
			        	 // alert($(".popover-title").html()); 
			        	   var url='';
			        	   if($(".popover-title").html()=='代理IP库'){
			        		  url='/job/reschedule_job_datasource?JOB_NAME='+row.job_name+'&DATASOURCE='+row.dataSource;
			        	  }else if($(".popover-title").html()=='线程'){
			        		  url='/job/reschedule_job_thread?JOB_NAME='+row.job_name+'&THREAD_SUM='+row.thread_sum+'&PLATFORM='+row.platform;
			        	  }
			              $.ajax({ 
			            	  type: "post",
			            	  url: url,
			                  success: function (data, status) {
			                	  console.log(data); console.log(status);  
			                      if (status == "success") {    
			                    	  $("#example2").bootstrapTable('refresh'); 
			                      }
			                  },
			                  error: function () {
			                      alert("Error");
			                  },
			                  complete: function () {

			                  } 
			              });
			          }
		});
}
//启动爬虫
function handleResume(platform ,user_Id ,dataSource,thread_sum,data_time){
	$.post('/crawler/crawler_failure_fill',{"platform":platform,"user_Id":user_Id,"dataSource":dataSource,"thread_sum":thread_sum,"data_time":data_time},function (data){
		if(data.success=1){
			alert('补抓数据完成请检查数据');	
		}else{
			alert('补抓数据失败!');	
		}
		$("#example2").bootstrapTable('refresh');
	});
	
}
function refreshPage(){
	var data = { url: "/crawler/query_rawler_fill_catch", silent: true, };
	$('#example2').bootstrapTable('refresh',data);	
	$("#example2").bootstrapTable('refresh');
}
