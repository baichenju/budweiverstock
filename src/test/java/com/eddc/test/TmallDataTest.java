package com.eddc.test;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Craw_goods_pic_Info;
import com.eddc.model.Parameter;
import com.eddc.util.Fields;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

public class TmallDataTest {
	
	
	
	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void getItemPrice() throws IOException {
		String StringMessage = FileUtils.readFileToString(new File("d:/aa.html"));
		JSONObject json=JSONObject.parseObject(StringMessage);
		//送货地址
		String deliveryAddress=json.getJSONObject("defaultModel").getJSONObject("deliveryDO").getString("deliveryAddress");
		//目的地
		String destination=json.getJSONObject("defaultModel").getJSONObject("deliveryDO").getString("destination");
		//库存
		String totalQuantity=json.getJSONObject("defaultModel").getJSONObject("inventoryDO").getString("icTotalQuantity");
		//运费
		JSONArray freightList=json.getJSONObject("defaultModel").getJSONObject("deliveryDO").getJSONObject("deliverySkuMap").getJSONArray("default");
		if(freightList!=null&& freightList.size()>0) {
			JSONObject object=JSONObject.parseObject(freightList.toArray()[0].toString());
			String freight=object.getString("money");
			if(freight.contains("0.00")) {
				freight="免费";
			}
		}
		//判断商品的sku库存
		JSONObject dataJson=JSONObject.parseObject(json.getJSONObject("defaultModel").getJSONObject("inventoryDO").getString("skuQuantity"));
		if(dataJson == null || dataJson.isEmpty()  || "null".equals(dataJson)){		
			System.out.println("当前商品没有sku库存商品");
			//商品现价
			JSONArray arrayPrice=json.getJSONObject("defaultModel").getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").getJSONObject("def").getJSONArray("promotionList");
			if(arrayPrice!=null && arrayPrice.size()>0) {
				JSONObject jsPrice=JSONObject.parseObject(arrayPrice.toArray()[0].toString());
				String currentPrice=jsPrice.getString("price");
				System.out.println("商品现价:"+currentPrice);
			}
			//商品原价
			String originalPrice=json.getJSONObject("defaultModel").getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").getJSONObject("def").getString("price");
			System.out.println("商品原价:"+originalPrice);
		}else {
			//库存
			String skuQuantity=json.getJSONObject("defaultModel").getJSONObject("inventoryDO").getJSONObject("skuQuantity").getJSONObject("skuId").getString("totalQuantity");
			
			//商品现价
			JSONArray arrayPrice=json.getJSONObject("defaultModel").getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").getJSONObject("skuId").getJSONArray("promotionList");
			if(arrayPrice!=null && arrayPrice.size()>0) {
				JSONObject jsPrice=JSONObject.parseObject(arrayPrice.toArray()[0].toString());
				String currentPrice=jsPrice.getString("price");
				System.out.println("商品现价:"+currentPrice);
			}
			//商品原价
			String originalPrice=json.getJSONObject("defaultModel").getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").getJSONObject("skuId").getString("price");
		}	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	@Test
	public void getTmall() throws IOException {
		Map<String, Object> map = new HashMap<String, Object>(10);
		//价格
		List<Map<String, Object>> insertPriceList = Lists.newArrayList();
		//详情
		List<Map<String, Object>> insertItemList = Lists.newArrayList();
		//图片
		List<Map<String, Object>> insertImage = Lists.newArrayList();
		List<Map<String, Object>> commentList = Lists.newArrayList();
		//商品规格属性
		List<Map<String, Object>> attributeList = Lists.newArrayList();

		StringBuffer promotion = new StringBuffer();
		String StringMessage = FileUtils.readFileToString(new File("d:/aa.html"));
		List<Craw_goods_Price_Info> priceList = new ArrayList<Craw_goods_Price_Info>(10);
		Document doc = Jsoup.parseBodyFragment(StringMessage);
		String productName = doc.getElementsByClass("main cell").html();
		String shop_name = doc.getElementsByClass("shop-name").html();
		int inventoryCount = 0;
		Element ment=doc.getElementById("J_ImgBooth");
		ArrayList<Element> element = doc.select("script");
		if(ment!=null) {
			productName=doc.getElementById("J_ImgBooth").attr("alt");
			String productImage="http:"+doc.getElementById("J_ImgBooth").attr("src");
			String productStore=doc.getElementsByClass("slogo-shopname").text();

			for (int i = 0; i < element.size(); i++) {

				if (element.get(i).data().toString().contains("TShop.Setup")) {
					// System.out.println(element.get(i).data().toString()
					String skuMessage=element.get(i).data().toString();
					String shopJson="TShop.Setup(";
					String  productSku=skuMessage.substring(skuMessage.indexOf(shopJson)+shopJson.length(),skuMessage.lastIndexOf(" );"));
					JSONObject json=JSONObject.parseObject(productSku);
					String priceUrl="http:"+json.getString("changeLocationApi");
					System.out.println(priceUrl);
					String city=json.getJSONObject("itemDO").getString("prov");
					String userId=json.getJSONObject("itemDO").getString("userId");
					String rootCatId=json.getJSONObject("itemDO").getString("rootCatId");
					String categoryId=json.getJSONObject("itemDO").getString("categoryId");
					String shopId=json.getString("rstShopId");
					if(json.containsKey("valItemInfo")) {
						JSONArray array=json.getJSONObject("valItemInfo").getJSONArray("skuList");
						for(int kk=0;kk<array.size();kk++) {
							JSONObject object=JSONObject.parseObject(array.toArray()[kk].toString());
							System.out.println("names:"+object.getString("names"));
							System.out.println("skuId:"+object.getString("skuId"));
							System.out.println("pvs:"+object.getString("pvs"));
							String pvs=object.getString("pvs");
							String originalPrice=json.getJSONObject("valItemInfo").getJSONObject("skuMap").getJSONObject(";"+pvs+";").getString("price");
							String stock=json.getJSONObject("valItemInfo").getJSONObject("skuMap").getJSONObject(";"+pvs+";").getString("stock");
							System.out.println(originalPrice);
							System.out.println(stock);
							System.out.println("========================================================================");
						} 
					}

					//商品图片

					try {
						/**获取商品小图片*/
						if (StringUtil.isNotEmpty(json.getString("propertyPics"))) {
							JSONArray jsonArray=json.getJSONObject("propertyPics").getJSONArray("default");
							for (int ii = 0; ii < jsonArray.size(); ii++) {
								try {
									String picturl="";
									if (ii == 0) {
										picturl = "http:" + jsonArray.getString(ii);
									}
									System.out.println("http:"+jsonArray.getString(ii)); 
									//判断是否抓取商品小图片
									//if (parameter.getListjobName().get(0).getDescribe().contains("抓取商品小图片")) {
									//Craw_goods_pic_Info info = getImageProduct(parameter, "http:" + jsonArray.getString(ii), ii);
									/*封装数据 商品图片情插入**/
									//insertImage.addAll(list(info));

									//}
								} catch (Exception e) {
									//logger.error("==>>解析商品小图片失败 ，error:{}<<==", e);
								}

							}
						}
					} catch (Exception e) {
						//logger.error("==>>天猫获取商品小图片失败 error:{}<<==", e);
					}
				}
			}
			//商品属性
			Element attr=doc.getElementById("J_AttrUL");
			if(attr!=null) {
				List<Element>list=attr.select("li");
				String colon=":";
				for(int i=1;i<list.size();i++) {
					try {
						char[] chars = list.get(i).text().toCharArray();
						 for (char aChar : chars) {
							if(isChinesePunctuation(aChar)) {
								 colon="：";
								 break;
							}else {
								colon=":";
							}
						}
						String[] attrSprit=list.get(i).text().replace(" ","").split(colon);
						System.out.println("Key:"+attrSprit[0]+"Value:"+attrSprit[1] );
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			
			
			
		}





		//判断商品是否存在
		//        if (StringUtils.isNotBlank(productName)) {
		//            //FileUtils.writeStringToFile(new File("D://tmall//"+parameter.getEgoodsId()+".html"),StringMessage,"utf-8");
		//            ArrayList<Element> element = doc.select("script");
		//            for (int i = 0; i < element.size(); i++) {
		//
		//                if (element.get(i).data().toString().contains("var _DATA_Detail = ")) {
		//                    String[] elScriptList = element.get(i).data().toString().split("var _DATA_Detail = ");
		//                    String itemMessage = elScriptList[1].replace(";", "").trim();
		//
		//                    //获取商品库存
		//                    String[] dataDetail = element.get(i + 1).data().toString().split("var _DATA_Mdskip = ");
		//                    String inventory = dataDetail[1].trim();
		//
		//                    JSONObject inventoryMessage = JSONObject.parseObject(inventory);
		//                    JSONObject json = JSONObject.parseObject(itemMessage);
		//                    JSONObject delivery = JSONObject.parseObject(inventoryMessage.getString("delivery"));
		//                    //商品地址
		//                    if (delivery != null) {
		//                        if (delivery.containsKey("completedTo")) {
		//                            String completedTo = delivery.getString("completedTo");
		//                            // infovo.setDelivery_place(completedTo);
		//                        } else if (delivery.containsKey("from")) {
		//                            String from = delivery.getString("from");
		//                            //  infovo.setSeller_location(from);
		//                        } else if (delivery.containsKey("to")) {
		//                            System.out.println(delivery.getString("to"));
		//                        }
		//                    }
		//
		//
		//                    if (delivery != null) {
		//                        if (delivery.containsKey("postage")) {
		//                            //运费
		//                            String postage = delivery.getString("postage");
		//                            //infovo.setDelivery_info(postage);
		//                        }
		//                    }
		//                    if (StringUtils.isEmpty(productName)) {
		//                        //商品名称
		//                        productName = json.getJSONObject("item").getString("title");
		//                    }
		//                    if (StringUtils.isEmpty(shop_name)) {
		//                        //店铺名称
		//                        shop_name = json.getJSONObject("seller").getString("shopName");
		//                    }
		//                    //店铺Id
		//                    if (json.getJSONObject("seller").containsKey("shopId")) {
		//                        System.out.println(json.getJSONObject("seller").getString("shopId"));
		//                    }
		//
		//                    //用户Id
		//                    String userId = json.getJSONObject("seller").getString("userId");
		//                    //行业Id
		//                    String categoryId = json.getJSONObject("item").getString("categoryId");
		//                    //评论数
		//                    JSONObject rate = JSONObject.parseObject(json.getString("json"));
		//                    if (rate != null) {
		//                        if (rate.containsKey("totalCount")) {
		//                            String commentCount = rate.getString("totalCount");
		//                            System.out.println(commentCount);
		//                        }
		//                    }
		//
		//                    //判断商品小图是否存在
		//                    if (StringUtil.isNotEmpty(json.getJSONObject("item").get("images").toString())) {
		//                        JSONArray jsonArray = JSONArray.parseArray(json.getJSONObject("item").getString("images"));
		//                        for (int ii = 0; ii < jsonArray.size(); ii++) {
		//                            try {
		//                                if (ii == 0) {
		//                                    String productImage = "http:" + jsonArray.getString(ii);
		//                                    System.out.println(productImage);
		//                                }
		//
		//                            } catch (Exception e) {
		//                                System.out.println("==>>解析商品小图片失败 ，error:{}<<==" + e.getMessage());
		//                            }
		//
		//                        }
		//                    }
		//                    //获取商品价格
		//                    if (json.getJSONObject("skuBase").containsKey("props")) {
		//                        //促销
		//                        JSONObject priceJson = JSONObject.parseObject(inventoryMessage.getString("price"));
		//                        if (priceJson != null) {
		//                            if (priceJson.containsKey("shopProm")) {
		//                                if (!priceJson.toString().contains("\"shopProm\":null")) {
		//                                    JSONArray shopPromList = JSONArray.parseArray(priceJson.getJSONArray("shopProm").toString());
		//                                    for (int w = 0; w < shopPromList.size(); w++) {
		//                                        JSONObject PromMessage = JSONObject.parseObject(shopPromList.toArray()[w].toString());
		//                                        String prom = PromMessage.getString("content").replace("[", "").replace("]", "").replace("\"", "");
		//                                        if (shopPromList.size() == w + 1) {
		//                                            promotion.append(prom);
		//                                        } else {
		//                                            promotion.append(prom + "&&");
		//                                        }
		//                                    }
		//                                }
		//                            }
		//                        }
		//                    }
		//                    //					//月销量
		//                    //					String sellcount=productSellCount(parameter);
		//                    //					infovo.setSale_qty(sellcount);//销量
		//                    //请求促销
		//                    //					try {
		//                    //						parameter.setItemUrl_1("https://h5api.m.tmall.hk/h5/mtop.tmall.detail.couponpage/1.0/?jsv=2.4.8&appKey=12574478&t="+System.currentTimeMillis()+"&sign=06b2458db3e283accd3a177c2cd1acbb&api=mtop.tmall.detail.couponpage&v=1.0&ttid=tmalldetail&type=jsonp&dataType=jsonp&callback=mtopjsonp5&data=%7B%22itemId%22%3A"+parameter.getEgoodsId()+"%2C%22source%22%3A%22tmallH5%22%7D");
		//                    //						String  Stringpromotion=httpClientService.interfaceSwitch(parameter);//请求数据
		//                    //						if(Stringpromotion.contains("coupons")) {
		//                    //							String strs=Stringpromotion.substring(Stringpromotion.indexOf("{"), Stringpromotion.lastIndexOf("}")+1);
		//                    //							JSONObject PromMessage = JSONObject.fromObject(strs);
		//                    //							JSONArray couponList = JSONArray.fromObject(PromMessage.getJSONObject("data").getJSONArray("coupons"));
		//                    //							for(int ii=0;ii<couponList.size();ii++) {
		//                    //								JSONObject promJson = JSONObject.fromObject(couponList.toArray()[ii].toString());
		//                    //								JSONArray couponListMessage = JSONArray.fromObject(promJson.getJSONArray("couponList"));
		//                    //								for(int ss=0;ss<couponList.size();ss++) {
		//                    //									JSONObject promObject = JSONObject.fromObject(couponListMessage.toArray()[ss].toString());
		//                    //									promotion.append(promObject.getString("subtitles").replace("[", "").replace("[", "").replace("\"", "")+"&&");
		//                    //								}
		//                    //
		//                    //							}
		//                    //						}
		//                    //					} catch (Exception e1) {
		//                    //
		//                    //						e1.printStackTrace();
		//                    //					}
		//                    //
		//
		//                    try {
		//                        JSONObject resource = JSONObject.parseObject(inventoryMessage.getString("resource"));
		//                        if (resource != null) {
		//                            if (resource.getJSONObject("coupon").containsKey("couponList")) {
		//
		//                                JSONArray couponList = JSONArray.parseArray(resource.getJSONObject("coupon").getJSONArray("couponList").toString());
		//                                for (int cc = 0; cc < couponList.size(); cc++) {
		//                                    JSONObject PromMessage = JSONObject.parseObject(couponList.toArray()[cc].toString());
		//                                    String prom = PromMessage.getString("title");
		//                                    promotion.append(prom + "&&");
		//                                }
		//                            }
		//                        }
		//
		//                    } catch (Exception e1) {
		//                        System.out.println("==>>获取商品促销失败error:{}<<==" + e1.getMessage());
		//                    }
		//                    //解析价格
		//                   // if (json.getString("skuBase").contains("price")) {
		//                        if (!json.getJSONObject("skuBase").toString().contains("\"props\":null")) {
		//                            JSONArray skuBaseList = JSONArray.parseArray(json.getJSONObject("skuBase").getJSONArray("props").toString());
		//                            if (skuBaseList.size() == 1) {
		//                                JSONObject valuesprops = JSONObject.parseObject(skuBaseList.toArray()[0].toString());
		//                                JSONArray valuespropsList = JSONArray.parseArray(valuesprops.getJSONArray("values").toString());
		//                                for (int kk = 0; kk < valuespropsList.size(); kk++) {
		//                                    JSONObject valuespropsNameList = JSONObject.parseObject(valuespropsList.toArray()[kk].toString());
		//                                    String productSkuName = valuespropsNameList.getString("name");
		//
		//                                    JSONArray skuIdList = JSONArray.parseArray(json.getJSONObject("skuBase").getJSONArray("skus").toString());
		//                                    JSONObject productSkuId = JSONObject.parseObject(skuIdList.toArray()[kk].toString());
		//                                    String productSkuMessage = productSkuId.getString("skuId");
		//
		//                                    String originalPrice = json.getJSONObject("mock").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getJSONObject("price").getString("priceText");
		//                                    String productPrice = inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getJSONObject("price").getString("priceText");
		//                                    String inventorys = inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getString("quantity");
		//                                    if (StringUtils.isNotBlank(inventorys)) {
		//                                        inventoryCount = +Integer.valueOf(inventorys);
		//                                    }
		//                                    if (StringUtils.isBlank(originalPrice)) {
		//                                        originalPrice = productPrice;
		//                                    }
		//
		//                                }
		//                            } else {
		//                                JSONObject skuBase = JSONObject.parseObject(skuBaseList.toArray()[1].toString());
		//                                JSONArray valuesList = JSONArray.parseArray(skuBase.getJSONArray("values").toString());
		//                                for (int s = 0; s < valuesList.size(); s++) {
		//                                    //颜色分类
		//                                    JSONObject skuName = JSONObject.parseObject(valuesList.toArray()[s].toString());
		//                                    String name = skuName.getString("name");
		//                                    //尺码
		//                                    JSONObject skuType = JSONObject.parseObject(skuBaseList.toArray()[0].toString());
		//                                    String skuTypeName = skuType.getString("name");
		//
		//                                    JSONArray valuespropsList = skuType.getJSONArray("values");
		//                                    for (int kk = 0; kk < valuespropsList.size(); kk++) {
		//                                        JSONObject valuespropsNameList = JSONObject.parseObject(valuespropsList.toArray()[kk].toString());
		//                                        String productSkuName = name + "," + skuTypeName + "," + valuespropsNameList.getString("name");
		//                                        /**商品价格*/
		//                                        JSONArray skuIdList = json.getJSONObject("skuBase").getJSONArray("skus");
		//                                        JSONObject productSkuId = JSONObject.parseObject(skuIdList.toArray()[kk].toString());
		//                                        String productSkuMessage = productSkuId.getString("skuId");
		//
		//
		//                                        String originalPrice = json.getJSONObject("mock").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getJSONObject("price").getString("priceText");
		//                                        String productPrice = inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getJSONObject("price").getString("priceText");
		//
		//                                        String inventorys = inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getString("quantity");
		//                                        if (StringUtils.isNotBlank(inventorys)) {
		//                                            inventoryCount = +Integer.valueOf(inventorys);
		//                                        }
		//                                        if (StringUtils.isBlank(originalPrice)) {
		//                                            originalPrice = productPrice;
		//                                        }
		//
		//                                    }
		//
		//                                }
		//
		//                            }
		//
		//                        } else {
		//                            //获取商品价格
		//
		//                            if (inventoryMessage.getJSONObject("price").containsKey("extraPrices")) {
		//                                if (!inventoryMessage.getJSONObject("price").toString().contains("\"extraPrices\":null")) {
		//                                    //现价
		//                                    String currentPrice = inventoryMessage.getJSONObject("price").getJSONObject("price").getString("priceText");
		//                                    // priceInfo.setCurrent_price(currentPrice);
		//
		//                                    //原价
		//                                    JSONArray priceListData = inventoryMessage.getJSONObject("price").getJSONArray("extraPrices");
		//                                    for (int y = 0; y < priceListData.size(); y++) {
		//                                        JSONObject priceMessage = JSONObject.parseObject(priceListData.toArray()[y].toString());
		//                                        String originalPrice = priceMessage.getString("priceText");
		//                                        // priceInfo.setOriginal_price(originalPrice);
		//                                    }
		//                                }
		//                            } else {
		//                                String originalPrice = inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject("0").getJSONObject("price").getString("priceText");
		//                                String currentPrice = inventoryMessage.getJSONObject("price").getJSONObject("price").getString("priceText");
		//                                //priceInfo.setOriginal_price(originalPrice);
		//                                //priceInfo.setCurrent_price(currentPrice);
		//
		//                            }
		//
		//
		//                            //库存
		//                            String quantity = inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject("0").getString("quantity");
		//                            if (StringUtils.isNotBlank(quantity)) {
		//                                inventoryCount = +Integer.valueOf(quantity);
		//                            }
		//
		//
		//                        }
		//                    //} else {
		//
		//                        String originalPrice = json.getJSONObject("mock").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject("0").getJSONObject("price").getString("priceText");
		//
		//                    //}
		//
		//                    /**商品属性*/
		//                    if (json.containsKey("props")) {
		//                        if (!json.getJSONObject("props").toString().contains("\"propsList\":null")) {
		//                            if (json.getJSONObject("props").toString().contains("propsList")) {
		//                                JSONArray commodityPropertyList = JSONArray.parseArray(json.getJSONObject("props").getJSONArray("propsList").toString());
		//
		//                                for (int g = 0; g < commodityPropertyList.size(); g++) {
		//                                    JSONObject commodityPropert = JSONObject.parseObject(commodityPropertyList.toArray()[g].toString());
		//                                    JSONArray groupPropsList = commodityPropert.getJSONArray("baseProps");
		//                                    for (int e = 0; e < groupPropsList.size(); e++) {
		//                                        JSONObject object = JSONObject.parseObject(groupPropsList.toArray()[e].toString());
		//                                        String key = object.getString("key");
		//                                        String value = object.getString("value");
		//
		//                                        /**封装数据 商品属性**/
		//
		//                                    }
		//                                }
		//                            }
		//                        }
		//                        if (json.getJSONObject("props").containsKey("groupProps")) {
		//                            JSONArray commodityPropertyList = json.getJSONObject("props").getJSONArray("groupProps");
		//                            for (int g = 0; g < commodityPropertyList.size(); g++) {
		//                                JSONObject commodityPropert = JSONObject.parseObject(commodityPropertyList.toArray()[g].toString());
		//                                JSONArray groupPropsList = commodityPropert.getJSONArray("基本信息");
		//                                for (int e = 0; e < groupPropsList.size(); e++) {
		//                                    JSONObject object = JSONObject.parseObject(groupPropsList.toArray()[e].toString());
		//                                    Set<Map.Entry<String, Object>> setdata = object.entrySet();
		//                                    //商品属性
		//                                    for (Map.Entry<String, Object> entry : setdata) {
		//
		//                                        String key = entry.getKey().toString();
		//                                        String value = entry.getValue().toString();
		//
		//                                        /*封装数据 商品属性**/
		//                                        //attributeList.addAll(list(attributeInfo(key, value, parameter)));
		//                                    }
		//                                }
		//                            }
		//                        }
		//                    }
		//                }
		//
		//            }
		//            System.out.println(String.valueOf(inventoryCount));
		//        }
		//
	}
	// 根据UnicodeBlock方法判断中文标点符号
	public boolean isChinesePunctuation(char c) {
		Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
		if (ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
				|| ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
				|| ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
				|| ub == Character.UnicodeBlock.CJK_COMPATIBILITY_FORMS
				|| ub == Character.UnicodeBlock.VERTICAL_FORMS) {
			return true;
		} else {
			return false;
		}
	}

}
