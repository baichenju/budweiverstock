package com.eddc.test;

import com.eddc.util.JsoupUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.FileOutputStream;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * @描述
 * @参数 $
 * @返回值 $
 * @创建人 jack.zhao
 * @创建时间 $
 * @修改人和其它信息
 */
//@Slf4j
public class JsoupTest {
    @Test
    public void alibaba() throws Exception {
     
        String url = "https://www.bio-equip.com/more.asp?keyword=&page=3";
        Connection conn = Jsoup.connect(url).timeout(600000).ignoreContentType(true).maxBodySize(0);
        conn.header("Accept", "*/*");
        conn.header("Accept-Charset", "gb2312,*;q=0.5");
        conn.header("Accept-Encoding", "gzip, deflate, sdch");
        conn.header("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
        conn.header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        conn.header("Cache-Control", "no-cache");
        conn.header("Connection", "keep-alive");
        conn.header("Pragma", "no-cache");
        conn.header("Host", "www.jiuxian.com");
       // conn.header("Cookie", "PTOKEN=C7EF0277945EB26DADFDFE3F0120D6D0 ; user_province=11 ; RandomTest=0.49331934682995304 ; __jsluid_h=92e7db6ac81e866759bd537cf512a70c ; OZ_SI_1722=sTime=1578035221&sIndex=1 ; __jsl_clearance=1578035216.537|0|NKHrUUAypIFlwXn9ggTAsKkoz%2BQ%3D ; OZ_RU_1722=0 ; _jzqb=1.3.10.1578035221.1 ; _jzqc=1 ; _qzjc=1 ; _qzjb=1.1578035221544.1.0.0.0 ; NTKF_T2D_CLIENTID=guest55582CCB-7BE9-0D99-E5A4-6A3A94A94276 ; nTalk_CACHE_DATA={uid:jx_1000_ISME9754_guest55582CCB-7BE9-0D,tid:1578035221672767} ; _qzjto=1.1.0 ; _qzja=1.49175504.1578035221544.1578035221544.1578035221544.1578035221544.1578035221544.0.0.0.1.1 ; _jzqa=1.3321356435321805000.1578035221.1578035221.1578035221.1 ; JX_TID=6d3fcabf-92c1-19fc-b7c3-a30547b83dfe ; JX_SID=5da4d58b-55c2-170f-8024-da38b22eddcb ; _jzqckmp=1 ; _jzqx=1.1578035221.1578035221.1.jzqsr=jiuxian%2Ecom|jzqct=/goods-91272%2Ehtml.- ; OZ_1U_1722=vid=ve0ee8155b48b5.0&ctime=1578035221&ltime=0 ; SearchUserKey=9d5f1bc0-04a7-11dd-8729-376098dbd1d3 ; viewhis=%5B%7B%22id%22%3A%2291272%22%2C%22k%22%3A0%7D%5D");
        conn.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36");
        conn.header("Referer", "https://res.m.suning.com/project/coupon/dist/center.html");

        try {
			Connection.Response response = conn.method(Connection.Method.GET).execute();
			String priceString = response.body();
        	Response resultImageResponse = Jsoup.connect(url).ignoreContentType(true).execute();
        	FileOutputStream out = (new FileOutputStream(new java.io.File("d://aa.html")));
        	out.write(resultImageResponse.bodyAsBytes());
        	out.close();
			//log.info(priceString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Test
    public void Test01(){
        String refer = "https://world.tmall.com/";
        String cookie = "_med=dw:1792&dh:1120&pw:3584&ph:2240&ist:0; cq=ccp%3D1; sm4=230403; cna=M2toF58X2HECARd2L8G3UYZh; hng=US%7Czh-CN%7CUSD%7C840; lid=chaseju; _fbp=fb.1.1599917341931.911447292; enc=w0GnFeljlJBszKqiviVNm3gAArGbd9DfylMNq4aXzMDLaHgyHe0t8%2FBDnJtAt8M%2FPSFwxyjgk%2BhADuBcWXM%2BZQ%3D%3D; _slk_skip_tbpass_=true; dnk=chaseju; tracknick=chaseju; lgc=chaseju; login=true; cookie2=2acf3280899822690a5107812b9d7ae3; t=b78fdd3e27d098d999b2603695a89995; _tb_token_=53e56e7a3575; uc1=cookie16=UIHiLt3xCS3yM2h4eKHS9lpEOw%3D%3D&existShop=false&cookie15=U%2BGCWk%2F75gdr5Q%3D%3D&cookie21=W5iHLLyFe3xm&cookie14=Uoe0ZeTrNjQW2g%3D%3D&pas=0; uc3=lg2=VFC%2FuZ9ayeYq2g%3D%3D&nk2=AHLWll6xkA%3D%3D&vt3=F8dCuAMuSnDcjS7qdBE%3D&id2=UUpgT7m%2BqEmi%2FQ%3D%3D; uc4=nk4=0%40AhyAlqaIga8aHqQujXbX7MvM&id4=0%40U2gqwAZ6CN3vTK2EPUAooWLNwzFC; sgcookie=E100sxQ04ahu9eKl%2FwD5gKJYMrlciYsjNuXib4ORw4jPaHT4aRfmwAAJKQZi3j92xCqybSHq%2FC9Xgyl1l2MZNThImA%3D%3D; csg=b952bfc4; sm4=320500; _m_h5_tk=8683887a2dfbe87e244a97cdef0dfdd7_1608713801945; _m_h5_tk_enc=704f236565c723e04df770db892a1470; pnm_cku822=098%23E1hv1QvUvbpvUpCkvvvvvjiWP2LwQjYjR2s90jthPmPU6jiEPLz9QjiUPLdvzjrnR4OCvvBvpvpZRvhvChCvvvvRvpvhMMGvvvvCvvXvovvvvvmgvpvIphvvXvvvphCvpCQvvvCCK6CvjvvvvhpyphvwvvvvBvnvpCQvvvChvT9Cvv3vpvLXBTRZnI9CvvXmp99WjE%2BUvpCWpOzbv8RQW5hSotZHR3pDN5Hja4p7%2B3%2Biwos6D40OJmx%2FpExreEQaWox%2FAnoK5kx%2FwZDlGCTAVAil8bmxdX3gEc6OfwkOde%2BRVA5Zap%3D%3D; tfstk=cau1ByVnMx3F_v-V7fOUgb6hzvaVavpQGhwZ1c0U5hnljFl_pscPz81S1mXfJuFC.; l=eBQ6IceIQtnaDq5QBO5a-urza779NIdfGrVzaNbMiInca6ZdtiauFNQ20j92Sdtj_t5boetyPQBt2REvP7a3WdtxvtHOaE4SivJwSe1..; isg=BGJi3H_-SV5K4lQP5Hld4SFus-7Es2bNkIYt4qz7jlWXfwL5lEO23ej9q_Mm795l";
        String url_3 = "https://mdskip.taobao.com/core/changeLocation.htm?queryDelivery=true&queryProm=true&tmallBuySupport=true&ref=&areaId=230403&_ksTS=1608713186747_1150&callback=jsonp1151&isUseInventoryCenter=true&cartEnable=true&sellerUserTag3=144185556820066432&service3C=false&sellerUserTag2=18014536485306368&isSecKill=false&isAreaSell=true&sellerUserTag4=387&offlineShop=false&itemId=542776040329&sellerUserTag=125255532&showShopProm=false&tgTag=false&isPurchaseMallPage=true&isRegionLevel=true&notAllowOriginPrice=false&addressLevel=2";
        String page = JsoupUtil.getHtmlByJsoup(url_3, cookie, refer);
        System.out.println(page);
    }

}
