//package com.eddc.test;
//
///**
// * @描述
// * @参数 $
// * @返回值 $
// * @创建人 jack.zhao
// * @创建时间 $
// * @修改人和其它信息
// */
//import com.eddc.Application;
//import com.eddc.method.HpkData;
//import com.eddc.model.QrtzCrawlerTable;
//import com.eddc.service.impl.JobAndTriggerService;
//import com.eddc.util.ChaoJiYing;
//import com.eddc.util.Fields;
//import com.eddc.util.WebDriverUtil;
//import com.xxl.job.core.log.XxlJobLogger;
//
//import net.sf.json.JSONObject;
//
//import org.apache.commons.lang3.StringUtils;
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.openqa.selenium.By;
//import org.openqa.selenium.OutputType;
//import org.openqa.selenium.Point;
//import org.openqa.selenium.Rectangle;
//import org.openqa.selenium.TakesScreenshot;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import javax.imageio.ImageIO;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = Application.class)
//@WebAppConfiguration
//public class hpkDataTest {
//    @Autowired
//    private HpkData hpkData;
////    @Autowired
////    private JobAndTriggerService jobAndTriggerService;
//
//    @Test
//    //@Scheduled(cron = "0 56 9 * * ?")
//    public void hpkTest() throws IOException {
//    	//达能 17601265958  DAneng12
//        //String dataMessage="jobName=EDDC_HPK_JOB_108,platform=hpk,thread=1,database=CustomerMonitor,type=2,userName=15310241040,password=0c0218lu,fillCatch=1,crawlerStore=false,productdesc=达能,requestCount=2,crawlerFillCatch=1";
//        //美素
//        String dataMessage="jobName=EDDC_HPK_JOB_222,platform=hpk,thread=1,database=CustomerMonitor,type=2,userName=13818911942001,password=lsyysl1019,fillCatch=1,crawlerStore=true,productdesc=无,requestCount=2,crawlerFillCatch=1";
//        String requestCount="1";
//        List<QrtzCrawlerTable> listjobName=jobAndTriggerService.queryJob("EDDC_HPK_JOB_222");
//
//        hpkData.regionCrawler(listjobName, Fields.TABLE_CRAW_KEYWORDS_INF, "1", 0, Integer.valueOf(requestCount),getParams(dataMessage));
//    }
//
//
//    @Test
//    public void hpkWebDriver() throws IOException, Throwable {
//    	WebDriver driver = WebDriverUtil.getWebDriver();
//    	WebElement verifyCodeElement = null;
//    	File directory = new File(".");
//		String imageUrl = directory.getCanonicalPath();
//
//		for (int i = 0; i < 15; i++) {
//			try {
//				driver.get("https://mall.hipac.cn/mall/view/login/login.html?loginFlag=false&action=login");
//				String itemData = driver.getPageSource();
//				Document dataMessage = Jsoup.parse(itemData);
//				String url = "http:" + dataMessage.getElementById("loginMain").getElementsByTag("iframe").attr("src").toString();
//				driver.get(url);
//				Thread.sleep(5000);
//				driver.findElement(By.xpath(".//*[@id='username']")).clear();
//				driver.findElement(By.id("username")).sendKeys("13818911942001");
//				driver.findElement(By.xpath(".//*[@id='password']")).clear();
//				driver.findElement(By.xpath(".//*[@id='password']")).sendKeys("lsyysl1019");
//				driver.findElement(By.xpath(".//*[@id='kaptcha']")).clear();
//				captureElement(driver);
//				String result = ChaoJiYing.PostPic(imageUrl + "\\code.png");
//				JSONObject currPriceJson = JSONObject.fromObject(result);
//				result = currPriceJson.getString("pic_str");
//				Thread.sleep(10000);
//				driver.findElement(By.xpath(".//*[@id='kaptcha']")).sendKeys(result);
//				driver.findElement(By.className("login-btn")).click();
//				Thread.sleep(9000);
//				driver.get("https://mall.hipac.cn/mall/view/goods/itemIndex.html?t=1579160110239");
//				String item = driver.getPageSource();
//				XxlJobLogger.log("==>>当前是第："+(i+1)+"次 请求数据<<==");
//				if (item.contains("巨划算")) {
//					break;
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//
//    	Thread.sleep(9000);
//    	driver.get("https://mall.hipac.cn/ytms/page/qualification.html?supplierId=cce38e9355684cf4923a3ff297174e02");
//    	File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//		BufferedImage img = ImageIO.read(screen);
//		verifyCodeElement = driver.findElement(By.cssSelector("#J_container > div > div.codeImageWrapper > div.inputImage > img"));
//		Point p = verifyCodeElement.getLocation();
//		// 创建一个矩形使用上面的高度，和宽度
//		Rectangle rect = new Rectangle(p, verifyCodeElement.getSize());
//		// 得到元素的坐标
//		BufferedImage dest = img.getSubimage(p.getX(), p.getY(), rect.width, rect.height);
//		//存为png格式
//		ImageIO.write(dest, "png", new File("store.png"));
//		String result = ChaoJiYing.PostPic(imageUrl + "\\store.png");
//		JSONObject currPriceJson = JSONObject.fromObject(result);
//		result = currPriceJson.getString("pic_str");
//		try {
//			Thread.sleep(9000);
//			driver.findElement(By.cssSelector("#J_container > div > div.codeImageWrapper > div.inputImage > input")).sendKeys(result);
//			driver.findElement(By.cssSelector("#J_container > div > div.codeImageWrapper > div:nth-child(3) > button")).click();
//			driver.switchTo().alert().accept();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    }
//    public  void captureElement(WebDriver driver) throws IOException {
//		WebElement verifyCodeElement = null;
//		File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//		BufferedImage img = ImageIO.read(screen);
//		verifyCodeElement = driver.findElement(By.xpath("//img[@class='small-btn f-r']"));
//		Point p = verifyCodeElement.getLocation();
//		// 创建一个矩形使用上面的高度，和宽度
//		Rectangle rect = new Rectangle(p, verifyCodeElement.getSize());
//		// 得到元素的坐标
//		BufferedImage dest = img.getSubimage(p.getX(), p.getY(), rect.width, rect.height);
//		//存为png格式
//		ImageIO.write(dest, "png", new File("code.png"));
//	}
//
//
//    @SuppressWarnings({"rawtypes", "unchecked"})
//    public Map getParams(String param) {
//        Map params = new HashMap();
//        if (StringUtils.isNotEmpty(param)) {
//            String[] ps = StringUtils.split(param, ",");
//            for (String p : ps) {
//                String[] s = StringUtils.split(p, "=");
//                params.put(StringUtils.trim(s[0]), StringUtils.trim(s[1]));
//            }
//        }
//        return params;
//    }
//
//}
